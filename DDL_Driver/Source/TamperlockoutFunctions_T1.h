//------------------------------------------------------------------------------
/** 	@file		TamperlockoutFunctions_T1.h
	@brief	3분락 관련 처리 함수
*/
//------------------------------------------------------------------------------


#ifndef __TAMPERLOCKOUTFUNCTIONS_T1_INCLUDED
#define __TAMPERLOCKOUTFUNCTIONS_T1_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"



//------------------------------------------------------------------------------
/*! 	\def	TAMPER_PROOF_LOCKOUT_TIME		
	gTamperProofLockoutTimer1s에서 참조 - 3분락 동작 설정 시간, 3분 설정
*/
//------------------------------------------------------------------------------
#define	TAMPER_PROOF_LOCKOUT_TIME		180


//------------------------------------------------------------------------------
/*! 	\def	MAX_WRONG_CODE_ENTRY_LIMIT		
	gAuthFailCnt에서 참조 - 인증 오류 회수
*/
//------------------------------------------------------------------------------
#define	MAX_WRONG_CODE_ENTRY_LIMIT		5



void TamperCountClear(void);
void TamperCountIncrease(void);
void TamperCountLoad(void);

BYTE GetTamperProofPrcsStep(void);
void TamperProofLockoutCounter(void);

void TamperProofProcess(void);


#endif

