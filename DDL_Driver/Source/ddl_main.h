#ifndef	_DDL_MAIN_H_
#define	_DDL_MAIN_H_

#ifdef	_DDL_MAIN_C_

	#ifdef	DDL_CFG_DIMMER
	ddl_i2c_t		gl_dimming_i2c;
	#endif

	#ifdef	DDL_CFG_TOUCHKEY
	ddl_i2c_t		gl_tkey_i2c;
	#endif

#else

	#ifdef	DDL_CFG_DIMMER
	extern	ddl_i2c_t		gl_dimming_i2c;
	#endif

	#ifdef	DDL_CFG_TOUCHKEY
	extern	ddl_i2c_t		gl_tkey_i2c;
	#endif

#endif

void NansuGenerate();
void DDLStatusFlagSet(BYTE bFlagData);
void DDLStatusFlagClear(BYTE bFlagData);


void	InitializeDDL_Var( void );
void	InitializeDDL_HW( void );
void	InitializeDDL_SW( void );
#ifdef DDL_CFG_FACTORY_DEFAULT_SETTING
void FactoryDefaultSetting(void);
#endif 

void	StartDDL_SW( void );
uint16_t	DDL_main( void );

void GPIO_SetToPowerDown(void);



extern BYTE gbtypevalue;
extern BYTE gbVoicevalue;
#define	DDL_SET_RF		0x01
#define	DDL_SET_BIO	0x02
#define	DDL_SET_RF_BIO	0x03
#define	DDL_SET_VOICE			0x01
#define	DDL_SET_NO_VOICE		0x00



extern FLAG MainSysFlag;
#define	gbMainSysData		MainSysFlag.STATEFLAGDATA
#define	gfLowBattery		MainSysFlag.STATEFLAG.Bit0
#define	gfBatteryStatus		MainSysFlag.STATEFLAG.Bit2
#define	gfHighTempAlarm	MainSysFlag.STATEFLAG.Bit3
#ifdef DDL_CFG_EMERGENCY_119
#define	gfEmergencyRun		MainSysFlag.STATEFLAG.Bit4
#endif 
#define	gfDeadBoltErr		MainSysFlag.STATEFLAG.Bit7	


extern FLAG MainSysErrFlag;
#define	gbMainSysErrData	MainSysErrFlag.STATEFLAGDATA
#define	gfRfDetectErr		MainSysErrFlag.STATEFLAG.Bit0	
#define	gfRtcErr				MainSysErrFlag.STATEFLAG.Bit1
#define	gfEepromErr			MainSysErrFlag.STATEFLAG.Bit2
#define	gfTouchErr			MainSysErrFlag.STATEFLAG.Bit3
#define   gfBioErr				MainSysErrFlag.STATEFLAG.Bit4
#define 	gfBioRegnumErr		MainSysErrFlag.STATEFLAG.Bit5
#define	gfMotorSensorErr		MainSysErrFlag.STATEFLAG.Bit6
#define	gfHandingLockErr		MainSysErrFlag.STATEFLAG.Bit7


extern FLAG MainSysTempFlag;
#define	gbMainSysTempData	MainSysTempFlag.STATEFLAGDATA
#define	gfVoiceSet			MainSysTempFlag.STATEFLAG.Bit0	
#define	gfOutLockReq		MainSysTempFlag.STATEFLAG.Bit1	
#define	gfHandleOpen		MainSysTempFlag.STATEFLAG.Bit2
#define	gfArmGoOut			MainSysTempFlag.STATEFLAG.Bit4
#define	gfSystemErr			MainSysTempFlag.STATEFLAG.Bit5


extern FLAG DDLStateFlag;
#define	gbDDLStateData		DDLStateFlag.STATEFLAGDATA
#define	gfDDLStateRst		DDLStateFlag.STATEFLAG.Bit0
#define	gfManualLock		DDLStateFlag.STATEFLAG.Bit1
#define gfOutLock			DDLStateFlag.STATEFLAG.Bit2
#define	gfIntrusionAlarm	DDLStateFlag.STATEFLAG.Bit3
#define	gfBrokenAlarm		DDLStateFlag.STATEFLAG.Bit4
#define	gfTamperProofSet	DDLStateFlag.STATEFLAG.Bit5
#ifdef P_KEY_INTER_CL_T
#define	gfKeyInterClAlarm	DDLStateFlag.STATEFLAG.Bit6				// 보이스모드 설정 (1:보이스, 0:사운드 )
#else 
#define	gfEnableVoice		DDLStateFlag.STATEFLAG.Bit6				// 보이스모드 설정 (1:보이스, 0:사운드 )
#endif 
#define	gfFailRegBio    	DDLStateFlag.STATEFLAG.Bit7				// 지문 등록시 메모리 가득차 저장시.


#define	DDL_STS_MANUALLOCK			(1<<1)
#define	DDL_STS_OUTLOCK				(1<<2)
#define	DDL_STS_INTRUSION_ALARM		(1<<3)
#define	DDL_STS_BROKEN_ALARM		(1<<4)
#define	DDL_STS_TAMPER_PROOF		(1<<5)
#ifdef P_KEY_INTER_CL_T
#define	DDL_STS_KEY_INTER_CL_ALARM		(1<<6)
#endif 
#define	DDL_STS_FAIL_BIO			(1<<7)



#define STATUS_SUCCESS          (0x0000)	
#define	STATUS_FAIL				(0x0001)
#define	STATUS_WRONG_KEY		(0x0002)
#define	STATUS_MKS				(0x0003)
#define	STATUS_PROCESSING		(0x0004)


extern BYTE gtbNansu[8];
extern BYTE gtbNansuResetCnt;

/*
	AbnormalStatus
	4 byte gwLockModeStatus 저장 
	gbLockInfo[0] = gwLockModeStatus LSB
	gbLockInfo[1] = gwLockModeStatus 
	gbLockInfo[2] = gwLockModeStatus 
	gbLockInfo[3] = gwLockModeStatus MSB

	gbLockInfo[4] = Reset 전 MainMode 값 
	gbLockInfo[5] = Reset 전 gbModePrcsStep 값 	
	gbLockInfo[6] = Reset 전 GetCardMode() 값 
	gbLockInfo[7] = Reset 전 gCardProcessStep 값

	gbLockInfo[8] = uart1 Pack Connection Mode 
	gbLockInfo[9] = uart1 Pack Tx mode
	gbLockInfo[10] = uart1 Pack Rx mode

	gbLockInfo[11] = uart2 Pack Connection Mode
	gbLockInfo[12] = uart2 Pack Tx mode
	gbLockInfo[13] = uart2 Pack Rx mode
	
	gbLockInfo[14] = 마지막 GPIO interrupt PIN number LSB
	gbLockInfo[15] = 마지막 GPIO interrupt PIN number MSB	
	gbLockInfo[16] = SYSTEM_ERROR_FLAG
	gbLockInfo[17] = DDL_STATEFLAG	
	gbLockInfo[18] = 마지막 key 값 		
	gbLockInfo[19] = 마지막 switch 값 

	RESERVED 
	
	gbLockInfo[22] = Reset Count	
	gbLockInfo[23] = Silent Reset Flag 	
*/
typedef union
{
	WORD gwLockModeStatus;						
	BYTE gbLockInfo[24];
}LOCKSTATUS;

enum{
	LOCK_MAIN_MODE = 4,
	LOCK_MAIN_STEP_MODE = 5,
	LOCK_CARD_MODE = 6,
	LOCK_CARD_STEP_MODE = 7,  
	
	LCOK_UART1_CONNECTION_MODE = 8,
	LCOK_UART1_TX_PROCESS_STOP = 9,
	LCOK_UART1_RX_PROCESS_STOP = 10,
	
	LCOK_UART2_CONNECTION_MODE = 11,
	LCOK_UART2_TX_PROCESS_STOP = 12,
	LCOK_UART2_RX_PROCESS_STOP = 13,
	
	LOCK_LAST_GPIO_INT_PIN_LSB = 14,	
	LOCK_LAST_GPIO_INT_PIN_MSB = 15,
	
	LOCK_MAIN_SYSTEM_ERROR_FLAG = 16,
	LOCK_MAIN_DDL_STATEFLAG_FLAG = 17,

	LOCK_LAST_KEY = 18,	
	LOCK_LAST_SWITCH = 19,
	/* RESERVED */
	SILENT_RESET_COUNT = 22,
	SILENT_RESET_FLAG = 23
};

void SetLockStatusInfo(BYTE index , BYTE Data);
void SetAbnormalSleepCheckTimer(WORD Data);
void Silent_Reset_Check(void);
WORD GetAbnormalSleepCheckTimer(void);
void AbnormalSleepCheckCounter(void);

#endif	//~_DDL_MAIN_H_

