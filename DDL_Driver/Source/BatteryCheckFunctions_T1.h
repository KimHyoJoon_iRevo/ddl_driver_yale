//------------------------------------------------------------------------------
/** 	@file		BatteryCheckFunctions_T1.c
	@brief	Battery Check with ADC
*/
//------------------------------------------------------------------------------


#ifndef __BATTERYCHECKFUNCTIONS_T1_INCLUDED
#define __BATTERYCHECKFUNCTIONS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


#define	PERCENT_OF_LOWBATTERY	10

//void BatteryCheckTimeCount(void);
void BatteryCheckStart(void);
WORD GetBatteryAdcValue(void);
void BatteryCheck(void);
void BatteryCheck_No_LED_control(void);

BYTE MakePercetageOfBatteryLevel(void);


#endif


