
#include "Main.h"



//==============================================================================//
// global variables								//
//==============================================================================//
BYTE gbIbShift = 0;
BYTE gbIbBitCount = 0;
BYTE gbIbByteCount = 0;
BYTE gbTouchSignalSeq = 0;
BYTE gfIbOldBit = 0;
BYTE gfIbRecOk = 0;
BYTE gbTkTimer2ms = 0;

WORD gGmTouchKeyStartTimeCnt = 0;

BYTE gbiButtonBuf[10];




void GMTouchKeyTimeCount(void)
{
	if(gbTkTimer2ms)		--gbTkTimer2ms;
}


void SetGMTouchKeyTime(BYTE TimeValue)
{
	gbTkTimer2ms = TimeValue;
}


BYTE GMTouchKeyTimeCheck(void)
{
	if(gbTkTimer2ms == 0)
	{
		return (STATUS_SUCCESS);
	}	

	return (STATUS_FAIL);
}


BYTE GMTouchKeyInputCheck(void)
{
	if(gfIbRecOk)
	{
		return (STATUS_SUCCESS);
	}	

	return (STATUS_FAIL);
}


void GMTouchKeyInputClear(void)
{
	gfIbRecOk = 0;
}



void StartDataTimingCheck(void)
{
	gGmTouchKeyStartTimeCnt = SystickLoad();
}


WORD GetDataTimingResult(void)
{
	WORD wTmp;
	
	wTmp = SystickLoad();
	wTmp = (wTmp - gGmTouchKeyStartTimeCnt) & 0xFFFF;
	
	return (wTmp);
}




BYTE PrcsTkBccCheck(void)
{
	BYTE bIbBcc = 0;
	BYTE bCnt;
	BYTE fCheck = 1;
	
	for(bCnt = 1; bCnt < 7; bCnt++) 
	{
		bIbBcc += gbiButtonBuf[bCnt];
		if((gbiButtonBuf[bCnt] != 0xFF) && (gbiButtonBuf[bCnt] != 0x00))
		{
			fCheck = 0;	
		}
	}
	
	//When all ID bytes are FFh or 00h, it is error.
	if(fCheck)	return 2;
	
	if(bIbBcc == gbiButtonBuf[7])	return 1;
	
	return 0;
}	

//==============================================================================//
// purpose  	: 수신한 8비트를 바이트로 저장하고 동시에 수신하고자 하는 	//
//		: 데이타 수만큼 수신하였는지를 처리하는 함수 			//
//		: LSB first send						//
// input    	: sVal		: 비트값					//
// output   	: void								//
//==============================================================================//
void TkBitDef(BYTE sVal)
{
	gbIbShift >>= 1;
	gbIbBitCount++;
	
	if(sVal) 	gbIbShift |= 0x80;	
	else 		gbIbShift &= (BYTE)~0x80;
	
  	if(gbIbBitCount == 8) 
	{
		gbIbBitCount = 0;
		gbiButtonBuf[gbIbByteCount] = gbIbShift;
		gbIbByteCount++;
		if(gbIbByteCount == 8) 
		{
			if(PrcsTkBccCheck() == 1)
			{
				gfIbRecOk = 1;
			}
			else
			{
				gfIbRecOk = 0;
				gbTouchSignalSeq = 0;
			}
		}
	}
}


#define	TOUCHKEY_SYNCH_CHECK		1
#define	TOUCHKEY_DATA_RECEIVING		2


// 10us Tick을 이용해 Signal Time 확인

//3500us < 4000us <= 4500us
#define	SYNCH_SIG_MIN_TIME			(400 - 50)		
#define	SYNCH_SIG_MAX_TIME			(400 + 50)


//400us < 500us <= 600us
#define	DATA_BIT_TYPE_1_MIN			(50 - 10)
#define	DATA_BIT_TYPE_1_MAX			(50 + 10)


//600us < 750us <= 900us
#define	DATA_BIT_TYPE_2_MIN			(75 - 15)
#define	DATA_BIT_TYPE_2_MAX			(75 + 15)


//200us < 250us <= 300us
#define	DATA_BIT_TYPE_3_MIN			(25 - 5)
#define	DATA_BIT_TYPE_3_MAX			(25 + 5)


//600us < 750us <= 870us
#define	DATA_BIT_TYPE_4_MIN			(75 - 15)
#define	DATA_BIT_TYPE_4_MAX			(75 + 12)


//870us < 1000us <= 1200us
#define	DATA_BIT_TYPE_5_MIN			(100 - 13)
#define	DATA_BIT_TYPE_5_MAX			(100 + 20)



//==============================================================================//
// purpose  	: interrupt handler of INT7		 			//
//		: P_TOUCHKEY_IN	for MODEL_TOUTCH_BUZZER_AD			//
// input    	: void								//
// output   	: void								//
//==============================================================================//
void TkIn(void)
{
	WORD wIbSigTime;
	BYTE bIbData;
	BYTE bCnt;

	SetGMTouchKeyTime(100);
	
	switch(gbTouchSignalSeq) 
	{
		//======================================================================//
		//synch bit를 확인하는 단계(margain about 10%)				//
		//======================================================================//
		case 0:
			if(gfIbRecOk)		break;

			// Rising Edge만 확인
			if(!P_IBUTTON_T)	break;

			memset(gbiButtonBuf, 0xFF, MAX_KEY_UID_SIZE);
			StartDataTimingCheck();
			gbTouchSignalSeq = TOUCHKEY_SYNCH_CHECK;
			break;

		case TOUCHKEY_SYNCH_CHECK:
			// Rising Edge만 확인
			if(!P_IBUTTON_T)	break;

			wIbSigTime = GetDataTimingResult();		
			StartDataTimingCheck();
			if((wIbSigTime > SYNCH_SIG_MIN_TIME) && (wIbSigTime <= SYNCH_SIG_MAX_TIME))
			{		
				gbIbShift = 0;
				gfIbOldBit = 0;
				gbIbBitCount = 0;
				gbIbByteCount = 0;
				gbTouchSignalSeq = TOUCHKEY_DATA_RECEIVING;


				for(bCnt = 0; bCnt < 8; bCnt++)
				{	
					gbiButtonBuf[bCnt] = 0xFF;
				}	
			}
			break;

		//======================================================================//
		//data bit를 수신하는 단계(margain 20%)					//
		//bIbData = 10	: noise	sinal						//
		// 1		: old 1 -> now 1					// 
		// 2		: old 1 -> now 1 and 0					//
		// 3		: old 0 -> now 0					//
		// 4		: old 0 -> now 1					//
		// 5		: old 0 -> now 1 and 0					//
		//======================================================================//
		case TOUCHKEY_DATA_RECEIVING:
			// Falling Edge만 확인
			if(P_IBUTTON_T)		break;

			wIbSigTime = GetDataTimingResult();		
			StartDataTimingCheck();
	        if(gfIbOldBit) 
			{
		        if((wIbSigTime > DATA_BIT_TYPE_1_MIN) && (wIbSigTime <= DATA_BIT_TYPE_1_MAX)) 
				{	
		        	bIbData = 1;	
		        }
		        else if((wIbSigTime > DATA_BIT_TYPE_2_MIN) && (wIbSigTime <= DATA_BIT_TYPE_2_MAX)) 
				{	
		        	bIbData = 2;	
		        }
	            else 
				{
	               	bIbData = 10;
	            }
	        }
	        else 
			{          
        		//======================================================//
	        	// synch bit 다음에는 bit0 이 반드시 온다		//
	        	// 프로토콜상의 규정인 듯				//
	        	//======================================================//
		        if((wIbSigTime > DATA_BIT_TYPE_3_MIN) && (wIbSigTime <= DATA_BIT_TYPE_3_MAX)) 
				{	
		        	bIbData = 3;	
		        }
		        else if((wIbSigTime > DATA_BIT_TYPE_1_MIN) && (wIbSigTime <= DATA_BIT_TYPE_1_MAX)) 
				{	
		        	bIbData = 3;	
		        }
		        else if((wIbSigTime > DATA_BIT_TYPE_4_MIN) && (wIbSigTime <= DATA_BIT_TYPE_4_MAX)) 
				{	
		        	bIbData = 4;	
		        }
		        else if((wIbSigTime > DATA_BIT_TYPE_5_MIN) && (wIbSigTime <= DATA_BIT_TYPE_5_MAX)) 
				{	
		        	bIbData = 5;	
		        }
	            else 
				{
	               	bIbData = 10;
	            }
	        }

			switch(bIbData) 
			{
				case 1:
				case 4:
					TkBitDef(1);
					gfIbOldBit = 1;
					break;

				case 2:
				case 5:
					TkBitDef(1);
					TkBitDef(0);
					gfIbOldBit = 0;
					break;

				case 3:
					TkBitDef(0);
					gfIbOldBit = 0;
					break;

				default:
					gbTouchSignalSeq = 0;
					break;
			}
			
			if(gfIbRecOk)
			{
				gbTouchSignalSeq = 0;
			}
			break;

		default:
			break;
	}
}



BYTE GMTouchKeyReceiveStatusCheck(void)
{
	if(gbTouchSignalSeq == 0)
	{
		return 0;
	}	

	if(GMTouchKeyTimeCheck() == STATUS_SUCCESS)
	{
		gfIbRecOk = 0;
		gbTouchSignalSeq = 0;

		return 0;
	}
		
	return 1;
}




void GMTouchKeyDetectionProcess(void)
{
	if(GMTouchKeyInputCheck() == STATUS_SUCCESS)
	{
		JigInputDataSave(0x00, gbiButtonBuf, MAX_KEY_UID_SIZE);

		ModeGotoGMTouchKeyVerify();
	}
}









