//==============================================================================//
// Company  	: IREVO	Inc							//
// Project  	: CT-860							//
// CPU		: uPD78F0544-80LQFP						//
// Clock	: 8MHz(Internal RC high speed ring oscillator)			//
// Compile	: 								//
// Design   	: Kim Seokhwan							//
//==============================================================================//
#ifndef _GMTOUCHKEYPROCESS_T1_H
#define _GMTOUCHKEYPROCESS_T1_H

#include "DefineMacro.h"
//#include "DefinePin.h"

//==============================================================================//
// extern function prototype							//
//==============================================================================//
void GMTouchKeyTimeCount(void);
void SetGMTouchKeyTime(BYTE TimeValue);
BYTE GMTouchKeyTimeCheck(void);
BYTE GMTouchKeyInputCheck(void);
void GMTouchKeyInputClear(void);


void TkIn(void);

BYTE GMTouchKeyReceiveStatusCheck(void);
void GMTouchKeyDetectionProcess(void);


//==============================================================================//
// extern global variables							//
//==============================================================================//
extern BYTE gbiButtonBuf[10];

//==============================================================================//
// touchkey									//
//==============================================================================//
#define	MAX_KEY_UID_SIZE		8





#endif


