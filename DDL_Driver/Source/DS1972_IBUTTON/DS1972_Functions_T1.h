#ifndef __DS1972FUNCTIONS_INCLUDED        
#define __DS1972FUNCTIONS_INCLUDED   


#include "DefineMacro.h"
//#include "DefinePin.h"

void  IBUTTON_DATA_MODE(BYTE Type);

BYTE iButtonReset(void);
BYTE iButtonReadRom(BYTE * bpData);
BYTE iButtonWriteMem(BYTE * bpData, BYTE bAdrs, BYTE bNum);
BYTE iButtonReadMem(BYTE * bpData, BYTE bAdrs, BYTE bNum);

#endif

