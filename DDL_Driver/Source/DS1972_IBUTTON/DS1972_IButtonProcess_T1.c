
#include "Main.h"

FLAG iButtonFlag;
UNKEYUIDBUF KeyBuffer;

BYTE gbiButtonMainProcessMode = 0;
BYTE gbiButtonMode = 0;
BYTE gbiButtonPrcsStep = 0;
BYTE gbiButtonReadStatus = 0;

BYTE gbPrcsiButtonTmp;						//RollCode를 저장을 위한 임시 변수
BYTE gbLonOnIDForiButton[8];
WORD gwDetectionTimer2ms = 0; //이게 정말 필요 한가 싶은데 ... 
WORD gwIbuttonVerifyDetectionTimer2ms = 0;


BYTE gbPhysicalKeyContact = 0;							//카드가 떨어지는걸 check 하기 위한 카운터

BYTE gbDS1972WriteBuf1[160];
BYTE gbDS1972WriteBuf2[80];
BYTE gbDS1972WriteBuf3[20];
BYTE gbDS1972ReadBuf[64];
BYTE gbDS1972Flag;

//BYTE gbTRFUIDBuf[75];	
//BYTE gbiButtonBuf[75];
//BYTE gbiButtonMemTmp[10];

BYTE DS1972TouchKeyInputCheck(void)
{
	if(gbiButtonReadStatus == IBTNREAD_SUCCESS ||gbiButtonReadStatus == IBTNREAD_SAME_UID)
	{
		return STATUS_SUCCESS;
	}

	if(gbiButtonReadStatus != 0)
	{
		iButtonGotoUIDCheck();
		gbiButtonReadStatus = 0;
	}
	return STATUS_FAIL ; 
}

void iButtonEnable(void)
{
	IBUTTON_DATA_MODE(INPUT);
	__HAL_GPIO_EXTI_CLEAR_IT(IBUTTON_COVER_RFID_GND_Pin);
	HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);
}

void iButtonDisable(void)
{
//	IBUTTON_DATA_MODE(INPUT);
	__HAL_GPIO_EXTI_CLEAR_IT(IBUTTON_COVER_RFID_GND_Pin);
	HAL_NVIC_DisableIRQ(EXTI3_IRQn);
}

BYTE iButtonCheck(void)
{
	BYTE bTmp;

	if(gwDetectionTimer2ms)		return 0;

	//50ms 마다 detection하기 위한 timer	
	gwDetectionTimer2ms = 25;

//	__disable_irq();
	bTmp = iButtonReset();
//	__enable_irq();
	
	if(bTmp == OK)
		return 1;

	return 2;
}



BYTE iButtonUIDRead(void)
{
	BYTE bTmp;

//	__disable_irq();
	bTmp = iButtonReadRom(gbiButtonBuf);
//	__enable_irq();

	if(bTmp == FAIL)		return 3;

	memcpy(gbLonOnIDForiButton,gbiButtonBuf,8);

	JigInputDataSave(0x00, gbiButtonBuf, MAX_KEY_UID_SIZE);

#ifdef	_MASTERKEY_IBUTTON_SUPPORT
	// Master Key iButton은 DS1971이거나 DS1972일 경우에만 처리
	if((gbiButtonBuf[0] != 0x14) && (gbiButtonBuf[0] != 0x2D))	 
		return 2;
#else	
	if(gbiButtonBuf[0] != 0x2D)
		return 2;             	
#endif

	return 1;
}



void iButtonRegOperation(void)
{
	RomWrite(gbDS1972WriteBuf1, (WORD)TOUCHKEY_UID, 16);			//20개의 Serial ID 저장 (160 byte)
	RomWrite(gbDS1972WriteBuf1+16, (WORD)TOUCHKEY_UID+16, 16);
	RomWrite(gbDS1972WriteBuf1+32, (WORD)TOUCHKEY_UID+32, 16);
	RomWrite(gbDS1972WriteBuf1+48, (WORD)TOUCHKEY_UID+48, 16);		//20개의 Serial ID 저장 (160 byte)
	RomWrite(gbDS1972WriteBuf1+64, (WORD)TOUCHKEY_UID+64, 16);
	RomWrite(gbDS1972WriteBuf1+80, (WORD)TOUCHKEY_UID+80, 16);		//20개의 Serial ID 저장 (160 byte)
	RomWrite(gbDS1972WriteBuf1+96, (WORD)TOUCHKEY_UID+96, 16);
	RomWrite(gbDS1972WriteBuf1+112, (WORD)TOUCHKEY_UID+112, 16);
	RomWrite(gbDS1972WriteBuf1+128, (WORD)TOUCHKEY_UID+128, 16);	//20개의 Serial ID 저장 (160 byte)
	RomWrite(gbDS1972WriteBuf1+144, (WORD)TOUCHKEY_UID+144, 16);

	RomWrite(gbDS1972WriteBuf2, (WORD)TOUCHKEY_FV, 16);            		//20개의 Floating ID 저장 (80 byte)
	RomWrite(gbDS1972WriteBuf2+16, (WORD)TOUCHKEY_FV+16, 16);            	//20개의 Floating ID 저장 (80 byte)
	RomWrite(gbDS1972WriteBuf2+32, (WORD)TOUCHKEY_FV+32, 16);            	//20개의 Floating ID 저장 (80 byte)
	RomWrite(gbDS1972WriteBuf2+48, (WORD)TOUCHKEY_FV+48, 16);            	//20개의 Floating ID 저장 (80 byte)
	RomWrite(gbDS1972WriteBuf2+64, (WORD)TOUCHKEY_FV+64, 16);            	//20개의 Floating ID 저장 (80 byte)

	RomWrite(gbDS1972WriteBuf3, (WORD)TOUCHKEY_RC, 4);        		//20개의 Roll Code값 저장 (20 byte)
	RomWrite(gbDS1972WriteBuf3+4, (WORD)TOUCHKEY_RC+4, 16);        	//20개의 Roll Code값 저장 (20 byte)

	gbDS1972WriteBuf1[0] = gInputKeyNumberForRegister;
	RomWrite(gbDS1972WriteBuf1, (WORD)KEY_NUM, 1);           		//등록된 Key 개수 저장
}



void	iButtonGeneralSave(void)
{
	BYTE bCnt;
	BYTE bNum;
	BYTE bTmp;
	BYTE bBuf[5];

//Roll Code 지정하기 위해 (Floating ID를 읽어오고 저장할 위치 저장)
	for(bCnt = 0; bCnt < 4; bCnt++)	
		bBuf[bCnt] = gbiButtonBuf[bCnt*16+11] & 0xF0;

	bBuf[4] = gbiButtonBuf[11] & 0xF0;
	for(bCnt = 0; bCnt < 4; bCnt++)
	{
		bTmp = bBuf[bCnt];
		if((bTmp & 0x30) != (bCnt << 4))
		{                       	
		//1번째 자리 : 0, 4
		//2번째 자리 : 1, 5
		//3번째 자리 : 2, 6
		//4번째 자리 : 3, 7  
		//	이외의 숫자가 들어가 있을 경우 무조건 그 자리에 기록
			bNum = bCnt;                                    	
			gbDS1972WriteBuf3[gInputKeyNumberForRegister] = (bCnt << 4) & 0x70; 	
			break;                                          	
		}

		//그 다음 Roll Code가 지금 현재의 값에서 1을 더한 값이 아니면 바로 아닌 그곳에 기록
		bTmp = (bTmp+0x10) & 0x70;                            	
		if(bTmp != bBuf[bCnt+1])
		{                        
			bNum = (bTmp >> 4) & 0x03;
			gbDS1972WriteBuf3[gInputKeyNumberForRegister] = bTmp;
			break;
		}
	}

	for(bCnt = 0; bCnt < 8; bCnt++)
	{
		gbDS1972WriteBuf1[gInputKeyNumberForRegister*8+bCnt] = gbLonOnIDForiButton[bCnt];
	}

	//최초 등록시는 Floating ID Write는 하지 않고 단지 기존의 값만
	//      Read 저장한다. 등록 취소 했을 경우 이전 Key값을 보존하기 위해서
	for(bCnt = 0; bCnt < 4; bCnt++)
	{
		gbDS1972WriteBuf2[gInputKeyNumberForRegister*4+bCnt] = gbiButtonBuf[bNum*16+bCnt];
	}
}


BYTE iButtonMasterCheck(BYTE bNum)
{
	if(gbiButtonBuf[bNum]   != 0x25)	return FAIL;
	if(gbiButtonBuf[bNum+1] != 0x80)    	return FAIL;
	if(gbiButtonBuf[bNum+2] != 0x25)    	return FAIL;
	if(gbiButtonBuf[bNum+3] != 0x80)    	return FAIL;

	return OK;
}


void	iButtonMasterSave(void)
{
	BYTE bCnt;

	for(bCnt = 0; bCnt < 8; bCnt++)
	{
		gbDS1972WriteBuf1[gInputKeyNumberForRegister*8+bCnt] = gbLonOnIDForiButton[bCnt];
	}
	gbDS1972WriteBuf1[gInputKeyNumberForRegister*8+6] |= 0xC0;				//마스터 키로 저장

	gbDS1972WriteBuf2[gInputKeyNumberForRegister*4] = 0x25;
	gbDS1972WriteBuf2[gInputKeyNumberForRegister*4+1] = 0x80;
	gbDS1972WriteBuf2[gInputKeyNumberForRegister*4+2] = 0x25;
	gbDS1972WriteBuf2[gInputKeyNumberForRegister*4+3] = 0x80;

	gbDS1972WriteBuf3[gInputKeyNumberForRegister] = 0x00;
}



BYTE iButtonUIDCheck(void)
{
	BYTE bCnt;
	BYTE bCnt1;
	BYTE bTmp;
	BYTE fSame;

	for(bCnt = 0; bCnt < gInputKeyNumberForRegister; bCnt++)
	{
		fSame = 1;
		for(bCnt1 = 0; bCnt1 < 8; bCnt1++)
		{
			if(bCnt1 == 6)	continue;
			bTmp = gbDS1972WriteBuf1[bCnt*8 + bCnt1];
			if(bTmp != gbiButtonBuf[bCnt1])
			{
				fSame = 0;
				break;
			}
		}
		//같은 ID가 있을 경우 (이미 등록했을 경우) 에러 처리
		if(fSame)	
			return FAIL;					
	}

	//iButton내 Memory를 읽기 위해 UID Data 임시 저장
	for(bCnt = 0; bCnt < 8; bCnt++)
	{
		gbLonOnIDForiButton[bCnt] = gbiButtonBuf[bCnt];
	}
	return OK;
}



BYTE iButtonFIDReadAll(void)
{
	BYTE bTmp;

//	__disable_irq();
	//iButton내 모든 Data Read (64 byte)
	bTmp = iButtonReadMem(gbiButtonBuf, 0x00, 64);					
//	__enable_irq();

	if(bTmp == FAIL)	return 2;

	return 1;
}



void iButtonBufInitial(void)
{
	//등록 카드 수 초기화
	gInputKeyNumberForRegister = 0;				

	//등록 버퍼 초기화
	memset(gbDS1972WriteBuf1, 0xFF, (SUPPORTED_DS1972KEY_NUMBER*8));
	memset(gbDS1972WriteBuf2, 0xFF, (SUPPORTED_DS1972KEY_NUMBER*4));
	memset(gbDS1972WriteBuf3, 0xFF, (SUPPORTED_DS1972KEY_NUMBER));
}
	


BYTE iButtonUIDVerify(void)
{
	BYTE fSame;
	BYTE bCnt;

	RomRead(gbDS1972ReadBuf, (WORD)TOUCHKEY_UID+(8*gInputKeyNumberForRegister), 8);

	fSame = 1;
	for(bCnt = 0; bCnt < 8; bCnt++)
	{
		if(bCnt == 6)		continue;
		if((gbDS1972ReadBuf[bCnt]) != (gbiButtonBuf[bCnt]))
		{
			fSame = 0;
			break;
		}
	}
	
	if(fSame)
	{	
		if((gbDS1972ReadBuf[6] & 0xC0) == 0xC0)
		{
		//Registered Master Key	
			return 3;						
		}
		//Registered Normal Key	
		return 1;                                			
	}

	gInputKeyNumberForRegister++;
	if(gInputKeyNumberForRegister >= SUPPORTED_DS1972KEY_NUMBER)	return 2;

	return 0;
}

void iButtonRollCodeRead(void)
{
	RomRead(&gbPrcsiButtonTmp, (WORD)TOUCHKEY_RC+gInputKeyNumberForRegister, 1); 
}



BYTE iButtonFIDRead(void)
{
	BYTE bTmp;

	bTmp = (gbPrcsiButtonTmp >> 4) & 0x03;
//	__disable_irq();
	bTmp = iButtonReadMem(gbiButtonBuf, bTmp*16, 16);
//	__enable_irq();

	if(bTmp == FAIL)	return 2;
	return 1;
}


BYTE iButtonFIDVerify(void)
{
	BYTE bCnt;
	BYTE fSame;

	RomRead(gbDS1972ReadBuf, (WORD)TOUCHKEY_FV+(gInputKeyNumberForRegister*4), 4);

	fSame = 1;
	for(bCnt = 0; bCnt < 4; bCnt++)
	{
		if(gbiButtonBuf[bCnt] != gbDS1972ReadBuf[bCnt])
		{
			fSame = 0;
			break;
		}
	}
	if(fSame)	return 1;

	fSame = 1;
	for(bCnt = 0; bCnt < 4; bCnt++)
	{
		if(gbiButtonBuf[bCnt+8] != gbDS1972ReadBuf[bCnt])
		{
			fSame = 0;
			break;
		}
	}
	if(fSame)	return 2;

	return 3;

}


BYTE iButtonNewFIDWrite(BYTE bAdrs)
{
	BYTE bTmp;

//	__disable_irq();
	bTmp = iButtonWriteMem(gtbNansu, bAdrs, 8);
//	__enable_irq();

	if(bTmp == FAIL)	return 2;

	memcpy(gbDS1972WriteBuf1, gtbNansu, 4);
	return 1;
}


BYTE iButtonNewFIDConfirm(BYTE bAdrs)
{
	BYTE bTmp;

//	__disable_irq();
	bTmp = iButtonReadMem(gbiButtonBuf, bAdrs, 4);
//	__enable_irq();

	if(bTmp == FAIL)	return 2;

	bTmp = (BYTE)memcmp(gbiButtonBuf, gbDS1972WriteBuf1, 4);
	if(bTmp)		return 3;

	return 1;
}


void iButtonNewFIDSave(void)
{
	RomWrite(gbDS1972WriteBuf1, (WORD)TOUCHKEY_FV+(gInputKeyNumberForRegister*4), 4);
}



void iButtonModeClear(void)
{
	gbiButtonMode = 0;
	gbiButtonPrcsStep = 0;
//	gbDS1972Flag = 0;
}



void iButtonGotoUIDCheck(void)
{
	gbPhysicalKeyContact = 5;

	gbiButtonMode = IBTNMODE_UID_CHECK;
	gbiButtonPrcsStep = IBTNPRCS_INPUT_CHECK;
}

void iButtonGotoRegModeStart(void)
{
	gbiButtonMainProcessMode = IBTNMAIN_REGISTER_MODE;	

	gbiButtonReadStatus = 0;
	iButtonBufInitial();
	
	iButtonGotoUIDCheck();
}

void iButtonGotoVeriModeStart(void)
{
	gbiButtonMainProcessMode = IBTNMAIN_VERIFY_MODE;	

	gbiButtonReadStatus = 0;
}


void iButtonGotoReadStop(void)
{
	gbiButtonMode = IBTNMODE_IBUTTON_AWAY_CHK;
	gbiButtonPrcsStep = IBTNPRCS_CONTACT_OFF;

	gbiButtonReadStatus = 0;
}

void iButtonGotoFIDCheck(void)
{
	gbiButtonMode = IBTNMODE_FID_CHECK;
	gbiButtonPrcsStep = IBTNPRCS_FID_READ;
}

void iButtonGotoUIDVerify(void)
{
	gInputKeyNumberForRegister = 0;
	gbPhysicalKeyContact = 5;

	gbiButtonMode = IBTNMODE_UID_VERIFY;
	gbiButtonPrcsStep = IBTNPRCS_LOCKOUT_CHK;
}


void iButtonGotoFIDVerify(void)
{
	gbiButtonMode = IBTNMODE_FID_VERIFY;
	gbiButtonPrcsStep = IBTNPRCS_ROLLCODE_READ;
}


void iButtonGotoFIDWrite1(void)
{
	gbiButtonMode = IBTNMODE_FID_WRITE;
	gbiButtonPrcsStep = IBTNPRCS_FID_WRITE1;
}


void iButtonGotoFIDWrite2(void)
{
	gbiButtonMode = IBTNMODE_FID_WRITE;
	gbiButtonPrcsStep = IBTNPRCS_FID_WRITE2;
}


void iButtonGotoiButtonAwayCheck(void)
{
	gbiButtonMode = IBTNMODE_IBUTTON_AWAY_CHK;
	gbiButtonPrcsStep = IBTNPRCS_AWAY_CHECK;
}


void iButtonGotoiButtonModeClear(void)
{
	gbiButtonMode = IBTNMODE_IBUTTON_AWAY_CHK;
	gbiButtonPrcsStep = IBTNPRCS_CONTACT_OFF;
}


BYTE iButtonModeStatusCheck(void)
{
	BYTE bTmp;
	
	if(gbiButtonMode)	
		bTmp = STATUS_SUCCESS;
	else
		bTmp = STATUS_FAIL;
	
	return bTmp;
}



void iButtonModeIdleState(void)
{
	switch(gbiButtonPrcsStep)
	{
		case IBTNPRCS_IDLE_STATE:
			break;

		default:
			break;
	}
}


void iButtonModeUIDCheck(void)
{
	BYTE bTmp;
//	BYTE bBuf[10];
	
	switch(gbiButtonPrcsStep)
	{
		case IBTNPRCS_INPUT_CHECK:
			gbiButtonPrcsStep = IBTNPRCS_PRESENCE_CHECK;
			break;

		case IBTNPRCS_PRESENCE_CHECK:
			bTmp = iButtonCheck(); 
/*
bBuf[0] = gbiButtonBuf[0];
bBuf[1] = gbiButtonBuf[1];
bBuf[2] = gbiButtonBuf[2];
bBuf[3] = gbiButtonBuf[3];
bBuf[4] = bTmp;

PrintDebugData(bBuf, 5, DEBUG_TX_DATA);
*/

			if(bTmp == 1)
			{
				gbPhysicalKeyContact = 5;
				gbiButtonPrcsStep = IBTNPRCS_UID_READ;
			}	
			else if(bTmp == 2)
			{
				if(gbPhysicalKeyContact == 0)
				{
					gbiButtonReadStatus = IBTNREAD_NO_KEY;
					iButtonGotoiButtonModeClear();
				}
				else
				{
					if(gbPhysicalKeyContact)	gbPhysicalKeyContact--;
				}
			}
			break;
			
		case IBTNPRCS_UID_READ:
			bTmp = iButtonUIDRead();
			if(bTmp == 1)
			{
				gbiButtonPrcsStep = IBTNPRCS_UID_CHECK;
			}
			else if(bTmp == 2)
			{						
				//DS1972 Type이 아닌 경우
				gbiButtonReadStatus = IBTNREAD_WRONG_KEY;
				iButtonGotoiButtonAwayCheck();
			}
			else if(bTmp == 3)
			{
				iButtonGotoUIDCheck();
			}
			break;

		case IBTNPRCS_UID_CHECK:
			bTmp = iButtonUIDCheck();					
			if(bTmp == OK)
			{
#ifdef	_MASTERKEY_IBUTTON_SUPPORT
				ModeGotoiButtonWithoutFloatingID();
#else
				iButtonGotoFIDCheck();
#endif
			}
			else
			{
				//이미 등록한 것일 경우
				gbiButtonReadStatus = IBTNREAD_SAME_UID;
				iButtonGotoiButtonAwayCheck();
			}
			break;

		default:
			iButtonModeClear();
			break;
	}	
}



void iButtonModeFIDCheck(void)
{
	BYTE bTmp;
	
	switch(gbiButtonPrcsStep)
	{
		case IBTNPRCS_FID_READ:
			//iButton내 전체 Memory값을 Read (32 byte)
			bTmp = iButtonFIDReadAll();
			
			if(bTmp == 1)	
			{
				gbiButtonPrcsStep = IBTNPRCS_FID_MASTER_CHK1;
			}
			else if(bTmp == 2)	
			{
				iButtonGotoUIDCheck();
			}
			
			break;
		
		case IBTNPRCS_FID_MASTER_CHK1 :
			bTmp = iButtonMasterCheck(0);
			
			if(bTmp == OK) 
			{
			//마스터 키인지를 검사하여 마스터 키일 경우 저장
				gbiButtonPrcsStep = IBTNPRCS_MASTER_SAVE;
			}
			else
			{
				gbiButtonPrcsStep = IBTNPRCS_FID_MASTER_CHK2;
			}
			
			break;

		case IBTNPRCS_FID_MASTER_CHK2 :
			bTmp = iButtonMasterCheck(8);

			if(bTmp == OK) 
			{
			//마스터 키인지를 검사하여 마스터 키일 경우 저장
				gbiButtonPrcsStep = IBTNPRCS_MASTER_SAVE;
			}
			else
			{
				gbiButtonPrcsStep = IBTNPRCS_NORMAL_SAVE;
			}
			
			break;

		case IBTNPRCS_MASTER_SAVE:
			iButtonMasterSave();

			gbiButtonReadStatus = IBTNREAD_SUCCESS;
			iButtonGotoiButtonAwayCheck();
			break;
			
		case IBTNPRCS_NORMAL_SAVE:
			iButtonGeneralSave();

			gbiButtonReadStatus = IBTNREAD_SUCCESS;
			iButtonGotoiButtonAwayCheck();
			break;

		default:
			iButtonModeClear();
			break;
	}	
}



void iButtonModeUIDVerify(void)
{
	BYTE bTmp;
	
	switch(gbiButtonPrcsStep)
	{
		case IBTNPRCS_LOCKOUT_CHK:
			gwDetectionTimer2ms = 0;
			gbiButtonPrcsStep = IBTNPRCS_PRESENCE_CHECK;
			break;
			
		case IBTNPRCS_PRESENCE_CHECK:
			bTmp = iButtonCheck(); 
			if(bTmp == 1)
			{
				gbPhysicalKeyContact = 5;
				gbiButtonPrcsStep = IBTNPRCS_UID_READ;
			}	
			else if(bTmp == 2)
			{
				if(gbPhysicalKeyContact == 0)
				{
					gbiButtonReadStatus = IBTNREAD_NO_KEY;
					iButtonGotoiButtonModeClear();
				}
				else
				{
					if(gbPhysicalKeyContact)	
						gbPhysicalKeyContact--;
				}
			}
			break;
			
		case IBTNPRCS_UID_READ:
			bTmp = iButtonUIDRead();
			if(bTmp == 1)
			{
				gbiButtonPrcsStep = IBTNPRCS_UID_VERIFY;
			}
			else if(bTmp == 2)
			{						
				//DS1972 Type이 아닌 경우
				gbiButtonReadStatus = IBTNREAD_WRONG_KEY;
				iButtonGotoiButtonAwayCheck();
			}
			else if(bTmp == 3)
			{
				iButtonGotoUIDVerify();
			}
			break;

		case IBTNPRCS_UID_VERIFY:
			bTmp = iButtonUIDVerify();					
			if(bTmp == 1)
			{
			//Normal Key
				iButtonGotoFIDVerify();
			}
			else if(bTmp == 2)
			{
			//Not registered key
				gbiButtonReadStatus = IBTNREAD_FAIL;
				iButtonGotoiButtonAwayCheck();
			}
			else if(bTmp == 3)
			{
			//Master Key
				gbiButtonReadStatus = IBTNREAD_SUCCESS;
				iButtonGotoiButtonAwayCheck();
			}
			break;

		default:
			iButtonModeClear();
			break;
	}	
}



void iButtonModeFIDVerify(void)
{
	BYTE bTmp;
	
	switch(gbiButtonPrcsStep)
	{
		case IBTNPRCS_ROLLCODE_READ:
			iButtonRollCodeRead();
			gbiButtonPrcsStep = IBTNPRCS_FID_READ;
			break;

		case IBTNPRCS_FID_READ:
			bTmp = iButtonFIDRead();
			if(bTmp == 1)
			{
				gbiButtonPrcsStep = IBTNPRCS_FID_VERIFY;
			}
			else
			{
				iButtonGotoUIDVerify();
			}
			break;

		case IBTNPRCS_FID_VERIFY :
			bTmp = iButtonFIDVerify(); 
			if(bTmp == 1)
			{
				iButtonGotoFIDWrite1();
			}
			else if(bTmp == 2)
			{
				iButtonGotoFIDWrite2();
			}
			else 
			{
			//Registered UID, but not matched FID
				gbiButtonReadStatus = IBTNREAD_FID_LOST;
				iButtonGotoiButtonAwayCheck();
			}
			break;
			
		default:
			iButtonModeClear();
			break;
	}
}



void iButtonModeFIDWrite(void)
{
	BYTE bTmp;
	BYTE bAdrs;
//	BYTE bBuf[10];
	
	switch(gbiButtonPrcsStep)
	{
		case IBTNPRCS_FID_WRITE1 :
			gtbNansu[3] = (gtbNansu[3] & 0x0F) | gbPrcsiButtonTmp;
			bAdrs = (gbPrcsiButtonTmp >> 4) & 0x03;
			bAdrs = bAdrs*16+8;

			bTmp = iButtonNewFIDWrite(bAdrs);		
			if(bTmp == 1)
			{
				gbiButtonPrcsStep = IBTNPRCS_FID_CONFIRM1;
			}
			else if(bTmp == 2)
			{
				iButtonGotoUIDVerify();
			}
			break;

		case IBTNPRCS_FID_CONFIRM1 :
			bAdrs = (gbPrcsiButtonTmp >> 4) & 0x03;
			bAdrs = bAdrs*16+8;

			bTmp = iButtonNewFIDConfirm(bAdrs);				
			if(bTmp == 1)
			{
/*
bBuf[0] = gbiButtonBuf[0];
bBuf[1] = gbiButtonBuf[1];
bBuf[2] = gbiButtonBuf[2];
bBuf[3] = gbiButtonBuf[3];
bBuf[4] = 2;

PrintDebugData(bBuf, 5, DEBUG_TX_DATA);
*/				

				gbiButtonPrcsStep = IBTNPRCS_FID_SAVE;
			}
			else 
			{
				iButtonGotoUIDVerify();
			}
			break;

		case IBTNPRCS_FID_WRITE2 :
			gtbNansu[3] = (gtbNansu[3] & 0x0F) | gbPrcsiButtonTmp;
			bAdrs = (gbPrcsiButtonTmp >> 4) & 0x03;
			bAdrs = bAdrs*16;

			bTmp = iButtonNewFIDWrite(bAdrs);			
			if(bTmp == 1)
			{
				gbiButtonPrcsStep = IBTNPRCS_FID_CONFIRM2;
			}
			else if(bTmp == 2)
			{
				iButtonGotoUIDVerify();
			}
			break;

		case IBTNPRCS_FID_CONFIRM2 :
			bAdrs = (gbPrcsiButtonTmp >> 4) & 0x03;
			bAdrs = bAdrs*16;

			bTmp = iButtonNewFIDConfirm(bAdrs); 			
			if(bTmp == 1)
			{
/*
bBuf[0] = gbiButtonBuf[0];
bBuf[1] = gbiButtonBuf[1];
bBuf[2] = gbiButtonBuf[2];
bBuf[3] = gbiButtonBuf[3];
bBuf[4] = 1;

PrintDebugData(bBuf, 5, DEBUG_TX_DATA);
*/
				gbiButtonPrcsStep = IBTNPRCS_FID_SAVE;
			}
			else 
			{
				iButtonGotoUIDVerify();
			}
			break;

		case IBTNPRCS_FID_SAVE :
			iButtonNewFIDSave();	

			gbiButtonReadStatus = IBTNREAD_SUCCESS;
			iButtonGotoiButtonAwayCheck();
			break;

		default:
			iButtonModeClear();
			break;
	}
}



void iButtonModeiButtonAwayCheck(void)
{
	BYTE bTmp;

	switch(gbiButtonPrcsStep)
	{
		case IBTNPRCS_AWAY_CHECK:
			bTmp = iButtonCheck(); 
			if(bTmp == 1)
			{
				gbPhysicalKeyContact = 5;
			}	
			else if(bTmp == 2)
			{
				if(gbPhysicalKeyContact == 0)
				{
					gbiButtonReadStatus = IBTNREAD_NO_KEY;
					iButtonGotoiButtonModeClear();
				}
				else
				{
					if(gbPhysicalKeyContact)	gbPhysicalKeyContact--;
				}
			}
			break;

		case IBTNPRCS_CONTACT_OFF:
		default:
			iButtonModeClear();
			break;
	}
}

BYTE GetDS1972ModeProcess(void)
{
	return (gbiButtonMode);
}

void DS1972TouchKeyDetectionProcess(void)
{
	BYTE bTmp;

	if(GetDS1972ModeProcess() || GetBuzPrcsStep() || GetVoicePrcsStep())		
	{
		return;
	}
		
	bTmp = iButtonCheck(); 
	if(bTmp == 1)
	{	
		ModeGotoDS1972TouchKeyVerify();
		gbiButtonReadStatus = 0;
		iButtonGotoUIDVerify();
	}
}


void(*const iButtonModeProcess_Tbl[])(void) = {
	iButtonModeIdleState,
	iButtonModeUIDCheck,
	iButtonModeFIDCheck,

	iButtonModeUIDVerify,
	iButtonModeFIDVerify,
	iButtonModeFIDWrite,
	iButtonModeiButtonAwayCheck,
};


void iButtonModeProcess(void)
{
	iButtonModeProcess_Tbl[gbiButtonMode]();
}

