#ifndef __IBUTTONPROCESS_INCLUDED        
#define __IBUTTONPROCESS_INCLUDED   


#include "DefineMacro.h"
//#include "DefinePin.h"
 
//#define	_IB_INT(A)		if(A) PMK7 = 0;else PMK7 = 1			//INTP7
//#define	_IB_PND_BIT	PIF7									//INTP7
//#define	_IB_PNDC		PIF7 = 0								//INTP7

//#define _IBUTTON_DATA_MODE(A)	if(A) SET_BIT(PM14, 0x02);else CLR_BIT(PM14, 0x02)


extern FLAG iButtonFlag;
//#define	gfiButton1972		iButtonFlag.STATEFLAG.Bit0					//iButton check 금지
//#define	gfiBtnReadErr		iButtonFlag.STATEFLAG.Bit1
//#define	gfiButChkSTOP	iButtonFlag.STATEFLAG.Bit2					//DS1972 iButton일 경우 Set
//#define 	gfiButtonIn		iButtonFlag.STATEFLAG.Bit3


#define 	STATUS_SUCCESS                  	(0x0000)	
#define	STATUS_FAIL				(0x0001)
#define	STATUS_WRONG_KEY			(0x0002)
#define	STATUS_MKS				(0x0003)


#define	IBTNREAD_SUCCESS				1
#define	IBTNREAD_NO_KEY					2
#define	IBTNREAD_FAIL					3
#define	IBTNREAD_WRONG_KEY				4
#define	IBTNREAD_FID_LOST				5
#define	IBTNREAD_INNER_FORCED_LOCK		6
#define	IBTNREAD_SAME_UID				7

#define	MAX_KEY_UID_SIZE		8
#define	MAX_KEY_FV_SIZE		4
#define	MAX_KEY_RC_SIZE		1

extern UNKEYUIDBUF KeyBuffer;
#define	gbTRFUIDBuf			KeyBuffer.BCARDBUF				//읽어들인 UID를 저장하는 버퍼
#define	gbiButtonBuf			KeyBuffer.STIBUTTON.BIBUTTONBUF
#define	gbiButtonMemTmp	KeyBuffer.STIBUTTON.BIBUTTONTMP

extern BYTE gbPrcsiButtonTmp;							//RollCode를 저장을 위한 임시 변수
extern BYTE gbTimeiButtonCnt;
extern BYTE gbLonOnIDForiButton[8];
extern WORD gwDetectionTimer2ms;
extern WORD gwIbuttonVerifyDetectionTimer2ms;

extern BYTE gbPhysicalKeyContact;
//extern BYTE gbDS1972Flag;

extern BYTE gbDS1972WriteBuf1[160];
extern BYTE gbDS1972WriteBuf2[80];
extern BYTE gbDS1972WriteBuf3[20];
extern BYTE gbDS1972ReadBuf[64];

//extern BYTE gbTRFUIDBuf[75];	
//extern BYTE gbiButtonBuf[75];
//extern BYTE gbiButtonMemTmp[10];


extern const BYTE gcbiButtonCRC[256];

extern BYTE gbiButtonMainProcessMode;
enum{
	IBTNMAIN_VERIFY_MODE = 0,
	IBTNMAIN_REGISTER_MODE,
};	

extern BYTE gbiButtonMode; 
enum{
	IBTNMODE_UID_CHECK = 1,
	IBTNMODE_FID_CHECK,
	IBTNMODE_UID_VERIFY,
	IBTNMODE_FID_VERIFY,
	IBTNMODE_FID_WRITE,
	IBTNMODE_IBUTTON_AWAY_CHK,
};

extern BYTE gbiButtonPrcsStep;
enum
{
	IBTNPRCS_IDLE_STATE = 0,

	IBTNPRCS_INPUT_CHECK,
	IBTNPRCS_LOCKOUT_CHK,
	IBTNPRCS_PRESENCE_CHECK,
	IBTNPRCS_UID_READ,
	IBTNPRCS_UID_CHECK,
	IBTNPRCS_UID_VERIFY,

	IBTNPRCS_ROLLCODE_READ,
	IBTNPRCS_FID_READ,
	IBTNPRCS_FID_MASTER_CHK1,
	IBTNPRCS_FID_MASTER_CHK2,
	IBTNPRCS_MASTER_SAVE,
	IBTNPRCS_NORMAL_SAVE,

	IBTNPRCS_FID_VERIFY,
	IBTNPRCS_FID_WRITE1,
	IBTNPRCS_FID_CONFIRM1,
	IBTNPRCS_FID_WRITE2,
	IBTNPRCS_FID_CONFIRM2,
	IBTNPRCS_FID_SAVE,

	IBTNPRCS_AWAY_CHECK,
	IBTNPRCS_CONTACT_OFF
};


extern BYTE gbiButtonReadStatus;


BYTE DS1972TouchKeyInputCheck(void);

void iButtonEnable(void);
void iButtonDisable(void);

void SetupiButton(void);

void iButtonBufInitial(void);
void iButtonModeClear(void);

void iButtonGotoRegModeStart(void);
void iButtonGotoUIDCheck(void);

void iButtonGotoVeriModeStart(void);
void iButtonGotoReadStop(void);
BYTE GetDS1972ModeProcess(void);
void DS1972TouchKeyDetectionProcess(void);


BYTE iButtonModeStatusCheck(void);

void iButtonModeProcess(void);
void iButtonRegOperation(void);


#endif

