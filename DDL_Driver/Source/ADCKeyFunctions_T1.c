//------------------------------------------------------------------------------
/** 	@file		ADCKeyFunctions_T1.c
	@version 0.1.00
	@date	2016.04.25
	@brief	Ten Key Check Functions using ADC
	@remark  ADC를 이용한 Ten Key 입력 처리 함수
	@see 	KeyInput.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.25		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gAdcKeyCheckCnt = 0;


#define	ADC_KEY_MIN_LEVEL_1		3042			// 3302
#define	ADC_KEY_MIN_LEVEL_2		2529			// 2786
#define	ADC_KEY_MIN_LEVEL_3		2021			// 2249
#define	ADC_KEY_MIN_LEVEL_4		1529			// 1781
#define	ADC_KEY_MIN_LEVEL_5		1016			// 1280
#define	ADC_KEY_MIN_LEVEL_6		660				//  772


void ADKeyCheckRtn(void)
{
	BYTE bKeyTmp;
	BYTE bKeyAd0;
	BYTE bKeyAd1;
	WORD wAdVal;

#ifdef	DDL_CFG_PUSHKEY_TYPE1
	//-- AD 10Key 동작
	wAdVal = ADCCheck( ADC_KEY_2_CHECK );

	if(wAdVal > ADC_KEY_MIN_LEVEL_1)			bKeyAd0	= TENKEY_7;			
	else if(wAdVal > ADC_KEY_MIN_LEVEL_2)		bKeyAd0	= TENKEY_8;			
	else if(wAdVal > ADC_KEY_MIN_LEVEL_3)		bKeyAd0	= TENKEY_9;			
	else if(wAdVal > ADC_KEY_MIN_LEVEL_4)		bKeyAd0	= TENKEY_NONE;		
	else if(wAdVal > ADC_KEY_MIN_LEVEL_5)		bKeyAd0	= TENKEY_0;			
	else if(wAdVal > ADC_KEY_MIN_LEVEL_6)		bKeyAd0	= TENKEY_SHARP;		
	else										bKeyAd0	= TENKEY_NONE;

	wAdVal = ADCCheck( ADC_KEY_1_CHECK );

	if(wAdVal > ADC_KEY_MIN_LEVEL_1)			bKeyAd1	= TENKEY_1;			
	else if(wAdVal > ADC_KEY_MIN_LEVEL_2)		bKeyAd1	= TENKEY_2;			
	else if(wAdVal > ADC_KEY_MIN_LEVEL_3)		bKeyAd1	= TENKEY_3;			
	else if(wAdVal > ADC_KEY_MIN_LEVEL_4)		bKeyAd1	= TENKEY_4;			
	else if(wAdVal > ADC_KEY_MIN_LEVEL_5)		bKeyAd1	= TENKEY_5;			
	else if(wAdVal > ADC_KEY_MIN_LEVEL_6)		bKeyAd1	= TENKEY_6;
	else										bKeyAd1	= TENKEY_NONE;
#endif

	if((bKeyAd0 != TENKEY_NONE) && (bKeyAd1	!= TENKEY_NONE))
	{
		bKeyTmp	= TENKEY_ERROR;
	}
	else if((bKeyAd0 == TENKEY_NONE) && (bKeyAd1 == TENKEY_NONE))
	{
		bKeyTmp	= TENKEY_NONE;
	}
	else if(bKeyAd0 == TENKEY_NONE)
	{
		bKeyTmp	= bKeyAd1;						//Key 1 ~ 6
	}
	else
	{
		bKeyTmp	= bKeyAd0;						//Key 7 ~ #
	}

	if(gbOldKeyBuffer != bKeyTmp)
	{
//		PCErrorClear();

		gAdcKeyCheckCnt = 0;
		gbOldKeyBuffer = bKeyTmp;
	}

	gAdcKeyCheckCnt++;

	if(gAdcKeyCheckCnt >= 10)
	{						
		gbNewKeyBuffer = gbOldKeyBuffer;
		gAdcKeyCheckCnt = 0;
	}
}



