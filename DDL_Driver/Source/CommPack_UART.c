//------------------------------------------------------------------------------
/** 	@file	CommPack_UART.c
	@brief	UART functions for communication pack module
*/
//------------------------------------------------------------------------------

#define		_COMMPACK_UART_C_

#include	"main.h"


uint16_t	CommPack_Rx_Buff_head, CommPack_Rx_Buff_tail;
BYTE	commpack_rx_queue[COMMPACK_RX_BUFF_LENGTH];
BYTE	commpack_rx_byte;

BYTE	CommPack_Tx_Buff_Id=0;
BYTE	commpack_tx_buff[COMMPACK_NUM_TX_BUFF][COMMPACK_TX_BUFF_LENGTH];

//UART 이상시 재설도 하기 때문에 초기화 코드를 별도로 마련해 둔다.
// 대기 전류를 줄이기 위해 STOP mode가 될때 Tx/Rx pin을 analog로 만들기에 이를 다시 UART용으로 설정하는 코드이다.
void		SetupCommPack_UART( void )
{
#if defined (P_COM_DDL_EN_T)
	GPIO_InitTypeDef GPIO_InitStruct;

	// CommPack 모듈과의 통신핀 Tx pin을 gpio로 설정 사용한 뒤 다시 uart로 사용하려면 해당 pin의 설정에서 Alternate function 기능을 활성화 해야한다.
	GPIO_InitStruct.Pin = COM_DDL_TX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;				// Select Alternate function
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART1;			// Select Alternate function
	HAL_GPIO_Init(COM_DDL_TX_GPIO_Port, &GPIO_InitStruct);

	// CommPack 모듈과의 통신핀 Rx pin을 gpio로 설정 사용한 뒤 다시 uart로 사용하려면 해당 pin의 설정에서 Alternate function 기능을 활성화 해야한다.
	GPIO_InitStruct.Pin = COM_DDL_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;				// Select Alternate function
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART1;			// Select Alternate function
	HAL_GPIO_Init(COM_DDL_RX_GPIO_Port, &GPIO_InitStruct);

	gh_mcu_uart_com->Instance = MCU_COMMPACK_UART;
	gh_mcu_uart_com->Init.BaudRate = 19200;
	gh_mcu_uart_com->Init.WordLength = UART_WORDLENGTH_8B;
	gh_mcu_uart_com->Init.StopBits = UART_STOPBITS_1;
	gh_mcu_uart_com->Init.Parity = UART_PARITY_NONE;
	gh_mcu_uart_com->Init.Mode = UART_MODE_TX_RX;
	gh_mcu_uart_com->Init.HwFlowCtl = UART_HWCONTROL_NONE;
	gh_mcu_uart_com->Init.OverSampling = UART_OVERSAMPLING_16;
	HAL_UART_Init( gh_mcu_uart_com );
#endif 
}



// 대기 전류를 줄이기 위해 CommPack와의 통신 pin을 analog로 바꾼다.
void OffGpioCommPack( void )
{
#if defined (P_COM_DDL_EN_T)
	GPIO_InitTypeDef GPIO_InitStruct;

	HAL_UART_DeInit( gh_mcu_uart_com );

	GPIO_InitStruct.Pin = COM_DDL_TX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(COM_DDL_TX_GPIO_Port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = COM_DDL_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(COM_DDL_RX_GPIO_Port, &GPIO_InitStruct);
#endif
}



// UART callback 함수를 MX_uart_lapper에 등록한다.
void		RegisterCommPack_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) )
{
#if defined (P_COM_DDL_EN_T)
	if ( uart_rxcplt_callback != NULL )
		register_uart_rxcplt_callback( gh_mcu_uart_com, uart_rxcplt_callback );
	if ( uart_txcplt_callback != NULL )
		register_uart_txcplt_callback( gh_mcu_uart_com, uart_txcplt_callback );
#endif 	
}


void		CommPackRxInterruptRequest( void )
{
#if defined (P_COM_DDL_EN_T)
	HAL_UART_Receive_IT( gh_mcu_uart_com, &commpack_rx_byte, sizeof(commpack_rx_byte) );
#endif 
}



//-----------------------------------------------------------------------------


//Rx complete callback function for CommPack
void CommPack_uart_rxcplt_callback(UART_HandleTypeDef *huart)
{
#if defined (P_COM_DDL_EN_T)
	uint16_t	tmp;

//	__disable_irq();

	commpack_rx_queue[ CommPack_Rx_Buff_head ] = commpack_rx_byte;		// enqueue a received byte

	tmp = (CommPack_Rx_Buff_head + 1) % COMMPACK_RX_BUFF_LENGTH;
	if ( tmp != CommPack_Rx_Buff_tail ) {	//rx queue가 full 이 아니면, increase header
		CommPack_Rx_Buff_head++;
		CommPack_Rx_Buff_head %= COMMPACK_RX_BUFF_LENGTH;
	}

//	commpack_rx_queue[ CommPack_Rx_Buff_head ] = commpack_rx_byte;

//	__enable_irq();

	CommPackRxInterruptRequest();		// Trigger Rx Interrupt

	PackDataReceiveProcess();
#endif 
}


//Tx complete callback function for CommPack
void CommPack_uart_txcplt_callback(UART_HandleTypeDef *huart)
{
#if defined (P_COM_DDL_EN_T)
	PackDataTransmitProcess();
#endif 
}

//-----------------------------------------------------------------------------




// CommPack의 Rx Queue에 들어 있는 내용을 지정한 길이 만큼 Dequeue한다.
// Dequeue한 byte 수를 return 한다.
uint16_t	CommPack_DequeueRx( BYTE *buff, uint16_t length )
{
	uint16_t	ret = 0;
#if defined (P_COM_DDL_EN_T)


//	__disable_irq();

	while( (CommPack_Rx_Buff_tail != CommPack_Rx_Buff_head) && (length>0) ) {
		*buff = commpack_rx_queue[ CommPack_Rx_Buff_tail ];
		buff ++;
		length--;
		ret++;
		CommPack_Rx_Buff_tail++;
		CommPack_Rx_Buff_tail %= COMMPACK_RX_BUFF_LENGTH;
	}

//	__enable_irq();
#endif 
	return	ret;
}


// 전송할 byte 수를 return 한다.
uint16_t	UartSendCommPack( BYTE *data, uint16_t length )
{
#if defined (P_COM_DDL_EN_T)
	CommPack_Tx_Buff_Id = 0;
	memcpy( commpack_tx_buff[ CommPack_Tx_Buff_Id ], data, length );
	HAL_UART_Transmit_IT( gh_mcu_uart_com, commpack_tx_buff[ CommPack_Tx_Buff_Id ], length );
#endif 
	return	length;
}



