//------------------------------------------------------------------------------
/** 	@file	CommPack_UART.c
	@brief	UART functions for communication pack module
*/
//------------------------------------------------------------------------------

#define		_INNERPACK_UART_C_

#include	"main.h"

uint16_t	InnerPack_Rx_Buff_head, InnerPack_Rx_Buff_tail;
BYTE	innerpack_rx_queue[INNERPACK_RX_BUFF_LENGTH];
BYTE	innerpack_rx_byte;

BYTE	InnerPack_Tx_Buff_Id=0;
BYTE	innerpack_tx_buff[INNERPACK_NUM_TX_BUFF][INNERPACK_TX_BUFF_LENGTH];


//UART 이상시 재설도 하기 때문에 초기화 코드를 별도로 마련해 둔다.
// 대기 전류를 줄이기 위해 STOP mode가 될때 Tx/Rx pin을 analog로 만들기에 이를 다시 UART용으로 설정하는 코드이다.
void		SetupInnerPack_UART( void )
{
#if defined (P_BT_WAKEUP_T)
	GPIO_InitTypeDef GPIO_InitStruct;

	// CommPack 모듈과의 통신핀 Tx pin을 gpio로 설정 사용한 뒤 다시 uart로 사용하려면 해당 pin의 설정에서 Alternate function 기능을 활성화 해야한다.
	GPIO_InitStruct.Pin = BT_TX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;				// Select Alternate function
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART2;			// Select Alternate function
	HAL_GPIO_Init(BT_TX_GPIO_Port, &GPIO_InitStruct);

	// CommPack 모듈과의 통신핀 Rx pin을 gpio로 설정 사용한 뒤 다시 uart로 사용하려면 해당 pin의 설정에서 Alternate function 기능을 활성화 해야한다.
	GPIO_InitStruct.Pin = BT_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;				// Select Alternate function
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART2;			// Select Alternate function
	HAL_GPIO_Init(BT_RX_GPIO_Port, &GPIO_InitStruct);

	gh_mcu_uart_com2->Instance = MCU_INNERPACK_UART;
	gh_mcu_uart_com2->Init.BaudRate = 19200;
	gh_mcu_uart_com2->Init.WordLength = UART_WORDLENGTH_8B;
	gh_mcu_uart_com2->Init.StopBits = UART_STOPBITS_1;
	gh_mcu_uart_com2->Init.Parity = UART_PARITY_NONE;
	gh_mcu_uart_com2->Init.Mode = UART_MODE_TX_RX;
	gh_mcu_uart_com2->Init.HwFlowCtl = UART_HWCONTROL_NONE;
	gh_mcu_uart_com2->Init.OverSampling = UART_OVERSAMPLING_16;
	HAL_UART_Init( gh_mcu_uart_com2 );
#endif 
}



// 대기 전류를 줄이기 위해 CommPack와의 통신 pin을 analog로 바꾼다.
void OffGpioInnerPack( void )
{
#if defined (P_BT_WAKEUP_T)
	GPIO_InitTypeDef GPIO_InitStruct;

	HAL_UART_DeInit( gh_mcu_uart_com2 );

	GPIO_InitStruct.Pin = BT_TX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(BT_TX_GPIO_Port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = BT_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(BT_RX_GPIO_Port, &GPIO_InitStruct);
#endif 	
}



// UART callback 함수를 MX_uart_lapper에 등록한다.
void		RegisterInnerPack_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) )
{
#if defined (P_BT_WAKEUP_T)
	if ( uart_rxcplt_callback != NULL )
		register_uart_rxcplt_callback( gh_mcu_uart_com2, uart_rxcplt_callback );
	if ( uart_txcplt_callback != NULL )
		register_uart_txcplt_callback( gh_mcu_uart_com2, uart_txcplt_callback );
#endif 	
}


void		InnerPackRxInterruptRequest( void )
{
#if defined (P_BT_WAKEUP_T)
	HAL_UART_Receive_IT( gh_mcu_uart_com2, &innerpack_rx_byte, sizeof(innerpack_rx_byte) );
#endif 
}



//-----------------------------------------------------------------------------


//Rx complete callback function for CommPack
void InnerPack_uart_rxcplt_callback(UART_HandleTypeDef *huart)
{
#if defined (P_BT_WAKEUP_T)
	uint16_t	tmp;

//	__disable_irq();

	innerpack_rx_queue[ InnerPack_Rx_Buff_head ] = innerpack_rx_byte;		// enqueue a received byte

	tmp = (InnerPack_Rx_Buff_head + 1) % INNERPACK_RX_BUFF_LENGTH;
	if ( tmp != InnerPack_Rx_Buff_tail ) {	//rx queue가 full 이 아니면, increase header
		InnerPack_Rx_Buff_head++;
		InnerPack_Rx_Buff_head %= INNERPACK_RX_BUFF_LENGTH;
	}

//	innerpack_rx_queue[ InnerPack_Rx_Buff_head ] = innerpack_rx_byte;

//	__enable_irq();

	InnerPackRxInterruptRequest();		// Trigger Rx Interrupt

	InnerPackDataReceiveProcess();
#endif 	
}


//Tx complete callback function for CommPack
void InnerPack_uart_txcplt_callback(UART_HandleTypeDef *huart)
{
#if defined (P_BT_WAKEUP_T)
	InnerPackDataTransmitProcess();
#endif 
}

//-----------------------------------------------------------------------------




// CommPack의 Rx Queue에 들어 있는 내용을 지정한 길이 만큼 Dequeue한다.
// Dequeue한 byte 수를 return 한다.
uint16_t	InnerPack_DequeueRx( BYTE *buff, uint16_t length )
{
	uint16_t	ret = 0;

#if defined (P_BT_WAKEUP_T)
//	__disable_irq();

	while( (InnerPack_Rx_Buff_tail != InnerPack_Rx_Buff_head) && (length>0) ) {
		*buff = innerpack_rx_queue[ InnerPack_Rx_Buff_tail ];
		buff ++;
		length--;
		ret++;
		InnerPack_Rx_Buff_tail++;
		InnerPack_Rx_Buff_tail %= INNERPACK_RX_BUFF_LENGTH;
	}

//	__enable_irq();
#endif 

	return	ret;
}



// 전송할 byte 수를 return 한다.
uint16_t	UartSendInnerPack( BYTE *data, uint16_t length )
{
#if defined (P_BT_WAKEUP_T)
//	if ( length > INNERPACK_TX_BUFF_LENGTH )
//		length = INNERPACK_TX_BUFF_LENGTH;

	InnerPack_Tx_Buff_Id = 0;
	memcpy( innerpack_tx_buff[ InnerPack_Tx_Buff_Id ], data, length );
	HAL_UART_Transmit_IT( gh_mcu_uart_com2, innerpack_tx_buff[ InnerPack_Tx_Buff_Id ], length );

//	InnerPack_Tx_Buff_Id ++;
//	InnerPack_Tx_Buff_Id %= INNERPACK_NUM_TX_BUFF;

#endif 
	return	length;
}


