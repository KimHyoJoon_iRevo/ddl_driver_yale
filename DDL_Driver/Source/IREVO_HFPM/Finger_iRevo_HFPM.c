//------------------------------------------------------------------------------
/** 	@file		Finger_iRevo_HFPM.c
	@brief	uart 통신 protocl 관련 파일 
*/
//------------------------------------------------------------------------------


#define		_FINGER_IREVO_HFPM_C_

#include	"main.h"

FLAG BioFlag;

/* gbBioTimer10ms 다른 지문 모듈과 같은 방식으로 gbBioTimer10ms 제어 하지 말것 */
WORD gbBioTimer10ms=0;

static BYTE gbFingerPrint_txd_end;//사용한 일이 없지만 서도... 
static BYTE FingerPrint_txding_buf[40];

BYTE gbFingerPrint_rxd_end;
static BYTE gbFingerPrint_rxding_buf[40] ;
BYTE gbFingerPrint_rxd_buf[40];

static BYTE gbfingerdatacounter;

BYTE gbCoverAccessDelaytime100ms = 0x00;
BYTE gbFingerLongTimeWait100ms = 0;
BYTE gbFID=0;
BYTE gbFingerRegisterDisplayStep;
BYTE dbDisplayOn = 0x00;

static BYTE bRandomVerificationCode[16]; 

static BYTE gbHFPMRxAddDataLen = 0;
static BYTE gbHFPMRxChecksum = 0;
static WORD gbHFPMRxByteCnt = 0;

static uint8_t		glhfpm_rx_byte;
//static uint8_t		glhfpm_tx_byte; buffer 로 다 처리 하니 삭제 

// 지역 변수로 쓰기에stack 감당하기 어려워 전역 변수로 
BYTE gbHFPMVerifyArray[32];
/* 외부에서 쓰지 말것 */
static BYTE bFPMTempArray[32];
static BYTE bHFPMTempArrayKey[16];
static BYTE bHFPMTempArrayID[16];

static BYTE bHFPMVerifyStepCounter; //step 별로 하나씩 증가 시켜서 4가 되야 문을 열수 있게 

BYTE gbHFPMRxDataProcessStep = 0;

#ifdef	BLE_N_SUPPORT
BYTE gbEnterMode = 0x00;
#endif 

#define HFPM_WAIT_RX 0
#define HFPM_RX_COMPLETE 1
#define HFPM_SEND_VERIFICATION_CODE 2
#define HFPM_SEND_SESSION_CLOSE 3


#define HFPM_MODULE_INIT 50
#define HFPM_RX_ERROR_STATE 100


//#define likely(A)		__builtin_expect((A),1)
//#define unlikely(A)	__builtin_expect((A),0)


// UART callback 함수를 MX_uart_lapper에 등록한다.
void		Register_HFPM_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) )
{
	if ( uart_rxcplt_callback != NULL )
		register_uart_rxcplt_callback( gh_mcu_uart_finger, uart_rxcplt_callback );
	if ( uart_txcplt_callback != NULL )
		register_uart_txcplt_callback( gh_mcu_uart_finger, uart_txcplt_callback );
}

void HFPMRepairUart(void)
{
	HAL_UART_DeInit(gh_mcu_uart_finger);
	Delay(SYS_TIMER_10MS);
	gh_mcu_uart_finger->Instance = USART3;		
	gh_mcu_uart_finger->Init.BaudRate = 115200;
	gh_mcu_uart_finger->Init.WordLength = UART_WORDLENGTH_8B;
	gh_mcu_uart_finger->Init.StopBits = UART_STOPBITS_1;
	gh_mcu_uart_finger->Init.Parity = UART_PARITY_NONE;
	gh_mcu_uart_finger->Init.Mode = UART_MODE_TX_RX;
	gh_mcu_uart_finger->Init.HwFlowCtl = UART_HWCONTROL_NONE;
	gh_mcu_uart_finger->Init.OverSampling = UART_OVERSAMPLING_16;	
	HAL_UART_Init( gh_mcu_uart_finger );
	InitialSetupFingerModule();
}


void HFPMRxDataClear(void)
{
	gbHFPMRxByteCnt = 0;
	gbHFPMRxAddDataLen = 0;
	gbHFPMRxChecksum = 0;
}

void HFPMRxDataStepChange(BYTE Step)
{
	if(Step == HFPM_WAIT_RX)
	{
		gbWakeUpMinTime10ms = 10;
	}
	gbHFPMRxDataProcessStep = Step;
}

BYTE GetHFPMRxDataStep(void)
{
	return gbHFPMRxDataProcessStep;
}

void HFPMRxDataProcess(void)
{
	BYTE bTemp = 0x00;

	switch(gbHFPMRxDataProcessStep)
	{
		case HFPM_WAIT_RX :
		{
			; /* 아무것도 안함 */
		}
		break;		

		/* Rx */

		case HFPM_MODULE_INIT :
		{
			memcpy(gbFingerPrint_rxd_buf,gbFingerPrint_rxding_buf,gbHFPMRxByteCnt);
			gbFingerPrint_rxd_end = 0x01;
			HFPMRxDataClear();
			HFPMRxDataStepChange(HFPM_WAIT_RX);
		}
		break;
		
		case HFPM_RX_COMPLETE :
		{
			if(GetTamperProofPrcsStep() ||gfBrokenAlarm || GetMainMode() == MODE_ABNORMAL_PROCESS)
			{
				HFPMRxDataStepChange(HFPM_WAIT_RX);
				HFPMRxDataClear();
				gbFingerPrint_rxd_end = 0x01;
				break;
			}

			/* packet 의 CRC 까지 오류 없음 */			
			memcpy(gbFingerPrint_rxd_buf,gbFingerPrint_rxding_buf,gbHFPMRxByteCnt);
			gbFingerPrint_rxd_end = 0x01;
			HFPMRxDataClear();

			switch (gbFingerPrint_rxd_buf[4])
			{
				case IREVO_HFPM_SUB_EVENT_REQUEST_AUTHENTICATION : /*  step 1  */
				{
					/* 받은 data 를 비교 해서 처리 */
					/* Make verification data */
					/* Ak 생성 방법 */
					/* step 1  module 로 부터 IREVO_HFPM_SUB_EVENT_REQUEST_AUTHENTICATION 받은후 */
					/* step 2  lock 은 HFPMTxEventVerificationCode() 함수를 통해 16byte 의 Rkey 를 생성 이를 현재 key로 AES 후 모듈에 전송*/
					/* step 3  모듈에서 내가 보낸 랜덤 Rk 를 AES 로 복호후 */
					/* step 3' 모듈은 Rk 를 다시 ~pkey ^ Module deviceID 로 만든 pk' 로 AES 한 Rk' lock 에게 보낸다 */
					/* step 4  lock 은 받은 Rk' 를 내가 가진 ~pkey ^ Module deviceID 로 AES 128 를 한다 */
					/* step 4'  이렇게 최종 적으로 나온 값이 내가 보낸 랜덤 key 와 동일 하면 authentication 성공 */
					/* step 4''  authentication 완료 되면 HFPMTxEventStartTerminateSession start 한다 */
					/* step 5  HFPMTxEventStartTerminateSession start 의 ack 가 정상이면 최종적으로 32byte sub data 가 온다*/
					
					if(gbFingerPrint_rxd_buf[5] == 0x01 && gbFingerPrint_rxd_buf[6] == 0x00)
					{
						bHFPMVerifyStepCounter++;
						HFPMTxEventRequestAuthAck(0x00);
						bTemp = HFPM_SEND_VERIFICATION_CODE;
					}
					else 
					{
						HFPMTxEventRequestAuthAck(0x01);						
						bTemp = HFPM_RX_ERROR_STATE;
					}
				}
				break;

				case IREVO_HFPM_SUB_EVENT_VERIFICATION_CODE : /*  step 4  */
				{
					RomRead(bHFPMTempArrayKey,ENCRYPT_KEY,sizeof(bHFPMTempArrayKey));
					RomRead(bHFPMTempArrayID,HFPM_MODULE_ID,sizeof(bHFPMTempArrayID));					

#ifdef _USE_IREVO_CRYPTO_
					EncryptDecryptKey(bHFPMTempArrayKey,sizeof(bHFPMTempArrayKey));
					EncryptDecryptKey(bHFPMTempArrayID,sizeof(bHFPMTempArrayID));
#endif 					

					for(BYTE i = 0 ; i < 16 ; i++)
					{
						bHFPMTempArrayKey[i] = (~bHFPMTempArrayKey[i]) ^ bHFPMTempArrayID[i];
					}

#ifdef _USE_STM32_CRYPTO_LIB_
					DataDecrypt(&gbFingerPrint_rxd_buf[5],16,bHFPMTempArrayKey,bFPMTempArray);
#endif 

					if(memcmp(bRandomVerificationCode,bFPMTempArray,16) == 0x00)
					{
						/* start session */
						bHFPMVerifyStepCounter++;
						HFPMTxEventStartTerminateSession(0x01);
						bTemp = HFPM_WAIT_RX;
					}
					else
					{
						bTemp = HFPM_RX_ERROR_STATE;
					}
				}
				break;

				case IREVO_HFPM_SUB_EVENT_START_TERMINATE_AUTH_SESSION :
				{
					if(gbFingerPrint_rxd_buf[5] == 0x01 && gbFingerPrint_rxd_buf[6] == 0x00)
					{
						/* start session ack ok */
						bHFPMVerifyStepCounter++;
						bTemp = HFPM_WAIT_RX;
					}
					else if(gbFingerPrint_rxd_buf[5] == 0x02 && gbFingerPrint_rxd_buf[6] == 0x00)
					{
						/* terminate session ack ok */
						bHFPMVerifyStepCounter = 0;
						bTemp = HFPM_WAIT_RX;
					}
					else 
					{
						bTemp = HFPM_RX_ERROR_STATE;
					}
				}
				break;

				case IREVO_HFPM_SUB_EVENT_ENCRYPTED_DATA : /* step 5  */
				{
					
					RomRead(bHFPMTempArrayKey,ENCRYPT_KEY,16);
#ifdef _USE_IREVO_CRYPTO_
					EncryptDecryptKey(bHFPMTempArrayKey,16);
#endif 					
#ifdef _USE_STM32_CRYPTO_LIB_
					DataDecrypt(&gbFingerPrint_rxd_buf[5],32,bHFPMTempArrayKey,bFPMTempArray);
#endif 
					
					if(HFPMVerifyCommandParser(bFPMTempArray) != 0xFF && bHFPMVerifyStepCounter == 4)
					{
						bHFPMVerifyStepCounter = 0;
						memcpy(gbHFPMVerifyArray,bFPMTempArray,32);						
						HFPMTxEventEncryptDataAck(0x00);
						FPCoverSwProcess();
						bTemp = HFPM_SEND_SESSION_CLOSE; /* session stop 시키러 */
					}
					else 
					{
						HFPMTxEventEncryptDataAck(0x01);						
						bTemp = HFPM_RX_ERROR_STATE;
					}
				}
				break;
				
				default:
					/* verify 관련 암호화 부분 제외 나머진 각 main mode 에서 처리 */
					/* 즉 위 command 를 제외 하면 바로 RX 대기 상태로 */
					bHFPMVerifyStepCounter = 0;
					bTemp = HFPM_WAIT_RX;
				break;
			}
			HFPMRxDataStepChange(bTemp);
		}
		break;

		/* Tx */
		case HFPM_SEND_VERIFICATION_CODE : /*  step 2  */
		{
			/* step 2 step1의 정상 ack 가 나간후 tx 끝날때 까지대기 후 verification data send */
			
			if(gbFingerPrint_txd_end) 
			{
				bHFPMVerifyStepCounter++;
			HFPMTxEventVerificationCode();
			HFPMRxDataStepChange(HFPM_WAIT_RX);
		}
		}
		break;		

		case HFPM_SEND_SESSION_CLOSE :
		{
			/* 0x65 ack 이후 session close 를 보낸다  */
			if(gbFingerPrint_txd_end) 
			{
				HFPMTxEventStartTerminateSession(0x02);
				HFPMRxDataStepChange(HFPM_WAIT_RX);
			}
		}
		break;		


		/* Error state */
		case HFPM_RX_ERROR_STATE:
		{
			/* 뭐든 뭔가 잘못 되었을 때 */
			HFPMRxDataClear();
			TamperCountIncrease();
			FeedbackError(VOICE_MIDI_ERROR,VOL_CHECK);
			gbFingerPrint_rxd_end = 0x01;
			bHFPMVerifyStepCounter = 0;
			gbLedDisplayFPMOperation = 0x00;
			
			if(gbJigTimer1s){
				memset(gbJigInputDataBuf, 0xFF, 9);
				gbJigInputDataBuf[0] = 0x00;
				gbJigInputDataBuf[1] = 0x01;
				gbJigInputDataBuf[2] = 0x01;				
			}
			HFPMRxDataStepChange(HFPM_WAIT_RX);
		}
		break;		
		

		default:
			bHFPMVerifyStepCounter = 0;
			HFPMRxDataStepChange(HFPM_WAIT_RX);
		break;

	}
}

void HFPMTiTwuControl(BYTE bTiTwuControl)
{
	/* DDL -> Module 을 wake up 할 경우 active high 로 동작 */
	/* Module -> DDL 을 wake up 할 경우 active low 로 동작Pack 동작과 동일 */	
	/* lock 이 module 을 깨울 시에만 TI twu control 한다 */
	if(bTiTwuControl)
	{
		/* Enroll 의 경우 시간이 더 필요함 */
		P_FP_EN(1);
		Delay(SYS_TIMER_20MS+SYS_TIMER_5MS); // TI 20ms 이상 
		P_FP_EN(0);
		Delay(SYS_TIMER_100MS+SYS_TIMER_30MS+SYS_TIMER_5MS); // 130ms < Twu < 300ms 
	}
}

void HFPM_uart_rxcplt_callback(UART_HandleTypeDef *huart)
{
	gbFingerPrint_rxding_buf[gbHFPMRxByteCnt] = glhfpm_rx_byte;
	if(gbHFPMRxByteCnt >= (gbHFPMRxAddDataLen + 4))
	{
		gbHFPMRxByteCnt++;

		if(gbFingerPrint_rxding_buf[4 + gbHFPMRxAddDataLen] == gbHFPMRxChecksum)	// 체크섬이 맞으면 
		{
			if(gbFingerPrint_rxding_buf[0] == 0x44)
			{
				/* 0x44 의 경우 바로 처리 */
				HFPMRxDataStepChange(HFPM_MODULE_INIT);
				HFPMRxDataProcess();
			}
			else
			{
				HFPMRxDataStepChange(HFPM_RX_COMPLETE);
			}
		}
		else 
		{
			HFPMRxDataClear();
			//HFPMRxDataStepChange(HFPM_RX_ERROR_STATE);
		}
	}
	else
	{	
		gbFingerPrint_rxd_end = 0x00;
		gbHFPMRxChecksum ^= gbFingerPrint_rxding_buf[gbHFPMRxByteCnt];
		gbHFPMRxByteCnt++;
		if(gbHFPMRxByteCnt == (PACK_F_LENGTH_ADDDATA+1))
		{
			gbHFPMRxAddDataLen = gbFingerPrint_rxding_buf[PACK_F_LENGTH_ADDDATA];
		}
	}
	HAL_UART_Receive_IT( gh_mcu_uart_finger, &glhfpm_rx_byte, sizeof(glhfpm_rx_byte));
}

void HFPM_uart_txcplt_callback(UART_HandleTypeDef *huart)
{
	gbBioTimer10ms = 0;
	gbFingerPrint_txd_end = 1;
}

void		InitialSetupFingerModule( void )
{
	Register_HFPM_INT_Callback( HFPM_uart_txcplt_callback, HFPM_uart_rxcplt_callback);
	HAL_UART_Receive_IT( gh_mcu_uart_finger, &glhfpm_rx_byte, sizeof(glhfpm_rx_byte) );
}

//이전 코드에선 void FingerStatusLoad(void) 함수로 EEPROM에 저장된 지문의 수를 읽어 들인다.
BYTE GetNoFingerFromEeprom( void )
{
	BYTE	no_eeprom_key;

	RomRead(&no_eeprom_key, (WORD)KEY_NUM, 1);					// 현재 등록된 지문수 불러오기..
	return	no_eeprom_key;
}

void FingerPrintModuleCheck(void)
{
	BYTE i = 0x00;
	BYTE bytesum = 0x1A;

	// Ti Twu control
	P_FP_EN(0);
	Delay(SYS_TIMER_15MS);
	HFPMTiTwuControl(0x01);

	HFPMRxDataStepChange(HFPM_WAIT_RX);
	HFPMRxDataClear();

	// Make 0x44 command 
	FingerPrint_txding_buf[PACK_F_EVENT] = 0x44;		//event
	FingerPrint_txding_buf[PACK_F_SOURCE] = 0xB0;		//source
	FingerPrint_txding_buf[PACK_F_COUNT] = gbfingerdatacounter++;		//Counter
	FingerPrint_txding_buf[PACK_F_LENGTH_ADDDATA] = bytesum;	
	
	FingerPrint_txding_buf[PACK_F_LOCKTYPE] = 0x70;		//Lock Type High BYTE(None RF module)
	FingerPrint_txding_buf[PACK_F_LOCKTYPE+1] = 0x71;		//Lock Type Low BYTE(Lock Micom (Main Micom))
	FingerPrint_txding_buf[PACK_F_ENCRYPTIONTYPE] = 0x01;		//Encryption Type None AES128 ECB

	//Make Device ID 
#if defined (_USE_IREVO_CRYPTO_)
	for(i=0;i<16;i++)
	{
		if(i < 8)
			FingerPrint_txding_buf[PACK_F_DEVICEID+i] = gbStUniqueID[i];
		else 
			FingerPrint_txding_buf[PACK_F_DEVICEID+i] = gbEncryptionKey[i-8];
	}
#else 	
	for(i=0;i<16;i++)
	{
		FingerPrint_txding_buf[PACK_F_DEVICEID+i] = 0xFF;
	}
#endif 	

	FingerPrint_txding_buf[PACK_F_CHECKSUMTYPE] = 0x00;		
	FingerPrint_txding_buf[PACK_F_RESERVED] = 0xFF;
	FingerPrint_txding_buf[PACK_F_RESERVED+1] = 0xFF;
	FingerPrint_txding_buf[PACK_F_WAKEUPTIMING] = 0x00; 
	FingerPrint_txding_buf[PACK_F_WAKEUPTIMING+1] = 0x00;
	FingerPrint_txding_buf[PACK_F_WAKEUPTIMING+2] = 0x00; 
	FingerPrint_txding_buf[PACK_F_PROTOCOLVERSION] = 0x20;
	bytesum += 0x04; // + event + source + count + length 
	FingerPrint_txding_buf[(PACK_F_PROTOCOLVERSION+1)] = Get_Checksum_BCC(FingerPrint_txding_buf,bytesum);
	bytesum += 0x01; // + checksum

	// send 0x44 command 
	gbFingerPrint_txd_end = 0;
	HAL_UART_Transmit_IT(gh_mcu_uart_finger,FingerPrint_txding_buf,bytesum);

	gbBioTimer10ms = 10;

	//wait Tx end 
	while (!gbFingerPrint_txd_end)
	{
		Delay(SYS_TIMER_3MS);
#ifdef	DDL_CFG_WATCHDOG
		RefreshIwdg();
#endif
		gbBioTimer10ms--;
		if(gbBioTimer10ms == 0x00)
		{
			gfBioErr = 0x01;
			break;
		}
	}
	
	gbBioTimer10ms = 0;
	gbFingerPrint_txd_end = 0;

	// wait 0x44 ack 
	gbBioTimer10ms = 10;
	
	while (!gbFingerPrint_rxd_end)
	{
		Delay(SYS_TIMER_100MS);
		Delay(SYS_TIMER_100MS);		
#ifdef	DDL_CFG_WATCHDOG
		RefreshIwdg();
#endif
		gbBioTimer10ms--;
		if(gbBioTimer10ms == 0x00)
		{
			gfBioErr = 0x01;
			break;
		}
	}

	if(gfBioErr == 0x01)
	{
		//44 응답을 못 받았다면 한번 더 보낸다
		HFPMRxDataStepChange(HFPM_WAIT_RX);
		HFPMRxDataClear();		
		HFPMTiTwuControl(0x01);
		
		gbFingerPrint_txd_end = 0;
		HAL_UART_Transmit_IT(gh_mcu_uart_finger,FingerPrint_txding_buf,bytesum);

		gbBioTimer10ms = 10;
		//wait Tx end 
		while (!gbFingerPrint_txd_end)
		{
			Delay(SYS_TIMER_3MS);
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			gbBioTimer10ms--;
			if(gbBioTimer10ms == 0x00)
			{
				gfBioErr = 0x01;
				break;
			}
		}

		gbBioTimer10ms = 0;
		gbFingerPrint_txd_end = 0;

		// wait 0x44 ack 
		gbBioTimer10ms = 10;
		
		while (!gbFingerPrint_rxd_end)
		{
		Delay(SYS_TIMER_3MS);
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
		gbBioTimer10ms--;
		if(gbBioTimer10ms == 0x00)
		{
			gfBioErr = 0x01;
				HFPMRxDataStepChange(HFPM_WAIT_RX);
				HFPMRxDataClear();
				gbBioTimer10ms = 0;	
				gbFingerPrint_rxd_end = 0;
			return;
		}
	}
	}
	
	gbBioTimer10ms = 0;	
	gbFingerPrint_rxd_end = 0;

	// module type check 0x7030
	if(gbFingerPrint_rxd_buf[4] == 0x70 && gbFingerPrint_rxd_buf[5] == 0x30)
	{
		memcpy(bFPMTempArray,&gbFingerPrint_rxd_buf[7],16);
#ifdef _USE_IREVO_CRYPTO_
		EncryptDecryptKey(bFPMTempArray,16);
#endif 		
		RomWrite(bFPMTempArray,HFPM_MODULE_ID,16);
		gfBioErr = 0x00;		
	}
	gbBioTimer10ms = 0x00;
	HFPMRxDataStepChange(HFPM_WAIT_RX);
	HFPMRxDataClear();	
}

void FingerPrintTxDataMakeFrame(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength)
{
	BYTE bCnt = 0x00;

	FingerPrint_txding_buf[0] = bEvent;
	FingerPrint_txding_buf[1] = bEventSrc;
	if(bEventSrc == IREVO_HFPM_SOURCE_B0)	(*bCount)++;			// Event 패킷일 때만 Count 증가
	FingerPrint_txding_buf[2] = (*bCount);
	FingerPrint_txding_buf[3] = bLength ;	// checksum(1바이트) 길이 추가 
	for(bCnt = 0; bCnt < bLength; bCnt++)
	{
		FingerPrint_txding_buf[4 + bCnt] = *bpData;
		bpData++;
	}
	gbPackTxByteNum = 4 + bLength;
	FingerPrint_txding_buf[(FingerPrint_txding_buf[3]+4)] = Get_Checksum_BCC(FingerPrint_txding_buf,(FingerPrint_txding_buf[3]+4));

	if(FingerPrint_txding_buf[4] == IREVO_HFPM_SUB_EVENT_FP_ENROLL ||
		FingerPrint_txding_buf[4] == IREVO_HFPM_SUB_EVENT_FP_DELETE ||
		FingerPrint_txding_buf[4] == IREVO_HFPM_SUB_EVENT_FP_REQUEST_SLOT_INFO||
		FingerPrint_txding_buf[4] == IREVO_HFPM_SUB_EVENT_PERMANENT_KEY)
	{
		/* lock 에서 먼저 보내는 경우 */
		HFPMTiTwuControl(0x01);
	}

	HFPMRxDataClear();	
	gbFingerPrint_txd_end = 0;
	gbBioTimer10ms = 5;
	HAL_UART_Transmit_IT(gh_mcu_uart_finger,FingerPrint_txding_buf,(FingerPrint_txding_buf[3]+4+1));
}

void HFPMTxEventEnroll(BYTE Data)
{
	bFPMTempArray[0] = IREVO_HFPM_SUB_EVENT_FP_ENROLL;
	//Enroll : Sub_Data = 0x00 : auto add , 0x1~0x14 slot number , 0xFF enroll cancle
	bFPMTempArray[1] = Data; 
	bFPMTempArray[2] = 0x00;				
	FingerPrintTxDataMakeFrame(IREVO_HFPM_EVENT,IREVO_HFPM_SOURCE_B0,&gbfingerdatacounter,bFPMTempArray,3);
}

void HFPMTxEventDelete(BYTE Data)
{
	bFPMTempArray[0] = IREVO_HFPM_SUB_EVENT_FP_DELETE;
	//Delete : Sub_Data = 0x1~0x14 slot number , 0xFF delete all
	bFPMTempArray[1] = Data; 
	bFPMTempArray[2] = 0x00;				
	FingerPrintTxDataMakeFrame(IREVO_HFPM_EVENT,IREVO_HFPM_SOURCE_B0,&gbfingerdatacounter,bFPMTempArray,3);
}

void HFPMTxEventSlotInfo(void)
{
	bFPMTempArray[0] = IREVO_HFPM_SUB_EVENT_FP_REQUEST_SLOT_INFO;
	bFPMTempArray[1] = 0x00; 
	bFPMTempArray[2] = 0x00;				
	FingerPrintTxDataMakeFrame(IREVO_HFPM_EVENT,IREVO_HFPM_SOURCE_B0,&gbfingerdatacounter,bFPMTempArray,3);
}

void HFPMTxEventEnrollAlarm(void)
{
	bFPMTempArray[0] = IREVO_HFPM_SUB_EVENT_FP_ENROLL_STATUS_ALARM;
	bFPMTempArray[1] = 0x00; 
	FingerPrintTxDataMakeFrame(IREVO_HFPM_EVENT,IREVO_HFPM_SOURCE_B1,&gbfingerdatacounter,bFPMTempArray,2);
}

void HFPMTxEventRequestAuthAck(BYTE Data)
{
	bFPMTempArray[0] = IREVO_HFPM_SUB_EVENT_REQUEST_AUTHENTICATION;
	bFPMTempArray[1] = 0x01; 
	bFPMTempArray[2] = Data;	
	FingerPrintTxDataMakeFrame(IREVO_HFPM_EVENT,IREVO_HFPM_SOURCE_B1,&gbfingerdatacounter,bFPMTempArray,3);
}

void HFPMTxEventEncryptDataAck(BYTE Data)
{
	bFPMTempArray[0] = IREVO_HFPM_SUB_EVENT_ENCRYPTED_DATA;
	bFPMTempArray[1] = Data; 
	FingerPrintTxDataMakeFrame(IREVO_HFPM_EVENT,IREVO_HFPM_SOURCE_B1,&gbfingerdatacounter,bFPMTempArray,2);
}

void HFPMTxEventVerificationCode(void)
{
	/* 16 byte random number 생성 및 encrypt 후 send */
	/* bRandomVerificationCode 값은 나중에 IREVO_HFPM_SUB_EVENT_VERIFICATION_CODE */
	/* 의 ack data 와 비교 해야 함 */
	BYTE Temp[16];
	
	bFPMTempArray[0] = IREVO_HFPM_SUB_EVENT_VERIFICATION_CODE;
	
	RomRead(Temp,ENCRYPT_KEY,sizeof(Temp));
#ifdef _USE_IREVO_CRYPTO_
	EncryptDecryptKey(Temp,sizeof(Temp));
#endif 
	RandomNumberGenerator(bRandomVerificationCode,sizeof(bRandomVerificationCode));
#ifdef _USE_STM32_CRYPTO_LIB_
	DataEncrypt(bRandomVerificationCode,sizeof(bRandomVerificationCode),Temp,&bFPMTempArray[1]);
#endif 
	FingerPrintTxDataMakeFrame(IREVO_HFPM_EVENT,IREVO_HFPM_SOURCE_B0,&gbfingerdatacounter,bFPMTempArray,17);
}

void HFPMTxEventStartTerminateSession(BYTE Data)
{
	bFPMTempArray[0] = IREVO_HFPM_SUB_EVENT_START_TERMINATE_AUTH_SESSION;
	/* 0x01 start session , 0x00 terminate session */
	bFPMTempArray[1] = Data; 
	bFPMTempArray[2] = 0x00;	
	FingerPrintTxDataMakeFrame(IREVO_HFPM_EVENT,IREVO_HFPM_SOURCE_B0,&gbfingerdatacounter,bFPMTempArray,3);
}

#if 0
void HFPMTxEventSeedValue(void)
{
	bFPMTempArray[0] = IREVO_HFPM_SUB_EVENT_SEED_VALUE;
#if 1 // test
	// p = 37 , g = 5 , A = 2 , A' = 25
	// p = 37 , g = 5 , B = 7 , B' = 18
	memset(&bFPMTempArray[1],0x00,15);
#ifdef _USE_STM32_CRYPTO_LIB_
	bFPMTempArray[16] = 0x19;//(BYTE)Diffie_Hellman(5u,2u,37u);
#endif 
#else 
	Diffie_Hellman(37,5,2);
#endif 
	FingerPrintTxDataMakeFrame(IREVO_HFPM_EVENT,IREVO_HFPM_SOURCE_B0,&gbfingerdatacounter,bFPMTempArray,17);
}
#endif 

void HFPMTxEventPermanentKey(void)
{
	BYTE fp_tkey[16] = { 0xFA, 0xFE, 0xE4, 0xCA, 0xFE, 0x09, 0x52, 0x89, 0x19, 0x81, 0x02, 0x01, 0x07, 0x02, 0x06, 0x27};
	bFPMTempArray[0] = IREVO_HFPM_SUB_EVENT_PERMANENT_KEY;
	RandomNumberGenerator(bRandomVerificationCode,sizeof(bRandomVerificationCode));
#ifdef _USE_STM32_CRYPTO_LIB_
	DataEncrypt(bRandomVerificationCode,sizeof(bRandomVerificationCode),fp_tkey,&bFPMTempArray[1]);
#endif 
	FingerPrintTxDataMakeFrame(IREVO_HFPM_EVENT,IREVO_HFPM_SOURCE_B0,&gbfingerdatacounter,bFPMTempArray,17);
}

void HFPMSavePermanentKey(void)
{
	memcpy(bHFPMTempArrayKey,bRandomVerificationCode,sizeof(bHFPMTempArrayKey));						
#ifdef _USE_IREVO_CRYPTO_
	EncryptDecryptKey(bHFPMTempArrayKey,sizeof(bHFPMTempArrayKey));
#endif 	
	RomWrite(bHFPMTempArrayKey,ENCRYPT_KEY,sizeof(bHFPMTempArrayKey));
}

BYTE HFPMVerifyCommandParser(BYTE* bData)
{
	/* data 가 정상이면서 인증 완료의 경우  slotnumber 를 return */	
	/* data 가 정상이면서 지문이 없는 경우  IREVO_HFPM_RESULT_CODE_FAIL_VERIFICATION 를 return */		
	/* data 가 정성아지 않으면 0xFF 를 return */
	
	BYTE bTemp = bData[0];
	if(Get_Checksum_BCC(&bData[1],(bTemp-1)) == bData[bTemp]) /* CRC */
	{
		if(bData[1] == IREVO_HFPM_EVENT && bData[5] == IREVO_HFPM_SUB_EVENT_FP_VERIFY)
		{
			if(bData[6] == IREVO_HFPM_RESULT_CODE_SUCCESS_VERIFICATION)
			{
				/* 모든 과정이 끝나고 인증 ok 인 경우 */
				bTemp = bData[7];
			}
			else
			{
				/* data 는 정상이나 지문이 없는 경우 */
				bTemp = IREVO_HFPM_RESULT_CODE_FAIL_VERIFICATION;
			}
		}
		else 
		{
			/* 잘못된 경우 */
			bTemp = 0xFF;
		}
	}
	else
	{
		/* 잘못된 경우 */		
		bTemp = 0xFF;
	}
	return bTemp;
}

void HFPMBioTimerCallback(void)
{
	/* Tx 발생 시키기 전에 gbBioTimer10ms = 5 , gbFingerPrint_txd_end = 0 */
	/* Tx complete callback 에서 gbBioTimer10ms = 0 , gbFingerPrint_txd_end = 1 */
	/* Tx 가 complete 되지 않으면 uart 재 설정 */
	/* gbBioTimer10ms 다른 지문 모듈과 같은 방식으로 gbBioTimer10ms 제어 하지 말것 */
	if(gbBioTimer10ms)
	{
		--gbBioTimer10ms;
		if(gbBioTimer10ms == 0 && !gbFingerPrint_txd_end)
		{
			HFPMRepairUart();
			FeedbackError(AVML_MALFUNCTION_FPM_MSG, VOL_CHECK);
			HFPMRxDataStepChange(HFPM_WAIT_RX);
			BioOffModeClear();
		}
	}
}

void HFPMTxEventFingerPresentStatus(void)
{
	bFPMTempArray[0] = IREVO_HFPM_SUB_EVENT_FINGER_PRESENT_STATUS;
	bFPMTempArray[1] = 0x00; 
	FingerPrintTxDataMakeFrame(IREVO_HFPM_EVENT,IREVO_HFPM_SOURCE_B0,&gbfingerdatacounter,bFPMTempArray,2);
}

void BioCommCheck(BYTE bsave)
{
	// POR 이후 1 초 이상 걸려야 통신 가능 
	Delay(SYS_TIMER_100MS*5);
	Delay(SYS_TIMER_100MS*5);

	// send 0x44 
	FingerPrintModuleCheck();
	BioModuleOff();
}

void FPCoverSwProcess(void)
{
	if(GetVoicePrcsStep() && !gfIntrusionAlarm) return; //경보 중일 경우는 열수 있게 
	
	if(GetMainMode() == 0)
	{		
		if(GetMotorPrcsStep())		return;
			ModeGotoFingerVerify();
	}
	else
	{
		if(GetMainMode() == MODE_PIN_VERIFY && gfMute == 1 && gPinInputKeyCnt == 0)
		{
			ModeGotoFingerVerify();
		}
	}
}

void BioModuleON( void )
{
	return;
}

void BioModuleOff( void )
{
#if 0	
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = FP_DDL_TX_Pin | FP_DDL_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(FP_DDL_TX_GPIO_Port, &GPIO_InitStruct);
#endif 	
	HFPMRxDataClear();
	return ;
}

void BioOffModeClear( void )
{
	BioModuleOff();
	ModeClear();
	gbModeChangeFlag4Finger = 0;
	gbLedDisplayFPMOperation = 0x00;
}

void FingerPrintRegisterDisplay(void)
{
	if(gbFingerLongTimeWait100ms >= 5)	
	{
		if(dbDisplayOn == 0x00)
		{
			LedSetting(gcbLedFMREG, LED_KEEP_DISPLAY);
			dbDisplayOn = 0x01;
		}
	}
	else if(gbFingerLongTimeWait100ms < 5)
	{ 
		if(dbDisplayOn == 0x01)
		{
			LedSetting(&gcbLedAREAFMREGSTEP[gbFingerRegisterDisplayStep][0], LED_KEEP_DISPLAY);
			dbDisplayOn = 0x00;
		}

		if(gbFingerLongTimeWait100ms == 0) 
			gbFingerLongTimeWait100ms = 10;
	}
}




