#ifndef		_FINGER_IREVO_HFPM_H_
#define		_FINGER_IREVO_HFPM_H_

#include "DefineMacro.h"
//#include "DefinePin.h"

#define	IREVO_HFPM_EVENT			0x74
#define	IREVO_HFPM_SOURCE_B0		0xB0
#define	IREVO_HFPM_SOURCE_B1		0xB1
#define	IREVO_HFPM_SOURCE_B2		0xB2
#define	IREVO_HFPM_SOURCE_B3		0xB3

#define	IREVO_HFPM_SUB_EVENT_FP_ALARM	0x50

#define	IREVO_HFPM_SUB_EVENT_FP_ENROLL	0x51
#define	IREVO_HFPM_SUB_EVENT_FP_ENROLL_CANCLE	0x59

#define	IREVO_HFPM_SUB_EVENT_FP_ENROLL_STATUS_ALARM	0x58

#define	IREVO_HFPM_RESULT_CODE_PROCESSING_ENROLL	0x12
#define	IREVO_HFPM_RESULT_INVALID_SLOT_NUMBER_ENROLL	0x13


#define	IREVO_HFPM_SUB_EVENT_FP_VERIFY	0x52
#define	IREVO_HFPM_SUB_EVENT_FP_DELETE	0x53

#define	IREVO_HFPM_SUB_EVENT_FP_REQUEST_SLOT_INFO	0x54
#define	IREVO_HFPM_SUB_EVENT_FP_LOOP_BACK	0x55

#define	IREVO_HFPM_SUB_EVENT_FINGER_PRESENT_STATUS	0x57


#define	IREVO_HFPM_SUB_EVENT_SEED_VALUE	0x60
#define	IREVO_HFPM_SUB_EVENT_PERMANENT_KEY	0x61
#define	IREVO_HFPM_SUB_EVENT_REQUEST_AUTHENTICATION		0x62
#define	IREVO_HFPM_SUB_EVENT_VERIFICATION_CODE	0x63
#define	IREVO_HFPM_SUB_EVENT_START_TERMINATE_AUTH_SESSION	0x64
#define	IREVO_HFPM_SUB_EVENT_ENCRYPTED_DATA	0x65







#define	IREVO_HFPM_RESULT_CODE_STATUS_OK	0x00
#define	IREVO_HFPM_RESULT_CODE_PACKET_ERR	0x01
#define	IREVO_HFPM_RESULT_CODE_CHECK_SUM_ERR	0x02
#define	IREVO_HFPM_RESULT_CODE_SUCCESS_VERIFICATION	0x10
#define	IREVO_HFPM_RESULT_CODE_FAIL_VERIFICATION	0x20
#define	IREVO_HFPM_RESULT_CODE_SLOT_EMPTY	0x11

// BLE only
//#define	IREVO_HFPM_SUB_EVENT_FP_BLE_START	0x56
//#define	IREVO_HFPM_SUB_EVENT_FP_BLE_START_PARING	0x57

#define BIO_WAKEUP_TM		250

extern  FLAG BioFlag;

#define gfModuleOK		BioFlag.STATEFLAG.Bit0						//정상적인 모듈
#define gfUART0TxEnd	BioFlag.STATEFLAG.Bit1
#define gfUART0RxEnd	BioFlag.STATEFLAG.Bit2
#define gfBioCoverIn		BioFlag.STATEFLAG.Bit3
#define gfBioCoverDown	BioFlag.STATEFLAG.Bit4
#define gfBioUARTST		BioFlag.STATEFLAG.Bit5
#define gfBioUARTStuff	BioFlag.STATEFLAG.Bit6
#define gfBioTXStuff		BioFlag.STATEFLAG.Bit7

extern WORD gbBioTimer10ms;
extern BYTE gbFingerPrint_rxd_end;
extern BYTE gbFingerPrint_rxd_buf[40];
extern BYTE gbCoverAccessDelaytime100ms;
extern BYTE gbFingerLongTimeWait100ms;
extern BYTE gbFID;
extern BYTE gbFingerRegisterDisplayStep;
extern BYTE dbDisplayOn;

#ifdef	BLE_N_SUPPORT
extern BYTE gbEnterMode;
#endif 

void Register_HFPM_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) );
void HFPMRepairUart(void);


void FingerPrintTxDataMakeFrame(BYTE bEvent, BYTE bEventSrc, BYTE* bCount, BYTE* bpData, BYTE bLength);
void HFPMTxEventEnroll(BYTE Data);
void HFPMTxEventDelete(BYTE Data);
void HFPMTxEventSlotInfo(void);
void HFPMTxEventEnrollAlarm(void);
void HFPMTxEventRequestAuthAck(BYTE Data);
void HFPMTxEventEncryptDataAck(BYTE Data);
void HFPMTxEventVerificationCode(void);
void HFPMTxEventStartTerminateSession(BYTE Data);
#if 0
void HFPMTxEventSeedValue(void);
#endif 
void HFPMTxEventPermanentKey(void);
void HFPMSavePermanentKey(void);
BYTE HFPMVerifyCommandParser(BYTE* bData);
void HFPMBioTimerCallback(void);
void HFPMTxEventFingerPresentStatus(void);
void HFPMRxDataClear(void);
void HFPMRxDataStepChange(BYTE Step);
BYTE GetHFPMRxDataStep(void);
void HFPMRxDataProcess(void);
void HFPMTiTwuControl(BYTE bTiTwuControl);
void HFPM_uart_rxcplt_callback(UART_HandleTypeDef *huart);
void HFPM_uart_txcplt_callback(UART_HandleTypeDef *huart);
void InitialSetupFingerModule( void );
BYTE GetNoFingerFromEeprom( void );
void FingerPrintModuleCheck(void);
void BioCommCheck(BYTE bsave);
void FPCoverSwProcess(void);
void BioModuleON( void );
void BioModuleOff( void );
void BioOffModeClear( void );
void FingerPrintRegisterDisplay(void);
#endif

