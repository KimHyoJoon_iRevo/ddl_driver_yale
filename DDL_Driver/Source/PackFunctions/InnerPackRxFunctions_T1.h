//------------------------------------------------------------------------------
/** 	@file		InnerPackRxFunctions_T1.h
	@brief	Communication Pack RX Process Functions
*/
//------------------------------------------------------------------------------


#ifndef __INNERPACKRXFUNCTIONS_T1_INCLUDED
#define __INNERPACKRXFUNCTIONS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"



extern WORD gbInnerPackRxByteCnt;
extern BYTE gbInnerPackRxAddDataLen;
extern BYTE gbInnerPackRxDataBuf[MAX_INNER_PACK_BUF_SIZE];

extern WORD gbInnerPackRxCopiedByteCnt;
extern BYTE gbInnerPackRxDataCopiedBuf[MAX_INNER_PACK_BUF_SIZE];

extern BYTE gInnerPackRxProcessStep;
extern BYTE gbInnerPackTxAddNone[10];


// gPackRxProcessStep
enum{
	INNER_PACK_RX_WAITING_MODE = 0,
	INNER_PACK_RX_WAKEUP,
	INNER_PACK_RX_PACKET_CHECKING_MODE,
	INNER_PACK_RX_ACK_PACKET_PROCESSING_MODE,
	INNER_PACK_RX_CMD_PACKET_PROCESSING_MODE,
	INNER_PACK_RX_CMD_PACKET_DECRYPT_PROCESSING_MODE,	
};

void InnerPackRxDataClear(void);

void InnerPackRxDataProcess(void);

void InnerPackDataReceiveProcess(void);


void InnerPackRxProcessStepStart(void);
void InnerPackRxProcessWakeUp(void);
void InnerPackRxProcessStepClear(void);
BYTE GetInnerPackRxProcessStep(void);

BYTE InnerPackRxAckProcess(void);
BYTE InnerPackRxCmdProcess(void);
void InnerPackRxPermittedCmdProcess(void);

BYTE InnerPackRxDecapsulateData(BYTE *bAdditionalData , uint16_t length);
BYTE InnerPackRxEncryptionKeyExchangeCheck(BYTE* bCheckSumData);


__weak BYTE LockStsCheck(void);
__weak void DoorStsCheck(BYTE *bTmpArray);
__weak void SetUserStatus(WORD wUserSlot, BYTE bUserStatus);
__weak BYTE GetUserStatus(WORD wUserSlot);
__weak void SetScheduleStatus(WORD wUserSlot, BYTE bStatus);
__weak BYTE GetScheduleStatus(WORD wUserSlot);
__weak void AsciiToBcd(BYTE *bAsciiArray, BYTE *bBcdArray, BYTE bNum);
__weak void BcdToAscii(BYTE *bBcdArray, BYTE *bAsciiArray);


#endif


