//------------------------------------------------------------------------------
/** 	@file		InnerPackFunctions_T1.h
	@brief	Communication Pack Process Functions
*/
//------------------------------------------------------------------------------


#ifndef __INNER_PACKFUNCTIONS_T1_INCLUDED
#define __INNER_PACKFUNCTIONS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"



#define	MAX_INNER_PACK_BUF_SIZE		40


#include "EventLog.h"
#include "InnerPack_UART.h"
#include "InnerPackTxFunctions_T1.h"
#include "InnerPackRxFunctions_T1.h"
#include "RemoconFunctions_T1.h"

#ifdef	BLE_N_SUPPORT
#include "BLENFunctions_T1.h"
#endif

extern FLAG InnerPackFlag;								
#define	gfInnerPackIDChecked				InnerPackFlag.STATEFLAG.Bit0
#define	gfInnerPackTypeiRevo				InnerPackFlag.STATEFLAG.Bit1
#define	gfInnerPackZBZWRegisterd			InnerPackFlag.STATEFLAG.Bit2
#define	gfInnerPackTypeiRevoBleN			InnerPackFlag.STATEFLAG.Bit3
#define	gfInnerPackTypeZBZWModule		InnerPackFlag.STATEFLAG.Bit4
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
#define	gfInnerPackTypeiRevoBleN30		InnerPackFlag.STATEFLAG.Bit5
#endif
#define	gfInnerPackTypeCBABle			InnerPackFlag.STATEFLAG.Bit6

extern BYTE gbInnerComCnt;

extern BYTE gInnerPackConnectionMode;
extern BYTE gbInnerModuleMode;
extern BYTE gbInnerModuleProtocolVersion;
extern BYTE gbInnerEncryptionType;

extern BYTE gbInnerPackModeTimer10ms;
extern BYTE gInnerPackProcessResult;

void InnerPackWakeupInitial(void);
void InitInnerPackService( void );
void InnerPackConnectionStart(void);
void InnerPackSendRunApplicaionAck(void);
void InnerPackModuleModeClear(void);
void InnerPackCheckTimeCounter(void);
BYTE InnerPackConnectedCheck(void);
void InnerPackModeTimeCounter(void);
void SetInnerPackModeTimeCount(BYTE SetData);
BYTE GetInnerPackModeTimeCount(void);
void InnerPackConnectionStartCheck(void);
void InnerPackDisconnectionCheck(void);
void InnerPackConnectionProcess(void);
void InnerPackTxProcessStepStart(WORD AckWaitTime);

enum{
	INNERPACK_RESERVED,
	INNERPACK_IREVO_PACK,
	INNERPACK_IREVO_BLE_N_PACK,
	INNERPACK_IREVO_ZWAVE_PACK,	
	INNERPACK_IREVO_ZIGBEE_PACK = INNERPACK_IREVO_ZWAVE_PACK,
	INNERPACK_IREVO_CBA_BLE_PACK,
	INNERPACK_IREVO_MAX = 0xff	
};


#if 0
//	gbModuleMode
#ifndef P_COM_DDL_EN_T
enum{
	RF_TRX_MODULE = 0x01,				//장착된 양방향 모듈, 등록장치 없음
	RF_TRX_MODULE_1WAY = 0x05,			//장착되어 단방향 등록장치 등록된 양방향 모듈
	RF_TRX_MODULE_2WAY = 0x09,			//장착되어 양방향 등록장치 등록된 양방향 모듈
};
#endif 
#endif 

BYTE InnerPackModeCheck(void);
void InnerPackTypeVariableClear(void);


#define	PACK_TYPE_IREVO				0x01
#define	PACK_TYPE_EMEA_ZWAVE_300	0x07
#define PACK_TYPE_IREVO_BLEN		0x11
#define	PACK_TYPE_JIG				0xFE
#define	PACK_ID_CONFIRMED			0xAB
#define	PACK_ID_NOT_CONFIRMED		0x01
#define	PACK_ID_TEST_CONFIRMED		0x02

BYTE InnerPackTypeCheck(BYTE TypeData);
void InnerPackDeviceCheck(void);
void InnerPackSaveID(void);


//#define	PACK_PACKET_EVENT				0
//#define	PACK_PACKET_SOURCE			1
//#define	PACK_PACKET_COUNT				2
//#define	PACK_PACKET_LENGTH_ADDDATA	3
//#define	PACK_PACKET_ADDDATA			4
#define	PACK_F_EVENT				0
#define	PACK_F_SOURCE				1
#define	PACK_F_COUNT				2
#define	PACK_F_LENGTH_ADDDATA		3
#define	PACK_F_ADDDATA				4

//#define	PACK_PACKET_LOCKTYPE			4
//#define	PACK_PACKET_ENCRYPTIONTYPE		(PACK_F_LOCKTYPE+2)	
//#define	PACK_PACKET_DEIVCEID				(PACK_F_ENCRYPTIONTYPE+1)	
//#define	PACK_PACKET_CHECKSUMTYPE		(PACK_F_DEVICEID+16)	
//#define	PACK_PACKET_RESERVED			(PACK_F_CHECKSUMTYPE+1)	
//#define	PACK_PACKET_WAKEUPTIMING		(PACK_F_RESERVED+2)		
//#define	PACK_PACKET_PROTOCOLVERSION		(PACK_F_WAKEUPTIMING+3)	
#define	PACK_F_LOCKTYPE			4
#define	PACK_F_ENCRYPTIONTYPE		(PACK_F_LOCKTYPE+2)	
#define	PACK_F_DEVICEID			(PACK_F_ENCRYPTIONTYPE+1)	
#define	PACK_F_CHECKSUMTYPE		(PACK_F_DEVICEID+16)	
#define	PACK_F_RESERVED			(PACK_F_CHECKSUMTYPE+1)	
#define	PACK_F_WAKEUPTIMING		(PACK_F_RESERVED+2)		
#define	PACK_F_PROTOCOLVERSION	(PACK_F_WAKEUPTIMING+3)	



#define	PACK_PACKET_RMC_TYPE			4
#define	PACK_PACKET_RMC_SLOTNUMBER	5
// gPackProcessResult
#if 0
#ifndef P_COM_DDL_EN_T
enum{
	PACK_RMC_REGDEVICE_IN = 1,
	PACK_RMC_ALLREG_DEV_END,
	PACK_RMC_ALL_REMOVE_END,
	PACK_RMC_ALL_REGISTRATION_ACK,

	PACK_HCP_JOIN_UNJOIN_OK,
	PACK_HCP_JOIN_UNJOIN_FAIL,
				

};
#endif 
#endif 

void SetInnerPackProcessResult(BYTE bResult);
BYTE GetInnerPackProcessResult(void);
void InnerPackProcessResultClear(void);
void ChangeInnerPackPinToAnalog(void);
#endif


