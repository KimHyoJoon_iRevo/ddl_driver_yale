//------------------------------------------------------------------------------
/** 	@file		PackRxFunctions_T1.h
	@brief	Communication Pack RX Process Functions
*/
//------------------------------------------------------------------------------


#ifndef __PACKRXFUNCTIONS_T1_INCLUDED
#define __PACKRXFUNCTIONS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"



extern WORD gbPackRxByteCnt;
extern BYTE gbPackRxAddDataLen;
extern BYTE gbPackRxDataBuf[MAX_PACK_BUF_SIZE];

extern WORD gbPackRxCopiedByteCnt;
extern BYTE gbPackRxDataCopiedBuf[MAX_PACK_BUF_SIZE];

extern BYTE gPackRxProcessStep;
extern BYTE gbPackTxAddNone[10];
extern bool gfDirectSending;

extern BYTE gbAckCompleteData[2];


// gPackRxProcessStep
enum{
	PACK_RX_WAITING_MODE = 0,
	PACK_RX_WAKEUP,
	PACK_RX_PACKET_CHECKING_MODE,
	PACK_RX_ACK_PACKET_PROCESSING_MODE,
	PACK_RX_CMD_PACKET_PROCESSING_MODE,
	PACK_RX_CMD_PACKET_DECRYPT_PROCESSING_MODE,	
};

void PackRxDataClear(void);

void PackRxDataProcess(void);

void PackDataReceiveProcess(void);


void PackRxProcessStepStart(void);
void PackRxProcessWakeUp(void);
void PackRxProcessStepClear(void);
BYTE GetPackRxProcessStep(void);
BYTE PackRxDecapsulateData(BYTE *bAdditionalData , uint16_t length);
BYTE PackRxEncryptionKeyExchangeCheck(BYTE* bCheckSumData);

BYTE PackRxAckProcess(void);

BYTE PackRxCmdProcess(void);
void PackRxPermittedCmdProcess(void);


BYTE LockStsCheck(void);
void DoorStsCheck(BYTE *bTmpArray);
void SetUserStatus(WORD wUserSlot, BYTE bUserStatus);
BYTE GetUserStatus(WORD wUserSlot);
void SetScheduleStatus(WORD wUserSlot, BYTE bStatus);
BYTE GetScheduleStatus(WORD wUserSlot);
void AsciiToBcd(BYTE *bAsciiArray, BYTE *bBcdArray, BYTE bNum);
void BcdToAscii(BYTE *bBcdArray, BYTE *bAsciiArray);


static void PackDeleteUserGMTouchKey(WORD wUserSlot);


#endif


