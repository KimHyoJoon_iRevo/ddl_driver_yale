//------------------------------------------------------------------------------
/** 	@file		InnerPackFunctions_T1.c
	@version 0.1.01
	@date	2016.12.26
	@brief	Inner Communication Pack Process Functions
	@see	
	@section MODIFYINFO
		[Revision History]
		Main , Front 에 바로 삽입 되는 내장 통신 팩에 대한 functions 
		PackFunctions_T1.c 의 내용을 그대로 복사 BY 김효준 
		
*/
//------------------------------------------------------------------------------

#include "Main.h"

FLAG InnerPackFlag;	
BYTE gInnerPackConnectionMode = 0;
BYTE gbInnerPackStatus = 0;
BYTE gbInnerPackChkTimer100ms = 0;
BYTE gbInnerPackChkCnt = 0;
BYTE gbInnerComCnt = 0;	// 통신 패킷 Count(제3바이트)
BYTE gbInnerModuleMode = 0;
BYTE gbInnerModuleProtocolVersion = 0;
BYTE gbInnerEncryptionType = 0 ;
BYTE gbInnerPackModeTimer10ms = 0;
BYTE gInnerPackProcessResult = 0;

BYTE InnerPackConnectedCheck(void)
{
#if defined (P_BT_WAKEUP_T)
	if(P_BT_WAKEUP_T)	
	{
		return 1;
	}
#endif 
	return 0;	
}

void InnerPackSaveID(void)
{
	RomWrite(&gbInnerPackRxDataCopiedBuf[PACK_PACKET_RMC_SLOTNUMBER], INNER_PACK_ID, 4);
	gbInnerModuleMode = PACK_ID_CONFIRMED; 
}


BYTE GetInnerPackProcessResult(void)
{
	return (gInnerPackProcessResult);
}

void InnerPackProcessResultClear(void)
{
	gInnerPackProcessResult = 0;
}

#if defined (P_BT_WAKEUP_T)
void InnerPackModuleModeClear(void)
{
//	OffGpioCommPack();

	gbInnerModuleMode = 0;
	gbInnerPackChkCnt = 0;
}

void InnerPackCheckTimeCounter(void)
{
	if(gbInnerPackChkTimer100ms)		--gbInnerPackChkTimer100ms;
#ifdef	__FRONT_BLE_CONTROL__	
	if(gbFrontBLENStatusCheckTimer100ms)		--gbFrontBLENStatusCheckTimer100ms;
#endif 
}


void InnerPackModeTimeCounter(void)
{
	if(gbInnerPackModeTimer10ms) 	--gbInnerPackModeTimer10ms;
}


void SetInnerPackModeTimeCount(BYTE SetData)
{
	gbInnerPackModeTimer10ms = SetData;
}


BYTE GetInnerPackModeTimeCount(void)
{
	return (gbInnerPackModeTimer10ms);
}



void InnerPackWakeupInitial(void)
{
	InnerPackRxDataClear();

	SetInnerPackModeTimeCount(20);

	InnerPackRxProcessWakeUp();

 /*	
	if(gbPackMode != PACK_ACKWAIT_MODE)
	{
		gbPackMode = PACK_RX_DATA_WAITING;

		InnerPackRxInterruptRequest();
	}
*/
}



void InnerPackDeviceInitialSend(void)
{
	BYTE bCnt;
	BYTE		tmpArray[50];

//	gbPackTxDataBuf[PACK_F_EVENT] = 0x44;
//	gbPackTxDataBuf[PACK_F_SOURCE] = 0xB0;
//	gbPackTxDataBuf[PACK_F_COUNT] = 0x00;
	
//	gbPackTxDataBuf[PACK_F_LENGTH_ADDDATA] = 0x1A;
//	gbPackTxDataBuf[PACK_F_LOCKTYPE] = 0x70;
//	gbPackTxDataBuf[PACK_F_LOCKTYPE+1] = 0x71;
//	gbPackTxDataBuf[PACK_F_ENCRYPTIONTYPE] = 0x00;
//
//	for(bCnt = 0; bCnt < 16; bCnt++)
//	{
//		gbPackTxDataBuf[PACK_F_DEVICEID+bCnt] = bCnt+1;
//	}
//	

//	gbPackTxDataBuf[PACK_F_CHECKSUMTYPE] = 0x00;	 // N-Protocol V2	
//	gbPackTxDataBuf[PACK_F_RESERVED] = 0xFF;
//	gbPackTxDataBuf[PACK_F_RESERVED+1] = 0xFF;
//	gbPackTxDataBuf[PACK_F_WAKEUPTIMING] = 0x00; // Tl Time : Default (100us)
//	gbPackTxDataBuf[PACK_F_WAKEUPTIMING+1] = 0x00; // Twu Time : Default (150us)
//	gbPackTxDataBuf[PACK_F_WAKEUPTIMING+2] = 0x00; // Units and Multiplier
//	gbPackTxDataBuf[PACK_F_PROTOCOLVERSION] = 0x20;
//
//	gbPackTxByteNum = 30;

//	PackTxProcessStepStart(0);

	tmpArray[PACK_F_LOCKTYPE] = 0x70;
	tmpArray[PACK_F_LOCKTYPE+1] = 0x71;
#if (DDL_CFG_BLE_30_ENABLE >= 0x30)	
	tmpArray[PACK_F_ENCRYPTIONTYPE] = 0x01;
#else 	
	tmpArray[PACK_F_ENCRYPTIONTYPE] = 0x00;
#endif 

//	for(bCnt = 0; bCnt < 16; bCnt++)
//	{
//		tmpArray[PACK_F_DEVICEID+bCnt] = bCnt+1;
//	}

	RomRead(&tmpArray[PACK_F_DEVICEID], (WORD)DOORLOCK_ID, 16);  

	
	tmpArray[PACK_F_CHECKSUMTYPE] = 0x00;		// N-Protocol V2
	tmpArray[PACK_F_RESERVED] = 0xFF;
	tmpArray[PACK_F_RESERVED+1] = 0xFF;
	tmpArray[PACK_F_WAKEUPTIMING] = 0x00; // Tl Time : Default (100us)
	tmpArray[PACK_F_WAKEUPTIMING+1] = 0x00; // Twu Time : Default (150us)
	tmpArray[PACK_F_WAKEUPTIMING+2] = 0x00; // Units and Multiplier

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	tmpArray[PACK_F_PROTOCOLVERSION] = DDL_CFG_BLE_30_ENABLE;
#else
	tmpArray[PACK_F_PROTOCOLVERSION] = 0x20;
#endif

	InnerPackTx_MakePacketForInitial(F0_RF_MODULE_DEVICE_INFORMATION, ES_LOCK_EVENT, &gbInnerComCnt, &tmpArray[PACK_F_LOCKTYPE], 26);
}




void InnerPackConnectionStart(void)
{
	// 생산 지그 연결 확인을 위해 1회는 Device Information Exchange 시도
	P_BT_EN(1);

	InnerPackTxEnSigSetDefault();

	SetupInnerPack_UART();
	//반드시 UART Setup 후에 Service callback 함수 등록을 해야 정상적으로 동작됨. 
	InitInnerPackService();

	gbInnerPackChkCnt = 0;
	gInnerPackConnectionMode = 1;
}

void InnerPackSendRunApplicaionAck(void)
{
	BYTE bTemp = 0x00;

	if(InnerPackConnectedCheck())
	{
		P_BT_EN(1);
		InnerPackTxEnSigSetDefault();
		SetupInnerPack_UART();
		InitInnerPackService();
		gbInnerModuleMode = PACK_ID_CONFIRMED;
		InnerPackTx_MakeAndSendPacketNow(F0_RUN_APPLICATION,ES_LOCK_ACK,&gbInnerComCnt,&bTemp, 0);
		InnerPackTx_QueueInit();
		gbInnerModuleMode = 0x00;	
	}
}


void InnerPackConnectionStartCheck(void)
{
	if(InnerPackConnectedCheck())
	{
		InnerPackConnectionStart();
	}
	else
	{
		// 해당 함수가 여기에 있을 경우 팩 장착이 되어 있지 않으면 계속 Load가 걸려 카드 읽기 처리하는데 문제가 됨.
		// InnerPackModuleModeClear 함수 내에 위치하도록 수정
		//OffGpioCommPack();

		gInnerPackConnectionMode = 0;
	}
}


void InnerPackDisconnectionCheck(void)
{
	gbInnerPackChkTimer100ms = 5;
	gInnerPackConnectionMode = 5;
}


#define	INNER_PACK_CONNECTION_TRY_COUNT		10		// 응답 없을 경우 총 10회 시도
#define	INNER_PACK_CONNECTION_TRY_INTERVAL	5		// 500 ms 간격으로 확인	

// 통신팩이 장착되어 있을 경우 500ms 간격으로 10회 반복하여 Device Information Exchange 시도
// 위 시도는 도어록의 전원이 인가된 후 500ms ~ 2s 사이에 첫 시도가 진행되어야 함.
void InnerPackConnectionProcess(void)
{
	switch(gInnerPackConnectionMode)
	{
		case 0:
			// 점검이 끝난 뒤, 상태 체크중
			// voice 나 buzzer 중이면 기다렸다가 체크 
#ifdef		__DDL_MODEL_B2C15MIN
			if(GetLedMode()) break;
#else 
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
#endif 			

			if(gbInnerModuleMode)
			{
				if(InnerPackConnectedCheck() == 0)
				{
					InnerPackDisconnectionCheck();
				}
			}
			else if(gbInnerPackChkCnt == 0)
			{
				InnerPackConnectionStartCheck();
			}
			break;
		
		case 1:
			if(gbInnerPackChkTimer100ms)			break;

			InnerPackDeviceInitialSend();
			InnerPackRxDataClear();

			gbInnerPackChkTimer100ms = INNER_PACK_CONNECTION_TRY_INTERVAL;

			gInnerPackConnectionMode++;
			break;
		
		case 2:
#if defined (P_COM_DDL_EN_T ) && defined (P_BT_WAKEUP_T) 			
			if(gbInnerModuleMode)
#else 
			if(gbInnerModuleMode || (JigModeStatusCheck() == STATUS_SUCCESS))
#endif 
			{
				// UART PROCESS 에서 Pack 체크가 되었는지 기다린다.
				gbInnerPackChkCnt++;
				gInnerPackConnectionMode = 0;
			}
			else if(gbInnerPackChkTimer100ms == 0)	// time out이면 
			{
				gbInnerPackChkCnt++;

				if(gbInnerPackChkCnt >= INNER_PACK_CONNECTION_TRY_COUNT)
				{
					// UART 실패후 DDL 에서 Device Check 송신한다.
					gInnerPackConnectionMode = 0;
				}
				else
				{
					if(InnerPackConnectedCheck())
					{
						// UART 실패후 DDL 에서 Device Check 송신한다.
						gInnerPackConnectionMode--;
					
					}
					else
					{
						gbInnerPackChkCnt = 0;
						gInnerPackConnectionMode = 0;
					}
				}
			}
			break;

#ifdef	__FRONT_BLE_CONTROL__
		case 4:
			if(gbFrontBLENStatusCheckTimer100ms)
				break;

			BLENTxFrontBLEStartStopCheckSend(SUBEV_DATA_CHECK_MODULE);	
			gInnerPackConnectionMode = 0;
		break;
#endif 

		case 5:
			// Pack 떨어짐. 채터링
			if(InnerPackConnectedCheck() == 0)
			{
				if(gbInnerPackChkTimer100ms == 0)
				{
					// 탈착 처리
					OffGpioInnerPack();

					InnerPackModuleModeClear();

					gInnerPackConnectionMode = 0;

					InnerPackTypeVariableClear();
					
					if(AlarmStatusCheck() == STATUS_SUCCESS)		break;

#if 0 // pack 제거시 UI 적으로 아무것도 안한다. 
					if(GetTamperProofPrcsStep())
					{
						TamperCountClear();
					}

#ifdef	DDL_CFG_DIMMER
					LedSetting(gcbLedOff, LED_DISPLAY_OFF);
#endif
						
#ifdef	DDL_CFG_NO_DIMMER
					LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
#endif

#ifdef	DDL_CFG_FP  //hyojoon_20160831 PFM-3000 add
					BioModuleOff();
#endif
					ModeClear();
					
//지문 모델의 경우 음성안내가 길기 때문에 팩이 빠지면 중간에 음성을 끊고 완료음을 내도록 한다
#ifndef	DDL_CFG_FP //hyojoon_20160831 PFM-3000 add
					if(GetBuzPrcsStep())	break;
					if(GetVoicePrcsStep())	break;
#endif

					//FeedbackModeClear();
#endif 
				}
			}
			else
			{
				gInnerPackConnectionMode = 0;
			}
			break;

		default:
			gInnerPackConnectionMode = 0;
			break;
	}
}




void		InitInnerPackService( void )
{
	RegisterInnerPack_INT_Callback( InnerPack_uart_txcplt_callback, InnerPack_uart_rxcplt_callback );
	InnerPackRxInterruptRequest();
	InnerPackTx_QueueInit();
}





void InnerPackModeClear(void)
{
	InnerPackRxDataClear();
	InnerPackRxProcessStepClear();

//	gbPackMode = 0;
	gbInnerPackModeTimer10ms = 0;
}



BYTE InnerPackModeCheck(void)
{
	if(gInnerPackConnectionMode)		return (STATUS_SUCCESS);

	return (STATUS_FAIL);
}



void InnerPackTypeVariableClear(void)
{
	extern uint8_t gInnerPackTxAckWaitIndex;
	extern uint8_t gbInnerAckWaitBuffer[ACK_WAIT_BUFFER_SIZE][MAX_PACK_BUF_SIZE+3];
	
	gfInnerPackTypeiRevo = 0;
	gfInnerPackTypeiRevoBleN = 0;
	gfInnerPackTypeZBZWModule = 0;
	gfInnerPackTypeCBABle = 0;
	gbInnerEncryptionType = 0x00;
	gbInnerModuleProtocolVersion = 0;

	gInnerPackTxAckWaitTimer100ms = 0;
	gInnerPackTxAckWaitIndex= 0;
	for(uint8_t i = 0 ; i <ACK_WAIT_BUFFER_SIZE ; i++)
	{
		gbInnerAckWaitBuffer[i][0] = 0;
	}	
}
	

 
BYTE InnerPackTypeCheck(BYTE TypeData)
{
	BYTE RetVal = 0;
	BYTE CheckResult[2];

	// Pack Typ을 초기화하는 부분이 없어 중복 설정될 수 있는 가능성 때문에 추가
	InnerPackTypeVariableClear();

	switch(TypeData)
	{
		case PACK_TYPE_JIG:
//			JigModeStart();

			InnerPackRxDataClear();
			InnerPackRxProcessStepClear();

			RetVal = PACK_TYPE_JIG;
			break;

		case PACK_TYPE_IREVO:
			gfInnerPackTypeiRevo = 1;
			RetVal = PACK_TYPE_IREVO;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			gbInnerEncryptionType = gbInnerPackRxDataCopiedBuf[PACK_F_ENCRYPTIONTYPE];
			gbInnerModuleProtocolVersion = gbInnerPackRxDataCopiedBuf[PACK_F_PROTOCOLVERSION];
#else 
			gbInnerModuleProtocolVersion =0x00;
			gbInnerEncryptionType = 0x00;
#endif 
			if((DataCompare(&gbInnerPackRxDataCopiedBuf[PACK_F_DEVICEID], 0xFF, 4) == _DATA_IDENTIFIED)
				|| (DataCompare(&gbInnerPackRxDataCopiedBuf[PACK_F_DEVICEID], 0x00, 4) == _DATA_IDENTIFIED))
			{
				// 아이레보 통신팩 (TRX/RX)일 경우 ID가 모두 FF이거나 00일 경우 지그를 통해 오류 알림
				CheckResult[0] = 3; // ID 오류 
			}
			else
			{
				CheckResult[0] = 1; // 성공 
			}
			CheckResult[1] = INNERPACK_IREVO_PACK;	
			break;

#ifdef	BLE_N_SUPPORT
		case PACK_TYPE_IREVO_BLEN:
			gfInnerPackTypeiRevoBleN = 1;
			RetVal = PACK_TYPE_IREVO_BLEN;
			CheckResult[0] = 1; // 성공 
			CheckResult[1] = INNERPACK_IREVO_BLE_N_PACK;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			gbInnerEncryptionType = gbInnerPackRxDataCopiedBuf[PACK_F_ENCRYPTIONTYPE];
			gbInnerModuleProtocolVersion = gbInnerPackRxDataCopiedBuf[PACK_F_PROTOCOLVERSION];
#else 
			gbInnerModuleProtocolVersion = 0;
			gbInnerEncryptionType = 0x00;
#endif 

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			if(gbInnerModuleProtocolVersion >= 0x29)
			{
				gfInnerPackTypeiRevoBleN30 = 1;
			}
#endif	// DDL_CFG_BLE_30_ENABLE
			break;
#endif

		case PACK_TYPE_CBA_BLE:
			gfInnerPackTypeCBABle = 1;
			RetVal = PACK_TYPE_CBA_BLE;
			gbInnerEncryptionType = gbInnerPackRxDataCopiedBuf[PACK_F_ENCRYPTIONTYPE];
			gbInnerModuleProtocolVersion = gbInnerPackRxDataCopiedBuf[PACK_F_PROTOCOLVERSION] ;
			CheckResult[0] = 1; // 성공 
			CheckResult[1] = INNERPACK_IREVO_CBA_BLE_PACK;			

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			if(gbInnerModuleProtocolVersion >= 0x29)
			{
				gfInnerPackTypeiRevoBleN30 = 1;
			}
#endif 			
			break;

		default: //Zwave or zigbee
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)	
			gbInnerEncryptionType = gbInnerPackRxDataCopiedBuf[PACK_F_ENCRYPTIONTYPE];
			gbInnerModuleProtocolVersion = gbInnerPackRxDataCopiedBuf[PACK_F_PROTOCOLVERSION];			
#else 
			gbInnerModuleProtocolVersion = 0;
			gbInnerEncryptionType = 0x00;
#endif 
			gfInnerPackTypeZBZWModule = 1;
			CheckResult[0] = 1;
			CheckResult[1] = INNERPACK_IREVO_ZWAVE_PACK;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			if(gbInnerModuleProtocolVersion >= 0x29)
			{
				gfInnerPackTypeiRevoBleN30 = 1;
			}
#endif 			
			break;	
	}

#if (DDL_CFG_BLE_30_ENABLE == 0x29)
	gbInnerModuleProtocolVersion = 0x00;
	gbInnerEncryptionType = 0x00;
#endif 

	JigInputDataSave(0x02,CheckResult,2);
	return (RetVal);
}


void InnerPackIDCheck(BYTE *pDeviceIdBuf)
{
#ifdef	DDL_CFG_ADD_REMOCON_LINK_FUNCTION
	// 리모컨 등록 신호 전송을 위해 무조건 ID Confirm 처리
	gbInnerModuleMode = PACK_ID_CONFIRMED; 

#else
	BYTE SavedIdBuf[5];
	
	if(gfInnerPackTypeiRevo || gfInnerPackTypeiRevoBleN)
	{
		RomRead(SavedIdBuf, INNER_PACK_ID, 4);
		
		if(memcmp(SavedIdBuf, pDeviceIdBuf, 4) == 0)
		{
			gbInnerModuleMode = PACK_ID_CONFIRMED; 
		}
		else
		{
			gbInnerModuleMode = PACK_ID_NOT_CONFIRMED; 
		}
	}
	else
	{
		gbInnerModuleMode = PACK_ID_CONFIRMED; 
	}
#endif
}


void InnerPackDeviceCheck(void)
{
	InnerPackIDCheck(&gbInnerPackRxDataCopiedBuf[PACK_F_DEVICEID]);
#if 0 // pack 제거시 UI 적으로 아무것도 안한다.
	if(AlarmStatusCheck() == STATUS_SUCCESS)		return;
	if(GetMainMode())		return;
	
	if(GetBuzPrcsStep())	return;
	if(GetVoicePrcsStep())	return;
	if(JigModeStatusCheck() == STATUS_SUCCESS)	return;
	
	//FeedbackModeClear();
#endif 	
}

void SetInnerPackProcessResult(BYTE bResult)
{
	gInnerPackProcessResult = bResult;
}

// Power Down 모드 실행 전에 PACK 모듈 장착이 되어 있지 않은 상태면 해당 UART Pin Analog로 설정하기 위해 내용 추가
void ChangeInnerPackPinToAnalog(void)
{
	if((!P_BT_WAKEUP_T) && (gbInnerModuleMode == 0))
	{
	 	OffGpioInnerPack();
		InnerPackModuleModeClear();
	}
}
#endif 
