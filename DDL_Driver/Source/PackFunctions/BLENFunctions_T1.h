
#ifndef __BLENFUNCTIONS_T1_INCLUDED
#define __BLENFUNCTIONS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"

#ifdef	BLE_N_SUPPORT

extern BYTE gfBLENAllLockOut; 

//=============================================================//
//	BLE N EVENT
//=============================================================//
#define EV_IREVO_SUB_EVENT						0x74


//=============================================================//
//	SUB EVENT FOR BLE N
//=============================================================//
#define SUBEV_START_SCAN						0x01	
#define SUBEV_STOP_SCAN							0x03	
#ifdef	__FRONT_BLE_CONTROL__
#define SUBEV_START_STOP_FRONT_BLE				0x04
#define SUBEV_DATA_START_MODULE				0x00
#define SUBEV_DATA_STOP_MODULE				0x01
#define SUBEV_DATA_CHECK_MODULE				0x02
#endif 


#define SUBEV_GET_REGISTERD_CREDENTIAL			0xB1
#define SUBEV_GET_SETTING						0xB2
#define SUBEV_ALL_CREDENTIAL_LOCKOUT			0xB7
#define SUBEV_CHANGE_CREDENTIAL_REGISTRATION	0xB4

#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
#define SUBEV_GET_CREDENTIAL_SLOT_WITH_SCHEDULE 0xD1


#define SUBEV_GET_SETTING_SUB_EVENT				0xD2
#define D2_ADVANCED_MODE_ENABLE			0x01
#define D2_ONETIME_PINCODE_SUPPORT		0x02
#define D2_ONETIME_PINCODE_SET			0x04
#define D2_VISITOR_PINCODE_SUPPORT		0x08
#define D2_VISITOR_PINCODE_SET			0x10
#define D2_AUTO_RELOCK_SUPPORT			0x20
#define D2_AUTO_RELOCK_ENABLE			0x40
#define D2_AUTO_RELOCK_SOFT_MODE		0x80

#define D2_AUTO_RELOCK_TIME_SET_SUPPORT	0x01
#define D2_DOUBLE_CHECK_LOCK_SUPPORT	0x02
#define D2_DOUBLE_CHECK_LOCK_ENABLE		0x04
#define D2_DOUBLE_CHECK_LOCK_SOFTMODE	0x08
#define D2_EMERGENCY_UNLOCK_SUPPORT		0x10
#define D2_EMERGENCY_UNLOCK_ENABLE		0x20
#define D2_EMERGENCY_UNLOCK_SOFT_MODE	0x40
#define D2_TBD1							0x40

#define D2_TYPE_PINCODE		0x01
#define D2_TYPE_CARD		0x02
#define D2_TYPE_FINGER		0x04
#define D2_TYPE_IBUTTON		0x08
#define D2_TYPE_FACE		0x10
#define D2_TYPE_IRIS		0x20
#define D2_TYPE_RESERVE1	0x40
#define D2_TYPE_RESERVE2	0x80


#define SUBEV_GET_SUPPORTED_LANGUAGES			0xD3
#define	D3_SUPPORT_KOREAN		0x0001 
#define	D3_SUPPORT_ENGLISH		0x0002
#define	D3_SUPPORT_CHINESE		0x0004
#define	D3_SUPPORT_PORTUGUESE	0x0008
#define	D3_SUPPORT_SPANISH		0x0010
#define	D3_SUPPORT_TAIWANESE	0x0020
#define	D3_SUPPORT_TURKISH		0x0040
#define	D3_SUPPORT_RUSSIAN		0x0080
#define	D3_SUPPORT_FRANCE		0x0100
#define	D3_SUPPORT_JAPANESE		0x0200
#define	D3_SUPPORT_HINDI		0x0400
#define	D3_SUPPORT_TDB1			0x0800
#define	D3_SUPPORT_TDB2			0x1000
#define	D3_SUPPORT_TDB3			0x2000
#define	D3_SUPPORT_TDB4			0x4000
#define	D3_SUPPORT_TDB5			0x8000
#endif

#define SUBEV_GET_COMPARE_SAVED_MODULE_ID	0x75
#define SUBEV_GET_LOCK_OPERATION_MODE			0x76


//=============================================================//
//	BLE N Credential Type
//=============================================================//

#define BLEN_PINCODE	0x00
#define BLEN_CARD		0x01
#define BLEN_FINGER		0x02
#define BLEN_FACE		0x03
#define BLEN_IBUTTON		0x04
#define BLEN_IRIS			0x05
#define BLEN_RF_BLE		0x10

#define SAME_WITH_SAVED_ONE 		0x01
#define DIFFERENT_WITH_SAVED_ONE 	0x02

#define BLE_CREDENCIAL_REGISTER		0x00
#define BLE_CREDENCIAL_REG_CANCLE		0xFF



#undef FALSE
#define FALSE               0

#undef TRUE
#define TRUE                1


extern BYTE gbBLENTmpData[20];

#ifdef	__FRONT_BLE_CONTROL__
extern BYTE gbFrontBLENStatus;
extern BYTE gbFrontBLENStatusCheckTimer100ms;
#endif 



void BLENSubCommandProcess(BYTE bSubEvent);
void BLEN_Module_Test_Start(BYTE bType);
void BLEN_Module_App_Test_Start(uint8_t BleAppTestKeyVal);
void BLENTxStartScanSend(BYTE bSource);
void BLENTxStopScanSend(void);
void BLENTxIdCompareDatasend(BYTE bSource);
void BLENTxGetSettingNackSend(void);
void BLENTxChangeCredentialRegistrationMode(BYTE bType , BYTE bSlotNumber);
void BLENTxModeChangeSubEventSend(BYTE mode); //hyojoon.kim_BLE
BYTE BLENCredentialCancleCheck(BYTE bType);


BYTE AllLockOutStatusCheck(BYTE bSlotNumber);//hyojoon.kim 
void LoadAllLockOutStatus(void);//hyojoon.kim 
void SetAllLockOutStatus(BYTE OnOff);
#ifdef	__FRONT_BLE_CONTROL__
BYTE InnerPackFrontBLENProcess(void);
void FrontBLENStartStopAckProcess(BYTE command , BYTE data);
void BLENTxFrontBLEStartStopCheckSend(BYTE command);
#endif


BYTE PackRxBleNCmdProcess(void);	
BYTE InnerPackRxBleNCmdProcess(void);	


BYTE GetCurrentOperationMode(void);

#endif		// BLE_N_SUPPROT


#endif

