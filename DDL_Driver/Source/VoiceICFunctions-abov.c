/**************************************************************************//**
 * @file		: VoiceICFunctions.c
 * @brief 		: 음성 IC 셋팅 및 동작 부분
 * @author iRevo
 * @version 0.1.0
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2012 iRevo Company, http://www.iRevo.com</b>
 ******************************************************************************
 *  History
 *
 *
 *	// 2012. 10.12 심현진 책임 2차 작업
 *	// 2012. 09.01 박재현 책임 1차 작업
 *****************************************************************************/
 

#include "Main.h"

FLAG VoiceFlag;

BYTE gbVoiceStep = 0;							//음성 출력 처리 단계
BYTE gbVoiceData[4];							//출력할 음성 데이타
BYTE gbVoiceCnt	= 0;								//Clock	신호 처리를 위한 count(1ms마다 증가)

BYTE gbVoiceReqData = 0;							//voice Setting에서 저장하는 값
BYTE gbVoiceReqOut = 0;							//voice Setting에서 저장하는 값
WORD gwVoiceTimer2ms = 0;


DWORD gdwVoiceTestCnt =	0;
BYTE gbVoiceDataPending = 0;
BYTE gbBuzSet =0;

BYTE MidiAddress;

WORD gwLanguageMode;


#define		_VOICE_TIMER_START	HAL_TIM_Base_Start_IT(gh_mcu_tim_voice);gh_mcu_tim_voice->State = HAL_TIM_STATE_BUSY
#define		_VOICE_TIMER_STOP		HAL_TIM_Base_Stop_IT(gh_mcu_tim_voice);gh_mcu_tim_voice->State = HAL_TIM_STATE_READY


/**************************************************************************//**
 *   @brief voice timer callback function
 ******************************************************************************/
void VoiceCallback(void)
{
#ifdef	P_VOICE_RST	

	gbVoiceCnt++;
	if(gbVoiceCnt > 88)
	{
		P_VOICE_CLK(0);
		P_VOICE_DATA(0);
		gbVoiceCnt = 0;
		_VOICE_TIMER_STOP;

		gbVoiceData[0] = 0;
		gbVoiceData[1] = 0;
		gbVoiceData[2] = 0;
		gbVoiceData[3] = 0;
		gfVoiceData = 0;
	}
	else
	{
		if(gbVoiceCnt < 17)
		{
			if(gbVoiceCnt & 0x01)
			{
				P_VOICE_CLK(0); 						   //Clock이 rising edge일 때 Data Setting
				if(gbVoiceData[0]&0x80)   P_VOICE_DATA(1);	   //Bit가 1일 경우 High
				else					  P_VOICE_DATA(0);	   //Bit가 0일 경우 Low

				gbVoiceData[0] <<= 1;
			}
			else  P_VOICE_CLK(1);
		}
		else if(gbVoiceCnt == 17)
		{				 
			P_VOICE_CLK(0);
			P_VOICE_DATA(0);
		}
		else if(gbVoiceCnt > 24 && gbVoiceCnt < 41)
		{
			if(gbVoiceCnt & 0x01)
			{
				P_VOICE_CLK(0); 						   //Clock이 rising edge일 때 Data Setting
				if(gbVoiceData[1]&0x80) P_VOICE_DATA(1);	   //Bit가 1일 경우 High
				else					P_VOICE_DATA(0);	   //Bit가 0일 경우 Low

				gbVoiceData[1] <<= 1;
			}
			else	P_VOICE_CLK(1);
		}			 
		else if(gbVoiceCnt == 41)
		{				 
			P_VOICE_CLK(0);
			P_VOICE_DATA(0);
		}
		else if(gbVoiceCnt > 48 && gbVoiceCnt < 65)
		{
			if(gbVoiceCnt & 0x01)
			{
				P_VOICE_CLK(0); 						   //Clock이 rising edge일 때 Data Setting
				if(gbVoiceData[2]&0x80) P_VOICE_DATA(1);	   //Bit가 1일 경우 High
				else					P_VOICE_DATA(0);	   //Bit가 0일 경우 Low

				gbVoiceData[2] <<= 1;
			}
			else	P_VOICE_CLK(1);
		}	 
		else if(gbVoiceCnt == 65)
		{				 
			P_VOICE_CLK(0);
			P_VOICE_DATA(0);
		}
		else if(gbVoiceCnt > 72)
		{
			if(gbVoiceCnt & 0x01)
			{
				P_VOICE_CLK(0); 						   //Clock이 rising edge일 때 Data Setting
				if(gbVoiceData[3]&0x80) P_VOICE_DATA(1);	   //Bit가 1일 경우 High
				else					P_VOICE_DATA(0);	   //Bit가 0일 경우 Low

				gbVoiceData[3] <<= 1;
			}
			else	P_VOICE_CLK(1);
		}	 
		

	}

#endif	
}


/**************************************************************************//**
 *   @brief 보이스 IC와 통신을 위한 타이머 셋업
 *   @param 보이스 출력 어드레스 bData 및 음성 크기 및 메너 모드  bMode
 * 	//AudioFeedback의 bMode
 *	//bit 0 : 1->매너모드에 따른 처리,	0->매너모드 무시
 *	//bit 1 : 1->설정된 볼륨에 따른 처리,	0->최고 볼륨 처리
 ******************************************************************************/
void VoiceSetting(WORD bData, BYTE bMode)
{
#ifdef	P_VOICE_RST	
	BYTE bTmp;

	bTmp = VolumeSettingCheck();

	if((bMode & MANNER_CHK ) && ((bTmp == VOL_SILENT) || gfMute)) 
	{
		return;
	}

	gbVoiceData[0] = 0;
	gbVoiceData[1] = 0;
	gbVoiceData[2] = 0;
	gbVoiceData[3] = 0;
	if(bData > 0xFE00) MidiAddress = 1;
	else               MidiAddress = 0;

	gbVoiceReqData = (BYTE)bData;
	gbVoiceReqOut = bMode;

/*	
	if((gbVoiceStep==0)&&(bData==VOICE_MIDI_BUTTON)){
		P_VOICE_CLK(0);
		P_VOICE_DATA(0);
		gbVoiceCnt = 0;
		NVIC_DisableIRQ(TIMER0_IRQn);
		gwVoiceTimer2ms = 10;
		gbVoiceStep = VOICE_VOLUME_CHECK;
	}
	else{
		gbVoiceStep = VOICE_RESET_ON;
	}
*/	
	gbVoiceStep = VOICE_RESET_ON;
	gwVoiceTimer2ms = 0;

#endif
}


/**************************************************************************//**
 *   @brief 보이스 IC 초기화 부분 Reset 신호 10ms 이상- 정전기 이후 동작 안정화 부분
 ******************************************************************************/
void VoiceICInitial(void)
{
#ifdef	P_VOICE_RST	

	P_VOICE_F_HOLD(1);

	P_VOICE_RST(0);
	P_VOICE_DATA(0);
	P_VOICE_CLK(0);

	Delay(SYS_TIMER_10MS);		// 10ms Reset  -- 정전기 이후 20ms 정도 reset 시간을 주어야 되 살아 남

	gfVoiceOut = 0;
	gfVoiceReq = 0;
	gfVoiceData = 0;
	gfVoiceNew = 0;

	P_VOICE_RST(1);

#endif
}

#if (defined	(DDL_CFG_LANGUAGE_SET_TYPE1) || defined	(DDL_CFG_LANGUAGE_SET_TYPE2) || defined	(DDL_CFG_LANGUAGE_SET_TYPE3)) && defined (AVML_MSG_T2)
// AVML_MSG_T2 선어 되면 2016년 하반기 voice IC 인데 
//각 언어별 35 번 index 밖에 없음 
// 현재 언어 모드에 상관 없이 각국의 언어를 선택하면 각국의 언어로 나오도록 gbVoiceData[1] 값을 변경 하는 함수
void LanuageSelecteVoiceGuideIndexChange(void)
{
	switch (gbVoiceReqData)
	{
		case AVML_CHG_KOREAN_SHARP:
			gbVoiceData[1] =(BYTE)( _KOREAN_MODE >> 8);
			gbVoiceReqData = AVML_CHG_KOREAN_SHARP; // 35 번  
			break;
	
		case AVML_CHG_ENGLISH_SHARP:
			gbVoiceData[1] =(BYTE)( _ENGLISH_MODE >> 8);
			gbVoiceReqData = AVML_CHG_KOREAN_SHARP; // 35 번
			break;

		case AVML_CHG_CHINESE_SHARP:
			gbVoiceData[1] = (BYTE)(_CHINESE_MODE >> 8);
			gbVoiceReqData = AVML_CHG_KOREAN_SHARP; // 35 번		
			break;

		case AVML_CHG_SPANISH_SHARP:
			gbVoiceData[1] = (BYTE)(_SPANISH_MODE >> 8);
			gbVoiceReqData = AVML_CHG_KOREAN_SHARP;	// 35 번
			break;

		case AVML_CHG_PORTGUESE_SHARP:
			gbVoiceData[1] = (BYTE)(_PORTUGUESE_MODE >> 8);
			gbVoiceReqData = AVML_CHG_KOREAN_SHARP;	// 35 번
			break;

		case AVML_CHG_TAIWANESE_SHARP:
			gbVoiceData[1] = (BYTE)(_TAIWANESE_MODE >> 8);
			gbVoiceReqData = AVML_CHG_KOREAN_SHARP;	// 35 번
			break;

		default : //위 음원을 제외한 나머지 것들 
			gbVoiceData[1] = (BYTE)(gwLanguageMode>>8);
			break;
	}
}
#endif 

/**************************************************************************//**
 *   @brief 보이스 IC 동작 부분 : 리셋 + 음성 레벨 + 음성 어드레스 + 비지 체크
 *
 ******************************************************************************/
void VoiceDataProcess(void)
{
//	gbVoiceStep = 0;
#ifdef	P_VOICE_RST	

	WORD wTmp;
	BYTE	bVol;

	switch(gbVoiceStep)
	{
		case 0:
			break;

		/* 음성 출력 전 리셋 해줌 - 정전기 대책  */
		case VOICE_RESET_ON:
				_VOICE_TIMER_STOP;			// disable voice TIM (and disable INT)
				P_VOICE_RST(0);
				P_VOICE_DATA(0);
				P_VOICE_CLK(0);
				gwVoiceTimer2ms = 2;
				gbVoiceDataPending = 0;
				gbVoiceStep = VOICE_RESET_OFF;
			break;

		/* 리셋 후 40ms 안전화 시간 설정   */
		case VOICE_RESET_OFF:
			if(gwVoiceTimer2ms) break;

			P_VOICE_RST(1);
//			gwVoiceTimer2ms = 20;			// Reset 후  IC  안정 시간  40ms
			gwVoiceTimer2ms = 10;			// Reset 후  IC  안정 시간	20ms
			gbVoiceStep = VOICE_VOLUME_CHECK;

			break;
		/* 볼륨 측정 - 스위치 /메모리 형태   */
		case VOICE_VOLUME_CHECK:
			if(gwVoiceTimer2ms) break;

			if( P_VOICE_BUSY_T )
				break;

			if (gbVoiceReqOut & VOL_HIGH ) {
				bVol = _VOICE_HIGH;
			}
			else {
				wTmp = VolumeSettingCheck();
				if ( wTmp == VOL_HIGH ) {
					bVol = _VOICE_HIGH;
				}
				else {		// low or silent is low
					bVol = _VOICE_LOW;
				}
			}

			gbVoiceData[0] = bVol;		// send 1st data : Voice level
			if(MidiAddress == 1){
				gbVoiceData[1] = 0x00;
			}
			else
			{
#if (defined	(DDL_CFG_LANGUAGE_SET_TYPE1) || defined	(DDL_CFG_LANGUAGE_SET_TYPE2) || defined	(DDL_CFG_LANGUAGE_SET_TYPE3)) && defined (AVML_MSG_T2)
				LanuageSelecteVoiceGuideIndexChange();
#else 
				gbVoiceData[1] = (BYTE)(gwLanguageMode>>8);
#endif 
			}
			gbVoiceData[2] = gbVoiceReqData;			//send  2nd data  : Voice Address
			gbVoiceData[3] = (gbVoiceData[0] ^ gbVoiceData[1] ^ gbVoiceData[2]);
			gfVoiceData = 1;
			gbVoiceCnt = 0;
			gwVoiceTimer2ms = 0;
			gbVoiceStep = VOICE_DATA_SEND;
			gbVoiceReqData = 0;
			break;

		/* data 전송 전 싱크 신호 전달 20ms low  */
		case VOICE_SYNCH_SEND:
			if(gwVoiceTimer2ms) break;

			P_VOICE_CLK(1); 							//Wakeup Synch Signal : 20ms

//			gwVoiceTimer2ms = 10;
			gwVoiceTimer2ms = 5;
			gbVoiceStep = VOICE_DATA_SEND;
			break;

		/* 8bit data 전송 */
		case VOICE_DATA_SEND:
			if(gwVoiceTimer2ms)	break;

			_VOICE_TIMER_START;

			gbVoiceStep = VOICE_BUSY_CHECK;
			break;
		/* 음성 출력 상태 체크  */
		/* 비어 있는 어드레스는 busy low*/
		case VOICE_BUSY_CHECK:
			if(gfVoiceData)	break;		//  data 전송 완료

			if(P_VOICE_BUSY_T)	// 정상 출력
			{
				gbVoiceStep = VOICE_END_WAIT;
				gwVoiceTimer2ms = 11000 ;  			// 최대 음성 출력 시간 설정  16초
				gfVoiceOut = 1;
				gbVoiceDataPending = 1;
			}
			else											// 비정상 적인
			{
				if(gbVoiceDataPending==0)
				{
					gbVoiceStep = VOICE_VOLUME_CHECK;
					P_VOICE_CLK(0);
					gwVoiceTimer2ms = 2;
					gbVoiceDataPending = 1;
					gfVoiceData = 1;
				}
				else										// 3rd 보이스 어드레스 전송 후 low 보이스 IC 리셋
				{
					gbVoiceStep = VOICE_STABLE_WAIT;
					VoiceICInitial();
					break;
				}
			}

			break;
		/* 음성 끝나는 부분 체크 */
		case VOICE_END_WAIT:
			if(gwVoiceTimer2ms ==0) 			// 음성 출력이 없을 시 릴 셋 전송
			{
				gbVoiceStep = VOICE_STABLE_WAIT;
				VoiceICInitial();
				break;
			}

			if(P_VOICE_BUSY_T)	break;
			gbVoiceStep = VOICE_STABLE_WAIT;
			gwVoiceTimer2ms=0;
			break;

		case VOICE_STABLE_WAIT:
		default:
			if(gwVoiceTimer2ms) break;
			gbVoiceStep = 0;
			gfVoiceOut = 0;
			gbVoiceDataPending= 0;            
            		MidiAddress = 0;
			break;

	}
#endif	
}


BYTE GetVoicePrcsStep(void)
{
	return (gbVoiceStep);
}


void LanguageSetting(BYTE bMode) 
{
	BYTE bTmp;
	BYTE bTmp1;

	RomRead(&bTmp, (WORD)LANG_MODE, 1);
	
	bTmp1 = (bTmp & 0xF0);

	switch(bMode)
	{
		case LANGUAGE_KOREAN:
			gwLanguageMode = _KOREAN_MODE;
			break;

		case LANGUAGE_CHINESE:
			gwLanguageMode = _CHINESE_MODE;
			break;

		case LANGUAGE_ENGLISH:
			gwLanguageMode = _ENGLISH_MODE;
			break;

		case LANGUAGE_SPANISH:
			gwLanguageMode = _SPANISH_MODE;
			break;

		case LANGUAGE_PORTUGUESE:
			gwLanguageMode = _PORTUGUESE_MODE;
			break;

		case LANGUAGE_TAIWANESE:
			gwLanguageMode = _TAIWANESE_MODE;
			break;
			
		default:
			gwLanguageMode = _KOREAN_MODE;
			break;
	}

	bTmp = ((BYTE)(gwLanguageMode >> 8) | bTmp1);
	RomWrite(&bTmp, (WORD)LANG_MODE, 1);

#if 1	
	// 언어 설정이 필요 없는 제품도 중국과 다른 지역 FW를 나누어 관리하지 않도록 하기 위해 공용 처리
	if(gwLanguageMode == _CHINESE_MODE)
		gPinInputMinLength = 6;
	else 
		gPinInputMinLength = 4;

#else
#ifdef	P_VOICE_RST // defined (DDL_CFG_SUPPORT_CHINESE)
	if(gwLanguageMode == _CHINESE_MODE)
		gPinInputMinLength = 6;
	else 
		gPinInputMinLength = 4;
#endif 
#endif	
}


void LanguageLoad(void)
{
	BYTE bTmp;
	
	RomRead(&bTmp, (WORD)LANG_MODE, 1);

	bTmp = (bTmp & 0x0F);

#if 1	
	// 언어 설정이 필요 없는 제품도 중국과 다른 지역 FW를 나누어 관리하지 않도록 하기 위해 공용 처리
	if(bTmp >= (_KOREAN_MODE >> 8) && bTmp <= (_TAIWANESE_MODE  >> 8 )) //ROM 값이 vaild 하면 그 값으로 셋팅 
	{
		gwLanguageMode = (WORD)bTmp << 8;
	}
	else // ROM 이 비어 있으면 언어별 default 로 
	{
		// 비밀번호 오동작 방지를 위해 언어 설정이 되지 않으면 무조건 한국어로 설정 
		gwLanguageMode = _KOREAN_MODE; 
	}

	if(gwLanguageMode == _CHINESE_MODE)
		gPinInputMinLength = 6;
	else 
		gPinInputMinLength = 4;

#else		
#if	defined (LOCKSET_LANGUAGE_SET)	
	if(bTmp >= (_KOREAN_MODE >> 8) && bTmp <= (_TAIWANESE_MODE  >> 8 )) //ROM 값이 vaild 하면 그 값으로 셋팅 
	{
		gwLanguageMode = (WORD)bTmp << 8;
	}
	else // ROM 이 비어 있으면 언어별 default 로 
	{
#ifdef	DDL_CFG_LANGUAGE_SET_TYPE1	
		// 한국어, 중국어 음성 지원 (중국 생산으로 중국어 기본)
		gwLanguageMode = _CHINESE_MODE; 
#elif defined  DDL_CFG_LANGUAGE_SET_TYPE2
		// 한국어, 중국어, 영어, 스페인어, 포르투갈어, 대만어  음성 지원 (영어 기본)
		gwLanguageMode = _ENGLISH_MODE; 
#else 
		// 한국어, 중국어 음성 지원 (한국 생산으로 한국어 기본)
		gwLanguageMode = _KOREAN_MODE; 
#endif 
	}
#else 	// defined (LOCKSET_LANGUAGE_SET)
	// 음성 설정 기능이 없으면 한국어 기본
	gwLanguageMode = _KOREAN_MODE;
#endif // defined (LOCKSET_LANGUAGE_SET)

#ifdef	P_VOICE_RST // defined (DDL_CFG_SUPPORT_CHINESE)
	if(gwLanguageMode == _CHINESE_MODE)
		gPinInputMinLength = 6;
	else 
		gPinInputMinLength = 4;
#endif
#endif
}

void LanguageSetDefault(void)
{
	BYTE bTmp;
	BYTE bTmp1;
	
	RomRead(&bTmp, (WORD)LANG_MODE, 1);

	bTmp1 = ((bTmp >> 4) & 0x0F);
	bTmp = (bTmp & 0xF0)  | bTmp1;

	RomWrite(&bTmp, (WORD)LANG_MODE, 1);
	LanguageLoad();
}

WORD LanguageCheck(void)
{
	return (gwLanguageMode);
}


uint8_t ChineseGatemanCheck(void)
{
#if defined	(VOICE_OPTION)
	uint8_t bTmp;

	RomRead(&bTmp, (WORD)VOICE_OPTION, 1);

	if((bTmp == 0x33) && (gwLanguageMode == _CHINESE_MODE)) 	return 1;
#endif	

	return 0;
}




