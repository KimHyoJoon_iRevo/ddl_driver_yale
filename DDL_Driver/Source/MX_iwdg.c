#define	_MX_IWDG_C_

#include "main.h"

/*
	ST23L151의 IWDG는 MCU내부 LSI(37kHz)를 사용한다.
	IWDG를 사용하면 그리 정밀한 시간관리가 필요치 않기 때문에 제일 큰 prescaler를 설정해 사용한다.

	LSI		= 37 kHz
	Precaler	= 256
	초당 count = 37000 / 256 = 144.5
*/

void		SetIwdgPeriod( uint32_t count )
{
	gh_mcu_iwdg->Instance = IWDG;
	gh_mcu_iwdg->Init.Prescaler = IWDG_PRESCALER_256;
	gh_mcu_iwdg->Init.Reload = count;
	HAL_IWDG_Init( gh_mcu_iwdg );
}


void		StartIwdg( void )
{
		HAL_IWDG_Start( gh_mcu_iwdg );
}

void		RefreshIwdg( void )
{
		HAL_IWDG_Refresh( gh_mcu_iwdg );
}


