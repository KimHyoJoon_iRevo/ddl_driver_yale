#ifndef		_MX_I2C_LAPPER_H_
#define		_MX_I2C_LAPPER_H_

//ddl_i2c_t의 변수 type의 내용 정의
// DDL_I2C_MCU_TYPE1 : EEPROM, RTC I2C 통신을 위한 모드
enum	{
	DDL_I2C_NONE = 0,
	DDL_I2C_MCU = 1,
	DDL_I2C_GPIO = 2,
	DDL_I2C_MCU_TYPE1 = 3,
};

typedef	struct {
	uint16_t		type;		// type : gpio or mcu
	uint8_t		slave_id;	// I2C slave id		( Form = bit7~bit1:id, bit 0:cmd )
	uint8_t		flag;		// flag : reserved

	union {
		struct {
			I2C_HandleTypeDef	*hi2c;
		} mcu;

		struct {
			//SCL의 gpio port 및 pin
			GPIO_TypeDef		*SCL_GPIOx;
			uint16_t			SCL_GPIO_Pin;
		
			//SDA의 gpio port 및 pin
			GPIO_TypeDef		*SDA_GPIOx;
			uint16_t			SDA_GPIO_Pin;
		} gpio;
	} dev;
}	ddl_i2c_t;


#define	I2C_CMD_RD			1
#define	I2C_CMD_WR			0

#define	I2C_ACK				0
#define	I2C_NACK			1

#define	I2C_NORMAL_NACK		0
#define	I2C_IGNORE_NACK		1

#define	I2C_PIN_LOW			0
#define	I2C_PIN_HIGH		1


//아래 값은 CPU가 16MHz로 동작할 때, GPIO I2C clk 이 100kHz로 동작하는 값이다.
#define	GPIO_I2C_DELAY		6

#define	MCU_I2C_TIMEOUT		5


// Serial EEPROM(24C08) Device Address
#define	I2C_EEPROM_ADDRESS		0xA0


#ifdef		_MX_I2C_LAPPER_C_
#else
#endif

void	ddl_i2c_write( ddl_i2c_t *ddl_i2c, uint16_t address, uint16_t addsize, uint8_t *data, uint16_t length );
void	ddl_i2c_read( ddl_i2c_t *ddl_i2c, uint16_t address, uint16_t addsize, uint8_t *data, uint16_t length );

void ddl_i2c_error_repair(void);


#endif
