//------------------------------------------------------------------------------
/** 	@file	CommPack_UART.h
	@brief	UART functions for communication pack module
*/
//------------------------------------------------------------------------------


#ifndef		_INNERPACK_UART_H_
#define		_INNERPACK_UART_H_

#include "DefineMacro.h"
//#include "DefinePin.h"


#define		MCU_INNERPACK_UART		USART2


#define		INNERPACK_RX_BUFF_LENGTH		256

#define		INNERPACK_NUM_TX_BUFF		2
#define		INNERPACK_TX_BUFF_LENGTH		300


void		SetupInnerPack_UART( void );
void 		OffGpioInnerPack( void );
void		RegisterInnerPack_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) );
void		InnerPackRxInterruptRequest( void );
uint16_t	InnerPack_DequeueRx( BYTE *buff, uint16_t length );
uint16_t	UartSendInnerPack( BYTE *data, uint16_t length );


void InnerPack_uart_rxcplt_callback(UART_HandleTypeDef *huart);
void InnerPack_uart_txcplt_callback(UART_HandleTypeDef *huart);

#endif

