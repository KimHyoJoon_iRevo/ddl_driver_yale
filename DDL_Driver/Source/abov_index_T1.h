#ifndef		_ABOV_INDEX_T1_H_

#define		_ABOV_INDEX_T1_H_

#define		AVML_MSG_T1_001			1				// 완료되었습니다.
#define		AVML_MSG_T1_002			2				// 메뉴 선택 / 번호를 입력하세요.
#define		AVML_MSG_T1_003			3				// 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요 / 카드 등록을 원하시면 카드를 리더기에 입력하세요.
#define		AVML_MSG_T1_004			4				// 추가로 등록할 카드키를 차례대로 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요
#define		AVML_MSG_T1_005			5				// 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요 / 지문 등록을 원하시면 원형 아이콘을 누르세요.
#define		AVML_MSG_T1_006			6				// 비밀번호 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_007			7				// 비밀번호 등록은 1번, 비밀번호 삭제는 3번을 눌러주세요.
#define		AVML_MSG_T1_008			8				// 개별 비밀번호 관리를 위하여 사용자 번호 두자리를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_009			9				// 비밀번호 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_010			10				// 방문자 비밀번호 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_MSG_T1_011			11				// 등록하실 방문자 비밀번호 네자리를 입력하세요 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_012			12				// 일회용 비밀번호 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_013			13				// 일회용 비밀번호 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_014			14				// 등록하실 일회용 비밀번호 네자리를 입력하세요;. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_015			15				// 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_016			16				// 비밀번호 삭제를 원하시면 등록된 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_017			17				// 설정모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_018			18				// 모드 변경은 1번 / 언어 변경은 2번
#define		AVML_MSG_T1_019			19				// 일회용 비밀번호 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_020			20				// 리모컨 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_MSG_T1_021			21				// 리모컨 등록은 1번, 리모컨 삭제는 3번을 눌러주세요.
#define		AVML_MSG_T1_022			22				// 리모컨 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_023			23				// 리모컨에  있는 등록버튼을 누르세요.
#define		AVML_MSG_T1_024			24				// 리모컨 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_025			25				// 홈네트워크 등록/삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_026			26				// 홈네트워크 등록은 1번, 홈네트워크 삭제는 3번을 눌러주세요.
#define		AVML_MSG_T1_027			27				// 홈네트워크 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_028			28				// 홈네트워크를 설정대기 모드로 전환해주세요.
#define		AVML_MSG_T1_029			29				// 홈네트워크 삭제모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_030			30				// 마스터 모드 전환 메뉴 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_MSG_T1_031			31				// 네자리에서 열자리의 마스터 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_032			32				// 입력하신 마스터 비밀번호를 다시 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요
#define		AVML_MSG_T1_033			33				// 언어 설정 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_034			34				// 언어 설정모드 입니다. / 번호를 입력하세요.
#define		AVML_MSG_T1_035			35				// 한국어 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_036			36				// 중국어 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_037			37				// 스페인어 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_038			38				// 포르투갈어 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_039			39				// 완료되었습니다. / 다른 설정을 하시려면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_MSG_T1_040			40				// 완료되었습니다. / 다른 설정을 하시려면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.

#define		AVML_MSG_T1_041			41				// 완료되었습니다. / 추가 등록을 원하시면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_MSG_T1_042			42				// 완료되었습니다. / 추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_MSG_T1_043			43				// 비밀번호 등록은 1번, 비밀번호 삭제는 3번을 누르세요.
#define		AVML_MSG_T1_044			44				// 지문이 한번 입력 되었습니다. 동일 지문을 다시 입력하세요.
#define		AVML_MSG_T1_045			45				// 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_046			46				// 마스터 비밀번호 변경모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_047			47				// 입력하신 비밀번호를 다시 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요
#define		AVML_MSG_T1_048			48				// 비밀번호 삭제를 원하시면 마스터코드를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_049			49				// 삭제를 원하시는 사용자 번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_050			50				// 마스터 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요

#define		AVML_MSG_T1_051			51				// 입력하신 마스터 비밀번호를 다시 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_052			52				// 완료되었습니다. / 추가 삭제를 원하시면 원형 아이콘을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_MSG_T1_053			53				// 비밀번호 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_054			54				// 전체삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_055			55				// 전체삭제를 위해서 마스터 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_056			56				// 일반 모드 전환 메뉴 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_057			57				// 카드키 전체삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_058			58				// 완료되었습니다. / 추가 삭제를 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요.
#define		AVML_MSG_T1_059			59				// 카드키 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_060			60				// 카드키 등록은 1번, 카드키 삭제는 3번을 누르세요.

#define		AVML_MSG_T1_061			61				// 카드키 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_062			62				// 카드키가 등록 되었습니다. / 카드키를 카드키 리더기에 접촉해 주세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_063			63				// 카드키를 카드키 리더기에 접촉해 주세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_064			64				// 카드키 삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_065			65				// 지문 등록 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_066			66				// 지문삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_067			67				// 지문 등록/삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_068			68				// 지문 등록은 1번, 지문 삭제는 3번을 누르세요.
#define		AVML_MSG_T1_069			69				// 지문 삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_070			70				// 지문이 두번 입력 되었습니다. 동일 지문을 다시 입력하세요.

#define		AVML_MSG_T1_071			71				// 한 개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_072			72				// 두개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_073			73				// 세개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_074			74				// 네개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_075			75				// 다섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_076			76				// 여섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_077			77				// 일곱개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_078			78				// 여덟개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_079			79				// 아홉개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_080			80				// 열개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.

#define		AVML_MSG_T1_081			81				// 열한개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_082			82				// 열두개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_083			83				// 열세개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_084			84				// 열네개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_085			85				// 열다섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_086			86				// 열여섯개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_087			87				// 열일곱개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_088			88				// 열여덟개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_089			89				// 열아홉개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_090			90				// 스무개 / 지문이 등록 되었습니다. / 등록하실 지문을 차례로 세번 입력하세요.

#define		AVML_MSG_T1_091			91				// 넘버원 프라이드 게이트맨 입니다.
#define		AVML_MSG_T1_092			92				// 네자리에서 열자리의 마스터 비밀번호를 입력하세요.
#define		AVML_MSG_T1_093			93				// 마스터 비밀번호가 설정되었습니다.
#define		AVML_MSG_T1_094			94				// 문이 닫혀있는지 확인하세요
#define		AVML_MSG_T1_095			95				// 좌수 우수 상태를 확인하고 있습니다.
#define		AVML_MSG_T1_096			96				// 문이 잠겼습니다.
#define		AVML_MSG_T1_097			97				// 설정이 완료 되었습니다
#define		AVML_MSG_T1_098			98				// 이제 도어록 사용이 가능합니다.
#define		AVML_MSG_T1_099			99				// 도어록 센서에 이상이 있습니다
#define		AVML_MSG_T1_100			100				// 데드볼트는 동작했으나, 문이 제대로 잠기지 않았을 수 있습니다.

#define		AVML_MSG_T1_101			101				// 시간이 초과되었습니다.
#define		AVML_MSG_T1_102			102				// 건전지를 교체해주세요.
#define		AVML_MSG_T1_103			103				// 열렸습니다.
#define		AVML_MSG_T1_104			104				// 닫혔습니다.
#define		AVML_MSG_T1_105			105				// 입력하신 자릿수가 맞지 않습니다.
#define		AVML_MSG_T1_106			106				// 이미 사용하고 있는 비밀번호 입니다.
#define		AVML_MSG_T1_107			107				// 이미 사용하고 있는 사용자번호 입니다.
#define		AVML_MSG_T1_108			108				// 이미 사용하고있는 카드키 입니다.
#define		AVML_MSG_T1_109			109				// 이미 사용하고 있는 비밀번호 입니다.
#define		AVML_MSG_T1_110			110				// 외부강제잠금 상태 입니다.

#define		AVML_MSG_T1_111			111				// 내부강제잠금 상태 입니다.
#define		AVML_MSG_T1_112			112				// 비밀번호는 네 자리에서 열 자리까지 등록이 가능합니다
#define		AVML_MSG_T1_113			113				// 파손 경보가 발생하였습니다.
#define		AVML_MSG_T1_114			114				// 침입 경보가 발생하였습니다.
#define		AVML_MSG_T1_115			115				// 화제 경보가 발생하였습니다.
#define		AVML_MSG_T1_116			116				// 도어록이 정상적으로 작동되지 않습니다
#define		AVML_MSG_T1_117			117				// 통신팩이 정상적으로 작동되지 않습니다
#define		AVML_MSG_T1_118			118				// 지문인증 모듈이 정상적으로 작동되지 않습니다
#define		AVML_MSG_T1_119			119				// 지문인증 모듈을 점검하세요
#define		AVML_MSG_T1_120			120				// 카드가 정상적으로 등록되지 않았습니다.

#define		AVML_MSG_T1_121			121				// 카드가 정상적으로 등록되지 않았습니다.
#define		AVML_MSG_T1_122			122				// 지문이 정상적으로 등록되지 않았습니다.
#define		AVML_MSG_T1_123			123				// 지문인증 모듈의 이물질을 제거해 주세요.
#define		AVML_MSG_T1_124			124				// 지문센서가 훼손되었습니다.
#define		AVML_MSG_T1_125			125				// Error / 저장공간이 부족합니다.
#define		AVML_MSG_T1_126			126				// 좌수로 설정 되었습니다
#define		AVML_MSG_T1_127			127				// 우수로 설정 되었습니다
#define		AVML_MSG_T1_128			128				// 다시 시도하세요.
#define		AVML_MSG_T1_129			129				// 비밀번호가 등록 되었습니다.
#define		AVML_MSG_T1_130			130				// 마스터 비밀번호가 등록 되었습니다.

#define		AVML_MSG_T1_131			131				// 모든 설정이 완료 되었습니다.
#define		AVML_MSG_T1_132			132				// 비밀번호가 변경 되었습니다.
#define		AVML_MSG_T1_133			133				// 비밀번호가 삭제 되었습니다.
#define		AVML_MSG_T1_134			134				// 등록된 모든 지문이 삭제 되었습니다.
#define		AVML_MSG_T1_135			135				// 등록된 모든 카드키가 삭제 되었습니다.
#define		AVML_MSG_T1_136			136				// 3분락 상태 입니다. 3분후 정상작동 됩니다.
#define		AVML_MSG_T1_137			137				// 설정이 완료 되었습니다
#define		AVML_MSG_T1_138			138				// 3분락 상태 입니다. 3분이 지난 후에 다시 사용해주세요.
#define		AVML_MSG_T1_139			139				// Error / 등록이 취소 되었습니다.
#define		AVML_MSG_T1_140			140				// Error / 삭제가 취소 되었습니다.

#define		AVML_MSG_T1_141			141				// Error / 메뉴 모드가 취소 되었습니다.
#define		AVML_MSG_T1_142			142				// 음량 설정 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_143			143				// 고음은 1번, 저음은 2번, 무음은 3번을 누르세요
#define		AVML_MSG_T1_144			144				// 고음으로 설정 되었습니다 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_145			145				// 저음으로 설정 되었습니다 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_146			146				// 무음으로 설정 되었습니다 / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_147			147				// 고음
#define		AVML_MSG_T1_148			148				// 저음
#define		AVML_MSG_T1_149			149				// 무음
#define		AVML_MSG_T1_150			150				// 으로 설정 되었습니다.

#define		AVML_MSG_T1_151			151				// 해제되었습니다.
#define		AVML_MSG_T1_152			152				// 자동잠금 설정모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_153			153				// 설정은 1번, 해제는 3번을 누르세요.
#define		AVML_MSG_T1_154			154				// 설정되었습니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_155			155				// 해제되었습니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_156			156				// 자동잠금이 설정되었습니다.
#define		AVML_MSG_T1_157			157				// 자동잠금이 해제 되었습니다
#define		AVML_MSG_T1_158			158				// 좌수 우수 설정모드 입니다.
#define		AVML_MSG_T1_159			159				// 정상적으로 설정되었습니다.
#define		AVML_MSG_T1_160			160				// 등록된 모든 비밀번호를 사용할 수 없도록 설정된 상태 입니다.

#define		AVML_MSG_T1_161			161				// 마스터 비밀번호만 사용이 가능합니다.
#define		AVML_MSG_T1_162			162				// 현재 등록된 모든 비밀번호를 사용할 수 없도록 설정된 상태 입니다.
#define		AVML_MSG_T1_163			163				// 우리집 통합경비 시스템 게이트맨입니다.
#define		AVML_MSG_T1_164			164				// 통신팩 등록 모드 입니다.
#define		AVML_MSG_T1_165			165				// 통신팩 삭제 모드 입니다.
#define		AVML_MSG_T1_166			166				// 경비가 설정 되었습니다.
#define		AVML_MSG_T1_167			167				// 경비 설정이 실패하였습니다.
#define		AVML_MSG_T1_168			168				// 문 또는 창문이 열려 있습니다.
#define		AVML_MSG_T1_169			169				// 시스템 경비가 설정되었습니다.
#define		AVML_MSG_T1_170			170				// 시스템 경비가 해제되었습니다.

#define		AVML_MSG_T1_171			171				// 경비 해제에 실패하였습니다.
#define		AVML_MSG_T1_172			172				// 경비 기능이 여전히 작동중일 수 있습니다.
#define		AVML_MSG_T1_173			173				// 연동 시스템에 연결하세요
#define		AVML_MSG_T1_174			174				// 연동을 시작합니다.
#define		AVML_MSG_T1_175			175				// 연동이 활성화 되었습니다.
#define		AVML_MSG_T1_176			176				// 활성화에 실패 하였습니다.
#define		AVML_MSG_T1_177			177				// 등록하실 지문을 차례로 세번 입력하세요.
#define		AVML_MSG_T1_178			178				// Error / 지문이 정상적으로 등록되지 않았습니다. / 다시 시도하세요.
#define		AVML_MSG_T1_179			179				// 외부강제잠금 상태 입니다. / 으로 설정 되었습니다.
#define		AVML_MSG_T1_180			180				// 방문자 비밀번호 삭제 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.

#define		AVML_MSG_T1_181			181				// 블루투스키가 등록 되었습니다.
#define		AVML_MSG_T1_182			182				// 방문자 비밀번호 등록모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_183			183				// 추가로 등록할 리모콘을 차례대로 입력하세요.
#define		AVML_MSG_T1_184			184				//
#define		AVML_MSG_T1_185			185				//
#define		AVML_MSG_T1_186			186				//
#define		AVML_MSG_T1_187			187				//
#define		AVML_MSG_T1_188			188				//
#define		AVML_MSG_T1_189			189				//
#define		AVML_MSG_T1_190			190				// 한 개 / 카드키가 등록 되었습니다.

#define		AVML_MSG_T1_191			191				// 두개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_192			192				// 세개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_193			193				// 네개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_194			194				// 다섯개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_195			195				// 여섯개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_196			196				// 일곱개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_197			197				// 여덟개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_198			198				// 아홉개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_199			199				// 열개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_200			200				// 열한개 / 카드키가 등록 되었습니다.

#define		AVML_MSG_T1_201			201				// 열두개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_202			202				// 열세개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_203			203				// 열네개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_204			204				// 열다섯개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_205			205				// 열여섯개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_206			206				// 열일곱개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_207			207				// 열여덟개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_208			208				// 열아홉개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_209			209				// 스무개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_210			210				// 스물한개 / 카드키가 등록 되었습니다.

#define		AVML_MSG_T1_211			211				// 스물두개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_212			212				// 스물세개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_213			213				// 스물네개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_214			214				// 스물다섯개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_215			215				// 스물여섯개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_216			216				// 스물일곱개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_217			217				// 스물여덟개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_218			218				// 스물아홉개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_219			219				// 서른개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_220			220				// 서른한개 / 카드키가 등록 되었습니다.

#define		AVML_MSG_T1_221			221				// 서른두개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_222			222				// 서른세개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_223			223				// 서른네개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_224			224				// 서른다섯개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_225			225				// 서른여섯개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_226			226				// 서른일곱개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_227			227				// 서른여덟개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_228			228				// 서른아홉개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_229			229				// 마흔개 / 카드키가 등록 되었습니다.
#define		AVML_MSG_T1_230			230				// 계속하시려면 샵 버튼을 누르세요.

#define		AVML_MSG_T1_231			231				// 네자리에서 열자리의 마스터 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요
#define		AVML_MSG_T1_232			232				// 좌수 우수 설정 모드 입니다. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_233			233				// 샵 버튼을 누르시면 좌수 우수가 자동으로 설정됩니다.
#define		AVML_MSG_T1_234			234				// 마스터 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
#define		AVML_MSG_T1_235			235				// 네자리에서 열자리의 사용자 비밀번호를 입력하세요. / 설정을 완료하시려면 R버튼을 누르세요 / 지문 등록을 원하시면 *버튼을 누르세요.
#define		AVML_MSG_T1_236			236				//
#define		AVML_MSG_T1_237			237				//
#define		AVML_MSG_T1_238			238				//
#define		AVML_MSG_T1_239			239				//
#define		AVML_MSG_T1_240			240				//

#define		AVML_MSG_T1_241			241				//
#define		AVML_MSG_T1_242			242				//
#define		AVML_MSG_T1_243			243				//
#define		AVML_MSG_T1_244			244				//
#define		AVML_MSG_T1_245			245				//
#define		AVML_MSG_T1_246			246				//
#define		AVML_MSG_T1_247			247				//
#define		AVML_MSG_T1_248			248				//
#define		AVML_MSG_T1_249			249				//
#define		AVML_MSG_T1_250			250				//

#define		AVML_MSG_T1_251			251				//
#define		AVML_MSG_T1_252			252				//
#define		AVML_MSG_T1_253			253				//
#define		AVML_MSG_T1_254			254				//
#define		AVML_MSG_T1_255			255				//


#endif

