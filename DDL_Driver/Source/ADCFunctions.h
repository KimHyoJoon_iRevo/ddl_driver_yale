#ifndef __ADCFUNCTION_INCLUDED
#define __ADCFUNCTION_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"


#define	ADC_VOLUME_CHECK			0
#define	ADC_TEMPERATURE_CHECK		1
#define	ADC_BATTERY_CHECK			2
#define	ADC_RFID_CHECK				3
#define	ADC_MODE_CHECK			4
#define	ADC_KEY_1_CHECK				5
#define	ADC_KEY_2_CHECK				6
#ifdef	ST_TEMPERATURE_SENSOR
#define	ADC_TEMPERATURE_CHECK_ST	7
#define	ADC_VREFINT_ST	8
#endif 




#define	MCU_ADC_TIMEOUT			10


WORD ADCCheck(BYTE bMode);



extern WORD gwAdcValue;


#endif
