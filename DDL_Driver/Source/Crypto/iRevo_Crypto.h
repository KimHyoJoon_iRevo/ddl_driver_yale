
#ifndef __IREVOCRYPTO_INCLUDED
#define __IREVOCRYPTO_INCLUDED


extern BYTE gbStUniqueID[8];
extern BYTE gbEncryptionKey[8];

void GetSTM32Uid(void);
void EncryptDecryptKey(BYTE* bSource, BYTE bSize);



#ifdef _USE_STM32_CRYPTO_LIB_
void AESTest(void);
uint32_t DataEncrypt(BYTE* bpPlainText , uint32_t InputMessageLength , BYTE* bpKey , BYTE* bpCipherText);
uint32_t DataDecrypt(BYTE* bpCipherText , BYTE InputMessageLength , BYTE* bpKey ,  BYTE* bpPlainText);
#endif 


#endif 

