#define	_IREVO_CRYPTO_C_

#include	"main.h"

#ifdef _USE_STM32_CRYPTO_LIB_
#include	"crypto.h"
#endif 

#ifdef _USE_IREVO_CRYPTO_
/* 내부 EEPROM 사용 하면서 비밀 번호 Card UID 를 암호 복호 하긴 위한 define */
/* AES 와 같은 crypto 와는 별개 */


BYTE gbStUniqueID[8];
BYTE gbEncryptionKey[8];


/* 현재로는 암호화 과정 외에는 STM32 의 UID가 필요 없으므로 crypto 안에서 구현 */
void GetSTM32Uid(void)
{
	uint32_t STM32Uid[3] = {0x00000000,0x00000000,0x00000000};
	BYTE i = 0x00;
	/*
	ST UID 는 UID_BASE 에서 12byte 가 유니크 하다.
	다만 96bit 가 전부 유니크 한것은 아니다 Lot number 나 , wafer number , x , y 좌표 값으로 구성 된다 
	해서 Lot number > wafer number > x, y 좌표 이므로 Lot number , wafer number  이값이 같을수 있다 
	x , y 좌표는 STM32Uid[0] 에 있음. 
	x , y 좌표는 wafer 안해서 해당 chip 의 좌표 
	암호화 및 해제 는 EncryptDecryptKey() 로 한다. 
	gbStUniqueID 와 gbEncryptionKey 를 사용 
	gbEncryptionKey 4byte 는 STM32Uid[2] 값을 
	남은 4byte 는 그냥 STM32Uid[0] 값을 넣는다. 특별한 의미 없음 
	*/
	STM32Uid[0] = *(__IO uint32_t*)(UID_BASE);
	STM32Uid[1] = *(__IO uint32_t*)(UID_BASE+0x04);
	STM32Uid[2] = *(__IO uint32_t*)(UID_BASE+0x08);

	for(i =0 ; i < 8 ; i++)
	{
		if(i < 4)
		{
			gbStUniqueID[i] = (BYTE)((STM32Uid[0] >> (i*8)) & 0xFF);
			gbEncryptionKey[i] = (BYTE)((STM32Uid[2] >> (i*8)) & 0xFF);
		}
		else 
		{
			gbStUniqueID[i] = (BYTE)((STM32Uid[1] >> ((i-4)*8)) & 0xFF);
			gbEncryptionKey[i] = (BYTE)((STM32Uid[0] >> ((i-4)*8)) & 0xFF);
		}
	}
}

void EncryptDecryptKey(BYTE* bSource, BYTE bSize)
{
	/* 기본 size 는 8byte 8의 배수로 gbStUniqueID , gbEncryptionKey 를 돌려 가며 사용 */
	/* size 8 이하 사용 가능 8보다 크면 8의 배수로만 사용 16 32 etc..*/
	BYTE i = 0x00;

	GetSTM32Uid();
	
	for(i = 0 ; i < bSize ; i++)
	{
		bSource[i] ^= (gbStUniqueID[(i%8)] ^ gbEncryptionKey[(i%8)]);
	}
}
#endif 

#ifdef _USE_STM32_CRYPTO_LIB_

#if 0
void AESTest(void)
{
	static BYTE result = 0x00;
	static BYTE PlainText[16];
	static BYTE CipherText[16];
	static BYTE FinalText[16];
	static BYTE ENC_KEYBuf[16];
	static BYTE bTempArray[17];
	static BYTE AES_KEY[16];
	static BYTE bVerificationCode[16];

	/* random 함수 및 en , de test */
	RandomNumberGenerator(ENC_KEYBuf,16);
	RandomNumberGenerator(PlainText,16);
	RandomNumberGenerator(CipherText,16);
	RandomNumberGenerator(FinalText,16);	

	DataEncrypt(PlainText,16,ENC_KEYBuf,CipherText);
	DataDecrypt(CipherText,16,ENC_KEYBuf,FinalText);

	if(memcmp(PlainText,FinalText,16) == 0)
	{
		/* 여기 들어 오면 ok */
		__NOP();
	}
	else 
	{
		/* 뭔가 error */
		result = 1;
	}

	/* diffie hellman 및 en , de test */
	bTempArray[0] = IREVO_HFPM_SUB_EVENT_SEED_VALUE;
	memset(&bTempArray[1],0x00,15);
	bTempArray[16] = 25; //(BYTE)Diffie_Hellman("5","2","37"); //5^2 mod 37 = 25

	if(bTempArray[16] != 25)
	{
		/* error */
		result = 2;
	}

	bTempArray[16] = 28; //(BYTE)Diffie_Hellman("18","2","37"); //18^2 mod 37 = 28

	memset(AES_KEY,0x00,16);
	AES_KEY[15] = 28;

	if(memcmp(&bTempArray[1],AES_KEY,16) == 0)
	{
		/* 여기 들어 오면 ok */
		__NOP();
	}
	else 
	{
		/* 뭔가 error */
		result = 3;
	}

	bVerificationCode[0] = 0x00;
	bVerificationCode[1] = 0x01;
	bVerificationCode[2] = 0x02;
	bVerificationCode[3] = 0x03;
	bVerificationCode[4] = 0x04;
	bVerificationCode[5] = 0x05;
	bVerificationCode[6] = 0x06 ;
	bVerificationCode[7] = 0x07;
	bVerificationCode[8] = 0x08;
	bVerificationCode[9] = 0x09;
	bVerificationCode[10] = 0x0a;
	bVerificationCode[11] = 0x0b; 
	bVerificationCode[12] = 0x0c;
	bVerificationCode[13] = 0x0d;
	bVerificationCode[14] = 0x0e;
	bVerificationCode[15] = 0x0f;
	
	DataEncrypt(bVerificationCode,sizeof(bVerificationCode),AES_KEY,CipherText);

	/* 정해진 결과값 */
	FinalText[0] = 0xbd;
	FinalText[1] = 0xd6;
	FinalText[2] = 0x6d;
	FinalText[3] = 0xc0;
	FinalText[4] = 0xea;
	FinalText[5] = 0xaa;
	FinalText[6] = 0x0f;
	FinalText[7] = 0x6e;
	FinalText[8] = 0x93;
	FinalText[9] = 0xd8;
	FinalText[10] = 0x20;
	FinalText[11] = 0x34;
	FinalText[12] = 0x36;
	FinalText[13] = 0x1d;
	FinalText[14] = 0x52;
	FinalText[15] = 0x8f;

	if(memcmp(CipherText,FinalText,16) == 0)
	{
		/* 여기 들어 오면 ok */
		__NOP();
	}
	else 
	{
		/* 뭔가 error */
		result = 4;
	}

	DataDecrypt(CipherText,sizeof(CipherText),AES_KEY,PlainText);

	if(memcmp(PlainText,bVerificationCode,16) == 0)
	{
		/* 여기 들어 오면 ok */
		__NOP();
	}
	else 
	{
		/* 뭔가 error */
		result = 5;
	}

	if(result){
		/* 뭔가 error */
		__NOP();
	}
}
#endif 

uint32_t DataEncrypt(BYTE* bpPlainText , uint32_t InputMessageLength , BYTE* bpKey , BYTE* bpCipherText)
{
#if 1 
	/* ST crypto lib */
	AESECBctx_stt AESctx_st; 
	int32_t outputLength = 0x00;
	WORD error_status = 0x00;

	if(InputMessageLength % CRL_AES128_KEY) // 16 의 배수 인지 확인 
	{
		return AES_ERR_BAD_INPUT_SIZE;
	}

	__HAL_RCC_CRC_CLK_ENABLE();

	error_status = AES_SUCCESS;
	AESctx_st.mFlags = E_SK_DEFAULT;
	AESctx_st.mKeySize=CRL_AES128_KEY;

	error_status = AES_ECB_Encrypt_Init(&AESctx_st, bpKey, NULL );
	if (error_status == AES_SUCCESS)
	{
		error_status = AES_ECB_Encrypt_Append(&AESctx_st, bpPlainText,InputMessageLength, bpCipherText, &outputLength);
		if(error_status == AES_SUCCESS)
		{
			error_status = AES_ECB_Encrypt_Finish(&AESctx_st, bpCipherText + outputLength, &outputLength);
		}
	}

	__HAL_RCC_CRC_CLK_DISABLE();
	
	return error_status;
#else 
	/* cyclone lib */
	AesContext sAESctx;
	error_t error_status;

	if(InputMessageLength % AES_BLOCK_SIZE) // 16 의 배수 인지 확인 
	{
		return ERROR_INVALID_PARAMETER;
	}

//	sAESctx.nr = InputMessageLength;

	error_status= aesInit(&sAESctx,bpKey,InputMessageLength);

	if(error_status == NO_ERROR)
	{
		aesEncryptBlock(&sAESctx, bpPlainText,bpCipherText);
	}

	return error_status ;

#endif 	
}

uint32_t DataDecrypt(BYTE* bpCipherText , BYTE InputMessageLength , BYTE* bpKey ,  BYTE* bpPlainText)
{
#if 1
	/* ST crypto lib */
	AESECBctx_stt AESctx_st; 
	int32_t outputLength;
	WORD error_status;

	if(InputMessageLength % CRL_AES128_KEY) // 16 의 배수 인지 확인 
	{
		return AES_ERR_BAD_INPUT_SIZE;
	}

	__HAL_RCC_CRC_CLK_ENABLE();

	error_status = AES_SUCCESS;
	AESctx_st.mFlags = E_SK_DEFAULT;
	AESctx_st.mKeySize=CRL_AES128_KEY;

	error_status = AES_ECB_Decrypt_Init(&AESctx_st, bpKey, NULL );
	if (error_status == AES_SUCCESS)
	{
		error_status = AES_ECB_Decrypt_Append(&AESctx_st, bpCipherText,InputMessageLength,bpPlainText, &outputLength);
		if (error_status == AES_SUCCESS)
		{
			error_status = AES_ECB_Decrypt_Finish(&AESctx_st, bpPlainText + outputLength, &outputLength);
		}
	}

	__HAL_RCC_CRC_CLK_DISABLE();
	
	return error_status;
#else 
	/* cyclone lib */
	AesContext sAESctx;
	error_t error_status;

	if(InputMessageLength % AES_BLOCK_SIZE) // 16 의 배수 인지 확인 
	{
		return ERROR_INVALID_PARAMETER;
	}

//	sAESctx.nr = InputMessageLength;

	error_status= aesInit(&sAESctx,bpKey,InputMessageLength);

	if(error_status == NO_ERROR)
	{
		aesDecryptBlock(&sAESctx, bpCipherText,bpPlainText);
	}

	return error_status ;	
#endif 	
}

#endif


