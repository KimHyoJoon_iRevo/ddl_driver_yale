//------------------------------------------------------------------------------
/** 	@file	TCS4K_UART.c
	@brief	UART functions for TCS4K communication
*/
//------------------------------------------------------------------------------

#define		_TCS4K_UART_C_

#include	"main.h"


//UART 이상시 재설도 하기 때문에 초기화 코드를 별도로 마련해 둔다.
// 대기 전류를 줄이기 위해 STOP mode가 될때 Tx/Rx pin을 analog로 만들기에 이를 다시 UART용으로 설정하는 코드이다.
void		SetupTCS4k_UART( void )
{
	GPIO_InitTypeDef GPIO_InitStruct;

	//지문모듈과의 통신핀 Tx pin을 gpio로 설정 사용한 뒤 다시 uart로 사용하려면 해당 pin의 설정에서 Alternate function 기능을 활성화 해야한다.
	GPIO_InitStruct.Pin = FP_DDL_TX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;				// Select Alternate function
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART3;			// Select Alternate function
	HAL_GPIO_Init(FP_DDL_TX_GPIO_Port, &GPIO_InitStruct);

	//지문모듈과의 통신핀 Rx pin을 gpio로 설정 사용한 뒤 다시 uart로 사용하려면 해당 pin의 설정에서 Alternate function 기능을 활성화 해야한다.
	GPIO_InitStruct.Pin = FP_DDL_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;				// Select Alternate function
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART3;			// Select Alternate function
	HAL_GPIO_Init(FP_DDL_RX_GPIO_Port, &GPIO_InitStruct);

	gh_mcu_uart_finger->Instance = MCU_TCS4K_UART;		// MCU_TCS4K_UART는 TCS4K_UART.h에 선언되어  있다.
	gh_mcu_uart_finger->Init.BaudRate = 9600;
	gh_mcu_uart_finger->Init.WordLength = UART_WORDLENGTH_8B;
	gh_mcu_uart_finger->Init.StopBits = UART_STOPBITS_1;
	gh_mcu_uart_finger->Init.Parity = UART_PARITY_NONE;
	gh_mcu_uart_finger->Init.Mode = UART_MODE_TX_RX;
	gh_mcu_uart_finger->Init.HwFlowCtl = UART_HWCONTROL_NONE;
	gh_mcu_uart_finger->Init.OverSampling = UART_OVERSAMPLING_16;
	HAL_UART_Init( gh_mcu_uart_finger );
}


// 아래 함수는 TCS4K와 통신시 TX pin을 제어하기 위해 TX만 GPIO로 만드는 함수이다.
// TX를 High -> Low -> High로 하면 TCS4K로부터 data가 나온다.
void EnableTCS4KTxAsGpio(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = FP_DDL_TX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(FP_DDL_TX_GPIO_Port, &GPIO_InitStruct);

	P_FP_DDL_TX(1);
}


// 대기 전류를 줄이기 위해 TCS4K와의 통신 pin을 analog로 바꾼다.
void OffGpioTCS4K(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = FP_DDL_TX_Pin | FP_DDL_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(FP_DDL_TX_GPIO_Port, &GPIO_InitStruct);
}



// UART callback 함수를 MX_uart_lapper에 등록한다.
void		RegisterTCS4K_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) )
{
	if ( uart_rxcplt_callback != NULL )
		register_uart_rxcplt_callback( gh_mcu_uart_finger, uart_rxcplt_callback );
	if ( uart_txcplt_callback != NULL )
		register_uart_txcplt_callback( gh_mcu_uart_finger, uart_txcplt_callback );
}


void		UartSendTCS4K( BYTE data )
{
	HAL_UART_Transmit_IT( gh_mcu_uart_finger, &data, sizeof(data) );
}


