#define		_FINGER_TCS4K_C_

#include	"main.h"

uint8_t		tcs4k_rx_byte;
uint8_t		tcs4k_tx_byte;




FINGERPACKET FingerRx;
FINGERPACKET FingerTx;

FLAG BioFlag;


BYTE *pFingerTx;
BYTE *pFingerRx;
BYTE BioRetryCnt;

DWORD gBioEnrollCnt;


BYTE gbUart6_data=4;
BYTE gbUart6TX_data=4;
BYTE gbSQNum=0;
BYTE gbFID=0;
BYTE gbREG_CNT=0;
BYTE gbBIOErrCheck = 0;

BYTE gbError = 0;
BYTE gbErrorCnt = 0;
BYTE gbBIOErrorCNT = 0;

BYTE gbCoverOn=0;
	
BYTE gbBIOCONCNT=0;
BYTE gbBIORMCNT;

WORD gbBioTimer10ms=0;

BYTE gbCoverOld=0;
BYTE gbSecurity;

BYTE gbFingerPTMode;
BYTE gbFingerLongTimeWait100ms = 0;
BYTE gbLonOnID[8];

BYTE gbFingerActionTimer10ms = 0;

WORD gbfingerCloseDelaycnt = 0;
BYTE gbfingerCloseDelayflag = 0;





uint32_t	uart3_rxcplt_count;
uint32_t	uart3_txcplt_count;
void TCS4K_uart_rxcplt_callback(UART_HandleTypeDef *huart)
{
	BYTE i, bTmp;
	WORD usTmp;

	uart3_rxcplt_count++;

	bTmp = tcs4k_rx_byte;

	if(bTmp == 0x02){	// data 전송 시작 
		pFingerRx = &FingerRx.PACKET.Start_st;
		gfBioUARTST =1; 		
	}

	if(gfBioUARTST ==1){				// 시작 이후터 는 이곳에..
		 if(gfBioUARTStuff ==1){		// rx 부분에서 0x10 이후 나온 것들 처리./..
			if(bTmp==0x10){
				*pFingerRx = bTmp;
				pFingerRx++;
			}else{
				*pFingerRx = bTmp-1;
				pFingerRx++;
			}
			gfBioUARTStuff=0;
		}else if(bTmp==0x10){	//stuff 시작 
			gfBioUARTStuff =1;
		}else{								// 일반 적인 것들..
			*pFingerRx = bTmp;
			pFingerRx++;
		}
	}	


	if(pFingerRx == (&FingerRx.PACKET.Start_st + 4)){	// 총 data 길이 부분 수신 후.. data 길이 확정 하기
		pFingerRx--;							// data길이 부분 주소..
		gbUart6_data = (*pFingerRx) + 6;
		pFingerRx++;							//주소 원상 복귀..
	}

	if(pFingerRx >= &FingerRx.PACKET.Start_st + gbUart6_data){		// data 모두 수신.. CRC 만들기...
		usTmp=0;
		pFingerRx = &FingerRx.PACKET.Start_st;
		for(i=1;i<(gbUart6_data-2);i++){								//CRC data 만들기..
			pFingerRx++;
			usTmp  = (WORD)((BYTE)(usTmp >> 8) | (usTmp << 8));
			usTmp ^= (BYTE) *pFingerRx;
			usTmp ^= (BYTE)(usTmp & 0xff) >> 4;
			usTmp ^= (usTmp << 8) << 4;
			usTmp ^= ((usTmp & 0xff) << 4) << 1;
		}

		pFingerRx = &FingerRx.PACKET.Start_st + gbUart6_data -2;	
		if(((BYTE)(usTmp & 0x0ff)) == *pFingerRx){					// 1st CRC 비교 //
			pFingerRx++;
			if((((BYTE)(usTmp>>8) & 0x0ff)) == *pFingerRx){ 	// 2nd CRC 비교 //
				gfUART0RxEnd = 1;								//수신완료..
				gfBioUARTST=0;
			}else{
				gfUART0RxEnd = 0;								//초기화..
				gfBioUARTST=0;
				gbError = LLSTATUS_LLFRAME_SYNTAX_ERROR;
			}
		}else{
			gfUART0RxEnd = 0;									//초기화..
			gfBioUARTST=0;
			gbError = LLSTATUS_LLFRAME_SYNTAX_ERROR;
		}
		
	}

	//반복적인 Rx를 위해 다시 Receive 요청
	HAL_UART_Receive_IT( gh_mcu_uart_finger, &tcs4k_rx_byte, sizeof(tcs4k_rx_byte) );
}


void TCS4K_uart_txcplt_callback(UART_HandleTypeDef *huart)
{
	uart3_txcplt_count++;

	if ( pFingerTx == NULL )		return;

	pFingerTx++;
	if(pFingerTx >= &FingerTx.PACKET.Start_st + gbUart6TX_data){		//송신 완료 시.. 
		gfUART0TxEnd = 1;
	}else if(*pFingerTx == LL_CHAR_XON){			//송신 data 중 ..0x11 나오면 우선 0x10보내고..그자리에 0x11+1 더해 넣고 주소 1개 줄이기..
//		Usart3DataTransmit(LL_CHAR_DLE);
		tcs4k_tx_byte = LL_CHAR_DLE;
		HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );

		*pFingerTx=LL_CHAR_XON+1;
		pFingerTx--;
	}else if(*pFingerTx == LL_CHAR_XOFF){		//송신 data 중 ..0x13 나오면 우선 0x10보내고..그자리에 0x13+1 더해 넣고 주소 1개 줄이기..
//		Usart3DataTransmit(LL_CHAR_DLE);
		tcs4k_tx_byte = LL_CHAR_DLE;
		HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );

		*pFingerTx=LL_CHAR_XOFF+1;
		pFingerTx--;
	}else if(*pFingerTx == LL_CHAR_STX){			//송신 data 중 ..0x02 나오면 우선 0x10보내고..그자리에 0x02+1 더해 넣고 주소 1개 줄이기..
//		Usart3DataTransmit(LL_CHAR_DLE);
		tcs4k_tx_byte = LL_CHAR_DLE;
		HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );

		*pFingerTx=LL_CHAR_STX+1;
		pFingerTx--;
	}else if(gfBioTXStuff==1){						// 0x10 전송시는 다음 0x10을 전송하기에 stuff flag 사용ㅇ..
		gfBioTXStuff=0;
//		Usart3DataTransmit(*pFingerTx);
		tcs4k_tx_byte = *pFingerTx;
		HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );
	}else if(*pFingerTx == LL_CHAR_DLE){			//송신 data 중 ..0x20 나오면 우선 0x10보내고..그자리에 0x10  넣고 주소 1개 줄이고 flag 1을 살려 사용
//		Usart3DataTransmit(LL_CHAR_DLE);
		tcs4k_tx_byte = LL_CHAR_DLE;
		HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );

		*pFingerTx=LL_CHAR_DLE;
		gfBioTXStuff=1;
		pFingerTx--;
	}else{
//		Usart3DataTransmit(*pFingerTx);
		tcs4k_tx_byte = *pFingerTx;
		HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );
	}
}



void		FingerRxInterruptRequest( void )
{
	// UART receive complete INT가 들어오도록 호출
	HAL_UART_Receive_IT( gh_mcu_uart_finger, &tcs4k_rx_byte, sizeof(tcs4k_rx_byte) );
}


// TCS4K와 통신을 위해 UART 초기화 와 Tx/Rx Complete Callback함수를 등록하고
// 처음 Rx를 위해 Receive IT를 요청한다.
void		InitialSetupFingerModule( void )
{
//	SetupTCS4k_UART();

	RegisterTCS4K_INT_Callback( TCS4K_uart_txcplt_callback, TCS4K_uart_rxcplt_callback);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//FP_Motor_Cover_Type - 지문 모듈 커버타입이 모터 타입일 경우 ddl_Config-[모델명].h 에 아래 define
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE

BYTE gbFingerMode = 0; //모터커버 프로세스 
BYTE gbFingerCoverState = 0; //모터커버 상태
BYTE gbFingerCoverCloseRetryCnt = 0; //모터커버 에러 카운트

void FPMotorCoverProcess(void)
{
	switch(gbFingerMode)
	{
		case 0:
			break;
			
		case 10:			//case 10 ~ case 11 : Motor Cover Open
			P_FP_COVER_CLOSE(0);			
			P_FP_COVER_OPEN(1);
			gbFingerCoverState = FINGER_COVER_OPENING;
			gbWakeUpMinTime10ms = 20;
			gbFingerMode++;
			break;

		case 11:
			if(gbWakeUpMinTime10ms)break;
			P_FP_COVER_CLOSE(0);			
			P_FP_COVER_OPEN(0);
			gbFingerCoverState = FINGER_COVER_OPENED;
			gbFingerCoverCloseRetryCnt = 0;			
			gbFingerMode = 0;
			break;

		case 20:		//case 20 ~ case 21 : Motor Cover Close, or Error
			P_FP_COVER_CLOSE(1);			
			P_FP_COVER_OPEN(0);
			gbFingerCoverState = FINGER_COVER_CLOSING;
			gbWakeUpMinTime10ms = 20;
			gbFingerMode++;			
			break;

		case 21:
			if(gbWakeUpMinTime10ms)break;
			P_FP_COVER_CLOSE(0);			
			P_FP_COVER_OPEN(0);
			if(gfCoverSw)
			{
				// 열려 있으면 
				gbFingerCoverState = FINGER_COVER_CLOSED_ERROR;
			}
			else
			{
				gbFingerCoverState = FINGER_COVER_CLOSED;
				gbFingerCoverCloseRetryCnt = 0;
			}
			gbFingerMode = 0;			
			break;		
			
		default:
			P_FP_COVER_CLOSE(0);			
			P_FP_COVER_OPEN(0);
			gbFingerMode = 0;	
			break;
		}

}

void FPMotorCoverAction(BYTE direction)
{
	if(direction == _OPEN)
	{
		if(gbFingerCoverState != FINGER_COVER_OPENED)gbFingerMode = 10;	//이미 OPEN상태 이면 동작 안하게
	}
	else if(direction == _CLOSE)
	{
		if(gbFingerCoverState != FINGER_COVER_CLOSED)gbFingerMode = 20;	//이미 CLOSE상태 이면 동작 안하게 
	}
}

void FPMotorCoverCloseRetry(void)      //ddl_main.c에 StopModePrepareCallback() 에 call, stop mode 들어가기전에 커버가 열려 있으면 닫는 기능, 에러발생시  3회 시도
{
	if((gbFingerCoverState != FINGER_COVER_CLOSED) && (gfCoverSw == 1))
	{
		if(gbFingerCoverCloseRetryCnt<3)
		{
			FPMotorCoverAction(_CLOSE); 
			gbFingerCoverCloseRetryCnt++;	
		}
	}
}

#endif 

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////



void FPCoverSwProcess(void)
{
	BYTE bTmp;

	if(!gfXORCoverSw)	return;						//Cover의 신호가 바뀌어야만 루틴 수행
	gfXORCoverSw = 0;

	// 3분락 수행 중일 경우 MainMode 처리하지 않음
	if(GetTamperProofPrcsStep())
		return;

	if ( gfBrokenAlarm )		//파손 경보이면 커버스위치 동작 안함
		return;

	bTmp = GetMainMode();
	if( bTmp == 0 || bTmp == MODE_PIN_VERIFY ) {
		//Main Mode 0 에서 커버 동작시, 비밀번호 인증 모드에서 커버동작시.
		if(gfCoverSw){
			// Cover Open
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
			FPMotorCoverAction(_OPEN);	//버튼이 아닌 직접 손으로 커버 오픈 하면 모터커버 동작 
#endif
			if(GetMotorPrcsStep())	return;
//			if(gwStopModeCheck != 0x84BB)	return;

			gbfingerCloseDelaycnt = 0;
			ModeGotoFingerVerify();
		}
		else{
			// Cover CLOSE
			if ( bTmp != 0 )		 //대기 모드에서 커버만 닫히면 피드백하지 않는다.
#ifdef	DDL_CFG_FP_MOTOR_COVER_TYPE
				if(gbFingerMode == 0)FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); //모터가 닫히는 중 텐키 wakup 되면 에러 표시하는 증상으로 인해 조건 추가 
#else
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
#endif
			else
			{
				if(!(MotorSensorCheck() & (SENSOR_LOCK_STATE|SENSOR_OPENCEN_STATE)))
				{
					FeedbackBuzStopLedOff();
				}
				//내부 강제 잠금 상태가 아니면 normal operation - 내부 강제 잠금 일 경우는 LED OFF 하지 않는다.(내부 잠금 알람 LED 모션)
				//내부 강제 잠금 상태에서 커버를 Open - Close 하면 내부 강제 잠금 LED가 강제로 Off되는 문제로 위 조건 추가
				
			}															
			BioOffModeClear();
		}
	}
	else {
		if(gfBioCoverIn)
		//if(gfCoverSw)
		{
			if( GetMainMode() == 0 )		return;
			// 커버가 내려갔을 경우
			//if(GetMainMode()==MODE_FINGER_VERIFY && gbModePrcsStep ==MODE_PIN_VERIFY) 	return;
//			if(GetMainMode()==MODE_FINGER_VERIFY)	return;
			if(gfCoverSw){
				return;	// 0 : close, 1 : open
			}

			gfBioCoverIn = 0;
			gfBioCoverDown = 1;
		}
	}
}



void BioModuleON( void )
{
	P_FP_EN(1);		//	enable Finger module power

	SetupTCS4k_UART();
	EnableTCS4KTxAsGpio();

	pFingerRx = &FingerRx.PACKET.Start_st;
	gbUart6_data=4;
	gfUART0RxEnd=0;
	gfUART0TxEnd=0;
	gfBioUARTST=0;
	gfBioUARTStuff=0;
	gfBioTXStuff=0;

	gbErrorCnt = 0;   // 재 전송을 하기  위해서 

	// 최초 UART receive complete INT가 들어오도록 호출
	HAL_UART_Receive_IT( gh_mcu_uart_finger, &tcs4k_rx_byte, sizeof(tcs4k_rx_byte) );
}



void BioModuleOff( void )
{
	gfBioCoverIn = 0;
	gfBioCoverDown = 0;
	gfUART0RxEnd = 0;
	gfUART0TxEnd = 0;
	gfBioUARTST = 0;
	gfBioUARTStuff = 0;
	gfBioTXStuff = 0;
	gbFingerPTMode = 0;
	P_FP_EN(0);		//	disable Finger module

	OffGpioTCS4K();
}


void BioOffModeClear( void )
{
	BioModuleOff();
	ModeClear();

	gbModeChangeFlag4Finger = 0;
}


void BioReTry( void )				// 지문 모듈 이상 하면 전체 off on
{
	gbBioTimer10ms--;

	if(!P_FP_DDL_RX_T) {
		//RX가 LOW 일 경우
		HAL_UART_DeInit( gh_mcu_uart_finger );

		P_FP_EN(0);			// 파워만 조절 하여 이상 현상 제거 가능 (그러나 UART도 초기화  안전하게 가기 위해서 )
		Delay(SYS_TIMER_15MS);
		P_FP_EN(1);
		Delay(SYS_TIMER_100MS);

		SetupTCS4k_UART();

		// 최초 UART receive complete INT가 들어오도록 호출
		HAL_UART_Receive_IT( gh_mcu_uart_finger, &tcs4k_rx_byte, sizeof(tcs4k_rx_byte) );
	}
}


#if 0
//==============================================================================//
// Comment	: 지문 번호가 메모리에 존재하는지 검사
// Return 	: 0:없음 	1:존재 함.
//==============================================================================//
BYTE FingerSlotVerify(void)
{
	BYTE bTmp;
	BYTE bCnt;
	BYTE bBuff;

	for(bCnt = 0; bCnt < _MAX_REG_BIO; bCnt++){
		RomRead(&bBuff, (WORD)BIO_ID+(2*bCnt), 1);

		bTmp=0;
		
		if( &bBuff == gbTenKeyWriteBuf[0])	++bTmp;

		if(bTmp == 1){
			gbFID = bCnt;
			return 1;
		}else{
			gbFID=0xff;
		}
	}

	return 0;	
}
#endif

BYTE  uart_reset_interface(void)
{
	BYTE btmp;

	//SetupUsart3Rx();
	//EnableGPIOUsart3();
	P_FP_DDL_TX(0);
	gfUART0RxEnd=0; 
	btmp=30;
	while(!gfUART0RxEnd){
		if(gfBioUARTST==1){ 	// PTopen 잘 안되어서,..
			//SetupUsart3(9600);		
			P_FP_DDL_TX(1);
			return OK;
		}	
		btmp--;
		Delay(SYS_TICK_1MS);

		if(btmp==0){
			//SetupUsart3(9600);
			P_FP_DDL_TX(1);
			return OK;
		}	
	}
	//SetupUsart3(9600);	
	P_FP_DDL_TX(1);			// PTopen 잘 안되어서,..
	return OK;	  
}




static	WORD PTCRC(BYTE DATA_len)
{
	WORD wCRC;
	BYTE i;

	wCRC=0;
	pFingerTx = &FingerTx.PACKET.Start_st;
	for(i=1;i<DATA_len;i++){				// CRC 부분 함 수..
		pFingerTx++;
		wCRC  = (WORD)((BYTE)(wCRC >> 8) | (wCRC << 8));
		wCRC ^= (BYTE) *pFingerTx;
		wCRC ^= (BYTE)(wCRC & 0xff) >> 4;
		wCRC ^= (wCRC << 8) << 4;
		wCRC ^= ((wCRC & 0xff) << 4) << 1;
	}
	return wCRC;
}


BYTE PTOPEN_SEND( void )
{
	WORD usTmp;
	
	FingerTx.PACKET.Start_st = 0x02;				// STX
	
	FingerTx.PACKET.Command_st[0] = LL_FRAME_SET_PARAMS;		//  LL Hdr: LL_FRAME_TYPE_SET_PARAMS, 9 bytes
	FingerTx.PACKET.Command_st[1] = 0x00;
	FingerTx.PACKET.Command_st[2] = 0x09;

	FingerTx.PACKET.Parameter_st[0] = TFM_PROTOCOL_VERSION;		

	FingerTx.PACKET.Parameter_st[1] = 0x00;

	FingerTx.PACKET.Parameter_st[2] = 0x88; 		////WTX timeout 0x1388=5000 msec (70% of comm.
	FingerTx.PACKET.Parameter_st[3] = 0x14;
	FingerTx.PACKET.Parameter_st[4] = 0x00;
	FingerTx.PACKET.Parameter_st[5] = 0x00;		// max. possible transfer size,

	FingerTx.PACKET.Parameter_st[6] = 0xFF;
	FingerTx.PACKET.Parameter_st[7] = 0x00;

	FingerTx.PACKET.Parameter_st[8] = LL_SIO_BAUDRATE_9600;		//9600 bps

	gbUart6TX_data = 0x09 + 6;
	usTmp=0;

	usTmp=PTCRC(gbUart6TX_data-2);	
	
	FingerTx.PACKET.Parameter_st[9] = ((BYTE)(usTmp & 0x0ff)) ;				// CRC 넣기 ..

	FingerTx.PACKET.Parameter_st[10] = (((BYTE)(usTmp>>8) & 0x0ff));		// CRC 넣기 ..

	pFingerTx = &FingerTx.PACKET.Start_st;

	//UartSendTCS4K(*pFingerTx);
	tcs4k_tx_byte = *pFingerTx;
	HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );
	return 1;
}


BYTE PTALLLIST_SEND( void )
{
	WORD usTmp;
	
	FingerTx.PACKET.Start_st = 0x02;				// STX
	
	FingerTx.PACKET.Command_st[0] = 0x00;		//  LL Hdr: LL_FRAME_TYPE_SET_PARAMS, 9 bytes
	FingerTx.PACKET.Command_st[1] = 0x10;
	FingerTx.PACKET.Command_st[2] = 0x07;

	FingerTx.PACKET.Parameter_st[0] = 0x28;		
	FingerTx.PACKET.Parameter_st[1] = 0x04;
	FingerTx.PACKET.Parameter_st[2] = 0x00; 		////WTX timeout 0x1388=5000 msec (70% of comm.

	FingerTx.PACKET.Parameter_st[3] = 0x00;
	FingerTx.PACKET.Parameter_st[4] = 0x00;
	FingerTx.PACKET.Parameter_st[5] = 0x0d;		// max. possible transfer size,
	FingerTx.PACKET.Parameter_st[6] = 0x02;

	gbUart6TX_data = 0x07 + 6;
	usTmp=0;

	usTmp=PTCRC(gbUart6TX_data-2);	
	
	FingerTx.PACKET.Parameter_st[7] = ((BYTE)(usTmp & 0x0ff)) ;				// CRC 넣기 ..

	FingerTx.PACKET.Parameter_st[8] = (((BYTE)(usTmp>>8) & 0x0ff));		// CRC 넣기 ..

	pFingerTx = &FingerTx.PACKET.Start_st;

	//UartSendTCS4K(*pFingerTx);
	tcs4k_tx_byte = *pFingerTx;
	HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );

	return 1;
}

BYTE PTENROLL_SEND( BYTE upper_id, BYTE lower_id, BYTE no_fp )
{
	WORD usTmp;
	
	FingerTx.PACKET.Start_st = 0x02;

	FingerTx.PACKET.Command_st[0] = LL_FRAME_DATA;		// data 형태 
	FingerTx.PACKET.Command_st[1] = (gbSQNum<<4);		// SQ 첫 번 
	FingerTx.PACKET.Command_st[2] = 0x18;		// data 길이

	FingerTx.PACKET.Parameter_st[0] = 0x28;		// first & last fragment
	FingerTx.PACKET.Parameter_st[1] = 0x15;		// data 길이
	FingerTx.PACKET.Parameter_st[2] = 0x00;

	FingerTx.PACKET.Parameter_st[3] = 0x00;		// 등록 명령어 0x02020000
	FingerTx.PACKET.Parameter_st[4] = 0x00;
	FingerTx.PACKET.Parameter_st[5] = 0x02;
	FingerTx.PACKET.Parameter_st[6] = 0x02;
	
	FingerTx.PACKET.Parameter_st[7] = 0x30;		// 타임 아웃  0x00007530 = 3000ms 
	FingerTx.PACKET.Parameter_st[8] = 0x75;
	FingerTx.PACKET.Parameter_st[9] = 0x00;
	FingerTx.PACKET.Parameter_st[10]=0x00;

	FingerTx.PACKET.Parameter_st[11]=PT_PURPOSE_ENROLL;		// 등록 명령	

	FingerTx.PACKET.Parameter_st[12]= (BE_STORE_TEMPLATE | BE_HAS_PAYLOAD) ;  // 0x0A;
	FingerTx.PACKET.Parameter_st[13]=0x00;

	FingerTx.PACKET.Parameter_st[14]=0xCD;		// 추가 data 

	FingerTx.PACKET.Parameter_st[15]=0x05;		// 저장 data 5바이트..
	FingerTx.PACKET.Parameter_st[16]=0x00;
	FingerTx.PACKET.Parameter_st[17]=0x00;
	FingerTx.PACKET.Parameter_st[18]=0x00;

	FingerTx.PACKET.Parameter_st[19]=upper_id;	// 지문 번호 
	FingerTx.PACKET.Parameter_st[20]=lower_id;
	FingerTx.PACKET.Parameter_st[21]=no_fp;		// 등록 숫자 
	FingerTx.PACKET.Parameter_st[22]=0x34;					//reserve
	FingerTx.PACKET.Parameter_st[23]=0x35;
	
	gbUart6TX_data = 0x18 + 6;	
	usTmp=0;

	usTmp=PTCRC(gbUart6TX_data-2);	
	
	FingerTx.PACKET.Parameter_st[24] = ((BYTE)(usTmp & 0x0ff)) ;

	FingerTx.PACKET.Parameter_st[25]  = (((BYTE)(usTmp>>8) & 0x0ff));

	pFingerTx = &FingerTx.PACKET.Start_st;

	//UartSendTCS4K(*pFingerTx);
	tcs4k_tx_byte = *pFingerTx;
	HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );
	
	return 1;
}


BYTE PTVERIFY_SEND(BYTE VID)
{
	WORD usTmp;
	
	FingerTx.PACKET.Start_st = 0x02;

	FingerTx.PACKET.Command_st[0] = LL_FRAME_DATA;	// data 형태	
	FingerTx.PACKET.Command_st[1] = 0x10;	// SQ
	FingerTx.PACKET.Command_st[2] = 0x23;	// data 길이

	FingerTx.PACKET.Parameter_st[0] = 0x28;	// first & last fragment
	FingerTx.PACKET.Parameter_st[1] = 0x20;	// data 길이
	FingerTx.PACKET.Parameter_st[2] = 0x00;

	FingerTx.PACKET.Parameter_st[3] = 0x00;	// verify 명령어  0x02030000	
	FingerTx.PACKET.Parameter_st[4] = 0x00;
	FingerTx.PACKET.Parameter_st[5] = 0x03;
	FingerTx.PACKET.Parameter_st[6] = 0x02;
	
	FingerTx.PACKET.Parameter_st[7] = 0x00;	// maxFARRequested = 0
	FingerTx.PACKET.Parameter_st[8] = 0x00;
	FingerTx.PACKET.Parameter_st[9] = 0x00;
	FingerTx.PACKET.Parameter_st[10]=0x00;

	FingerTx.PACKET.Parameter_st[11]=0x00;	//maxFRRRequested = 0
	FingerTx.PACKET.Parameter_st[12]=0x00;
	FingerTx.PACKET.Parameter_st[13]=0x00;
	FingerTx.PACKET.Parameter_st[14]=0x00;

	FingerTx.PACKET.Parameter_st[15]=0x00;	//precedenceFAR = 0
	FingerTx.PACKET.Parameter_st[16]=0x00;
	FingerTx.PACKET.Parameter_st[17]=0x00;
	FingerTx.PACKET.Parameter_st[18]=0x00;

	FingerTx.PACKET.Parameter_st[19]=0x30;	// 타임 아웃  0x00007530 = 3000ms 
	FingerTx.PACKET.Parameter_st[20]=0x75;
	FingerTx.PACKET.Parameter_st[21]=0x00;
	FingerTx.PACKET.Parameter_st[22]=0x00;

	FingerTx.PACKET.Parameter_st[23]=0x20;	//	BV_RETURN_PAYLOAD
	FingerTx.PACKET.Parameter_st[24]=0x04;	//    BV_CAPTURE_FINGERPRINT

	FingerTx.PACKET.Parameter_st[25]=0xCD;	//    stuffing bytes,next item is DWORD aligned
	FingerTx.PACKET.Parameter_st[26]=0xCD;

	FingerTx.PACKET.Parameter_st[27]=0x80;	// form = pt slot input	
	FingerTx.PACKET.Parameter_st[28]=0x00;
	FingerTx.PACKET.Parameter_st[29]=0x00;
	FingerTx.PACKET.Parameter_st[30]=0x00;

	FingerTx.PACKET.Parameter_st[31]=VID;		// ID	
	FingerTx.PACKET.Parameter_st[32]=0x00;
	FingerTx.PACKET.Parameter_st[33]=0x00;
	FingerTx.PACKET.Parameter_st[34]=0x00;


	gbUart6TX_data = 0x23 + 6;	
	usTmp=0;

	usTmp=PTCRC(gbUart6TX_data-2);	
	FingerTx.PACKET.Parameter_st[35] = ((BYTE)(usTmp & 0x0ff)) ;

	FingerTx.PACKET.Parameter_st[36]  = (((BYTE)(usTmp>>8) & 0x0ff));

	pFingerTx = &FingerTx.PACKET.Start_st;

	//UartSendTCS4K(*pFingerTx);
	tcs4k_tx_byte = *pFingerTx;
	HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );
	return 1;
}


BYTE PTVERIFYALL_SEND( void )
{
	WORD usTmp;
	
	FingerTx.PACKET.Start_st = 0x02;

	FingerTx.PACKET.Command_st[0] = LL_FRAME_DATA;	// data 형태
	FingerTx.PACKET.Command_st[1] = 0x10;	// SQ
	FingerTx.PACKET.Command_st[2] = 0x19;	// data 길이

	FingerTx.PACKET.Parameter_st[0] = 0x28;	// first & last fragment
	FingerTx.PACKET.Parameter_st[1] = 0x16;	// data 길이
	FingerTx.PACKET.Parameter_st[2] = 0x00;

	FingerTx.PACKET.Parameter_st[3] = 0x00;	// verifyall 명령어  0x020a0000	
	FingerTx.PACKET.Parameter_st[4] = 0x00;
	FingerTx.PACKET.Parameter_st[5] = 0x0A;
	FingerTx.PACKET.Parameter_st[6] = 0x02;
	
	FingerTx.PACKET.Parameter_st[7] = 0x00;	// maxFARRequested = 0
	FingerTx.PACKET.Parameter_st[8] = 0x00;
	FingerTx.PACKET.Parameter_st[9] = 0x00;
	FingerTx.PACKET.Parameter_st[10]=0x00;

	FingerTx.PACKET.Parameter_st[11]=0x00;	//maxFRRRequested = 0
	FingerTx.PACKET.Parameter_st[12]=0x00;
	FingerTx.PACKET.Parameter_st[13]=0x00;
	FingerTx.PACKET.Parameter_st[14]=0x00;

	FingerTx.PACKET.Parameter_st[15]=0x00;	//precedenceFAR = 0
	FingerTx.PACKET.Parameter_st[16]=0x00;
	FingerTx.PACKET.Parameter_st[17]=0x00;
	FingerTx.PACKET.Parameter_st[18]=0x00;

	FingerTx.PACKET.Parameter_st[19]=0x30;	// 타임 아웃  0x00007530 = 3000ms 
	FingerTx.PACKET.Parameter_st[20]=0x75;
	FingerTx.PACKET.Parameter_st[21]=0x00;
	FingerTx.PACKET.Parameter_st[22]=0x00;

	FingerTx.PACKET.Parameter_st[23]=BV_CAPTURE_FINGERPRINT;		
	FingerTx.PACKET.Parameter_st[24]=0x00;

	gbUart6TX_data = 0x19 + 6;
	usTmp=0;
	usTmp=PTCRC(gbUart6TX_data-2);	
	FingerTx.PACKET.Parameter_st[25] = ((BYTE)(usTmp & 0x0ff)) ;

	FingerTx.PACKET.Parameter_st[26]  = (((BYTE)(usTmp>>8) & 0x0ff));

	pFingerTx = &FingerTx.PACKET.Start_st;

	//UartSendTCS4K(*pFingerTx);
	tcs4k_tx_byte = *pFingerTx;
	HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );
	return 1;
}



BYTE PTDEL_SEND(BYTE DID)
{
	WORD usTmp;
	
	FingerTx.PACKET.Start_st = 0x02;

	FingerTx.PACKET.Command_st[0] = LL_FRAME_DATA;	// data 형태
	FingerTx.PACKET.Command_st[1] = 0x10;	// SQ
	FingerTx.PACKET.Command_st[2] = 0x0D;	// data 길이

	FingerTx.PACKET.Parameter_st[0] = 0x28;	// first & last fragment
	FingerTx.PACKET.Parameter_st[1] = 0x0A;	// data 길이
	FingerTx.PACKET.Parameter_st[2] = 0x00;

	FingerTx.PACKET.Parameter_st[3] = 0x00;	// dell 명령어  0x02060000
	FingerTx.PACKET.Parameter_st[4] = 0x00;
	FingerTx.PACKET.Parameter_st[5] = 0x06;
	FingerTx.PACKET.Parameter_st[6] = 0x02;
	
	FingerTx.PACKET.Parameter_st[7] = DID;		// ID 
	FingerTx.PACKET.Parameter_st[8] = 0x00;
	FingerTx.PACKET.Parameter_st[9] = 0x00;
	FingerTx.PACKET.Parameter_st[10]=0x00;

	FingerTx.PACKET.Parameter_st[11]=0x00;
	FingerTx.PACKET.Parameter_st[12]=0x00;
	
	gbUart6TX_data = 0x0D + 6;	
	usTmp=0;
	usTmp=PTCRC(gbUart6TX_data-2);	
	FingerTx.PACKET.Parameter_st[13] = ((BYTE)(usTmp & 0x0ff)) ;

	FingerTx.PACKET.Parameter_st[14]  = (((BYTE)(usTmp>>8) & 0x0ff));

	pFingerTx = &FingerTx.PACKET.Start_st;

	//UartSendTCS4K(*pFingerTx);
	tcs4k_tx_byte = *pFingerTx;
	HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );

	return 1;
}

BYTE PTDELALL_SEND( void )
{
	WORD usTmp;
	
	FingerTx.PACKET.Start_st = 0x02;

	FingerTx.PACKET.Command_st[0] = LL_FRAME_DATA;	// data 형태
	FingerTx.PACKET.Command_st[1] = 0x10;	// SQ
	FingerTx.PACKET.Command_st[2] = 0x07;	// data 길이

	FingerTx.PACKET.Parameter_st[0] = 0x28;	// first & last fragment
	FingerTx.PACKET.Parameter_st[1] = 0x04;	// data 길이
	FingerTx.PACKET.Parameter_st[2] = 0x00;

	FingerTx.PACKET.Parameter_st[3] = 0x00;	// dellall 명령어  0x02070000
	FingerTx.PACKET.Parameter_st[4] = 0x00;
	FingerTx.PACKET.Parameter_st[5] = 0x07;
	FingerTx.PACKET.Parameter_st[6] = 0x02;
	
	gbUart6TX_data = 0x07 + 6;	
	usTmp=0;
	usTmp=PTCRC(gbUart6TX_data-2);	
	FingerTx.PACKET.Parameter_st[7] = ((BYTE)(usTmp & 0x0ff)) ;

	FingerTx.PACKET.Parameter_st[8]  = (((BYTE)(usTmp>>8) & 0x0ff));

	pFingerTx = &FingerTx.PACKET.Start_st;

	//UartSendTCS4K(*pFingerTx);
	tcs4k_tx_byte = *pFingerTx;
	HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );
	return 1;
}


BYTE PTLIST_SEND( void )
{
	WORD usTmp;
	
	FingerTx.PACKET.Start_st = 0x02;

	FingerTx.PACKET.Command_st[0] = LL_FRAME_DATA;	// data 형태
	FingerTx.PACKET.Command_st[1] = 0x10;	// SQ
	FingerTx.PACKET.Command_st[2] = 0x07;	// data 길이

	FingerTx.PACKET.Parameter_st[0] = 0x28;	// first & last fragment
	FingerTx.PACKET.Parameter_st[1] = 0x04;	// data 길이
	FingerTx.PACKET.Parameter_st[2] = 0x00;

	FingerTx.PACKET.Parameter_st[3] = 0x00;	// LIST 명령어  0x020D0000
	FingerTx.PACKET.Parameter_st[4] = 0x00;
	FingerTx.PACKET.Parameter_st[5] = 0x0D;
	FingerTx.PACKET.Parameter_st[6] = 0x02;

	gbUart6TX_data = 0x07 + 6;	
	usTmp=0;
	usTmp=PTCRC(gbUart6TX_data-2);	
	FingerTx.PACKET.Parameter_st[7] = ((BYTE)(usTmp & 0x0ff)) ;

	FingerTx.PACKET.Parameter_st[8]  = (((BYTE)(usTmp>>8) & 0x0ff));

	pFingerTx = &FingerTx.PACKET.Start_st;

	//UartSendTCS4K(*pFingerTx);
	tcs4k_tx_byte = *pFingerTx;
	HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );

	return 1;
}


BYTE PTCON_SEND( void )
{
	WORD usTmp;
	
	FingerTx.PACKET.Start_st = 0x02;

	FingerTx.PACKET.Command_st[0] = LL_FRAME_DATA;		//data 형태 
	
	FingerTx.PACKET.Command_st[1] = (gbSQNum<<4);	//SQ
	FingerTx.PACKET.Command_st[2] = 0x08;	// data 길이

	FingerTx.PACKET.Parameter_st[0] = 0x28;	// first & last fragment
	FingerTx.PACKET.Parameter_st[1] = 0x05;	// data 길이
	FingerTx.PACKET.Parameter_st[2] = 0x00;

	FingerTx.PACKET.Parameter_st[3] = 0x00;
	FingerTx.PACKET.Parameter_st[4] = 0x00;
	FingerTx.PACKET.Parameter_st[5] = 0x00;
	FingerTx.PACKET.Parameter_st[6] = 0x30;	//  CL_GUI_RESPONSE 	
	
	FingerTx.PACKET.Parameter_st[7] = PT_CONTINUE;	// continue...
	
	gbUart6TX_data = 0x08 + 6;
	usTmp=0;
	usTmp=PTCRC(gbUart6TX_data-2);	
	FingerTx.PACKET.Parameter_st[8] = ((BYTE)(usTmp & 0x0ff)) ;

	FingerTx.PACKET.Parameter_st[9]  = (((BYTE)(usTmp>>8) & 0x0ff));

	pFingerTx = &FingerTx.PACKET.Start_st;

	//UartSendTCS4K(*pFingerTx);
	tcs4k_tx_byte = *pFingerTx;
	HAL_UART_Transmit_IT( gh_mcu_uart_finger, &tcs4k_tx_byte, sizeof(tcs4k_tx_byte) );

	return 1;
}



//이전 코드에선 void FingerStatusLoad(void) 함수로 EEPROM에 저장된 지문의 수를 읽어 들인다.
BYTE GetNoFingerFromEeprom( void )
{
	BYTE	no_eeprom_key;

	RomRead(&no_eeprom_key, (WORD)KEY_NUM, 1);					// 현재 등록된 지문수 불러오기..
	return	no_eeprom_key;
}


uint32_t	dbg_x;
void BioCommCheckRetry(BYTE bsave)
{
	BYTE bTmp=0;
	BYTE	no_eeprom_key;

	// 지문 안정화 
	BioModuleOff();
	Delay(SYS_TIMER_30MS);	
	//POWER ON, Uart On
	BioModuleON();
	Delay(SYS_TIMER_30MS);
	//Rx Port Check
	BioReTry();
	Delay(SYS_TIMER_100MS);
	Delay(SYS_TIMER_100MS);
//	WatchdogCntClear();
	Delay(SYS_TIMER_100MS);
	Delay(SYS_TIMER_100MS);
//	WatchdogCntClear();
	Delay(SYS_TIMER_100MS);
	//Rx Port Check

	if(!P_FP_DDL_RX_T) {
		//RX가 LOW 일 경우
		gfBioErr = 1;
		return;
	}

	pFingerRx = &FingerRx.PACKET.Start_st;
	if(!uart_reset_interface()){
		//RESET을 안 되엇을 ㄷ대
		gfBioErr = 1;
		dbg_x = __LINE__;
		return;
	}

	gbBioTimer10ms = 20;
	while(!gfUART0RxEnd){
		gbBioTimer10ms--;
		Delay(SYS_TIMER_2MS);
		 if(gbBioTimer10ms==0){
			gfBioErr = 1;
			dbg_x = __LINE__;
			return;
		}	
	}	

	SetupTCS4k_UART();
	Delay(SYS_TIMER_20MS);
	FingerRxInterruptRequest();

	gfUART0RxEnd=0;
	pFingerRx = &FingerRx.PACKET.Start_st+1;
	
	if(*pFingerRx != 0x03){
		gfBioErr = 1;
		dbg_x = __LINE__;
		return;
	}

	pFingerRx = &FingerRx.PACKET.Start_st+4;

	if(*pFingerRx != 0x01){
		gfBioErr = 1;
		dbg_x = __LINE__;
		return;
	}
	Delay(SYS_TIMER_15MS);				//15_09_30 부팅시 PT OPEN Error를 막기 위해 딜레이 추가
	pFingerRx = &FingerRx.PACKET.Start_st;
	PTOPEN_SEND();

	gbBioTimer10ms = 250;
	while(!gfUART0TxEnd){
		gbBioTimer10ms--;
		Delay(SYS_TIMER_4MS);
		if(gbBioTimer10ms==0){
			gfBioErr = 1;
			dbg_x = __LINE__;
			return;
		}	
	}	
	gfUART0TxEnd=0;
	gbBioTimer10ms = 250;	
	while(!gfUART0RxEnd){
		gbBioTimer10ms--;
		Delay(SYS_TIMER_2MS);
		if(gbBioTimer10ms==0){
			gfBioErr = 1;
			dbg_x = __LINE__;
			return;
		}	
	}
	gfUART0RxEnd=0;

	pFingerRx = &FingerRx.PACKET.Start_st+1;
	
	if(*pFingerRx != 0x05){
		gfBioErr = 1;
		dbg_x = __LINE__;
		return;
	}

	pFingerRx = &FingerRx.PACKET.Start_st+4;

	if(*pFingerRx != 0x01){
		gfBioErr = 1;
		dbg_x = __LINE__;
		return;
	}
//////////////////////////////////////////////////////////
	Delay(SYS_TIMER_15MS);				//15_09_30 부팅시 PT OPEN Error를 막기 위해 딜레이 추가

	pFingerRx = &FingerRx.PACKET.Start_st;

	PTALLLIST_SEND();
	gbBioTimer10ms = 300;
	while(!gfUART0TxEnd){
		gbBioTimer10ms--;
		Delay(SYS_TIMER_4MS);
		if(gbBioTimer10ms==0){
			gfBioErr = 1;
			dbg_x = __LINE__;
			return;
		}	
	}	
	bTmp=0;
	gfUART0TxEnd=0;
	gbBioTimer10ms = 600;
	gfBioRegnumErr=0;
	no_eeprom_key = GetNoFingerFromEeprom();
	while(!gfUART0RxEnd){
		gbBioTimer10ms--;
		Delay(SYS_TIMER_2MS);

		if(pFingerRx >= (&FingerRx.PACKET.Start_st + 15)){ //finger count 읽고 난후 	
			bTmp = *(&FingerRx.PACKET.Start_st + 11); // finger count 를 가지고 비교 
			//bTmp=((*pFingerRx-3)/8)-1;
		
			if(no_eeprom_key != bTmp)
			{
				if(bsave == 0)
				{
					gfBioRegnumErr=1;  					
				}
				else if(bsave == 1) //biocommcheck retry해서 지문 모듈에 등록 된 개수와   eeprom 개수가 다르면 지문 모듈 갯수를 eeprom에 write  
				{
					RomWrite(&bTmp, (WORD)KEY_NUM, 1);
					gfBioRegnumErr = 0;
					//EEPROM 에  등록 개수 저장 하기.....
				}
#if 0
				if(gfFailRegBio==1){		//지문 메모리 가득 상황시 20개로 등록 못하게 함 
					bTmp=20;
				}
				
				if(bsave ==1){
				//if((bTmp>=21) ||(gbSecurity==1)){
					if(bTmp>=21)
					{
						no_eeprom_key=20;
						RomWrite(&no_eeprom_key, (WORD)KEY_NUM, 1);		// 넣어야 될 것 같음..	보안 모드는 모조건 지우기 
					}else{
						no_eeprom_key = bTmp;
						RomWrite(&no_eeprom_key, (WORD)KEY_NUM, 1);		// 넣어야 될 것 같음..	일반 모드에서 는 그냥 사용 가능 하게 
						//EEPROM 에  등록 개수 저장 하기.....
					}
				}			
				gfBioRegnumErr=1;
#endif
			}
			return;
		}

		if(gbBioTimer10ms==0){
			gfBioErr = 1;
			dbg_x = __LINE__;
			return;
		}	
	}
	gfUART0RxEnd=0;
	gfBioErr = 0;
	BioModuleOff();
}

void BioCommCheck(BYTE cnt)
{
	BioCommCheckRetry(0); 
	if(gfBioRegnumErr == 1)
	{
		BioCommCheckRetry(1);  //biocommcheck retry해서 지문 모듈에 등록 된 개수와   eeprom 개수가 다르면 지문 모듈 갯수를 eeprom에 write  
	}
}


