#ifndef		_FINGER_TCS4K_H_
#define		_FINGER_TCS4K_H_

#include "DefineMacro.h"
//#include "DefinePin.h"


#define BIO_WAKEUP_TM		250

#define LL_CHAR_STX                     0x02
#define LL_CHAR_DLE                     0x10
#define LL_CHAR_XON                     0x11
#define LL_CHAR_XOFF                    0x13


#define TFM_PROTOCOL_VERSION    (0x01)          /**< Support TFM Communication Protocol Ver 1.1 */

#define PREFERRED_BAUD_RATE     (LL_SIO_BAUDRATE_9600)

#define MAX_LL_FRAME_SIZE       (128)           /**< Maximum number of bytes in a 
			                                                   @b Link-Layer frame */
#define MAX_APP_FRAME_SIZE      (64)            /**< Maximum number of bytes in an application frame,
                                                     including Tag of 4 bytes. Longer frame will be 
                                                     fragmented into several segments of 
                                                     @b MAX_APP_FRAME_SIZE bytes for transmitting to 
                                                     the TFM 
                                                     @par
                                                     Must of power of 2 for programming efficiency */
#define MAX_APP_FRAME_SIZE_SHIFT_BIT (6)        /**< Equal to log2(@b MAX_APP_FRAME_SIZE ) */

///////////////////////////////////////////////////////////////////////////////////////


#define LLSTATUS_OK                     (0)     /**< Function return successfully */
#define LLSTATUS_LINK_ALREADY_OPEN      (1)     /**< The communication channel is already in use */
#define LLSTATUS_READ_CHAR_TIMEOUT      (2)     /**< Error if the first character in a frame is not 
                                                     received within 5s, or if successive characters
                                                     is not received within 100ms */
#define LLSTATUS_WRITE_CHAR_TIMEOUT     (3)     /**< Fails to write a character within 100ms */
#define LLSTATUS_LLFRAME_SIZE_EXCEED    (4)     /**< LLFRAME exceed the allowable size */
#define LLSTATUS_LLFRAME_SYNTAX_ERROR   (5)     /**< LLFRAME syntax error */
#define LLSTATUS_IS_STX                 (6)     /**< An STX is received from TFM */
#define LLSTATUS_WRONG_TFM_VERSION      (7)     /**< The TFM version is not supported */
#define LLSTATUS_SET_HW_PARAM_ERROR     (8)     /**< Fail to set hw parameter */
#define LLSTATUS_WRONG_SEQ_NUMBER       (11)    /**< Receive @b Link-Layer frame sequence number
                                                     does not match the transmitted sequence 
                                                     number */
#define LLSTATUS_GENERAL_ERROR          (255)   /**< Unknown error */


#define LL_FRAME_DATA                   	0x00
#define LL_FRAME_TYPE_ACK               	0x01
#define LL_FRAME_NACK                   	0x02
#define LL_FRAME_ATR                    	0x03
#define LL_FRAME_SET_PARAMS          	0x04
#define LL_FRAME_CONFIRM_PARAMS  	0x05
#define LL_FRAME_CLOSE                  	0x07
#define LL_FRAME_WTX                    	0x08
#define LL_FRAME_CONTINUE               	0x09

#define LL_SIO_BAUDRATE_INVALID         0
#define LL_SIO_BAUDRATE_9600            1
#define LL_SIO_BAUDRATE_19200           2
#define LL_SIO_BAUDRATE_38400           3
#define LL_SIO_BAUDRATE_57600           4
#define LL_SIO_BAUDRATE_115200          5
#define LL_SIO_BAUDRATE_MIN             LL_SIO_BAUDRATE_9600
#define LL_SIO_BAUDRATE_MAX             LL_SIO_BAUDRATE_115200

#define LL_RX_STATE_UNKNOWN             (1)     /**< Unknown state */                    
#define LL_RX_STATE_STX                 	(2)     /**< Waiting to receive STX */
#define LL_RX_STATE_LLFRAME_HEADER      (3)     /**< Waiting to receive LLFRAME header */
#define LL_RX_STATE_CRC                 (4)     /**< Waiting to receive CRC */
#define LL_RX_STATE_TLFRAME_HEADER      (5)     /**< Waiting to receive TLFRAME header */
#define LL_RX_STATE_TLFRAME_LENGTH      (6)     /**< Waiting to receive TLFRAME packet length */
#define LL_RX_STATE_DATA                (8)     /**< Waiting to receive LLFRAME/TLFRAME data */


#define TLSTATUS_OK                     (0)     /**< Function return successfully */
#define TLSTATUS_CANNOT_CONNECT         (1)     /**< Connection error with TFM */
#define TLSTATUS_READ_CHAR_TIMEOUT      (2)     /**< Error if the first character in a frame 
                                                     is not received within 5s, or if successive 
                                                     characters is not received within 100ms */
#define TLSTATUS_WRITE_CHAR_TIMEOUT     (3)     /**< Fails to write a character within 100ms */
#define TLSTATUS_TLFRAME_SIZE_EXCEED    (4)     /**< @b Transport-Layer frame exceed the 
                                                     allowable number of bytes */
#define TLSTATUS_GENERAL_ERROR          (255)   /**< Unknown error */


/** Success return status */
#define PT_STATUS_OK                    (0)
/** Cannot connect to TFM */
#define PT_STATUS_CANNOT_CONNECT        (1)
/** Timeout elapsed */
#define PT_STATUS_TIMEOUT               (2)
/** Passed data are too large */
#define PT_STATUS_DATA_TOO_LARGE        (3)
/** Requested slot was not found */
#define PT_STATUS_SLOT_NOT_FOUND        (4)
/** TFM has returned wrong or unexpected response */
#define PT_STATUS_WRONG_RESPONSE        (5)
/** No finger match */
#define PT_STATUS_FINGER_NO_MATCH       (6)
/** General or unknown error status. It is also possible that the function 
    only partially succeeded, and that the device is in an inconsistent state. */
#define PT_STATUS_GENERAL_ERROR         (255)

/** Maximal possible length of application data associated with finger 
    stored in TFM's non-volatile memory. */
#define PT_MAX_FINGER_DATA_LENGTH       (96)

/**
 * CL Frame Type
 */
#define CL_COMMAND                      (0x00)
#define CL_RESPONSE                     (0x01)
#define CL_GUI_CALLBACK                 (0x02)
#define CL_GUI_RESPONSE                 (0x03)
#define CL_ENCRYPTED                    (0x08)

/**
 * Flag for finger detect
 */
#define BD_WAIT_GOOD_FINGER             (0x01)


/**
 * Flags for enroll and capture
 */
#define BE_HAS_STORED_TEMPLATE          (0x01)
#define BE_HAS_PAYLOAD                  (0x02)
#define BE_HAS_SIGN_DATA                (0x04)
#define BE_STORE_TEMPLATE               (0x08)
#define BE_RETURN_AUDIT_DATA            (0x0100)
#define BE_RETURN_SIGNATURE             (0x0200)
#define BE_RETURN_TEMPLATE              (0x0800)


/**
 * Flags for verification 
 */
#define BV_HAS_PAYLOAD                  (0x2)
#define BV_HAS_SIGN_DATA                (0x4)
#define BV_USE_FRR_MAX                  (0x8)
#define BV_HAS_CAPTURED_TEMPLATE        (0x10)
#define BV_CAPTURE_FINGERPRINT          (0x20)
#define BV_RETURN_AUDIT_DATA            (0x100)
#define BV_RETURN_SIGNATURE             (0x200)
#define BV_RETURN_PAYLOAD               (0x400)
#define BV_RETURN_ADAPTED_TEMPLATE      (0x800)
#define BV_RETURN_FRR_ACHIEVED          (0x1000)
#define BV_RETURN_FAR_ACHIEVED          (0x2000)
#define BV_USE_ANTISPOOFING             (0x8000)


/**
 * Flags for store template
 */
#define BS_HAS_TEMPLATE                 (0x1)



/**
 * BIR and biometric operations purposes.
 */
#define PT_PURPOSE_VERIFY                           (1)
#define PT_PURPOSE_IDENTIFY                         (2)
#define PT_PURPOSE_ENROLL                           (3)
#define PT_PURPOSE_ENROLL_FOR_VERIFICATION_ONLY     (4)
#define PT_PURPOSE_ENROLL_FOR_IDENTIFICATION_ONLY   (5)
#define PT_PURPOSE_AUDIT                            (6)


/**
 * This is value indicating that message parameter contains 
 * valid value.
 */
#define PT_MESSAGE_PROVIDED             (0x1)

#define PT_GUIMSG_GOOD_IMAGE            (0)
#define PT_GUIMSG_NO_FINGER             (1)
#define PT_GUIMSG_TOO_LIGHT             (2)
#define PT_GUIMSG_TOO_DRY               (3)
#define PT_GUIMSG_TOO_DARK              (4)
#define PT_GUIMSG_TOO_HIGH              (5)
#define PT_GUIMSG_TOO_LOW               (6)
#define PT_GUIMSG_TOO_LEFT              (7)
#define PT_GUIMSG_TOO_RIGHT             (8)
#define PT_GUIMSG_TOO_SMALL             (9)
#define PT_GUIMSG_TOO_STRANGE           (10)
#define PT_GUIMSG_BAD_QUALITY           (11)
#define PT_GUIMSG_PUT_FINGER            (12)
#define PT_GUIMSG_PUT_FINGER2           (13)
#define PT_GUIMSG_PUT_FINGER3           (14)
#define PT_GUIMSG_REMOVE_FINGER         (15)
#define PT_GUIMSG_CLEAN_SENSOR          (16)
#define PT_GUIMSG_KEEP_FINGER           (17)
#define PT_GUIMSG_BEGIN_GUI             (18)
#define PT_GUIMSG_END_GUI               (19)
#define PT_GUIMSG_GOOD_REG             (32)
#define PT_GUIMSG_PUT_FINGER4           (38)
#define PT_GUIMSG_PUT_FINGER5           (39)

#define PT_CANCEL                       (0)
#define PT_CONTINUE                     (1)

#define	PT_FULLBIR_INPUT	            (3)
#define	PT_SLOT_INPUT		            (128)


#define _MAX_REG_BIO	20


extern  FLAG BioFlag;

#define gfModuleOK		BioFlag.STATEFLAG.Bit0						//정상적인 모듈
#define gfUART0TxEnd	BioFlag.STATEFLAG.Bit1
#define gfUART0RxEnd	BioFlag.STATEFLAG.Bit2
#define gfBioCoverIn		BioFlag.STATEFLAG.Bit3
#define gfBioCoverDown	BioFlag.STATEFLAG.Bit4
#define gfBioUARTST		BioFlag.STATEFLAG.Bit5
#define gfBioUARTStuff	BioFlag.STATEFLAG.Bit6
#define gfBioTXStuff		BioFlag.STATEFLAG.Bit7



typedef union{
	struct{
		BYTE Start_st;
		BYTE Command_st[3];
		BYTE Parameter_st[40];
	}PACKET;
}FINGERPACKET;	

#define FINGER_MODE_ALL_DELETE		1
#define FINGER_MODE_EACH_DELETE		2
#define FINGER_MODE_ALL_REGISTER			3
#define FINGER_MODE_EACH_REGISTER	4

#define FINGER_MODE_VERIFY				5

#define FINGER_MODE_ALL_DELETE_BYBLEN		6
#define FINGER_MODE_EACH_DELETE_BYBLEN		7
#define FINGER_MODE_ALL_REGISTER_BYBLEN			8
#define FINGER_MODE_EACH_REGISTER_BYBLEN	9

#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
#define FINGER_COVER_OPENING			1
#define FINGER_COVER_OPENED				2
#define FINGER_COVER_CLOSING			3
#define FINGER_COVER_CLOSED				4
#define FINGER_COVER_CLOSED_ERROR		5

#define _OPEN	1
#define _CLOSE	2
#endif




extern  FINGERPACKET FingerRx;
extern  FINGERPACKET FingerTx;

extern  BYTE *pFingerTx;
extern  BYTE *pFingerRx;
extern  BYTE BioRetryCnt;
extern  BYTE gbUart6_data;
extern  DWORD gBioEnrollCnt;

//extern BYTE gbSecurity;	

extern WORD gbBioTimer10ms;

extern BYTE gbSQNum;

extern BYTE gbErrorCnt;

extern BYTE gbREG_CNT;
extern BYTE gbBIOErrCheck;

extern BYTE gbBIOErrorCNT ;
extern BYTE gbBIORMCNT;
extern BYTE gbFID;

extern BYTE gbFingerPTMode;
extern BYTE gbFingerLongTimeWait100ms;
extern BYTE gbLonOnID[8];
extern BYTE gbCoverOn;
extern BYTE gbCoverOld;
extern BYTE gbError ;
extern BYTE gbBIOCONCNT;

extern WORD gbfingerCloseDelaycnt;
extern BYTE gbfingerCloseDelayflag;


extern BYTE gbUart6TX_data;

#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
extern BYTE gbFingerCoverState; //모터커버 상태
extern BYTE gbFingerCoverCloseRetryCnt; //모터커버 에러 카운트
extern BYTE gbFingerMode; //모터커버 프로세스 스텝

void FPMotorCoverProcess(void);
void FPMotorCoverAction(BYTE direction);
void FPMotorCoverCloseRetry(void);
#endif


void FingerRxInterruptRequest( void );
void InitialSetupFingerModule( void );
void FPCoverSwProcess(void);

void BioModuleON( void );
void BioModuleOff( void );
void BioOffModeClear( void );
void BioReTry( void );

BYTE  uart_reset_interface(void);
BYTE PTOPEN_SEND( void );
BYTE PTENROLL_SEND( BYTE upper_id, BYTE lower_id, BYTE no_fp );
BYTE PTVERIFYALL_SEND( void );
BYTE PTDEL_SEND(BYTE DID);
BYTE PTDELALL_SEND( void );
BYTE PTCON_SEND( void );


BYTE GetNoFingerFromEeprom( void );
void BioCommCheck(BYTE cnt);
void BioCommCheckRetry(BYTE bsave);



#endif

