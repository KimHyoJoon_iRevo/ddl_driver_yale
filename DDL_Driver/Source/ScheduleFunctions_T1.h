#ifndef	__SCHEDULEFUNCTIONS_T1_INCLUDED
#define	__SCHEDULEFUNCTIONS_T1_INCLUDED

#include "DefineMacro.h"
//#include "Definepin.h"




#define	DAYOFWEEK_SUN		(1<<6)
#define	DAYOFWEEK_MON		(1<<5)
#define	DAYOFWEEK_TUE		(1<<4)
#define	DAYOFWEEK_WED		(1<<3)
#define	DAYOFWEEK_THU		(1<<2)
#define	DAYOFWEEK_FRI		(1<<1)
#define	DAYOFWEEK_SAT		(1<<0)


#if (DDL_CFG_BLE_30_ENABLE >= 0x29) && defined (_USE_ST32_EEPROM_)

#define	NUM_OF_YEARDAY				1
#define	NUM_OF_DAILY_REPEAT			1

#define	BYTENUM_OF_YEARDAY			13
#define	BYTENUM_OF_DAILY_REPEAT		6


BYTE SlotNumberCheck(BYTE CredentialType, BYTE SlotNumber);
BYTE SlotNumberExtendCheck(BYTE *SlotNumber);

BYTE GetUserSchduleStatus(BYTE *SlotNumber);
void SetDailyRepeatingSchdule(BYTE *SlotNumber, BYTE *bScheduleData);
void SetYearDaySchdule(BYTE *SlotNumber, BYTE *bScheduleData);
void SetDailyRepeatingEnable(BYTE *SlotNumber, BYTE *bEnableData);
void SetYearDayEnable(BYTE *SlotNumber, BYTE *bEnableData);
BYTE GetDailyRepeatingSchdule(BYTE *SlotNumber, BYTE *bScheduleData);
BYTE GetYearDaySchdule(BYTE *SlotNumber, BYTE *bScheduleData);


BYTE ScheduleEnableCheck_Ble30(BYTE CredentialType, BYTE bSlotNum);
BYTE ScheduleVerify_Ble30(BYTE CredentialType, BYTE *pCurrentTimeBuf, BYTE bSlotNum);

void ScheduleDelete_Ble30(BYTE CredentialType, BYTE bSlotNum);
void ScheduleDeleteOfAllUserCode(void);
void ScheduleDeleteOfAllUserCard(void);
void ScheduleDeleteOfAllUserFingerprint(void);


#endif

#endif



