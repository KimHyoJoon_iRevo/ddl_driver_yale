//------------------------------------------------------------------------------
/** 	@file		ddl_config-rose2-ih.h
	@version 0.1.00
	@date	2016.06.03
	@brief	DDL의 전체 구성에 대한 Macro를 정의한다.
	@see
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00	2016.06.03		by Jay
			- 모델별로 ddl_config.h를 사용하기 위해 기존 ddl_config.h의 내용이 옮겨옴.
*/
//------------------------------------------------------------------------------

#ifndef		_DDL_CONFIG_ROSE2_IH_H_
#define		_DDL_CONFIG_ROSE2_IH_H_


//------------------------------------------------------------------------------
// Ten Key 입력형태에 대한 정의

// 정전방식 터치키
//#define		DDL_CFG_TOUCHKEY

#ifdef		DDL_CFG_TOUCHKEY
	#define		DDL_CFG_TOUCHKEY_TSM12

	//Z10-IH, WF20-DMP, A20-IH, E300-FH, WV-200
	#define		DDL_CFG_KEY_ARRANGE_TYPE1

	//J20-IH, WV40-DMP, WE40-DMP, WE30-DMP
//	#define 	DDL_CFG_KEY_ARRANGE_TYPE2
#endif

// push switch
#define		DDL_CFG_PUSHKEY

#ifdef		DDL_CFG_PUSHKEY
	#define		DDL_CFG_PUSHKEY_TYPE1
#endif


//------------------------------------------------------------------------------
// Front LED Dimmer 정의

//#define		DDL_CFG_DIMMER

#ifdef		DDL_CFG_DIMMER
	// 16bit I2C LED dimmer PCA9532
	#define		DDL_CFG_DIMMER_PCA9532
#endif

// 개별 라이팅이 아닌 방식
#define		DDL_CFG_NO_DIMMER

#ifdef		DDL_CFG_NO_DIMMER 
	// Front LED_DECO, LED_TENKEY
	//	Rose2_IH
	#define 	DDL_CFG_LED_TYPE1

	// Front LED_TENKEY, LED_LOWBAT, LED_DEAD
	//	J20-IH, WV40-DMP, WE40-DMP, WV-200 
//	#define 	DDL_CFG_LED_TYPE2

	// Front LED_TENKEY, LED_LOWBAT, LED_DEAD, LED_IB
	//	WE30-DMP
//	#define DDL_CFG_LED_TYPE3

	// Front LED_DECO, LED_TENKEY, 커버를 올리면 LED_DECO가 보이지 않아 차이를 둠.
	//	Edge2-DMP
//	#define DDL_CFG_LED_TYPE4

#endif


//------------------------------------------------------------------------------
// 2way 인증시 Ten Key 이외의 인증 수단 방법 정의

// RFID
#define		DDL_CFG_RFID

#ifdef		DDL_CFG_RFID
	#define		DDL_CFG_RFID_TRF7970A
	#define		DDL_CFG_RFID_PARALLEL

//------------------------------------------------------------------------------
/*! 	\def	IREVO_CARD_ONLY 	
	아이레보 카드와 같은 Type의 IC만 대응하고자 할 경우에 Define을 설정 (4byte, 7byte UID 모두 대응 가능)
	OPEN UID 설정을 위해서는 해당 Define을 해제 (4byte, 7byte UID 모두 대응 가능)
*/
//------------------------------------------------------------------------------
	#define		IREVO_CARD_ONLY		

#endif


// 지문
//#define		DDL_CFG_FP

#ifdef		DDL_CFG_FP
	#define		_MAX_REG_BIO		20

	#define		DDL_CFG_FP_TCS4K
#endif

// I-BUTTON
//#define		DDL_CFG_IBUTTON
//#define		DDL_CFG_DS1972_IBUTTON




//------------------------------------------------------------------------------
//	Stop Mode에서 깨어나는 RTC wakeup time 지정

/*
	STM32L151의 경우 RTCCLK는 37kHz
	Wakeup clock으로 RTCCLK/16을 사용하는 경우,
	tick당 시간은 1 / (37000/16) = 1/2312.5 = 0.000432 sec

	Example) 1.7sec wakeup time := 3931
*/
#define	DDL_CFG_RTC_WAKUPTIME	3472
//#define	DDL_CFG_RTC_WAKUPTIME	10000



//------------------------------------------------------------------------------
//	모터 구동 정의

//보조키 모터 (IH, DM)
#define	DDL_CFG_MOTOR_TYPE1


// PIN 최소 최대 길이를 정의 한다. 이곳에서 정의 되어 있지 않으면,
// PincodeFunctions.h에 정의된 default가 사용된다.
// DDL_CFG_SUPPORT_CHINESE 를 define 한다는 것은 중국어 등록은 6자리 제한 / 인증은 4자리 제한을 두기 위함 이다. 

#define	MIN_PIN_LENGTH			4
#define	MAX_PIN_LENGTH			10

#define	DDL_CFG_SUPPORT_CHINESE 

//------------------------------------------------------------------------------
//	Touch O/C 정의 

#define	DDL_CFG_TOUCH_OC



//------------------------------------------------------------------------------
//	MUTE 키 정의

#define	DDL_CFG_MUTE_KEY



//------------------------------------------------------------------------------
//	Ten Key Cover Switch 정의

#define	DDL_CFG_COVER_SWITCH

#ifdef	DDL_CFG_COVER_SWITCH
//	#define		DDL_CFG_COVER_ACTIVE_HIGH	// Edge2-DM+ -> Cover Up Active High
											// Rose2-IH -> Cover Up Active Low
#endif

//------------------------------------------------------------------------------
//	High Temperature Sensor 정의

#define	DDL_CFG_HIGHTEMP_SENSOR


//------------------------------------------------------------------------------
//	멀티 터치에 의한 일회용 무음 모드 지원

#ifdef	DDL_CFG_TOUCHKEY
	#define	DDL_CFG_ONETIME_MUTE
#endif



//------------------------------------------------------------------------------
//	와치독 정의

#define	DDL_CFG_WATCHDOG


//------------------------------------------------------------------------------
//	Buzzer Volume Type 정의
#define	DDL_CFG_BUZ_VOL_TYPE1
//#define	DDL_CFG_BUZ_VOL_TYPE2



//------------------------------------------------------------------------------
//	Inside Low Battery LED 정의
//#define	DDL_CFG_INSIDE_LED_LOW





//------------------------------------------------------------------------------
//	Lock Setting Menu 정의
//
//#define		DDL_CFG_LOCK_SET_TYPE1	

//Z10-IH, WF20-DMP, A20-IH, Rose2-IH, Edge2-DMP, J20-IH, WV40-DMP, WE40_DMP, WE40-DMP 
//WV-200, WF-200
#define 	DDL_CFG_LOCK_SET_TYPE2

//E300-FH
//#define 	DDL_CFG_LOCK_SET_TYPE4




//------------------------------------------------------------------------------
//	BLE-N 정의
#define	BLE_N_SUPPORT


//------------------------------------------------------------------------------
//	Abov voice index type define
//
//	AVML_MSG_T1 : 2016년 상반기에 정의된 list
//	AVML_MSG_T2 : 2016년 하반기에 정의된 list

//#define	AVML_MSG_T1
#define	AVML_MSG_T2



//------------------------------------------------------------------------------
//	지문 주키 제품을 위한 마스터키 iButton 기능 정의
//#define	_MASTERKEY_IBUTTON_SUPPORT



//------------------------------------------------------------------------------
//	사용자 비밀번호 스케줄 기능 및 Lockout 기능 지원 정의
#define	_PINCODE_SCHEDULE_SUPPORT



//------------------------------------------------------------------------------
//	음성 안내 정의
// 한국어, 중국어 정의 (중국 생산으로 중국어 기본)
//#define	DDL_CFG_LANGUAGE_SET_TYPE1

// 한국어, 중국어, 영어, 스페인어, 포르투갈어, 대만어 정의 (영어 기본)
//#define	DDL_CFG_LANGUAGE_SET_TYPE2

// 한국어, 중국어 정의 (한국 생산으로 한국어 기본)
// E300-FH, WF-200, WS-200
//#define	DDL_CFG_LANGUAGE_SET_TYPE3

#endif

