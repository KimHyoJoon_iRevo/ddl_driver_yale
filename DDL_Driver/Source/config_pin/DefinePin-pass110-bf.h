#ifndef __DEFINEPIN_PASS110_F_INCLUDED
#define __DEFINEPIN_PASS110_F_INCLUDED

#include	"DefineMacro.h"


// DefinePin-a20-ih.h 파일과의 차이점 
//	- SW_AUTO Pin 활성화
//	- SNS_LOCK pin 활성화


//--------------------------------------------------------------------
//	STM32L151VBTxA LQFP100 Pin Assignment
//--------------------------------------------------------------------

#define 	P_JIG_POINT_T		HAL_GPIO_ReadPin(JIG_POINT_GPIO_Port, JIG_POINT_Pin)										// 1 jig pint

#ifdef		DDL_CFG_IBUTTON
#define 	P_IBUTTON_T			HAL_GPIO_ReadPin(I_BUTTON_GPIO_Port, I_BUTTON_Pin)											// 2  I-Buton pint
#endif

#if defined	(DDL_CFG_FP_TCS4K) || defined	(DDL_CFG_FP_PFM_3000)  //hyojoon_20160831 PFM-3000 add
#define 	P_IBUTTON_COVER_T	HAL_GPIO_ReadPin(IBUTTON_COVER_RFID_GND_GPIO_Port, IBUTTON_COVER_RFID_GND_Pin)				// 2 (IBUTTON/COVER)
#endif

#define 	P_OP_RFID_T			HAL_GPIO_ReadPin(OP_RFID_GPIO_Port, OP_RFID_Pin)											// 3 (Option RFID)
#define 	P_OP_RFID(A)		HAL_GPIO_WritePin(OP_RFID_GPIO_Port, OP_RFID_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)			// 3 (Option RFID)
#define 	P_OP_FP_T			HAL_GPIO_ReadPin(OP_FP_GPIO_Port, OP_FP_Pin)												// 4 (Option FingerPrint)
#define 	P_OP_FP(A)			HAL_GPIO_WritePin(OP_FP_GPIO_Port, OP_FP_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)				// 4 (Option FP)
#define 	P_OP_VOICE_T		HAL_GPIO_ReadPin(OP_VOICE_GPIO_Port, OP_VOICE_Pin)											// 5 (Option Voice)
#define 	P_OP_VOICE(A)		HAL_GPIO_WritePin(OP_VOICE_GPIO_Port, OP_VOICE_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 5 (Option Voice)
																															// 6 VLCD
#define 	P_COM_DDL_EN_T		HAL_GPIO_ReadPin(COM_DDL_EN_GPIO_Port, COM_DDL_EN_Pin)										// 7 (COM_DLL_EN)
#define 	P_BT_WAKEUP_T		HAL_GPIO_ReadPin(BT_WAKEUP_GPIO_Port, BT_WAKEUP_Pin)										// 8 TP18 (for #INT14)

																															// 9 TP17 (fot #INT15)
																															// 10 VSS_5
																															// 11 VDD_5
#ifdef		DDL_CFG_BUZ_VOL_TYPE2
#define 	P_VOL_OUT(A)		HAL_GPIO_WritePin(VOL_OUT_GPIO_Port, VOL_OUT_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)			// 12 (VOL_OUT)
#endif

#define 	P_LOWBAT_EN(A)		HAL_GPIO_WritePin(LOW_BAT_EN_GPIO_Port, LOW_BAT_EN_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 13 (LOWBAT_EN)
																															// 14 (MCU_RST)
#ifdef		DDL_CFG_MUTE_KEY
#define 	P_MUTE_T			HAL_GPIO_ReadPin(MUTE_GPIO_Port, MUTE_Pin)													// 15 (MUTE)
#endif


#ifdef		DDL_CFG_COVER_SWITCH
#define 	P_SW_COVER_T		HAL_GPIO_ReadPin(SW_COVER_GPIO_Port, SW_COVER_Pin)											// 15 (SW_COVER)
#endif

//#define 	P_LED_RST_RFID_DETECT_T	HAL_GPIO_ReadPin(LED_RST_RFID_DETECT_GPIO_Port, LED_RST_RFID_DETECT_Pin)				// 15 (LED_RST#RFID_DETECT)

#ifdef		DDL_CFG_BUZ_VOL_TYPE2
#define 	P_VOL_IN_T			HAL_GPIO_ReadPin(VOL_IN_GPIO_Port, VOL_IN_Pin)												// 16 (VOL_IN)
#endif

#ifdef		DDL_CFG_LED_TYPE2
#define 	P_LED_DEAD(A)		HAL_GPIO_WritePin(LED_DEAD_GPIO_Port, LED_DEAD_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 15 (LED_DEAD)
#endif

#ifdef		DDL_CFG_LED_TYPE3
#define 	P_LED_DEAD(A)		HAL_GPIO_WritePin(LED_DEAD_GPIO_Port, LED_DEAD_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 15 (LED_DEAD)
#endif

#ifdef		DDL_CFG_HIGHTEMP_SENSOR
#define 	P_FIRE_AD_T			HAL_GPIO_ReadPin(FIRE_AD_GPIO_Port, FIRE_AD_Pin)											// 17 (FIRE_AD)
#endif

#define 	P_LOWBAT_AD_T		HAL_GPIO_ReadPin(LOWBAT_AD_GPIO_Port, LOWBAT_AD_Pin)										// 18 (LOWBAT_AD)
																															// 19 VSSA
																															// 20 VREF-
																															// 21 VREF+
																															// 22 VDDA
																															// 23 TP2 (for #ADC_IN0)
																															// 24 TP3 (for #ADC_IN1)
																															// 25 TP4 (for #UART2_TX)
																															// 26 TP1 (for #UART2_RX)
																															// 27 VSS_4

																															// 28 VDD_4
#ifdef	DDL_CFG_RFID
//#define 	P_RFID_SPI_NSS_T	HAL_GPIO_ReadPin(RFID_SPI_NSS_GPIO_Port, RFID_SPI_NSS_Pin)									// 29 (RFID_SPI-NSS)
//#define 	P_RFID_SPI_NSS(A)	HAL_GPIO_WritePin(RFID_SPI_NSS_GPIO_Port, RFID_SPI_NSS_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)// 29 (RFID_SPI-NSS)
//#define 	P_RFID_SPI_CLK(A)	HAL_GPIO_WritePin(RFID_SPI_CLK_GPIO_Port, RFID_SPI_CLK_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 30 (RFID_SPI_CLK)
//#define 	P_RFID_SPI_MISO(A)	HAL_GPIO_WritePin(RFID_SPI_MISO_GPIO_Port, RFID_SPI_MISO_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 31 (RFID_SPI_MISO)
//#define 	P_RFID_SPI_MOSI(A)	HAL_GPIO_WritePin(RFID_SPI_MOSI_GPIO_Port, RFID_SPI_MOSI_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 32 (RFID_SPI_MOSI)
#define P_RFID_DATA0(A)	HAL_GPIO_WritePin(RFID_IO00_GPIO_Port, RFID_IO00_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 23
#define P_RFID_DATA1(A)	HAL_GPIO_WritePin(RFID_IO00_GPIO_Port, RFID_IO01_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 24
#define P_RFID_DATA2(A)	HAL_GPIO_WritePin(RFID_IO00_GPIO_Port, RFID_IO02_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 25
#define P_RFID_DATA3(A)	HAL_GPIO_WritePin(RFID_IO00_GPIO_Port, RFID_IO03_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 26
#define P_RFID_DATA4(A)	HAL_GPIO_WritePin(RFID_IO00_GPIO_Port, RFID_IO04_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 29
#define P_RFID_DATA5(A)	HAL_GPIO_WritePin(RFID_IO00_GPIO_Port, RFID_IO05_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 30
#define P_RFID_DATA6(A)	HAL_GPIO_WritePin(RFID_IO00_GPIO_Port, RFID_IO06_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 31
#define P_RFID_DATA7(A)	HAL_GPIO_WritePin(RFID_IO00_GPIO_Port, RFID_IO07_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 32
#define P_RFID_CLK(A)		HAL_GPIO_WritePin(RFID_CLK_GPIO_Port, RFID_CLK_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 85
#define P_RFID_IRQ_T		HAL_GPIO_ReadPin(RFID_IRQ_GPIO_Port, RFID_IRQ_Pin)										// 34
#define P_RFID_EN(A)		HAL_GPIO_WritePin(RFID_EN_GPIO_Port, RFID_EN_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 81
#define P_RFID_EN2(A)		HAL_GPIO_WritePin(RFID_EN2_GPIO_Port, RFID_EN2_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 84
#endif

//#define		P_SW_AUTO_T			HAL_GPIO_ReadPin(SW_AUTO_GPIO_Port, SW_AUTO_Pin)											// 33 (SW_AUTO)

#ifdef	DDL_CFG_RFID
#define 	P_RFID_IRQ_T		HAL_GPIO_ReadPin(RFID_IRQ_GPIO_Port, RFID_IRQ_Pin)											// 34 (RFID_IRQ)
#endif

#ifdef	DDL_CFG_TOUCHKEY
#define 	P_TKEY_SDA_T		HAL_GPIO_ReadPin(TKEY_SDA_GPIO_Port, TKEY_SDA_Pin)											// 35 (TKEY_SDA)
#define 	P_TKEY_SDA(A)		HAL_GPIO_WritePin(TKEY_SDA_GPIO_Port, TKEY_SDA_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 35 (TKEY_SDA)
#define 	P_TKEY_SCL(A)		HAL_GPIO_WritePin(TKEY_SCL_GPIO_Port, TKEY_SCL_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 36 (TKEY_SCL)
																															// 37 10k GND
#define 	P_TKEY_EN(A)		HAL_GPIO_WritePin(TKEY_EN_GPIO_Port, TKEY_EN_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)			// 38 (TKEY_EN)
#define 	P_TKEY_RST(A)		HAL_GPIO_WritePin(TKEY_RST_GPIO_Port, TKEY_RST_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 39 (TKEY_RST)
#define 	P_TKEY_INT_T		HAL_GPIO_ReadPin(TKEY_INT_GPIO_Port, TKEY_INT_Pin)											// 40 (TKEY_SDA)
#endif

#ifdef		DDL_CFG_PUSHKEY
#define 	P_KEY_AD_2_T		HAL_GPIO_ReadPin(KEY_AD_2_GPIO_Port, KEY_AD_2_Pin)											// 35 (KEY_AD_2)
#define 	P_KEY_AD_1_T		HAL_GPIO_ReadPin(KEY_AD_1_GPIO_Port, KEY_AD_1_Pin)											// 36 (KEY_AD_1)
#endif
																															// 37 10k GND
//#ifdef		DDL_CFG_LED_TYPE1
//#define 	P_LED_DECO(A)		HAL_GPIO_WritePin(LED_DECO_GPIO_Port, LED_DECO_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 38 (LED_DECO)
//#endif
  
#ifdef		DDL_CFG_LED_TYPE4
#define 	P_LED_DECO(A)		HAL_GPIO_WritePin(LED_DECO_GPIO_Port, LED_DECO_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 38 (LED_DECO)
#endif

#ifdef		DDL_CFG_PUSHKEY
#define 	P_KEY_STAR_T		HAL_GPIO_ReadPin(KEY_STAR_GPIO_Port, KEY_STAR_Pin)											// 39 (KEY_STAR)
#endif

//#ifdef		DDL_CFG_COVER_SWITCH
//#define 	P_SW_COVER_T		HAL_GPIO_ReadPin(SW_COVER_GPIO_Port, SW_COVER_Pin)											// 40 (SW_COVER)
//#endif

// #define 	P_RFID_EN(A)		HAL_GPIO_WritePin(RFID_EN_GPIO_Port, RFID_EN_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)			// 41 (RFID_EN)
#define	P_SW_O_C_T		HAL_GPIO_ReadPin(SW_O_C_GPIO_Port, SW_O_C_Pin)												// 42 (SW_O/C)	

//#define	P_SNS_LEFT_RIGHT_T	HAL_GPIO_ReadPin(SNS_LEFT_RIGHT_GPIO_Port, SNS_LEFT_RIGHT_Pin)								// 43 (SNS_LEFT/RIGHT)

//#define 	P_VOICE_F_HOLD(A)		HAL_GPIO_WritePin(VOICE_F_HOLD_GPIO_Port, VOICE_F_HOLD_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 44 (VOICE_F_HOLD)
//#define 	P_VOICE_RST(A)			HAL_GPIO_WritePin(VOICE_RST_GPIO_Port, VOICE_RST_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 45 (VOICE_RST)
//#define	P_VOICE_BUSY_T			HAL_GPIO_ReadPin(VOICE_BUSY_GPIO_Port, VOICE_BUSY_Pin)										// 46 (VOICE_BUSY)
//#define 	P_VOICE_CLK(A)			HAL_GPIO_WritePin(VOICE_CLK_GPIO_Port, VOICE_CLK_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)			// 47 (VOICE_CLK)
//#define 	P_VOICE_DATA(A)			HAL_GPIO_WritePin(VOICE_DATA_GPIO_Port, VOICE_DATA_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 48 (VOICE_DATA)
																															// 49 VSS_1
																															// 50 VDD_1
#define 	P_M_CLOSE_1(A)		HAL_GPIO_WritePin(M_CLOSE_1_GPIO_Port, M_CLOSE_1_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 51 (M_CLOSE_1)
#define 	P_M_CLOSE_2(A)		HAL_GPIO_WritePin(M_CLOSE_2_GPIO_Port, M_CLOSE_2_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 52 (M_CLOSE_2)
#define 	P_M_OPEN_1(A)		HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 53 (M_OPEN_1)
#define 	P_M_OPEN_2(A)		HAL_GPIO_WritePin(M_OPEN_2_GPIO_Port, M_OPEN_2_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 54 (M_OPEN_2)

#ifdef		DDL_CFG_LED_TYPE3
#define 	P_LED_IB(A)			HAL_GPIO_WritePin(LED_IB_GPIO_Port, LED_IB_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)			// 55 (LED_IB)
#endif

#if defined	(DDL_CFG_FP_TCS4K) || defined	(DDL_CFG_FP_PFM_3000)  //hyojoon_20160831 PFM-3000 add
#define	P_FP_DDL_TX_T			HAL_GPIO_ReadPin(FP_DDL_TX_GPIO_Port, FP_DDL_TX_Pin)										// 55 (FP_DDL_TX)
#define	P_FP_DDL_TX(A)			HAL_GPIO_WritePin(FP_DDL_TX_GPIO_Port, FP_DDL_TX_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 55 (FP_DDL_TX)
#define	P_FP_DDL_RX_T			HAL_GPIO_ReadPin(FP_DDL_RX_GPIO_Port, FP_DDL_RX_Pin)										// 56 (FP_DDL_RX)
#define 	P_FP_EN(A)			HAL_GPIO_WritePin(FP_EN_GPIO_Port, FP_EN_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)				// 57 (FP_EN)
#endif

//#ifdef		DDL_CFG_COVER_SWITCH
//#define 	P_SW_COVER_T		HAL_GPIO_ReadPin(FP_DDL_RX_GPIO_Port, FP_DDL_RX_Pin)											// 40 (SW_COVER)
//#endif

//#define		P_SNS_LOCK_T			HAL_GPIO_ReadPin(SNS_LOCK_GPIO_Port, SNS_LOCK_Pin)											// 58 (SNS_LOCK)	

#ifdef		DDL_CFG_INSIDE_LED_LOW
#define 	P_LED_LOW(A)			HAL_GPIO_WritePin(LED_LOW_GPIO_Port, LED_LOW_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 59 for (LED_LOW)
#endif

																													
#ifdef		DDL_CFG_LED_TYPE5
#define 	P_LED_CLOSE(A)		HAL_GPIO_WritePin(LED_CLOSE_GPIO_Port, LED_CLOSE_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 59
#define 	P_LED_OPEN(A)		HAL_GPIO_WritePin(LED_OPEN_GPIO_Port, LED_OPEN_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 60
#define 	P_LED_DEAD(A)		HAL_GPIO_WritePin(LED_DEAD_GPIO_Port, LED_DEAD_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 61

#endif	





//#define 	P__LED_FORCED(A)	HAL_GPIO_WritePin(_LED_FORCED_GPIO_Port, _LED_FORCED_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 60 fot #LED_FORCED
//#define 	P__LED_SPARE(A)		HAL_GPIO_WritePin(_LED_SPARE_GPIO_Port, _LED_SPARE_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 61 fot #LED_SPARE

#ifdef		DDL_CFG_BUZ_VOL_TYPE1
#define 	P_BU_VO_MID(A)		HAL_GPIO_WritePin(BU_VO_MID_GPIO_Port, BU_VO_MID_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)		// 62 (BU_VO-MID)
#endif

#define		P_SNS_OPEN_T		HAL_GPIO_ReadPin(SNS_OPEN_GPIO_Port, SNS_OPEN_Pin)											// 63 (SNS_OPEN)	
#define		P_SNS_CLOSE_T		HAL_GPIO_ReadPin(SNS_CLOSE_GPIO_Port, SNS_CLOSE_Pin)										// 64 (SNS_CLOSE)	

//#define	P_SNS_CENTER_T		HAL_GPIO_ReadPin(SNS_CENTER_GPIO_Port, SNS_CENTER_Pin)										// 65 #SNS_CENTER

#define 	P_BU_FREQ(A)		HAL_GPIO_WritePin(BU_FREQ_GPIO_Port, BU_FREQ_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)			// 66 (BU_FREQ)
#define		P_COM_HCP_RST_T		HAL_GPIO_ReadPin(COM_HCP_RST_GPIO_Port, COM_HCP_RST_Pin)									// 67 (COM_HCP-RST)	
#define		P_COM_DDL_TX_T		HAL_GPIO_ReadPin(COM_DDL_TX_GPIO_Port, COM_DDL_TX_Pin)										// 68 (COM_DDL_TX)	
#define		P_COM_DDL_RX_T		HAL_GPIO_ReadPin(COM_DDL_RX_GPIO_Port, COM_DDL_RX_Pin)										// 69 (COM_DDL_RX)	
#define 	P_BU_VO_HIGH(A)		HAL_GPIO_WritePin(BU_VO_HIGH_GPIO_Port, BU_VO_HIGH_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 70 (BU_VO-HIGH)
#define 	P_COM_HCP_EN(A)		HAL_GPIO_WritePin(COM_HCP_EN_GPIO_Port, COM_HCP_EN_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 71 (COM_HCP_EN)
																															// 72 for MCU_SW-DAT
#define		P_COM_BUSY_T		HAL_GPIO_ReadPin(COM_BUSY_GPIO_Port, COM_BUSY_Pin)											// 73 (COM_BUSY)	
																															// 74 VSS_2
																															// 75 VDD_2
																															// 76 for MCU_SW-CLK
#define 	P_SNS_EN(A)			HAL_GPIO_WritePin(SNS_EN_GPIO_Port, SNS_EN_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)			// 77 (SNS_EN)
#define		P_SW_REG_T			HAL_GPIO_ReadPin(SW_REG_GPIO_Port, SW_REG_Pin)												// 78 (SW_REG)	

#ifdef		DDL_CFG_HIGHTEMP_SENSOR
#define 	P_FIRE_EN(A)		HAL_GPIO_WritePin(FIRE_EN_GPIO_Port, FIRE_EN_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)			// 79 (FIRE_EN)
#endif 
#define		P_SNS_EDGE_T		HAL_GPIO_ReadPin(SNS_EDGE_GPIO_Port, SNS_EDGE_Pin)											// 80 (SNS_EDGE)
																															// 81 #SPI2_NSS
																															// 82 #SPI2_SCK
#define		P_SNS_F_BROKEN_T	HAL_GPIO_ReadPin(SNS_F_BROKEN_GPIO_Port, SNS_F_BROKEN_Pin)									// 83 (SNS_F-BROKEN)	
																															// 84 #SPI2_MISO
#define 	P_BT_EN(A)		HAL_GPIO_WritePin(BT_EN_GPIO_Port, BT_EN_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 71 (COM_HCP_EN)// 91 NC (TP12)

																															// 86 #LDO_EN
																															// 87 #DCDC_EN
																															// 88 #INT_7
																															// 89 MCU_SW-OB
																															// 90 NC	(TP11)
																															// 91 NC (TP12)
#define		P_I2C_SCL_T			HAL_GPIO_ReadPin(I2C_SCL_GPIO_Port, I2C_SCL_Pin)											// 92 (I2C_SCL)	
#define		P_I2C_SDA_T			HAL_GPIO_ReadPin(I2C_SDA_GPIO_Port, I2C_SDA_Pin)											// 93 (I2C_SDA)	
																															// 94 (BOOT0)	
#define		P_SW_FACTORY_T		HAL_GPIO_ReadPin(SW_FACTORY_GPIO_Port, SW_FACTORY_Pin)										// 95 (SW_FACTORY)	

#ifdef		DDL_CFG_TOUCH_OC
#define		P_TOUCH_O_C_T		HAL_GPIO_ReadPin(TOUCH_O_C_GPIO_Port, TOUCH_O_C_Pin)										// 96 (TOUCH_O/C)	
#endif

#ifdef	DDL_CFG_DIMMER
#define 	P_DIM_SDA(A)		HAL_GPIO_WritePin(DIM_SDA_GPIO_Port, DIM_SDA_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)			// 97 (DIM_SDA)
#define 	P_DIM_SCL(A)		HAL_GPIO_WritePin(DIM_SCL_GPIO_Port, DIM_SCL_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)			// 98 (DIM_SCL)
#endif																														

#ifdef	DDL_CFG_LED_TYPE1
#define 	P_LED_TENKEY(A)		HAL_GPIO_WritePin(LED_TENKEY_GPIO_Port, LED_TENKEY_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 97 (LED_TENKEY/DIM_SDA)
#define 	P_LED_DECO(A)		HAL_GPIO_WritePin(LED_DECO_GPIO_Port, LED_DECO_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 98 (LED_LOWBAT/DIM_SCL)
#endif								

#ifdef		DDL_CFG_LED_TYPE4
#define 	P_LED_TENKEY(A)		HAL_GPIO_WritePin(LED_TENKEY_GPIO_Port, LED_TENKEY_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 97 (LED_TENKEY/DIM_SDA)
#endif								

#ifdef		DDL_CFG_LED_TYPE2
#define 	P_LED_TENKEY(A)		HAL_GPIO_WritePin(LED_TENKEY_GPIO_Port, LED_TENKEY_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 97 (LED_TENKEY/DIM_SDA)
#define 	P_LED_LOWBAT(A)		HAL_GPIO_WritePin(LED_LOWBAT_GPIO_Port, LED_LOWBAT_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 98 (LED_LOWBAT/DIM_SCL)
#endif																														

#ifdef		DDL_CFG_LED_TYPE3
#define 	P_LED_TENKEY(A)		HAL_GPIO_WritePin(LED_TENKEY_GPIO_Port, LED_TENKEY_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 97 (LED_TENKEY/DIM_SDA)
#define 	P_LED_LOWBAT(A)		HAL_GPIO_WritePin(LED_LOWBAT_GPIO_Port, LED_LOWBAT_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 98 (LED_LOWBAT/DIM_SCL)
#endif																															

																															
#ifdef		DDL_CFG_LED_TYPE5
#define 	P_LED_TENKEY(A)		HAL_GPIO_WritePin(LED_TENKEY_GPIO_Port, LED_TENKEY_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 97 (LED_TENKEY/DIM_SDA)
#define 	P_LED_LOWBAT(A)		HAL_GPIO_WritePin(LED_LOWBAT_GPIO_Port, LED_LOWBAT_Pin, (A)?GPIO_PIN_SET:GPIO_PIN_RESET)	// 98 (LED_LOWBAT/DIM_SCL)
#endif																															

																															// 99 VSS_3				
																															// 100 VDD_3


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif



