//------------------------------------------------------------------------------
/** 	@file		ddl_config-basic-model.h
	@version 0.1.04
	@date	2018.02.06
	@brief	DDL의 전체 구성에 대한 Macro를 정의한다.
	@see
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00	2017.11.27		by hyojoon
			- 2017.11.27 일자 define 정리 통합라이브러리 에서 사용 하는 define 에 대한 정의 정리 

		V0.1.01 2017.12.06		by Jay
			- Remocon 신호 전송 기능 정의 추가 (Gate Lock에서 사용)
			- 기본 동작 모드 정의 (Gate Lock에서 사용)
			- 일본 Felica 카드 적용 여부를 관리하는 정의 추가
			
		V0.1.02 2018.02.01		by Jay
			- _MASTERKEY_IBUTTON_SUPPORT 설정 조건에서 DDL_CFG_DS1972_IBUTTON 사전 정의 확인 부분 삭제
			  DDL_CFG_DS1972_IBUTTON 설정이 될 경우 오히려 에러 발생. 
			  _MASTERKEY_IBUTTON_SUPPORT 단독으로 동작 가능

		V0.1.03 2018.02.06		by Jay
			- Touch IC Sensitivity 설정 내용 추가

		V0.1.04 2018.04.13		by hyojoon
			- IAR 설정 추가 
			- _USE_IREVO_CRYPTO_ EEPROM 설정에 상관없이 비밀 번호 , Card UID를 암호 화 설정 내용 추가
			- _USE_STM32_CRYPTO_LIB_ STM32 lib crypto 사용시 설정 내용 추가
*/
//------------------------------------------------------------------------------


/*

기본 설정 시작 
==================================================

CubeMX 에서 생성 해주는 기본 프로젝트 설정에서 하기와 같이 변경 사용 

IAR project option
1. C/C++ Compiler : Optimization : Level -> Low
	Optimization 은 현재 Low 로 설정 사용 
	
2. C/C++ Compiler : Preprocessor: 기본으로 생성 되는 PATH 외에 아래 내용 복사 사용 필요 없는 부분 삭제 
$PROJ_DIR$/../DDL_Source
$PROJ_DIR$/../DDL_Driver/Source
$PROJ_DIR$/../DDL_Driver/Source/config_pin
$PROJ_DIR$/../DDL_Driver/Source/ModeFunctions
$PROJ_DIR$/../DDL_Driver/Source/PackFunctions
$PROJ_DIR$/../DDL_Driver/Source/MOTOR
$PROJ_DIR$/../DDL_Driver/Source/DS1972_IBUTTON
$PROJ_DIR$/../DDL_Driver/Source/IBUTTON
$PROJ_DIR$/../DDL_Driver/Source/IREVO_HFPM
$PROJ_DIR$/../DDL_Driver/Source/PFM_3000
$PROJ_DIR$/../DDL_Driver/Source/TCS4K
$PROJ_DIR$/../DDL_Driver/Source/TRF7970A
$PROJ_DIR$/../DDL_Driver/Source/Crypto
$PROJ_DIR$/../DDL_Driver/Source/Crypto/STM32_Cryptographic/Inc

Crypto 선언 해서 사용 할 경우 
STM32_Cryptographic 내에서 lib 파일을 선택해서 추가 해야 함 

3. OutPut Converter : Generate additional output
					output format : binary
					
4. Debugger : Step : I-jet/JTAGjet 선택 (자신이 가지고 있는 개발 장비 셋팅)

5. I-jet/JTAGjet : JTAG/SWD : SWD 선택 

==================================================
기본 설정  끝 


stack / heap 설정 
Linker : Config : *.icf 을 Edit... 해서 vector Table , Memory Regions , Stack / Heap Size 설정 가능 

stack usage 설정 
Linker : Advanced : Enable stack usage analysis
build 후에 map 파일을 열어 보면 stack usage 를 확인 할 수 있음 

*/

#ifndef		_DDL_CONFIG_Z10_IH_H_
#define		_DDL_CONFIG_Z10_IH_H_


//------------------------------------------------------------------------------
// Ten Key 입력형태에 대한 정의
// touch 10key 를 사용 하는 제품의 경우 해당 macro 를 define 한다.
//통합라이브러리에서는 TSM12 touch 센서를 사용 그외 IC 는 없다.
// 정전방식 터치키
#define	DDL_CFG_TOUCHKEY

#ifdef	DDL_CFG_TOUCHKEY
	//TSM12 를 쓰는 경우 define 한다. 현재 TSM12 밖에 안쓰므로 
	//DDL_CFG_TOUCHKEY 가 정의 되면 DDL_CFG_TOUCHKEY_TSM12 
	//를 같이 정의 한다. 
	#define	DDL_CFG_TOUCHKEY_TSM12

	// TSM12 를 사용 하면서 버튼 배열이 TYPE 1 인경우 
	//Z10-IH, WF20-DMP, A20-IH, E300-FH, WV-200
	//FPCB type
	#define	DDL_CFG_KEY_ARRANGE_TYPE1 

	// TSM12 를 사용 하면서 버튼 배열이 TYPE 2 인경우 
	//J20-IH, WV40-DMP, WE40-DMP, WE30-DMP
	// EMI GASKET type
	//#define	DDL_CFG_KEY_ARRANGE_TYPE2

	//	멀티 터치에 의한 일회용 무음 모드 지원
	#define	DDL_CFG_ONETIME_MUTE

	// Touch IC Sensitivity 값 설정
	// #define	TOUCH_IC_SENSITIVITY_VALUE	0xAA

	// Touch IC Response Time Control 
	// Touch IC 에서 사용 하는 체터링 타임 
	// default 값 0x03 이면 3+2 구조로 5번 체터링 함 
	// 이를 1 + 2 하여 3번 체터링 하는 것으로 변경 하려면 아래 define enable 
	// 효과 : 반응 속도 빨라 짐 
	// #define TOUCH_IC_RESPONSE_TIME_CONTROL_VALUE		0x31
	
#else 
	// push switch
	// ADC 를 이용하는 Push Switch Ten key 
	#define	DDL_CFG_PUSHKEY	

	//버튼 배열에 따른 정의로 현재 한가지 type 만 존재한다. 
	//DDL_CFG_PUSHKEY 가 정의 되면 DDL_CFG_PUSHKEY_TYPE1 같이 
	//정의 한다.
	#define	DDL_CFG_PUSHKEY_TYPE1
#endif
// Ten Key 입력형태에 대한 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
// LED 에 대한 정의

// Front LED Dimmer 
// 통합라이브러리 에서는 PCA9532 IC 만 사용 한다.
#define		DDL_CFG_DIMMER

#ifdef		DDL_CFG_DIMMER
	// 16bit I2C LED dimmer PCA9532
	// DDL_CFG_DIMMER 가 정의 되면 DDL_CFG_DIMMER_PCA9532 같이 정의 한다. 
	#define	DDL_CFG_DIMMER_PCA9532

	//DIMMER 사용중 Main에 장착 되어 있는 GPIO제어 OPEN/CLOSE LED 제어 정의 
	//#define	DDL_MAIN_OC_LED
#else 
	// 개별 라이팅이 아닌 방식
	// 아래 type 별로 제품에 맞는 type 만 정의 해서 쓰고 나머지는 undefine 한다.
	#define	DDL_CFG_NO_DIMMER

	// Front LED_TENKEY , LED_DECO 
	#define	DDL_CFG_LED_TYPE1

	// Front LED_TENKEY , LED_LOWBAT (front) , LED_DEAD
	#define	DDL_CFG_LED_TYPE2

	// LED_TENKEY , LED_LOWBAT (front) , LED_DEAD , LED_IB
	#define	DDL_CFG_LED_TYPE3

	// Front LED_TENKEY , LED_DECO (TYPE1 과 같지만 커버에 의해 DECO 가 안보이는 경우가 있어 4로 차이를 둠)
	#define	DDL_CFG_LED_TYPE4

	//Front LED_TENKEY , LED_LOWBAT (front) , LED_DEAD , LED_OPEN , LED_CLOSE
	#define	DDL_CFG_LED_TYPE5

	//Front LED_TENKEY , LED_OPEN , LED_CLOSE
	#define	DDL_CFG_LED_TYPE6	
#endif

//	Inside Low Battery LED 정의
	// low battery led 가 main 에 이는 경우 define 한다. 
//	#define	DDL_CFG_INSIDE_LED_LOW
	
// LED 에 대한 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//Ten Key 이외의 다른 인증 수단에 대한 정의 
// Ten key + 지문 구현 완료 
// Ten Key + Card 구현 완료 
// Ten Key + I-Button 구현 완료 
// Ten Key + Card + 지문 구현 완료 
// 위 조함 외에 Ten Key + 2way 이상 정의시 GetSupportCredentialCount() 함수 분석 할것 
// GetSupportCredentialCount() 함수는 memu mode 중 등록 삭제 에대한 하위 menu 를 자동으로 
// 구성 하기 위한 함수 이 부분 말고도 손대야 할 부분이 많음 (BLE 라든지...)
//------------------------------------------------------------------------------
// RFID
// Card 지원 제품 사용시 정의 한다 
//#define	DDL_CFG_RFID

#ifdef	DDL_CFG_RFID
	// RFID IC TRF7970A 만 사용 하므로 같이 정의
	#define	DDL_CFG_RFID_TRF7970A

	//통신 라인에 대한 정의 DDL_CFG_RFID_PARALLEL 가 정의 되면 PARALLEL 통신을 한다.
	//SPI 통신을 하려면 해당 macro 를 undefine 한다.
	#define	DDL_CFG_RFID_PARALLEL

	// OPEN UID 설정 여부를 관리 
	// 최초 빌드시 혹은 N1 , N2 팩에 의해서만 초기화 시에 
	// #define IREVO_CARD_ONLY    아이레보 카드 4byte 만 가능 
	// #undef IREVO_CARD_ONLY    4byte + 7 byte 카드 모두 가능 
	// PC 생산 프로그램 , Test mode 에서 enable , disable 설정 가능 
	// 현재는  #define OPEN_UID_EN  (ROM_TEST+13) 통해서 enable , disable 가능 하다. 
	#define	IREVO_CARD_ONLY		

	// 일본 Felica 카드 적용 여부를 관리
	// Felica 카드 적용할 때에는 전류 문제가 발생할 수 있기 때문에 Wake Up 후 카드 사용하도록 UI 적용 필요
	#define	FELICA_CARD_SUPPORTED


//#ifndef IREVO_CARD_ONLY
	// OPEN UID card read time 을 설정 한다. 
	// OPEN UID 를 사용 하기 위해 IREVO_CARD_ONLY가 undefie 되면 자동으로 OPEN_UID_DELAY
	// 를 define 하며 기본 값은 2MS 로 한다 (HW 적 파형은 3MS 로 동작한다, OPEN UID 가 아니면 HW 적으로 1MS로 동작)
	// 제품의 특성에 따라 delay 값을 유동적으로 가지고 간다. 
	// 값이 클수록 소모 전류가 올라 간다. 
	#define	OPEN_UID_DELAY		SYS_TIMER_2MS
//#endif 	

#endif
// RFID 에 대한 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
// 지문
//지문 지원 제품 사용시 정의 한다
#define	DDL_CFG_FP

#ifdef	DDL_CFG_FP
	//지문 등록 총 수 보통 20 , Biosec TS1071M 의 경우 40개 까지 사용 가능 
	//_MAX_REG_BIO 값을 20이 아닌 40으로 변경 할 경우 
	// BLE 관련 함수인 CredentialOccupiedCheck() 를 추가 구현 해야 한다. (Biosec TS1071M 만 구현 됨)
	#define	_MAX_REG_BIO		20

	//TCS4K , TCS4C 사용시 
	#define	DDL_CFG_FP_TCS4K

	// Area type 필리아 지문 모듈 (구현 되어 있지 않음)  
	//#define	DDL_CFG_FP_PFM_3000

	// Area type Biosec 지문 모듈 	
	//#define	DDL_CFG_FP_TS1071M	

	// Area type iRevo PPI 지문 모듈 HFPM (Handle Finger Print Module)
	//#define	DDL_CFG_FP_HFPM
#ifdef	DDL_CFG_FP_TCS4K
	//지문 cover 가 motor 로 제어 되는 경우 
	//#define	DDL_CFG_FP_MOTOR_COVER_TYPE
#endif 

#endif
// 지문 에 대한 정의 끝
//------------------------------------------------------------------------------





//------------------------------------------------------------------------------
// I-BUTTON
// I-Button 사용시 정의 
// DDL_CFG_IBUTTON : GM touch key 사용시(사각형 key) , DDL_CFG_DS1972_IBUTTON DS1972 사용시 (원형키)
// GM touch key 사용시 
//#define	DDL_CFG_IBUTTON //GM touch key

//DS1972 I-Buuton 사용시 
//#define	DDL_CFG_DS1972_IBUTTON // DS1972 

// 지문 주키 제품을 위한 마스터키 iButton 기능 정의
// DS1972 이면서 마스터키 ibutton 일때 정의 (ex WF-200 , 지문 모델 이면서 master i-button 지원)
//#define	_MASTERKEY_IBUTTON_SUPPORT 
// I-BUTTON 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	Stop Mode에서 깨어나는 RTC wakeup time 지정 하기 값(3472)을 일반적으로 사용 함 
/*
	STM32L151의 경우 RTCCLK는 37kHz
	Wakeup clock으로 RTCCLK/16을 사용하는 경우,
	tick당 시간은 1 / (37000/16) = 1/2312.5 = 0.000432 sec

	Example) 1.7sec wakeup time := 3931
*/
#define	DDL_CFG_RTC_WAKUPTIME	3472
//#define	DDL_CFG_RTC_WAKUPTIME	10000

// RTC wakeup time 지정 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	모터 구동 정의
// 현재 사용 하고 있지 않는 define 임 
//#define	DDL_CFG_MOTOR_TYPE1
// 하기 define 중 제품에 맞는 motor 선택 후 정의 나머지는 undef 

//GR14_S Motor 사용시 정의 (YDG413)
//#define	DDL_CFG_RIM_GR14_S_MOTOR

//RIM Motor 사용시 정의 (보조키 계열 Z10-IH 등등)
#define	DDL_CFG_RIM_MOTOR

//L-MECHA 계열 모티스 사용시 정의
//#define	DDL_CFG_L_MECHA

//M60G 계열 모티스 사용시 정의 (YMG30 , 40 등 중국 계열 제품)
//#define	DDL_CFG_M60G_MECHA

//Clutch type 사용시 정의 (YMH70)
//#define	DDL_CFG_CLUTCH

//DeadBolt 사용시 정의 (YDD424)
//#define	DDL_CFG_DEADBOLT


#ifdef DDL_CFG_CLUTCH
	// 내부 강제 잠금 상태에서 open 할 수 있도록 해주는 define 
	#define	DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
#endif 

//문이 열려 있는 경우 멀티 터치에 의해 모터 닫힘 동작 못하도록 하는 정의 (중국 제품의 경우 define 해서 사용)
//#define	MOTOR_CLOSED_IN_OPEN_STATUS

//Open close key 가 아닌 close 전용 key 에 대한 정의
//#define	CLOSE_ONLY_BUTTON


// PWM 제어 
// PWM 제어는 Motor-xxxxx.h 파일에서 정의 한다. 
// 기본 적으로 M_CLOSE_PWMCH1_Pin 이 CubeMX에서 정의가 되고 timer9 이 PWM 설정이 되야 PWM 으로 동작한다.  
// L-mecha , Dead bolt 의 경우 Test 가 완료 되었으나 , 나머지의 경우 튜닝이 필요 하다. 
// 아래는 각 Motor-xxxxx.h 에 정의 하는 define 에대한 설명을 한다. define 은 각 Motor-xxxxx.h 에서 한다.

// timer9 이 일반 timer 로 선언 되고 , PWM_BACKTURN_SKIP_COUNT 이 정의가 되면 
// PWM 사용 없이 timer9 은 motor 동작 시간 체크 용으로 쓰인다. 

	//PWM 시작 Duty 설정 50 으로 50%로 설정 
	//#define	PWM_START_DUTY	50

	//16Khz 로 동작 하는 PWM 적용시 100count 당 약 6ms 를 인지 할 수 있다
	//PWM 으로 제어 하는 경우 50% 에서 PWM_DUTY_HOLD_COUNTER 150 값 이후 다음 step 으로 % 를 올린다. 
	//현재 구현은 50% 9ms -> 60% 9ms -> 70% 9ms -> 80% 9ms -> 90% 9ms -> 100% 
	//9ms 는 100 count 당 6ms 이므로 150 의 경우 약 9ms 가 된다.  
	//현재 L-mecha 와 Deadbolt 값만 수렴 되어 있으므로 다른 모터 메카에서 PWM 사용시
	//값들을 측정후 적절히 사용 해야 한다.
	//#define	PWM_DUTY_HOLD_COUNTER	150

	//PWM 제어시나 timer9 을 PWM 아닌 시간 측정용으로 사용시 define 한다.
	//이 define 은 모터 닫힘 동작 시도중 바로 오프 시도를 하는 경우 모터 부하 상태로 남는 문제를 해결 하기 위한 값으로 모터가 닫힌 중일때 오픈 시도를 하는 경우 얼마나 빨리 open sensor 에 값이 인지가 되느냐를 측정 하고 그값이 PWM_BACKTURN_SKIP_COUNT 값보다 적을 경우 back turn 을 skip 하기 위한 define 이다. L-Mecha 만 측정 되었고 값은 5000 이다 
	//Timer9 을 선언하고  PWM_BACKTURN_SKIP_COUNT , PWM_HOLD_OPEN_TIME 이 둘다 선언될경우 위와 같은 논리로 PWM 없이 동작 한다.
	//motor_control_open_type1() 함수를 분석 하라.
	//#define	PWM_BACKTURN_SKIP_COUNT	5000

	//모터 닫힘 동작중 오픈 시도시 PWM_BACKTURN_SKIP_COUNT 값보다 적을 경우 센터를 어느 정도라도 맞추기 위해 close 방향으로 얼마만큼 돌리느냐를 설정 
	//L-mecha 만 측정 되어 있고 크게 중요 한 값은 아니다 현재 100 (200ms) 로 설정 
	//#define	PWM_HOLD_OPEN_TIME		100
//	모터 구동 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
// Language 및 비밀번호 자리수 정의 
// PIN 최소 최대 길이를 정의 한다. 이곳에서 정의 되어 있지 않으면,
// PincodeFunctions.h에 정의된 default가 사용된다.
// 음성 IC 사용 하는 경우 : 중국어 선택시 자동으로 MIN 값이 6으로 변경 된다. 
// 음성 IC 사용 하는 하지 않는 경우 : 아래 define 값으로 정의 된다. 
#define	MIN_PIN_LENGTH			4 // 6
#define	MAX_PIN_LENGTH			10

//voice IC 를 지원 하는 경우 
// 아래중 하나 선택 
//4개 언어 지원 한국어 , 중국어 , 영어 , 대만어 지원 하면서 중국어 기본 설정 
//#define	DDL_CFG_LANGUAGE_SET_TYPE1

//6개 언어 지원 (아직 없음) 
//#define	DDL_CFG_LANGUAGE_SET_TYPE2

//4개 언어 지원 한국어 , 중국어 , 영어 , 대만어 지원 하면서 한국어 기본 설정
#define	DDL_CFG_LANGUAGE_SET_TYPE3

// Language 및 비밀번호 자리수 정의  끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	Touch O/C 정의 
// 이중 Open Close 버튼에 대한 정의 
#define	DDL_CFG_TOUCH_OC
//	Touch O/C 정의  끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	MUTE 키 정의
// Mute key 지원 제품시 설정 (Touch Ten key 이회용 무음과는 관계없음)
//#define	DDL_CFG_MUTE_KEY
//	MUTE 키 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	Ten Key slide cover 에 대한 정의  
//	DDL_CFG_COVER_SWITCH 를 정의시 해당 PIN 의 번호를 명시 해 준다. 
//	HAL_GPIO_EXTI_Callback() 의 DDL_CFG_COVER_SWITCH 부분 확인 할 것 
//	PIN 이 9번이면 9 . 0번이면 0 둘중에 하나 define 할 것 
//#define	DDL_CFG_COVER_SWITCH		9
//#define	DDL_CFG_COVER_SWITCH		0
//	Ten Key slide cover 에 대한 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	High Temperature Sensor 정의
//	고온 감지 지원 제품시 설정
#define	DDL_CFG_HIGHTEMP_SENSOR

//	56 도 제한시 설정 46도의 경우 undefine 
//#define	TEMPERATURE_HIGH_56_DEGREE 
//	High Temperature Sensor 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	와치독 정의
// 기본적으로 ddl_config_model.h 파일에서는 define 한 상태에서 사용 하고 
// 개발 용이나 , 디버깅 용으로 IAR 디버거를 사용 할 경우 
// ddl_config.h 파일에서 undef 해서 사용 
#define	DDL_CFG_WATCHDOG
//	와치독 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	Buzzer Volume Type 정의
//	Software volume control
#define	DDL_CFG_BUZ_VOL_TYPE1	

//	HW volume control
//#define	DDL_CFG_BUZ_VOL_TYPE2	

// 	특수 한 경우로 Buzzer 가 없는 제품의 경우 define 함 (YMH70)
//#define	DDL_CFG_NO_BUZZER
//	Buzzer Volume Type 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
// main menu 관련 정의 

//	Lock Setting Menu 정의
//	메뉴 모드의 4번 하위 메뉴에 대한 정의 (advanced mode 6번)
//	FeedbackFunctions_T1.h , ModeMenuLockSetting_T1.h 관련 부분 확인 

//모드 변경 , 자동 잠금  설정 , 음량 설정 , 언어 설정
#define	DDL_CFG_LOCK_SET_TYPE1	

//모드 변경 , 자동 잠금  설정 , 음량 설정 
//#define 	DDL_CFG_LOCK_SET_TYPE2

//모드 변경 
//#define 	DDL_CFG_LOCK_SET_TYPE3

//모드 변경 , 언어 설정
//#define 	DDL_CFG_LOCK_SET_TYPE4

//모드 변경 , 언어 설정 , 좌우수 설정 
//#define 	DDL_CFG_LOCK_SET_TYPE5

//모드 변경 , 자동 잠금  설정 , 음량 설정 , 언어 설정 , 좌우수 설정
//#define 	DDL_CFG_LOCK_SET_TYPE6
//	Lock Setting Menu 정의  끝


//	듀얼 팩 8 번 메뉴 관련 정의 
//	듀얼 팩 지원 제품의 경우 팩 종류에 상관없이 정의 
//	정의 하게 되면 메뉴의 8번 하위 에 8번 리모콘 등록 메뉴가 활성화
//#define 	DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU
//	듀얼 팩 8 번 메뉴 관련 정의  끝


//	Zwave soft reset menu 정의 
//	일반 적으로 0번을 soft reset 기능으로 사용 하나 , R200-CH 같은 모델의 경우 9번을 사용 
//	9번을 soft reset 으로 사용시 define 
//#define 	_ZWAVE_SOFT_RESET_MENU_9_
//	Zwave soft reset menu 정의 끝

// main menu 관련 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	Abov voice index type define
//
//	AVML_MSG_T1 : 2016년 상반기에 정의된 list
//	AVML_MSG_T2 : 2016년 하반기에 정의된 list

//#define	AVML_MSG_T1
#define	AVML_MSG_T2
//	Abov voice index type define  끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	BLE-N 정의
//	스마트 리빙 BLE 팩 사용시 정의 
//	기본적으로 define 하여 사용
#define	BLE_N_SUPPORT
//	BLE-N 정의 끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	BLE-N 정의
// Z-wave 300 사용시 정의 (국내의 경우 define 하지 않는다)
//#define	ZWAVE300_PACK_SUPPORTED
// Z-wave 300 사용시 정의  끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	사용자 비밀번호 스케줄 기능 및 Lockout 기능 지원 정의
//	스케줄 기능 및 lockout 지원 정의
//	Z-wave 를 지원 하는 제품은 define 한다. 
//	기본적으로 define 하여 사용 
#define	_PINCODE_SCHEDULE_SUPPORT
//	사용자 비밀번호 스케줄 기능 및 Lockout 기능 지원 정의  끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
// Interrupt 및 input PIN 에 대한 active state 관련 정의 

#ifdef	DDL_CFG_FP
// 지문 커버나 , 에어리어 타잎 베젤 의 active state 를 정의 low , High 에 따라 High 인 경우 define 함.
#define	DDL_CFG_FP_COVER_ACTIVE_HIGH
#endif 

#ifdef	DDL_CFG_COVER_SWITCH
//	slide cover 가 active High 인 경우 
#define	DDL_CFG_COVER_ACTIVE_HIGH
#endif

//edge sns 가 active high 인 경우 
//#define	DDL_CFG_SNS_EDGE_ACTIVE_HIGH

// 등록 버튼이 active low 인 경우 
//#define	DDL_CFG_SW_REG_ACTIVE_LOW

// 내부 강제 잠금이 active high 인 경우 
//#define	DDL_CFG_SNS_LOCK_ACTIVE_HIGH

// Interrupt 및 input PIN 에 대한 active state 관련 정의  끝
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
// Remocon 신호 전송 기능 정의
// - Gate Lock의 기본 장착 통신팩에 적용된 기능
// - Gate Lock이 열리면 다른 연동된 도어록에 열림 신호를 전송하도록 하는 기능 추가

//#define	DDL_CFG_ADD_REMOCON_LINK_FUNCTION

//	Remocon 신호 전송 기능 정의 끝
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// 기본 동작 모드 정의
// 동작 모드 중 1개만 define 설정할 것

#define	DDL_CFG_NORMAL_DEFAULT		// Normal Mode, Advanced Mode 모두 지원 ; Normal Mode가 Default
//#define	DDL_CFG_ADVANCED_DEFAULT	// Normal Mode, Advanced Mode 모두 지원 ; Advanced Mode가 Default
//#define	DDL_CFG_NORMAL_ONLY			// Normal Mode 만 지원
//#define	DDL_CFG_ADVANCED_ONLY		// Advanced Mode 만 지원

// 기본 동작 모드 정의 끝
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// 기타 정의 

// PC 생산 프로그램 , N1 , N2 팩으로 초기화를 못하여 factory reset 만으로 초기화 하는경우 
// FactoryDefaultSetting(); 함수를 새로 제품에 맞게 구현 할것 
// 현재는 YMH70 기준으로 정의 되어 있음 
// 주의 사항!!!!!!!!!! 언어는 변경 하지 않는다. 
//#define	DDL_CFG_FACTORY_DEFAULT_SETTING

// 항주 연동기 
// 119+비번 , 비번+119 시 연동기로 신호를 보냄 (PIN 움직는건 확인 했으나 .. 실제 동작은 모름)
//#define	DDL_CFG_EMERGENCY_119

// EEPROM 사용 macro 
// ST 내부 EEPROM 사용시 define 
// 외부 EEPROM 사용시 undef 
#define _USE_ST32_EEPROM_
  
// EEPROM 사용 macro 
// 내부 EEPROM 사용시 비밀 번호 , Card UID 암호화 define 
// define 시 비밀 번호 , Card UID 관련 암호 복호 enable 
#ifdef _USE_ST32_EEPROM_
#define _USE_IREVO_CRYPTO_
#endif 

// AES128 사용시 
// 현재 STM32L 은 crypto HW 블럭이 없으므로 STM32 SW lib 을 사용 해야 함 
// STM32_Cryptographic 폴더 안에 STM32CryptographicV3.0.0_CM3_IAR_otnsc.a 사용 중임
// _USE_STM32_CRYPTO_LIB_ 가 선언되면 
// main.h 에서 crypto.h 파일을 include 하게 되어 있음 
// 해당 파일을 include 하면 config.h 파일을 indclude 하는데 
// config.h 파일의 안에 define 여부에 따라 AES 의 종류를 설정 할수 있음 
// 현재는 AES128 ECB 사용으로 설정 
//#define _USE_STM32_CRYPTO_LIB_



// 통합 라이브러리 MS 사양 
// 통합 라이브러리의 MS 대응을 위한 define 
//#define	DDL_CFG_MS

#ifdef DDL_CFG_MS
/* MS 사양은 normal mode 만 지원 */
#undef	DDL_CFG_NORMAL_DEFAULT		// Normal Mode, Advanced Mode 모두 지원 ; Normal Mode가 Default
#undef	DDL_CFG_ADVANCED_DEFAULT	// Normal Mode, Advanced Mode 모두 지원 ; Advanced Mode가 Default
#define	DDL_CFG_NORMAL_ONLY			// Normal Mode 만 지원
#undef	DDL_CFG_ADVANCED_ONLY		// Advanced Mode 만 지원
/* 스케줄 미지원 */
#undef	_PINCODE_SCHEDULE_SUPPORT
/* BLE 미지원 */
#undef	BLE_N_SUPPORT
/* 내부 강제 잠금 상태에서 motor open */ 
#define	DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
#endif 





// 기타 정의 끝
//------------------------------------------------------------------------------
#endif

