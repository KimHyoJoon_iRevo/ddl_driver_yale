//------------------------------------------------------------------------------
/** 	@file		ddl_config-we40-dmp.h
	@version 0.1.00
	@date	2016.06.02
	@brief	DDL의 전체 구성에 대한 Macro를 정의한다.
	@see
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00	2016.06.02		by Jay
			- 모델별로 ddl_config.h를 사용하기 위해 기존 ddl_config.h의 내용이 옮겨옴.
*/
//------------------------------------------------------------------------------

#ifndef		_DDL_CONFIG_WE40_DMP_H_
#define		_DDL_CONFIG_WE40_DMP_H_


// WE40-DM+, WV40-DM+ 공용

//------------------------------------------------------------------------------
// Ten Key 입력형태에 대한 정의

// 정전방식 터치키
#define		DDL_CFG_TOUCHKEY

#ifdef	DDL_CFG_TOUCHKEY
	#define		DDL_CFG_TOUCHKEY_TSM12

	//Z10-IH, WF20-DMP, A20-IH, E300-FH, WV-200
//	#define		DDL_CFG_KEY_ARRANGE_TYPE1

	//J20-IH, WV40-DMP, WE40-DMP, WE30-DMP
	#define 	DDL_CFG_KEY_ARRANGE_TYPE2
#endif

// push switch
//#define		DDL_CFG_PUSHKEY

#ifdef		DDL_CFG_PUSHKEY
	#define		DDL_CFG_PUSHKEY_TYPE1
#endif


//------------------------------------------------------------------------------
// Front LED Dimmer 정의

//#define		DDL_CFG_DIMMER

#ifdef		DDL_CFG_DIMMER
	// 16bit I2C LED dimmer PCA9532
	#define		DDL_CFG_DIMMER_PCA9532
#endif

// 개별 라이팅이 아닌 방식
#define		DDL_CFG_NO_DIMMER

#ifdef		DDL_CFG_NO_DIMMER 
	// Front LED_DECO, LED_TENKEY
	//	Rose2_IH
//	#define 	DDL_CFG_LED_TYPE1

	// Front LED_TENKEY, LED_LOWBAT, LED_DEAD
	//	J20-IH, WV40-DMP, WE40-DMP, WV-200 
	#define 	DDL_CFG_LED_TYPE2

	// Front LED_TENKEY, LED_LOWBAT, LED_DEAD, LED_IB
	//	WE30-DMP
//	#define DDL_CFG_LED_TYPE3

	// Front LED_DECO, LED_TENKEY, 커버를 올리면 LED_DECO가 보이지 않아 차이를 둠.
	//	Edge2-DMP
//	#define DDL_CFG_LED_TYPE4

#endif


//------------------------------------------------------------------------------
// 2way 인증시 Ten Key 이외의 인증 수단 방법 정의

// RFID
#define		DDL_CFG_RFID

#ifdef	DDL_CFG_RFID
	#define		DDL_CFG_RFID_TRF7970A
	#define		DDL_CFG_RFID_PARALLEL

//------------------------------------------------------------------------------
/*! 	\def	IREVO_CARD_ONLY 	
	아이레보 카드와 같은 Type의 IC만 대응하고자 할 경우에 Define을 설정 (4byte, 7byte UID 모두 대응 가능)
	OPEN UID 설정을 위해서는 해당 Define을 해제 (4byte, 7byte UID 모두 대응 가능)
*/
//------------------------------------------------------------------------------
#ifdef 	__DDL_MODEL_WV40_DMP_TAIWAN
	#undef 		IREVO_CARD_ONLY
#else 
	#define		IREVO_CARD_ONLY		
#endif 	

#endif


// 지문
//#define		DDL_CFG_FP

#ifdef		DDL_CFG_FP
	#define		_MAX_REG_BIO		20

	#define		DDL_CFG_FP_TCS4K
#endif

// I-BUTTON
//#define		DDL_CFG_IBUTTON
//#define		DDL_CFG_DS1972_IBUTTON




//------------------------------------------------------------------------------
//	Stop Mode에서 깨어나는 RTC wakeup time 지정

/*
	STM32L151의 경우 RTCCLK는 37kHz
	Wakeup clock으로 RTCCLK/16을 사용하는 경우,
	tick당 시간은 1 / (37000/16) = 1/2312.5 = 0.000432 sec

	Example) 1.7sec wakeup time := 3931
*/
#define	DDL_CFG_RTC_WAKUPTIME	3472
//#define	DDL_CFG_RTC_WAKUPTIME	10000



//------------------------------------------------------------------------------
//	모터 구동 정의

//보조키 모터 (IH, DM)
#define	DDL_CFG_MOTOR_TYPE1


// PIN 최소 최대 길이를 정의 한다. 이곳에서 정의 되어 있지 않으면,
// PincodeFunctions.h에 정의된 default가 사용된다.
// DDL_CFG_SUPPORT_CHINESE 를 define 한다는 것은 중국어 등록은 6자리 제한 / 인증은 4자리 제한을 두기 위함 이다. 

#define	MIN_PIN_LENGTH			4
#define	MAX_PIN_LENGTH			10

#define	DDL_CFG_SUPPORT_CHINESE 

//------------------------------------------------------------------------------
//	Touch O/C 정의 

//#define	DDL_CFG_TOUCH_OC



//------------------------------------------------------------------------------
//	MUTE 키 정의

//#define	DDL_CFG_MUTE_KEY



//------------------------------------------------------------------------------
//	Ten Key Cover Switch 정의

//#define	DDL_CFG_COVER_SWITCH

#ifdef	DDL_CFG_COVER_SWITCH
	#define		DDL_CFG_COVER_ACTIVE_HIGH	// Edge2-DM+ -> Cover Up Active High
											// Rose2-IH -> Cover Up Active Low
#endif

//------------------------------------------------------------------------------
//	High Temperature Sensor 정의

#define	DDL_CFG_HIGHTEMP_SENSOR


//------------------------------------------------------------------------------
//	멀티 터치에 의한 일회용 무음 모드 지원

#ifdef	DDL_CFG_TOUCHKEY
	#define	DDL_CFG_ONETIME_MUTE
#endif



//------------------------------------------------------------------------------
//	와치독 정의

#define	DDL_CFG_WATCHDOG


//------------------------------------------------------------------------------
//	Buzzer Volume Type 정의
#define	DDL_CFG_BUZ_VOL_TYPE1
//#define	DDL_CFG_BUZ_VOL_TYPE2



//------------------------------------------------------------------------------
//	Inside Low Battery LED 정의
//#define	DDL_CFG_INSIDE_LED_LOW





//------------------------------------------------------------------------------
//	Lock Setting Menu 정의
//
//#define		DDL_CFG_LOCK_SET_TYPE1	

//Z10-IH, WF20-DMP, A20-IH, Rose2-IH, Edge2-DMP, J20-IH, WV40-DMP, WE40_DMP, WE40-DMP 
//WV-200, WF-200
#define 	DDL_CFG_LOCK_SET_TYPE2

//E300-FH
//#define 	DDL_CFG_LOCK_SET_TYPE4




//------------------------------------------------------------------------------
//	BLE-N 정의
#define	BLE_N_SUPPORT


//------------------------------------------------------------------------------
//	Abov voice index type define
//
//	AVML_MSG_T1 : 2016년 상반기에 정의된 list
//	AVML_MSG_T2 : 2016년 하반기에 정의된 list

//#define	AVML_MSG_T1
#define	AVML_MSG_T2


// POWER_ON_MSG 
// power on 시 MSG 
// AVML_HELLO_MSG2 , AVML_HELLO_MSG1 둘중에 하나를 설정 해서 사용 
// define 하지 않으면 buzzer sound 
//AVML_HELLO_MSG1 당신의 세상을 지키다 게이트맨 
//AVML_HELLO_MSG2 당신의 세상을 지키다 게이트맨 타국 말의 경우 YALE brand 
//#define	POWER_ON_MSG 		AVML_HELLO_MSG2
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//	BLE-N 정의
//	스마트 리빙 BLE 팩 사용시 정의 
//	기본적으로 define 하여 사용
#define	BLE_N_SUPPORT

//	N-Protocol 3.0 적용할 경우 정의(BLE 3.0)
#define	DDL_CFG_BLE_30_ENABLE 0x31 


//------------------------------------------------------------------------------
//	지문 주키 제품을 위한 마스터키 iButton 기능 정의
//#define	_MASTERKEY_IBUTTON_SUPPORT

#define	DDL_CFG_RIM_MOTOR


//------------------------------------------------------------------------------
//	사용자 비밀번호 스케줄 기능 및 Lockout 기능 지원 정의
//#define	_PINCODE_SCHEDULE_SUPPORT

#define	DDL_CFG_NORMAL_DEFAULT		// Normal Mode, Advanced Mode 모두 지원 ; Normal Mode가 Default

//#define DDL_CFG_YALE_ACCESS_SUPPORT  0x1A // slot number 26

//------------------------------------------------------------------------------
//	음성 안내 정의
// 한국어, 중국어 정의 (중국 생산으로 중국어 기본)
//#define	DDL_CFG_LANGUAGE_SET_TYPE1

// 한국어, 중국어, 영어, 스페인어, 포르투갈어, 대만어 정의 (영어 기본)
//#define	DDL_CFG_LANGUAGE_SET_TYPE2

// 한국어, 중국어 정의 (한국 생산으로 한국어 기본)
// E300-FH, WF-200, WS-200
//#define	DDL_CFG_LANGUAGE_SET_TYPE3

// EEPROM 사용 macro 
// ST 내부 EEPROM 사용시 define 
// 외부 EEPROM 사용시 undef 
#define _USE_ST32_EEPROM_
  
// EEPROM 사용 macro 
// 내부 EEPROM 사용시 비밀 번호 , Card UID 암호화 define 
// define 시 비밀 번호 , Card UID 관련 암호 복호 enable 
#ifdef _USE_ST32_EEPROM_
#define _USE_IREVO_CRYPTO_
#endif 

// AES128 사용시 
// 현재 STM32L 은 crypto HW 블럭이 없으므로 STM32 SW lib 을 사용 해야 함 
// STM32_Cryptographic 폴더 안에 STM32CryptographicV3.0.0_CM3_IAR_otnsc.a 사용 중임
// _USE_STM32_CRYPTO_LIB_ 가 선언되면 
// main.h 에서 crypto.h 파일을 include 하게 되어 있음 
// 해당 파일을 include 하면 config.h 파일을 indclude 하는데 
// config.h 파일의 안에 define 여부에 따라 AES 의 종류를 설정 할수 있음 
// 현재는 AES128 ECB 사용으로 설정 
#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
#define _USE_STM32_CRYPTO_LIB_
#endif 

#define DDL_SET_CODE_MAJOR		0x20
#define DDL_SET_CODE_MINOR		0x1A
#define PROGRAM_VERSION_MAJOR	10
#define PROGRAM_VERSION_MINOR	00

#define FW1_MAIN		PROGRAM_VERSION_MAJOR
#define FW1_SUB			PROGRAM_VERSION_MINOR
	
#define _PTODUCT_TYPE_ID1		0xFF		
#define _PTODUCT_TYPE_ID2		0xFF	
#define _PTODUCT_ID1			DDL_SET_CODE_MAJOR
#define _PTODUCT_ID2			DDL_SET_CODE_MINOR

// PC 생산 프로그램에서 테스트 기능을 자동 설정하는 데 필요한 내용(제품 사양에 맞게 설정값 변경하여 사용)
// -> 도어록 입력 추가 상태 확인 내용 1개 존재
//   => Front 9V 존재 (0x01), Front Reset 존재 (0x02)
#define ADD_DDL_STATUS_DATA_NUM                 0x01                // 추가 상태 확인 정보 1개
#define HW_CHECK_DATA                           0x03                // Front 9V | Front Reset   


#endif

