#define		_CALLBACK_FUNCTION_C_

#include "Main.h"


uint32_t	OC_Switch_Int = 0;
uint32_t	REG_Switch_Int = 0;
uint32_t	TouchKey_int = 0;
uint32_t	FingerPrint_int = 0;
uint32_t	Edge_Switch_int = 0;
uint32_t	CommPack_int = 0;


void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if ( htim->Instance == TIM2 ) {
		tim2_count++;
		VoiceCallback();
	}
	else if ( htim->Instance == TIM3 ) {
		tim3_count++;
	}
	else if ( htim->Instance == TIM4 ) {
		tim4_count++;
	}
	else if ( htim->Instance == TIM6 ) {
		BuzzerFeedbackCallback();
		tim6_count++;
	}
#if !defined (M_CLOSE_PWMCH1_Pin) && defined (PWM_BACKTURN_SKIP_COUNT)	
	else if ( htim->Instance == TIM9 ) {
		tim9_count++;
	}	
#endif 	
}


void	HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef *htim)
{
	if ( htim->Instance == TIM3 ) {
		tim3_count++;
	}
#ifdef M_CLOSE_PWMCH1_Pin	
	else if ( htim->Instance == TIM9 ) {
		tim9_count++; //100 count 6ms
		SetMotorPWMFreq();
	}
#endif 	
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
#if defined (P_BT_WAKEUP_T) && defined (P_COM_DDL_EN_T)
	if ( GPIO_Pin == GPIO_PIN_14) { 	// CommPack INT
		InnerPackWakeupInitial();
		CommPack_int++;
		gbWakeUpMinTime10ms = 20;
	}
	else if ( GPIO_Pin == GPIO_PIN_13) { 	// CommPack INT
		PackWakeupInitial();
		CommPack_int++;
		gbWakeUpMinTime10ms = 20;
	}
#elif defined (P_BT_WAKEUP_T)
	if ( GPIO_Pin == GPIO_PIN_14) {	// CommPack INT
		InnerPackWakeupInitial();
		CommPack_int++;
		gbWakeUpMinTime10ms = 20;
	}
#elif defined (P_COM_DDL_EN_T)	
	if ( GPIO_Pin == GPIO_PIN_13) { 	// CommPack INT
		PackWakeupInitial();
		CommPack_int++;
		gbWakeUpMinTime10ms = 20;
	}
#endif 
#ifdef	DDL_CFG_RFID
	else if ( GPIO_Pin == GPIO_PIN_5) {		// RFID IRQ In
		gbRFID_IRQ_In ++;
	}
#endif	
#ifdef	DDL_CFG_IBUTTON
	else if ( GPIO_Pin == GPIO_PIN_3) { 	// I-BUTTON INT
		TkIn();

		gbWakeUpMinTime10ms = 20;
	}
#endif
	else if ( GPIO_Pin == GPIO_PIN_11 ) {	// OC button pressed
		OC_Switch_Int++;
		gbWakeUpMinTime10ms = 20;
		gbKeyOCCheckCnt = 0;
//		KeyInterruptInitial();
	}
#ifdef	DDL_CFG_COVER_SWITCH
#if (DDL_CFG_COVER_SWITCH == 9)
	else if ( GPIO_Pin == GPIO_PIN_9 ) {	// Cover Switch INT
		gbWakeUpMinTime10ms = 20;
	}
#else 
	else if ( GPIO_Pin == GPIO_PIN_0 ) {	// Cover Switch INT
		gbWakeUpMinTime10ms = 20;
	}
#endif 
#endif
	else if ( GPIO_Pin == GPIO_PIN_10 ) {	// Reg Button INT
		REG_Switch_Int++;
		gbWakeUpMinTime10ms = 20;
		KeyInterruptInitial();
	}

#ifdef	DDL_CFG_TOUCHKEY
	else if ( GPIO_Pin == GPIO_PIN_9 ) {	// Touch IC INT
		TouchKey_int++;
		gbWakeUpMinTime10ms = 20;
	}
#endif

#ifdef	DDL_CFG_PUSHKEY
	else if ( GPIO_Pin == GPIO_PIN_8 ) {	// Ten Key * Button
		gbWakeUpMinTime10ms = 20;
		KeyInterruptInitial();
	}
	else if ( GPIO_Pin == GPIO_PIN_9 ) {	// Ten Key Cover Switch INT
		gbWakeUpMinTime10ms = 20;
	}
#endif
#ifdef	DDL_CFG_FP
	else if ( GPIO_Pin == GPIO_PIN_3) {		// FingerPrint module cover INT
		FingerPrint_int++;
		gbWakeUpMinTime10ms = 20;
	}
#endif
#ifdef	DDL_CFG_DS1972_IBUTTON //hyojoon_20160907 I-button
	else if ( GPIO_Pin == GPIO_PIN_3) { 	// I-BUTTON INT
		gbWakeUpMinTime10ms = 20;
	}
#endif
	else if ( GPIO_Pin == GPIO_PIN_12) { 	// Edge INT
		Edge_Switch_int++;
		gbWakeUpMinTime10ms = 20;
	}
#ifdef	P_SNS_F_BROKEN_T		// Front break signal
	else if ( GPIO_Pin == GPIO_PIN_2) { 	// Front Broken INT
		gbWakeUpMinTime10ms = 20;
	}
#endif
#ifdef P_KEY_INTER_CL_T
	else if ( GPIO_Pin == GPIO_PIN_1) { 	// cylinder key 
		gbWakeUpMinTime10ms = 20;
	}
#endif 
#ifdef	P_MUTE_T				
	else if ( GPIO_Pin == GPIO_PIN_0) { 	// Mute Key INT
		gbWakeUpMinTime10ms = 20;
		KeyInterruptInitial();
	}
#endif
#ifdef	_MASTERKEY_IBUTTON_SUPPORT
	else if ( GPIO_Pin == GPIO_PIN_0) { 	// Master Key iButton INT
		gbWakeUpMinTime10ms = 20;
	}
#endif
#ifdef DDL_CFG_AUTO_KEY_PROCESS
	else if ( GPIO_Pin == GPIO_PIN_4) { 	// Auto key 
		gbWakeUpMinTime10ms = 20;
	}
#endif 
	SetLockStatusInfo(LOCK_LAST_GPIO_INT_PIN_LSB,((BYTE)(GPIO_Pin)));
	SetLockStatusInfo(LOCK_LAST_GPIO_INT_PIN_MSB,(GPIO_Pin >> 8u));
#ifdef DDL_CFG_LONG_SLEEP
	extern uint8_t LongSleepOn;
	LongSleepOn = 0;
#endif 
}


