#ifndef	_CALLBACK_FUNCTION_H_
#define	_CALLBACK_FUNCTION_H_

#ifdef	_CALLBACK_FUNCTION_C_

uint32_t	tim2_count;
uint32_t	tim3_count;
uint32_t	tim4_count;
uint32_t	tim6_count;
#if defined (M_CLOSE_PWMCH1_Pin) ||defined (PWM_BACKTURN_SKIP_COUNT)
uint32_t	tim9_count;
#endif 


#ifdef	DDL_CFG_RFID
uint8_t	gbRFID_IRQ_In = 0;
#endif

#else

extern	uint32_t	tim2_count;
extern	uint32_t	tim3_count;
extern	uint32_t	tim4_count;
extern	uint32_t	tim6_count;

#if defined (M_CLOSE_PWMCH1_Pin) ||defined (PWM_BACKTURN_SKIP_COUNT)
extern	uint32_t	tim9_count;
#endif 

#ifdef	DDL_CFG_RFID
extern	uint8_t	gbRFID_IRQ_In;
#endif

#endif

#endif

