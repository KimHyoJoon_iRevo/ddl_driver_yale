#ifndef	__KEYINPUT_INCLUDED
#define	__KEYINPUT_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"


																				
//Define Key								
#define	TENKEY_0			0					// 0 입력
#define	TENKEY_1			1					// 1 입력
#define	TENKEY_2			2					// 2 입력
#define	TENKEY_3			3					// 3 입력
#define	TENKEY_4			4					// 4 입력
#define	TENKEY_5			5					// 5 입력
#define	TENKEY_6			6					// 6 입력
#define	TENKEY_7			7					// 7 입력
#define	TENKEY_8			8					// 8 입력
#define	TENKEY_9			9					// 9 입력
#define	TENKEY_STAR			10					// * 입력
#define	TENKEY_SHARP		12					//# 입력
#define	FUNKEY_OPCLOSE		13					//OPEN/CLOSE
#define	FUNKEY_REG			14					//등록키
#define	FUNKEY_MENU			15					//MENU KEY
#define TENKEY_MULTI		17					// 3개 이상 입력
#define	FUNKEY_MUTE			18					//MUTE KEY
#define	TENKEY_NONE			254					// 0 입력
#define	TENKEY_ERROR		255					// 2개 이상 입력

#define	FUNKEY_OPCLOSE_PRESS		20					//OPEN/CLOSE
#define	FUNKEY_OPCLOSE_RELEASE		21					//OPEN/CLOSE



// OChyojoon.kim
extern BYTE gbNewFunctionKey;
extern BYTE gbOldFunctionKey;

extern BYTE gbNewOCFunctionKey;
extern BYTE gbOldOCFunctionKey;


extern BYTE gbInputKeyValue;
extern BYTE gbNewKeyBuffer;
extern BYTE gbOldKeyBuffer;
extern BYTE gbKeyOCCheckCnt;

extern FLAG NewPort;
#define	gbNewPortChatering	NewPort.STATEFLAGDATA

#ifdef	P_SNS_EDGE_T			// Edge sensor
#define	gfDoorSwOpenState	NewPort.STATEFLAG.Bit0				
#endif

#define	gfAutoLockState		NewPort.STATEFLAG.Bit1
#define	gfFrontBroken			NewPort.STATEFLAG.Bit2
#define	gfCoverSw			NewPort.STATEFLAG.Bit3				//커버 스위치
#ifdef P_KEY_INTER_CL_T
#define	gfkeyInterCl			NewPort.STATEFLAG.Bit4
#endif 



extern FLAG PortVal;
#define	gbPortVal			PortVal.STATEFLAGDATA

#ifdef	P_SNS_EDGE_T			// Edge sensor
#define	gfXORDoorSw			PortVal.STATEFLAG.Bit0
#endif

#define	gfXORAutoLockSw		PortVal.STATEFLAG.Bit1
#define	gfXORFrontBroken		PortVal.STATEFLAG.Bit2
#define	gfXORCoverSw		PortVal.STATEFLAG.Bit3
#ifdef P_KEY_INTER_CL_T
#define	gfXORkeyInterCl		PortVal.STATEFLAG.Bit4
#endif 


extern BYTE gbKeyInitialCnt;


void SwitchCheckRtn(void);



void KeyChattering(void);
BYTE KeyInputCheck(void);
void KeyInterruptInitial(void);

void IntialKeySwtichSet(void);



#endif
