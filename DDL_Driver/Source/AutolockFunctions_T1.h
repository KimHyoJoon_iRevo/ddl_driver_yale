//------------------------------------------------------------------------------
/** 	@file		AutolockFunctions_T1.h
	@brief	AutoRelock Process
*/
//------------------------------------------------------------------------------

#ifndef __AUTOLOCKFUNCTIONS_T1_INCLUDED
#define __AUTOLOCKFUNCTIONS_T1_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"



//------------------------------------------------------------------------------
// gLockStatusPrcsStep에서 참조 - 자동 잠김 수행 단계
//------------------------------------------------------------------------------
enum{
	LOCKSTS_AUTOLOCK_CHECK = 1,
	LOCKSTS_AUTOLOCK_CLOSE,
	LOCKSTS_AUTOLOCK_CLOSED_CHK,
	LOCKSTS_AUTOLOCK_CLOSED_ERROR,
	LOCKSTS_ALARM_CHECK,
	LOCKSTS_AUTOLOCK_OPENED_CHK,
};


//------------------------------------------------------------------------------
/*! 	\def	_MAX_AUTOLOCK_TRYCNT		
	gAutoLockTryCnt에서 참조- 자동 잠김 수행 최대 회수, 총 2회 구동
*/
//------------------------------------------------------------------------------
#define	_MAX_AUTOLOCK_TRYCNT			2


//------------------------------------------------------------------------------
/*! 	\def	DEFAULT_AUTOLOCK_TIME		
	gLockStatusTimer1s에서 참조 - 문이 닫힐 때 3초 자동 잠김 시간
	
	\def	DEFAULT_RELOCK_TIME		
	gLockStatusTimer1s에서 참조 - 문이 닫혀 있을 때 7초 자동 잠김 시간
*/
//------------------------------------------------------------------------------
#define	DEFAULT_AUTOLOCK_TIME			3
#define	DEFAULT_RELOCK_TIME				7
//#define	MIN_RELOCK_TIME					7
//#define	MAX_RELOCK_TIME					60


extern BYTE gLockStatusTimer1s;


void LockStatusCheckTimeCount(void);
void LockStatusCheckTimeClear(void);
void LockStatusCheckClear(void);

BYTE AutoLockSetCheck(void);
BYTE AutoRelockProcessClearCheck(void);

void AutoRelockEnable(void);
void AutoRelockDisable(void);
void AutoRelockTimeCounter(void);
void LockStatusProcess(void);
BYTE GetLockStatusPrcsStep(void);

#ifdef	P_SNS_EDGE_T			// Edge sensor
void DoorClosedCheckCounter(void);
void DoorClosedTimeSet(void);
BYTE DoorClosedTimeCheck(void);
#endif

// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 시작
#ifdef		P_SNS_LOCK_T	
extern BYTE gbInnerForceLockDetectionValue;
extern BYTE gbInnerForceLockDetectionValueBackUp;
#endif
// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 끝


BYTE GetRemainingAutoRelockTime(void);

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
extern BYTE gbFeedbackCloseOnceTimercnt100ms;
#endif

#ifdef DDL_CFG_AUTO_KEY_PROCESS
void AutoSwProcess(void);
#endif 

#endif

