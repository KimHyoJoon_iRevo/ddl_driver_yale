#include	"Main.h"
WORD gwAdcValue = 0;



WORD ADCCheck(BYTE bMode)
{
	ADC_ChannelConfTypeDef sConfig;

	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_192CYCLES;

	//1개의 채널만 ADC한다.
	if(bMode == ADC_BATTERY_CHECK) {
		sConfig.Channel = ADC_CHANNEL_13;
	}
	else if(bMode == ADC_TEMPERATURE_CHECK) {
		sConfig.Channel = ADC_CHANNEL_12;
	}
	else if(bMode == ADC_KEY_1_CHECK) {
		sConfig.Channel = ADC_CHANNEL_9;
	}
	else if(bMode == ADC_KEY_2_CHECK) {
		sConfig.Channel = ADC_CHANNEL_8;
	}
	else if(bMode == ADC_VOLUME_CHECK) {
		sConfig.Channel = ADC_CHANNEL_11;
	}
#ifdef	ST_TEMPERATURE_SENSOR
	else if(bMode == ADC_TEMPERATURE_CHECK_ST) {
		sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
	}
	else if(bMode == ADC_VREFINT_ST) {
		sConfig.Channel = ADC_CHANNEL_VREFINT;
	}	
#endif 	
	else{
		return	gwAdcValue = 0;
	}

	HAL_ADC_ConfigChannel( gh_mcu_adc, &sConfig );

	HAL_ADC_Start( gh_mcu_adc );

	HAL_ADC_PollForConversion( gh_mcu_adc, MCU_ADC_TIMEOUT );		//conversion이 끝날 때까지 기다림.
	gwAdcValue = HAL_ADC_GetValue( gh_mcu_adc );

	HAL_ADC_Stop( gh_mcu_adc );

#ifndef	ST_TEMPERATURE_SENSOR
	if(gwAdcValue > 5000) {
		gwAdcValue >>= 4;
	}
#endif 	

	return gwAdcValue;	
}



