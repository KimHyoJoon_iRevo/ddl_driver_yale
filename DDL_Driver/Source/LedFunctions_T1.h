//------------------------------------------------------------------------------
/** 	@file		LedFunctions_T1.c
	@brief	Led Control with or without Dimming IC Philips PCA9532 Interface Functions 
*/
//------------------------------------------------------------------------------
#ifndef __LEDFUNCTIONS_T1_INCLUDED        
#define __LEDFUNCTIONS_T1_INCLUDED   


#include "DefineMacro.h"
//#include "DefinePin.h"


//------------------------------------------------------------------------------
/*! 	\def	LED_ENDD		
	LedProcess 함수 구동 과정에서 구동 종료를 나타내는 Data 
*/
//------------------------------------------------------------------------------
#define	LED_ENDD				0xFF



BYTE GetLedMode(void);
void LedProcessTimeCount(void);
void LedProcess(void);
void LedSetting(BYTE const* bpLedData, BYTE bMode);
void LedModeRefresh(void);

void LedForcedAllOff(void);


//LedSetting의 bMode, gbLedDisplayMode
#define	LED_DISPLAY_OFF			0
#define	LED_KEEP_DISPLAY		1


//------------------------------------------------------------------------------
// LED Data 생성을 위한 구조체
//	pLedBuf : 출력할 LED Data 종류
//	DimmLedNewSwitch : 생성할 Dimming Data의 출력할 LED
//	DimmDisplayType : 생성할 Dimming Data의 출력 방식
//	DimmBlinkKey : 생성할 Dimming Data에서 선택된 특정 키
//------------------------------------------------------------------------------
typedef struct{
	BYTE *pLedBuf;

	WORD DimmLedNewSwitch;
	BYTE DimmDisplayType;
	BYTE DimmBlinkKey;
	BYTE WaitTimeForNextDisplay;
	BYTE DisplayMode;
}LedType_Diplay;


extern const BYTE gcbWaitLedNextData[]; 

#ifdef DDL_CFG_FP
extern BYTE gbLedDisplayFPMOperation;
#endif 


//LED Dimmer가 존재하면...
#ifdef	DDL_CFG_DIMMER

//------------------------------------------------------------------------------
/*! 	\def	I2C_DIMMING_ADDRESS		
	Dimming IC의 I2C Device ID (Slave Address)
*/
//------------------------------------------------------------------------------
#define	I2C_DIMMING_ADDRESS		0xC0


extern WORD gDimmLedLastSwitch;	


extern const BYTE gcbLedKeyOff[];
extern const BYTE gcbLedErr[];
extern const BYTE gcbLedLock[];
extern const BYTE gcbLedOk[];
extern const BYTE gcbLedAlert[];

extern const BYTE gcbLedOpen[];
extern const BYTE gcbLedClose[];
extern const BYTE gcbLedFPMOperation[];



void SetupDimmingLed( ddl_i2c_t *h_i2c_dev );
void FLedWrite(void);

void LedGenerate(WORD LedSwitch, BYTE BlinkOn, BYTE BlinkNum);
void LastDimmingDisplayClear(BYTE DimmType);
//void DimmOneKeyFastBlink(BYTE KeyVal);



//------------------------------------------------------------------------------
// DimmDisplayType에서 참조 - Dimming LED 출력 방식 결정
//	LED_ALL_OFF : 모든 LED Off
//	LED_SELECT_ON : 설정된 LED만 On, 나머지 LED Off
//	BLINK_DIMM_ON : 설정된 LED에서 선택된 Key LED만 Dimming Off > Dimming On 수행	
//	BLINK_ON : 설정된 LED에서 선택된 Key LED만 Off->On 수행	
//	BLINK_OFF : 설정된 LED에서 선택된 Key LED만 Off 수행	
//	DIMM_ON_SLOW : 설정된 LED 전체가 Dimming On Slow	
//	DIMM_ON_NORMAL : 설정된 LED 전체가 Dimming On with Normal Speed	
//	DIMM_ON_FAST : 설정된 LED 전체가 Dimming On Fast	
//	DIMM_OFF_SLOW : 설정된 LED 전체가 Dimming Off Slow	
//	DIMM_OFF_NORMAL : 설정된 LED 전체가 Dimming Off with Normal Speed	
//	DIMM_OFF_FAST : 설정된 LED 전체가 Dimming Off Fast	
//------------------------------------------------------------------------------
enum{
	LED_ALL_OFF = 0,
	LED_SELECT_ON,
	BLINK_DIMM_ON,
	BLINK_ON,
	BLINK_OFF,

	DIMM_ON_SLOW,
	DIMM_ON_NORMAL,
	DIMM_ON_FAST,

	DIMM_OFF_SLOW,
	DIMM_OFF_NORMAL,
	DIMM_OFF_FAST,
};


//------------------------------------------------------------------------------
// ConvertLedSwitchBitToLSxData 함수에서 각각의 LED 제어에 사용하기 위한 LED 설정
//	회로도에서 연결하는 순서와 동일하게 Define되어야 함.
// 	LED 기능이 바뀌면 다시 설정되어야 함.
// 
// 	TEN KEY 값은 0~12 값에 있어야 함.
// 	13~15 값은 기타 LED 값으로 배정
//------------------------------------------------------------------------------
#define		FUNC_NO_ASSIGN		0xFF
//#ifdef	__DDL_MODEL_GRAB100_FH
#if defined (__DDL_MODEL_GRAB100_FH) || defined (__DDL_MODEL_X500_FH)

#define		FUNC_FPM_OPERATION	11
#else 
#define		FUNC_FPM_OPERATION	0xFF
#endif 
#define		FUNC_DEADBOLT_JAM	13
#define		FUNC_LOW_BATTERY	14
#define		FUNC_OPERATION		15


#define		LS0_LED0		TENKEY_6
#define		LS0_LED1		TENKEY_3
#define		LS0_LED2		TENKEY_2
#define		LS0_LED3		TENKEY_5
#define		LS1_LED4		TENKEY_1
#define		LS1_LED5		TENKEY_4
#define		LS1_LED6		TENKEY_7
#define		LS1_LED7		TENKEY_STAR
#define		LS2_LED8		FUNC_DEADBOLT_JAM	
#define		LS2_LED9		TENKEY_8
#define		LS2_LED10		TENKEY_0
#define		LS2_LED11		FUNC_LOW_BATTERY	
#define		LS3_LED12		FUNC_OPERATION		
#define		LS3_LED13		FUNC_FPM_OPERATION
#define		LS3_LED14		TENKEY_SHARP
#define		LS3_LED15		TENKEY_9


//------------------------------------------------------------------------------
// DimmLedNewSwitch 변수에서 각각의 LED 제어에 사용하기 위한 LED 설정
//	LedGenerate 함수 호출 시 첫번째 인자(LedSwitch)에 사용됨.
//	LED_KEY_0 값이 bit 0 (lsb), LED_설정값이 갈수록 높은 bit에 배치되는 구조
//    LED 기능이 바뀌면 다시 설정되어야 함.
//------------------------------------------------------------------------------
#define	LED_KEY_0			(1<<TENKEY_0)
#define	LED_KEY_1			(1<<TENKEY_1)
#define	LED_KEY_2			(1<<TENKEY_2)
#define	LED_KEY_3			(1<<TENKEY_3)
#define	LED_KEY_4			(1<<TENKEY_4)
#define	LED_KEY_5			(1<<TENKEY_5)
#define	LED_KEY_6			(1<<TENKEY_6)
#define	LED_KEY_7			(1<<TENKEY_7)
#define	LED_KEY_8			(1<<TENKEY_8)
#define	LED_KEY_9			(1<<TENKEY_9)
#define	LED_KEY_STAR		(1<<TENKEY_STAR)
#define	LED_KEY_SHARP		(1<<TENKEY_SHARP)

#define	LED_DEADBOLT_JAM	(1<<FUNC_DEADBOLT_JAM)
#define	LED_LOW_BATTERY		(1<<FUNC_LOW_BATTERY)
#define	LED_OPERATION		(1<<FUNC_OPERATION)
#define	LED_FPM_OPERATION		(FUNC_FPM_OPERATION == 0xFF ? 0x00 : (1<<FUNC_FPM_OPERATION))

#define	LED_TENKEY_ALL		(LED_KEY_0|LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_6|LED_KEY_7|LED_KEY_8|LED_KEY_9)
#define LED_KEYPAD_ALL		(LED_TENKEY_ALL|LED_KEY_STAR|LED_KEY_SHARP)
#define	LED_ALL				(LED_KEYPAD_ALL|LED_DEADBOLT_JAM|LED_LOW_BATTERY|LED_OPERATION|LED_FPM_OPERATION)




//==============================================================
//	TEST FUNCTIONS
//==============================================================
// 아래 함수는 LED Dimming 제어 함수가 수정되었을 때 제대로 동작하는지 여부를 확인하기 위한 테스트 함수임

#if 0
void DimmLedTestProcess(void);
void DimmLedTestTimeCount(void);
#endif







#define	LED_ERROR_DISPLAY		0x04
#define LED_STAR_BLINK       	0x20
#define LED_SHARP_BLINK       	0x40
#define LED_STAR_SHARP_BLINK  	0x60


extern FLAG LEDFlag;
#define gfKeyLED			LEDFlag.STATEFLAG.Bit1			



extern const BYTE gcbLedPwr[];
extern const BYTE gcbLedOff[];
extern const BYTE gcbLedOutLock1[];
extern const BYTE gcbLedOutLock2[];
extern const BYTE gcbLedOutLock3[];
extern const BYTE gcbLedOutLock4[];
extern const BYTE gcbLedDimmOn[];
extern const BYTE gcbLedDimmOff[];
extern const BYTE gcbLedLock3Min1[];
extern const BYTE gcbLedLock3Min2[];
extern const BYTE gcbLedLock3Min3[];
extern const BYTE gcbLedErr3[];

extern const BYTE gcbLedFMERROR[];
extern const BYTE gcbLedFM[];
extern const BYTE gcbLedFM2[];
extern const BYTE gcbLedFM3[];
extern const BYTE gcbLedFMREG[];
extern const BYTE gcbLedFMREG1[];
extern const BYTE gcbLedFMREG2[];
extern const BYTE gcbLedFMREG3[];

#if defined (DDL_CFG_FP_PFM_3000) || defined (DDL_CFG_FP_TS1071M)
extern const BYTE gcbLedAREAFMREG[];
extern const BYTE gcbLedAREAFMREGSTEP[8][8];
#endif 

#if defined (DDL_CFG_FP_HFPM)
extern const BYTE gcbLedAREAFMREGSTEP[3][8];
#endif 

void LedKeyNumSetting(BYTE bKeyValue);
void LedRecCountSetting(BYTE bcount, BYTE bMode);

void LedPWLedSetting(BYTE bNum);
void LedIndivLedSetting(BYTE bNum);

void LedIndivLedDataClear(void);


extern BYTE gcbLedMultilevelUI[20];

#if 1
#define	KEY0				(0x0001 << 0)
#define	KEY1				(0x0001 << 1)
#define	KEY2				(0x0001 << 2)
#define	KEY3				(0x0001 << 3)
#define	KEY4				(0x0001 << 4)
#define	KEY5				(0x0001 << 5)
#define	KEY6				(0x0001 << 6)
#define	KEY7				(0x0001 << 7)
#define	KEY8				(0x0001 << 8)
#define	KEY9				(0x0001 << 9)
#define	KEYSTAR				(0x0001 << 10)
#define	KEYSHARP			(0x0001 << 12)
#define	KEYALLON			(0x1fff)
#define	KEYALLOFF			(0x0000)
#define	KEY123				(KEY1 | KEY2 | KEY3)
#define	KEY456				(KEY4 | KEY5 | KEY6)
#define	KEY789				(KEY7 | KEY8 | KEY9)
#define	KEYSTAR0SHARP		(KEYSTAR | KEY0 | KEYSHARP)
#define	KEYNUMALL			(KEY1 | KEY2 | KEY3 | KEY4 | KEY5 | KEY6 | KEY7 | KEY8 | KEY9 | KEY0)
#define	KEYSTARSHARP 		(KEYSTAR | KEYSHARP)
#endif
#if 0
#define	KEY1				(0x0001 << 0)
#define	KEY2				(0x0001 << 1)
#define	KEY3				(0x0001 << 2)
#define	KEY4				(0x0001 << 3)
#define	KEY5				(0x0001 << 4)
#define	KEY6				(0x0001 << 5)
#define	KEY7				(0x0001 << 6)
#define	KEY8				(0x0001 << 7)
#define	KEY9				(0x0001 << 8)
#define	KEYSTAR			(0x0001 << 9)
#define	KEY0				(0x0001 << 10)
#define	KEYSHARP			(0x0001 << 11)
#define	KEYCANCLE			(0x0001 << 12)
#define	KEYALLON			(0x1fff)
#define	KEYALLOFF			(0x0000)
#define	KEY123				(KEY1 | KEY2 | KEY3)
#define	KEY456				(KEY4 | KEY5 | KEY6)
#define	KEY789				(KEY7 | KEY8 | KEY9)
#define	KEYSTAR0SHARP		(KEYSTAR | KEY0 | KEYSHARP)
#define	KEYNUMALL			(KEY1 | KEY2 | KEY3 | KEY4 | KEY5 | KEY6 | KEY7 | KEY8 | KEY9 | KEY0)
#define	KEYSTARSHARP 		(KEYSTAR | KEYSHARP)
#endif

//extern BYTE gbLedDisplayMode;

void MultilevelUILedGenerate(WORD UISwitch, BYTE BlinkOn, BYTE BlinkNum);

#endif		// End of "false of ifdef DDL_CFG_DIMMER"








#ifdef	DDL_CFG_LED_TYPE1
// LED_DECO, LED_BACK이 있는 경우

#define	NODIM_LED_ALL_ON		1
#define	NODIM_LED_ALL_OFF		2
#define	NODIM_LED_DECO_ON		3
#define	NODIM_LED_DECO_OFF		4


extern const BYTE gcbNoDimLed100msOn[];
extern const BYTE gcbNoDimLed2sOn[];
extern const BYTE gcbNoDimLedBlink[];
extern const BYTE gcbNoDimLedTamperLockout[];
extern const BYTE gcbNoDimLedOff[];
extern const BYTE gcbNoDimLedErr3[];
extern const BYTE gcbNoDimLedErr4[];


void SetupLed(void);


#endif		// End of "false of ifdef DDL_CFG_LED_TYPE1"




#ifdef	DDL_CFG_LED_TYPE2
// LED_TENKEY, LED_LOWBAT, LED_DEAD가 있는 경우

#define	NODIM_LED_ALL_ON		1
#define	NODIM_LED_ALL_OFF		2
#define	NODIM_LED_BACK_ON		3
#define	NODIM_LED_BACK_OFF		4


extern const BYTE gcbNoDimLed100msOn[];
extern const BYTE gcbNoDimAllLed2sOn[];
extern const BYTE gcbNoDimLed2sOn[];
extern const BYTE gcbNoDimLedBlink[];
extern const BYTE gcbNoDimLedTamperLockout[];
extern const BYTE gcbNoDimLedOff[];
extern const BYTE gcbNoDimLedErr3[];
extern const BYTE gcbNoDimLedErr4[];


void SetupLed(void);


#endif		// End of "false of ifdef DDL_CFG_LED_TYPE2"


#ifdef	DDL_CFG_LED_TYPE3
// LED_TENKEY, LED_LOWBAT, LED_DEAD, LED_IB가 있는 경우

#define	NODIM_LED_ALL_ON		1
#define	NODIM_LED_ALL_OFF		2
#define	NODIM_LED_BACK_ON		3
#define	NODIM_LED_BACK_OFF		4
#define	NODIM_LED_DECO_ON		5
#define	NODIM_LED_DECO_OFF		6
#define	NODIM_LED_BACKDECO_ON	7
#define	NODIM_LED_BACKDECO_OFF	8


extern const BYTE gcbNoDimLed100msOn[];
extern const BYTE gcbNoDimAllLed2sOn[];
extern const BYTE gcbNoDimLed2sOn[];
extern const BYTE gcbNoDimLedBlink[];
extern const BYTE gcbNoDimLedBlinkWithBack[];
extern const BYTE gcbNoDimLedTamperLockout[];
extern const BYTE gcbNoDimLedOff[];
extern const BYTE gcbNoDimLedErr3[];
extern const BYTE gcbNoDimLedErr4[];


void SetupLed(void);


#endif		// End of "false of ifdef DDL_CFG_LED_TYPE3"



#ifdef	DDL_CFG_LED_TYPE4
// LED_DECO, LED_BACK이 있는 경우

#define	NODIM_LED_ALL_ON		1
#define	NODIM_LED_ALL_OFF		2
#define	NODIM_LED_DECO_ON		3
#define	NODIM_LED_DECO_OFF		4
#define	NODIM_LED_BACK_ON		5


extern const BYTE gcbNoDimLed100msOn[];
extern const BYTE gcbNoDimAllLed2sOn[];
extern const BYTE gcbNoDimLed2sOn[];
extern const BYTE gcbNoDimLedBlink[];
extern const BYTE gcbNoDimLedTamperLockout[];
extern const BYTE gcbNoDimLedOff[];
extern const BYTE gcbNoDimLedErr3[];
extern const BYTE gcbNoDimLedErr4[];


void SetupLed(void);


#endif		// End of "false of ifdef DDL_CFG_LED_TYPE4"

#ifdef	DDL_CFG_LED_TYPE5
// LED_TENKEY, LED_LOWBAT, LED_DEAD가 있는 경우

#define	NODIM_LED_ALL_ON		1
#define	NODIM_LED_ALL_OFF		2
#define	NODIM_LED_BACK_ON		3
#define	NODIM_LED_BACK_OFF		4
#define	NODIM_LED_OPEN_ON		5
#define	NODIM_LED_OPEN_OFF		6
#define	NODIM_LED_CLOSE_ON		7
#define	NODIM_LED_CLOSE_OFF		8


extern const BYTE gcbNoDimLed100msOn[];
extern const BYTE gcbNoDimAllLed2sOn[];
extern const BYTE gcbNoDimLed2sOn[];
extern const BYTE gcbNoDimLedBlink[];
extern const BYTE gcbNoDimLedTamperLockout[];
extern const BYTE gcbNoDimLedOff[];
extern const BYTE gcbNoDimLedErr3[];
extern const BYTE gcbNoDimLedErr4[];
extern const BYTE gcbNoDimLedOpen[];
extern const BYTE gcbNoDimLedClose[];
extern const BYTE gcbNoDimBackOpenLed2sOn[];
extern const BYTE gcbNoDimBackCloseLed2sOn[];



void SetupLed(void);


#endif		// End of "false of ifdef DDL_CFG_LED_TYPE2"

#ifdef	DDL_CFG_LED_TYPE6
// LED_TENKEY, LED_LOW, LED_OPEN , LED CLOSE가 있는 경우

#define	NODIM_LED_ALL_ON		1
#define	NODIM_LED_ALL_OFF		2
#define	NODIM_LED_BACK_ON		3
#define	NODIM_LED_BACK_OFF		4
#define	NODIM_LED_OPEN_ON		5
#define	NODIM_LED_OPEN_OFF		6
#define	NODIM_LED_CLOSE_ON		7
#define	NODIM_LED_CLOSE_OFF		8


extern const BYTE gcbNoDimLed100msOn[];
extern const BYTE gcbNoDimAllLed2sOn[];
extern const BYTE gcbNoDimLed2sOn[];
extern const BYTE gcbNoDimLedBlink[];
extern const BYTE gcbNoDimLedTamperLockout[];
extern const BYTE gcbNoDimLedOff[];
extern const BYTE gcbNoDimLedErr3[];
extern const BYTE gcbNoDimLedErr4[];
extern const BYTE gcbNoDimLedOpen[];
extern const BYTE gcbNoDimLedClose[];
extern const BYTE gcbNoDimBackOpenLed2sOn[];
extern const BYTE gcbNoDimBackCloseLed2sOn[];



void SetupLed(void);


#endif		// End of "false of ifdef DDL_CFG_LED_TYPE2"

#ifdef	DDL_CFG_LED_TYPE7
// LED_TENKEY, LED_LOWBAT, LED_DEAD가 있는 경우

#define	NODIM_LED_ALL_ON		0x01
#define	NODIM_LED_ALL_OFF		0x02
#define	NODIM_LED_BACK_ON		0x03
#define	NODIM_LED_BACK_OFF	0x04

#define	NODIM_LED_RED_ON		0x05
#define	NODIM_LED_RED_OFF		0x06
#define	NODIM_LED_GREEN_ON	0x07
#define	NODIM_LED_GREEN_OFF	0x08
#define	NODIM_LED_BLUE_ON		0x09
#define	NODIM_LED_BLUE_OFF	0x0A
#define	NODIM_LED_YELLOW_ON	0x0B
#define	NODIM_LED_YELLOW_OFF	0x0C





extern const BYTE gcbNoDimLed100msOn[];
extern const BYTE gcbNoDimAllLed2sOn[];
extern const BYTE gcbNoDimLed2sOn[];
extern const BYTE gcbNoDimLedBlink[];
extern const BYTE gcbNoDimLedTamperLockout[];
extern const BYTE gcbNoDimLedOff[];
extern const BYTE gcbNoDimLedErr3[];
extern const BYTE gcbNoDimLedErr4[];

extern const BYTE gcbNoDimPassage[];
extern const BYTE gcbNoDimSafety[];
extern const BYTE gcbNoDimSecure[];

extern const BYTE gcbNoDimAutoLockHold[];
extern const BYTE gcbNoDimAutoLockUnHold[];





void SetupLed(void);


#endif		// End of "false of ifdef DDL_CFG_LED_TYPE7"


#ifdef	DDL_CFG_LED_TYPE8

#define	NODIM_LED_ALL_ON		1
#define	NODIM_LED_ALL_OFF		2
#define	NODIM_LED_BACK_ON		3
#define	NODIM_LED_BACK_OFF		4
#define	NODIM_LED_OPEN_ON		5
#define	NODIM_LED_OPEN_OFF		6
#define	NODIM_LED_CLOSE_ON		7
#define	NODIM_LED_CLOSE_OFF		8


extern const BYTE gcbNoDimLed100msOn[];
extern const BYTE gcbNoDimAllLed2sOn[];
extern const BYTE gcbNoDimLed2sOn[];
extern const BYTE gcbNoDimLedBlink[];
extern const BYTE gcbNoDimLedOff[];
extern const BYTE gcbNoDimLedErr3[];
extern const BYTE gcbNoDimLedErr4[];
extern const BYTE gcbNoDimLedTamperLockout[];
extern const BYTE gcbLedOk[];
extern const BYTE gcbLedOff[];
extern const BYTE gcbLedOutLock1[];
extern const BYTE gcbLedOutLock2[];
extern const BYTE gcbLedOutLock3[];
extern const BYTE gcbLedOutLock4[];








void SetupLed(void);
#endif		// End of "false of ifdef DDL_CFG_LED_TYPE2"



#endif
