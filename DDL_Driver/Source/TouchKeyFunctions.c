/*
	TSM12 TouchIC

		TSM12 의 Interrupt 핀은 Open Drain 출력인데, 회로에는 외부 pull-up이 없다.
		따라서 CPU GPIO pin 에게 내부 pull-up을 설정해 줘야하니,
		CubeMX의 GPIO pin 설정에서 pull-up을 해줘야 interrupt가 발생한다.
*/

#define		_TOUCH_KEY_FUNCTIONS_C_


#include	"main.h"


//TSM12 Register Map Address
#define _TSM12_ADDR_SENSITIVITY1	0x02
#define _TSM12_ADDR_SENSITIVITY2	0x03
#define _TSM12_ADDR_SENSITIVITY3	0x04
#define _TSM12_ADDR_SENSITIVITY4	0x05
#define _TSM12_ADDR_SENSITIVITY5	0x06
#define _TSM12_ADDR_SENSITIVITY6	0x07
#define _TSM12_ADDR_CTRL1			0x08
#define _TSM12_ADDR_CTRL2			0x09
#define _TSM12_ADDR_REF_RST1		0x0A
#define _TSM12_ADDR_REF_RST2		0x0B
#define _TSM12_ADDR_CH_HOLD1		0x0C
#define _TSM12_ADDR_CH_HOLD2		0x0D
#define _TSM12_ADDR_CAL_HOLD1		0x0E
#define _TSM12_ADDR_CAL_HOLD2		0x0F
#define _TSM12_ADDR_OUTPUT1		0x10
#define _TSM12_ADDR_OUTPUT2		0x11
#define _TSM12_ADDR_OUTPUT3		0x12


#ifdef	TOUCH_IC_SENSITIVITY_VALUE
//TSM12 Init DATA
#define _TSM12_SENSITIVITY1		TOUCH_IC_SENSITIVITY_VALUE	//Ch1-9, 2-6	
#define _TSM12_SENSITIVITY2		TOUCH_IC_SENSITIVITY_VALUE	//Ch3-3, 4-#	
#define _TSM12_SENSITIVITY3		TOUCH_IC_SENSITIVITY_VALUE	//Ch5-0, 6-8	
#define _TSM12_SENSITIVITY4		TOUCH_IC_SENSITIVITY_VALUE	//Ch7-2, 8-5	
#define _TSM12_SENSITIVITY5		TOUCH_IC_SENSITIVITY_VALUE	//Ch9-*, 10-1	
#define _TSM12_SENSITIVITY6		TOUCH_IC_SENSITIVITY_VALUE	//Ch11-4,12-7	
#else
//TSM12 Init DATA
#define _TSM12_SENSITIVITY1		0x99	//Ch1-9, 2-6	//1100 1100
#define _TSM12_SENSITIVITY2		0x99	//Ch3-3, 4-#	//1100 1100
#define _TSM12_SENSITIVITY3		0x99	//Ch5-0, 6-8	//1100 1100
#define _TSM12_SENSITIVITY4		0x99	//Ch7-2, 8-5	//1100 1100
#define _TSM12_SENSITIVITY5		0x99	//Ch9-*, 10-1	//1100 1100
#define _TSM12_SENSITIVITY6		0x99	//Ch11-4,12-7	//1100 1100
#endif

#ifdef	TOUCH_IC_RESPONSE_TIME_CONTROL_VALUE
#define	_TSM12_RTC 0x31
#else 
#define	_TSM12_RTC 0x33
#endif 

#define _TSM12_CTRL1			0xA1		// 1 01 00 001
#define _TSM12_CTRL2			0x07		// 0000 0011
#define _TSM12_REF_RST1			0x00
#define _TSM12_REF_RST2			0x00
#define _TSM12_CH_HOLD1		0x00
#define _TSM12_CH_HOLD2		0x00
#define _TSM12_CAL_HOLD1		0x00
#define _TSM12_CAL_HOLD2		0x00




#define		TOUCH_I2CBUFF_SZ	19

BYTE	gbTouchKey_i2cbuff[ TOUCH_I2CBUFF_SZ+1 ];


ddl_i2c_t		*h_touchkey_i2c_dev;

FLAG	TKeyFLAG;

BYTE	gbTKeySensVal = 0xAA;

WORD	gbTKeyCheckCnt = 0;
BYTE	gbTKeyTimer2ms = 0;

BYTE	gbTouchKeyPrcsStep = 0; 



void RegisterTouchKey( ddl_i2c_t *h_i2c_dev )
{
	h_touchkey_i2c_dev = h_i2c_dev;
}

//한 바이트 읽는 함수
BYTE TouchKeyReadByte(BYTE bAdrs)
{
	P_TKEY_EN(0);		// I2C Enable, Active Low

	gbTouchKey_i2cbuff[ 0 ] = bAdrs;
	ddl_i2c_write( h_touchkey_i2c_dev, 0, 0, gbTouchKey_i2cbuff, 1 );		// write address

	ddl_i2c_read( h_touchkey_i2c_dev, 0, 0, gbTouchKey_i2cbuff, 1 );			// read data

	P_TKEY_EN(1);		// I2C Disable, Active Low

	return	gbTouchKey_i2cbuff[0];
}


//복수의 data를 읽는 함수
BYTE TouchKeyReadByteN( BYTE bAdrs, BYTE *buff, BYTE length )
{
	P_TKEY_EN(0);		// I2C Enable, Active Low

	gbTouchKey_i2cbuff[ 0 ] = bAdrs;
	ddl_i2c_write( h_touchkey_i2c_dev, 0, 0, gbTouchKey_i2cbuff, 1 );		// write address

	ddl_i2c_read( h_touchkey_i2c_dev, 0, 0, buff, length );			// read data

	P_TKEY_EN(1);		// I2C Disable, Active Low

	return	buff[0];
}

//다수의 byte를 기록하는 함수는 별도로 제작하지 않고 직접 ddl_i2c_write()를 호출한다.
//한 byte기록 함수
void	TouchKeyWriteByte( BYTE addr, BYTE data )
{
	P_TKEY_EN(0);		// I2C Enable, Active Low

	gbTouchKey_i2cbuff[ 0 ] = addr;
	gbTouchKey_i2cbuff[ 1 ] = data;

	ddl_i2c_write( h_touchkey_i2c_dev, 0, 0, gbTouchKey_i2cbuff, 2 );

	P_TKEY_EN(1);		// I2C Disable, Active Low
}





void TouchKeyGotoSleep( void )
{
	TouchKeyWriteByte( _TSM12_ADDR_CTRL2, 0x07 );
	gfTKeySleep = 1;
}		


BYTE TouchKeyRegisterCheck(void)
{
	static	BYTE	buff[2];

	buff[ 0 ] = TouchKeyReadByte( _TSM12_ADDR_CTRL1 );
	buff[ 1 ] = TouchKeyReadByte( _TSM12_ADDR_SENSITIVITY3 );

	gfTouchErr = 1;

	if( (buff[0] == _TSM12_RTC) && (buff[1] == _TSM12_SENSITIVITY3) ) {
		gfTouchErr = 0;
		return 1;
	}
	else {
		P_TKEY_RST(1);
		P_TKEY_EN(1);
	}	

	return 0;
}


BYTE	tk_dbg;
static	BYTE TKeyRegisterInitialCheck(void)
{
	BYTE	bSum = 0;

	TouchKeyReadByte( _TSM12_ADDR_OUTPUT1 );
	TouchKeyReadByte( _TSM12_ADDR_OUTPUT2 );
	TouchKeyReadByte( _TSM12_ADDR_OUTPUT3 );

	if ( (tk_dbg=TouchKeyReadByte( _TSM12_ADDR_CTRL1 )) == _TSM12_RTC )
		bSum++;

	if ( (tk_dbg=TouchKeyReadByte( _TSM12_ADDR_REF_RST1 )) == 0x00 )
		bSum++;
	
	if ( TouchKeyReadByte( _TSM12_ADDR_CH_HOLD1 ) == 0x00 )
		bSum++;

	if ( TouchKeyReadByte( _TSM12_ADDR_CH_HOLD2 ) == 0x00)
		bSum++;
	
	if ( TouchKeyReadByte( _TSM12_ADDR_SENSITIVITY1 ) == _TSM12_SENSITIVITY1 )
		bSum++;

	if ( TouchKeyReadByte( _TSM12_ADDR_SENSITIVITY2 ) == _TSM12_SENSITIVITY2 )
		bSum++;

	if ( TouchKeyReadByte( _TSM12_ADDR_SENSITIVITY3 ) == _TSM12_SENSITIVITY3 )
		bSum++;
	
	if ( TouchKeyReadByte( _TSM12_ADDR_SENSITIVITY4 ) == _TSM12_SENSITIVITY4 )
		bSum++;

	if ( TouchKeyReadByte( _TSM12_ADDR_SENSITIVITY5 ) == _TSM12_SENSITIVITY5 )
		bSum++;

	if ( TouchKeyReadByte( _TSM12_ADDR_SENSITIVITY6 ) == _TSM12_SENSITIVITY6 )
		bSum++;


	gfTouchErr = 1;

	if ( bSum == 10 ) {
		gfTouchErr = 0;
		return 1;
	}

	return 0;
	
}





uint32_t	TouchKeyInit( void )
{
	P_TKEY_EN(1);		// I2C Disable, Active Low
	P_TKEY_SDA(1);
	P_TKEY_SCL(1);
	// 1. H/W Reset
	// 2. Ch1 On -> Ch1 Enable operation is default in Ch_hold1
	// 3. Micom On
	P_TKEY_RST(1);
	Delay( SYS_TIMER_2MS );

	P_TKEY_RST(0);

	Delay( SYS_TIMER_100MS );

	// 7. CTRL1 setting
	TouchKeyWriteByte( _TSM12_ADDR_CTRL1, _TSM12_RTC );

	// 4. CTRL2 setting : 0x0F		-> Enable Software Reset, Enable Sleep Mode
	TouchKeyWriteByte( _TSM12_ADDR_CTRL2, 0x0F );

	// 5. CTRL2 setting : 0x07 -> Disable Sleep Mode : 0x03
	TouchKeyWriteByte( _TSM12_ADDR_CTRL2, 0x03 );

	// 6. Sensitivity setting
	P_TKEY_EN(0);		// I2C Enable, Active Low

	gbTouchKey_i2cbuff[ 0 ] = _TSM12_ADDR_SENSITIVITY1;
	gbTouchKey_i2cbuff[ 1 ] = _TSM12_SENSITIVITY1;
	gbTouchKey_i2cbuff[ 2 ] = _TSM12_SENSITIVITY2;
	gbTouchKey_i2cbuff[ 3 ] = _TSM12_SENSITIVITY3;
	gbTouchKey_i2cbuff[ 4 ] = _TSM12_SENSITIVITY4;
	gbTouchKey_i2cbuff[ 5 ] = _TSM12_SENSITIVITY5;
	gbTouchKey_i2cbuff[ 6 ] = _TSM12_SENSITIVITY6;
	ddl_i2c_write( h_touchkey_i2c_dev, 0, 0, gbTouchKey_i2cbuff, 7 );

	// 9. Reset Off
	gbTouchKey_i2cbuff[ 0 ] = _TSM12_ADDR_REF_RST1;
	gbTouchKey_i2cbuff[ 1 ] = 0x00;
	gbTouchKey_i2cbuff[ 2 ] = 0x00;
	ddl_i2c_write( h_touchkey_i2c_dev, 0, 0, gbTouchKey_i2cbuff, 3 );

	// 8. Channel_Hold Off
	gbTouchKey_i2cbuff[ 0 ] = _TSM12_ADDR_CH_HOLD1;
	gbTouchKey_i2cbuff[ 1 ] = 0x00;
	gbTouchKey_i2cbuff[ 2 ] = 0x00;
	ddl_i2c_write( h_touchkey_i2c_dev, 0, 0, gbTouchKey_i2cbuff, 3 );

	P_TKEY_EN(1);		// I2C Disable, Active Low

	return 0;
}

void		TouchKeyInitProc( BYTE count )
{
	while( count ) {
		TouchKeyInit();
		if ( TKeyRegisterInitialCheck() )
			break;
		else {
			count --;
			Delay( SYS_TIMER_10MS );
		}
	}
}


void		TouchKeyHold( void )
{
	P_TKEY_EN(0);		// I2C Enable, Active Low
	gbTouchKey_i2cbuff[ 0 ] = _TSM12_ADDR_CH_HOLD1;
	gbTouchKey_i2cbuff[ 1 ] = 0xFF;
	gbTouchKey_i2cbuff[ 2 ] = 0x0F;
	ddl_i2c_write( h_touchkey_i2c_dev, 0, 0, gbTouchKey_i2cbuff, 3 );
	P_TKEY_EN(1);		// I2C Disable, Active Low
}


void		TouchKeyHoldClear( void )
{
	P_TKEY_EN(0);		// I2C Enable, Active Low
	gbTouchKey_i2cbuff[ 0 ] = _TSM12_ADDR_CH_HOLD1;
	gbTouchKey_i2cbuff[ 1 ] = 0x00;
	gbTouchKey_i2cbuff[ 2 ] = 0x00;
	ddl_i2c_write( h_touchkey_i2c_dev, 0, 0, gbTouchKey_i2cbuff, 3 );
	P_TKEY_EN(1);		// I2C Disable, Active Low
}



static	WORD TouchKeyCheck(BYTE* bpData, BYTE bInit)
{
	BYTE bCnt, bCnt1;
	BYTE bTmp;
	WORD wKeyData = 0;
	WORD wKeyShift = 0x0001;
	BYTE bInitShift;
	BYTE bNum = 0;

//Key touch check from ch1 to ch12

	bCnt = 3;
	while(bCnt--)
	{
		bTmp = *bpData++;
		bInitShift = bInit;

		bCnt1 = 4;
		while(bCnt1--)
		{
			if((bTmp & bInitShift) == bInitShift)
			{
				wKeyData |= wKeyShift;
				bNum++;
			}
			wKeyShift = wKeyShift << 1;
			bInitShift = bInitShift << 2; 
		}
	}	

// Input 3 keys or more keys for multi touch process
	if(bNum >= 3)
		wKeyData |= 0x8000;

	return wKeyData;
}


#ifdef	DEBUG_TOUCH
void DebugTouchKeyDataSave(BYTE *TouchKeyData);
#endif


WORD	TouchKeyInputCheck( void )
{
	WORD wKeyData = 0;

	gfTKeyOutIn = 1;

////////////////////////////////////////////////////////////////////////////////
#ifdef P_SNS_F_BROKEN_T
	if ( !P_SNS_F_BROKEN_T )
		TouchKeyRegisterCheck(); 		// 폴링 시 전류를 줄이기 위해 최소한의 레지스터만 확인한다.
#else 		
#ifdef P_SW_FACTORY_BROKEN_T
	if ( !P_SW_FACTORY_BROKEN_T )
#endif 		
		TouchKeyRegisterCheck();		// 폴링 시 전류를 줄이기 위해 최소한의 레지스터만 확인한다.
#endif 		

	if ( gfTKeySleep ) {
		TouchKeyWriteByte( _TSM12_ADDR_CTRL2, 0x03 );
		gfTKeySleep = 0;
	}

	TouchKeyReadByteN( _TSM12_ADDR_OUTPUT1, gbTouchKey_i2cbuff, 3 );

#ifdef	DEBUG_TOUCH
DebugTouchKeyDataSave(gbTouchKey_i2cbuff);
#endif

	//키값이 있을 경우 Reset을 하기 위한 Flag 를 셋팅한다.
	if ( (gbTouchKey_i2cbuff[0] == 0x00) && (gbTouchKey_i2cbuff[1] == 0x00) && (gbTouchKey_i2cbuff[2] == 0x00) ) {
		gfTKeyOutIn = 0;
		return wKeyData;
	}

	wKeyData = TouchKeyCheck( gbTouchKey_i2cbuff, 0x03 );

	if ( wKeyData )
			return wKeyData;

	wKeyData = TouchKeyCheck( gbTouchKey_i2cbuff, 0x02 );

	return wKeyData;
}




BYTE gbSaveCnt = 0;

static	BYTE TKeyDataConvert(WORD wKeyData)
{
	BYTE bCnt;

#ifdef		DDL_CFG_KEY_ARRANGE_TYPE1

	if(wKeyData & 0x00008000)
		return TENKEY_MULTI;
	
	switch(wKeyData & 0x0000FFFF) {

		case 0x00000000:	bCnt = TENKEY_NONE; 		break;
		case 0x00000800:	bCnt = TENKEY_9;			break;	// S12
		case 0x00000400:	bCnt = TENKEY_6;			break;	// S11
		case 0x00000200:	bCnt = TENKEY_3;			break;	// S10
		case 0x00000100:	bCnt = TENKEY_SHARP;		break;	// S9
		case 0x00000080:	bCnt = TENKEY_0;			break;	// S8
		case 0x00000040:	bCnt = TENKEY_8;			break;	// S7
		case 0x00000020:	bCnt = TENKEY_2;			break;	// S6
		case 0x00000010:	bCnt = TENKEY_5;			break;	// S5
		case 0x00000008:	bCnt = TENKEY_STAR; 	break;		// S4
		case 0x00000004:	bCnt = TENKEY_1;			break;	// S3
		case 0x00000002:	bCnt = TENKEY_4;			break;	// S2
		case 0x00000001:	bCnt = TENKEY_7;			break;	// S1

//		case 0x00000060:	bCnt = FUNKEY_LOGO; 		break;	//logo+2 -> logo
		case 0x00000006:	bCnt = TENKEY_1;			break;	//S3+S2 	1+4 -> 1
		case 0x00000030:	bCnt = TENKEY_2;			break;	//S6+S5 	2+5 -> 2
		case 0x00000600:	bCnt = TENKEY_3;			break;	//S10+S11	3+6 -> 3
		case 0x00000003:	bCnt = TENKEY_4;			break;	//S2+S1 	7+4 -> 4
		case 0x00000050:	bCnt = TENKEY_5;			break;	//S5+S7 	8+5 -> 5
		case 0x00000C00:	bCnt = TENKEY_6;			break;	//S11+S12	6+9 -> 6
		case 0x00000009:	bCnt = TENKEY_7;			break;	//S1+S4 	*+7 -> 7
		case 0x000000C0:	bCnt = TENKEY_8;			break;	//S7+S8 	0+8 -> 8

		default :			bCnt = TENKEY_ERROR;
	}

#endif

#ifdef		DDL_CFG_KEY_ARRANGE_TYPE2
	
	if(wKeyData & 0x00008000)
		return TENKEY_MULTI;
	
	switch(wKeyData & 0x0000FFFF) {

		case 0x00000000:	bCnt = TENKEY_NONE; 		break;
		case 0x00000800:	bCnt = TENKEY_5;			break;	// S12
		case 0x00000400:	bCnt = TENKEY_8;			break;	// S11
		case 0x00000200:	bCnt = TENKEY_0;			break;	// S10
		case 0x00000100:	bCnt = TENKEY_SHARP;		break;	// S9
		case 0x00000080:	bCnt = TENKEY_9;			break;	// S8
		case 0x00000040:	bCnt = TENKEY_6;			break;	// S7
		case 0x00000020:	bCnt = TENKEY_3;			break;	// S6
		case 0x00000010:	bCnt = TENKEY_2;			break;	// S5
		case 0x00000008:	bCnt = TENKEY_1; 			break;	// S4
		case 0x00000004:	bCnt = TENKEY_4;			break;	// S3
		case 0x00000002:	bCnt = TENKEY_7;			break;	// S2
		case 0x00000001:	bCnt = TENKEY_STAR;			break;	// S1

		case 0x0000000C:	bCnt = TENKEY_1;			break;	//S3+S2 	1+4 -> 1
		case 0x00000810:	bCnt = TENKEY_2;			break;	//S6+S5 	2+5 -> 2
		case 0x00000060:	bCnt = TENKEY_3;			break;	//S10+S11	3+6 -> 3
		case 0x00000006:	bCnt = TENKEY_4;			break;	//S2+S1 	7+4 -> 4
		case 0x00000C00:	bCnt = TENKEY_5;			break;	//S5+S7 	8+5 -> 5
		case 0x000000C0:	bCnt = TENKEY_6;			break;	//S11+S12	6+9 -> 6
		case 0x00000003:	bCnt = TENKEY_7;			break;	//S1+S4 	*+7 -> 7
		case 0x00000600:	bCnt = TENKEY_8;			break;	//S7+S8 	0+8 -> 8
		case 0x00000180:	bCnt = TENKEY_9;			break;	//S8+S9 	9+# -> 9

		default :			bCnt = TENKEY_ERROR;
	}
	
#endif

	return bCnt;
}



void TKeyCheckRtn(void)
{
	WORD wtkey = 0;
	BYTE bKeyTmp;
	
	if(gbTKeyTimer2ms)		return;								
	gbTKeyTimer2ms = 7;							
	
	if(gfTouchErr)		return;
	
	wtkey = TouchKeyInputCheck();
	bKeyTmp = TKeyDataConvert(wtkey);

/***
	if(gbTKey_No_Time10ms)		//WGA18(ST MICOM)에서는 사용 하는듯 하나 필요가 없을듯 하여 아직 살리진 않음
	{
		// 키 입력 금지 시간일 경우는 키 처리 하지 않음
		// Key Int가 High 일 경우는 LOW로 만들기 위해 DATA를 받음.
		return;
	}
	//gbTKey_No_Time10ms = 2;
***/

	if(gbOldKeyBuffer != bKeyTmp)
	{
//		PCErrorClear();

		gbTKeyCheckCnt = 0;
		gbOldKeyBuffer = bKeyTmp;
	}

	gbTKeyCheckCnt++;

	if(gbTKeyCheckCnt >= 2)
	{						
		gbNewKeyBuffer = gbOldKeyBuffer;
		gbTKeyCheckCnt = 0;
	}
}



BYTE gbTouchTryCnt = 0;
BYTE gbTouchSleepCnt = 0;


void TouchKeyCheckBeforeSleep(void)
{
	if(!gfTKeySleep)
	{
		if(gfTKeyOutIn == 1)
		{
			TouchKeyInputCheck();
			if(gfTKeyOutIn == 0)
			{
				TouchKeyGotoSleep();
				gbNewKeyBuffer = TENKEY_NONE;
				gbOldKeyBuffer = TENKEY_NONE;
				gbTouchSleepCnt =0;
			}
			else
			{
// For what?
				if(gbTouchSleepCnt < 5)
				{
					gbTouchSleepCnt ++;
				}
				else
				{
					TouchKeyInitProc( 3 );
					gbNewKeyBuffer = TENKEY_NONE;
					gbOldKeyBuffer = TENKEY_NONE;
					gbTouchSleepCnt = 0;
					gfTKeySleep = 0;
				}
			}
		}
		else
		{
			TouchKeyGotoSleep();
			gbTouchSleepCnt = 0;
		}
	}
	else	
	{
//Touch IC initializing to get out of Touch IC malfunction caused by ESD
		if(gbTouchTryCnt)
		{
			gbTouchTryCnt --;
		}
		else
		{
			gbTouchTryCnt = 10;

			TouchKeyRegisterCheck();
				
			if(gfTouchErr)
			{
				TouchKeyInitProc( 3 );
				gbNewKeyBuffer = TENKEY_NONE;
				gbOldKeyBuffer = TENKEY_NONE;
				gbTouchSleepCnt = 0;
				gfTKeySleep = 0;
			}
			else
			{
				TouchKeyGotoSleep();
			}
		}	
	}
}


//=============================================================
//	TEST FUNCTIONS
//=============================================================

// 테스트 외에는 아래 내용은 모두 주석 처리 가능

#ifdef	DEBUG_TOUCH

BYTE gTestTouchKeyTimer10ms = 0;


void DebugTouchKeyTimeCount(void)
{	
	if(gTestTouchKeyTimer10ms)		--gTestTouchKeyTimer10ms;
}	

void DebugTouchKeyDataSave(BYTE *TouchKeyData)
{
	DebugDataBuf[4] = TouchKeyData[0];
	DebugDataBuf[5] = TouchKeyData[1];
	DebugDataBuf[6] = TouchKeyData[2];
}


void DebugTouchKeySend(void)
{
	DebugDataBuf[0] = 0x02;
	DebugDataBuf[1] = 0x01;
	DebugDataBuf[2] = 0x80;
	DebugDataBuf[3] = 3;

	DebugDataBuf[7] = 0x03;

	DebugTxDataSend(DebugDataBuf, 8);	
}


void DebugTouchKeyAutoSend(void)
{
	if(gTestTouchKeyTimer10ms)		return;
	gTestTouchKeyTimer10ms = 20;

	DebugTouchKeySend();
}

#endif

