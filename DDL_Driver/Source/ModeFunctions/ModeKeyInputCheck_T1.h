//------------------------------------------------------------------------------
/** 	@file		ModeKeyInputCheck_T1.h
	@brief	Key Input Wait Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODEKEYINPUTCHECK_T1_INCLUDED
#define __MODEKEYINPUTCHECK_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeKeyInputCheck(void);


void KeyTenKeyWakeUpProcess(void);
void ModeGotoRegisterCheck(void);
void ModeRegisterCheck(void);


#endif


