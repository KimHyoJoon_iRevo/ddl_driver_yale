#define		_MODE_FINGER_VERITY_C_

#include "Main.h"

extern BYTE gbHFPMVerifyArray[32];

void MotorOpenCompleteByFinger(void)
{
	BYTE bTmpArray[10];
				
	CopySlotNumberForPack((WORD)ConvertFingerprintSlotNumber(gbFID));

	CopySlotNumberForPack((WORD)bTmp);
	bTmpArray[0] = CREDENTIALTYPE_FINGERPRINT;
	bTmpArray[1] = GetSlotNumberForPack();
	memset(&bTmpArray[2], 0x00, 8);
	PackTx_MakePacket(EV_USER_CREDENTIAL_UNLOCK, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 10);

	gbModePrcsStep = 7;
}

void OutForcedLockSetByFinger(void)
{
	BYTE bTmpArray[11];

	DDLStatusFlagSet(DDL_STS_OUTLOCK);

	BioModuleOff();
	bTmpArray[0] = 0xFF;		// No Code Data
	bTmpArray[1] = CREDENTIALTYPE_FINGERPRINT;
	bTmpArray[2] = GetSlotNumberForPack();
	memset(&bTmpArray[3], 0xFF, 8);
	PackTx_MakePacket(EV_ARM_REQEST, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 11);
	SetModeProcessTime(100);
	gbModePrcsStep = 12;
}

//------------------------------------------------------------------------------
/** 	@brief	FingerPrint Register Mode Start
	@param	None
	@return 	None
	@remark 지문 인증 모드 시작
*/
//------------------------------------------------------------------------------
void	ModeGotoFingerVerify( void )
{
	gbMainMode = MODE_FINGER_VERIFY;
	gbModePrcsStep = 0;
}

void ModeFingerVerify(void)
{
	BYTE bTmp;
	
	switch(gbModePrcsStep)
	{
		case 0:
			bTmp = MotorSensorCheck();
			switch(bTmp)
			{
				case SENSOR_OPENLOCK_STATE:
				case SENSOR_CLOSELOCK_STATE:
				case SENSOR_CENTERLOCK_STATE:
				case SENSOR_LOCK_STATE:
				case SENSOR_CLOSECENLOCK_STATE:
					if(AlarmStatusCheck() == STATUS_SUCCESS) // 경보가 있으면 경보 일단 해제 위해 인증 모드로 
					{
						gbModePrcsStep++;	
						SetModeTimeOut(100);
						//LedModeRefresh();
					}
					else // 경보가 없으면 내부강제잠금 확인 
					{
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
						if(bTmp == SENSOR_CLOSELOCK_STATE)
						{
							FeedbackLockInOpenAllowState();
							gbModePrcsStep++;	
							SetModeTimeOut(100);
							//LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
							LedSetting(gcbLedOk, LED_KEEP_DISPLAY);
#endif
						}
						else if(bTmp == SENSOR_OPENLOCK_STATE)
						{
							bTmp = AlarmStatusCheck();
							if(bTmp == STATUS_FAIL) {			//경보발생 상황이 아니면 문 닫힘 동작

								// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
								if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
								{
									FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
									ModeClear();
									break;							
								}
#ifdef	MOTOR_CLOSED_IN_OPEN_STATUS
								if(gfDoorSwOpenState)	//문이 열려있는 경우
								{		
									FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
									ModeClear();
									break;			
								}
#endif								

								FeedbackMotorClose();
								StartMotorClose();

								gbModePrcsStep = 30;		// wait for motor operatation finished
								break;
							}
						}
						else
						{
							PackAlarmReportInnerForcedLockOpenCloseFailSend();
							FeedbackLockIn();
							ModeClear();
						}
#else 		
						PackAlarmReportInnerForcedLockOpenCloseFailSend();
						gbLedDisplayFPMOperation = 0x01;
						FeedbackLockIn();
						//ModeClear();
						gbModePrcsStep = 32;
#endif 						
					}
					break;

				case SENSOR_OPEN_STATE:
				case SENSOR_OPENCEN_STATE:
					bTmp = AlarmStatusCheck();
					if(bTmp == STATUS_FAIL) {			//경보발생 상황이 아니면 문 닫힘 동작

						// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
						if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
						{
							FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
							ModeClear();
							break;							
						}
#ifdef	MOTOR_CLOSED_IN_OPEN_STATUS
						if(gfDoorSwOpenState)	//문이 열려있는 경우
						{		
							FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
							ModeClear();
							break;			
						}
#endif								

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
						BatteryCheck();
						gbLedDisplayFPMOperation = 0x01;
						LedSetting(gcbLedFPMOperation,LED_KEEP_DISPLAY);
#else
						FeedbackMotorClose();
#endif
						StartMotorClose();

						gbModePrcsStep = 30;		// wait for motor operatation finished
						break;
					}
					//경보발생 상황이면 지문 인증 절차 진행. "break" 구문 없음에 유의

				default:	
					gbModePrcsStep++;	
					LedModeRefresh();
					break;					
			}
			break;
			
		case 1:
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )
				break;
#endif 
			gbModePrcsStep++;
			SetModeTimeOut(10);
			break;

		case 2:	
			if(GetModeTimeOut() == 0){
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				BioOffModeClear();
				break;
			}

			gbFID = HFPMVerifyCommandParser(gbHFPMVerifyArray);

			if((0x00 < gbFID) &&  (gbFID < 0x15)) /* slotnumber 1 ~ 20 까지 */
			{
				/* 인증 완료 */
				memset(gbHFPMVerifyArray,0xFF,32);
				
				if(gbJigTimer1s){
					memset(gbJigInputDataBuf, 0xFF, 9);
					gbJigInputDataBuf[0] = 0x00;
					gbJigInputDataBuf[1] = 0x01;
					gbJigInputDataBuf[2] = 0x01;				
				}

#ifdef	BLE_N_SUPPORT
				/* all lock out check PIN 을 제외한 나머지는 slotnumber 를 0xFF 로 */ 
				if(AllLockOutStatusCheck(0xFF))
				{
					FeedbackAllCodeLockOut();
					ModeClear();
					break;
				}
#endif

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
				{
					if(ScheduleEnableCheck_Ble30(CREDENTIALTYPE_FINGERPRINT, (ConvertFingerprintSlotNumber(gbFID)-1)) != 0)
					{
						gbModePrcsStep = 251;
						break;
					}
				}
#endif	// DDL_CFG_BLE_30_ENABLE
				bTmp = AlarmStatusCheck();
				if(bTmp == STATUS_SUCCESS)
				{
					AlarmGotoAlarmClear();
					ModeClear();
				}
				else
				{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
					gbLedDisplayFPMOperation = 0x01;
					LedSetting(gcbLedFPMOperation,LED_KEEP_DISPLAY);
#else
					FeedbackMotorOpen();
#endif
					PCErrorClearSet();
					StartMotorOpen();		//모터 open
					gbModePrcsStep = 6;	//모터 OPEN 상태 확인
				}
				DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
				TamperCountClear();
			}
			else 
			{
				gbFID = 0xFF;
				TamperCountIncrease();
				gbLedDisplayFPMOperation = 0x01;
				FeedbackError(VOICE_MIDI_ERROR,VOL_CHECK);
				if(gbJigTimer1s){
					memset(gbJigInputDataBuf, 0xFF, 9);
					gbJigInputDataBuf[0] = 0x00;
					gbJigInputDataBuf[1] = 0x01;
					gbJigInputDataBuf[2] = 0x01;				
				}												
				//BioOffModeClear();
				gbModePrcsStep = 32;
			}
			break;

		case 6:
			MotorOpenCompleteCheck(MotorOpenCompleteByFinger,31);
			break;
			
		case 7:
			if(GetLedMode() || GetBuzPrcsStep()) break;
			gbModePrcsStep++;
			SetModeTimeOut(2);
			HFPMTxEventFingerPresentStatus();
			break;
			
		case 8:		
#ifdef	P_SNS_EDGE_T			
			if(gfXORDoorSw) {
				LedSetting(gcbLedOff, LED_DISPLAY_OFF);
				BioOffModeClear();
				break;
			}
#endif
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == IREVO_HFPM_SUB_EVENT_FINGER_PRESENT_STATUS && 
					gbFingerPrint_rxd_buf[5] == 0xFF)
			{
				gbModePrcsStep++;
			}

			}
			else if(GetModeTimeOut()  == 0)
			{
				LedSetting(gcbLedOff, LED_DISPLAY_OFF);
				BioOffModeClear();
			}
			break;
		
		case 9:
			if(GetBuzPrcsStep() || GetVoicePrcsStep())		break;			//부저음/음성 출력중 외부 강제 잠금 시간 증가 금지
			gbFingerLongTimeWait100ms = 24; 				//외부 강제 잠금 모드 진입 2초 대기
			gbCoverAccessDelaytime100ms = 2;
			HFPMTxEventFingerPresentStatus(); 
			gbModePrcsStep++;
			break;

		case 10:
			if(gbFingerLongTimeWait100ms == 17)
			{
				gbFingerLongTimeWait100ms--;					//100ms동안 같은 값을 가지므로 반복하여 들어오는
											//	것을 방지하기 위해 인위적으로 줄임
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);//(음성) 버튼음2											
				LedSetting(gcbLedOutLock1, LED_KEEP_DISPLAY);
			}
			if(gbFingerLongTimeWait100ms == 11)
			{
				gbFingerLongTimeWait100ms--;					//100ms동안 같은 값을 가지므로 반복하여 들어오는
											//	것을 방지하기 위해 인위적으로 줄임
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);//(음성) 버튼음											
				LedSetting(gcbLedOutLock2, LED_KEEP_DISPLAY);
			}
			if(gbFingerLongTimeWait100ms == 5)
			{
				gbFingerLongTimeWait100ms--;					//100ms동안 같은 값을 가지므로 반복하여 들어오는
											//	것을 방지하기 위해 인위적으로 줄임
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);//(음성) 버튼음											
				LedSetting(gcbLedOutLock3, LED_KEEP_DISPLAY);
			}

			if(gbFingerLongTimeWait100ms == 0){ 				//외부강제잠금
				LedSetting(gcbLedOutLock4, LED_KEEP_DISPLAY);
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
				gbLedDisplayFPMOperation = 0x01;
				LedSetting(gcbLedFPMOperation,LED_KEEP_DISPLAY);
#else
				FeedbackMotorClose();
#endif
				StartMotorClose();
				gbModePrcsStep++;
				break; 
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == IREVO_HFPM_SUB_EVENT_FINGER_PRESENT_STATUS && 
					gbFingerPrint_rxd_buf[5] == 0x00)
			{
				LedSetting(gcbLedOff, 0);
				BioOffModeClear();
					break;
			}

			}

			if(gbCoverAccessDelaytime100ms == 0)
				{
				gbCoverAccessDelaytime100ms = 2;
					HFPMTxEventFingerPresentStatus(); 
				}
			break;
			
		case 11:
			//외부강제잠금
			MotorCloseCompleteCheck(OutForcedLockSetByFinger,31);			
			break;
		
		case 12: 	
			// 외부강제잠금에 의한 잠김 신호가 없어 아래 신호 추가 전송
			if(GetModeProcessTime()== 0)
			{
				PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);
				ModeClear();
			}
			break;

		case 30:
#if 0
			if(GetMotorPrcsStep())break; //180917 moonsw 백턴기다리고 피드백은 너무 늦어서 해당 조건 삭제함
#endif
			bTmp = GetFinalMotorStatus();

			switch( bTmp ) 
			{
				case	FINAL_MOTOR_STATE_OPEN_ING:
				case	FINAL_MOTOR_STATE_CLOSE_ING:
					break;

				case	FINAL_MOTOR_STATE_OPEN_ERROR:
					gbModePrcsStep++;

					FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);

					// Pack Module로 Event 내용 전송 
					PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x01);
					break;
			
				case	FINAL_MOTOR_STATE_CLOSE_ERROR:
					gbModePrcsStep++;

					FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
			
					// Pack Module로 Event 내용 전송 
					PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x00);
					break;

				case	FINAL_MOTOR_STATE_OPEN:
					// 지문 인증 과정에서 해당 부분으로 Pack으로 Event 전송할 경우 없음.
					BioOffModeClear();
					break;

				case	FINAL_MOTOR_STATE_CLOSE:
					// Manual Lock Event 전송
					PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					FeedbackMotorClose();
#endif 
					gbModePrcsStep = 32;
					//BioOffModeClear();
					break;
			}
			break;

		case 31:
			if(GetLedMode())		break;
			
			if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}
			else if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}
			BioOffModeClear();
			break;

		case 32:
			if(GetLedMode())		break;

			BioOffModeClear();
			break;


		case 200:
			FeedbackError(AVML_MALFUNCTION_FPM_MSG, VOL_CHECK);
			
			if(gbJigTimer1s){
				memset(gbJigInputDataBuf, 0xFF, 9);
				gbJigInputDataBuf[0] = 0x00;
				gbJigInputDataBuf[1] = 0x01;
				gbJigInputDataBuf[2] = 0x00;				
			}
			break;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
		case 251:
			TimeDataClear();

			PackTx_MakePacket(F0_GET_TIME_AND_DATE, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);

			// Time Data가 1초 내에 회신되지 않으면 에러 처리
			gModeTimeOutTimer100ms = 10;

			gbModePrcsStep++;
			break;

		case 252:
			if(IsGetTimeDataCompleted() == true)
			{
				gbModePrcsStep++;
			}
			else if(gModeTimeOutTimer100ms == 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				BioOffModeClear();
			}
			break;			

		case 253:
			if(ScheduleVerify_Ble30(CREDENTIALTYPE_FINGERPRINT, gbTimeBuff_Ble30, (gbFID+1)) == 1)
			{
				bTmp = AlarmStatusCheck();
				if(bTmp == STATUS_SUCCESS)
				{
					AlarmGotoAlarmClear();
					BioOffModeClear();
				}
				else
				{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
					gbLedDisplayFPMOperation = 0x01;
					LedSetting(gcbLedFPMOperation,LED_KEEP_DISPLAY);
#else
					FeedbackMotorOpen();
#endif
					PCErrorClearSet();
					StartMotorOpen();		//모터 open
					gbModePrcsStep = 6; //모터 OPEN 상태 확인
				}
				DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
				TamperCountClear();
			}
			else
			{
				TamperCountIncrease();

				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				BioOffModeClear();
			}
			break;
#endif	// DDL_CFG_BLE_30_ENABLE

		default :
			BioOffModeClear();
			break;
	}
}



BYTE ConvertFingerprintSlotNumber(BYTE ConvertingData)
{
	return (ConvertingData);
}




