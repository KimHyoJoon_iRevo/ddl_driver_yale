//------------------------------------------------------------------------------
/** 	@file		ModeDS1972TouchKeyRegister_T1.c
	@brief	DS1972 TouchKey Register Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODEGMTOUCHKEYREGISTER_T1_INCLUDED
#define __MODEGMTOUCHKEYREGISTER_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"

extern BYTE gInputKeyNumberForRegister;

void ModeGotoDS1972TouchKeyRegister(void);
void ModeDS1972TouchKeyRegister(void);
void AllDS1972TouchKeyDelete(void);

#ifdef	BLE_N_SUPPORT
void ModeGotoDS1972TouchKeyRegisterByBleN(void);
void ModeDS1972TouchKeyRegisterByBleN(void);
#endif 

#endif


