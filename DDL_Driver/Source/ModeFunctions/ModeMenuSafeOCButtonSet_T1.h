//------------------------------------------------------------------------------
/** 	@file		ModeMenuSafeOCButtonSet_T1.h
	@brief	Programmable Safe Open/Close Button Setting
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUSAFEOCBUTTONSET_T1_INCLUDED
#define __MODEMENUSAFEOCBUTTONSET_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"

extern BYTE gfSafeOCEn;

void SafeOCButtonLoad(void);
BYTE IsSafeOCButtonClear(void);


void ModeGotoSafeOCButtonSetting(void);
void ModeSafeOCButtonSetting(void);



#endif


