//------------------------------------------------------------------------------
/** 	@file		ModeMenuSafeOCButtonSet_T1.c
	@version 0.1.00
	@date	2016.07.07
	@brief	Programmable Safe Open/Close Button Setting
	@remark	이중 열림/닫힙 버튼 설정 모드
	@see	ModeMenuMainSelect_T1.c
	@see	ModeMenuLockSetting_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.07.07		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"


#ifdef	DDL_CFG_TOUCH_OC

BYTE gfSafeOCEn = 0;


void SafeOCButtonSet(void)
{
	gfSafeOCEn = 0xBB;
	RomWrite(&gfSafeOCEn, OCBUTTON_MODE, 1);
}

void SafeOCButtonClear(void)
{
	gfSafeOCEn = 0xAA;
	RomWrite(&gfSafeOCEn, OCBUTTON_MODE, 1);
}


void SafeOCButtonLoad(void)
{
	RomRead(&gfSafeOCEn, OCBUTTON_MODE, 1);
}


// Safe Open/Close Button 설정 기능이 있는 제품은 해제 상태가 예외의 경우임.
BYTE IsSafeOCButtonClear(void)
{
	if(gfSafeOCEn == 0xAA)
		return (STATUS_SUCCESS);

	return (STATUS_FAIL);
}




void ModeGotoSafeOCButtonSetting(void)
{
	gbMainMode = MODE_MENU_SAFE_OC_SET;
	gbModePrcsStep = 0;
}


void SafeOCButtonSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		//Safe Open/Close Button Enable
		case 1: 	
			SafeOCButtonSet();
			
//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);

			gbModePrcsStep++;
			break;
	
		//Safe Open/Close Button Disable
		case 3: 		
			SafeOCButtonClear();

//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);

			gbModePrcsStep++;
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 이전 모드로 이동
			gbModePrcsStep--;
			break;
	}			
}



void MenuSafeOCButtonSetSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		//Safe Open/Close Button Enable
		case TENKEY_1:			
#if	defined(AVML_MSG_T1)
//			MenuSelectKeyTempSaveNTimeoutReset(154, gbInputKeyValue);		
			MenuSelectKeyTempSaveNTimeoutReset(AVML_TURNON_CONT_SHARP, gbInputKeyValue);		
#elif	defined(AVML_MSG_T2)
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DOUBLE_OC_ON_SHARP, gbInputKeyValue);		
#else
			MenuSelectKeyTempSaveNTimeoutReset(AVML_TURNON_CONT_SHARP, gbInputKeyValue);		
#endif
			break;

		//Safe Open/Close Button Disable
		case TENKEY_3:			
#if	defined(AVML_MSG_T1)
//			MenuSelectKeyTempSaveNTimeoutReset(155, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_TURNOFF_CONT_SHARP, gbInputKeyValue);
#elif	defined(AVML_MSG_T2)
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DOUBLE_OC_OFF_SHARP, gbInputKeyValue);
#else
			MenuSelectKeyTempSaveNTimeoutReset(AVML_TURNOFF_CONT_SHARP, gbInputKeyValue);
#endif
			break;

		case TENKEY_SHARP:		 //# 버튼음 출력
#ifdef	P_VOICE_RST	
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
				FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuLockSetting();
			break;			

		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;

		default:
			TimeExpiredCheck();
			break;
	}	
}




//------------------------------------------------------------------------------
/** 	@brief	Auto Lock Setting Mode
	@param	None
	@return 	None
	@remark 자동잠김/수동잠김 설정 모드 시작
*/
//------------------------------------------------------------------------------
void ModeSafeOCButtonSetting(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
#if	defined(AVML_MSG_T1)
//			Feedback13MenuOn(153, VOL_CHECK);
			Feedback13MenuOn(AVML_TURNON1_TURNOFF3, VOL_CHECK);
#elif	defined(AVML_MSG_T2)
			Feedback13MenuOn(AVML_DOUBLE_OC_ON1_OFF3, VOL_CHECK);
#else
			Feedback13MenuOn(AVML_TURNON1_TURNOFF3, VOL_CHECK);
#endif
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = 0;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuSafeOCButtonSetSelectProcess();
			break;	
	
		case 2:
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
			{
				if(GetBuzPrcsStep() || GetVoicePrcsStep()) 	break;
			}

			SafeOCButtonSetSelected();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoMenuLockSetting);
			break;

		default:
			ModeClear();
			break;
	}

}

#endif


