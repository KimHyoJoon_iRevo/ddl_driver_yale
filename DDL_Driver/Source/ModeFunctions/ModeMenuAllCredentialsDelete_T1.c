//------------------------------------------------------------------------------
/** 	@file		ModeMenuAllCredentialsDelete_T1.c
	@version 0.1.00
	@date	2016.05.19
	@brief	All Credentials Delete Mode
	@remark	전체 카드/지문, 사용자 비밀번호, 일회용 비밀번호 삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.05.19		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"

static uint8_t bResetType = 0x00;

void ModeGotoAllCredentialsDelete(void)
{
	gbMainMode = MODE_MENU_ALLCREDENTIALS_DELETE;
	gbModePrcsStep = 0;
}

void ProcessDeleteAllCredentials(void)
{
	BYTE bTmp;

	bTmp = PincodeVerify(0);
	switch(bTmp)
	{
		case RET_MASTER_CODE_MATCH:
			OnetimeCodeDelete();
			AllUserCodeDelete();

#ifdef	DDL_CFG_RFID
			AllCardDelete();
//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);	//	completed, Press the star for setting other options or press the "R" button to finish
#ifndef	DDL_CFG_FP //지문 카드 동시에 지원 하는 경우 feedback 은 지문 쪽에서 
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);	//	completed, Press the star for setting other options or press the "R" button to finish
#endif			
			gbModePrcsStep++;
#endif		// ~DDL_CFG_RFID

#ifdef	DDL_CFG_IBUTTON
			AllGMTouchKeyDelete();
//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);	//	completed, Press the star for setting other options or press the "R" button to finish
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);	//	completed, Press the star for setting other options or press the "R" button to finish
			gbModePrcsStep++;
#endif  	// ~DDL_CFG_IBUTTON    

#ifdef	DDL_CFG_DS1972_IBUTTON
			AllDS1972TouchKeyDelete();
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);	//	completed, Press the star for setting other options or press the "R" button to finish
			gbModePrcsStep++;
#endif  	

			// All Credential Delete Event 전송

#ifdef	DDL_CFG_FP
			 //TCS4K 지문 모듈의 지문을 지우는 것은 MainMode를 변화시켜 처리한다.
			 gbModeChangeFlag4Finger = 2; 
			ModeGotoAdvancedAllFingerDelete2();
#endif
			PackTxEventCredentialDeleted(0xFF, 0xFF);

			break;

		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	
			ModeClear();
			break;				
	}
}




void ModeAllCredentialsDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(55, VOL_CHECK);				// Enter the master code to delete all credentials, then press the hash to continue.		
			FeedbackKeyPadLedOn(AVML_IN_MPIN_FOR_DELALL_SHARP, VOL_CHECK);				// Enter the master code to delete all credentials, then press the hash to continue.		

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:		 //# 버튼음 출력
					FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
					gbModePrcsStep++;
					break;		

				case TENKEY_STAR:
					ReInputOrModeMove(ModeGotoMenuMainSelect);
					break;
					
				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessDeleteAllCredentials();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			break;

		default:
			ModeClear();
			break;
	}
}



void ModeGotoAllDataReset(uint8_t data)
{
	gbMainMode = MODE_ALL_DATA_RESET;
	gbModePrcsStep = 0x00;

	if(data != 0xFF) 
	{
		bResetType = data;
	}
}

void ModeAllDataReset(void)
{
	switch(gbModePrcsStep)
	{
		case 0:	
			TamperCountClear();
#ifndef	P_SNS_LOCK_T	
			InnerForcedLockClear();
#endif			
			gbDDLStateData = 0x00;							
			RomWrite(&gbDDLStateData, (WORD)DDL_STATE, 1);		

			OnetimeCodeDelete();

			gbModePrcsStep++;
			break;

		case 1:
			AllUserCodeDelete();
			gbModePrcsStep++;
			break;

		case 2:	
#ifdef	DDL_CFG_RFID
			AllCardDelete();
#endif
			gbModePrcsStep++;
			break;

		case 3:
#ifdef	DDL_CFG_FP
			gbModeChangeFlag4Finger = 3;
			ModeGotoAdvancedAllFingerDelete2();
#else 
			gbModePrcsStep++;
#endif
			break;

		case 4: //master code 와 language 처리 
			switch(bResetType)
			{
				case 0x00:				
					MasterCodeDelete();
				case 0x01:
					break;

				case 0x02:
					MasterCodeDelete();
				case 0x03:
					/* config parameter */
					VolumeSetting(VOL_HIGH);
					AutoRelockEnable();
#if defined(DDL_CFG_TOUCH_OC)
					SafeOCButtonSet();
#endif 
					LanguageSetDefault();
					break;

				default:
					// ModeClear();
					break;
			}

			bResetType = 0;
			FactoryResetDoneCheck();
			/* to Do */
			/* send alarm report */
			ModeClear();
			break;

		default:
			ModeClear();
			break;
	}
}



