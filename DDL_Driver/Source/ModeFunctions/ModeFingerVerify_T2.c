#define		_MODE_FINGER_VERITY_C_

//------------------------------------------------------------------------------
/** 	@file		ModeFingerVerfy_T2.c
	@version 0.1.00
	@date	2016.09.01
	@brief	FingerPrint Verify Mode 
	@remark	 등록된 지문 확인 모드
	@see	MainModeProcess_T2.c
	@see	FingerPFM_3000.c
	@see	PFM_3000_UART.c
	@see	ModeFiverPTOpen.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.09.01		by hyojoon
*/
//------------------------------------------------------------------------------

#include "Main.h"

void MotorOpenCompleteByFinger(void)
{
	BYTE bTmpArray[10];
				
	CopySlotNumberForPack((WORD)ConvertFingerprintSlotNumber(gbFID));
	
	bTmpArray[0] = CREDENTIALTYPE_FINGERPRINT;
	bTmpArray[1] = GetSlotNumberForPack();
	memset(&bTmpArray[2], 0x00, 8);
	PackTx_MakePacket(EV_USER_CREDENTIAL_UNLOCK, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 10);

	gbModePrcsStep = 7;
}

void OutForcedLockSetByFinger(void)
{
	BYTE bTmpArray[11];

	DDLStatusFlagSet(DDL_STS_OUTLOCK);

	BioModuleOff();
	bTmpArray[0] = 0xFF;		// No Code Data
	bTmpArray[1] = CREDENTIALTYPE_FINGERPRINT;
	bTmpArray[2] = GetSlotNumberForPack();
	memset(&bTmpArray[3], 0xFF, 8);
	PackTx_MakePacket(EV_ARM_REQEST, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 11);
	SetModeProcessTime(100);
	gbModePrcsStep = 12;
}

//------------------------------------------------------------------------------
/** 	@brief	FingerPrint Register Mode Start
	@param	None
	@return 	None
	@remark 지문 인증 모드 시작
*/
//------------------------------------------------------------------------------
void	ModeGotoFingerVerify( void )
{
	gbMainMode = MODE_FINGER_VERIFY;
	gbModePrcsStep = 0;
}

void ModeFingerVerify(void)
{
	BYTE bTmp;
	
	switch(gbModePrcsStep)
	{
		case 0:
			bTmp = MotorSensorCheck();
			switch(bTmp)
			{
				case SENSOR_OPENLOCK_STATE:
				case SENSOR_CLOSELOCK_STATE:
				case SENSOR_CENTERLOCK_STATE:
				case SENSOR_LOCK_STATE:
				case SENSOR_CLOSECENLOCK_STATE:
					if(AlarmStatusCheck() == STATUS_SUCCESS) // 경보가 있으면 경보 일단 해제 위해 인증 모드로 
					{
						gbModePrcsStep++;	
						SetModeTimeOut(10);
						BioModuleON();				
						BioRetryCnt = 0;
						gbFingerRetryCounter = 0;
					}
					else // 경보가 없으면 내부강제잠금 확인 
					{
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
						if(bTmp == SENSOR_CLOSELOCK_STATE)
						{
							FeedbackLockInOpenAllowState();
							gbModePrcsStep++;	
							SetModeTimeOut(10);
							BioModuleON();
							BioRetryCnt = 0;
							gbFingerRetryCounter = 0;
						}
#if 0
						else if(bTmp == SENSOR_OPENLOCK_STATE)
						{
							bTmp = AlarmStatusCheck();
							if(bTmp == STATUS_FAIL) {			//경보발생 상황이 아니면 문 닫힘 동작

								// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
								if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
								{
									FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
									ModeClear();
									break;							
								}
#ifdef	MOTOR_CLOSED_IN_OPEN_STATUS
								if(gfDoorSwOpenState)	//문이 열려있는 경우
								{		
									FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
									ModeClear();
									break;			
								}
#endif
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN								
								BatteryCheck();
#else
								FeedbackMotorClose(); 
#endif
								StartMotorClose();

								gbModePrcsStep = 30;		// wait for motor operatation finished
								break;
							}
						}
#endif 						
						else
						{
							PackAlarmReportInnerForcedLockOpenCloseFailSend();
							FeedbackLockIn();
							ModeClear();
						}
#else 		
						PackAlarmReportInnerForcedLockOpenCloseFailSend();
						FeedbackLockIn();
						ModeClear();
#endif 						
					}
					break;

#if 0
				case SENSOR_OPEN_STATE:
				case SENSOR_OPENCEN_STATE:
					bTmp = AlarmStatusCheck();
					if(bTmp == STATUS_FAIL) {			//경보발생 상황이 아니면 문 닫힘 동작

						// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
						if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
						{
							FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
							ModeClear();
							break;							
						}
#ifdef	MOTOR_CLOSED_IN_OPEN_STATUS
						if(gfDoorSwOpenState)	//문이 열려있는 경우
						{		
							FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
							ModeClear();
							break;			
						}
#endif								

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
						BatteryCheck();
#else
						FeedbackMotorClose();
#endif
						StartMotorClose();

						gbModePrcsStep = 30;		// wait for motor operatation finished
						break;
					}

					//경보발생 상황이면 지문 인증 절차 진행. "break" 구문 없음에 유의
#endif 
				default:	
					gbModePrcsStep++;	
					SetModeTimeOut(10);
					BioModuleON();
					LedModeRefresh();				
					BioRetryCnt = 0;
					gbFingerRetryCounter = 0;				
					break;					
			}
			break;
			
		case 1:
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )
				break;
#endif 

			if(GetModeTimeOut() == 0){
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				BioOffModeClear();
				break;
			}
			
			if(P_FP_DDL_RX_T)	//지문 모듈 Power On하고 약 100ms의 대기 시간을 가져야 지문 모듈이 완전히 Wakeup되며 Wakeup되었다는 증거로 RX Port가 High로 된다.
			{
				FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_SETSECURITY_LEVEL,SETSECURITY_LEVEL_VALUE_5,NO_FINGER_DATA);	//Enroll Command 송신 전 SETSECURITY_LEVEL을 3으로 먼저 설정 한다.
				gbModePrcsStep++;
				gbFingerRetryCounter = 0;
				SetModeTimeOut(100);
				//gbBioTimer10ms = 10;	//지문 등록 명령어 응답 대기 시간 100ms 설정 
			}
			else
			{
				FingerPrintRetry();
			}
			break;

		case 2:	
			if(GetModeTimeOut() == 0){
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				BioOffModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[4] == 0x86)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							SetModeTimeOut(100);
							//gbBioTimer10ms = BIO_WAKEUP_TM+750;
							gbModePrcsStep++;
							FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_VERIFY , FINGER_ALL_VERIFY , FINGER_ALL_VERIFY);
							gbFingerLongTimeWait100ms = 2;
							gbFingerRegisterDisplayStep = 0;
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY						
						case 0x1F:	//명령 실패							
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
							
						case 0x11:	//Slot이 비어 있지 않음
							BioModuleOff();
							AudioFeedback(AVML_ERR_NOSPACE,gcbBuzSta, VOL_CHECK);//(음성)no more space to memorize.
							break;
					}
				}		
			}	
			break;

		case 3:
			if(GetModeTimeOut() == 0){
				//FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				//BioOffModeClear();
				gbModePrcsStep = 100;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == 0x82)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							gbModePrcsStep++;
							SetModeTimeOut(100);
							//gbBioTimer10ms = 50;	//지문 인증 Status 응답 대기 시간 500ms 설정 
//							LedSetting(gcbLedOff, LED_KEEP_DISPLAY);
							break;
						
						case 0x12:	//Slot에 Template이 없음(저장된 지문이 없음)
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
							TamperCountIncrease();
							ModeClear();
							break;
							
						case 0x01:	//전송 패킷 오류
						case 0x1E:	//BUSY 상태면 Cancer명령어를 송신 하고 다시 인증을 시도함
						case 0x02:	//Check sum 오류							
						case 0x1F:	//명령 실패	
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}				
			}
			break;
			
		case 4:		
			if(GetModeTimeOut() == 0){
				//FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				//BioOffModeClear();
				gbModePrcsStep = 100;
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == 0x87)
				{
					switch(gbFingerPrint_rxd_buf[5]&0x0F)
					{
						case 0x00:	//명령 성공 
							switch(gbFingerPrint_rxd_buf[5]&0xF0)
							{
								case 0x20:	//Command 수행중
								case 0x40:	//BUSY(지문 SCAN 대기중)
									switch(gbFingerPrint_rxd_buf[6]&0xF0)
									{
										case 0x20:	//인증
											switch(gbFingerPrint_rxd_buf[6]&0x0F)
											{
												case 0x01:	//1번째 지문 입력 완료
													//AudioFeedback(VOICE_MIDI_FINGER,	gcbBuzNum, VOL_CHECK|MANNER_CHK);//(음성) 버튼음
													gbModePrcsStep++;
													SetModeTimeOut(30);
													//gbBioTimer10ms = 300;	//지문 인증 Results 응답 대기 시간 3s 설정 
													break;
												case 0x00:	//지문 입력 대기 않함
													//FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
													BioOffModeClear();
													break;
												case 0x02:	//2번째 지문 입력 완료	
												case 0x03:	//3번째 지문 입력 완료	
												case 0x08:	//Image 에러
												default:
													FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
													BioOffModeClear();
													break;
											}
											break;
										case 0x00:	//명령 없음
										case 0x10:	//등록
										case 0x30:	//삭제
										case 0x40:	//지문 LIST 보고
										case 0x60:	//보안 레벨 변경 
										case 0x70:	//상태
										case 0x80:	//결과
										case 0x90:	//취소
										case 0xA0:	//Version
										default:
											gbModePrcsStep = 200;	 //Check the fingerprint module.
											break;
									}
									
									 break;
								case 0x80:	//Idle 상태
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}							
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류							
						case 0x0F:	//명령 실패(원래 수신 되는 값은 0x1F이지만 하위 니블만 가공 하여 쓴다)
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
			}
			break;

		case 5:		
			if(GetModeTimeOut() == 0){
#if defined (__DDL_MODEL_B2C15MIN) || defined (__DDL_MODEL_YMH70A) // YMH70 만 time out 시 LED feedback 만 
				FeedbackError(0xFFFF, VOL_CHECK);
				/* 지문 cancel command */
				FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_CANCEL , NO_FINGER_DATA , NO_FINGER_DATA);
				
				if(gbJigTimer1s){
					memset(gbJigInputDataBuf, 0xFF, 9);
					gbJigInputDataBuf[0] = 0x00;
					gbJigInputDataBuf[1] = 0x01;
					gbJigInputDataBuf[2] = 0x01;				
				}		
				
				gbModePrcsStep = 101;
				gbBioTimer10ms = 100;			
#else 
				//FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				//BioOffModeClear();
				gbModePrcsStep = 100;
#endif 				
				break;

			}
#if 0
			if(gbBioTimer10ms == 0)		//지문 인증 Results 응답 대기 시간 3s를 초과 하면
			{
				gbModePrcsStep = 100;			
				break;
			}
#endif 
			if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
			{		
				gbFingerPrint_rxd_end=0;
				if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{
								case 0x02:	//Verify 결과 성공
									PCErrorClearSet();
									gbFID = gbFingerPrint_rxd_buf[7];

									if(gbJigTimer1s){
										memset(gbJigInputDataBuf, 0xFF, 9);
										gbJigInputDataBuf[0] = 0x00;
										gbJigInputDataBuf[1] = 0x01;
										gbJigInputDataBuf[2] = 0x01;				
									}

									if(gbFID == 0xFF){
										gbModePrcsStep=100;
										break;
									}
									
									SetModeTimeOut(100);
									BioModuleOff();
#ifdef	BLE_N_SUPPORT
									/* all lock out check PIN 을 제외한 나머지는 slotnumber 를 0xFF 로 */ 
									if(AllLockOutStatusCheck(0xFF))
									{
										FeedbackAllCodeLockOut();
										ModeClear();
										break;
									}
#endif

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
									{
										if(ScheduleEnableCheck_Ble30(CREDENTIALTYPE_FINGERPRINT, (ConvertFingerprintSlotNumber(gbFID)-1)) != 0)
										{
											gbModePrcsStep = 251;
											break;
										}
									}
#endif // DDL_CFG_BLE_30_ENABLE
									bTmp = AlarmStatusCheck();
									if(bTmp == STATUS_SUCCESS)
									{
										AlarmGotoAlarmClear();
										ModeClear();
									}
									else
									{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
										BatteryCheck(); 
#else
										FeedbackMotorOpen();
#endif
										StartMotorOpen();		//모터 open
										gbCoverAccessDelaytime100ms = 10;
										gbModePrcsStep++;	//모터 OPEN 상태 확인
									}
									DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
									TamperCountClear();
									break;

								case 0x82:	//Verify 결과 실패
									TamperCountIncrease();
									FeedbackError(VOICE_MIDI_ERROR,VOL_CHECK);
									BioOffModeClear();

									if(gbJigTimer1s){
										memset(gbJigInputDataBuf, 0xFF, 9);
										gbJigInputDataBuf[0] = 0x00;
										gbJigInputDataBuf[1] = 0x01;
										gbJigInputDataBuf[2] = 0x01;				
									}								
									break;
									
								case 0x00:	//결과 없음
								case 0x01:	//Enrollment 결과 성공
								case 0x03:	//Delete 결과 성공
								case 0x81:	//Enrollment 결과 실패								
								case 0x83:	//Delete 결과 실패
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}
							break;
							
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						case 0x1E:	//BUSY 상태(현재 명령 무시) 							
						case 0x1F:	//명령 실패
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
			}	
			break;
	
		case 6:
				MotorOpenCompleteCheck(MotorOpenCompleteByFinger,31);
			break;
			
		case 7:
			if(GetLedMode() || GetBuzPrcsStep()) break;
	//		SetModeTimeOut(20);		//2sec
			gbModePrcsStep++;		//FINGER_OUTLOCK_CKECK;	
			break;
			
		case 8:		//FINGER_OUTLOCK_CKECK:
			if(P_IBUTTON_COVER_T)	//			if( (gbNewKeyBuffer == TENKEY_SHARP))
			{
				gbModePrcsStep++;
			}
			else /*if(GetModeTimeOut()  == 0)*/
			{
				LedSetting(gcbLedOff, LED_DISPLAY_OFF);
				BioOffModeClear();
			}
			break;
		
		case 9:
			if(GetBuzPrcsStep() || GetVoicePrcsStep())		break;			//부저음/음성 출력중 외부 강제 잠금 시간 증가 금지
			gbFingerLongTimeWait100ms = 24; 				//외부 강제 잠금 모드 진입 2초 대기
			gbModePrcsStep++;
			break;

		case 10:
			if(gbFingerLongTimeWait100ms == 17)
			{
				gbFingerLongTimeWait100ms--;					//100ms동안 같은 값을 가지므로 반복하여 들어오는
											//	것을 방지하기 위해 인위적으로 줄임
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);//(음성) 버튼음2											
				LedSetting(gcbLedOutLock1, LED_KEEP_DISPLAY);
			}
			if(gbFingerLongTimeWait100ms == 11)
			{
				gbFingerLongTimeWait100ms--;					//100ms동안 같은 값을 가지므로 반복하여 들어오는
											//	것을 방지하기 위해 인위적으로 줄임
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);//(음성) 버튼음											
				LedSetting(gcbLedOutLock2, LED_KEEP_DISPLAY);
			}
			if(gbFingerLongTimeWait100ms == 5)
			{
				gbFingerLongTimeWait100ms--;					//100ms동안 같은 값을 가지므로 반복하여 들어오는
											//	것을 방지하기 위해 인위적으로 줄임
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);//(음성) 버튼음											
				LedSetting(gcbLedOutLock3, LED_KEEP_DISPLAY);
			}
			if(gbFingerLongTimeWait100ms == 0){ 				//외부강제잠금
				LedSetting(gcbLedOutLock4, LED_KEEP_DISPLAY);
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorClose();
#endif
				StartMotorClose();
//				gfOutLockReq = 1;
				gbModePrcsStep++;
				break;
			}

			if(!P_IBUTTON_COVER_T) // if(!(gbNewKeyBuffer == TENKEY_SHARP) )
			{
				LedSetting(gcbLedOff, 0);
				BioOffModeClear();
			}
			break;
			
		case 11:
			//외부강제잠금
				MotorCloseCompleteCheck(OutForcedLockSetByFinger,31);			
			break;
		
		case 12: 	
			// 외부강제잠금에 의한 잠김 신호가 없어 아래 신호 추가 전송
			if(GetModeProcessTime()== 0)
			{
				PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);
				ModeClear();
			}
			break;

#if 0	
		case 19:
			if(GetLedMode())	break;
			
			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			BioOffModeClear();
			break;
#endif 

		case 30:
			bTmp = GetFinalMotorStatus();

			switch( bTmp ) 
			{
				case	FINAL_MOTOR_STATE_OPEN_ING:
				case	FINAL_MOTOR_STATE_CLOSE_ING:
					break;

				case	FINAL_MOTOR_STATE_OPEN_ERROR:
					gbModePrcsStep++;

					FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);

					// Pack Module로 Event 내용 전송 
					PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x01);
					break;
			
				case	FINAL_MOTOR_STATE_CLOSE_ERROR:
					gbModePrcsStep++;

					FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
			
					// Pack Module로 Event 내용 전송 
					PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x00);
					break;

				case	FINAL_MOTOR_STATE_OPEN:
					// 지문 인증 과정에서 해당 부분으로 Pack으로 Event 전송할 경우 없음.
					BioOffModeClear();
					break;


				case	FINAL_MOTOR_STATE_CLOSE:
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					FeedbackMotorClose();
#endif
					// Manual Lock Event 전송
					PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);
					BioOffModeClear();
					break;
			}
			break;

		case 31:
			if(GetLedMode())		break;
			
			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			else if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}
			BioOffModeClear();
			break;

		case 100:					// 지문 fail 부분///
			FeedbackError(AVML_TIMEOUT, VOL_CHECK);
				//TamperCountIncrease();
			//FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);

				/* 지문 cancel command */
				FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_CANCEL , NO_FINGER_DATA , NO_FINGER_DATA);

			if(gbJigTimer1s){
				memset(gbJigInputDataBuf, 0xFF, 9);
				gbJigInputDataBuf[0] = 0x00;
				gbJigInputDataBuf[1] = 0x01;
				gbJigInputDataBuf[2] = 0x01;				
			}		
			
			gbModePrcsStep++;
			gbBioTimer10ms = 100;				
			break;
	
		case 101: // command 취소 ack 받고 off 
		
			if(gbBioTimer10ms == 0) 	//지문 인증 Results 응답 대기 시간 3s를 초과 하면
			{
				BioOffModeClear(); // 여기 어쩔수 없이 modeclear 
				break;
			}

			if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
			{		
				gbFingerPrint_rxd_end=0;
				if(gbFingerPrint_rxd_buf[4] == 0x89)
				{
					switch(gbFingerPrint_rxd_buf[5]) // 뭐가 오든 간에... 
					{
						case 0x00: // OK RC_STATUS_OK
						case 0x01: // RC_PACKET_ERR
						case 0x02: // RC_CHECKSUM_ERR
						case 0x1F: // RC_FUNCTION_FAILED
						default :
							BioOffModeClear();
						break;
					}
				}
			}
			break;

		case 200:
			FeedbackError(AVML_MALFUNCTION_FPM_MSG, VOL_CHECK);//	The Finger print module is not working properly.
			//BioOffModeClear();

			/* 지문 cancel command */
			FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_CANCEL , NO_FINGER_DATA , NO_FINGER_DATA);
			
			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}	

			if(gbJigTimer1s){
				memset(gbJigInputDataBuf, 0xFF, 9);
				gbJigInputDataBuf[0] = 0x00;
				gbJigInputDataBuf[1] = 0x01;
					gbJigInputDataBuf[2] = 0x00;				
			}

			gbModePrcsStep = 101;
			gbBioTimer10ms = 100;
			break;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
		case 251:
			TimeDataClear();

			PackTx_MakePacket(F0_GET_TIME_AND_DATE, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);

			// Time Data가 1초 내에 회신되지 않으면 에러 처리
			gModeTimeOutTimer100ms = 10;

			gbModePrcsStep++;
			break;

		case 252:
			if(IsGetTimeDataCompleted() == true)
			{
				gbModePrcsStep++;
			}
			else if(gModeTimeOutTimer100ms == 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				BioOffModeClear();
			}
			break;			

		case 253:
			if(ScheduleVerify_Ble30(CREDENTIALTYPE_FINGERPRINT, gbTimeBuff_Ble30, gbFID) == 1)
			{
				bTmp = AlarmStatusCheck();
				if(bTmp == STATUS_SUCCESS)
				{
					AlarmGotoAlarmClear();
					BioOffModeClear();
				}
				else
				{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck(); 
#else
					FeedbackMotorOpen();
#endif
					StartMotorOpen();		//모터 open
					gbCoverAccessDelaytime100ms = 10;
					gbModePrcsStep = 6;	//모터 OPEN 상태 확인
				}
				DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
				TamperCountClear();
			}
			else
			{
				TamperCountIncrease();

				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				BioOffModeClear();
			}
			break;
#endif	// DDL_CFG_BLE_30_ENABLE

		default :
			BioOffModeClear();
			break;
	}
}

BYTE ConvertFingerprintSlotNumber(BYTE ConvertingData)
{
	return (ConvertingData);
}

