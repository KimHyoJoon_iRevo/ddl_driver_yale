//------------------------------------------------------------------------------
/** 	@file		ModeMenuRemoconRegDel_T1.h
	@brief	All Remocons Register/Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUREMOCONREGDEL_T1_INCLUDED
#define __MODEMENUREMOCONREGDEL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoRemoconRegDelCheck(void);
void RemoconSlotNumberSet(BYTE SlotNumber);
BYTE GetRemoconSlotNumber(void);
void ModeRemoconRegDelCheck(void);
void ModeRemoconRegister(void);
void ModeRemoconDelete(void);



#ifdef	BLE_N_SUPPORT
void ModeGotoRemoconRegisterByBleN(void);
void ModeGotoRemoconDeleteByBleN(void);
void ModeRemoconRegisterByBleN(void);
void ModeRemoconDeleteByBleN(void);
#endif


#ifdef DDL_CFG_ADD_REMOCON_LINK_FUNCTION
void ModeGotoLockOpenConnectionRegister(void);
void ModeLockOpenConnectionRegister(void);
#endif


#endif


