//------------------------------------------------------------------------------
/** 	@file		ModeCardRegister_T1.c
	@brief	Card Register Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODECARDREGISTER_T1_INCLUDED
#define __MODECARDREGISTER_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"



extern BYTE gbInputCardNumberForRegister;
extern BYTE gCardProcessLoopCnt;				



void CardInputCheckForRegister(void);


void ModeGotoCardRegister(void);
void ModeCardRegister(void);

void AllCardDelete(void);


#ifdef	BLE_N_SUPPORT
void ModeGotoCardRegisterByBleN(void);
void ModeCardRegisterByBleN(void);
#endif

#endif


