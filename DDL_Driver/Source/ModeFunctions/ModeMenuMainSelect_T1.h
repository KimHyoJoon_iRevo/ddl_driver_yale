//------------------------------------------------------------------------------
/** 	@file		ModeMenuMode_T1.c
	@brief	Menu Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUMAINSELECT_T1_INCLUDED
#define __MODEMENUMAINSELECT_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


#include "ModeMenuLockSetting_T1.h"

#ifdef	LOCKSET_HANDING_LOCK
#include"ModeMenuHandingLock_T1.h"		
#endif

#ifdef	LOCKSET_AUTO_RELOCK
#include "ModeMenuAutoLockSet_T1.h"
#endif

#ifdef	LOCKSET_VOLUME_SET
#include "ModeMenuVolumeSet_T1.h"
#endif

#ifdef	DDL_CFG_TOUCH_OC
#include "ModeMenuSafeOCButtonSet_T1.h"
#endif

#ifdef	LOCKSET_LANGUAGE_SET
#include "ModeMenuLanguageSet_T1.h"
#endif 

#include "ModeMenuVisitorCodeRegDel_T1.h"
#include "ModeMenuOnetimeCodeRegDel_T1.h"

#ifdef	DDL_CFG_RFID
#include "ModeMenuAllCardDelete_T1.h"
#include "ModeMenuOneCardRegDel_T1.h"
#endif

#ifdef	DDL_CFG_IBUTTON
#include "ModeMenuAllGMTouchKeyDelete_T1.h"
#include "ModeMenuOneGMTouchKeyRegDel_T1.h"
#endif

#ifdef	DDL_CFG_DS1972_IBUTTON
#include "ModeMenuAllDS1972TouchKeyDelete_T1.h"
#include "ModeMenuOneDS1972TouchKeyRegDel_T1.h"
#endif

#include "ModeMenuModeChange_T1.h"

#include "ModeMenuMasterCodeRegister_T1.h"
#include "ModeMenuOneUserCodeRegDel_T1.h"

#include "ModeMenuAllCredentialsDelete_T1.h"

#include "ModeMenuRemoconRegDel_T1.h"
#include "ModeMenuOneRemoconRegDel_T1.h"

//#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
#include "ModeMenuSubRemoconRegDel_T1.h"
#include "ModeMenuOneSubRemoconRegDel_T1.h"
//#endif 


#include "ModeMenuRfLinkModuleRegDel_T1.h"


extern BYTE gMenuSelectKeyTempSave;
extern BYTE gSelectedSlotNumber;


void ModeGotoMenuMainSelect();
void ReInputOrModeMove(void(*BackwardMenuMode)(void));
void ReInputOrModeMove_1(void(*BackwardMenuMode)(void), void(*ReInputProcess)(void));
void GotoPreviousOrComplete(void(*BackwardMenuMode)(void));
void MenuSelectKeyTempSaveNTimeoutReset(WORD VoiceData, BYTE MenuKey);
void ModeGotoMenuAdvancedMain(void);
BYTE GetSupportCredentialCount(void);
void ModeGotoMenuNormalMain();
void ModeMenuMainSelect(void);

extern BYTE gbMenuExchangeKey;
extern BYTE gbMenuInnerExchangeKey;

#if (DDL_CFG_BLE_30_ENABLE >= 0x30)
void ModeExchangeKeySet(void);
void ModeGotoExchangeKeySet();
#endif 

#endif


