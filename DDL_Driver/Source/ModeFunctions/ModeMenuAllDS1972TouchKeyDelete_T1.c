//------------------------------------------------------------------------------
/** 	@file		ModeMenuAllDS1972TouchKeyDelete_T1.c
	@version 0.1.00
	@date	2016.09.12
	@brief	All DS1972 Touch Key Delete Mode
	@remark	전체 터치키 삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.09.12		by hyojoon
*/
//------------------------------------------------------------------------------

#include "Main.h"


 
void ModeGotoAllDS1972TouchKeyDelete(void)
{
	gbMainMode = MODE_MENU_ALLDS1972TOUCHKEY_DELETE; 
	gbModePrcsStep = 0;
}

void ProcessDeleteAllDS1972TouchKey(void)
{
	BYTE bTmp;

	bTmp = PincodeVerify(0);
	switch(bTmp)
	{
		case 1:
		case RET_MASTER_CODE_MATCH:
			if((gbManageMode == _AD_MODE_SET) && (bTmp != RET_MASTER_CODE_MATCH))
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); 
				ModeClear();
				break;				
			}

			if((gbManageMode != _AD_MODE_SET) && (bTmp != 1))
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); 
				ModeClear();
				break;				
			}

			AllDS1972TouchKeyDelete();

			// All Touch Key Delete Event 전송
			PackTxEventCredentialDeleted(CREDENTIALTYPE_DS1972TOUCHKEY, 0xFF);

			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);			//	completed, Press the star for setting other options or press the "R" button to finish
			gbModePrcsStep++;
			break;

		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	
			ModeClear();
			break;				
	}
}




void ModeAllDS1972TouchKeyDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_PIN_SHARP, VOL_CHECK);		
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:		 //# 버튼음 출력
					FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
					gbModePrcsStep++;
					break;		

				case TENKEY_STAR:
					ReInputOrModeMove(ModeGotoMenuMainSelect);
					break;
					
				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessDeleteAllDS1972TouchKey();
			break;		

		case 3:
			GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			break;

		default:
			ModeClear();
			break;
	}
}
