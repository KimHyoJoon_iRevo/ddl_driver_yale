//------------------------------------------------------------------------------
/** 	@file		ModeMenuMasterCodeRegister_T1.h
	@brief	Master Code Register Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUMASTERCODEREGISTER_T1_INCLUDED
#define __MODEMENUMASTERCODEREGISTER_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




extern BYTE gPinCodeTmpBuf[8];


void ModeGotoMasterCodeRegister(void);
void ModeMasterCodeRegister(void);

#endif


