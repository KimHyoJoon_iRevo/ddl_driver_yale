//------------------------------------------------------------------------------
/** 	@file		ModeGMTouchKeyRegister_T1.c
	@version 0.1.00
	@date	2016.05.11
	@brief	GateMan TouchKey Register Mode 
	@remark	터치키 등록 모드
	@see	MainModeProcess_T1.c
	@see	GMTouchKeyProcess_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.05.11		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"



BYTE gInputKeyNumberForRegister = 0;

union{
	BYTE gbInPutKeyUidBuf[SUPPORTED_USERKEY_NUMBER][MAX_KEY_UID_SIZE];
	BYTE gAllKeyRegisterBuf[(SUPPORTED_USERKEY_NUMBER*MAX_KEY_UID_SIZE)];
}UKeyDataBuf;



void AllGMTouchKeyDataBufClear(void)
{
	gInputKeyNumberForRegister = 0;
	memset(UKeyDataBuf.gAllKeyRegisterBuf, 0xFF, (SUPPORTED_USERKEY_NUMBER*MAX_KEY_UID_SIZE));
}



void ModeGotoGMTouchKeyRegister(void)
{
	// 터치키를 이미 확인한 상태라 추가 확인이 필요 없어 삭제
//	GMTouchKeyInputClear();
	AllGMTouchKeyDataBufClear();
	
	gbMainMode = MODE_GM_TOUCHKEY_REGISTER;
	gbModePrcsStep = 0;
}



//==============================================================
// Input        : void                                          
// Output       : 1 => 이미 등록한 UID가 아닐 경우           
//		  0 => 이미 등록한 UID일 경우		
//==============================================================
BYTE PrcsTkUIDCheck(void)
{
	BYTE bCnt;

	for(bCnt = 0; bCnt < gInputKeyNumberForRegister; bCnt++)
	{
		if(memcmp(UKeyDataBuf.gbInPutKeyUidBuf[bCnt], gbiButtonBuf, 8) == 0)
		{
			return 0;					//같은 ID가 있을 경우 (이미 등록했을 경우)
		}
	}
	return 1;
}

void AllGMTouchKeyRegister()
{
	RomWrite(UKeyDataBuf.gAllKeyRegisterBuf, TOUCHKEY_UID, (SUPPORTED_USERKEY_NUMBER*MAX_KEY_UID_SIZE));
}



void AllGMTouchKeyDelete(void)
{
	RomWriteWithSameData(0xFF, TOUCHKEY_UID, (SUPPORTED_USERKEY_NUMBER*MAX_KEY_UID_SIZE));
}


void GMTouchKeyInputCheckForRegister(void)
{
	switch(gbInputKeyValue)
	{
		case FUNKEY_REG:
			gbModePrcsStep+=3;
			break;
			
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		default:
			if(GMTouchKeyInputCheck() == STATUS_SUCCESS)
			{
				gbModePrcsStep++;					//등록버튼 눌린 후 카드가 접촉되어 카드 저장 모드 진입
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}

			TimeExpiredCheck();
			break;
	}
}




void InputGMTouchKeyProcessLoop(void)
{
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

	//이미 등록한 것인지를 비교
	if(PrcsTkUIDCheck() == 0)
	{
		//Key is already registered in same process
		FeedbackErrorNumberOn(108, VOL_CHECK, gInputKeyNumberForRegister);
		gbModePrcsStep++;
		return;
	}

	memcpy(UKeyDataBuf.gbInPutKeyUidBuf[gInputKeyNumberForRegister], gbiButtonBuf, 8);
	
	gInputKeyNumberForRegister++;

	if(gInputKeyNumberForRegister >= SUPPORTED_USERKEY_NUMBER)
	{
		gbModePrcsStep+=2;
		return;
	}

	FeedbackCredentialInput(VOICE_MIDI_BUTTON2, gInputKeyNumberForRegister);
	gbModePrcsStep++;
}




//==============================================================//
// Input        : void                                          //
// Output       : void                                          //
//==============================================================//
void ModeGMTouchKeyRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			GMTouchKeyInputCheckForRegister();
			break;

		case 1:
			InputGMTouchKeyProcessLoop();
			break;			

		case 2:
			TimeExpiredCheck();

			if(P_IBUTTON_T)
			{
				SetGMTouchKeyTime(100);
			}	

			if(GMTouchKeyTimeCheck() == STATUS_SUCCESS)
			{
				GMTouchKeyInputClear();
				gbModePrcsStep = 0;
			}
			break;

		case 3:	
			if(gInputKeyNumberForRegister == 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;
			}

			AllGMTouchKeyRegister();
			FeedbackModeCompleted(1, VOL_CHECK);
			gbModePrcsStep++;

			PackTxEventCredentialAdded(CREDENTIALTYPE_GMTOUCHKEY, 0xFF);
			break;
		
		case 4:
			if(P_IBUTTON_T)
			{
				SetGMTouchKeyTime(100);
			}	

			if(GMTouchKeyTimeCheck() == STATUS_SUCCESS)
			{
				GMTouchKeyInputClear();
				ModeClear();
			}	
			break;

		default:								
			ModeClear();
			break;
	}
}





#ifdef	BLE_N_SUPPORT
void ModeGotoGMTouchKeyRegisterByBleN(void)
{
	GMTouchKeyInputClear();
	AllGMTouchKeyDataBufClear();
	
	gbMainMode = MODE_GMTOUCHKEY_REGISTER_BYBLEN;
	gbModePrcsStep = 0;

	FeedbackKeyPadLedOn(VOICE_MIDI_INTO, VOL_CHECK);

	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
}


void GMTouchKeyInputCheckForRegisterByBleN(void)
{
	switch(gbInputKeyValue)
	{
		case FUNKEY_REG:
			gbModePrcsStep+=3;
			break;
			
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		default:
			if(GMTouchKeyInputCheck() == STATUS_SUCCESS)
			{
				gbModePrcsStep++;				
				break;
			}

			if(GetModeTimeOut() == 0)
			{
				gbModePrcsStep+=3;
			}
			break;
	}
}


void InputGMTouchKeyProcessLoopByBleN(void)
{
	//이미 등록한 것인지를 비교
	if(PrcsTkUIDCheck() == 0)
	{
		//Key is already registered in same process
		FeedbackErrorNumberOn(108, VOL_CHECK, gInputKeyNumberForRegister);
		gbModePrcsStep++;
		return;
	}

	memcpy(UKeyDataBuf.gbInPutKeyUidBuf[gInputKeyNumberForRegister], gbiButtonBuf, 8);
	gInputKeyNumberForRegister++;

	if(gInputKeyNumberForRegister >= SUPPORTED_USERKEY_NUMBER)
	{
		gbModePrcsStep+=2;
		return;
	}

	FeedbackCredentialInput(VOICE_MIDI_BUTTON2, gInputKeyNumberForRegister);
	gbModePrcsStep++;

	SetModeTimeOut(50);
}


void ModeGMTouchKeyRegisterByBleN(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			GMTouchKeyInputCheckForRegisterByBleN();
			break;

		case 1:
			InputGMTouchKeyProcessLoopByBleN();
			break;			

		case 2:
			// 터치키 등록 중 Time Out이 발생하면 자동 종료되도록 수정
//			TimeExpiredCheck();
			if(GetModeTimeOut() == 0)
			{
				gbModePrcsStep++;
			}

			if(P_IBUTTON_T)
			{
				SetGMTouchKeyTime(100);
			}	

			if(GMTouchKeyTimeCheck() == STATUS_SUCCESS)
			{
				GMTouchKeyInputClear();
				gbModePrcsStep = 0;
			}
			break;

		case 3:
			if(gInputKeyNumberForRegister == 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;
			}

			AllGMTouchKeyRegister();
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			gbModePrcsStep++;
			
			PackTxEventCredentialAdded(CREDENTIALTYPE_GMTOUCHKEY, 0xFF);
			break;

		case 4:
			if(P_IBUTTON_T)
			{
				SetGMTouchKeyTime(100);
			}	

			if(GMTouchKeyTimeCheck() == STATUS_SUCCESS)
			{
				GMTouchKeyInputClear();
				ModeClear();
			}	
			break;

		default:
			ModeClear();
			break;
	}
}
#endif




