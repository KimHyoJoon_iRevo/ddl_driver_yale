//------------------------------------------------------------------------------
/** 	@file		ModeMenuSubRemoconRegDel_T1.c
	@version 0.1.00
	@date	2016.06.09
	@brief	All Remocons Register/Delete Mode
	@remark	전체 리모컨 등록/삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.06.09		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"



BYTE gbInputSubRemoconNumberForRegister = 0;				/**< 전체 리모컨 등록 모드에서 입력된 전체 리모컨 수  */

void SubRemoconSlotNumberSet(BYTE SlotNumber)
{
	gbInputSubRemoconNumberForRegister = SlotNumber;
}

BYTE GetSubRemoconSlotNumber(void)
{
	return (gbInputSubRemoconNumberForRegister);
}


#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)

void ModeGotoSubRemoconRegDelCheck(void)
{
	gbMainMode = MODE_MENU_SUBREMOCON_REGDEL;
	gbModePrcsStep = 0;
}


void ModeGotoSubRemoconRegister(void)
{
	gbInputSubRemoconNumberForRegister = 0;

	gbMainMode = MODE_MENU_SUBREMOCON_REGISTER;
	gbModePrcsStep = 0;
}

void ModeGotoSubRemoconDelete(void)
{
	gbMainMode = MODE_MENU_SUBREMOCON_DELETE;
	gbModePrcsStep = 0;
}

void SubRemoconModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// 전체 리모컨 등록
		case 1: 	
			ModeGotoSubRemoconRegister();
			break;
	
		// 전체 리모컨 삭제
		case 3: 		
			ModeGotoSubRemoconDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuSubRemoconModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 전체 리모컨 등록
		case TENKEY_1:			
//			MenuSelectKeyTempSaveNTimeoutReset(22, gbInputKeyValue);	// register a remote control, press the hash to continue. 	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_REMOCON_SHARP, gbInputKeyValue);	// register a remote control, press the hash to continue. 	
			break;

		// 전체 리모컨 삭제
		case TENKEY_3:		
//			MenuSelectKeyTempSaveNTimeoutReset(24, gbInputKeyValue);	// Delete a remote control, press the hash to continue. 
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_REMOCON_SHARP, gbInputKeyValue);	// Delete a remote control, press the hash to continue. 
			break;

		case TENKEY_SHARP:
			SubRemoconModeSetSelected();
			break;
									
		case TENKEY_STAR:
			ModeGotoRemoconRegDelCheck();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}




void ModeSubRemoconRegDelCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			Feedback13MenuOn(21, VOL_CHECK);				// press 1 to register a remote control, press 3 to delete a remote control
			Feedback13MenuOn(AVML_REGREMOCON1_DELREMOCON3, VOL_CHECK);				// press 1 to register a remote control, press 3 to delete a remote control

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuSubRemoconModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}

}




void ProcessRegisterSubRemocon(void)
{
	if(gbInputSubRemoconNumberForRegister == 0)
	{
		SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		ModeClear();
	}
	else
	{
		SubPackTxConnectionSend(EV_ALLREG_DEV_END, MODULE_TYPE_ONEWAY, 0);

		// BLE-N 회신 오는데 시간이 조금 더 걸려 500ms 시간 연장
//		SetModeTimeOut(10);
		SetModeTimeOut(15);
		
#ifdef	P_VOICE_RST	
		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
		gbModePrcsStep++;
	}
}


void RegisterSubRemocon(void)
{
	BYTE bTmp,bInnerTmp;
	
	bTmp = GetPackProcessResult();
	bInnerTmp = GetInnerPackProcessResult();
	if(bTmp == PACK_RMC_ALLREG_DEV_END || bInnerTmp == PACK_RMC_ALLREG_DEV_END)
	{
		if(bTmp)
			PackSaveID();

		if(bInnerTmp)
			InnerPackSaveID();

//		FeedbackModeCompletedKeepMode(42, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish
		FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);		//completed, Press the star for setting other options or press the "R" button to finish
	
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		gbModePrcsStep++;
	}
	
	if(bTmp != 0 || bInnerTmp != 0)
	{
		PackProcessResultClear();
	}

	TimeExpiredCheck();
}


void SubRemoconRegDelProcess(void)
{
//	FeedbackModeCompletedKeepMode(39, VOL_CHECK);
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
	
	gbModePrcsStep++;
}


void ModeSubRemoconRegister(void)
{
	BYTE bTmp,bInnerTmp;

	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(23, VOL_CHECK);			// Press the registration button on the remote control
			FeedbackKeyPadLedOn(AVML_PRESS_REMOCONSET, VOL_CHECK);			// Press the registration button on the remote control

			SetModeTimeOut(_MODE_TIME_OUT_60S);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;

			gbInputSubRemoconNumberForRegister = 0;

			SubPackTxConnectionSend(EV_ALL_REGISTRATION, MODULE_TYPE_ONEWAY, 0);
			break;
			
		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_SHARP:
					ProcessRegisterSubRemocon();
					break;		

				case TENKEY_STAR:
					SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);

					ReInputOrModeMove(ModeGotoSubRemoconRegDelCheck);
					break;

				case FUNKEY_REG:
				case FUNKEY_OPCLOSE:
					SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);

					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
				
				default:
					bTmp = GetPackProcessResult();
					bInnerTmp = GetInnerPackProcessResult();
					if(bTmp == PACK_RMC_REGDEVICE_IN || bInnerTmp == PACK_RMC_REGDEVICE_IN)
					{
						FeedbackCredentialInput_1(VOICE_MIDI_BUTTON2, gbInputSubRemoconNumberForRegister);
						SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					}

					if(bTmp != 0 || bInnerTmp != 0)
					{
						PackProcessResultClear();
					}
#if 0		//ModeSubRemoconRegister 은 BLE 에 대해서 고려 하지 않게 				
					if(gfPackTypeiRevoBleN ||gfInnerPackTypeiRevoBleN )
					{
						// BLE-N의 경우 입력이 있을 경우에는 #을 누르지 않고 TimeOut이 되더라도 등록 처리하도록 프로그램 수정	
						if(GetModeTimeOut() == 0)
						{
							ProcessRegisterSubRemocon();
							break;
						}
					}
					else
#endif 						
					{
						TimeExpiredCheck();
					}

					if(GetModeTimeOut() == 0)
					{
						SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);
					}						
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			RegisterSubRemocon();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoSubRemoconRegDelCheck);
			break;
		
		default:
			ModeClear();
			break;
	}
}





void ProcessDeleteSubRemocon(void)
{
	switch(PincodeVerify(0))
	{
		case 1:
			SubPackTxConnectionSend(EV_ALL_REMOVE, MODULE_TYPE_ONEWAY, 0);
			
			// BLE-N 회신 오는데 시간이 조금 더 걸려 500ms 시간 연장
//			SetModeTimeOut(10);
			SetModeTimeOut(15);

#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;

		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
			SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);
			
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);
			
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);		
			ModeClear();
			break;				
	}
}



void DeleteSubRemocon(void)
{
	BYTE bTmp,bInnerTmp;
	
	bTmp = GetPackProcessResult();
	bInnerTmp = GetInnerPackProcessResult();
	if(bTmp == PACK_RMC_ALL_REMOVE_END || bInnerTmp == PACK_RMC_ALL_REMOVE_END)
	{
//		FeedbackModeCompletedKeepMode(58, VOL_CHECK);		// Completed, Press the star for an additional deletion or press the "R"button to finish
		FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);		// Completed, Press the star for setting other options or press the "R"button to finish
	
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		gbModePrcsStep++;
	}
	
	if(bTmp != 0 || bInnerTmp != 0)
	{
		PackProcessResultClear();
	}

	TimeExpiredCheck();
}




void ModeSubRemoconDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(15, VOL_CHECK);			//Enter the user code, then press the hash to continue
			FeedbackKeyPadLedOn(AVML_IN_PIN_SHARP, VOL_CHECK);			//Enter the user code, then press the hash to continue

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					ProcessDeleteSubRemocon();
					break;		

				case TENKEY_STAR:
					SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);

					ReInputOrModeMove(ModeGotoSubRemoconRegDelCheck);
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);

					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			DeleteSubRemocon();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoSubRemoconRegDelCheck);
			break;

		default:
			ModeClear();
			break;
	}
}
#endif 
