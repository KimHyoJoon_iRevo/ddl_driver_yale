//------------------------------------------------------------------------------
/** 	@file		ModeMenuVolumeSet_T1.c
	@version 0.1.00
	@date	2016.04.26
	@brief	Programmable Volume Setting
	@remark	볼륨 설정 모드
	@see	ModeMenuMainSelect_T1.c
	@see	ModeMenuLockSetting_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.26		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"


#ifdef	LOCKSET_VOLUME_SET

void ModeGotoVolumeSetting(void)
{
	gbMainMode = MODE_MENU_VOLUME_SET;
	gbModePrcsStep = 0;
}



void VolumeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		//Volume High
		case 1: 	
			VolumeSetting(VOL_HIGH);;

			// 볼륨 설정 Event 전송

//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);

			gbModePrcsStep++;
			break;
	
		//Volume Low
		case 2: 	
			VolumeSetting(VOL_LOW);;
		
			// 볼륨 설정 Event 전송
		
//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
		
			gbModePrcsStep++;
			break;

		//Volume Silent
		case 3: 		
			VolumeSetting(VOL_SILENT);

			// 볼륨 설정 Event 전송

//			FeedbackModeCompletedKeepMode(39, VOL_CHECK);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);

			gbModePrcsStep++;
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 이전 모드로 이동
			gbModePrcsStep--;
			break;
	}			
}



void MenuVolumeSetSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		//Volume High
		case TENKEY_1:			
//			MenuSelectKeyTempSaveNTimeoutReset(144, gbInputKeyValue);		
			MenuSelectKeyTempSaveNTimeoutReset(AVML_VOL_HIGH_SHARP, gbInputKeyValue);		
			break;

		//Volume Low
		case TENKEY_2:			
//			MenuSelectKeyTempSaveNTimeoutReset(145, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_VOL_LOW_SHARP, gbInputKeyValue);	
			break;

		//Volume Silent
		case TENKEY_3:			
//			MenuSelectKeyTempSaveNTimeoutReset(146, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_VOL_MUTE_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_SHARP:		 //# 버튼음 출력
#ifdef	P_VOICE_RST	
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
				FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuLockSetting();
			break;
			
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;

		default:
			TimeExpiredCheck();
			break;
	}	
}




//------------------------------------------------------------------------------
/** 	@brief	Volume Setting Mode
	@param	None
	@return 	None
	@remark 볼륨 설정 모드 시작
*/
//------------------------------------------------------------------------------
void ModeVolumeSetting(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			Feedback123MenuOn(143, VOL_CHECK);
			Feedback123MenuOn(AVML_CHG_VOL_MSG, VOL_CHECK);

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuVolumeSetSelectProcess();
			break;	
	
		case 2:
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
			{
				if(GetBuzPrcsStep() || GetVoicePrcsStep()) 	break;
			}

			VolumeSetSelected();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoMenuLockSetting);
			break;

		default:
			ModeClear();
			break;
	}

}

#endif	// LOCKSET_VOLUME_SET


