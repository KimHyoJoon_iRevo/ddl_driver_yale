//------------------------------------------------------------------------------
/** 	@file		ModeGMTouchKeyRegister_T1.c
	@brief	GateMan TouchKey Register Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODEGMTOUCHKEYREGISTER_T1_INCLUDED
#define __MODEGMTOUCHKEYREGISTER_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


void ModeGotoGMTouchKeyRegister(void);
void ModeGMTouchKeyRegister(void);

void AllGMTouchKeyDelete(void);


#ifdef	BLE_N_SUPPORT
void ModeGotoGMTouchKeyRegisterByBleN(void);
void ModeGMTouchKeyRegisterByBleN(void);
#endif


#endif


