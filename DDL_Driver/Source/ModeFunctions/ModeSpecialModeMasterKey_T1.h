//------------------------------------------------------------------------------
/** 	@file		ModeSpecialModeMasterKey_T1.h
	@brief	DS1972 TouchKey Register Mode For Fingerprint Product 
*/
//------------------------------------------------------------------------------

#ifndef __MODESPECIALMODEMASTERKEY_T1_INCLUDED
#define __MODESPECIALMODEMASTERKEY_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"



#ifdef	_MASTERKEY_IBUTTON_SUPPORT

void MasterKeyEnable(void);
void MasterKeyDisable(void);


void ModeGotoSpecialModeMasterKeyRegister(void);
void ModeGotoSpecialModeMasterKeyVerify(void);
void ModeGotoiButtonWithoutFloatingID(void);


void MasterKeyEnabledSet(void);
void MasterKeyEnabledClear(void);
void MasterKeyEnableCheck(void);




#endif	// _MASTERKEY_IBUTTON_SUPPORT

#endif


