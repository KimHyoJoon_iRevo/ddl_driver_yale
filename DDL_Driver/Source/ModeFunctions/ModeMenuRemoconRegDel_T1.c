//------------------------------------------------------------------------------
/** 	@file		ModeMenuRemoconRegDel_T1.c
	@version 0.1.00
	@date	2016.06.09
	@brief	All Remocons Register/Delete Mode
	@remark	전체 리모컨 등록/삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.06.09		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"



BYTE gbInputRemoconNumberForRegister = 0;				/**< 전체 리모컨 등록 모드에서 입력된 전체 리모컨 수  */


void ModeGotoRemoconRegDelCheck(void)
{
	gbMainMode = MODE_MENU_REMOCON_REGDEL;
	gbModePrcsStep = 0;
}


void ModeGotoRemoconRegister(void)
{
	gbInputRemoconNumberForRegister = 0;

	gbMainMode = MODE_MENU_REMOCON_REGISTER;
	gbModePrcsStep = 0;
}

void ModeGotoRemoconDelete(void)
{
	gbMainMode = MODE_MENU_REMOCON_DELETE;
	gbModePrcsStep = 0;
}


void RemoconSlotNumberSet(BYTE SlotNumber)
{
	gbInputRemoconNumberForRegister = SlotNumber;
}

BYTE GetRemoconSlotNumber(void)
{
	return (gbInputRemoconNumberForRegister);
}



void RemoconModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// 전체 리모컨 등록
		case 1: 	
			ModeGotoRemoconRegister();
			break;
	
		// 전체 리모컨 삭제
		case 3: 		
			ModeGotoRemoconDelete();
			break;
	
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
		// 8번 하위 8번 메뉴 
		case 8: 		
			ModeGotoSubRemoconRegDelCheck();
			break;

#ifdef DDL_CFG_ADD_REMOCON_LINK_FUNCTION
		// 리모컨 등록 신호 출력
		case 9: 		
			ModeGotoLockOpenConnectionRegister();
			break;
#endif 		//DDL_CFG_ADD_REMOCON_LINK_FUNCTION

#endif 		//DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU	

		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuRemoconModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 전체 리모컨 등록
		case TENKEY_1:			
//			MenuSelectKeyTempSaveNTimeoutReset(22, gbInputKeyValue);	// register a remote control, press the hash to continue. 	
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
			if(!gfPackTypeiRevoBleN && !gfInnerPackTypeiRevoBleN)
			{
				FeedbackTenKeyErrorBlink(VOICE_MIDI_ERROR, VOL_CHECK, gbInputKeyValue);
				gMenuSelectKeyTempSave = TENKEY_NONE; 
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
			else 
			{
//				MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_REMOCON_SHARP, gbInputKeyValue);	// register a remote control, press the hash to continue. 	
				MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_BT_SHARP , gbInputKeyValue);// 블루투스 등록 모드 입니다. 계속 하시려면 샵 버튼을 누르세요 	

			}
#else 
#ifdef __DDL_MODEL_B2C15MIN
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_BT_SHARP, gbInputKeyValue); // register a remote control, press the hash to continue.	
#else 
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_REMOCON_SHARP, gbInputKeyValue);	// register a remote control, press the hash to continue. 	
#endif 			
#endif 			
			break;

		// 전체 리모컨 삭제
		case TENKEY_3:		
//			MenuSelectKeyTempSaveNTimeoutReset(24, gbInputKeyValue);	// Delete a remote control, press the hash to continue. 
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
			if(!gfPackTypeiRevoBleN && !gfInnerPackTypeiRevoBleN)
			{
				FeedbackTenKeyErrorBlink(VOICE_MIDI_ERROR, VOL_CHECK, gbInputKeyValue);
				gMenuSelectKeyTempSave = TENKEY_NONE; 
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
			else 
			{
//				MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_REMOCON_SHARP, gbInputKeyValue);	// register a remote control, press the hash to continue.	
				MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_BT_SHARP , gbInputKeyValue);	// 블루투스 삭제 모드입니다. 계속 하시려면 샵 버튼을 누르세요 
			}
#else 
#ifdef __DDL_MODEL_B2C15MIN
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_BT_SHARP, gbInputKeyValue); // Delete a remote control, press the hash to continue. 		
#else 
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_REMOCON_SHARP, gbInputKeyValue);	// Delete a remote control, press the hash to continue. 
#endif 			
#endif 			
			break;

#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
		case TENKEY_8:		
//			MenuSelectKeyTempSaveNTimeoutReset(24, gbInputKeyValue);	// Delete a remote control, press the hash to continue. 
			if(!gfPackTypeiRevo && !gfInnerPackTypeiRevo)
			{
				FeedbackTenKeyErrorBlink(VOICE_MIDI_ERROR, VOL_CHECK, gbInputKeyValue);
				gMenuSelectKeyTempSave = TENKEY_NONE; 
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
			else 
			{
				MenuSelectKeyTempSaveNTimeoutReset(AVML_MAN_REMOCON_SHARP, gbInputKeyValue);	// Delete a remote control, press the hash to continue. 
			}				
			break;

#ifdef DDL_CFG_ADD_REMOCON_LINK_FUNCTION
		case TENKEY_9:
			if(!gfPackTypeiRevo && !gfInnerPackTypeiRevo)
			{
				FeedbackTenKeyErrorBlink(VOICE_MIDI_ERROR, VOL_CHECK, gbInputKeyValue);
				gMenuSelectKeyTempSave = TENKEY_NONE; 
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
			else 
			{
				MenuSelectKeyTempSaveNTimeoutReset(VOICE_MIDI_BUTTON, gbInputKeyValue);
			}
			break;
#endif 	//DDL_CFG_ADD_REMOCON_LINK_FUNCTION

#endif 	//DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU			
		
		case TENKEY_SHARP:
			RemoconModeSetSelected();
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}




void ModeRemoconRegDelCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
#if 0
			if(gbModuleMode != PACK_ID_CONFIRMED && gbInnerModuleMode != PACK_ID_CONFIRMED)
			{
				FeedbackError(VOICE_MIDI_ERROR,VOL_HIGH);
				ModeClear();
				break;
			}
#endif			
			Feedback138MenuOn(AVML_REGBT1_DELBT3_REGREMOTE_8, VOL_CHECK);						// 블루투스 등록은 1번 , 블루투스 삭제는 3번 , 리모컨 등록은 8번을 누르세요. 
#else
#ifdef __DDL_MODEL_B2C15MIN
			Feedback13MenuOn(AVML_SELECT_MENU, VOL_CHECK);				// press 1 to register a remote control, press 3 to delete a remote control
#else 
			Feedback13MenuOn(AVML_REGREMOCON1_DELREMOCON3, VOL_CHECK);				// press 1 to register a remote control, press 3 to delete a remote control
#endif 			
#endif 			

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuRemoconModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}

}




void ProcessRegisterRemocon(void)
{
	if(gbInputRemoconNumberForRegister == 0)
	{
		PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		ModeClear();
	}
	else
	{
		PackTxConnectionSend(EV_ALLREG_DEV_END, MODULE_TYPE_ONEWAY, 0);

		// BLE-N 회신 오는데 시간이 조금 더 걸려 500ms 시간 연장
//		SetModeTimeOut(10);
		SetModeTimeOut(15);
		
#ifdef	P_VOICE_RST	
		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
		gbModePrcsStep++;
	}
}


void RegisterRemocon(void)
{
	BYTE bTmp,bInnerTmp;
	BYTE bTmpArray[8];
	
#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
	bTmp = GetPackProcessResult();
	bInnerTmp = GetInnerPackProcessResult();
#elif defined (P_COM_DDL_EN_T)
	bTmp = GetPackProcessResult();
	bInnerTmp = 0;	
#elif defined (P_BT_WAKEUP_T)
	bTmp = 0;
	bInnerTmp = GetInnerPackProcessResult();
#endif

	if(bTmp == PACK_RMC_ALLREG_DEV_END || bInnerTmp == PACK_RMC_ALLREG_DEV_END)
	{
		if(bTmp)
			PackSaveID();

		if(bInnerTmp)
			InnerPackSaveID();

//		FeedbackModeCompletedKeepMode(42, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish
		FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);		//completed, Press the star for setting other options or press the "R" button to finish
	
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	
		if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)
		{
			memset(bTmpArray, 0xFF, 8);
			CopyCredentialDataForPack(bTmpArray, 8);
			Delay(SYS_TIMER_30MS);
			PackTxEventCredentialAdded(CREDENTIALTYPE_BLE, GetRemoconSlotNumber());
		}

		gbModePrcsStep++;
	}
	
	if(bTmp != 0 || bInnerTmp != 0)
	{
		PackProcessResultClear();
	}

	TimeExpiredCheck();
}


void RemoconRegDelProcess(void)
{
//	FeedbackModeCompletedKeepMode(39, VOL_CHECK);
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
	
	gbModePrcsStep++;
}


void ModeRemoconRegister(void)
{
	BYTE bTmp,bInnerTmp;

	switch(gbModePrcsStep)
	{
		case 0:
#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU) || defined (__DDL_MODEL_B2C15MIN)
			FeedbackKeyPadLedOn(AVML_SMART_LIVING_APP_MSG, VOL_CHECK);
#else 
			FeedbackKeyPadLedOn(AVML_PRESS_REMOCONSET, VOL_CHECK);
#endif 

			SetModeTimeOut(_MODE_TIME_OUT_60S);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;

			gbInputRemoconNumberForRegister = 0;

			PackTxConnectionSend(EV_ALL_REGISTRATION, MODULE_TYPE_ONEWAY, 0);
			break;
			
		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_SHARP:
					ProcessRegisterRemocon();
					break;		

				case TENKEY_STAR:
					PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);

					ReInputOrModeMove(ModeGotoRemoconRegDelCheck);
					break;

				case FUNKEY_REG:
				case FUNKEY_OPCLOSE:
					PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);

					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
				
				default:
#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
					bTmp = GetPackProcessResult();
					bInnerTmp = GetInnerPackProcessResult();
#elif defined (P_COM_DDL_EN_T)
					bTmp = GetPackProcessResult();
					bInnerTmp = 0;
#elif defined (P_BT_WAKEUP_T)
					bTmp = 0;
					bInnerTmp = GetInnerPackProcessResult();
#endif

					if(bTmp == PACK_RMC_REGDEVICE_IN || bInnerTmp == PACK_RMC_REGDEVICE_IN)
					{
						FeedbackCredentialInput_1(VOICE_MIDI_BUTTON2, gbInputRemoconNumberForRegister);
						SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					}

					if(bTmp != 0 || bInnerTmp != 0)
					{
						PackProcessResultClear();
					}
						
					if(gfPackTypeiRevoBleN ||gfInnerPackTypeiRevoBleN )
					{
						// BLE-N의 경우 입력이 있을 경우에는 #을 누르지 않고 TimeOut이 되더라도 등록 처리하도록 프로그램 수정	
						if(GetModeTimeOut() == 0)
						{
							ProcessRegisterRemocon();
							break;
						}
					}
					else
					{
						TimeExpiredCheck();
					}

					if(GetModeTimeOut() == 0)
					{
						PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);
					}						
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			RegisterRemocon();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoRemoconRegDelCheck);
			break;
		
		default:
			ModeClear();
			break;
	}
}





void ProcessDeleteRemocon(void)
{
	switch(PincodeVerify(0))
	{
		case 1:
			PackTxConnectionSend(EV_ALL_REMOVE, MODULE_TYPE_ONEWAY, 0);
			
			// BLE-N 회신 오는데 시간이 조금 더 걸려 500ms 시간 연장
//			SetModeTimeOut(10);
			SetModeTimeOut(15);

#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;

		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
			PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);
			
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);
			
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);		
			ModeClear();
			break;				
	}
}



void DeleteRemocon(void)
{
	BYTE bTmp,bInnerTmp;
	
#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
	bTmp = GetPackProcessResult();
	bInnerTmp = GetInnerPackProcessResult();
#elif defined (P_COM_DDL_EN_T)
	bTmp = GetPackProcessResult();
	bInnerTmp = 0;
#elif defined (P_BT_WAKEUP_T)
	bTmp = 0;	
	bInnerTmp = GetInnerPackProcessResult();
#endif

	if(bTmp == PACK_RMC_ALL_REMOVE_END || bInnerTmp == PACK_RMC_ALL_REMOVE_END)
	{
//		FeedbackModeCompletedKeepMode(58, VOL_CHECK);		// Completed, Press the star for an additional deletion or press the "R"button to finish
		FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);		// Completed, Press the star for setting other options or press the "R"button to finish
	
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	
		if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)
		{
			Delay(SYS_TIMER_30MS);
			PackTxEventCredentialDeleted(CREDENTIALTYPE_BLE, 0xFF);
		}

		gbModePrcsStep++;
	}
	
	if(bTmp != 0 || bInnerTmp != 0)
	{
		PackProcessResultClear();
	}

	TimeExpiredCheck();
}




void ModeRemoconDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(15, VOL_CHECK);			//Enter the user code, then press the hash to continue
			FeedbackKeyPadLedOn(AVML_IN_PIN_SHARP, VOL_CHECK);			//Enter the user code, then press the hash to continue

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					ProcessDeleteRemocon();
					break;		

				case TENKEY_STAR:
					PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);

					ReInputOrModeMove(ModeGotoRemoconRegDelCheck);
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);

					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			DeleteRemocon();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoRemoconRegDelCheck);
			break;

		default:
			ModeClear();
			break;
	}
}



#ifdef	BLE_N_SUPPORT
void ModeGotoRemoconRegisterByBleN(void)
{
	gbInputRemoconNumberForRegister = 0;

	gbMainMode = MODE_MENU_REMOCON_REGISTER_BYBLEN;
	gbModePrcsStep = 0;
}


void ModeGotoRemoconDeleteByBleN(void)
{
	gbMainMode = MODE_MENU_REMOCON_DELETE_BYBLEN;
	gbModePrcsStep = 0;
}


void ModeRemoconRegisterByBleN(void)
{
	BYTE bTmp,bInnerTmp;

	switch(gbModePrcsStep)
	{
		case 0:
			// 바로 전송하면 동작하지 않는 경우 발생
			SetModeTimeOut(5);
			gbModePrcsStep++;
			break;

		case 1:
			if(GetModeTimeOut())	break;

#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU) || defined (__DDL_MODEL_B2C15MIN)
			FeedbackKeyPadLedOn(AVML_SMART_LIVING_APP_MSG, VOL_CHECK);
#else 
			FeedbackKeyPadLedOn(AVML_PRESS_REMOCONSET, VOL_CHECK);			// Press the registration button on the remote control
#endif 			

			SetModeTimeOut(_MODE_TIME_OUT_60S);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;

			gbInputRemoconNumberForRegister = 0;

			PackTxConnectionSend(EV_ALL_REGISTRATION, MODULE_TYPE_ONEWAY, 0);
			break;
			
		case 2:
			switch(gbInputKeyValue)
			{
				case TENKEY_SHARP:
					ProcessRegisterRemocon();
					break;		

				case TENKEY_STAR:
					PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);

					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;

				case FUNKEY_REG:
				case FUNKEY_OPCLOSE:
					PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);

					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
				
				default:
#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
					bTmp = GetPackProcessResult();
					bInnerTmp = GetInnerPackProcessResult();
#elif defined (P_COM_DDL_EN_T)
					bTmp = GetPackProcessResult();
					bInnerTmp = 0;
#elif defined (P_BT_WAKEUP_T)
					bTmp = 0;
					bInnerTmp = GetInnerPackProcessResult();
#endif
					if(bTmp == PACK_RMC_REGDEVICE_IN || bInnerTmp == PACK_RMC_REGDEVICE_IN)
					{
						FeedbackCredentialInput(VOICE_MIDI_BUTTON2, GetRemoconSlotNumber());

						SetModeTimeOut(5);
						gbModePrcsStep++;
					}

					if(bTmp != 0 || bInnerTmp != 0)
					{
						PackProcessResultClear();
					}
						
					TimeExpiredCheck();

					if(GetModeTimeOut() == 0)
					{
						PackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_ONEWAY, 0);
					}						
					break;
			}	
			break;

		case 3:
			if(GetModeTimeOut())	break;

			ProcessRegisterRemocon();
			break;			

		case 4:
			if(GetModeTimeOut())	break;

			RegisterRemocon();
			break;

		case 5:
//			FeedbackModeCompleted(1, VOL_CHECK);		//completed
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			ModeClear();
			break;
		
		default:
			ModeClear();
			break;
	}
}


void ModeRemoconDeleteByBleN(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			// 바로 전송하면 동작하지 않는 경우 발생
			SetModeTimeOut(5);
			gbModePrcsStep++;
			break;

		case 1:
			// 바로 전송하면 동작하지 않는 경우 발생
			if(GetModeTimeOut())	break;

			PackTxConnectionSend(EV_ALL_REMOVE, MODULE_TYPE_ONEWAY, 0);
			
			// BLE-N 회신 오는데 시간이 조금 더 걸려 500ms 시간 연장
//			SetModeTimeOut(10);
			SetModeTimeOut(15);

#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;		

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			DeleteRemocon();
			break;

		case 3:	
//			FeedbackModeCompleted(1, VOL_CHECK);		//completed
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			ModeClear();
			break;

		default:
			ModeClear();
			break;
	}
}


#endif




#ifdef DDL_CFG_ADD_REMOCON_LINK_FUNCTION
void ModeGotoLockOpenConnectionRegister(void)
{
	gbMainMode = MODE_MENU_LOCKOPEN_CONNECTION_REGISTER;
	gbModePrcsStep = 0;
}



void ModeLockOpenConnectionRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);
			PackTxRemoconLinkSend(0x01);

			FeedbackKeyPadLedOn(VOICE_MIDI_INTO, VOL_CHECK);	
			
			// 리모컨 신호 송신 시간 3초 대기
			SetModeTimeOut(30);
			
			gMenuSelectKeyTempSave = TENKEY_NONE;
			
			gbModePrcsStep++;
			break;

		case 1:
			if(GetModeTimeOut())	break;
			
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK); 	// Completed, Press the star for setting other options or press the "R"button to finish
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;

		case 2:
			GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			break;

		default:
			ModeClear();
			break;
	}
}
#endif


