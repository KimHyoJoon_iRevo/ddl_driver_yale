//------------------------------------------------------------------------------
/** 	@file		ModeMenuAllCredentialsDelete_T1.c
	@brief	All Credentials Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUALLCREDENTIALSDELETE_T1_INCLUDED
#define __MODEMENUALLCREDENTIALSDELETE_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoAllCredentialsDelete(void);
void ModeAllCredentialsDelete(void);
void ModeGotoAllDataReset(uint8_t data);
void ModeAllDataReset(void);

#endif


