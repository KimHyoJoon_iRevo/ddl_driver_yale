//------------------------------------------------------------------------------
/** 	@file		ModeMenuModeChange_T1.c
	@version 0.1.00
	@date	2016.05.18
	@brief	Normal/Advanced Mode Change
	@remark	Normal Mode와 Advanced Mode를 변환
	@see	ModeMenuMainSelect_T1.c
	@see	ModeMenuMasterCodeRegister_T1.c
	@see	ModePincodeRegister_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.05.18		by Jay
		V0.1.01 2016.07.07		by Hoon
							AllUserCodeDelete 함수 수정
								User status 삭제 기능 추가
*/
//------------------------------------------------------------------------------

#include "Main.h"



void ModeGotoChangeToAdvancedMode(void)
{
	gbMainMode = MODE_MENU_ADVANCED_MODE_SET;
	gbModePrcsStep = 0;
}


void ModeGotoChangeToNormalMode(void)
{
	gbMainMode = MODE_MENU_NORMAL_MODE_SET;
	gbModePrcsStep = 0;
}




void LockWorkingModeLoad(void)
{
	RomRead(&gbManageMode, MANAGE_MODE, 1);
}


void LockWorkingModeDefaultSet(void)
{
	gbManageMode = LOCK_WORKINGMODE_DEFAULT;
	RomWrite(&gbManageMode, (WORD)MANAGE_MODE, 1);
}


void AdvancedModeSet(void)
{
	gbManageMode = _AD_MODE_SET;
	RomWrite(&gbManageMode, (WORD)MANAGE_MODE, 1);
}


void NormalModeSet(void)
{
	gbManageMode = 0;
	RomWrite(&gbManageMode, (WORD)MANAGE_MODE, 1);
}



void MasterCodeDelete(void)
{
#if defined (_USE_IREVO_CRYPTO_)
	BYTE bTemp[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	EncryptDecryptKey(bTemp, 8);
	RomWrite(bTemp, MASTER_CODE, 8);	
#else 
	RomWriteWithSameData(0xFF, MASTER_CODE, 8);
#endif 
}


void AllUserCodeDelete()
{
	//  User code 영역 삭제
#if defined (_USE_IREVO_CRYPTO_)
	BYTE bTemp[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	BYTE i = 0x00;

	EncryptDecryptKey(bTemp, 8);

	for( i = 0 ; i < SUPPORTED_USERCODE_NUMBER ; i++)
	{
#ifdef	DDL_CFG_WATCHDOG
		RefreshIwdg();
#endif
		RomWrite(bTemp,USER_CODE+(i*8),8);
	}
#else 	
	RomWriteWithSameData(0xFF, USER_CODE, (SUPPORTED_USERCODE_NUMBER*8));		
#endif 

	// User status 영역 삭제 
	RomWriteWithSameData(USER_STATUS_AVAILABLE, USER_STATUS, (SUPPORTED_USERCODE_NUMBER*1));	

	// Schedule status 영역 삭제 
	RomWriteWithSameData(SCHEDULE_STATUS_DISABLE, USER_SCH_STA, 30U); 
	// Schedule 영역 삭제 
	RomWriteWithSameData(0xFF, USER_SCHEDULE, (30U*4)); 
	
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	ScheduleDeleteOfAllUserCode();
#endif
}



void ModeInitialSetChangeToAdvancedMode(void)
{
	MasterCodeDelete();
	OnetimeCodeDelete();
	AllUserCodeDelete();
	
//	AllScheduleStatusClear(CLEAR_ALL_CODE);

	AdvancedModeSet();

	UpdatePincodeToMemory(MASTER_CODE);

	// Master Code 등록 Event 전송

#ifdef 	DDL_CFG_RFID
	AllCardDelete();
#endif 

#ifdef	DDL_CFG_FP
#ifdef	DDL_CFG_DIMMER			//디밍 없는 지문 모델과 지문 없는 지문 모델의 모드 변경시 Pin Display를 구분 하기 위해 추가 2019년06월25일 sjc
	gbModeChangeFlag4Finger = 1;	//디밍 없는 지문 모델과 지문 없는 지문 모델의 모드 변경시 Pin Display를 구분 하기 위해 추가 2019년06월25일 sjc
#else							//디밍 없는 지문 모델과 지문 없는 지문 모델의 모드 변경시 Pin Display를 구분 하기 위해 추가 2019년06월25일 sjc
	gbModeChangeFlag4Finger = 2;	//디밍 없는 지문 모델과 지문 없는 지문 모델의 모드 변경시 Pin Display를 구분 하기 위해 추가 2019년06월25일 sjc
#endif							//디밍 없는 지문 모델과 지문 없는 지문 모델의 모드 변경시 Pin Display를 구분 하기 위해 추가 2019년06월25일 sjc
	ModeGotoAdvancedAllFingerDelete1();
#endif 

#ifdef	DDL_CFG_IBUTTON
	AllGMTouchKeyDelete();
#endif 

#ifdef	DDL_CFG_DS1972_IBUTTON
	AllDS1972TouchKeyDelete();
#endif

	TamperCountClear();

#ifdef	BLE_N_SUPPORT
	if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)
	{
		// 일반 모드의 값이 0이 아닌 0xBB로 회신해야 해서, 해당 내용을 Converting하는 함수 추가하여 처리
//		BLENTxModeChangeSubEventSend(GetCurrentOperationMode()); 
	}
#endif
}


void ModeInitialSetChangeToNormalMode(void)
{
	MasterCodeDelete();
	OnetimeCodeDelete();
	AllUserCodeDelete();
	
//	AllScheduleStatusClear(CLEAR_ALL_CODE);

	NormalModeSet();

	UpdatePincodeToMemory(USER_CODE);

	SetUserStatus(1, USER_STATUS_OCC_ENABLED);

	// User Code 등록 Event 전송

#ifdef 	DDL_CFG_RFID
	AllCardDelete();
#endif 

#ifdef	DDL_CFG_FP
#ifdef	DDL_CFG_DIMMER				//디밍 없는 지문 모델과 지문 없는 지문 모델의 모드 변경시 Pin Display를 구분 하기 위해 추가 2019년06월25일 sjc
		gbModeChangeFlag4Finger = 1;	//디밍 없는 지문 모델과 지문 없는 지문 모델의 모드 변경시 Pin Display를 구분 하기 위해 추가 2019년06월25일 sjc
#else								//디밍 없는 지문 모델과 지문 없는 지문 모델의 모드 변경시 Pin Display를 구분 하기 위해 추가 2019년06월25일 sjc
		gbModeChangeFlag4Finger = 2;	//디밍 없는 지문 모델과 지문 없는 지문 모델의 모드 변경시 Pin Display를 구분 하기 위해 추가 2019년06월25일 sjc
#endif								//디밍 없는 지문 모델과 지문 없는 지문 모델의 모드 변경시 Pin Display를 구분 하기 위해 추가 2019년06월25일 sjc

	ModeGotoAdvancedAllFingerDelete1();
#endif 

#ifdef	DDL_CFG_IBUTTON
	AllGMTouchKeyDelete();
#endif 

#ifdef	DDL_CFG_DS1972_IBUTTON
	AllDS1972TouchKeyDelete();
#endif	

	TamperCountClear();

#ifdef	BLE_N_SUPPORT
	if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)
	{
		// 일반 모드의 값이 0이 아닌 0xBB로 회신해야 해서, 해당 내용을 Converting하는 함수 추가하여 처리
//		BLENTxModeChangeSubEventSend(GetCurrentOperationMode()); 
	}
#endif
}	



void ProcessRegisterPinCodeInputStep(void)
{
	switch(PincodeVerify(0))
	{
		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			memcpy(gPinCodeTmpBuf, gPinInputKeyFromBeginBuf, 8);
			
			if(gbManageMode == _AD_MODE_SET)
			{
//				FeedbackKeyPadLedOn(47, VOL_CHECK);	// Enter the user code to verify, then press the R button to complete
				FeedbackKeyPadLedOn(AVML_REIN_PIN_R, VOL_CHECK);	// Enter the user code to verify, then press the R button to complete
			}
			else
			{
//			   	FeedbackKeyPadLedOn(32, VOL_CHECK);	// Enter the master code to verify, then press the R button to complete
			   	FeedbackKeyPadLedOn(AVML_REIN_MPIN_R, VOL_CHECK);	// Enter the master code to verify, then press the R button to complete
			}
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;				
	}
}




void ProcessRegisterPinCodeConfirmStep(void (*CompletedProcess)(void))
{
	switch(PincodeVerify(0))
	{
		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			if(memcmp(gPinCodeTmpBuf, gPinInputKeyFromBeginBuf, 8) != 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;				
			}
			
			CompletedProcess();
	
#ifdef	DDL_CFG_DIMMER
			FeedbackModeCompletedLedOff(VOICE_MIDI_CANCLE, VOL_CHECK);
	
			SetModeProcessTime(150);
			PrepareDataForDisplayPincode();
			gbModePrcsStep++;
#else
#ifdef	DDL_CFG_FP	//디밍 없는 지문 제품(PANPAN-FC2A-P, PANPAN-F5-P)의 지문 삭제를 위해 추가 2019년06월25일 sjc 추가
			SetModeProcessTime(150);
			//FeedbackModeCompletedLedOff(VOICE_MIDI_CANCLE, VOL_CHECK);
#else
//			FeedbackModeCompleted(1, VOL_CHECK);
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			ModeClear();
#endif			
#endif
			break;				
	}
}



void ModeAdvancedModeSet(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(31, VOL_CHECK);				// Enter a 4 to 10 digits master code, then press the hash to continue		
			FeedbackKeyPadLedOn(AVML_IN_0410MPIN_SHARP, VOL_CHECK);				// Enter a 4 to 10 digits master code, then press the hash to continue		

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;
			
		case 1:
		case 2:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					if(gbModePrcsStep == 1)
					{
						ProcessRegisterPinCodeInputStep();
					}
					else
					{
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
						ModeClear();
					}
					break;

				case FUNKEY_REG:
					if(gbModePrcsStep == 2)
					{
						//등록 버튼음 출력, 등록 버튼음은 FRONT DIMMING LED 출력 대상이 아니라 TENKEY_NONE 처리
						FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, TENKEY_NONE);
						gbModePrcsStep++;
					}
					else
					{
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
						ModeClear();
					}
					break;		

				case TENKEY_STAR:
					ReInputOrModeMove(ModeGotoMenuLockSetting);
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 3:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessRegisterPinCodeConfirmStep(ModeInitialSetChangeToAdvancedMode);
			break;

// PASSWORD DISPLAY RTN
		case 4:
			DisplayRegisteredPincode(PincodeRegisterCompleteProcess);
			break;
			
		default:
			ModeClear();
			break;
	}
}



void ModeNormalModeSet(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(45, VOL_CHECK);				// Enter a 4 to 10 digits usercode, then press the hash to continue	
			FeedbackKeyPadLedOn(AVML_IN_0410PIN_SHARP, VOL_CHECK);				// Enter a 4 to 10 digits usercode, then press the hash to continue	

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;
			
		case 1:
		case 2:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					if(gbModePrcsStep == 1)
					{
						ProcessRegisterPinCodeInputStep();
					}
					else
					{
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
						ModeClear();
					}
					break;

				case FUNKEY_REG:
					if(gbModePrcsStep == 2)
					{
						//등록 버튼음 출력, 등록 버튼음은 FRONT DIMMING LED 출력 대상이 아니라 TENKEY_NONE 처리
						FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, TENKEY_NONE);
						gbModePrcsStep++;
					}
					else
					{
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
						ModeClear();
					}
					break;		

				case TENKEY_STAR:
					ReInputOrModeMove(ModeGotoMenuLockSetting);
					break;
					
				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 3:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessRegisterPinCodeConfirmStep(ModeInitialSetChangeToNormalMode);
			break;

// PASSWORD DISPLAY RTN
		case 4:
			DisplayRegisteredPincode(PincodeRegisterCompleteProcess);
			break;
			
		default:
			ModeClear();
			break;
	}
}



#ifdef P_SW_FACTORY_BROKEN_T
void ChangeFactoryPintoBrokenPinControl(BYTE OnOff)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = FACTORY_RST_BROKEN_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(FACTORY_RST_BROKEN_GPIO_Port, &GPIO_InitStruct);

	P_SW_FACTORY_BROKEN(OnOff);
}
#endif 


BYTE gbFactoryReset = 0;

void FactoryResetSet(BYTE bCode)
{
	gbFactoryReset = bCode;
}


void FactoryResetCheck(void)
{
	WORD wTmp = 2000;
#ifdef P_SW_FACTORY_BROKEN_T
	/* factory reset 과 BROKEN 기능을 같이 쓰는 PIN 일 경우 아래와 같이 동작 한다 */
	/* 아래 delay 는 P_SW_FACTORY_BROKEN_T 핀이 안정화 되는 시간을 준다  */
	/* 팩토리 체크가 완료 되면 PIN 을 output low 로 유지 한다 */
	/* 해당 PIN 이 high 가 되는 경우는 HW 부저 음이 발생 한다 */

	Delay(SYS_TIMER_100MS);	
	Delay(SYS_TIMER_100MS);	
	Delay(SYS_TIMER_100MS);
	Delay(SYS_TIMER_100MS);
	Delay(SYS_TIMER_100MS);
	Delay(SYS_TIMER_100MS);
	Delay(SYS_TIMER_100MS);
	Delay(SYS_TIMER_100MS);
	
	if(!P_SW_FACTORY_BROKEN_T)
#else 
	if(P_SW_FACTORY_T)
#endif 		
	{
		while(--wTmp)
		{
#ifdef P_SW_FACTORY_BROKEN_T
			if(P_SW_FACTORY_BROKEN_T) 
			{
				ChangeFactoryPintoBrokenPinControl(0);
				return;
			}
#else 
			if(!P_SW_FACTORY_T)	return;
#endif 			
		}

		FactoryResetSet(FACTORYRESET_REQ_BY_MANUAL);	// Factory Reset by manual
	}
#ifdef P_SW_FACTORY_BROKEN_T
	else
	{
		ChangeFactoryPintoBrokenPinControl(0);
	}	
#endif 	
}


void FactoryResetRun(void)
{
#ifdef	__DDL_MODEL_PANPAN_FC2A_P
		BYTE bTmp;
#endif

	if((gbFactoryReset != FACTORYRESET_REQ_BY_MANUAL) && (gbFactoryReset != FACTORYRESET_REQ_BY_RF))	return;

#if defined (DDL_CFG_MS) 
	MS_All_Credential_Init();
	//MS_Credential_Info_Init();
	//MS_Exclusive_Functions_Init();
#else 
	MasterCodeDelete();
	OnetimeCodeDelete();
	AllUserCodeDelete();
	
//	AllScheduleStatusClear(CLEAR_ALL_CODE);

#ifdef 	DDL_CFG_RFID
	AllCardDelete();
#endif	

#ifdef 	_MASTER_CARD_SUPPORT
	AllMasterCardDelete();
#endif	

#ifdef 	DDL_CFG_IBUTTON
	AllGMTouchKeyDelete();
#endif	

#ifdef 	DDL_CFG_DS1972_IBUTTON
	AllDS1972TouchKeyDelete();
#endif

#ifdef	DDL_CFG_FP
	 //아래 모드를 변경하는 코드로 인해 위의 JigPowerDownModeEnable의 값을 1로 바꾼 효과는 지문이 모두 지워진 뒤에 발생한다.
	//TCS4K 지문 모듈의 지문을 지우는 것은 MainMode를 변화시켜 처리한다.
	ModeGotoAdvancedAllFingerDelete2();
#endif
#endif 

	LockWorkingModeDefaultSet();

	DDLStatusFlagClear(DDL_STS_OUTLOCK);

#ifdef	__DDL_MODEL_PANPAN_FC2A_P
	bTmp = 0x00;
	RomWrite(&bTmp, (WORD)CYLINDER_KEY_ALARM_SET, 1);
#endif

	TamperCountClear();

#ifndef	P_SNS_LOCK_T	
	InnerForcedLockClear();
#endif
	VolumeSetting(VOL_HIGH);

#ifdef	BLE_N_SUPPORT
//	SetAllLockOutStatus(0x00); //factory 오면 지우게 
#endif

#ifdef DDL_CFG_FACTORY_DEFAULT_SETTING 
	//팩을 꼽을 수 없는 환경일 경우 이 디파인을 살려서 각 모델별 FactoryDefaultSetting 함수를 만들어 쓴다. 
	//초기화 할 방법이 factory 밖에 없을때 사용 
	// FactoryDefaultSetting 함수는 ddl_main.c 에 만들어서 사용 b2c15min 의 ddl_main.c 파일 참고 
	FactoryDefaultSetting();
#endif 

#if defined	(P_SNS_LEFT_RIGHT_T) || defined (LOCK_TYPE_DEADBOLT) || defined (DDL_CFG_Y_MECHA)
	HandingLockAutoRun();
#endif

#if (DDL_CFG_BLE_30_ENABLE >= 0x31)
	SetPackCBARegisterSet(0);
#endif

	if(gbFactoryReset == FACTORYRESET_REQ_BY_MANUAL)
	{
		FactoryResetSet(FACTORYRESET_RUN_BY_MANUAL);
	}
	else if(gbFactoryReset == FACTORYRESET_REQ_BY_RF)
	{
		FactoryResetSet(FACTORYRESET_RUN_BY_RF);
	}

//	JigSetHistroy(MP_FACTORY_HISTORY);
	
}


BYTE FactoryResetProcessedCheck(void)
{
	if(JigModeStatusCheck() == STATUS_FAIL)
	{
		if((gbFactoryReset == FACTORYRESET_DONE_BY_MANUAL) || (gbFactoryReset == FACTORYRESET_DONE_BY_RF))
			return (STATUS_SUCCESS);
	}

	return (STATUS_FAIL);
}

BYTE FactoryResetRunCheck(void) //hyojoon_20160922
{
	if(JigModeStatusCheck() == STATUS_FAIL)
	{
		if((gbFactoryReset == FACTORYRESET_RUN_BY_MANUAL) || (gbFactoryReset == FACTORYRESET_RUN_BY_RF))
			return (STATUS_SUCCESS);
	}

	return (STATUS_FAIL);
}

void FactoryResetProcess(void)
{
	if((gbFactoryReset != FACTORYRESET_RUN_BY_MANUAL) && (gbFactoryReset != FACTORYRESET_RUN_BY_RF))	return;

	if(GetBuzPrcsStep())		return;
	if(GetVoicePrcsStep())		return;
	if(GetLedMode())			return;
	if(GetMainMode())			return;

	// Pack 장착되어 있을 경우 초기화 완료되었는지 추가 확인 필요

//	FeedbackModeCompleted(1, VOL_CHECK);
	FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);

	// Factory Reset Event 전송

	// 제품 자체 단자를 이용한 Factory Reset
	if(gbFactoryReset == FACTORYRESET_RUN_BY_MANUAL)
	{
		PackTx_MakeAlarmPacket(AL_LOCK_RESET_TO_FACT, 0x00, 0x00);
		FactoryResetSet(FACTORYRESET_DONE_BY_MANUAL);
#ifdef P_SW_FACTORY_BROKEN_T
	gbMainMode = MODEWAITFACTORYRESETCOMPLETE;
	gbModePrcsStep = 0;		
	SetModeProcessTime(500);
#endif 
	}
	// RF 모듈의 요청에 의한 Factory Reset
	else if(gbFactoryReset == FACTORYRESET_RUN_BY_RF)
	{
		PackTx_MakeAlarmPacket(AL_LOCK_RESET_TO_FACT, 0x00, 0x01);
		FactoryResetSet(FACTORYRESET_DONE_BY_RF);
	}
#ifdef DDL_CFG_FACTORY_DEFAULT_SETTING 
	FactoryResetClear();
#endif 

		
}



void FactoryResetClear(void)
{
	gbFactoryReset = 0;
}

#if defined (DDL_CFG_MS)
void FactoryResetDoneCheck(void)
{
	/* factory reset 이나 , 인증 수단이 전부 삭제 되었을 경우 모터 닫힘 상태를 만들지 않기 위해 */
	BYTE bTemp = 0x00;
	BYTE bTemp1 = 0x00;
	BYTE bVerifiedType = 0x00;

	FactoryResetSet(FACTORYRESET_DONE_BY_MANUAL);

	for(bTemp = 0 ; bTemp < SETTING_MENU_INDEX ; bTemp++)
	{
		MS_Get_Registered_Credential_Num(bTemp,&bTemp1);
		if(bTemp1 && MS_GetSupported_Credential_Number(bTemp) != 0x00)
		{
			/* 값이 있다는건 인증 수단이 등록 되어 있다는말씀 */
			/* 권한 확인을 위해 bVerifiedType을 채움 */
			bVerifiedType = (bTemp << 4);
			if(MS_GetSupported_Authority(bVerifiedType,MOTOR_OPEN_ALLOW))
			{	/* 인증 수단이 등록이 되어 있고 motor open 권한이 있으면 factory clear */
				FactoryResetClear();
				break;
			}
		}
	}

//	Ms_Credential_Info.bVerifiedType = 0xFF;
	
	if(JigModeStatusCheck() == STATUS_SUCCESS)
	{
		FactoryResetClear();
	}
}
#else 
void FactoryResetDoneCheck(void)
{
#ifdef _USE_IREVO_CRYPTO_
	//ST link 에의해서 memory 가 날아 가는 경우 전부 0x00 으로 들어 가므로 
	// 0xFF , 0x00 을 비교 하되 0x00 은 비밀번호 00 이랑 겹치므로 마지막에 
	// 길이도 같이 본다. 
	BYTE bTmp1[8], bTmp2[8];
	
	gbFactoryReset = 0;
	RomRead(bTmp1, MASTER_CODE, 8);
	RomRead(bTmp2, USER_CODE, 8);

	EncryptDecryptKey(bTmp1, 8);	
	EncryptDecryptKey(bTmp2, 8);	

	if(((bTmp1[0] == 0xFF) && (bTmp2[0] == 0xFF)) ||
		((bTmp1[0] == 0x00 && bTmp1[7] == 0x00) && (bTmp2[0] == 0x00&& bTmp2[7] == 0x00) ) )
	{
		FactoryResetSet(FACTORYRESET_DONE_BY_MANUAL);
	}
#else 	
	BYTE bTmp1, bTmp2 , bTmp3;
	
	gbFactoryReset = 0;
	RomRead(&bTmp1, MASTER_CODE, 1);
	RomRead(&bTmp2, USER_CODE, 1);

	if((bTmp1 == 0xFF) && (bTmp2 == 0xFF))
	{
		FactoryResetSet(FACTORYRESET_DONE_BY_MANUAL);
	}
#endif 	
}
#endif 

#ifdef P_SW_FACTORY_BROKEN_T
void ModeWaitFactoryResetComplete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			// P_SW_FACTORY_BROKEN_T 가 active low 가 때 까지 기다린다 
			if(P_SW_FACTORY_BROKEN_T)
			{
				
				ChangeFactoryPintoBrokenPinControl(0);
				ModeClear();
			}
			else
			{
				if(!GetModeProcessTime())
				{
					SetModeProcessTime(500);
#ifdef	DDL_CFG_DIMMER
					LedGenerate(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_6|LED_KEY_7|LED_KEY_STAR,DIMM_ON_FAST,0);
					LedSetting(gcbWaitLedNextData, 33);
					LedGenerate(LED_KEY_1|LED_KEY_2|LED_KEY_3|LED_KEY_4|LED_KEY_5|LED_KEY_6|LED_KEY_7|LED_KEY_STAR,DIMM_OFF_FAST,0);
#endif
										
#ifdef	DDL_CFG_NO_DIMMER
					LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
					LedSetting(gcbNoDimLedErr4, LED_DISPLAY_OFF);
#endif
				}
			}
		break;

		default:
		break;
	}
}
#endif 
