//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneUserCodeRegDel_T1.c
	@brief	User Code Individual Register/Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUONEUSERCODEREGDEL_T1_INCLUDED
#define __MODEMENUONEUSERCODEREGDEL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoOneUserCodeRegDelCheck(void);
void ModeOneUserCodeRegDelCheck(void);
void ModeOneUserCodeRegister(void);
void ModeOneUserCodeDelete(void);

BYTE ConvertInputKeyToByte(void);
BYTE ConvertByteToInputKey(BYTE bData);
void SelectSlotNumber(void(*SlotNumberProcess)(void), void(*BackwardProcess)(void),BYTE RegDelSelect);

#endif


