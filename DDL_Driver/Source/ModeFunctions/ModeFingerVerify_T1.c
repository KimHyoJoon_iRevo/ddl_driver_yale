#define		_MODE_FINGER_VERITY_C_

//------------------------------------------------------------------------------
/** 	@file		ModeFingerVerfy_T1.c
	@version 0.1.00
	@date	2016.04.26
	@brief	FingerPrint Verify Mode 
	@remark	 등록된 지문 확인 모드
	@see	MainModeProcess_T1.c
	@see	FingerTCS4K.c
	@see	TCS4K_UART.c
	@see	ModeFiverPTOpen.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.26		by Youn
*/
//------------------------------------------------------------------------------

#include "Main.h"



//------------------------------------------------------------------------------
/** 	@brief	FingerPrint Register Mode Start
	@param	None
	@return 	None
	@remark 지문 등록 모드 시작
*/
//------------------------------------------------------------------------------
void	ModeGotoFingerVerify( void )
{
	gbMainMode = MODE_FINGER_VERIFY;
	gbModePrcsStep = 0;
}


//------------------------------------------------------------------------------
/** 	@brief 	Motor Open Complete Process By finger
	@param	None
	@return 	None
	@remark 인증된 지문에 의한 모터 열림 완료된 후 동작 처리 
*/
//------------------------------------------------------------------------------
void MotorOpenCompleteByFinger(void)
{
	BYTE bTmpArray[10];

#ifdef DDL_CFG_YALE_ACCESS_SUPPORT
	if((gfPackTypeCBABle ||gfInnerPackTypeCBABle) && GetPackCBARegisterSet())
	{
		uint32_t tempSlotnumber = (WORD)ConvertFingerprintSlotNumber(gbFID);
		tempSlotnumber = (tempSlotnumber+DDL_CFG_YALE_ACCESS_SUPPORT)-1;
		CopySlotNumberForPack(tempSlotnumber);
	}
	else
		CopySlotNumberForPack((WORD)ConvertFingerprintSlotNumber(gbFID));
#else
	CopySlotNumberForPack((WORD)ConvertFingerprintSlotNumber(gbFID));
#endif 

#ifdef	DDL_CFG_ADD_REMOCON_LINK_FUNCTION
	PackTxRemoconLinkSend(0x02);
#endif

	bTmpArray[0] = CREDENTIALTYPE_FINGERPRINT;
	bTmpArray[1] = GetSlotNumberForPack();
	memset(&bTmpArray[2], 0x00, 8);
	PackTx_MakePacket(EV_USER_CREDENTIAL_UNLOCK, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 10);
	// 지문 열림 정보 전송

	gbModePrcsStep = 11;
}

void OutForcedLockSetByFinger(void)
{
	BYTE bTmpArray[11];

	DDLStatusFlagSet(DDL_STS_OUTLOCK);

	BioModuleOff();
	bTmpArray[0] = 0xFF;		// No Code Data
	bTmpArray[1] = CREDENTIALTYPE_FINGERPRINT;
	bTmpArray[2] = GetSlotNumberForPack();
	memset(&bTmpArray[3], 0xFF, 8);
	PackTx_MakePacket(EV_ARM_REQEST, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 11);
	SetModeProcessTime(100);
	gbModePrcsStep = 16;
}


void ModeFingerVerify(void)
{
	BYTE bTmp;
	
	switch(gbModePrcsStep)
	{
		case 0:
			bTmp = MotorSensorCheck();
			switch(bTmp)
			{
				case SENSOR_OPENLOCK_STATE:
				case SENSOR_CLOSELOCK_STATE:
				case SENSOR_CENTERLOCK_STATE:
				case SENSOR_LOCK_STATE:
				case SENSOR_CLOSECENLOCK_STATE:
					if(AlarmStatusCheck() == STATUS_SUCCESS) // 경보가 있으면 경보 일단 해제 위해 인증 모드로 
					{
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
						FPMotorCoverAction(_OPEN);
						gbModePrcsStep = 250;
#else 				
						gbModePrcsStep++;	
						BioModuleON();
						LedModeRefresh();
						AudioFeedback(VOICE_MIDI_INTO, gcbBuzSta,  VOL_CHECK | MANNER_CHK);

						gbBioTimer10ms = 43;  //42 30 OK 햇음
						BioRetryCnt = 0;
						gModeTimeOutTimer100ms = 70;			//	5sec 에서 7sec로 늘림. 커버동작시 시간 고려하여 늘림. 
						gbfingerCloseDelaycnt = 0;
						if(gfBioCoverIn == 0)
						{
							if((gfBioErr == 1) || (gbBIOErrorCNT >=4)){ 	// 지문 모듈 이상시 비밀 번호 동작 표시 
								LedSetting(gcbLedFMERROR, LED_ERROR_DISPLAY);
							}else
							{

							}
						}
						gbCoverOn=0;
#endif						
					}
					else // 경보가 없으면 내부강제잠금 확인 
					{
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
						if(bTmp == SENSOR_CLOSELOCK_STATE)
						{
							FeedbackLockInOpenAllowState();
						gbModePrcsStep++;	
						BioModuleON();
						LedModeRefresh();

						gbBioTimer10ms = 43;  //42 30 OK 햇음
						BioRetryCnt = 0;
						gModeTimeOutTimer100ms = 70;			//	5sec 에서 7sec로 늘림. 커버동작시 시간 고려하여 늘림. 
						gbfingerCloseDelaycnt = 0;
						if(gfBioCoverIn == 0)
						{
							if((gfBioErr == 1) || (gbBIOErrorCNT >=4)){ 	// 지문 모듈 이상시 비밀 번호 동작 표시 
								LedSetting(gcbLedFMERROR, LED_ERROR_DISPLAY);
							}else{
							}
							
						}
						gbCoverOn=0;
						}
						else 
						{
							PackAlarmReportInnerForcedLockOpenCloseFailSend();
							FeedbackLockIn();
							ModeClear();
						}
#else 
						PackAlarmReportInnerForcedLockOpenCloseFailSend();
						FeedbackLockIn();
						ModeClear();
#endif 
					}
					break;

				case SENSOR_OPEN_STATE:
				case SENSOR_OPENCEN_STATE:
					bTmp = AlarmStatusCheck();
					if(bTmp == STATUS_FAIL) {			//경보발생 상황이 아니면 문 닫힘 동작

						// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
						if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
						{
							FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
							ModeClear();
							break;							
						}
#ifdef	MOTOR_CLOSED_IN_OPEN_STATUS
						if(gfDoorSwOpenState)	//문이 열려있는 경우
						{		
							//FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
							ModeClear();
							break;			
						}
#endif	
						
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
						BatteryCheck();
#else
						FeedbackMotorClose();
#endif
						StartMotorClose();

						gbModePrcsStep = 30;		// wait for motor operatation finished
						break;
					}
					//경보발생 상황이면 지문 인증 절차 진행. "break" 구문 없음에 유의

				default:	
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
					FPMotorCoverAction(_OPEN);
					gbModePrcsStep = 250;
#else				
					gbModePrcsStep++;	
					BioModuleON();
					LedModeRefresh();
					AudioFeedback(VOICE_MIDI_INTO, gcbBuzSta,  VOL_CHECK | MANNER_CHK);

					gbBioTimer10ms = 43;  //42 30 OK 햇음
					BioRetryCnt = 0;
					gModeTimeOutTimer100ms = 70;			//	5sec 에서 7sec로 늘림. 커버동작시 시간 고려하여 늘림. 
					gbfingerCloseDelaycnt = 0;
					if(gfBioCoverIn == 0)
					{
						if((gfBioErr == 1) || (gbBIOErrorCNT >=4)){ 	// 지문 모듈 이상시 비밀 번호 동작 표시 
							LedSetting(gcbLedFMERROR, LED_ERROR_DISPLAY);
						}else{
						}
						
					}
					gbCoverOn=0;
					break;
#endif	
			}
			break;
			
		case 1:
			if(gbBioTimer10ms==41){ 			// 이상 현상 발생시 전원 초기화 
				BioReTry();
			}

			if(gfBioCoverIn && (gbBioTimer10ms==22)){
				gbBioTimer10ms--;
				VoiceSetting(VOICE_MIDI_INTO, VOL_CHECK|MANNER_CHK);
				gbCoverOn = 1;
			}
			if(gbBioTimer10ms==0){// 커버가 열려있고 타임아웃
				//if((gfBioCoverIn)||(!gfCoverSw)){	//sjc 주석 제거
					gbBioTimer10ms = BIO_WAKEUP_TM;
					gbMainMode = MODE_FINGER_PT_OPEN;
					gbModePrcsStep = 0;
					gbFingerPTMode = FINGER_MODE_VERIFY;					
				/*}//sjc 주석 제거
				else if(gfCoverSw){			// 커버가 닫혀있고 타임아웃	//sjc 주석 제거
					BioOffModeClear();//sjc 주석 제거
					AudioFeedback(VOICE_MIDI_ERROR, gcbBuzErr, VOL_CHECK); 	//sjc 주석 제거
					LedSetting(gcbLedOff, LED_DISPLAY_OFF);//sjc 주석 제거
				}*/
			}
			break;

		case 2:		// PTVERIFYLL SQ1
//			AudioFeedback(VOICE_MIDI_INTO, gcbBuzSta, VOL_CHECK|MANNER_CHK);					
			gbSQNum=1;
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTVERIFYALL_SEND();	//모든 지문 비교 
			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep ++;
			break;
		case 3:	// PTVERIFY SQ2
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				gbError = LLSTATUS_WRITE_CHAR_TIMEOUT;				
				break;
			}

			if(gfUART0TxEnd){		//송신 완료 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				gbFingerLongTimeWait100ms = 9;
				gbBIOCONCNT=0;
				break;
			}
			break;
			


		case 4:		// PTVERIFY SQ3 -->// PTVERIFY SQ5 반복  16~18

			if(gbBIOCONCNT==1){
				gbBioTimer10ms = 250;
			}else if(gbBIOCONCNT>1){
				gbBIOCONCNT=3;				//물 묻은 거 감지 
			}

			if(gbBioTimer10ms == 0){
				if( gbBIOCONCNT==3 ){
					gbModePrcsStep = 201;
				}else{
				gbModePrcsStep = 200;
				}	
				break;
			}
			
			if(gModeTimeOutTimer100ms ==20 && gbBIOCONCNT==1 ){
//				FeedbackError(123, VOL_CHECK);	//(음성) Remove dirts from the fingerprint module. 
				FeedbackError(AVML_ERR_FINGER_MSG2, VOL_CHECK);	//(음성) Remove dirts from the fingerprint module. 
				BioModuleOff();
				ModeClear();
				break;
			}


			if(gModeTimeOutTimer100ms  == 0){
				if(gfCoverSw){ 	// 체터링 값..
//					FeedbackError(101, VOL_CHECK);//times up
					FeedbackError(AVML_TIMEOUT, VOL_CHECK);//times up
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE					
					FPMotorCoverAction(_CLOSE);					
#endif
// 20160923  커버 열어둔체로 7초의 timeout이 발생하면 타임아웃음성 출력하고 에러카운트는 증가시키지 않도록 수정
//					TamperCountIncrease();
					BioModuleOff();
					ModeClear();
				}else{
					if(gbBIOErrCheck==0x20){									//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
//						FeedbackError(119, VOL_CHECK);				//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	//(음성) Check the fingerprint module.				정전기 열출격 센서 망가짐 
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK);				//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	//(음성) Check the fingerprint module.				정전기 열출격 센서 망가짐 
					}														//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					else{
// 20160923  커버 열고 닫은 뒤 지문 입력이 없으면 timeout 음성 출력, 입력 에러 카운트 증가시키지 않음
						FeedbackError(AVML_TIMEOUT, VOL_CHECK);//times up
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE					
						FPMotorCoverAction(_CLOSE);					
#endif
					}
					BioModuleOff();
					ModeClear();
				}
				break;
			}
			/*else
			{
				if(gfCoverSw)
				{
					if(gbfingerCloseDelayflag == 2)
					{
						gbfingerCloseDelaycnt = 0;
						gbfingerCloseDelayflag = 0;
						
					}
					else if(gbfingerCloseDelayflag == 0)
					{
						if(gbfingerCloseDelaycnt > 0xA000)	
						{
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//지문이 입력 되지 않았습니다.
							ModeClear();
						}
						else
						{
							gbfingerCloseDelaycnt += 1;
						}
					}
				}
				else
				{
					if(gbfingerCloseDelayflag == 1)	gbfingerCloseDelayflag = 2;
				}
			}*/
			
			switch(gbInputKeyValue){				//
#if 0
				case TENKEY_STAR:
					FeedbackBuzStopLedDimOff();
					BioModuleOff();
					ModeClear();
					break;
#endif
#ifdef TKEY_CIRCLE_BTN_TYPE				//원형 아이콘 사용시 지문인증 캔슬 버튼 동작 
				case TENKEY_STAR:
					FeedbackBuzStopLedOff();
					BioModuleOff();
					ModeClear();
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE					
					FPMotorCoverAction(_CLOSE);					
#endif
					break;
#endif

//				case FUNKEY_OPCLOSE:
				case FUNKEY_REG:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//(음성)잘못 입력 되었습니다.
					BioModuleOff();
					ModeClear();
					break;
			}


			if(gfUART0RxEnd == 1)	{
				gfUART0RxEnd=0;


				pFingerRx = &FingerRx.PACKET.Start_st+1;	//리셋 에크 수신 CMD 부
	
				if((*pFingerRx == 0x05) && (gbErrorCnt<2)){ 	// PTOPEN ACK CMD 재 수신 받을 시 
					gbBioTimer10ms = BIO_WAKEUP_TM;

					// PTVERIFYLL SQ1
					gbSQNum=1;
					pFingerRx = &FingerRx.PACKET.Start_st;
					PTVERIFYALL_SEND(); //모든 지문 비교 
					gbModePrcsStep = 3;

					gbErrorCnt++;
					break;
				}


				pFingerRx = &FingerRx.PACKET.Start_st+7;//CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx == 0xD6){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFB){
//						FeedbackError(124, VOL_CHECK);	//(음성) The fingerprint senser is damaged.
						FeedbackError(AVML_ERR_FINGER_MSG3, VOL_CHECK);	//(음성) The fingerprint senser is damaged.
						BioModuleOff();
						ModeClear();
						gbBIOErrorCNT++;
						break;
					}
				}



				pFingerRx = &FingerRx.PACKET.Start_st+7;//CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx == 0xDF){
					pFingerRx = &FingerRx.PACKET.Start_st+8;//CALLBACK CMD 에크 수신 CMD 부
					if(*pFingerRx == 0xFF){
//						FeedbackError(119, VOL_CHECK);	//(음성) Check the fingerprint module.				정전기 열출격 센서 망가짐 
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK);	//(음성) Check the fingerprint module.				정전기 열출격 센서 망가짐 
						BioModuleOff();
						ModeClear();
						gbBIOErrorCNT++;
						break;
					}
				}


				pFingerRx = &FingerRx.PACKET.Start_st+10;//CALLBACK CMD 에크 수신 CMD 부
	
				if(*pFingerRx != 0x20){
					gbModePrcsStep=100;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+11;	//메세지 준비 CMD 부

				if(*pFingerRx != 0x01){
					gbModePrcsStep=100;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+15;

				bTmp = *pFingerRx;

				if(bTmp == 0){		// 이미지 처리 완료 ..
					gbBioTimer10ms = BIO_WAKEUP_TM;
					gbModePrcsStep = 7;		// PTVERIFY SQ6
					LedSetting(gcbLedOff, LED_DISPLAY_OFF);
					break;
				}else if(bTmp  == 0x20){		//이미지 받앗다..
					gbBioTimer10ms = BIO_WAKEUP_TM;
					gbModePrcsStep++;
					gModeTimeOutTimer100ms = 30;//15;	//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
					gbBIOErrCheck = bTmp;
					AudioFeedback(VOICE_MIDI_FINGER,	gcbBuzNum, VOL_CHECK|MANNER_CHK);//(음성) 버튼음					
					break;
				}else if(bTmp == 0x0c){
					gbModePrcsStep++;
					if(!gfCoverSw&&gfBioCoverDown){		//  지문 입력 없이 커버 닫은 경우.
						FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	
						BioModuleOff();
						ModeClear();
						break;
					}

					if(gbFingerLongTimeWait100ms >= 6){
						LedSetting(gcbLedFM3, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms >= 3){
						LedSetting(gcbLedFM2, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms > 0){ //else if(gbFingerLongTimeWait100ms >= 0){	//ST는 0>=을 인정 하지 않음
						LedSetting(gcbLedFM, LED_KEEP_DISPLAY);
					}else if(gbFingerLongTimeWait100ms == 0){
						gbFingerLongTimeWait100ms = 9;
					}
					break;
				}else {
					gbModePrcsStep=100;
					break;
				}
		
//				gbBioTimer10ms = BIO_WAKEUP_TM;
//				gbModePrcsStep++;
				
				break;
			}

			break;


		case 5:		// PTVERIFY SQ4
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}

			gbBIOCONCNT++;
			
			if(gfBioUARTST==1){
				AudioFeedback(VOICE_MIDI_ERROR, gcbBuzSysErr4, VOL_HIGH);//잘못입력되엇습니다.
				gbModePrcsStep = 101;
				break;
			}
			
			gbSQNum = gbSQNum + 1;	// 전송 SQ 1 증가 
			// Call back contin....
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTCON_SEND();			// Call back contin....CMD 전송 

			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;

		case 6:	// PTVERIFY SQ5 --> // PTVERIFY SQ3
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}

				gbBIOErrorCNT=0;		// 지문 장치 정상 동작 
				gfBioErr=0;

			if(gfBioUARTST==1){
				AudioFeedback(VOICE_MIDI_ERROR, gcbBuzSysErr4, VOL_HIGH);//잘못입력 되엇습니다 
				gbModePrcsStep = 101;
				break;
			}

			if(gfUART0TxEnd){			// Call back contin....CMD 전송 	완료 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep = 4;		// call back 수신 위해 case 4
				break;
			}
			break;
		

		case 7:			// PTVERIFY SQ6
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}

			gbSQNum = gbSQNum + 1;	// 이미지 비교 완료 되어 ... 결과  받기 위해 Con 명령어 보냄..
			pFingerRx = &FingerRx.PACKET.Start_st;
			PTCON_SEND();
			gfBioUARTST=0;


			gbBioTimer10ms = BIO_WAKEUP_TM;
			gbModePrcsStep++;
			break;

		case 8:		// PTVERIFY SQ7
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}


			if(gfUART0TxEnd){			//송신 완료 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep = 9;

			}
			break;

		case 9:		// verify 마지막 부분 (일반모드 , 보안모드	)
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}

			if(gfUART0RxEnd == 1)	{		// 수신 완료 
				PCErrorClearSet();
				
				gfUART0RxEnd=0;

				pFingerRx = &FingerRx.PACKET.Start_st+9;

//				if(gbSecurity==1){	// 보안 모드 완료 명령어 1
//					if(*pFingerRx != 0x03){
//						gbModePrcsStep=100;
//						break;
//					}
//				}else{				// 일반  모드 완료 명령어 1
					if(*pFingerRx != 0x0A){
						gbModePrcsStep=100;
						break;
					}
//				}
				pFingerRx = &FingerRx.PACKET.Start_st+10;

				if(*pFingerRx != 0x12){ // 일반 & 보안 모드 완료 명령어 2
					gbModePrcsStep=100;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+11;

//				if(gbSecurity==1){		//	매칭 시 1  비매칭 시 0
//					if(!(*pFingerRx)){
//						gbModePrcsStep=100;
//						break;
//					}	
//				}else{					//	일반 모드는 ID가 나타남...
					gbFID = *pFingerRx;
//				}

				if(gbJigTimer1s){
					memset(gbJigInputDataBuf, 0xFF, 9);
					gbJigInputDataBuf[0] = 0x00;
					gbJigInputDataBuf[1] = 0x01;
					gbJigInputDataBuf[2] = 0x01;				
				}

				if(gbFID == 0xFF){
					gbModePrcsStep=100;
					break;
				}
				
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gModeTimeOutTimer100ms  = 100;
//				BioPowerOff();
//				P_BIO_VCC(0);
				BioModuleOff();


//				if(gbSecurity==0){
					gbLonOnID[0]=0;
					bTmp = (gbFID+1)/10;
					bTmp=bTmp<<4;
					bTmp=bTmp +(gbFID+1)%10;
					gbLonOnID[1]=bTmp;
					gbLonOnID[2]=0;
					gbLonOnID[3]=0; 				
//				}
//				else{
//					RomRead(gbLonOnID, (WORD)BIO_ID+(2*gbFID), 2);
//				}

#ifdef	BLE_N_SUPPORT
				/* all lock out check PIN 을 제외한 나머지는 slotnumber 를 0xFF 로 */ 
				if(AllLockOutStatusCheck(0xFF))
				{
					FeedbackAllCodeLockOut();

					ModeClear();
					break;
				}
#endif

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
				{
					if(ScheduleEnableCheck_Ble30(CREDENTIALTYPE_FINGERPRINT, (ConvertFingerprintSlotNumber(gbFID)-1)) != 0)
					{
						gbModePrcsStep = 251;
						break;
					}
				}
#endif	// DDL_CFG_BLE_30_ENABLE
				bTmp = AlarmStatusCheck();
				if(bTmp == STATUS_SUCCESS)
				{
					AlarmGotoAlarmClear();
					ModeClear();
				}
				else
				{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorOpen();
#endif
					StartMotorOpen();		//모터 open
					gbModePrcsStep = 10; 	//모터 OPEN 상태 확인
				}
				DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
				TamperCountClear();
				break;
			}

			break;
	
		case 10:
			MotorOpenCompleteCheck(MotorOpenCompleteByFinger,31);
			break;

		case 11:
#if 0			
			if(gModeTimeOutTimer100ms  == 0){
				BioOffModeClear();
			}
#endif			
			if(GetLedMode() || GetBuzPrcsStep()) break;
			gModeTimeOutTimer100ms  = 20;		//2sec
//			FeedbackKeySharpOn( 0, VOL_CHECK );			// 음성 출력 안함
			FeedbackKeySharpOn( AVML_STOP_VOICE, VOL_CHECK );			// 음성 출력 안함
			gbModePrcsStep = 12;		//FINGER_OUTLOCK_CKECK;	
			break;
			
		case 12:		//FINGER_OUTLOCK_CKECK:

//지문 인증으로 외부 강제 잠금 설정을 위해 #이 켜있는 동안 문이 열리고 닫히면, 바로 자동잠금이 실행되지 않는 증상을 해결하기 위해
#ifdef	P_SNS_EDGE_T			// Edge sensor
			if(gfXORDoorSw) {
				LedSetting(gcbLedOff, LED_DISPLAY_OFF);
				BioOffModeClear();
				break;
			}
#endif

			if( (gbNewKeyBuffer == TENKEY_SHARP))
			{
				//OPEN led 동작 중에 중에 SHARP 키를 눌렀을 경우.
				gbModePrcsStep++;
			}
/***
			else if(gbLockStatusPrcsStep == LOCKSTS_AUTOLOCK_CLOSE){
				LedSetting(gcbLedOff, LED_DISPLAY_OFF);
				gbMainMode = 0;
				gbModePrcsStep = 0;
				BioModuleOff();
			}
***/
			else if(gModeTimeOutTimer100ms  == 0)
			{
				LedSetting(gcbLedOff, LED_DISPLAY_OFF);
				BioOffModeClear();
			}
			break;
		
		case 13:
			if(GetBuzPrcsStep() || GetVoicePrcsStep())		break;			//부저음/음성 출력중 외부 강제 잠금 시간 증가 금지
			gbFingerLongTimeWait100ms = 24; 				//외부 강제 잠금 모드 진입 2초 대기
			gbModePrcsStep++;

//20160923 외부강제잠금 시도도 인증된 것이므로 인증오류카운트도 초기화 시킨다.
			TamperCountClear();
			break;

		case 14:
			if(gbFingerLongTimeWait100ms == 17)
			{
				gbFingerLongTimeWait100ms--;					//100ms동안 같은 값을 가지므로 반복하여 들어오는
											//	것을 방지하기 위해 인위적으로 줄임
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);//(음성) 버튼음2											
				LedSetting(gcbLedOutLock1, LED_KEEP_DISPLAY);
			}
			if(gbFingerLongTimeWait100ms == 11)
			{
				gbFingerLongTimeWait100ms--;					//100ms동안 같은 값을 가지므로 반복하여 들어오는
											//	것을 방지하기 위해 인위적으로 줄임
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);//(음성) 버튼음											
				LedSetting(gcbLedOutLock2, LED_KEEP_DISPLAY);
			}
			if(gbFingerLongTimeWait100ms == 5)
			{
				gbFingerLongTimeWait100ms--;					//100ms동안 같은 값을 가지므로 반복하여 들어오는
											//	것을 방지하기 위해 인위적으로 줄임
				AudioFeedback(VOICE_MIDI_BUTTON,	gcbBuzNum, VOL_CHECK|MANNER_CHK);//(음성) 버튼음											
				LedSetting(gcbLedOutLock3, LED_KEEP_DISPLAY);
			}
			if(gbFingerLongTimeWait100ms == 0){ 				//외부강제잠금
				LedSetting(gcbLedOutLock4, LED_KEEP_DISPLAY);
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorClose();
#endif				
				StartMotorClose();
//				gfOutLockReq = 1;
				gbModePrcsStep++;
				break;
			}

			if(!(gbNewKeyBuffer == TENKEY_SHARP) )
			{
				LedSetting(gcbLedOff, 0);
				BioOffModeClear();
			}
			break;
			
		case 15:
			//외부강제잠금
			MotorCloseCompleteCheck(OutForcedLockSetByFinger,31);
			break;
		
		case 16: 	
			// 외부강제잠금에 의한 잠김 신호가 없어 아래 신호 추가 전송
			if(GetModeProcessTime()== 0)
			{
				PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);
				ModeClear();
			}
			break;
#if 0
		case 19:
			if(GetLedMode())	break;
			
			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			BioOffModeClear();
			break;

		case 20:
			if(GetLedMode() == 0)
			{
				LedSetting(gcbLedDimmOn, LED_KEEP_DISPLAY);
				AudioFeedback(VOICE_MIDI_INTO, gcbBuzSta, VOL_CHECK|MANNER_CHK);
				gbMainMode = MODE_PIN_VERIFY;
				gfBioCoverDown = 0;
				gbModePrcsStep = MODEPRCS_VERIFY_TENKEY_START;		///MODEPRCS_PIN_VERIFY;
			}
			break;
#endif 
			
		case 30:
#if 0
			if(GetMotorPrcsStep())break; //180917 moonsw 백턴기다리고 피드백은 너무 늦어서 해당 조건 삭제함
#endif
			bTmp = GetFinalMotorStatus();

			switch( bTmp ) {
				case	FINAL_MOTOR_STATE_OPEN_ING:
				case	FINAL_MOTOR_STATE_CLOSE_ING:
					break;

				case	FINAL_MOTOR_STATE_OPEN_ERROR:
					gbModePrcsStep++;
					
					FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);

					// Pack Module로 Event 내용 전송 
					PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x01);
					break;
			
				case	FINAL_MOTOR_STATE_CLOSE_ERROR:
					gbModePrcsStep++;

					FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
			
					// Pack Module로 Event 내용 전송 
					PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x00);
					break;

				case	FINAL_MOTOR_STATE_OPEN:
					// 지문 인증 과정에서 해당 부분으로 Pack으로 Event 전송할 경우 없음.
					BioOffModeClear();
					break;


				case	FINAL_MOTOR_STATE_CLOSE:
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					FeedbackMotorClose();
#endif
					// Manual Lock Event 전송
					PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);
					BioOffModeClear();
					break;
			}
			break;

		case 31:
			if(GetLedMode())		break;

			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			else if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}
			BioOffModeClear();
			break;

		case 100:					// 지문 fail 부분///
				TamperCountIncrease();
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				BioOffModeClear();
			break;
	
		case 101:					// 지문 fail 부분///
				BioModuleOff();
				TamperCountIncrease();
//				gfLedFinger = 1;
				ModeClear();
				LedSetting(gcbLedOff, LED_DISPLAY_OFF);
			break;
		case 105:
			if(GetBuzPrcsStep()) break;
			if(GetVoicePrcsStep()) break;	
			gbModePrcsStep = 0;
			break;
	
		case 200:
//			FeedbackError(118, VOL_CHECK);//	The Finger print module is not working properly.
			FeedbackError(AVML_MALFUNCTION_FPM_MSG, VOL_CHECK);//	The Finger print module is not working properly.
			BioOffModeClear();
			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}	

			if(gbJigTimer1s){
				memset(gbJigInputDataBuf, 0xFF, 9);
				gbJigInputDataBuf[0] = 0x00;
				gbJigInputDataBuf[1] = 0x01;
				gbJigInputDataBuf[2] = 0x00;				
			}

			break;

		case 201:
			if(GetBuzPrcsStep()) break;
			if(GetVoicePrcsStep()) break;
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//(음성)잘못 입력 되었습니다.
			BioOffModeClear();
			break;

#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
		case 250:	//지문 모듈 커버 모터가 다열릴때 까지 기다리고 지문 모듈 부팅을 한다.  
			if(gbFingerMode == 0)
			{
				gbModePrcsStep = 1;	
				BioModuleON();
				LedModeRefresh();
				AudioFeedback(VOICE_MIDI_INTO, gcbBuzSta,  VOL_CHECK | MANNER_CHK);
				
				gbBioTimer10ms = 43;  //42 30 OK 햇음
				BioRetryCnt = 0;
				gModeTimeOutTimer100ms = 70;			//	5sec 에서 7sec로 늘림. 커버동작시 시간 고려하여 늘림. 
				gbfingerCloseDelaycnt = 0;
				if(gfBioCoverIn == 0)
				{
					if((gfBioErr == 1) || (gbBIOErrorCNT >=4)){ 	// 지문 모듈 이상시 비밀 번호 동작 표시 
						LedSetting(gcbLedFMERROR, LED_ERROR_DISPLAY);
					}else
					{
				
					}
				}
				gbCoverOn=0;
			}
			break;
#endif


#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
		case 251:
			TimeDataClear();

			PackTx_MakePacket(F0_GET_TIME_AND_DATE, ES_LOCK_EVENT, &gbComCnt, &bTmp, 0);

			// Time Data가 1초 내에 회신되지 않으면 에러 처리
			gModeTimeOutTimer100ms = 10;

			gbModePrcsStep++;
			break;

		case 252:
			if(IsGetTimeDataCompleted() == true)
			{
				gbModePrcsStep++;
			}
			else if(gModeTimeOutTimer100ms == 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				BioOffModeClear();
			}
			break;			

		case 253:
			if(ScheduleVerify_Ble30(CREDENTIALTYPE_FINGERPRINT, gbTimeBuff_Ble30, ConvertFingerprintSlotNumber(gbFID)) == 1)
			{
				bTmp = AlarmStatusCheck();
				if(bTmp == STATUS_SUCCESS)
				{
					AlarmGotoAlarmClear();
					BioOffModeClear();
				}
				else
				{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorOpen();
#endif
					StartMotorOpen();		//모터 open
					gbModePrcsStep = 10;	//모터 OPEN 상태 확인
				}
				DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
				TamperCountClear();
			}
			else
			{
				TamperCountIncrease();

				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				BioOffModeClear();
			}
			break;
#endif	// DDL_CFG_BLE_30_ENABLE

		default :
			BioOffModeClear();
			break;

	}

}



BYTE ConvertFingerprintSlotNumber(BYTE ConvertingData)
{
	BYTE bTmp1 = 0;
	BYTE bTmp2 = 0;
	BYTE bDataBuf[2];	

	// 지문 열림 정보 전송 TCS4k advanced mode 에서 BCD type 으로 롬에 저장 
	// BCD -> hex 변환 함수가 있긴 한데... 설정 할 변수들이 있어서 여기서 그냥 변환 
	RomRead(bDataBuf, (WORD)BIO_ID+(2*ConvertingData), 2);
	
	if(gbManageMode == 0) //Normal mode 2017.09.01. moonsungwoo
	{
		bTmp2 = bDataBuf[1];
	}
	else //((gbManageMode = _AD_MODE_SET)
	{
		bTmp2 = (bDataBuf[0] >> 4) & 0x0F;
		bTmp1 = bDataBuf[0] & 0x0F;
		bTmp2 = (bTmp2 * 10) + bTmp1;
	}

	return (bTmp2);
}




