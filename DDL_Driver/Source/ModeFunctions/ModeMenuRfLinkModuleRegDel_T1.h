//------------------------------------------------------------------------------
/** 	@file		ModeMenuRfLinkModuleRegDel_T1.h
	@brief	RF Link Module and ZWave&Zigbee Module Register/Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENURFLINKMODULEREGDEL_T1_INCLUDED
#define __MODEMENURFLINKMODULEREGDEL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoRfLinkModuleRegDelCheck(void);
void ModeRfLinkModuleRegDelCheck(void);
void ModeRfLinkModuleRegister(void);
void ModeRfLinkModuleDelete(void);



#endif


