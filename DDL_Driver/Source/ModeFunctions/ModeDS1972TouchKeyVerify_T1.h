//------------------------------------------------------------------------------
/** 	@file		ModeDS1972TouchKeyVerify_T1.c
	@brief	DS1972 TouchKey Verify Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODEGMTOUCHKEYVERIFY_T1_INCLUDED
#define __MODEGMTOUCHKEYVERIFY_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


void ModeGotoDS1972TouchKeyVerify(void);
void ModeDS1972TouchKeyVerify(void);


BYTE DS1972TouchKeyVerify(void);


#endif


