//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneCardRegDel_T1.c
	@version 0.1.00
	@date	2016.05.17
	@brief	Card Individual Register/Delete Mode
	@remark	카드 개별 등록/삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@see	ModeMenuOneUserCodeRegDel_T1.c
	@see	ModeCardVerify_T1.h"
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.05.17		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gOneCardUidBuf[MAX_CARD_UID_SIZE];


void ModeGotoOneCardRegDelCheck(void)
{
	gbMainMode = MODE_MENU_ONECARD_REGDEL;
	gbModePrcsStep = 0;
}


void ModeGotoOneCardRegister(void)
{
	gbMainMode = MODE_MENU_ONECARD_REGISTER;
	gbModePrcsStep = 0;
}

void ModeGotoOneCardDelete(void)
{
	gbMainMode = MODE_MENU_ONECARD_DELETE;
	gbModePrcsStep = 0;
}


void OneCardModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// 카드 개별 등록
		case 1: 	
			ModeGotoOneCardRegister();
			break;
	
		// 카드 개별 삭제
		case 3: 		
			ModeGotoOneCardDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuOneCardModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 카드 개별 등록
		case TENKEY_1:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_CARD_SHARP, gbInputKeyValue);	
			break;

		// 카드 개별 삭제
		case TENKEY_3:		
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_CARD_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_SHARP:
			OneCardModeSetSelected();
			break;
									
		case TENKEY_STAR:
			if(GetSupportCredentialCount() > 1)
				ModeGotoMenuAdvancedCredentialSelect();
			else 
				ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}




void ModeOneCardRegDelCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			Feedback13MenuOn(AVML_REGCARD1_DELCARD3, VOL_CHECK);
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuOneCardModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}
}




void CardSlotNumberToRegister(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();
#if (SUPPORTED_USERCARD_NUMBER == 100)	
	if(gSelectedSlotNumber ==0x00) gSelectedSlotNumber = 100;
#endif 
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= SUPPORTED_USERCARD_NUMBER))
	{
		FeedbackKeyPadLedOn(AVML_ENTER_CARD_SHARP, VOL_CHECK); 			
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		gbModePrcsStep++;
		CardGotoReadStart();
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}

#ifdef	BLE_N_SUPPORT
void CardSlotNumberToRegisterByBleN(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();

#if (SUPPORTED_USERCARD_NUMBER == 100)	
	if(gSelectedSlotNumber ==0x00) gSelectedSlotNumber = 100;
#endif 
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= SUPPORTED_USERCARD_NUMBER))
	{
		FeedbackKeyPadLedOn(VOICE_MIDI_INTO, VOL_CHECK);
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		gbModePrcsStep++;
		CardGotoReadStart();
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}
#endif 

void OneCardInputCheckForRegister(void)
{
	switch(gbInputKeyValue)
	{
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		case TENKEY_STAR:
			gbModePrcsStep = 0;
			break;

		default:
			if(GetCardReadStatus() == CARDREAD_SUCCESS)
			{
				gbModePrcsStep++;
				break;
			}

			CardReadStatusClear();
			TimeExpiredCheck();
			break;
	}
}

void OneCardInputCheckForRegisterByBleN(void)
{
	switch(gbInputKeyValue)
	{
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		default:
			if(GetCardReadStatus() == CARDREAD_SUCCESS)
			{
				gbModePrcsStep++;
				break;
			}

			CardReadStatusClear();
			TimeExpiredCheck();
			break;
	}
}


//------------------------------------------------------------------------------
/** 	@brief	Input Cards Temporary save Process
	@param	None
	@return 	None
	@remark 전체 카드 등록 모드에서 한번에 입력된 카드를 비교, 임시 저장 처리
	@remark 한번에 여러 장의 유효한 카드가 입력되었을 경우 차례로 부저 처리하기 위해 아래와 같이 구현
*/
//------------------------------------------------------------------------------
void InputOneCardProcessLoop(void)
{
	BYTE bTmp;

	// 카드는 처음 입력된 카드 1장만 처리
	bTmp = CardVerify(1);
	if((bTmp == _DATA_NOT_IDENTIFIED) || (bTmp == gSelectedSlotNumber))
	{
		memcpy(gOneCardUidBuf, gCardAllUidBuf[0], MAX_CARD_UID_SIZE);
	
		FeedbackCredentialInput(VOICE_MIDI_BUTTON, gSelectedSlotNumber);

		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

		gbModePrcsStep++;

		CardGotoReadStop();
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_ALREADY_USE_CARD, VOL_CHECK);		// The card key is already used.

		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

		// 입력한 카드가 이미 등록한 카드와 중복될 경우 다시 카드 입력 대기 
		gbModePrcsStep = 10;
	}	
}



void InputOneCardProcessLoopByBleN(void)
{
	BYTE bTmp;

	// 카드는 처음 입력된 카드 1장만 처리
	bTmp = CardVerify(1);
	if((bTmp == _DATA_NOT_IDENTIFIED) || (bTmp == gSelectedSlotNumber))
	{
		memcpy(gOneCardUidBuf, gCardAllUidBuf[0], MAX_CARD_UID_SIZE);
	
		FeedbackCredentialInput(VOICE_MIDI_BUTTON, gSelectedSlotNumber);

		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

		gbModePrcsStep++;

		CardGotoReadStop();
	}
	else
	{
		PackTx_MakeAlarmPacket(AL_DUPLICATE_RFID_ERROR, 0x00, bTmp);//moonsw
		FeedbackError(AVML_ALREADY_USE_CARD, VOL_CHECK);		// The card key is already used.

		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

		// 입력한 카드가 이미 등록한 카드와 중복될 경우 다시 카드 입력 대기 
		//gbModePrcsStep = 10;
		// 입력한 카드가 이미 등록한 카드와 중복될 경우 카드 입력 대기 종료 
		gbModePrcsStep = 30;
	}	
}


void ProcessRegisterOneCard(void)
{
	switch(gbInputKeyValue)
	{
		case TENKEY_SHARP:
			//# 버튼음 출력
#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;			

		case TENKEY_STAR:
			ReInputOrModeMove(ModeGotoOneCardRegister);
			break;
		
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				
	
		default:
			TimeExpiredCheck();
			break;
	}
}



void BackwardFunctionForOneCardRegDel(void)
{
	ReInputOrModeMove(ModeGotoOneCardRegDelCheck);
}



void RegisterOneCard(void)
{
	// Slot Number는 1부터 시작, 저장 번지 Index는 0부터 시작
#if defined (_USE_IREVO_CRYPTO_)
	EncryptDecryptKey(gOneCardUidBuf,8);
#endif 	
	RomWrite(gOneCardUidBuf, CARD_UID+(((WORD)gSelectedSlotNumber-1)*MAX_CARD_UID_SIZE), MAX_CARD_UID_SIZE);
	
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish

	TenKeyVariablesClear();
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

	gbModePrcsStep++;

	PackTxEventCredentialAdded(CREDENTIALTYPE_CARD, (WORD)gSelectedSlotNumber);

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	ScheduleDelete_Ble30(CREDENTIALTYPE_CARD, gSelectedSlotNumber);
#endif
}

void RegisterOneCardBleN(void)
{
	// Slot Number는 1부터 시작, 저장 번지 Index는 0부터 시작
#if defined (_USE_IREVO_CRYPTO_)
	EncryptDecryptKey(gOneCardUidBuf,8);
#endif 	
	RomWrite(gOneCardUidBuf, CARD_UID+(((WORD)gSelectedSlotNumber-1)*MAX_CARD_UID_SIZE), MAX_CARD_UID_SIZE);
	
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish

	TenKeyVariablesClear();
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

	gbModePrcsStep++;
#ifdef DDL_CFG_YALE_ACCESS_SUPPORT
	if(gfPackTypeCBABle||(gfInnerPackTypeCBABle) && GetPackCBARegisterSet())
	{
		PackTxEventCredentialAdded(CREDENTIALTYPE_CARD, (WORD)((gSelectedSlotNumber+DDL_CFG_YALE_ACCESS_SUPPORT)-1));
	}
	else
	{
		PackTxEventCredentialAdded(CREDENTIALTYPE_CARD, (WORD)gSelectedSlotNumber);
	}
#else
	PackTxEventCredentialAdded(CREDENTIALTYPE_CARD, (WORD)gSelectedSlotNumber);
#endif 

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	ScheduleDelete_Ble30(CREDENTIALTYPE_CARD, gSelectedSlotNumber);
#endif
}


void ModeOneCardRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			//enter the number of the user to register the user number not the code, then press the hash to continue.

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(CardSlotNumberToRegister, BackwardFunctionForOneCardRegDel,1);
			break;

		case 2:
			OneCardInputCheckForRegister();
			break;
		
		case 3:
			InputOneCardProcessLoop();
			break;

		case 4:
			ProcessRegisterOneCard();
			break;

		case 5:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			RegisterOneCard();
			break;

		case 6:
			GotoPreviousOrComplete(ModeGotoOneCardRegister);
			break;
		
		case 10:
			if(GetCardReadStatus()== CARDREAD_NO_CARD)
			{
				// 입력한 카드가 이미 등록한 카드와 중복될 경우 다시 카드 입력 대기 
				gbModePrcsStep = 2;
			}	
			break;

		default:
			ModeClear();
			break;
	}
}





void ProcessDeleteOneCard(void)
{
	memset(gOneCardUidBuf, 0xFF, MAX_CARD_UID_SIZE);
	// Slot Number는 1부터 시작, 저장 번지 Index는 0부터 시작
#if defined (_USE_IREVO_CRYPTO_)
	EncryptDecryptKey(gOneCardUidBuf,8);
#endif 	
	RomWrite(gOneCardUidBuf, CARD_UID+(((WORD)gSelectedSlotNumber-1)*MAX_CARD_UID_SIZE), MAX_CARD_UID_SIZE);

	FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R2, VOL_CHECK);		//completed Press the star for an additional deletion or press the "R" button to finish

	TenKeyVariablesClear();
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	
	gbModePrcsStep++;

	PackTxEventCredentialDeleted(CREDENTIALTYPE_CARD, (WORD)gSelectedSlotNumber);

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	ScheduleDelete_Ble30(CREDENTIALTYPE_CARD, gSelectedSlotNumber);
#endif
}



void CardSlotNumberToDelete(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();
#if (SUPPORTED_USERCARD_NUMBER == 100)	
	if(gSelectedSlotNumber ==0x00) gSelectedSlotNumber = 100;
#endif 	
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= SUPPORTED_USERCARD_NUMBER))
	{						
		//# 버튼음 출력
#ifdef	P_VOICE_RST	
		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
		gbModePrcsStep++;
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}



void ModeOneCardDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_FOR_DEL_SHARP, VOL_CHECK);			//enter the number of the user to delete the user number not the code, then press the hash to continue.

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;

		case 1:
			SelectSlotNumber(CardSlotNumberToDelete, BackwardFunctionForOneCardRegDel,0);
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessDeleteOneCard();
			break;
			
		case 3:
			GotoPreviousOrComplete(ModeGotoOneCardDelete);
			break;

		default:
			ModeClear();
			break;
	}
}





#ifdef	BLE_N_SUPPORT
void ModeGotoOneCardRegisterByBleN(void)
{
	gbMainMode = MODE_MENU_ONECARD_REGISTER_BYBLEN;
	gbModePrcsStep = 0;
}



void ModeOneCardRegisterByBleN(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			gPinInputKeyCnt = 2;
			CardSlotNumberToRegisterByBleN();
			break;

		case 1:
			OneCardInputCheckForRegisterByBleN();
			break;
		
		case 2:
			InputOneCardProcessLoopByBleN();
			break;

		case 3:
			RegisterOneCardBleN();

			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);		//completed

			CardGotoReadStart();
			gbModePrcsStep = 20;
			break;

		case 10:
			if(GetCardReadStatus()== CARDREAD_NO_CARD)
			{
				// 입력한 카드가 이미 등록한 카드와 중복될 경우 다시 카드 입력 대기 
				gbModePrcsStep = 1;
			}	
			break;

		case 20:
			if(GetCardReadStatus()== CARDREAD_NO_CARD)
			{
				ModeClear();
			}	
			break;

		case 30:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ModeClear();
			
			break;


		default:
			ModeClear();
			break;
	}
}
#endif





