
#ifndef __JIGTESTFUNCTIONS_T1_INCLUDED
#define __JIGTESTFUNCTIONS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"

#define	MP_FACTORY_HISTORY			0x01
#define	MP_PC_HISTORY					0x02
#define	MP_N1_HISTORY					0x03 // left right 
#define	MP_N2_HISTORY					0x04 // init

extern BYTE gbJigPrcsMode;
extern BYTE gbJigTimer1s;

extern BYTE gbJigInputDataBuf[9];

extern BYTE gbJigPackMotorControl;




void JigProcessTimeCount(void);
void JigSetHistroy(BYTE bData);
void JigHistroyload(void);
void SetJigModeID(BYTE *pDeviceIdBuf);
void JigModeCheck(void);
BYTE JigModeStatusCheck(void);
void JigDataReceive(void);
void JigDataTransmit(void);
void JigModeEndCheck(void);

void JigInputDataSave(BYTE bMode, BYTE *pData, BYTE bSize);

void JigModeStart(void);


#if 0
#define ES_JIG				0x1E	//생산 지그
//==============================================================================//추가
#define PACK_ID_ERROR				0xFB	//TRX PACK 의 ID가 맞지 않을 경우
#define	MECHANICAL_OPEN				0xFC	//기구적으로 강제로 문을 열었을 경우(수동 레버 포함)
#define	FLOATING_ID_LOST			0xFD	//iButton UID는 일치하나 Floating ID 일치하지 않음
#define	NONE_SUB_DATA				0xFE	//Event Sub Data 없음
#define	TX_ALLID_SEND				0xFF	//전송할 연동 장치 모두
#define	NONE_ADD_DATA				0x00	//전송할 추가 Data 없음	
#endif

#define	TEST_FRAME_STX				0x02
#define	TEST_FRAME_ETX				0x03


#define	TEST_CLASS_MAFA_JIG			0x00	// 생산 지그 동작
#define	TEST_JIG_GET_VERSION		0x00	// 프로그램 버전 확인 2byte
#define	TEST_JIG_SET_SERIAL			0x01	// 시리얼번호 설정 16byte
#define	TEST_JIG_GET_SERIAL			0x02	// 시리얼번호 가져오기  16byte
#define	TEST_JIG_SET_INITIAL		0x03	// 초기화 셋팅
#define	TEST_JIG_GET_INITIAL		0x04	// 초기화 확인 
#define	TEST_JIG_SET_OUTPUT			0x05	// 출력 요청 
#define TEST_JIG_SET_MASTER			0x06	// 마스터 설정  
#define TEST_JIG_GET_MASTER			0x07	// 마스터 확인  
#define TEST_JIG_SET_SLEEP			0x0C	// Sleep 진입
#define TEST_JIG_REQ_LOOPTEST		0x0D	// looptest 요청  
#define	TEST_JIG_REQ_STATUS			0x0E
#define TEST_JIG_REQ_MOTOR			0x0F	// Motor 동작 테스트 모터를 1회 동작시킨다.
#define	TEST_JIG_REQ_RIGHTLEFT		0x10	// 좌수/우수 설정 
#define	TEST_JIG_REQ_MEMACCESS		0x11	// 메모리 접근 가능 번지 요청
#define	TEST_JIG_REQ_MEMREADWRITE	0x12	// 메모리 Read/Write 요청
#define	TEST_JIG_REQ_JIGHISTORY		0x13	// 검사 이력 요청
#define	TEST_JIG_REQ_LOCKFUNCTIONS	0x14	// 도어록 기능 사양 요청	
#define	TEST_JIG_REQ_CMD_BYPASS		0x15	// 명령 전달 요청 

#define	TEST_JIG_SEND_VERSION		0x80	// 프로그램 버전 확인 
#define	TEST_JIG_SEND_SERIAL		0x81	// 시리얼 넘버 확인 
#define	TEST_JIG_SEND_INITAIL		0x82	// 초기값 확인 
#define	TEST_JIG_SEND_INPUT			0x83	// 입력 확인
#define	TEST_JIG_REQ_TEST_PRCS		0x84	// 검사 공정 테스트 요
#define TEST_JIG_ACK_LOOPTEST		0x88	// LoopTest ACK 
#define TEST_JIG_ACK_STATUS			0x89	 
#define TEST_JIG_ACK_MOTOR			0x8A    // Motor 동작 테스트 ACK
#define TEST_JIG_ACK_RIGHTLEFT		0x8B    // 좌수/우수 설정 ACK
#define TEST_JIG_ACK_SET_OUTPUT		0x8E		// 출력 요청 ACK
#define	TEST_JIG_ACK_MEMACCESS		0x8C	// 메모리 접근 가능 번지 확인
#define	TEST_JIG_ACK_MEMREADWRITE	0x8D	// 메모리 Read/Write 결과 확인
#define	TEST_JIG_ACK_JIGHISTORY	0x8F	// 검사 이력 결과 확인
#define	TEST_JIG_ACK_LOCKFUNCTIONS	0x90	// 도어록 기능 사양 확인	
#define	TEST_JIG_ACK_CMD_BYPASS		0x91	// 명령 전달 요청 응답 




BYTE PowerDownModeForJig(void);
void PowerDownModeForJigClear(void);





void LockFunctionsLoad(BYTE *bpData);


#if defined (P_COM_DDL_EN_T) && defined (P_BT_WAKEUP_T)
// CBA BEL 모듈 테스트를 위한 정의
#define	JIG_BYPASS_TO_COM_MODULE		0x01
#define	JIG_BYPASS_TO_INNER_MODULE		0x02

BYTE JigPacket_Bypass_Check(BYTE* bpDesBuf, BYTE* bSrcBuf, BYTE bRxCount, BYTE bInputData);
void JigPacket_Bypass_Process(BYTE bDirection, BYTE* bpData, BYTE bLength);
#endif



#endif



