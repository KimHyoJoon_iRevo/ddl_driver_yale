//------------------------------------------------------------------------------
/** 	@file		ModeMenuHandingLock_T1.c
	@version 0.1.00
	@date	2016.05.20
	@brief	Handing Lock Setting
	@remark	자동/수동 좌수/우수 설정 모드
	@see	ModeMenuMainSelect_T1.c
	@see	ModeMenuLockSetting_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.05.20		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"

BYTE gbRightLock = 0;

void ModeGotoHandingLockSetting(void)
{
	gbMainMode = MODE_MENU_HANDING_LOCK_SET;
	gbModePrcsStep = 0;
}


void HandingLockSave(BYTE HandingLockData)
{
#ifdef	LOCK_TYPE_DEADBOLT
	if((HandingLockData != HANDING_LEFT_AUTO)
		&& (HandingLockData != HANDING_RIGHT_AUTO)
		&& (HandingLockData != HANDING_LEFT_SET)
		&& (HandingLockData != HANDING_RIGHT_SET)
		&& (HandingLockData != HANDING_LOCK_START))
#else
	if((HandingLockData != HANDING_LEFT_AUTO)
		&& (HandingLockData != HANDING_RIGHT_AUTO)
		&& (HandingLockData != HANDING_LEFT_SET)
		&& (HandingLockData != HANDING_RIGHT_SET))
#endif
	{
		// 정상적으로 사용할 수 있는 좌수/우수 값이 아닐 경우 Save 하지 않음.
		return;
	}
	
	if(HandingLockData == HANDING_LEFT_AUTO)
	{
		gbRightLock = HANDING_LEFT_SET;
	}
	else if(HandingLockData == HANDING_RIGHT_AUTO)
	{
		gbRightLock = HANDING_RIGHT_SET;
	}
	else
	{
		gbRightLock = HandingLockData;
	}

	RomWrite(&HandingLockData, RIGHT_LEFT, 1);
}


void HandingLockLoad(void)
{
	gfHandingLockErr = 0;

	RomRead(&gbRightLock, RIGHT_LEFT, 1);
	
	if(gbRightLock == HANDING_LEFT_AUTO)		
	{
		gbRightLock = HANDING_LEFT_SET;
	}
	else if(gbRightLock == HANDING_RIGHT_AUTO)
	{
		gbRightLock = HANDING_RIGHT_SET;
	}
#ifdef	LOCK_TYPE_DEADBOLT
	else if(gbRightLock == HANDING_LOCK_START)
	{
		// 데브볼트 도어록의 자동 좌우수 설정 모드
	}
#endif	
	else if((gbRightLock != HANDING_LEFT_SET) && (gbRightLock != HANDING_RIGHT_SET))	
	{
		// 좌수/우수 설정 에러 처리
		gfHandingLockErr = 1;
	}
}


BYTE GetHandingLock(void)
{
#ifdef	LOCK_TYPE_DEADBOLT
	if((gbRightLock != HANDING_LEFT_SET) 
		&& (gbRightLock != HANDING_RIGHT_SET)
		&& (gbRightLock != HANDING_LOCK_START))
	{
		HandingLockLoad();
	}
#else
	if((gbRightLock != HANDING_LEFT_SET) && (gbRightLock != HANDING_RIGHT_SET))
	{
		HandingLockLoad();
	}
#endif
	return (gbRightLock);
}


void HandingLockDefaultSet(BYTE bSetData)
{
	if((bSetData == 0xFF) || (bSetData == 0x00))
	{
#ifdef	P_SNS_LEFT_RIGHT_T
		HandingLockSave(HANDING_LEFT_AUTO); 
#else
		HandingLockSave(HANDING_LEFT_SET); 
#endif
	}
	else
	{
		HandingLockSave(bSetData);
	}	
}


void HandingLockDefaultToggle(void)
{
#ifdef	P_SNS_LEFT_RIGHT_T
	HandingLockSave(HANDING_LEFT_AUTO); 
#else
	if(gbRightLock == HANDING_LEFT_SET)
	{
		gbRightLock = HANDING_RIGHT_SET;
	}
	else
	{
		gbRightLock = HANDING_LEFT_SET;
	}

	HandingLockSave(gbRightLock); 
#endif
}



#if	defined (P_SNS_LEFT_RIGHT_T)
void HandingLockAutoRun(void)
{
	WORD wTmp = 2000;
	BYTE HandingLockCheckResult;

	// 문이 닫혀 있을 경우에는 자동 좌수/우수 설정하지 않음.
	if(!gfDoorSwOpenState)			return;
		
	while(wTmp)
	{
		if(P_SNS_LEFT_RIGHT_T)
		{
			// 자석이 감지될 경우에는 좌수이다.
			if(HandingLockCheckResult == HANDING_LEFT_AUTO)
			{
				wTmp--;
			}
			else
			{
				HandingLockCheckResult = HANDING_LEFT_AUTO;
				wTmp = 2000;
			}
		}
		else
		{
			// 자석이 감지가 안될 경우는 우수이다.
			if(HandingLockCheckResult == HANDING_RIGHT_AUTO)
			{
				wTmp--;
			}
			else
			{
				HandingLockCheckResult = HANDING_RIGHT_AUTO;
				wTmp = 2000;
			}
		}
	}		


	RomRead(&gbRightLock, RIGHT_LEFT, 1);

	if((gbRightLock == HANDING_LEFT_SET) || (gbRightLock == HANDING_RIGHT_SET))
	{
		//설치자에 의한 좌우수가 셋팅되었으므로 새로 변경하지 않음.
	}
	else if(gbRightLock == HANDING_RIGHT_AUTO)
	{
		//핸들에 의한 우수로 셋팅
		if(gbRightLock == HandingLockCheckResult)
		{
			gbRightLock = HANDING_RIGHT_SET;
		}
		else
		{
			// 저장된 값과 측정 값이 다를 경우 측정 값이 재설정
			HandingLockSave(HANDING_LEFT_AUTO);	
		}
	}
	else if(gbRightLock == HANDING_LEFT_AUTO)
	{
		//핸들에 의한 좌수로 셋팅
		if(gbRightLock == HandingLockCheckResult)
		{
			gbRightLock = HANDING_LEFT_SET;
		}
		else
		{
			// 저장된 값과 측정 값이 다를 경우 측정 값이 재설정
			HandingLockSave(HANDING_RIGHT_AUTO);	
		}
	}
	else
	{
		// 좌수/우수 설정 에러 처리
	
//		VoiceSetting(14, gcbBuzSysErr9, VOL_HIGH);		//(음성)도어락이 정상적으로 동작하지 않습니다.
//		LedSetting(gcbLedErr, 0);
	}
}
#elif	defined (LOCK_TYPE_DEADBOLT)
void HandingLockAutoRun(void)
{
	HandingLockSave(HANDING_LOCK_START);
}

#elif	defined (DDL_CFG_Y_MECHA)
void HandingLockAutoRun(void)
{
	HandingLockSave(HANDING_RIGHT_SET);
}

#endif




void HandingLockSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
#if defined	(P_SNS_LEFT_RIGHT_T) || defined (LOCK_TYPE_DEADBOLT)
		//자동 좌수/우수 설정
		case 1: 	
			HandingLockAutoRun();
			// 좌수/우수 설정 Event 전송

			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);

			gbModePrcsStep++;
			break;
#endif
	
		//수동 좌수 설정 (외부에서 봤을 때 문의 힌지가 왼쪽에 위치)
		case 2: 	
			HandingLockSave(HANDING_LEFT_SET);	
		
			// 좌수 설정 Event 전송
		
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
		
			gbModePrcsStep++;
			break;

		//수동 우수 설정 (외부에서 봤을 때 문의 힌지가 오른쪽에 위치)
		case 3: 		
			HandingLockSave(HANDING_RIGHT_SET);	

			// 우수 설정 Event 전송

			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);

			gbModePrcsStep++;
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 이전 모드로 이동
			gbModePrcsStep--;
			break;
	}			
}



void MenuHandingLockSetSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
#if	defined	(P_SNS_LEFT_RIGHT_T) || defined (LOCK_TYPE_DEADBOLT)
		//자동 좌수/우수 설정
		case TENKEY_1:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_PRESS_SHARP_AUTOHANDLE, gbInputKeyValue);		
			break;
#endif
		//수동 좌수 설정 (외부에서 봤을 때 문의 힌지가 왼쪽에 위치)
		case TENKEY_2:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_LHANDED_MSG, gbInputKeyValue);	
			gbModePrcsStep = 4;			
			break;

		//수동 우수 설정 (외부에서 봤을 때 문의 힌지가 오른쪽에 위치)
		case TENKEY_3:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_RHANDED_MSG, gbInputKeyValue);	
			gbModePrcsStep = 4;
			break;

		case TENKEY_SHARP:
			//# 버튼음 출력
#ifdef	P_VOICE_RST	
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
				FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuLockSetting();
			break;
									
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK);	//	Menu mode has been cancelled.
			ModeClear();
			break;

		default:
			TimeExpiredCheck();
			break;
	}	
}




void ModeHandingLockSetting(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
#if	defined (P_SNS_LEFT_RIGHT_T) || defined (LOCK_TYPE_DEADBOLT) 
			Feedback123MenuOn(AVML_SELECT_MENU, VOL_CHECK);			
#else
			FeedbackSelectedMenuOn(AVML_SELECT_MENU, VOL_CHECK, LED_KEY_2|LED_KEY_3|LED_KEY_STAR|LED_KEY_SHARP);
#endif
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuHandingLockSetSelectProcess();
			break;	

		case 2:
			if(gMenuSelectKeyTempSave != TENKEY_NONE)
			{
				if(GetBuzPrcsStep() || GetVoicePrcsStep()) 	break;
			}

			HandingLockSetSelected();
			break;
			
		case 3:
			GotoPreviousOrComplete(ModeGotoMenuLockSetting);
			break;

		case 4:
#ifdef	P_VOICE_RST			
			if(gbInputKeyValue != TENKEY_NONE)//중간에 음성 끊을 수 있게 
			{
				MenuHandingLockSetSelectProcess();
				if(gbInputKeyValue == TENKEY_SHARP)
					gbModePrcsStep = 2;
				break;
			}
#endif			
			if(GetBuzPrcsStep() || GetVoicePrcsStep())
				break;
			AudioFeedback(AVML_PRESS_SHARP_CONT, gcbBuzWar, VOL_CHECK);
			gbModePrcsStep = 1;
			break;
			
			
		default:
			ModeClear();
			break;
	}

}




#ifdef	LOCK_TYPE_DEADBOLT



BYTE HandingLockProcessStartCheck(void)
{
	if(gbRightLock != HANDING_LOCK_START)		return 0;

	// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
	if(FactoryResetProcessedCheck() == STATUS_SUCCESS)	return 0;
	

	gbMainMode = MODE_MENU_HANDING_LCOK;
	gbModePrcsStep = 0;

	return 1;
}

BYTE HaningLockProcessCheck(void)
{
	if(gbRightLock == HANDING_LOCK_START)		return 1;

	return 0;
}


BYTE CurrentStateCheckForHandingLock(void)
{
	BYTE bRet = 0;

	//좌우수가 설정되어 있지 않은 상태라 MotorSensorCheck 함수를 사용하지 않음.

	P_SNS_EN( 0 );								//Sensor Power On (Acitive Low)
	Delay(SYS_TIMER_1MS);

	if(P_SNS_CLOSE1_T)
	{ 
		HandingLockSave(HANDING_LEFT_SET);

#ifdef	LOCK_TYPE_DEADBOLT
		AdditionalPositionCheckOn();
#endif

		if(gbModePrcsStep)
		{
			_MOTOR_OPEN;
			Delay(SYS_TIMER_20MS);
			_MOTOR_STOP;
			_MOTOR_CLOSE;
			Delay(SYS_TIMER_100MS);
			Delay(SYS_TIMER_20MS);
		}
		_MOTOR_STOP;
		
		SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);

		FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
		ModeClear();
		bRet = 1;
	}

	if(P_SNS_CLOSE2_T)
	{ 
		HandingLockSave(HANDING_RIGHT_SET);

#ifdef	LOCK_TYPE_DEADBOLT
		AdditionalPositionCheckOn();
#endif

		if(gbModePrcsStep)
		{
			_MOTOR_CLOSE;
			Delay(SYS_TIMER_20MS);
			_MOTOR_STOP;
			_MOTOR_OPEN;
			Delay(SYS_TIMER_100MS);
			Delay(SYS_TIMER_20MS);
		}
		_MOTOR_STOP;

		SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);

		FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
		ModeClear();
		bRet = 1;
	}

	P_SNS_EN( 1 );								//Sensor Power Off (Acitive Low)

	return (bRet);
}


void ModeHandingProcess(void)
{
	if(gbRightLock != HANDING_LOCK_START)		return;
	if(CurrentStateCheckForHandingLock())		return;
	
	switch(gbModePrcsStep)
	{
		case 0:
			_MOTOR_CLOSE;
			gbModePrcsStep++;
			SetModeProcessTime(300);
			FeedbackKeyPadLedOnBuzOnly(VOICE_MIDI_INTO, VOL_CHECK); 
			break;

		case 1:
			if(GetModeProcessTime() == 0)
			{
				gbModePrcsStep++;
				SetModeProcessTime(400);
				_MOTOR_STOP;
			}
			break;			

		case 2:
			_MOTOR_OPEN;
			gbModePrcsStep++;
			SetModeProcessTime(400);
			break;	
		
		case 3:
			if(GetModeProcessTime() == 0)
			{
				_MOTOR_STOP;
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();	
				return;
			}
			break;			

		default:
			ModeClear();	
			break;			
	}
}

#endif



