//------------------------------------------------------------------------------
/** 	@file		ModeMenuOnetimeCodeRegDel_T1.h
	@brief	Onetime Code Register/Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUONETIMECODEREGDEL_T1_INCLUDED
#define __MODEMENUONETIMECODEREGDEL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoOnetimeCodeRegDelCheck(void);
void ModeOnetimeCodeRegDelCheck(void);
void ModeOnetimeCodeRegister(void);
void ModeOnetimeCodeDelete(void);

void OnetimeCodeDelete(void);


#endif


