#ifndef		_MODE_MENU_ONEFINGER_REGDEL_H_
#define		_MODE_MENU_ONEFINGER_REGDEL_H_


void		ModeGotoOneFingerRegDelCheck(void);
void		ModeGotoOneFingerRegister(void);
void		ModeGotoOneFingerDelete(void);


void		ModeOneFingerRegDelCheck(void );
void		ModeOneFingerRegister(void);
void		ModeOneFingerDelete(void);

void FingerSlotNumberToRegister(void);
void FingerSlotNumberToDelete(void);

#ifdef	BLE_N_SUPPORT
void ModeGotoOneFingerRegisterByBleN(void);
void ModeGotoOneFingerDeleteByBleN(void);
void ModeOneFingerRegisterByBleN(void);
void ModeOneFingerDeleteByBleN(void);
#endif


#endif
