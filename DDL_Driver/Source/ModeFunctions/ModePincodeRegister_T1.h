//------------------------------------------------------------------------------
/** 	@file		ModePincodeRegister_T1.h
	@brief	Pincode Register Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODEPINCODEREGISTER_T1_INCLUDED
#define __MODEPINCODEREGISTER_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"



void ModeGotoPincodeRegister(void);
void ModePINRegister(void);


void PrepareDataForDisplayPincode(void);
void DisplayRegisteredPincode(void (*CompleteFunction)(void));

void PincodeRegisterCompleteProcess(void);



#endif


