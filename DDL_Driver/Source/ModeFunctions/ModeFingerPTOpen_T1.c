#define		_MODE_FINGER_PTOPEN_T1_C_
//------------------------------------------------------------------------------
/** 	@file		ModeFingerRegister_T1.c
	@version 0.1.00
	@date	2016.04.26
	@brief	FingerPrint Register Mode 
	@remark	 User Code 등록 모드
	@see	MainModeProcess_T1.c
	@see	FingerTCS4K.c
	@see	TCS4K_UART.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.26		by Youn
*/
//------------------------------------------------------------------------------

#include "Main.h"



//------------------------------------------------------------------------------
/** 	@brief	FingerPrint PT Open
	@param	None
	@return 	None
	@remark 지문 등록 모드 시작
*/
//------------------------------------------------------------------------------
void	ModeGotoFingerPTOpen( void )
{
	gbMainMode = MODE_FINGER_PT_OPEN;
	gbModePrcsStep = 0;
}



void ModeFingerPTOpen(void)
{
	switch(gbModePrcsStep)
	{

		case 0: 		//PTOPEN SQ1		// 모두 같이 사용함 .. 지문 관련 모든 
			gbBioTimer10ms = BIO_WAKEUP_TM;
			if(!uart_reset_interface()){	// FM에게 리셋 신호 보냄 
				//RESET을 안 되엇을 ㄷ대
//				AudioFeedback(118, gcbBuzSysErr4, VOL_HIGH); //The Finger print module is not working properly.				.
				AudioFeedback(AVML_MALFUNCTION_FPM_MSG, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.
				gbModePrcsStep = 200;
				break;
			}

			pFingerRx = &FingerRx.PACKET.Start_st; //초기 주소로 
			gbBioTimer10ms = 100;
			gbModePrcsStep++;
			break;
		
		case 1: 	//PTOPEN SQ2
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}
			// FM 리셋 신호 ACK 완료  
			if(!gfUART0RxEnd){
				break;
			}
			
//			SetupUsart3(9600);
//			EnableInterruptUsart3();
//			USART_Cmd(USART3, ENABLE);
			SetupTCS4k_UART();
			FingerRxInterruptRequest();
			
			gfUART0RxEnd=0;
			pFingerRx = &FingerRx.PACKET.Start_st+1;	//리셋 에크 수신 CMD 부
	
			if(*pFingerRx != 0x03){
				gbModePrcsStep = 200;
				break;
			}

			pFingerRx = &FingerRx.PACKET.Start_st+4;	//펌웨어 버전 확인 부 

			if(*pFingerRx != 0x01){
				gbModePrcsStep = 200;
				break;
			}

			gbBioTimer10ms = 1;
			gbBIOErrCheck = 0;	//TCS4K로 인한 추가 부분(YMG는 원래 추가 되어 있었음)	
			gbModePrcsStep = 2;
			break;

		case 2: 	//PTOPEN SQ2
			if(gbBioTimer10ms == 0){			// PTOPEN 잘 안되어서 잠시 뒤로 밀린 것 
				// PTOPEN 
				pFingerRx = &FingerRx.PACKET.Start_st;
				PTOPEN_SEND();							// PTOPEN 전송(파라메터 값 전송 )//

				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;		

		case 3: 	//PTOPEN SQ3
			if(gbBioTimer10ms == 0){
				gbModePrcsStep = 200;
				break;
			}


			if(gfUART0TxEnd){				// PTOPEN 전송 완료 확인 
				gfUART0TxEnd=0;
				pFingerTx = &FingerTx.PACKET.Start_st;	
				gbBioTimer10ms = BIO_WAKEUP_TM;
				gbModePrcsStep++;
				break;
			}
			break;


		case 4: 	//PTOPEN SQ4 END  // 보안 모 드 시.. submode 30으로 이동 ..
			if(gbBioTimer10ms == 0){
				gbModePrcsStep=200;
				break;
			}

			if(gfUART0RxEnd == 1)	{
				gfUART0RxEnd=0;

				pFingerRx = &FingerRx.PACKET.Start_st+1;	//리셋 에크 수신 CMD 부
	
				if(*pFingerRx != 0x05){ 	// PTOPEN ACK CMD
					gbModePrcsStep=200;
					break;
				}

				pFingerRx = &FingerRx.PACKET.Start_st+4;	//펌웨어 버전 확인 부 

				if(*pFingerRx != 0x01){ 	// 펌웨 어 버전 확인 
					gbModePrcsStep=200;
					break;
				}
				gbSQNum = 1;					// 전송 SQ NUM 10 의한 셋팅 .
				gbBioTimer10ms = BIO_WAKEUP_TM;

				if(gbFingerPTMode == FINGER_MODE_ALL_DELETE){
					gbMainMode = MODE_MENU_FINGER_DELETE;
					gbModePrcsStep = 4;
 				}
				else if(gbFingerPTMode == FINGER_MODE_ALL_REGISTER){
					gbMainMode = MODE_MENU_FINGER_REGISTER;
					gbModePrcsStep = 4;
				}
				else if(gbFingerPTMode == FINGER_MODE_VERIFY){
					gbMainMode = MODE_FINGER_VERIFY;
					gbModePrcsStep = 2;					
				}
				else if(gbFingerPTMode == FINGER_MODE_EACH_REGISTER){
					gbMainMode = MODE_MENU_ONEFINGER_REGISTER;
					gbModePrcsStep = 5;
				}
				else if(gbFingerPTMode == FINGER_MODE_EACH_DELETE){
					extern uint8_t flag_overwrite;
					
					if(flag_overwrite){
						gbMainMode = MODE_MENU_ONEFINGER_REGISTER;
						gbModePrcsStep = 53;
						flag_overwrite = 0;
					}
					else {
						gbMainMode = MODE_MENU_ONEFINGER_DELETE;
						gbModePrcsStep = 5;
					}
				}
#ifdef	BLE_N_SUPPORT
				else if(gbFingerPTMode == FINGER_MODE_ALL_DELETE_BYBLEN){
					gbMainMode = MODE_MENU_FINGER_DELETE_BYBLEN;
					gbModePrcsStep = 4;
 				}
				else if(gbFingerPTMode == FINGER_MODE_ALL_REGISTER_BYBLEN){
					gbMainMode = MODE_MENU_FINGER_REGISTER_BYBLEN;
					gbModePrcsStep = 4;
				}
				else if(gbFingerPTMode == FINGER_MODE_EACH_REGISTER_BYBLEN){
					gbMainMode = MODE_MENU_ONEFINGER_REGISTER_BYBLEN;
					gbModePrcsStep = 5;
				}
				else if(gbFingerPTMode == FINGER_MODE_EACH_DELETE_BYBLEN){
					extern uint8_t flag_overwrite;

					if(flag_overwrite){
						gbMainMode = FINGER_MODE_EACH_REGISTER_BYBLEN;
						gbModePrcsStep = 53;
						flag_overwrite = 0;
					}
					else {
						gbMainMode = MODE_MENU_ONEFINGER_DELETE_BYBLEN;
						gbModePrcsStep = 5;
					}
				}
#endif
			}

			break;
		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
//			FeedbackError(118, VOL_CHECK);	//The Finger print module is not working properly.				.
			FeedbackError(AVML_MALFUNCTION_FPM_MSG, VOL_CHECK);	//The Finger print module is not working properly.				.
			BioModuleOff();
			ModeClear();
			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}

			if(gbJigTimer1s){
				memset(gbJigInputDataBuf, 0xFF, 9);
				gbJigInputDataBuf[0] = 0x00;
				gbJigInputDataBuf[1] = 0x00;
				gbJigInputDataBuf[2] = 0x00;				
			}

			break;
		
		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//(음성)잘못 입력 되었습니다.
			BioModuleOff();
			ModeClear();
			break;
	}


}


