#ifndef		_MODE_FINGER_DELETE_H_
#define		_MODE_FINGER_DELETE_H_


#ifdef		_MODE_FINGER_DELETE_C_

	BYTE	gbModeChangeFlag4Finger;

#else

	extern	BYTE	gbModeChangeFlag4Finger;

#endif

void	ModeGotoNormalAllFingerDelete( void );
void	ModeGotoAdvancedAllFingerDelete1( void );
void	ModeGotoAdvancedAllFingerDelete2( void );


void	ModeAllFingerDelete( void );



#ifdef	BLE_N_SUPPORT
void	ModeGotoAllFingerDeleteByBleN( void );
void	ModeAllFingerDeleteByBleN( void );
#endif

#endif



