#define		_MODE_MENU_ONEFINGER_REGDEL_C_

#include	"main.h"

#define _GOODIX_OVER_WRITE_ (1)  /* 사용 중인 slot은 over write 하도록 수정 삭제 / 후 등록 요청 */


void		ModeGotoOneFingerRegDelCheck(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_REGDEL;
	gbModePrcsStep = 0;
}


void		ModeGotoOneFingerRegister(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_REGISTER;
	gbModePrcsStep = 0;
#ifdef	BLE_N_SUPPORT
	gbEnterMode = 0x00;
#endif 
}

void		ModeGotoOneFingerDelete(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_DELETE;
	gbModePrcsStep = 0;
#ifdef	BLE_N_SUPPORT
	gbEnterMode = 0x00;
#endif 
}

#ifdef	BLE_N_SUPPORT
void ModeGotoOneFingerRegisterByBleN(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_REGISTER;
	gbModePrcsStep = 0;
	gbEnterMode = 0x01;
}


void ModeGotoOneFingerDeleteByBleN(void)
{
	gbMainMode = MODE_MENU_ONEFINGER_DELETE;
	gbModePrcsStep = 2;
	gbEnterMode = 0x01;	
}
#endif 

void		OneFingerModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// 지문 개별 등록
		case 1: 	
			ModeGotoOneFingerRegister();
			break;
	
		// 지문 개별 삭제
		case 3: 		
			ModeGotoOneFingerDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}

void		MenuOneFingerModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 지문 개별 등록
		case TENKEY_1:			
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_FINGER_SHARP, gbInputKeyValue);	//지문등록 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			break;

		// 지문 개별 삭제
		case TENKEY_3:		
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_ALLFINGER_SHARP, gbInputKeyValue);	//지문삭제 모드입니다. 계속하시려면 샵 버튼을 누르세요.
			break;

		case TENKEY_SHARP:
			OneFingerModeSetSelected();
			break;
									
		case TENKEY_STAR:
			if(GetSupportCredentialCount() > 1)
				ModeGotoMenuAdvancedCredentialSelect();
			else 
				ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}

void		ModeOneFingerRegDelCheck(void )
{
	switch(gbModePrcsStep)
	{
		case 0:
			Feedback13MenuOn(AVML_REGFINGER1_DELFINGER3, VOL_CHECK);		//지문 등록은 1번, 지문 삭제는 3번을 누르세요.
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuOneFingerModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}
}


//==============================================================================//
// Comment	: 지문 번호가 메모리에 존재하는지 검사
// Return 	: 0:없음 	1:존재 함.
//==============================================================================//
BYTE	FingerSlotVerify()
{
	BYTE	bTmp, bCnt;

	for(bCnt = 0; bCnt < _MAX_REG_BIO; bCnt++){
		RomRead( &bTmp, (WORD)BIO_ID+(2*bCnt), 1);

		if ( bTmp == gSelectedSlotNumber ) {		// EEPROM에 저장된 slot 정보와 비교
			gbFID = bCnt;
			return 1;
		}else{
			gbFID=0xff;
		}
	}

	return 0;	
}

void FingerSlotNumberToRegister(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();		//slot number는 1~MAX size

#if (_MAX_REG_BIO == 100)	
	if(gSelectedSlotNumber == 0x00) gSelectedSlotNumber = 100;
#endif 	
	
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= _MAX_REG_BIO))
	{
		TenKeyVariablesClear();

		bTmp = FingerSlotVerify(); 
		if(bTmp == 0){			 
			gbModePrcsStep++;
		}
		else 
		{
#if _GOODIX_OVER_WRITE_  /* 사용 중인 slot은 over write 하도록 수정 삭제 / 후 등록 요청 */
			gbModePrcsStep = 30; 
#else 					 
			FeedbackError(AVML_ALREADY_USE_SLOT, VOL_CHECK); 
			if(gbEnterMode)
			{			
				PackTx_MakeAlarmPacket(AL_DUPLICATE_PIN_ERROR, 0x00, gSelectedSlotNumber & 0xff); // H level 은 0 만 나옴 
				ModeClear();
			}
			else 
			{
				gbBioTimer10ms = 10;
				gbModePrcsStep = 30; 
			}
#endif 
		}
	}
	else
	{
		gSelectedSlotNumber = 0xFF;
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}
}

void BackwardFunctionForOneFingerRegDel(void)
{
	ReInputOrModeMove(ModeGotoOneFingerRegDelCheck);
}

//------------------------------------------------------------------------------
/** 	@brief	One Finger Print Register Mode 
	@remark	User Code 등록 모드
	@see	MainModeProcess_T1.c
	@see	FingerPFM_3000.c
	@see	PFM_3000_UART.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.08.31		by Hyojoon
PFM-3000 / TSM1071M normal slot number 관리 
Enroll 시에 slot number 를 정해서 보내게 되면 Finger Module 에서는 전달 한 slotnumber 를 return 한다.  
ex) slot number 를 0x14 를 보내면 
ROM index 는 0 번 부터 시작 하므로 

gbFID = module return 값 - 1;  // ROM 의 index 로 사용 
FingerWriteBuff = gbFID;          // gbFID 받을 값을 gbFID-1 index 에 gbFID 로 저장 not BCD

개발자 마다 slot 관리가 틀려서 TCS4K 의 경우 slot number 를 BCD 로 저장 했는데 그럴 필요 없음 
신규 플렛 폼의 PFM-3000 및 앞으로 개발 할 TSM 의 경우 hex 값을 저장 아래 소스와 같이... 

ROM
--------------------------------
|               BIO_ID + gbFID                  |
--------------------------------
|FingerWriteBuff[0] | FingerWriteBuff[1] |
--------------------------------

*/
//------------------------------------------------------------------------------


void ModeOneFingerRegister(void)
{
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
#ifdef	BLE_N_SUPPORT
			
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if ( no_fingers >= _MAX_REG_BIO ) { 		// 이미 최대로 가득 차 있으면 개별 등록 거부.
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK); 	// Error, 저장공간이 부족합니다.
				gbModePrcsStep=40;					// 개별 등록 삭제 메뉴로 가기
				break;
			}

			if(gbEnterMode == 0x00)
			{
				FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			//개별 비밀번호 관리를 위하여 사용자 번호 두자리를 입력하세요, 계속하시려면 샵 버튼을 누르세요.
				TenKeyVariablesClear();
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
				gSelectedSlotNumber = 0;
			}
#else 						
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if ( no_fingers >= _MAX_REG_BIO ) {			// 이미 최대로 가득 차 있으면 개별 등록 거부.
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		// Error, 저장공간이 부족합니다.
				gbModePrcsStep=40;					// 개별 등록 삭제 메뉴로 가기
				break;
			}
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			//개별 비밀번호 관리를 위하여 사용자 번호 두자리를 입력하세요, 계속하시려면 샵 버튼을 누르세요.
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gSelectedSlotNumber = 0;
#endif 						
			gbModePrcsStep++;
			break;
			
		case 1:
#ifdef	BLE_N_SUPPORT
			if(gbEnterMode)
			{	
				/* BLE 로 온 경우 */
				gPinInputKeyCnt = 2;
				FingerSlotNumberToRegister();
			}
			else 
			{
				SelectSlotNumber(FingerSlotNumberToRegister, BackwardFunctionForOneFingerRegDel,1);
			}
#else 				
			SelectSlotNumber(FingerSlotNumberToRegister, BackwardFunctionForOneFingerRegDel,1);
#endif 
			break;

		case 2:
			AudioFeedback(AVML_ENTER_FINGER, gcbBuzSta, VOL_CHECK);			// 등록하실 지문을 차례로 입력 하세요.
			SetModeTimeOut(_MODE_TIME_OUT_7S);
			BioModuleON();
			SetModeTimeOut(10); //1s wait ATR
			gbModePrcsStep = 3;
			break;

		case 3:
			if(GetModeTimeOut() == 0)
			{
				/* ATR 이 1s 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep	= 100;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;

				// Device status check 
				if(gbFingerPrint_rxd_buf[12] != 0x00)
				{
					FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
					gbModePrcsStep	= 100;
					break;
				}
				//Get ALT 
				gbfingerALT = gbFingerPrint_rxd_buf[11];
				// save Device UID
				RomWrite(&gbFingerPrint_rxd_buf[3],HFPM_MODULE_ID,8);

				gbModePrcsStep = 4;
				SetModeTimeOut(gbfingerALT);
				NFPMTxEventEnroll(gSelectedSlotNumber);
			}
			break;

		case 4: 
			if(GetModeTimeOut() == 0)
			{
				/* ALT 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep = 100;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;

				// Enroll ack 처리 
				// Get Max Tempalte number
				if(gbFingerPrint_rxd_buf[2] != NFPM_EVNET_ENROLL || 
					(gbFingerPrint_rxd_buf[3] < 0x01 && gbFingerPrint_rxd_buf[3] > 0x08) ||
					gbFingerPrint_rxd_buf[4] !=0x00 )
			{
					FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
					gbModePrcsStep = 100;
				break;
			}
			
				gbfingerMaxTemplateNumber = gbFingerPrint_rxd_buf[3];
				gbModePrcsStep = 5;
				dbDisplayOn = 0x00;
				gbFingerRegisterDisplayStep = 0;
				SetModeTimeOut(_MODE_TIME_OUT_7S); // 등록 모드 대기 상태 이므로 7초 
					}
			break;
			
		case 5:
			if(GetModeTimeOut() == 0)
			{
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);
				gbModePrcsStep = 100;
				break;
			}

			if(gbInputKeyValue == FUNKEY_REG){
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				gbModePrcsStep = 100;
					break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				switch(gbFingerPrint_rxd_buf[3]) // template number 
				{
					case 0x01:
					case 0x02:
					case 0x03:
					case 0x04:
					case 0x05:
					case 0x06:
					case 0x07:
					case 0x08:					
									{
						switch(gbFingerPrint_rxd_buf[4])
						{
							case 0x00: // parameter ok 
							{
								AudioFeedback((AVML_REG_FINGER01_REIN+gbFingerPrint_rxd_buf[3]-1),gcbBuzNum, VOL_CHECK);
								SetModeTimeOut(_MODE_TIME_OUT_7S);
								NFPMTxEventEnrollAlarm(gbFingerPrint_rxd_buf[3]);
								gbFingerRegisterDisplayStep = gbFingerPrint_rxd_buf[3];								
							}
							break;

							case 0xFF: // enroll complete
							{
								SetModeTimeOut(_MODE_TIME_OUT_20S);
								gbFID = gbFingerPrint_rxd_buf[5];
								gbModePrcsStep = 6;
							}
							break;

							case 0x04: // dirty finger
							case 0x05: // repeat finger
							{
								AudioFeedback(AVML_REG_FINGER_ERR,gcbBuzNum, VOL_CHECK);
								SetModeTimeOut(_MODE_TIME_OUT_20S);
							}
							break;	

							default: // error 
							{
								FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
								gbModePrcsStep = 100;
							}
							break;
						}
					}
					break;

					default:
					{
						FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
						gbModePrcsStep = 100;
				}
					break;					
			}
			}		
			else 
			{
				FingerPrintRegisterDisplay();
			}
			break;

		case 6: 	
			if(GetModeTimeOut() == 0)
			{
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep = 100;
				break;
			}

			FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID;
			gbFID--;
			no_fingers++;
			RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				
			RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); 
			
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, FingerWriteBuff[0]);
#endif

			memset(bTmpArray, 0x00, 8);
			CopyCredentialDataForPack(bTmpArray, 8);
			PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)FingerWriteBuff[0]);

			PackTxWaitTime_EnQueue(5); // pack 에서 응답이 빠르다고 해서  보통 위 함수들이 등록 삭제 시에 불리는데 , 다른 event 와 겹치게 발생함 
			InnerPackTxWaitTime_EnQueue(5); 

#ifdef	BLE_N_SUPPORT
			if(no_fingers >= _MAX_REG_BIO || gbEnterMode == 0x01)
#else 			
			if(no_fingers >= _MAX_REG_BIO)
#endif 				
			{				
				AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);
				LedSetting(gcbLedOk, LED_DISPLAY_OFF);
				gbModePrcsStep = 100;
				gbEnterMode = 0x00;
				break;
			}
							
			FeedbackKeyStarOn(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);
			NFPMTxEventStop(0x00);
			SetModeTimeOut(gbfingerALT);
			gbModePrcsStep = 7;
			break;

		case 7:
			if(GetModeTimeOut() == 0 ||gbFingerPrint_rxd_end)
			{
				BioModuleOff();
				gbFingerPrint_rxd_end = 0;
				gbModePrcsStep = 8;
				SetModeTimeOut(_MODE_TIME_OUT_20S);
			}
			break;

		case 8:			// 등록 연속  or 종료...시..
			switch(gbInputKeyValue){
				case FUNKEY_REG:
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					BioOffModeClear();
					break;

				case TENKEY_STAR:
					// 추가 등록
					gbModePrcsStep = 0;		
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_HIGH);
					gModeTimeOutTimer100ms = _MODE_TIME_OUT_DEFAULT;
					break;

				case TENKEY_NONE:				//key 눌림 없음
					if(GetModeTimeOut() == 0)
					{
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					break;
				
				default:
					break;
			}
			break;


#ifdef _GOODIX_OVER_WRITE_ /* 사용 중인 slot은 over write 하도록 수정 삭제 / 후 등록 요청 */
		case 30:
			BioModuleON();
			SetModeTimeOut(10);
			gbModePrcsStep = 31;
		break;
		
		case 31:
			if(GetModeTimeOut() == 0)
			{
				/* ATR 이 1s 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep	= 100;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				// Device status check 
				if(gbFingerPrint_rxd_buf[12] != 0x00)
				{
					FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
					gbModePrcsStep	= 100;
					break;
				}
				//Get ALT 
				gbfingerALT = gbFingerPrint_rxd_buf[11];
				gbModePrcsStep = 32;
				SetModeTimeOut(gbfingerALT);
				NFPMTxEventDelete(gSelectedSlotNumber);
			}
			break;

		case 32: 		
			if(GetModeTimeOut() == 0)
			{
				/* ALT 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep = 100;
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[3] != 0x00)
				{
					FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
					gbModePrcsStep = 100;
					break;
				}

				no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..

				if(no_fingers)
					no_fingers--;
				
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);	// 등록 숫자 한 줄이기.
				FingerWriteBuff[0]=0xff;
				FingerWriteBuff[1]=0xff;
				gbFID = gSelectedSlotNumber;
				gbFID--;

				RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*gbFID), 2); //해당 지문 번호 자리 0xff로 넣기 ..	

				SetModeTimeOut(10);
				NFPMTxEventStop(0x00);
				SetModeTimeOut(gbfingerALT);
				gbModePrcsStep = 33;
			}
			break;

		case 33: 
			if(gbFingerPrint_rxd_end || GetModeTimeOut() == 0)
			{
				gbFingerPrint_rxd_end = 0;
				BioModuleOff();
				SetModeTimeOut(3); 
				gbModePrcsStep = 34;
			}
			break;

		case 34: 
			if(GetModeTimeOut() == 0)
			{
				gbModePrcsStep = 2;
			}
			break;			
#else 
		case 30:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;
			ModeGotoOneFingerRegister();
		break;
#endif 			

		case 100:
			NFPMTxEventStop(0x00);
			SetModeTimeOut(gbfingerALT);
			gbModePrcsStep = 101;
			break;

		case 101:
			if(GetModeTimeOut() == 0)
			{
			BioOffModeClear();
			break;
			}

			if(gbFingerPrint_rxd_end)
			{
				// Rx 만 받으면 그냥 off 
				gbFingerPrint_rxd_end = 0;
				BioOffModeClear();	
				break;
			}
			break;

		default:
			gbModePrcsStep = 100;
			break;
	}
}



void FingerSlotNumberToDelete(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();		//slot number는 1~20

#if (_MAX_REG_BIO == 100)	
	if(gSelectedSlotNumber ==0x00) gSelectedSlotNumber = 100;
#endif 	
	
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= _MAX_REG_BIO))
	{
		TenKeyVariablesClear();
		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);	//# 버튼에 대한 효과음
		bTmp = FingerSlotVerify(); 
		if(bTmp == 0){			 
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gbModePrcsStep = 6;
		}
		else {					 
			gbModePrcsStep++;
		}		
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}
}



void ModeOneFingerDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_FOR_DEL_SHARP, VOL_CHECK);			//삭제를 원하시는 사용자 번호를 입력하세요. 계속하시려면 샵 버튼을 누르세요.
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gSelectedSlotNumber = 0;
			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(FingerSlotNumberToDelete, BackwardFunctionForOneFingerRegDel,0);
			break;

		case 2:
#ifdef	BLE_N_SUPPORT
			if(gbEnterMode)
			{
				gPinInputKeyCnt = 2;
				FingerSlotNumberToDelete();
				LedModeRefresh();
			}
#endif 
			BioModuleON();
			SetModeTimeOut(10);
			gbModePrcsStep = 3;
			break;

		case 3:
			if(GetModeTimeOut() == 0)
			{
				/* ATR 이 1s 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep	= 100;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				// Device status check 
				if(gbFingerPrint_rxd_buf[12] != 0x00)
				{
					FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
					gbModePrcsStep	= 100;
					break;
				}
				//Get ALT 
				gbfingerALT = gbFingerPrint_rxd_buf[11];
				gbModePrcsStep = 4;
				SetModeTimeOut(gbfingerALT);
				NFPMTxEventDelete(gSelectedSlotNumber);
			}
			break;

		case 4: 		
			if(GetModeTimeOut() == 0)
			{
				/* ALT 안에 안오면 */
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep = 100;
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[3] != 0x00)
				{
					FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
					gbModePrcsStep = 100;
					break;
				}

				no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..

				if(no_fingers)
					no_fingers--;
				
				RomWrite(&no_fingers, (WORD)KEY_NUM, 1);	// 등록 숫자 한 줄이기.
				FingerWriteBuff[0]=0xff;
				FingerWriteBuff[1]=0xff;
				gbFID = gSelectedSlotNumber;
				gbFID--;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT,gSelectedSlotNumber);
#endif

				RomWrite(FingerWriteBuff, (WORD)BIO_ID+(2*gbFID), 2); //해당 지문 번호 자리 0xff로 넣기 ..	

				DDLStatusFlagClear(DDL_STS_FAIL_BIO);
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

				NFPMTxEventStop(0x00);
				SetModeTimeOut(gbfingerALT);
				gbModePrcsStep = 5;
			}
			break;


		case 5: 
			if(GetModeTimeOut() == 0)
			{
				FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH);
				gbModePrcsStep	= 100;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				BioModuleOff();
				gbModePrcsStep = 6;
			}
			break;

		case 6:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;			//버튼음 마침을 기다림

#ifdef	BLE_N_SUPPORT
			if(gbEnterMode)
			{
				FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
				BioOffModeClear();
				PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
				gbEnterMode = 0x00;
				break;
			}
#endif 
			PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, (WORD)gSelectedSlotNumber);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R2, VOL_CHECK);		//완료되었습니다. 다른 설정을 원하시면 별표를 누르시고, 종료를 원하시면 R버튼을 누르세요.
			gbModePrcsStep++;
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			break;

		case 7:			
			switch(gbInputKeyValue){
				case FUNKEY_REG:
					FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
					BioOffModeClear();
					break;
				case TENKEY_STAR:
					gbModePrcsStep = 0; 	
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_CHECK);
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_NONE:				
					if(GetModeTimeOut() == 0)
					{
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					break;
						
				default:
				break;
			}
			break;

		case 100:
			NFPMTxEventStop(0x00);
			SetModeTimeOut(gbfingerALT);
			gbModePrcsStep = 101;
			break;

		case 101:
			if(GetModeTimeOut() == 0)
			{
				BioOffModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				// Rx 만 받으면 그냥 off 
				gbFingerPrint_rxd_end = 0;
				BioOffModeClear();	
				break;
			}
			break;

		default:
			gbModePrcsStep = 100;
			break;
	}
}

