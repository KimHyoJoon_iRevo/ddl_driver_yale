#define		_MODE_FINGER_DELETE_C_

#include "main.h"

extern	BYTE		no_fingers;
extern	BYTE 	FingerReadBuff[4];
extern	BYTE 	FingerWriteBuff[4];

#define	 _MODE_TIME_OUT_5S			50

void	ModeGotoNormalAllFingerDelete( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 0;
}


void	ModeGotoAdvancedAllFingerDelete1( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
#ifdef	DDL_CFG_DIMMER	
	gbModePrcsStep = 1;			// 2; //이 함수를 부르는 함수에서 gbModeProcsStep을 하나 증가 시키므로 여기서는 원래 가고자 하는 값보다 1 작게 해야 한다.
#else 
	gbModePrcsStep = 2; 		
#endif 
}

void	ModeGotoAdvancedAllFingerDelete2( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE;
	gbModePrcsStep = 2;
}


void	ModeAllFingerDelete( void )
{
	BYTE	bTmp;

	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_PIN_SHARP, VOL_CHECK);		//비밀번호를 입력하세요. 계속하시려면 샵 버턴을 누르세요.
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_20S);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
					switch(PincodeVerify(0))
					{
						case 1:
							gbModePrcsStep++;
							break;

						case RET_NO_INPUT:
						case RET_WRONG_DIGIT_INPUT:
							FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
							ModeClear();
							break;

						default:
							gbModePrcsStep=20;
					}
					break;		

				case TENKEY_STAR:
					if(GetSupportCredentialCount() > 1)
					{
						ReInputOrModeMove(ModeGotoMenuCredentialSelect);
					}
					else 
					{
						ReInputOrModeMove(ModeGotoMenuMainSelect);
					}
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					BioOffModeClear();
					break;
			}	
			break;

		case 2:
			BioModuleON();
			SetModeTimeOut(_MODE_TIME_OUT_5S);
			gbFingerRetryCounter = 0;
			gbModePrcsStep++;
			break;
			
		case 3:
			if(GetModeTimeOut() == 0){
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(P_FP_DDL_RX_T)	//지문 모듈 Power On하고 약 100ms의 대기 시간을 가져야 지문 모듈이 완전히 Wakeup되며 Wakeup되었다는 증거로 RX Port가 High로 된다.
			{
				FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_DELETE , NO_FINGER_DATA , NO_FINGER_DATA);
				SetModeTimeOut(_MODE_TIME_OUT_20S);
				gbFingerRetryCounter = 0;
				gbModePrcsStep++;
			}
			else
			{
				FingerPrintRetry();
			}
			break;

		case 4: 		
			if(GetModeTimeOut() == 0){
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == 0x83)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							gbModePrcsStep = 5;
							break;		

						case 0x12:	//Slot에 Template이 없음(저장된 지문이 없음)
							gbModePrcsStep = 6;
							break;						
							
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY 상태면 Cancer명령어를 송신 하고 다시 인증을 시도함						
						case 0x1F:	//명령 실패	
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}				
			}
			break;
		
		case 5:
			if(GetModeTimeOut() == 0){
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
			{		
				gbFingerPrint_rxd_end=0;
				if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{
								case 0x03:	//Delete 결과 성공
									no_fingers = 0;
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장

									FingerReadBuff[0] = FingerReadBuff[1] = 0xFF;
									for(bTmp = 0; bTmp< _MAX_REG_BIO; bTmp++){		// 모두 초기화..
										RomWrite(FingerReadBuff, (WORD)BIO_ID+(2*bTmp), 2);
									}

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDeleteOfAllUserFingerprint();
#endif

									DDLStatusFlagClear(DDL_STS_FAIL_BIO);

									if( PowerDownModeForJig() == STATUS_SUCCESS ) { // jigmode의 동작이면
										FeedbackModeCompleted(AVML_DEL_ALL_FINGER_DONE_MSG, VOL_CHECK);	//	등록된 모든 지문이 모두 삭제 되었습니다.
										BioOffModeClear();
									}
									else 
									{
										BioModuleOff();
										if ( gbModeChangeFlag4Finger == 1 ) {		//모드 전환을 위해 전체 지문을 지운 것이라면 pincode 디스플레이를 한다.
											gbModePrcsStep = 7;
											gbModeChangeFlag4Finger= 0;
											break;
										}
										else if ( gbModeChangeFlag4Finger == 2 ) {
											// 안심모드에서 5번으로 전체 삭제일 경우 
											// 이경우 pack 으로 0xFF 0xFF 를 ProcessDeleteAllCredentials() 보내도록 수정 여긴 바로 break;
											gbModePrcsStep = 9;
											gbModeChangeFlag4Finger= 0;
                                                                                        FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
											break;
										}
										else if ( gbModeChangeFlag4Finger == 3 ) {
											gbModeChangeFlag4Finger= 0;
											ModeGotoAllDataReset(0xFF);
											gbModePrcsStep = 4;
											break;
										}
										else if(FactoryResetRunCheck() == STATUS_SUCCESS) // factory 중이라면  //hyojoon_20160922
										{
											ModeClear();
										}
										else 
										{
											FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);			//	completed, Press the star for setting other options or press the "R" button to finish
											gbModePrcsStep = 8;
										}
									
										// 지문 전체 삭제 정보 전송
										PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, 0xFF);
										// 지문 전체 삭제 정보 전송
									}
									break;
									
								case 0x00:	//결과 없음
								case 0x01:	//Enrollment 결과 성공
								case 0x02:	//Verify 결과 성공
								case 0x81:	//Enrollment 결과 실패
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}
							break;
							
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						case 0x1E:	//BUSY 상태(현재 명령 무시) Cancer명령어를 송신 하고 다시 인증을 시도함
						case 0x1F:	//명령 실패
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
			}
			break;

		case 6:
			no_fingers = 0;
			RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장
			
			FingerReadBuff[0] = FingerReadBuff[1] = 0xFF;
			for(bTmp = 0; bTmp< _MAX_REG_BIO; bTmp++){		// 모두 초기화..
				RomWrite(FingerReadBuff, (WORD)BIO_ID+(2*bTmp), 2);
			}

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			ScheduleDeleteOfAllUserFingerprint();
#endif
			
			DDLStatusFlagClear(DDL_STS_FAIL_BIO);
			
			if( PowerDownModeForJig() == STATUS_SUCCESS ) { // jigmode의 동작이면
				FeedbackModeCompleted(AVML_DEL_ALL_FINGER_DONE_MSG, VOL_CHECK);	//	등록된 모든 지문이 모두 삭제 되었습니다.
				BioOffModeClear();
			}
			else 
			{
				BioModuleOff();
				if ( gbModeChangeFlag4Finger == 1 ) {		//모드 전환을 위해 전체 지문을 지운 것이라면 pincode 디스플레이를 한다.
					gbModePrcsStep = 7;
					gbModeChangeFlag4Finger= 0;
				}
				else if ( gbModeChangeFlag4Finger == 2 ) {
					// 안심모드에서 5번으로 전체 삭제일 경우 
					// 이경우 pack 으로 0xFF 0xFF 를 ProcessDeleteAllCredentials() 보내도록 수정 여긴 바로 break
					gbModePrcsStep = 9;
					gbModeChangeFlag4Finger= 0;
                                        FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
					break;
				}
				else if ( gbModeChangeFlag4Finger == 3 ) {
						gbModeChangeFlag4Finger= 0;
						ModeGotoAllDataReset(0xFF);
						gbModePrcsStep = 4;
						break;
				}
				else if(FactoryResetRunCheck() == STATUS_SUCCESS) // factory 중이라면  //hyojoon_20160922
				{
					ModeClear();
				}
				else 
				{
					FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);			//	completed, Press the star for setting other options or press the "R" button to finish
					gbModePrcsStep = 8;
				}
			
				// 지문 전체 삭제 정보 전송
				PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, 0xFF);
				// 지문 전체 삭제 정보 전송
			}
			break;

		case 7:		// display pincode after changing mode (normal <-> advanced)
			DisplayRegisteredPincode(PincodeRegisterCompleteProcess);
			//위의 함수 호출 후 mode clear가 일어난다.
			break;

		case 8:
			if(GetSupportCredentialCount() > 1)
			{
				GotoPreviousOrComplete(ModeGotoMenuCredentialSelect);
			}
			else 
			{
				GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			}			
			//위 함수호출로 mode clear가 일어날 수 있다.
			break;

		case 9:
			// 안심모드에서 5번으로 전체 삭제일 경우 
			GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			//위 함수호출로 mode clear가 일어날 수 있다.
			break;

		case 20:		//비밀번호 인증이 틀린 경우 에러처리
			if(GetBuzPrcsStep() || GetVoicePrcsStep() )	break;

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			BioOffModeClear();
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
			BioOffModeClear();
			AudioFeedback(AVML_MALFUNCTION_FPM_MSG, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.
			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;
		default:
			BioOffModeClear();
			break;
	}
}





#ifdef	BLE_N_SUPPORT
void	ModeGotoAllFingerDeleteByBleN( void )
{
	gbMainMode = MODE_MENU_FINGER_DELETE_BYBLEN;
	gbModePrcsStep = 0;
}


void	ModeAllFingerDeleteByBleN( void )
{
	BYTE	bTmp;

	switch(gbModePrcsStep)
	{
		case 0:
			SetModeTimeOut(_MODE_TIME_OUT_20S);
			gbModePrcsStep = 2;
			break;
	
		case 2:
			BioModuleON();
			SetModeTimeOut(_MODE_TIME_OUT_5S);
			gbFingerRetryCounter = 0;
			gbModePrcsStep++;
			break;
			
		case 3:
			if(GetModeTimeOut() == 0){
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(P_FP_DDL_RX_T)	//지문 모듈 Power On하고 약 100ms의 대기 시간을 가져야 지문 모듈이 완전히 Wakeup되며 Wakeup되었다는 증거로 RX Port가 High로 된다.
			{
				FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_DELETE , NO_FINGER_DATA , NO_FINGER_DATA);
				SetModeTimeOut(_MODE_TIME_OUT_20S);
				gbFingerRetryCounter = 0;
				gbModePrcsStep++;
			}
			else
			{
				FingerPrintRetry();
			}
			break;

		case 4: 		
			if(GetModeTimeOut() == 0){
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == 0x83)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							gbModePrcsStep = 5;
							break;		

						case 0x12:	//Slot에 Template이 없음(저장된 지문이 없음)
							gbModePrcsStep = 6;
							break;						
							
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY 상태면 Cancer명령어를 송신 하고 다시 인증을 시도함						
						case 0x1F:	//명령 실패	
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}				
			}
			break;
		
		case 5:
			if(GetModeTimeOut() == 0){
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
			{		
				gbFingerPrint_rxd_end=0;
				if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{
								case 0x03:	//Delete 결과 성공
									no_fingers = 0;
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장

									FingerReadBuff[0] = FingerReadBuff[1] = 0xFF;
									for(bTmp = 0; bTmp< _MAX_REG_BIO; bTmp++){		// 모두 초기화..
										RomWrite(FingerReadBuff, (WORD)BIO_ID+(2*bTmp), 2);
									}

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDeleteOfAllUserFingerprint();
#endif

									DDLStatusFlagClear(DDL_STS_FAIL_BIO);

									if( PowerDownModeForJig() == STATUS_SUCCESS ) { // jigmode의 동작이면
										FeedbackModeCompleted(AVML_DEL_ALL_FINGER_DONE_MSG, VOL_CHECK);	//	등록된 모든 지문이 모두 삭제 되었습니다.
										BioOffModeClear();
									}
									else {
										BioModuleOff();
										FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
										ModeClear();
										// 지문 전체 삭제 정보 전송
										PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, 0xFF);
										// 지문 전체 삭제 정보 전송
									}
									break;
									
								case 0x00:	//결과 없음
								case 0x01:	//Enrollment 결과 성공
								case 0x02:	//Verify 결과 성공
								case 0x81:	//Enrollment 결과 실패
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}
							break;
							
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						case 0x1E:	//BUSY 상태(현재 명령 무시) Cancer명령어를 송신 하고 다시 인증을 시도함
						case 0x1F:	//명령 실패
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
			}
			break;

		case 6:
			no_fingers = 0;
			RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장
			
			FingerReadBuff[0] = FingerReadBuff[1] = 0xFF;
			for(bTmp = 0; bTmp< _MAX_REG_BIO; bTmp++){		// 모두 초기화..
				RomWrite(FingerReadBuff, (WORD)BIO_ID+(2*bTmp), 2);
			}

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			ScheduleDeleteOfAllUserFingerprint();
#endif
			
			DDLStatusFlagClear(DDL_STS_FAIL_BIO);
			
			if( PowerDownModeForJig() == STATUS_SUCCESS ) { // jigmode의 동작이면
				FeedbackModeCompleted(AVML_DEL_ALL_FINGER_DONE_MSG, VOL_CHECK);	//	등록된 모든 지문이 모두 삭제 되었습니다.
				BioOffModeClear();
			}
			else {
				BioModuleOff();
				FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
				ModeClear();
				// 지문 전체 삭제 정보 전송
				PackTxEventCredentialDeleted(CREDENTIALTYPE_FINGERPRINT, 0xFF);
				// 지문 전체 삭제 정보 전송
			}
			break;	

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
			BioOffModeClear();
			AudioFeedback(AVML_MALFUNCTION_FPM_MSG, gcbBuzErr, VOL_HIGH); // The Finger print module is not working properly.
			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;
		default:
			BioOffModeClear();
			break;
	}
}




#endif



