//------------------------------------------------------------------------------
/** 	@file		ModeMenuAllGMTouchKeyDelete_T1.c
	@brief	All GateMan Touch Key Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUALLGMTOUCHKEYDELETE_T1_INCLUDED
#define __MODEMENUALLGMTOUCHKEYDELETE_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoAllGMTouchKeyDelete(void);
void ModeAllGMTouchKeyDelete(void);

#endif


