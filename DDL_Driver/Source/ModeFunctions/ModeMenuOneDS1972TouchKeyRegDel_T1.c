//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneDS1972TouchKeyRegDel_T1.c
	@version 0.1.00
	@date	2016.09.12
	@brief	DS1972 Touch Key Individual Register/Delete Mode
	@remark	터치키 개별 등록/삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@see	ModeDS1972TouchKeyVerify_T1.h"
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.09.12		by hyojoon
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gOneDS1972TouchKeyUidBuf[MAX_KEY_UID_SIZE];


void ModeGotoOneDS1972TouchKeyRegDelCheck(void)
{
	gbMainMode = MODE_MENU_ONEDS1972TOUCHKEY_REGDEL;
	gbModePrcsStep = 0;
}


void ModeGotoOneDS1972TouchKeyRegister(void)
{
	gbMainMode = MODE_MENU_ONEDS1972TOUCHKEY_REGISTER;
	gbModePrcsStep = 0;
}

void ModeGotoOneDS1972TouchKeyDelete(void)
{
	gbMainMode = MODE_MENU_ONEDS1972TOUCHKEY_DELETE;
	gbModePrcsStep = 0;
}

BYTE GetNoDS1972TouchKeyFromEeprom( void )
{
	BYTE	no_eeprom_key;

	RomRead(&no_eeprom_key, (WORD)KEY_NUM, 1);					
	return	no_eeprom_key;
}

void OneDS1972TouchKeyRegister(void)
{
	// index 는 0 번 부터 
	gInputKeyNumberForRegister = GetNoDS1972TouchKeyFromEeprom();
	gInputKeyNumberForRegister++;
	RomWrite(gbDS1972WriteBuf1, TOUCHKEY_UID+(((WORD)gSelectedSlotNumber-1)*MAX_KEY_UID_SIZE), MAX_KEY_UID_SIZE);
	RomWrite(gbDS1972WriteBuf2, TOUCHKEY_FV+(((WORD)gSelectedSlotNumber-1)*MAX_KEY_FV_SIZE), MAX_KEY_FV_SIZE);
	RomWrite(gbDS1972WriteBuf3, TOUCHKEY_RC+(((WORD)gSelectedSlotNumber-1)*MAX_KEY_RC_SIZE), MAX_KEY_RC_SIZE);	
	RomWrite(&gInputKeyNumberForRegister, (WORD)KEY_NUM, 1);           		
}

void OneDS1972TouchKeyModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// 카드 개별 등록
		case 1: 	
			ModeGotoOneDS1972TouchKeyRegister();
			break;
	
		// 카드 개별 삭제
		case 3: 		
			ModeGotoOneDS1972TouchKeyDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuOneDS1972TouchKeyModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 카드 개별 등록
		case TENKEY_1:			
			//적당한 음원 없음
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_CARD_SHARP, gbInputKeyValue);	
			break;

		// 카드 개별 삭제
		case TENKEY_3:		
			//적당한 음원 없음
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_CARD_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_SHARP:
			OneDS1972TouchKeyModeSetSelected();
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}




void ModeOneDS1972TouchKeyRegDelCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			//적당한 음원 없음
			Feedback13MenuOn(AVML_REGCARD1_DELCARD3, VOL_CHECK);
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuOneDS1972TouchKeyModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}
}




void DS1972TouchKeySlotNumberToRegister(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= SUPPORTED_DS1972KEY_NUMBER))
	{
		//적당한 음원 없음
		FeedbackKeyPadLedOn(AVML_ENTER_CARD_SHARP, VOL_CHECK); 			
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		iButtonGotoRegModeStart();
		gbModePrcsStep++;
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}


void OneDS1972TouchKeyInputCheckForRegister(void)
{
	switch(gbInputKeyValue)
	{
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		default:
			if(DS1972TouchKeyInputCheck() == STATUS_SUCCESS)
			{
				gbModePrcsStep++;
				break;
			}

			TimeExpiredCheck();
			break;
	}
}

void InputOneDS1972TouchKeyProcessLoop(void)
{
	BYTE bTmp;

	bTmp = DS1972TouchKeyVerify();
	if((bTmp == _DATA_NOT_IDENTIFIED) || (bTmp == gSelectedSlotNumber))
	{
		FeedbackCredentialInput(VOICE_MIDI_BUTTON2, gSelectedSlotNumber);
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		gbModePrcsStep++;
	}
	else
	{
		// 입력한 터치키가 이미 등록한 터치티와 중복될 경우 다시 터치키 입력 대기 
		FeedbackErrorModeKeepOn(AVML_ALREADY_USE_CARD, VOL_CHECK);		
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		gbModePrcsStep = 10;
	}	
}

void ProcessRegisterOneDS1972TouchKey(void)
{
	switch(gbInputKeyValue)
	{
		case TENKEY_SHARP:
			OneDS1972TouchKeyRegister();
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			PackTxEventCredentialAdded(CREDENTIALTYPE_DS1972TOUCHKEY, (WORD)gSelectedSlotNumber);
			gbModePrcsStep++;			
			break;			

		case TENKEY_STAR:
			ReInputOrModeMove(ModeGotoOneDS1972TouchKeyRegister);
			break;
		
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				
	
		default:
			TimeExpiredCheck();
			break;
	}
}

void WaitForGotoDS1972RegModeStart(void)
{
	if(GetBuzPrcsStep() ||GetVoicePrcsStep())
	{
		SetModeProcessTime(100);
		return;
	}

	if(GetModeProcessTime() == 0x00)
	{
		iButtonGotoRegModeStart();	
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		gbModePrcsStep = 2;
	}
}


void BackwardFunctionForOneDS1972TouchKeyRegDel(void)
{
	ReInputOrModeMove(ModeGotoOneDS1972TouchKeyRegDelCheck);
}

void ModeOneDS1972TouchKeyRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			//enter the number of the user to register the user number not the code, then press the hash to continue.
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gSelectedSlotNumber = 0;
			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(DS1972TouchKeySlotNumberToRegister, BackwardFunctionForOneDS1972TouchKeyRegDel,1);
			break;

		case 2:
			OneDS1972TouchKeyInputCheckForRegister();
			break;

		case 3:
			InputOneDS1972TouchKeyProcessLoop();
			break;

		case 4:
			ProcessRegisterOneDS1972TouchKey();
			break;

		case 5:
			GotoPreviousOrComplete(ModeGotoOneDS1972TouchKeyRegister);
			break;
		
		case 10:
			WaitForGotoDS1972RegModeStart();
			break;

		default:
			ModeClear();
			break;
	}
}


void ProcessDeleteOneDS1972TouchKey(BYTE SlotNumber)
{
	BYTE TempBuf[MAX_KEY_UID_SIZE];

	RomRead(TempBuf, TOUCHKEY_UID+(((WORD)SlotNumber-1)*MAX_KEY_UID_SIZE), MAX_KEY_UID_SIZE);

	if(DataCompare(TempBuf, 0xFF, MAX_KEY_UID_SIZE) == _DATA_NOT_IDENTIFIED)
	{
	// Slot Number는 1부터 시작, 저장 번지 Index는 0부터 시작
	memset(gOneDS1972TouchKeyUidBuf, 0xFF, MAX_KEY_UID_SIZE);
	RomWrite(gOneDS1972TouchKeyUidBuf, TOUCHKEY_UID+(((WORD)SlotNumber-1)*MAX_KEY_UID_SIZE), MAX_KEY_UID_SIZE);
	RomWrite(gOneDS1972TouchKeyUidBuf, TOUCHKEY_FV+(((WORD)SlotNumber-1)*MAX_KEY_FV_SIZE), MAX_KEY_FV_SIZE);
	RomWrite(gOneDS1972TouchKeyUidBuf, TOUCHKEY_RC+(((WORD)SlotNumber-1)*MAX_KEY_RC_SIZE), MAX_KEY_RC_SIZE);	
		
	gInputKeyNumberForRegister = GetNoDS1972TouchKeyFromEeprom();
		
	if(gInputKeyNumberForRegister)
	{
	gInputKeyNumberForRegister--;
	RomWrite(&gInputKeyNumberForRegister, (WORD)KEY_NUM, 1); 
}
}
}

void DS1972TouchKeySlotNumberToDelete(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= SUPPORTED_DS1972KEY_NUMBER))
	{						
		ProcessDeleteOneDS1972TouchKey(gSelectedSlotNumber);
		FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R2, VOL_CHECK);		//completed Press the star for an additional deletion or press the "R" button to finish
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		gbModePrcsStep++;
		PackTxEventCredentialDeleted(CREDENTIALTYPE_DS1972TOUCHKEY, (WORD)gSelectedSlotNumber);
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
//		ModeClear();
	}
}



void ModeOneDS1972TouchKeyDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_FOR_DEL_SHARP, VOL_CHECK);			//enter the number of the user to delete the user number not the code, then press the hash to continue.
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;

		case 1:
			SelectSlotNumber(DS1972TouchKeySlotNumberToDelete, BackwardFunctionForOneDS1972TouchKeyRegDel,0);
			break;

		case 2:
			GotoPreviousOrComplete(ModeGotoOneDS1972TouchKeyDelete);
			break;

		default:
			ModeClear();
			break;
	}
}

#ifdef	BLE_N_SUPPORT
void ModeGotoOneDS1972TouchKeyRegisterByBleN(void)
{
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	gbMainMode = MODE_ONEDS1972_TOUCHKEY_REGISTER_BYBLEN;
	gbModePrcsStep = 0;
}

void DS1972TouchKeySlotNumberToRegisterByBleN(void)
{
	gSelectedSlotNumber = ConvertInputKeyToByte();
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= SUPPORTED_DS1972KEY_NUMBER))
	{
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		iButtonGotoRegModeStart();
		gbModePrcsStep++;
	}
	else
	{
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}

void ProcessRegisterOneDS1972TouchKeyByBleN(void)
{
	OneDS1972TouchKeyRegister();
	TenKeyVariablesClear();
	PackTxEventCredentialAdded(CREDENTIALTYPE_DS1972TOUCHKEY, (WORD)gSelectedSlotNumber);
	FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
	ModeClear();
}

void ModeOneDS1972TouchKeyRegisterByBleN(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			//enter the number of the user to register the user number not the code, then press the hash to continue.
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gSelectedSlotNumber = 0;
			gPinInputKeyCnt = 2;			
			gbModePrcsStep++;
			break;
			
		case 1:
			DS1972TouchKeySlotNumberToRegisterByBleN();
			break;

		case 2:
			OneDS1972TouchKeyInputCheckForRegister();
			break;

		case 3:
			InputOneDS1972TouchKeyProcessLoop();
			break;

		case 4:
			if(GetBuzPrcsStep() ||GetVoicePrcsStep()){ //voice 대기 
				break;
			}
			SetModeProcessTime(10);
			gbModePrcsStep++;
			break;

		case 5:
			if(GetModeProcessTime()) //voice 완료 후 다음 소리 까지 대기 
				break;
			
			ProcessRegisterOneDS1972TouchKeyByBleN();
			break;
			
		case 10:
			WaitForGotoDS1972RegModeStart();
			break;

		default:
			ModeClear();
			break;
	}
}
#endif 


