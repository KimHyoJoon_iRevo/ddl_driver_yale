//------------------------------------------------------------------------------
/** 	@file		ModeMenuAutoLockSet_T1.h
	@brief	Programmable Auto Lock Setting
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUAUTOLOCKSET_T1_INCLUDED
#define __MODEMENUAUTOLOCKSET_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


#ifdef	LOCKSET_AUTO_RELOCK

void ModeGotoAutoLockSetting(void);
void ModeAutoLockSetting(void);

#endif


#endif


