
#define		_MODE_FINGER_REGISTER_C_
//------------------------------------------------------------------------------
/** 	@file		ModeFingerRegister_T2.c
	@version 0.1.00
	@date	2016.08.31
	@brief	FingerPrint Register Mode 
	@remark	 User Code 등록 모드
	@see	MainModeProcess_T1.c
	@see	FingerPFM_3000.c
	@see	PFM_3000_UART.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.08.31		by Hyojoon
*/
//------------------------------------------------------------------------------

#include "Main.h"

//------------------------------------------------------------------------------
/** 	@brief	FingerPrint Register Mode Start
	@param	None
	@return 	None
	@remark 지문 등록 모드 시작
*/
//------------------------------------------------------------------------------
void	ModeGotoFingerRegister( void )
{
	gbMainMode = MODE_MENU_FINGER_REGISTER;
	gbModePrcsStep = 0;
	gbBioTimer10ms = 250;
}


//------------------------------------------------------------------------------
/** 	@brief	FingerPrint Register Mode 
	@remark	 User Code 등록 모드
	@see	MainModeProcess_T1.c
	@see	FingerPFM_3000.c
	@see	PFM_3000_UART.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.08.31		by Hyojoon
PFM-3000 / TSM1071M normal slot number 관리 
Enroll 시에 slot number 를 0x00 으로 보내게 되면 Finger Module 에서는 module 에 저장된 번호(저장 된 수)를 돌려 준다 
ex) 아무 것도 없는 상태에서 최초 지문 등록시 0x01 을 return 하게 되어 있고 lock 에선 아래와 같이 관리 한다 


gbFID = module return 값 - 1;  // ROM 의 index 로 사용 
FingerWriteBuff = gbFID;          // gbFID 받을 값을 gbFID-1 index 에 gbFID 로 저장 not BCD

ROM
--------------------------------
|               BIO_ID + gbFID                  |
--------------------------------
|FingerWriteBuff[0] | FingerWriteBuff[1] |
--------------------------------

*/
//------------------------------------------------------------------------------

void	ModeFingerRegister( void )
{
	BYTE bTmp;
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
			if(gbBioTimer10ms==248){	// 고장 진단 후 처리 
				BioReTry();
			}
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if(no_fingers >= _MAX_REG_BIO){
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		//(음성)no more space to memorize.
				BioOffModeClear();
				break;
			}
			else if(no_fingers==0){
				AudioFeedback(AVML_ENTER_FINGER, gcbBuzSta, VOL_CHECK);			// 등록하실 지문을 차례로 입력 하세요.
			}
			else{
				AudioFeedback((AVML_FINGER01_REG_MSG-1+no_fingers), gcbBuzSta, VOL_CHECK);	//(음성) n개의 지문이 등록 되었습니다. 등록하실 지문을 차례로 세번 입력하세요.
			}

			BioModuleON();
			LedModeRefresh();

			FingerReadBuff[0] = 0;		// 1의 자리
			FingerReadBuff[1] = 0;		// 10의 자리
			FingerReadBuff[2] = 0;		// 두자리 카운트
			FingerReadBuff[3] = 0;		// 임시 값 저장

			if(no_fingers == 0)
			{
			}
			else if(no_fingers < 10)
			{
				FingerReadBuff[0] = no_fingers;
			}
			else 
			{
				FingerReadBuff[1] = no_fingers / 10;
				FingerReadBuff[0] = no_fingers % 10;
			}			
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gbModePrcsStep ++;
			break;
			
		case 1:
			if(FingerReadBuff[2] >= 2)
			{
				FingerReadBuff[0] = 0;		// 1의 자리
				FingerReadBuff[1] = 0;		// 10의 자리
				FingerReadBuff[2] = 0;		// 두자리 카운트
				FingerReadBuff[3] = 0;		// 임시 값 저장
				gbModePrcsStep = 3;
				gbWakeUpMinTime10ms = 140;
			}
			else
			{
				if(FingerReadBuff[2] == 0)
					FingerReadBuff[3] = FingerReadBuff[1];
				else
					FingerReadBuff[3] = FingerReadBuff[0];

				LedPWLedSetting(FingerReadBuff[3]);
				gbModePrcsStep ++;
			}
			break;
			
		case 2:
			if(GetLedMode()) break;
			FingerReadBuff[2]++;
			gbModePrcsStep  --;
			break;
			
		case 3:
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(P_FP_DDL_RX_T)	//지문 모듈 Power On하고 약 100ms의 대기 시간을 가져야 지문 모듈이 완전히 Wakeup되며 Wakeup되었다는 증거로 RX Port가 High로 된다.
			{				
				FingerPrintTxEncryptionKeyMakeFrame();
				SetModeTimeOut(_MODE_TIME_OUT_7S);
				gbModePrcsStep = 4;
			}
			else
			{
				FingerPrintRetry();
			}
			break;

		case 4:
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[0] == 0xE2 &&  gbFingerPrint_rxd_buf[1] == 0xA1)
				{
					FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_SETSECURITY_LEVEL,SETSECURITY_LEVEL_VALUE_3,NO_FINGER_DATA);	//Enroll Command 송신 전 SETSECURITY_LEVEL을 3으로 먼저 설정 한다.
					gbModePrcsStep = 5;
					gbFingerRetryCounter = 0;
					SetModeTimeOut(_MODE_TIME_OUT_7S);
					//gbBioTimer10ms = 50;	//지문 등록 명령어 응답 대기 시간 100ms 설정 
					break;
				}
			}
			break;

		case 5:	//SETSECURITY_LEVEL설정 확인 루틴
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[4] == 0x86)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							SetModeTimeOut(_MODE_TIME_OUT_7S);
							//gbBioTimer10ms = BIO_WAKEUP_TM+750;
							gbModePrcsStep = 6;
#ifdef	DDL_CFG_SW_REG_ACTIVE_LOW
							if(!P_SW_REG_T)
#else 
							if(P_SW_REG_T)
#endif 								
							{
#ifdef DDL_CFG_FP_TS1071M
									FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_ENROLL , FINGER_ALL_REGISTER , FINGER_ALL_REGISTER);
									gbFingerPrint1TimeEnrollMode = 0;
#else
									FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_ENROLL , 99 , 0xC1);
									gbFingerPrint1TimeEnrollMode = 1;
#endif 

							}
							else
							{
								FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_ENROLL , FINGER_ALL_REGISTER , FINGER_ALL_REGISTER);
								gbFingerPrint1TimeEnrollMode = 0;
							}
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY						
						case 0x1F:	//명령 실패							
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
							
						case 0x11:	//Slot이 비어 있지 않음
							FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		//(음성)no more space to memorize.
							BioOffModeClear();
							break;
					}
				}		
			}	
			break;
			
		case 6:
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[4] == 0x81)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							SetModeTimeOut(_MODE_TIME_OUT_20S);
							//gbBioTimer10ms = BIO_WAKEUP_TM+750;
							gbModePrcsStep = 7;
							gbFingerLongTimeWait100ms = 10;
							dbDisplayOn = 0x00;
							gbFingerRegisterDisplayStep = 0;
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY						
						case 0x1F:	//명령 실패							
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
							
						case 0x11:	//Slot이 비어 있지 않음
							FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		//(음성)no more space to memorize.
							BioOffModeClear();
							break;
					}
				}		
			}			
			break;
			
		case 7:		// PTENROLL SQ2
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			switch(gbInputKeyValue){
				case FUNKEY_OPCLOSE:
				case FUNKEY_REG:
					gbModePrcsStep = 201;
					break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == 0x87)
				{
					switch(gbFingerPrint_rxd_buf[5]&0x0F)
					{
						case 0x00:	//명령 성공 
							switch(gbFingerPrint_rxd_buf[5]&0xF0)
							{
								case 0x20:	//Command 수행중
								case 0x40:	//BUSY(지문 SCAN 대기중)
									switch(gbFingerPrint_rxd_buf[6]&0xF0)
									{
										case 0x10:	//등록
											SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
											switch(gbFingerPrint_rxd_buf[6]&0x0F)
											{
												case 0x01:	//1번째 지문 입력 완료		
													if(gbFingerPrint1TimeEnrollMode == 0)
													{
														AudioFeedback(AVML_REG_FINGER01_REIN,gcbBuzNum, VOL_CHECK); // 지문이 한번 입력 되었습니다. 동일 지문을 다시 입력하세요.
														gbFingerRegisterDisplayStep = 1;
													}
													else
													{
														gbBioTimer10ms = 200;	//지문 등록 결과 Data 수신 대기 시간 1S 설정 
														gbModePrcsStep = 8;
													}
													break;
													
												case 0x02:	//2번째 지문 입력 완료
													AudioFeedback(AVML_REG_FINGER02_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 2;
													break;
													
												case 0x03:	//3번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER03_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 3;
													break;	
													
												case 0x04:	//4번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER04_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 4;
													break;	
													
												case 0x05:	//5번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER05_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 5;
													break;	
													
												case 0x06:	//6번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER06_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 6;
													break;	
													
												case 0x07:	//7번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER07_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 7;
													break;	
													
												case 0x08:	//8번째 지문 입력 완료	
													//gbFingerRegisterDisplayStep = 8;
													SetModeTimeOut(_MODE_TIME_OUT_7S);
													//gbBioTimer10ms = 200;	//지문 등록 결과 Data 수신 대기 시간 1S 설정 
													gbModePrcsStep = 8;
													break;	
													
												case 0x00:	//지문 입력 대기 않함
												default:
													BioModuleOff();
													FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//미디 에러음 출력
													ModeClear();
													break;
											}
											break;
										case 0x00:	//명령 없음
										case 0x20:	//인증
										case 0x30:	//삭제
										case 0x40:	//지문 LIST 보고
										case 0x60:	//보안 레벨 변경 
										case 0x70:	//상태
										case 0x80:	//결과
										case 0x90:	//취소
										case 0xA0:	//Version
										default:
											gbModePrcsStep = 200;	 //Check the fingerprint module.
											break;
									}
									
									 break;
									 
								case 0x80:	//Idle 상태
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}							
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류							
						case 0x0F:	//명령 실패(원래 수신 되는 값은 0x1F이지만 하위 니블만 가공 하여 쓴다)
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
				else if (gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{								
								case 0x01:	//Enrollment 결과 성공
									BioModuleOff();
									bTmp = FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID = gbFingerPrint_rxd_buf[7];
									gbFID--; //slot number 를 저장할 ROM index 처리 
									no_fingers++;					// 등록 수 증가..
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
									RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); //등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gbFID);
#endif
									RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
									no_fingers = FingerReadBuff[0];

									memset(bTmpArray, 0x00, 8);
									CopyCredentialDataForPack(bTmpArray, 8);
									PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)bTmp);
			
									gbBioTimer10ms = BIO_WAKEUP_TM;
									if(no_fingers >= _MAX_REG_BIO){ 				// 20개 모두 등록시 등록 완료.
										AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
										LedSetting(gcbLedOk, LED_DISPLAY_OFF);
										BioModuleOff();
										ModeClear();
										break;
									}
									
									FeedbackKeyStarOn(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);	 //추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요
									
									gbModePrcsStep = 9;
									SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
									break;
									
								case 0x81:	//Enrollment 결과 실패
									gbModePrcsStep = 100;
									break;
									
								case 0x00:	//결과 없음
								case 0x02:	//Verify 결과 성공
								case 0x03:	//Delete 결과 성공
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									BioModuleOff();
									FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
									break;
							}
							break;
							
						case 0x1E:	//BUSY 상태(현재 명령 무시) Cancer명령어를 송신 하고 다시 인증을 시도함							
						case 0x1F:	//명령 실패
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						default:
							BioModuleOff();
							FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
							break;
					}
					break;
				}
			}
			FingerPrintRegisterDisplay();
			break;
		
		case 8: 
			if(GetModeTimeOut() == 0)
			{
				gbModePrcsStep=200;
				break;
			}
#if 0			
			if(gbBioTimer10ms == 0)
			{
				gbModePrcsStep = 200;	 //Check the fingerprint module.
				break;
			}
#endif 
			if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
			{		
				gbFingerPrint_rxd_end=0;
				if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{								
								case 0x01:	//Enrollment 결과 성공
									BioModuleOff();
									bTmp = FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID = gbFingerPrint_rxd_buf[7];
									gbFID--; //slot number 를 저장할 ROM index 처리 
									no_fingers++;					// 등록 수 증가..
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
									RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); //등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gbFID);
#endif
									RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
									no_fingers = FingerReadBuff[0];

									memset(bTmpArray, 0x00, 8);
									CopyCredentialDataForPack(bTmpArray, 8);
									PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)bTmp);
			
									gbBioTimer10ms = BIO_WAKEUP_TM;
									if(no_fingers >= _MAX_REG_BIO){ 				// 20개 모두 등록시 등록 완료.
										AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
										LedSetting(gcbLedOk, LED_DISPLAY_OFF);
										BioModuleOff();
										ModeClear();
										break;
									}
									
									FeedbackKeyStarOn(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);	 //추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요
									
									gbModePrcsStep = 9;
									SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
									break;
									
								case 0x81:	//Enrollment 결과 실패
									gbModePrcsStep = 100;
									break;
									
								case 0x00:	//결과 없음
								case 0x02:	//Verify 결과 성공
								case 0x03:	//Delete 결과 성공
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									BioModuleOff();
									FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
									break;
							}
							break;
							
						case 0x1E:	//BUSY 상태(현재 명령 무시) Cancer명령어를 송신 하고 다시 인증을 시도함							
						case 0x1F:	//명령 실패
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						default:
							BioModuleOff();
							FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
							break;
					}
				}
#if 0				
				if(gbFingerPrint_rxd_buf[4] == 0x87)
				{
					switch(gbFingerPrint_rxd_buf[5]&0x0F)
					{
						case 0x00:	//명령 성공 
							switch(gbFingerPrint_rxd_buf[5]&0xF0)
							{
								case 0x20:	//Command 수행중
								case 0x40:	//BUSY(지문 SCAN 대기중)
									switch(gbFingerPrint_rxd_buf[6]&0xF0)
									{
										case 0x10:	//등록
											switch(gbFingerPrint_rxd_buf[6]&0x0F)
											{
												case 0x01:	//1번째 지문 입력 완료
												case 0x02:	//2번째 지문 입력 완료
												case 0x03:	//3번째 지문 입력 완료		
													gbModePrcsStep=200;
													break;	
													
												case 0x00:	//지문 입력 대기 않함
												case 0x08:	//Image 에러
												default:
													BioModuleOff();
													FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
													break;
											}
											break;
										case 0x00:	//명령 없음
										case 0x20:	//인증
										case 0x30:	//삭제
										case 0x40:	//지문 LIST 보고
										case 0x60:	//보안 레벨 변경 
										case 0x70:	//상태
										case 0x80:	//결과
										case 0x90:	//취소
										case 0xA0:	//Version
										default:
											gbModePrcsStep = 200;	 //Check the fingerprint module.
											break;
									}
									
									 break;
									 
								case 0x80:	//Idle 상태
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}							
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류							
						case 0x0F:	//명령 실패(원래 수신 되는 값은 0x1F이지만 하위 니블만 가공 하여 쓴다)
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
#endif 				
			}
			break;

		case 9:			// 등록 연속  or 종료...시..
			switch(gbInputKeyValue){				//등록 버튼 눌린후 키가 입력되어 비밀번호 입력 시작

				case FUNKEY_REG:						// 등록을 종료 하려면....눌러...
					AudioFeedback(VOICE_STOP,	gcbBuzNum,VOL_CHECK);//(음성) into음 
					gbModePrcsStep = 10;
					break;
				
				case TENKEY_STAR:
					// 추가 등록
					gbBioTimer10ms = 10;
					gbModePrcsStep = 0;		
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_CHECK);//(음성) //Swipe the finger print 3 times.
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_NONE:				//key 눌림 없음
					if(GetModeTimeOut() == 0)
					{
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					break;
				
				default:
//					gbTenKeyCnt=0;
					break;
			}
			break;

		case 10:
			if(GetBuzPrcsStep())	break;
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			BioOffModeClear();
			break;

		case 100:
//			if(GetLedMode()) break;
			FeedbackError(AVML_ERR_FINGER_MSG1, VOL_CHECK); 	//The Fingerprint is not registered.
			BioOffModeClear();
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_CHECK); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;

			
		default:
			BioOffModeClear();
			break;

	}

}







#ifdef	BLE_N_SUPPORT
void	ModeGotoFingerRegisterByBleN( void )
{
	gbMainMode = MODE_MENU_FINGER_REGISTER_BYBLEN;
	gbModePrcsStep = 0;
	gbBioTimer10ms = 250;
}




void	ModeFingerRegisterByBleN( void )
{
	BYTE bTmp;
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
			if(gbBioTimer10ms==248){	// 고장 진단 후 처리 
				BioReTry();
			}
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if(no_fingers >= _MAX_REG_BIO){
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		//(음성)no more space to memorize.
				BioOffModeClear();
				break;
			}
			else if(no_fingers==0){
				AudioFeedback(AVML_ENTER_FINGER, gcbBuzSta, VOL_CHECK);			// 등록하실 지문을 차례로 입력 하세요.
			}
			else{
				AudioFeedback((AVML_FINGER01_REG_MSG-1+no_fingers), gcbBuzSta, VOL_CHECK);	//(음성) n개의 지문이 등록 되었습니다. 등록하실 지문을 차례로 세번 입력하세요.
			}

			BioModuleON();
			LedModeRefresh();

			FingerReadBuff[0] = 0;		// 1의 자리
			FingerReadBuff[1] = 0;		// 10의 자리
			FingerReadBuff[2] = 0;		// 두자리 카운트
			FingerReadBuff[3] = 0;		// 임시 값 저장

			if(no_fingers == 0)
			{
			}
			else if(no_fingers < 10)
			{
				FingerReadBuff[0] = no_fingers;
			}
			else 
			{
				FingerReadBuff[1] = no_fingers / 10;
				FingerReadBuff[0] = no_fingers % 10;
			}			
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gbModePrcsStep ++;
			break;
			
		case 1:
			if(FingerReadBuff[2] >= 2)
			{
				FingerReadBuff[0] = 0;		// 1의 자리
				FingerReadBuff[1] = 0;		// 10의 자리
				FingerReadBuff[2] = 0;		// 두자리 카운트
				FingerReadBuff[3] = 0;		// 임시 값 저장
				gbModePrcsStep = 3;
				gbWakeUpMinTime10ms = 140;
			}
			else
			{
				if(FingerReadBuff[2] == 0)
					FingerReadBuff[3] = FingerReadBuff[1];
				else
					FingerReadBuff[3] = FingerReadBuff[0];

				LedPWLedSetting(FingerReadBuff[3]);
				gbModePrcsStep ++;
			}
			break;
		case 2:
			if(GetLedMode()) break;
			FingerReadBuff[2]++;
			gbModePrcsStep  --;
			break;
			
		case 3:
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(P_FP_DDL_RX_T)	//지문 모듈 Power On하고 약 100ms의 대기 시간을 가져야 지문 모듈이 완전히 Wakeup되며 Wakeup되었다는 증거로 RX Port가 High로 된다.
			{				
				FingerPrintTxEncryptionKeyMakeFrame();
				SetModeTimeOut(_MODE_TIME_OUT_7S);
				gbModePrcsStep = 4;
			}
			else
			{
				FingerPrintRetry();
			}
			break;

		case 4:
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[0] == 0xE2 &&  gbFingerPrint_rxd_buf[1] == 0xA1)
				{
					FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_SETSECURITY_LEVEL,SETSECURITY_LEVEL_VALUE_3,NO_FINGER_DATA);	//Enroll Command 송신 전 SETSECURITY_LEVEL을 3으로 먼저 설정 한다.
					gbModePrcsStep = 5;
					gbFingerRetryCounter = 0;
					SetModeTimeOut(_MODE_TIME_OUT_7S);
					//gbBioTimer10ms = 50;	//지문 등록 명령어 응답 대기 시간 100ms 설정 
					break;
				}
			}
			break;

		case 5:	//SETSECURITY_LEVEL설정 확인 루틴
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[4] == 0x86)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							//gbBioTimer10ms = BIO_WAKEUP_TM+750;
							SetModeTimeOut(_MODE_TIME_OUT_7S);
							gbModePrcsStep = 6;
#ifdef	DDL_CFG_SW_REG_ACTIVE_LOW
							if(!P_SW_REG_T)
#else 
							if(P_SW_REG_T)
#endif 
							{
#ifdef DDL_CFG_FP_TS1071M
								FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_ENROLL , FINGER_ALL_REGISTER , FINGER_ALL_REGISTER);
								gbFingerPrint1TimeEnrollMode = 0;
#else
								FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_ENROLL , 99 , 0xC1);
								gbFingerPrint1TimeEnrollMode = 1;
#endif 
							}
							else
							{
								FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT , CMD_ENROLL , FINGER_ALL_REGISTER , FINGER_ALL_REGISTER);
								gbFingerPrint1TimeEnrollMode = 0;
							}
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY						
						case 0x1F:	//명령 실패							
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
							
						case 0x11:	//Slot이 비어 있지 않음
							BioModuleOff();
							AudioFeedback(AVML_ERR_NOSPACE,	gcbBuzSta, VOL_CHECK);//(음성)no more space to memorize.
							break;
					}
				}		
			}	
			break;
			
		case 6:
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[4] == 0x81)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공 
							//gbBioTimer10ms = BIO_WAKEUP_TM+750;
							SetModeTimeOut(_MODE_TIME_OUT_20S);
							gbModePrcsStep = 7;
							gbFingerLongTimeWait100ms = 10;
							dbDisplayOn = 0x00;
							gbFingerRegisterDisplayStep = 0;
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류	
						case 0x1E:	//BUSY						
						case 0x1F:	//명령 실패							
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
							
						case 0x11:	//Slot이 비어 있지 않음
							BioModuleOff();
							AudioFeedback(AVML_ERR_NOSPACE,	gcbBuzSta, VOL_CHECK);//(음성)no more space to memorize.
							break;
					}
				}		
			}			
			break;
			
		case 7:		// PTENROLL SQ2
			if(GetModeTimeOut() == 0)
			{
				BioModuleOff();
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				ModeClear();
				break;
			}

			switch(gbInputKeyValue){
				case FUNKEY_OPCLOSE:
				case FUNKEY_REG:
					gbModePrcsStep = 201;
					break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == 0x87)
				{
					switch(gbFingerPrint_rxd_buf[5]&0x0F)
					{
						case 0x00:	//명령 성공 
							switch(gbFingerPrint_rxd_buf[5]&0xF0)
							{
								case 0x20:	//Command 수행중
								case 0x40:	//BUSY(지문 SCAN 대기중)
									switch(gbFingerPrint_rxd_buf[6]&0xF0)
									{
										case 0x10:	//등록
											SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
											switch(gbFingerPrint_rxd_buf[6]&0x0F)
											{
												case 0x01:	//1번째 지문 입력 완료		
													if(gbFingerPrint1TimeEnrollMode == 0)
													{
														AudioFeedback(AVML_REG_FINGER01_REIN, gcbBuzNum, VOL_CHECK);
														gbFingerRegisterDisplayStep = 1;
													}
													else
													{
														gbBioTimer10ms = 200;	//지문 등록 결과 Data 수신 대기 시간 1S 설정 
														gbModePrcsStep = 8;
													}
													break;
													
												case 0x02:	//2번째 지문 입력 완료
													AudioFeedback(AVML_REG_FINGER02_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 2;
													break;
													
												case 0x03:	//3번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER03_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 3;
													break;	
													
												case 0x04:	//4번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER04_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 4;
													break;	
													
												case 0x05:	//5번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER05_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 5;
													break;	
													
												case 0x06:	//6번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER06_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 6;
													break;	
													
												case 0x07:	//7번째 지문 입력 완료	
													AudioFeedback(AVML_REG_FINGER07_REIN, gcbBuzNum, VOL_CHECK); 
													gbFingerRegisterDisplayStep = 7;
													break;	
													
												case 0x08:	//8번째 지문 입력 완료	
													//gbFingerRegisterDisplayStep = 8;
													SetModeTimeOut(_MODE_TIME_OUT_7S);
													//gbBioTimer10ms = 200;	//지문 등록 결과 Data 수신 대기 시간 1S 설정 
													gbModePrcsStep = 8;
													break;	
													
												case 0x00:	//지문 입력 대기 않함
												default:
													BioModuleOff();
													FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//미디 에러음 출력
													ModeClear();
													break;
											}
											break;
										case 0x00:	//명령 없음
										case 0x20:	//인증
										case 0x30:	//삭제
										case 0x40:	//지문 LIST 보고
										case 0x60:	//보안 레벨 변경 
										case 0x70:	//상태
										case 0x80:	//결과
										case 0x90:	//취소
										case 0xA0:	//Version
										default:
											gbModePrcsStep = 200;	 //Check the fingerprint module.
											break;
									}
									
									 break;
									 
								case 0x80:	//Idle 상태
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}							
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류							
						case 0x0F:	//명령 실패(원래 수신 되는 값은 0x1F이지만 하위 니블만 가공 하여 쓴다)
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
				else if (gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{								
								case 0x01:	//Enrollment 결과 성공
									BioModuleOff();
									bTmp = FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID = gbFingerPrint_rxd_buf[7];
									gbFID--; //slot number 를 저장할 ROM index 처리 
									no_fingers++;					// 등록 수 증가..
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
									RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); //등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gbFID);
#endif
									RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
									no_fingers = FingerReadBuff[0];

									memset(bTmpArray, 0x00, 8);
									CopyCredentialDataForPack(bTmpArray, 8);
									PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)bTmp);
			
									gbBioTimer10ms = BIO_WAKEUP_TM;
									if(no_fingers >= _MAX_REG_BIO){ 				// 20개 모두 등록시 등록 완료.
										AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
										LedSetting(gcbLedOk, LED_DISPLAY_OFF);
										BioModuleOff();
										ModeClear();
										break;
									}
									
									FeedbackKeyStarOn(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);	 //추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요
									
									gbModePrcsStep = 9;
									SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
									break;
									
								case 0x81:	//Enrollment 결과 실패
									gbModePrcsStep = 100;
									break;
									
								case 0x00:	//결과 없음
								case 0x02:	//Verify 결과 성공
								case 0x03:	//Delete 결과 성공
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									BioModuleOff();
									FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
									break;
							}
							break;
							
						case 0x1E:	//BUSY 상태(현재 명령 무시) Cancer명령어를 송신 하고 다시 인증을 시도함							
						case 0x1F:	//명령 실패
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						default:
							BioModuleOff();
							FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
							break;
					}
					break;
				}
			}
			FingerPrintRegisterDisplay();
			break;
		
		case 8: 
			if(GetModeTimeOut() == 0)
			{
				gbModePrcsStep=200;
				break;
			}
#if 0			
			if(gbBioTimer10ms == 0)
			{
				gbModePrcsStep = 200;	 //Check the fingerprint module.
				break;
			}
#endif 
			if(gbFingerPrint_rxd_end == 1)	// 수신 완료 
			{		
				gbFingerPrint_rxd_end=0;
				if(gbFingerPrint_rxd_buf[4] == 0x88)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x00:	//명령 성공
							switch(gbFingerPrint_rxd_buf[6])
							{								
								case 0x01:	//Enrollment 결과 성공
									BioModuleOff();
									bTmp = FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID = gbFingerPrint_rxd_buf[7];
									gbFID--;//hyojoon  slot number 가 01 부터 시작함 index 는 0 번 부터 
									no_fingers++;					// 등록 수 증가..
									RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
									RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); //등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
									ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, gbFID);
#endif

									RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
									no_fingers = FingerReadBuff[0];

									memset(bTmpArray, 0x00, 8);
									CopyCredentialDataForPack(bTmpArray, 8);
									PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)bTmp);
									// 지문 등록 정보 전송
			
									gbBioTimer10ms = BIO_WAKEUP_TM;
									if(no_fingers >= _MAX_REG_BIO){ 				// 20개 모두 등록시 등록 완료.
										AudioFeedback(AVML_COMPLETE, gcbBuzReg, VOL_CHECK);	// Full 20 fingerprints have been registered
										LedSetting(gcbLedOk, LED_DISPLAY_OFF);
										BioModuleOff();
										ModeClear();
										break;
									}
									gbModePrcsStep = 10;
									break;
									
								case 0x81:	//Enrollment 결과 실패
									gbModePrcsStep = 100;
									break;
									
								case 0x00:	//결과 없음
								case 0x02:	//Verify 결과 성공
								case 0x03:	//Delete 결과 성공
								case 0x82:	//Verify 결과 실패
								case 0x83:	//Delete 결과 실패
								default:
									BioModuleOff();
									FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
									break;
							}
							break;
							
						case 0x1E:	//BUSY 상태(현재 명령 무시) Cancer명령어를 송신 하고 다시 인증을 시도함							
						case 0x1F:	//명령 실패
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Checksum 오류
						default:
							BioModuleOff();
							FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
							break;
					}
				}
#if 0				
				if(gbFingerPrint_rxd_buf[4] == 0x87)
				{
					switch(gbFingerPrint_rxd_buf[5]&0x0F)
					{
						case 0x00:	//명령 성공 
							switch(gbFingerPrint_rxd_buf[5]&0xF0)
							{
								case 0x20:	//Command 수행중
								case 0x40:	//BUSY(지문 SCAN 대기중)
									switch(gbFingerPrint_rxd_buf[6]&0xF0)
									{
										case 0x10:	//등록
											switch(gbFingerPrint_rxd_buf[6]&0x0F)
											{
												case 0x01:	//1번째 지문 입력 완료
												case 0x02:	//2번째 지문 입력 완료
												case 0x03:	//3번째 지문 입력 완료		
													gbModePrcsStep=200;
													break;	
													
												case 0x00:	//지문 입력 대기 않함
												case 0x08:	//Image 에러
												default:
													BioModuleOff();
													FeedbackError(VOICE_MIDI_ERROR, FEEDBACK_MODE_CLEAR);	//미디 에러음 출력
													break;
											}
											break;
										case 0x00:	//명령 없음
										case 0x20:	//인증
										case 0x30:	//삭제
										case 0x40:	//지문 LIST 보고
										case 0x60:	//보안 레벨 변경 
										case 0x70:	//상태
										case 0x80:	//결과
										case 0x90:	//취소
										case 0xA0:	//Version
										default:
											gbModePrcsStep = 200;	 //Check the fingerprint module.
											break;
									}
									
									 break;
									 
								case 0x80:	//Idle 상태
								default:
									gbModePrcsStep = 200;	 //Check the fingerprint module.
									break;
							}							
							break;
						
						case 0x01:	//전송 패킷 오류
						case 0x02:	//Check sum 오류							
						case 0x0F:	//명령 실패(원래 수신 되는 값은 0x1F이지만 하위 니블만 가공 하여 쓴다)
						default:
							gbModePrcsStep = 200;	 //Check the fingerprint module.
							break;
					}
				}
#endif 				
			}
			break;

		case 9:			// 등록 연속  or 종료...시..
			switch(gbInputKeyValue){				//등록 버튼 눌린후 키가 입력되어 비밀번호 입력 시작

				case FUNKEY_REG:						// 등록을 종료 하려면....눌러...
					AudioFeedback(VOICE_STOP,	gcbBuzNum, VOL_CHECK);//(음성) into음 
					gbModePrcsStep = 10;
					break;
				
				case TENKEY_STAR:
					// 추가 등록
					gbBioTimer10ms = 10;
					gbModePrcsStep = 3;		
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_CHECK);//(음성) //Swipe the finger print 3 times.
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_NONE:				//key 눌림 없음
					if(GetModeTimeOut() == 0)
					{
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					break;
				
				default:
//					gbTenKeyCnt=0;
					break;
			}
			break;

		case 10:
			if(GetBuzPrcsStep())	break;
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			BioOffModeClear();
			break;

		case 100:
//			if(GetLedMode()) break;
			FeedbackError(AVML_ERR_FINGER_MSG1, VOL_CHECK); 	//The Fingerprint is not registered.
			BioOffModeClear();
			break;

		case 200:		//통신 중  수신 이나 송신 을 시간 내에 못하는 경우.. 에러  처리 부
			FeedbackError(AVML_CHECK_FPM_MSG, VOL_HIGH); //			Check the fingerprint module.
			BioOffModeClear();

			gbBIOErrorCNT++;
			if(gbBIOErrorCNT >=5){
				gbBIOErrorCNT=5;
			}
			break;

		case 201:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//
			BioOffModeClear();
			break;

			
		default:
			BioOffModeClear();
			break;

	}

}
#endif

