//------------------------------------------------------------------------------
/** 	@file		modemenucredentialselet_T1.h
	@brief	Menu Mode 
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUCREDENTIALSELECT_T1_INCLUDED
#define __MODEMENUCREDENTIALSELECT_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"

void ModeGotoMenuAdvancedCredentialSelect(void);
void ModeGotoMenuNormalCredentialSelect(void);
void ModeGotoMenuCredentialSelect(void);
void AssignAdvancedCredentialSelectedMenu(void);
void AssignAdvancedCredentialSelectedMenu(void);
void AssignNormalCredentialSelectedMenu(void);
void MenuNormalCredentialSelectProcess(void);
void ModeGotoMenuAdvancedCredentialSelect(void);
void ModeMenuCredentialSelect(void);


#endif


