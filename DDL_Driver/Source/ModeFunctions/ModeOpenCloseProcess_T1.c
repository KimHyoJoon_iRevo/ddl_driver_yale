//------------------------------------------------------------------------------
/** 	@file		ModeOpenCloseProcess_T1.c
	@version 0.1.00
	@date	2016.04.19
	@brief	Open/Close Button Process 
	@remark	 열림/닫힘 버튼에 의한 동작 처리하는 모드
	@see	MainModeProcess_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.19		by Jay
			- 열림/닫힘 버튼에 의한 모터 열림/닫힘 수행
			- 내부강제잠금 설정 및 해제 처리 (추후 구현 예정)
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gInnerFrcedLockSettingCnt = 0;		/**< 내부강제잠금 설정 진입 Count 변수  */


//------------------------------------------------------------------------------
/** 	@brief	Motor Toggle run start
	@param 	None 
	@return 	None
	@remark 센서가 감지되면 센서에 따라 열림/닫힘 동작
	@remark 센서가 감지되지 않으면 이전 동작 방향과 반대로 열림/닫힘 동작
*/
//------------------------------------------------------------------------------
void StartMotorToggle(void)
{
	BYTE bTmp;

	PCErrorClearSet();

	bTmp = MotorSensorCheck();
	
	if(bTmp & SENSOR_LOCK_STATE)
	{
		PackAlarmReportInnerForcedLockOpenCloseFailSend();

		FeedbackLockIn();

#ifdef		P_SNS_LOCK_T	
		ModeClear();
#else
		gInnerFrcedLockSettingCnt = 0;

		SetModeProcessTime(250);
		gbModePrcsStep = MODEPRCS_INNER_LOCK_SET_CHECK;
#endif
	}
	else if(bTmp == SENSOR_OPEN_STATE)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		BatteryCheck();
#else
		FeedbackMotorClose();
#endif
		StartMotorClose();
	}
	else if(bTmp == SENSOR_CLOSE_STATE)
	{
		#ifdef CLOSE_ONLY_BUTTON		
		if(gbJigPackMotorControl == 0)	//Close 전용 Switch가 있는 제품의 경우 지그모드에서는 OPEN/CLOSE 모두 수행할수 있도록 추가 시작 //2017년09월06일 심재철 
		{
			if(JigModeStatusCheck() == STATUS_SUCCESS)
			{

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorOpen();
#endif
				StartMotorOpen();
			}
			else 
			{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorClose();
#endif
				StartMotorClose();
			}
		}
		else
		{

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorOpen();
#endif
			StartMotorOpen();
		}						//Close 전용 Switch가 있는 제품의 경우 지그모드에서는 OPEN/CLOSE 모두 수행할수 있도록 끝 시작 //2017년09월06일 심재철 
		#else
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		BatteryCheck();
#else
		FeedbackMotorOpen();
#endif
		StartMotorOpen();
		#endif
	}
	else
	{
		//When Motor is not opened, not closed, then Motor run to open or close 
		//on reference to previous Motor run
#ifdef CLOSE_ONLY_BUTTON	
		if(gbJigPackMotorControl == 0)	//Close 전용 Switch가 있는 제품의 경우 지그모드에서는 OPEN/CLOSE 모두 수행할수 있도록 추가 시작 //2017년09월06일 심재철 
		{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
			BatteryCheck();
#else
			FeedbackMotorClose();
#endif
			StartMotorClose();
		}
		else
		{
			switch(GetFinalMotorStatus())
			{
				case FINAL_MOTOR_STATE_CLOSE:
				case FINAL_MOTOR_STATE_CLOSE_ERROR:
					//When previous Motor run is close, then Motor run to open
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorOpen();
#endif
					StartMotorOpen();
					break;
				
				default:
					//When previous Motor run is open, then Motor run to close
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorClose();
#endif
					StartMotorClose();
					break;
			}
		}							//Close 전용 Switch가 있는 제품의 경우 지그모드에서는 OPEN/CLOSE 모두 수행할수 있도록 끝 시작 //2017년09월06일 심재철 
#else
		switch(GetFinalMotorStatus())
		{
			case FINAL_MOTOR_STATE_CLOSE:
			case FINAL_MOTOR_STATE_CLOSE_ERROR:
				//When previous Motor run is close, then Motor run to open
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorOpen();
#endif
				StartMotorOpen();
				break;
			
			default:
				//When previous Motor run is open, then Motor run to close
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorClose();
#endif
				StartMotorClose();
				break;
		}
#endif
	}
}


//------------------------------------------------------------------------------
/** 	@brief	Open/Close Mode Start
	@param	None
	@return 	None
	@remark 열림/닫힙 버튼 처리 모드 시작
*/
//------------------------------------------------------------------------------
void ModeGotoOpenClose(void)
{
	if(JigModeStatusCheck() == STATUS_FAIL) // OChyojoon.kim
	{
		gbMainMode = MODE_OPEN_CLOSE;
		gbModePrcsStep = MODEPRCS_OPENCLOSE_START_CHECK_KEY;
		SetModeProcessTime(250);	
	}
	else 
	{
		gbMainMode = MODE_OPEN_CLOSE;
		gbModePrcsStep = 0;
	}

#if defined (DDL_CFG_DISABLE_OC) 
	AlarmGotoAbnormalAlarm();
#endif 
	
}


//------------------------------------------------------------------------------
/** 	@brief	Motro Open/Close Complete Check
	@param	None
	@return 	None
	@remark 열림/닫힙 버튼에 의한 모터 열림/닫힘 결과 처리
*/
//------------------------------------------------------------------------------
void MotorOpenCloseByKeyCompleteCheck(void)
{
	BYTE bTmp;

	bTmp = GetFinalMotorStatus();
	
	if(bTmp == FINAL_MOTOR_STATE_OPEN_ING)		return;
	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ING) 	return;

#if 0
	if(GetMotorPrcsStep())break; //180917 moonsw 백턴기다리고 피드백은 너무 늦어서 해당 조건 삭제함
#endif
	
	if(bTmp == FINAL_MOTOR_STATE_OPEN_ERROR)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
	
		// Pack Module로 Event 내용 전송 
		PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x01);
		gbModePrcsStep = MODEPRCS_OPEN_BYKEY_COMPLETE_ERROR;
		return;
	}
	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
	
		// Pack Module로 Event 내용 전송 
		PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x00);
		gbModePrcsStep = MODEPRCS_OPEN_BYKEY_COMPLETE_ERROR;
		return;
	}
	else if(bTmp == FINAL_MOTOR_STATE_OPEN)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		FeedbackMotorOpen();
#endif 		
		// Pack Module로 Event 내용 전송 
		PackTx_MakeAlarmPacket(AL_MANUAL_UNLOCKED, 0x00, 0x02);
	
	}
	else if(bTmp == FINAL_MOTOR_STATE_CLOSE)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		FeedbackMotorClose();
#endif 		
		// Pack Module로 Event 내용 전송 
		PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x03);
	}

#ifdef		P_SNS_LOCK_T	
	ModeClear();
#else

#ifdef	P_SNS_EDGE_T			// Edge sensor
	if(gfDoorSwOpenState)
	{
		ModeClear();	
	}
	else
#endif
	{
		//문이 닫혀 있을 경우에만 내부강제잠금 설정 가능
		gInnerFrcedLockSettingCnt = 0;

		SetModeProcessTime(250);
		gbModePrcsStep = MODEPRCS_INNER_LOCK_SET_CHECK;
	}
#endif
}


#ifndef		P_SNS_LOCK_T	
void MotorCloseByLockoutCompleteCheck(void)
{
	BYTE bTmp;

	bTmp = GetFinalMotorStatus();
	
	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ING) 	return;

#if 0
	if(GetMotorPrcsStep())break; //180917 moonsw 백턴기다리고 피드백은 너무 늦어서 해당 조건 삭제함
#endif
	
	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR)
	{
		FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
	
		// Pack Module로 Event 내용 전송 
		PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x00);
		gbModePrcsStep = MODEPRCS_CLOSE_BYLOCKOUT_COMPLETE_ERROR;
		return;
	}
	else if(bTmp == FINAL_MOTOR_STATE_CLOSE)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		FeedbackMotorClose();
#endif
		// Pack Module로 Event 내용 전송 
#ifndef		P_SNS_LOCK_T	
		InnerForcedLockSet();
#endif		
		DDLStatusFlagClear(DDL_STS_OUTLOCK);
		PackTx_MakeAlarmPacket(AL_FORCED_LOCKED, 0x00, 0x00);
	}

	ModeClear();
}
#endif



//------------------------------------------------------------------------------
/** 	@brief	Open/Close Button Process Mode
	@param	None
	@return 	None
	@remark 열림/닫힙 버튼에 의한 동작 처리 과정
*/
//------------------------------------------------------------------------------
void ModeOpenClose(void)
{
	switch(gbModePrcsStep)
	{
		case MODEPRCS_OPENCLOSE_START_CHECK_KEY:
			if(GetModeProcessTime())
			{
#ifdef	P_SW_O_C_T		// Open/Close button OChyojoon.kim			
				if( gbNewOCFunctionKey == TENKEY_NONE && !P_SW_O_C_T)
#else 
				if( gbNewOCFunctionKey == TENKEY_NONE) // 애초에 이럴 경우는 없는데 compile 때문에 
#endif 
					gbModePrcsStep = MODEPRCS_OPENCLOSE_START_BYKEY;
			}
			else 
			{
#ifdef		P_SNS_LOCK_T	
				ModeClear();
#else
#ifdef	P_SNS_EDGE_T			// Edge sensor
				if(gfOutLock)
				{
					FeedbackLockOut();
					ModeClear();
					break;
				}
				
				// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
				if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
				{
					FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
					ModeClear();
					break;							
				}

				if(gfDoorSwOpenState)
				{
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();	
				}
				else
#endif
				{
					//문이 닫혀 있을 경우에만 내부강제잠금 설정 가능
					gInnerFrcedLockSettingCnt = 0;
					//SetModeProcessTime(250);
					gbModePrcsStep = MODEPRCS_INNER_LOCK_SET_CHECK;
				}
#endif
			}
			break;
			
		case MODEPRCS_OPENCLOSE_START_BYKEY:
			if(gfOutLock)
			{
				FeedbackLockOut();
				ModeClear();
				break;
			}

			// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
			if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
			{
				FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
				ModeClear();
				break;							
			}

			gbModePrcsStep = MODEPRCS_OPEN_BYKEY_COMPLETE_CHECK;
			StartMotorToggle();
		
			break;
				
		case MODEPRCS_OPEN_BYKEY_COMPLETE_CHECK:
			MotorOpenCloseByKeyCompleteCheck();
			break;

		case MODEPRCS_OPEN_BYKEY_COMPLETE_ERROR:
			if(GetLedMode())	break;
			
			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			else if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}
			ModeClear();
			break;

#ifndef		P_SNS_LOCK_T	
		case MODEPRCS_INNER_LOCK_SET_CHECK:
			if(GetMotorPrcsStep()) 	break;
			if(GetBuzPrcsStep())	break;                        
#ifdef	P_SNS_EDGE_T				
                          if(gbNewOCFunctionKey!= FUNKEY_OPCLOSE || gfDoorSwOpenState) // OChyojoon.kim
#else 
                          if(gbNewOCFunctionKey!= FUNKEY_OPCLOSE) // OChyojoon.kim
#endif 
                          {
                                  ModeClear();
                                  break;
                          }                                
			if(GetModeProcessTime())
			{
				break;
			}		
			SetModeProcessTime(50);
		
			gInnerFrcedLockSettingCnt++;
			FeedbackLockOutSetting(0xFF);		//No LED Display
			
			if(gInnerFrcedLockSettingCnt > 3)
			{
#ifdef	P_SNS_EDGE_T			
				if(gfDoorSwOpenState)
				{
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
				}
#endif 
				if(InnerForcedLockCheck())
				{
					InnerForcedLockClear();
		
					//FeedbackModeCompleted(VOICE_MIDI_OPEN, VOL_CHECK);
					FeedbackModeCompleted(AVML_IN_FORCE_UNLOCK, VOL_CHECK);					
					PackTx_MakeAlarmPacket(AL_FORCED_LOCKED, 0x00, 0x01);
					ModeClear();
				}
				else
				{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorClose();
#endif
					StartMotorClose();
					gbModePrcsStep = MODEPRCS_CLOSE_BYLOCKOUT_COMPLETE_CHECK;
				}
			}
			break;
		
		case MODEPRCS_CLOSE_BYLOCKOUT_COMPLETE_CHECK:
			MotorCloseByLockoutCompleteCheck();
			break;

		case MODEPRCS_CLOSE_BYLOCKOUT_COMPLETE_ERROR:
			if(GetLedMode())	break;
			
			// OPEN 상태가 유지된 상태에서 에러가 발생하게 되면 에러가 발생하지 않고, OPEN음이 나는 문제 해결을 위해 해당 내용 필요
			//	FINAL_MOTOR_STATE_CLOSE_ERROR 상태로 그냥 두게 되면 AutoRelockStartCheck 함수에서 OPEN 음 발생. 
			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			ModeClear();
			break;
#endif			
		default:
			ModeClear();
			break;
	}
}


