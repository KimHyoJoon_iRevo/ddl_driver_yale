//------------------------------------------------------------------------------
/** 	@file		ModeMenuAllCardDelete_T1.c
	@version 0.1.00
	@date	2016.04.26
	@brief	All Card Delete Mode
	@remark	전체 카드 삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.26		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"



void ModeGotoAllCardDelete(void)
{
	gbMainMode = MODE_MENU_ALLCARD_DELETE;
	gbModePrcsStep = 0;
}




void ProcessDeleteAllCard(void)
{
	switch(PincodeVerify(0))
	{
		case 1:
			AllCardDelete();

			// All Card Delete Event 전송

			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);			//	completed, Press the star for setting other options or press the "R" button to finish
			gbModePrcsStep++;


			PackTxEventCredentialDeleted(CREDENTIALTYPE_CARD, 0xFF);
			break;

		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	
			ModeClear();
			break;				
	}
}




void ModeAllCardDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			FeedbackKeyPadLedOn(AVML_IN_PIN_SHARP, VOL_CHECK); // 비밀번호를 입력하세요. / 계속하시려면 샵 버튼을 누르세요.
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:		 //# 버튼음 출력
					FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
					gbModePrcsStep++;
					break;		

				case TENKEY_STAR:
					if(GetSupportCredentialCount() > 1)
					{
						ReInputOrModeMove(ModeGotoMenuCredentialSelect);
					}
					else 
					{
						ReInputOrModeMove(ModeGotoMenuMainSelect);
					}					
					break;
					
				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessDeleteAllCard();
			break;		

		case 3:
			if(GetSupportCredentialCount() > 1)
			{
				GotoPreviousOrComplete(ModeGotoMenuCredentialSelect);
			}
			else 
			{
				GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			}			
			//위 함수호출로 mode clear가 일어날 수 있다
			break;

		default:
			ModeClear();
			break;
	}
}





