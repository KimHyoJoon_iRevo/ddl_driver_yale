//------------------------------------------------------------------------------
/** 	@file		ModeDS1972TouchKeyRegister_T1.c
	@version 0.1.00
	@date	2016.09.08
	@brief	DS1972 TouchKey Register Mode 
	@remark	터치키 등록 모드
	@see	MainModeProcess_T1.c
	@see	DS1972_Functions_T1.c
	@see	DS1972_IButtonProcess_T1.c	
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.09.08		by hyojoon
*/
//------------------------------------------------------------------------------

#include "Main.h"

BYTE gInputKeyNumberForRegister = 0;

void AllDS1972TouchKeyDataBufClear(void)
{
	iButtonBufInitial();
}

void ModeGotoDS1972TouchKeyRegister(void)
{
	gbMainMode = MODE_DS1972_TOUCHKEY_REGISTER;
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	gbModePrcsStep = 0;
}

//==============================================================
// Input        : void                                          
// Output       : 1 => 이미 등록한 UID가 아닐 경우           
//		  0 => 이미 등록한 UID일 경우		
//==============================================================
BYTE PrcsDS1972UIDCheck(void)
{
	if(gbiButtonReadStatus == IBTNREAD_SAME_UID)
	{
		gbiButtonReadStatus = 0;
		iButtonGotoUIDCheck();
		return 0;					//같은 ID가 있을 경우 (이미 등록했을 경우)
	}
	return 1;
}

void AllDS1972TouchKeyRegister()
{
	RomWrite(gbDS1972WriteBuf1, TOUCHKEY_UID, (SUPPORTED_DS1972KEY_NUMBER*MAX_KEY_UID_SIZE));
	RomWrite(gbDS1972WriteBuf2, TOUCHKEY_FV, (SUPPORTED_DS1972KEY_NUMBER*MAX_KEY_FV_SIZE));
	RomWrite(gbDS1972WriteBuf3, TOUCHKEY_RC, (SUPPORTED_DS1972KEY_NUMBER*MAX_KEY_RC_SIZE));	
	RomWrite(&gInputKeyNumberForRegister, (WORD)KEY_NUM, 1);           		
}

void AllDS1972TouchKeyDelete(void)
{
	RomWriteWithSameData(0xFF, TOUCHKEY_UID, (SUPPORTED_DS1972KEY_NUMBER*MAX_KEY_UID_SIZE));
	RomWriteWithSameData(0xFF, TOUCHKEY_FV, (SUPPORTED_DS1972KEY_NUMBER*MAX_KEY_FV_SIZE));
	RomWriteWithSameData(0xFF, TOUCHKEY_RC, (SUPPORTED_DS1972KEY_NUMBER*MAX_KEY_RC_SIZE));	
	RomWriteWithSameData(0x00, KEY_NUM,1);
}


void DS1972TouchKeyInputCheckForRegister(void)
{
	switch(gbInputKeyValue)
	{
		case FUNKEY_REG:
			gbModePrcsStep+=3;
			break;
			
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		default:
			if(DS1972TouchKeyInputCheck() == STATUS_SUCCESS)
			{
				gbModePrcsStep++;					//등록버튼 눌린 후 카드가 접촉되어 카드 저장 모드 진입
			}

			TimeExpiredCheck();
			break;
	}
}




void InputDS1972TouchKeyProcessLoop(void)
{
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

	//이미 등록한 것인지를 비교
	if(PrcsDS1972UIDCheck() == 0)
	{
		//Key is already registered in same process
		FeedbackErrorNumberOn(VOICE_MIDI_ERROR, VOL_CHECK, gInputKeyNumberForRegister);
		gbiButtonReadStatus = 0;
		gbModePrcsStep++;		
		return;
	}
	
	gInputKeyNumberForRegister++;
	gbiButtonReadStatus = 0;
	
	if(gInputKeyNumberForRegister >= SUPPORTED_DS1972KEY_NUMBER)
	{
		gbModePrcsStep+=2;		
		return;
	}

	FeedbackCredentialInput(VOICE_MIDI_BUTTON2, gInputKeyNumberForRegister);
	gbModePrcsStep++;	
}




//==============================================================//
// Input        : void                                          //
// Output       : void                                          //
//==============================================================//
void ModeDS1972TouchKeyRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			DS1972TouchKeyInputCheckForRegister();
			break;

		case 1:
			InputDS1972TouchKeyProcessLoop();
			break;			

		case 2:
			if(GetBuzPrcsStep() ||GetVoicePrcsStep())
			{
				SetModeProcessTime(100); // 등록이 check 된 이후 다음 key scan 까지 delay
				break;
			}
			
			TimeExpiredCheck();
				
			if(GetModeProcessTime() == 0x00)
			{
				gbModePrcsStep = 0;
			}
			break;

		case 3:	
			if(gInputKeyNumberForRegister == 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;
			}

			AllDS1972TouchKeyRegister();
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			gbModePrcsStep++;
			SetModeTimeOut(10);
			PackTxEventCredentialAdded(CREDENTIALTYPE_DS1972TOUCHKEY, 0xFF);

#ifdef	_MASTERKEY_IBUTTON_SUPPORT
			MasterKeyEnabledSet();
#endif
			break;
		
		case 4:
			if(GetModeProcessTime() == 0x00)
			{
				ModeClear();
			}	
			break;

		default:								
			ModeClear();
			break;
	}
}

#ifndef	_MASTERKEY_IBUTTON_SUPPORT

#ifdef	BLE_N_SUPPORT

void ModeGotoDS1972TouchKeyRegisterByBleN(void)
{
	FeedbackKeyPadLedOn(AVML_IN_0410PIN_R_CARD, VOL_CHECK);
	iButtonGotoRegModeStart();
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	gbMainMode = MODE_DS1972_TOUCHKEY_REGISTER_BYBLEN;
	gbModePrcsStep = 0;
}

void DS1972TouchKeyInputCheckForRegisterByBleN(void)
{
	switch(gbInputKeyValue)
	{
		case FUNKEY_REG:
			gbModePrcsStep+=3;
			break;
			
		case FUNKEY_OPCLOSE:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		default:
			if(DS1972TouchKeyInputCheck() == STATUS_SUCCESS)
			{
				gbModePrcsStep++;					//등록버튼 눌린 후 카드가 접촉되어 카드 저장 모드 진입
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			}
			else 
			{
				if(gInputKeyNumberForRegister) // 등록 한게 하나라도 있으면 timeout 이 5초 후 저장 
				{
					if(GetModeTimeOut() == 0x00)
					{
						gbModePrcsStep+=3;
					}
				}
				else // 등록이 없다 20초후 error 
					TimeExpiredCheck();
			}
			break;
	}
}

void InputDS1972TouchKeyProcessLoopByBleN(void)
{
	SetModeTimeOut(60); //gbModePrcsStep 2 에서 약 1초 소요 되니 6sec 를 주고 
	SetModeProcessTime(100); // 다음 key scan 까지 delay

	//이미 등록한 것인지를 비교
	if(PrcsDS1972UIDCheck() == 0)
	{
		//Key is already registered in same process
		FeedbackErrorNumberOn(VOICE_MIDI_ERROR, VOL_CHECK, gInputKeyNumberForRegister);
		gbiButtonReadStatus = 0;
		gbModePrcsStep++;		
		return;
	}
	
	gInputKeyNumberForRegister++;
	gbiButtonReadStatus = 0;
	
	if(gInputKeyNumberForRegister >= SUPPORTED_DS1972KEY_NUMBER)
	{
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		gbModePrcsStep+=2;		
		return;
	}

	FeedbackCredentialInput(VOICE_MIDI_BUTTON2, gInputKeyNumberForRegister);
	gbModePrcsStep++;	
}


void ModeDS1972TouchKeyRegisterByBleN(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
			DS1972TouchKeyInputCheckForRegisterByBleN();
			break;

		case 1:
			InputDS1972TouchKeyProcessLoopByBleN();
			break;			

		case 2:
			if(GetBuzPrcsStep() ||GetVoicePrcsStep())
			{
				SetModeProcessTime(100); // 등록이 check 된 이후 다음 key scan 까지 delay
				break;
			}
			
			if(GetModeProcessTime() == 0x00)
			{
				gbModePrcsStep = 0;
			}
			break;

		case 3:	
			if(gInputKeyNumberForRegister == 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;
			}

			AllDS1972TouchKeyRegister();
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			gbModePrcsStep++;
			PackTxEventCredentialAdded(CREDENTIALTYPE_DS1972TOUCHKEY, 0xFF);
			SetModeProcessTime(10);
			break;
		
		case 4:
			if(GetModeProcessTime() == 0x00)
			{
				ModeClear();
			}	
			break;

		default:								
			ModeClear();
			break;
	}
}


#endif 	// BLE_N_SUPPORT

#endif	// _MASTERKEY_IBUTTON_SUPPORT

