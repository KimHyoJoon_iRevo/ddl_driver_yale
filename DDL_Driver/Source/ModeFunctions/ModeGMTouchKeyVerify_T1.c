//------------------------------------------------------------------------------
/** 	@file		ModeGMTouchKeyVerify_T1.c
	@version 0.1.00
	@date	2016.05.11
	@brief	GateMan TouchKey Verify Mode 
	@remark	입력한 터치키를 저장된 터치키와 비교하여 처리하는 모드
	@see	MainModeProcess_T1.c
	@see	GMTouchKeyProcess_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.05.11		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"




void ModeGotoGMTouchKeyVerify(void)
{
	gbMainMode = MODE_GM_TOUCHKEY_VERIFY;
	gbModePrcsStep = 0;
}



BYTE GMTouchKeyVerify(void)
{
	BYTE bCnt;
	BYTE KeyUidData[MAX_KEY_UID_SIZE];
	
	CopySlotNumberForPack(0);
	memset(KeyUidData, 0xFF, MAX_KEY_UID_SIZE);
	CopyCredentialDataForPack(KeyUidData, 8);

	for(bCnt = 0; bCnt < SUPPORTED_USERKEY_NUMBER; bCnt++)
	{
		RomRead(KeyUidData, TOUCHKEY_UID+(MAX_KEY_UID_SIZE*(WORD)bCnt), MAX_KEY_UID_SIZE);
	
		if(DataCompare(KeyUidData, 0xFF, MAX_KEY_UID_SIZE) == _DATA_IDENTIFIED)
		{
			continue;
		}

		if(memcmp(KeyUidData, gbiButtonBuf, MAX_KEY_UID_SIZE) == 0)
		{
			CopySlotNumberForPack(bCnt+1);
			CopyCredentialDataForPack(KeyUidData, MAX_KEY_UID_SIZE);

			return (bCnt+1);
		}
	}
	
	CopyCredentialDataForPack(gbiButtonBuf, MAX_KEY_UID_SIZE);

	return _DATA_NOT_IDENTIFIED;
}




void InputGMTouchKeyVerify(void)
{
	PCErrorClearSet();

	if(GMTouchKeyVerify() == _DATA_NOT_IDENTIFIED)
	{
		TamperCountIncrease();
		FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
		gbModePrcsStep = 11;
		return;
	}					

#ifdef	BLE_N_SUPPORT
	/* all lock out check PIN 을 제외한 나머지는 slotnumber 를 0xFF 로 */ 
	if(AllLockOutStatusCheck(0xFF))
	{
		FeedbackAllCodeLockOut();
		gbModePrcsStep = 11;
		return;
	}
#endif

	if(AlarmStatusCheck() == STATUS_SUCCESS)
	{
		DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
		TamperCountClear();

		AlarmGotoAlarmClear();
		gbModePrcsStep = 11;
	}
	else
	{
		// GateMan TouchKey Unlock Event 전송
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		BatteryCheck();
#else
		FeedbackMotorOpen(); 
#endif
		StartMotorOpen();

		DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);
		TamperCountClear();
		gbModePrcsStep++;
	}
}


void MotorOpenCompleteByGMTouchKey(void)
{
	// Pack Module로 터치키에 의한 열림 Event 내용 전송 
	BYTE bTmpArray[MAX_KEY_UID_SIZE+2];

	// Pack Module로 카드에 의한 열림 Event 내용 전송 
	bTmpArray[0] = CREDENTIALTYPE_GMTOUCHKEY;
	bTmpArray[1] = GetSlotNumberForPack();
	GetCredentialDataForPack(&bTmpArray[2], MAX_KEY_UID_SIZE);
	PackTx_MakePacket(EV_USER_CREDENTIAL_UNLOCK, ES_LOCK_EVENT, &gbComCnt, bTmpArray, (MAX_KEY_UID_SIZE+2));

	gbModePrcsStep = 3;
}


void CancelConditionOfOutForcedLockByGMTouchKey(void)
{
	if(P_IBUTTON_T)
	{
		SetGMTouchKeyTime(100);
	}	
	
	if(GMTouchKeyTimeCheck() == STATUS_SUCCESS)
	{
		FeedbackBuzStopLedOff();

		GMTouchKeyInputClear();
		ModeClear();
	}
}



void OutForcedLockSetByGMTouchKey(void)
{
	BYTE bTmpArray[11];

	DDLStatusFlagSet(DDL_STS_OUTLOCK);

//	if(gbModuleMode == RF_TRX_MODULE_2WAY)
	{
		// Pack Module로 Event 내용 전송 
		bTmpArray[0] = 0xFF;		// No Code Data
		bTmpArray[1] = CREDENTIALTYPE_GMTOUCHKEY;
		bTmpArray[2] = GetSlotNumberForPack();
		memset(&bTmpArray[3], 0xFF, 8);
		GetCredentialDataForPack(&bTmpArray[3], MAX_KEY_UID_SIZE);
		PackTx_MakePacket(EV_ARM_REQEST, ES_LOCK_EVENT, &gbComCnt, bTmpArray, 11);
		
		SetModeProcessTime(100);
		gbModePrcsStep = 10;			
	}
}


//==============================================================//
// Input        : void                                          //
// Output       : 0 => EEPROM Write Running or UID 비교중       //
//		  1 => 일반 키로 등록되어 있는 UID일 경우	//
//		  2 => 등록되어 있지 않은 UID일 경우		//
//		  3 => 마스터 키로 등록되어 있는 UID일 경우	//
//==============================================================//
void ModeGMTouchKeyVerify(void)
{
	BYTE bTmp;
	
	switch(gbModePrcsStep)
	{
		case 0:
			if(AlarmStatusCheck() == STATUS_SUCCESS) //hyojoon_20161011
			{
				gbModePrcsStep++;
			}
			else 
			{
			// 등록된 터치키, 등록되지 않은 터치키 모두 내부강제잠금 처리
			switch(MotorSensorCheck())
			{
				case SENSOR_OPENLOCK_STATE:
				case SENSOR_CLOSELOCK_STATE:
				case SENSOR_CENTERLOCK_STATE:
				case SENSOR_LOCK_STATE:
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
					if(bTmp == SENSOR_CLOSELOCK_STATE)
					{
						FeedbackLockInOpenAllowState();
						gbModePrcsStep++;
					}
					else
					{
						PackAlarmReportInnerForcedLockOpenCloseFailSend();
						FeedbackLockIn();
						gbModePrcsStep = 11;
					}
#else 
					PackAlarmReportInnerForcedLockOpenCloseFailSend();
					FeedbackLockIn();
					gbModePrcsStep = 11;
#endif 
					break;
			
				default:
					gbModePrcsStep++;
					break;
			}
			}
			break;

		case 1:
			InputGMTouchKeyVerify();
			break;

		case 2:
			MotorOpenCompleteCheck(MotorOpenCompleteByGMTouchKey, 9);
			break;
						
		case 3:
			WaitStartOutForcedLockSetting(4);
			break;

		case 4:
			WaitSomeTimeByLongContact(CancelConditionOfOutForcedLockByGMTouchKey, 5);
			break;
		
		case 5:
			MotorCloseCompleteCheck(OutForcedLockSetByGMTouchKey, 9);
			break;

		case 9:
			if(GetLedMode())	break;
			
			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			else if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}
			gbModePrcsStep = 11;

		case 10:
			if(GetModeProcessTime()== 0)
			{
				PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);

				gbModePrcsStep++;
			}
			break;

		case 11:
			// Touch Key 떨어짐 확인
			if(P_IBUTTON_T)
			{
				SetGMTouchKeyTime(100);
			}	
			
			if(GMTouchKeyTimeCheck() == STATUS_SUCCESS)
			{
				GMTouchKeyInputClear();
				ModeClear();
			}	
			break;

		default:
			ModeClear();
			break;
	}
}



