//------------------------------------------------------------------------------
/** 	@file		ModeAlarmClearByKey_T1.c
	@brief	Alarm Clear By Long Key
*/
//------------------------------------------------------------------------------

#ifndef __MODEALARMCLEARBYKEY_T1_INCLUDED
#define __MODEALARMCLEARBYKEY_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoAlarmClearByKey(BYTE SetKey);
void ModeAlarmClearByKey(void);




#endif


