//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneCardRegDel_T1.c
	@brief	Card Individual Register/Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUONECARDREGDEL_T1_INCLUDED
#define __MODEMENUONECARDREGDEL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoOneCardRegDelCheck(void);
void ModeOneCardRegDelCheck(void);
void ModeOneCardRegister(void);
void ModeOneCardDelete(void);


#ifdef	BLE_N_SUPPORT
void ModeGotoOneCardRegisterByBleN(void);
void ModeOneCardRegisterByBleN(void);
#endif


#endif


