//------------------------------------------------------------------------------
/** 	@file		ModeMenuAllDS1972TouchKeyDelete_T1.c
	@brief	All GateMan Touch Key Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUALLDS1972TOUCHKEYDELETE_T1_INCLUDED
#define __MODEMENUALLDS1972TOUCHKEYDELETE_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoAllDS1972TouchKeyDelete(void);
void ModeAllDS1972TouchKeyDelete(void);

#endif


