//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneCardRegDel_T1.c
	@brief	Card Individual Register/Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUONECARDREGDEL_T1_INCLUDED
#define __MODEMENUONECARDREGDEL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoOneGMTouchKeyRegDelCheck(void);
void ModeOneGMTouchKeyRegDelCheck(void);
void ModeOneGMTouchKeyRegister(void);
void ModeOneGMTouchKeyDelete(void);


#ifdef	BLE_N_SUPPORT
void ModeGotoOneGMTouchKeyRegisterByBleN(void);
void ModeOneGMTouchKeyRegisterByBleN(void);
void ProcessDeleteOneGMTouchKeyByBleN(BYTE SlotNumber);
#endif


#endif


