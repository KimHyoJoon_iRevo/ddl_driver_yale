//------------------------------------------------------------------------------
/** 	@file		ModeMenuHandingLock_T1.c
	@brief	Handing Lock Setting
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUHANDINGLOCK_T1_INCLUDED
#define __MODEMENUHANDINGLOCK_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"



#define	HANDING_RIGHT_AUTO		0xCC 		// 자동 설정 우수
#define	HANDING_LEFT_AUTO		0xDD 		// 자동 설정 좌수
#define	HANDING_RIGHT_SET		0xC5 		// 수동 설정 우수
#define	HANDING_LEFT_SET		0xD2 		// 수동 설정 좌수

#define	HANDING_LOCK_START		0x55		// 데드볼트 도어록 좌우수 자동 설정


void ModeGotoHandingLockSetting(void);
void ModeHandingLockSetting(void);


BYTE GetHandingLock(void);
void HandingLockDefaultSet(BYTE bSetData);
void HandingLockDefaultToggle(void);
void HandingLockAutoRun(void);



#ifdef	LOCK_TYPE_DEADBOLT
BYTE HandingLockProcessStartCheck(void);
BYTE HaningLockProcessCheck(void);
void ModeHandingProcess(void);
#endif

#endif


