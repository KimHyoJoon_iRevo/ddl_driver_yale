//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneRemoconRegDel_T1.c
	@version 0.1.00
	@date	2016.06.09
	@brief	One Remocon Register/Delete Mode
	@remark	개별 리모컨 등록/삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.06.09		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"

// 실제 메모리에 저장되는게 아니라 별도 배치
#define	SUPPORTED_REMOCON_NUMBER		5
#define	SUPPORTED_BLEN_NUMBER			8

#if defined (DDL_CFG_REMOCON_PACK_REG_DEL_SUB_MENU)
void ModeGotoOneSubRemoconRegDelCheck(void)
{
	gbMainMode = MODE_MENU_ONESUBREMOCON_REGDEL;
	gbModePrcsStep = 0;
}


void ModeGotoOneSubRemoconRegister(void)
{
	gbMainMode = MODE_MENU_ONESUBREMOCON_REGISTER;
	gbModePrcsStep = 0;
}

void ModeGotoOneSubRemoconDelete(void)
{
	gbMainMode = MODE_MENU_ONESUBREMOCON_DELETE;
	gbModePrcsStep = 0;
}

void OneSubRemoconModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// 리모컨 개별 등록
		case 1: 	
			ModeGotoOneSubRemoconRegister();
			break;
	
		// 리모컨 개별 삭제
		case 3: 		
			ModeGotoOneSubRemoconDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuOneSubRemoconModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// 리모컨 개별 등록
		case TENKEY_1:			
//			MenuSelectKeyTempSaveNTimeoutReset(22, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_REMOCON_SHARP, gbInputKeyValue);	
			break;

		// 리모컨 개별 삭제
		case TENKEY_3:		
//			MenuSelectKeyTempSaveNTimeoutReset(24, gbInputKeyValue);	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_REMOCON_SHARP, gbInputKeyValue);	
			break;

		case TENKEY_SHARP:
			OneSubRemoconModeSetSelected();
			break;
									
		case TENKEY_STAR:
			ModeGotoOneRemoconRegDelCheck();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}




void ModeOneSubRemoconRegDelCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			Feedback13MenuOn(21, VOL_CHECK);
			Feedback13MenuOn(AVML_REGREMOCON1_DELREMOCON3, VOL_CHECK);
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuOneSubRemoconModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}
}




void SubRemoconSlotNumberToRegister(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();

	bTmp = SUPPORTED_REMOCON_NUMBER;

	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= bTmp))
	{
		FeedbackKeyPadLedOn(AVML_PRESS_REMOCONSET, VOL_CHECK); 			//리모콘에 있는 등록버튼을 누르세요

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_60S);

		gbModePrcsStep++;

		SubPackTxConnectionSend(EV_ALL_REGISTRATION, MODULE_TYPE_1_ONEWAY, gSelectedSlotNumber);
	}
	else
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}



void OneSubRemoconInputCheckForRegister(void)
{
	BYTE bTmp,bInnerTmp;
	
	switch(gbInputKeyValue)
	{
		case TENKEY_SHARP :
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				

		case TENKEY_STAR:
			SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);

			gbModePrcsStep = 0;
			break;

		default:
			bTmp = GetPackProcessResult();
			bInnerTmp = GetInnerPackProcessResult();
			if(bTmp == PACK_RMC_REGDEVICE_IN || bInnerTmp == PACK_RMC_REGDEVICE_IN)
			{
//				if(GetRemoconSlotNumber() == gSelectedSlotNumber)
				{
					gbModePrcsStep++;
				}			
			}
			
			if(bTmp != 0 || bInnerTmp != 0)
			{
				PackProcessResultClear();
			}

			TimeExpiredCheck();

			if(GetModeTimeOut() == 0)
			{
				SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);
			}	
			break;
	}
}



void InputOneSubRemoconProcessLoop(void)
{
	// 입력한 Slot Number 기준이 아닌 실제 등록되는 Slot Number 표시로 변경
//	FeedbackCredentialInput(VOICE_MIDI_BUTTON2, gSelectedSlotNumber);
	FeedbackCredentialInput(VOICE_MIDI_BUTTON2, GetRemoconSlotNumber());
	
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

	gbModePrcsStep++;
}



void ProcessRegisterOneSubRemocon(void)
{
	switch(gbInputKeyValue)
	{
		case TENKEY_SHARP:
			SubPackTxConnectionSend(EV_ALLREG_DEV_END, MODULE_TYPE_1_ONEWAY, 0);
	
			SetModeTimeOut(10);

			//# 버튼음 출력
#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;			

		case TENKEY_STAR:
			SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);
			ReInputOrModeMove(ModeGotoOneSubRemoconRegister);
			break;
		
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
			SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);

			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
			break;				
	
		default:
#if 0			
			if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)
			{
				// BLE-N의 경우 입력이 있을 경우에는 #을 누르지 않고 TimeOut이 되더라도 등록 처리하도록 프로그램 수정	
				if(GetModeTimeOut() == 0)
				{
					SubPackTxConnectionSend(EV_ALLREG_DEV_END, MODULE_TYPE_1_ONEWAY, 0);
			
					SetModeTimeOut(10);
					gbModePrcsStep++;
					break;			
				}
			}
			else
#endif 				
			{
				TimeExpiredCheck();
			}

			if(GetModeTimeOut() == 0)
			{
				SubPackTxConnectionSend(EV_ALLREG_DEV_CANCLE, MODULE_TYPE_1_ONEWAY, 0);
			}	
			break;
	}
}



void BackwardFunctionForOneSubRemoconRegDel(void)
{
	ReInputOrModeMove(ModeGotoOneSubRemoconRegDelCheck);
}



void RegisterOneSubRemocon(void)
{
	BYTE bTmp,bInnerTmp;
	
	bTmp = GetPackProcessResult();
	bInnerTmp = GetInnerPackProcessResult();
	if(bTmp == PACK_RMC_ALLREG_DEV_END || bInnerTmp == PACK_RMC_ALLREG_DEV_END)
	{
		if(bTmp)
			PackSaveID();

		if(bInnerTmp)
			InnerPackSaveID();

//		FeedbackModeCompletedKeepMode(42, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish
		FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		gbModePrcsStep++;
	}
	
	if(bTmp != 0 || bInnerTmp != 0)
	{
		PackProcessResultClear();
	}

	TimeExpiredCheck();
}

void ModeOneSubRemoconRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(8, VOL_CHECK);			//enter the number of the user to register the user number not the code, then press the hash to continue.
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			//enter the number of the user to register the user number not the code, then press the hash to continue.

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(SubRemoconSlotNumberToRegister, BackwardFunctionForOneSubRemoconRegDel,1);
			break;

		case 2:
			OneSubRemoconInputCheckForRegister();
			break;
		
		case 3:
			InputOneSubRemoconProcessLoop();
			break;

		case 4:
			ProcessRegisterOneSubRemocon();
			break;

		case 5:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			RegisterOneSubRemocon();
			break;

		case 6:
			GotoPreviousOrComplete(ModeGotoOneSubRemoconRegister);
			break;
		
		default:
			ModeClear();
			break;
	}
}





void ProcessDeleteOneSubRemocon(void)
{
	BYTE bTmp,bInnerTmp;
	
	bTmp = GetPackProcessResult();
	bInnerTmp = GetInnerPackProcessResult();
	if(bTmp == PACK_RMC_ALL_REMOVE_END ||bInnerTmp == PACK_RMC_ALL_REMOVE_END)
	{
//		FeedbackModeCompletedKeepMode(58, VOL_CHECK);		//completed Press the star for an additional deletion or press the "R" button to finish
		FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R2, VOL_CHECK);		//completed Press the star for an additional deletion or press the "R" button to finish
	
		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		gbModePrcsStep++;
	}
	
	if(bTmp != 0 || bInnerTmp != 0)
	{
		PackProcessResultClear();
	}

	TimeExpiredCheck();
}



void SubRemoconSlotNumberToDelete(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();

	bTmp = SUPPORTED_REMOCON_NUMBER;

#if 0	
	if(gfPackTypeiRevoBleN || gfInnerPackTypeiRevoBleN)	
	{
		bTmp = SUPPORTED_BLEN_NUMBER;
	}
#endif 	
		
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= bTmp))
	{						
		SubPackTxConnectionSend(EV_ALL_REMOVE, MODULE_TYPE_1_ONEWAY, gSelectedSlotNumber);
		
		SetModeTimeOut(15);

		//# 버튼음 출력
#ifdef	P_VOICE_RST	
		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
		gbModePrcsStep++;
	}
	else
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}



void ModeOneSubRemoconDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(49, VOL_CHECK);			//enter the number of the user to delete the user number not the code, then press the hash to continue.
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_FOR_DEL_SHARP, VOL_CHECK);			//enter the number of the user to delete the user number not the code, then press the hash to continue.

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;

		case 1:
			SelectSlotNumber(SubRemoconSlotNumberToDelete, BackwardFunctionForOneSubRemoconRegDel,0);
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessDeleteOneSubRemocon();
			break;
			
		case 3:
			GotoPreviousOrComplete(ModeGotoOneSubRemoconDelete);
			break;

		default:
			ModeClear();
			break;
	}
}
#endif 

