
#define		_MODE_FINGER_REGISTER_C_
//------------------------------------------------------------------------------
/** 	@file		ModeFingerRegister_T3.c
	@version 0.1.00
	@date	2018.03.29
	@brief	FingerPrint Register Mode 
	@remark	 User Code 등록 모드
	@see	MainModeProcess_T1.c
	@see	Finger_iRevo_HFPM.c
	@see	iRevo_HFPM_UART.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2018.03.29		by Hyojoon
*/
//------------------------------------------------------------------------------

#include "Main.h"

//------------------------------------------------------------------------------
/** 	@brief	FingerPrint Register Mode Start
	@param	None
	@return 	None
	@remark 지문 등록 모드 시작
*/
//------------------------------------------------------------------------------
void	ModeGotoFingerRegister( void )
{
	gbMainMode = MODE_MENU_FINGER_REGISTER;
	gbModePrcsStep = 0;
#ifdef	BLE_N_SUPPORT
	gbEnterMode = 0x00;
#endif 
}

#ifdef	BLE_N_SUPPORT
void	ModeGotoFingerRegisterByBleN( void )
{
	gbMainMode = MODE_MENU_FINGER_REGISTER;
	gbModePrcsStep = 0;
	gbEnterMode = 0x01;	
}
#endif 
//------------------------------------------------------------------------------
/** 	@brief	FingerPrint Register Mode 
PFM-3000 / TSM1071M / iRevo_HFPM normal slot number 관리 
Enroll 시에 slot number 를 0x00 으로 보내게 되면 Finger Module 에서는 module 에 저장된 번호(저장 된 수)를 돌려 준다 
ex) 아무 것도 없는 상태에서 최초 지문 등록시 0x01 을 return 하게 되어 있고 lock 에선 아래와 같이 관리 한다 


gbFID = module return 값 - 1;  // ROM 의 index 로 사용 
FingerWriteBuff = gbFID;          // gbFID 받을 값을 gbFID-1 index 에 gbFID 로 저장 not BCD

ROM
--------------------------------
|               BIO_ID + gbFID                  |
--------------------------------
|FingerWriteBuff[0] | FingerWriteBuff[1] |
--------------------------------

*/
//------------------------------------------------------------------------------

void	ModeFingerRegister( void )
{
	BYTE bTmp;
	BYTE bTmpArray[8];

	switch(gbModePrcsStep)
	{
		case 0:
			no_fingers = GetNoFingerFromEeprom();					// 현재 등록된 지문수 불러오기..
			if(no_fingers >= _MAX_REG_BIO){
				FeedbackError(AVML_ERR_NOSPACE, VOL_CHECK);		//(음성)no more space to memorize.
				BioOffModeClear();
				break;
			}
			else if(no_fingers==0){
				AudioFeedback(AVML_ENTER_FINGER, gcbBuzSta, VOL_CHECK);			// 등록하실 지문을 차례로 입력 하세요.
			}
			else{
				AudioFeedback((AVML_FINGER01_REG_MSG-1+no_fingers), gcbBuzSta, VOL_CHECK);	//(음성) n개의 지문이 등록 되었습니다. 등록하실 지문을 차례로 세번 입력하세요.
			}

			LedModeRefresh();

			FingerReadBuff[0] = 0;		// 1의 자리
			FingerReadBuff[1] = 0;		// 10의 자리
			FingerReadBuff[2] = 0;		// 두자리 카운트
			FingerReadBuff[3] = 0;		// 임시 값 저장

			if(no_fingers == 0)
			{
			}
			else if(no_fingers < 10)
			{
				FingerReadBuff[0] = no_fingers;
			}
			else 
			{
				FingerReadBuff[1] = no_fingers / 10;
				FingerReadBuff[0] = no_fingers % 10;
			}			
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			gbModePrcsStep ++;
			break;
			
		case 1:
			if(FingerReadBuff[2] >= 2)
			{
				FingerReadBuff[0] = 0;		// 1의 자리
				FingerReadBuff[1] = 0;		// 10의 자리
				FingerReadBuff[2] = 0;		// 두자리 카운트
				FingerReadBuff[3] = 0;		// 임시 값 저장
				gbModePrcsStep = 3;
				gbWakeUpMinTime10ms = 140;
			}
			else
			{
				if(FingerReadBuff[2] == 0)
					FingerReadBuff[3] = FingerReadBuff[1];
				else
					FingerReadBuff[3] = FingerReadBuff[0];

				LedPWLedSetting(FingerReadBuff[3]);
				gbModePrcsStep ++;
			}
			break;
			
		case 2:
			if(GetLedMode()) break;
			FingerReadBuff[2]++;
			gbModePrcsStep  --;
			break;

		case 3:
			HFPMTxEventEnroll(0x00);
			SetModeTimeOut(10);
			gbModePrcsStep = 5;
			break;
		
		case 5:
			if(GetModeTimeOut() == 0){
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				HFPMTxEventEnroll(0xFF);
				gbModePrcsStep = 200;
				break;
			}
			
			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[3] == 0x02 && gbFingerPrint_rxd_buf[4] == IREVO_HFPM_SUB_EVENT_FP_ENROLL)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case IREVO_HFPM_RESULT_CODE_STATUS_OK:
							SetModeTimeOut(_MODE_TIME_OUT_20S);
							gbModePrcsStep = 6;
							gbFingerLongTimeWait100ms = 10;
							dbDisplayOn = 0x00;
							gbFingerRegisterDisplayStep = 0;
							break;
						
						case IREVO_HFPM_RESULT_CODE_PACKET_ERR:	
						case IREVO_HFPM_RESULT_CODE_PROCESSING_ENROLL:	
						case IREVO_HFPM_RESULT_INVALID_SLOT_NUMBER_ENROLL:
						default :
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); 
							HFPMTxEventEnroll(0xFF);
							gbModePrcsStep = 200;
						break;
					}
				}		
			}			
			break;
			
		case 6:
			if(GetModeTimeOut() == 0){
				FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
				HFPMTxEventEnroll(0xFF);
				gbModePrcsStep = 200;
				break;
			}			

			if(gbInputKeyValue == FUNKEY_REG)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); 
				HFPMTxEventEnroll(0xFF);
				gbModePrcsStep = 200;
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				if(gbFingerPrint_rxd_buf[4] == IREVO_HFPM_SUB_EVENT_FP_ENROLL_STATUS_ALARM)
				{
					switch(gbFingerPrint_rxd_buf[5])
					{
						case 0x01:	//1번째 지문 입력 완료		
							HFPMTxEventEnrollAlarm();
							AudioFeedback(AVML_REG_FINGER01_REIN,gcbBuzNum, VOL_CHECK); // 지문이 한번 입력 되었습니다. 동일 지문을 다시 입력하세요.
							gbFingerRegisterDisplayStep = 1;
							SetModeTimeOut(_MODE_TIME_OUT_7S);							
							break;
							
						case 0x02:	//2번째 지문 입력 완료
							HFPMTxEventEnrollAlarm();
							AudioFeedback(AVML_REG_FINGER02_REIN, gcbBuzNum, VOL_CHECK); 
							gbFingerRegisterDisplayStep = 2;
							SetModeTimeOut(_MODE_TIME_OUT_7S);
							break;
							
						case 0x03:	//3번째 지문 입력 완료	
							HFPMTxEventEnrollAlarm();
							Delay(SYS_TIMER_15MS); 
							/* slotnumber 만 backup 하고 */
							FingerWriteBuff[0] = FingerWriteBuff[1] = gbFID = gbFingerPrint_rxd_buf[6];
							gbModePrcsStep = 20;
							break;	

						case 0xFF:
							FeedbackError(AVML_TIMEOUT, VOL_CHECK);		//	times up
							HFPMTxEventEnroll(0xFF);
							gbModePrcsStep = 200;
							break;

						case 0x00:	//지문 입력 대기 않함
						default:
							FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);	//미디 에러음 출력
							ModeClear();
							break;
					}
				}
			}
			else 
			{
				FingerPrintRegisterDisplay();
			}
			break;

		case 7:			// 등록 연속  or 종료...시..
			switch(gbInputKeyValue){				//등록 버튼 눌린후 키가 입력되어 비밀번호 입력 시작

				case FUNKEY_REG:						// 등록을 종료 하려면....눌러...
					AudioFeedback(VOICE_STOP,	gcbBuzNum,VOL_CHECK);//(음성) into음 
					gbModePrcsStep = 10;
					break;
				
				case TENKEY_STAR:
					// 추가 등록
					gbModePrcsStep = 0;		
					AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_CHECK);//(음성) //Swipe the finger print 3 times.
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_NONE:				//key 눌림 없음
					if(GetModeTimeOut() == 0)
					{
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					break;
				
				default:
					break;
			}
			break;

		case 20:
			SetModeTimeOut(10);
			HFPMTxEventPermanentKey();
			gbModePrcsStep = 21;
			break;

		case 21:
			if(GetModeTimeOut() == 0){
				FeedbackError(AVML_TIMEOUT, VOL_CHECK); 	//	times up
				gbModePrcsStep = 200;
				break;
			}

			if(gbFingerPrint_rxd_end)
			{
				gbFingerPrint_rxd_end = 0;
				
				if(gbFingerPrint_rxd_buf[4] == IREVO_HFPM_SUB_EVENT_PERMANENT_KEY &&
					gbFingerPrint_rxd_buf[5] == 0x01 && gbFingerPrint_rxd_buf[6] == 0x00)
				{
					HFPMSavePermanentKey();

					bTmp = gbFID;
					gbFID--; //slot number 를 저장할 ROM index 처리 
					no_fingers++; // 등록 수 증가..
					RomWrite(&no_fingers, (WORD)KEY_NUM, 1);				// 등록 수 저장//
					RomWrite(FingerWriteBuff, (WORD)(BIO_ID+(2*gbFID)), 2); //등록 ID 저장..	

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
					ScheduleDelete_Ble30(CREDENTIALTYPE_FINGERPRINT, ConvertFingerprintSlotNumber(gbFID));
#endif

					RomRead(FingerReadBuff, (WORD)KEY_NUM, 1);					//확인..
					no_fingers = FingerReadBuff[0];

					memset(bTmpArray, 0x00, 8);
					CopyCredentialDataForPack(bTmpArray, 8);
					PackTxEventCredentialAdded(CREDENTIALTYPE_FINGERPRINT, (WORD)bTmp);

#ifdef	BLE_N_SUPPORT
					if(no_fingers >= _MAX_REG_BIO || gbEnterMode) 				
#else 
					if(no_fingers >= _MAX_REG_BIO) 				
#endif 					
					{
						gbEnterMode = 0x00;
						FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
						BioOffModeClear();
					}
					else 
					{
					FeedbackKeyStarOn(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);	 //추가 등록을 원하시면 별표 버튼을 누르시고, 종료를 원하시면 R버튼을 누르세요
					gbModePrcsStep = 7;
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
				}
				}
				else
				{
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK); 	//	times up
					gbModePrcsStep = 200;
				}
			}
		break;

		case 10:
			if(GetBuzPrcsStep())	break;
			FeedbackModeCompleted(AVML_COMPLETE, VOL_CHECK);
			BioOffModeClear();
			break;

		case 200:		 
		default:			
			BioOffModeClear();
			break;
	}
}

