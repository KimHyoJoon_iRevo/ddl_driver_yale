//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneRemoconRegDel_T1.h
	@brief	One Remocon Register/Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUONESUBREMOCONREGDEL_T1_INCLUDED
#define __MODEMENUONESUBREMOCONREGDEL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoOneSubRemoconRegDelCheck(void);
void ModeOneSubRemoconRegDelCheck(void);
void ModeOneSubRemoconRegister(void);
void ModeOneSubRemoconDelete(void);

#endif


