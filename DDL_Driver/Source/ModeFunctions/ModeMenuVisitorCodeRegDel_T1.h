//------------------------------------------------------------------------------
/** 	@file		ModeMenuVisitorCodeRegDel_T1.h
	@brief	Visitor Code Register/Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUVISITORCODEREGDEL_T1_INCLUDED
#define __MODEMENUVISITORCODEREGDEL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoVisitorCodeRegDelCheck(void);
void ModeVisitorCodeRegDelCheck(void);
void ModeVisitorCodeRegister(void);
void ModeVisitorCodeDelete(void);

void VisitorCodeDelete(void);


#endif


