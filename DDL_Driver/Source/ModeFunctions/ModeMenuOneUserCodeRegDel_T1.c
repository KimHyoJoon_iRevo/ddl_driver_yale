//------------------------------------------------------------------------------
/** 	@file		ModeMenuOneUserCodeRegDel_T1.c
	@version 0.1.01
	@date	2016.05.26
	@brief	User Code Individual Register/Delete Mode
	@remark	사용자 비밀번호 개별 등록/삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.05.17		by Jay

		V0.1.01 2017.05.26		by MoonSungWoo
		- 개별 등록시 슬롯 넘버 입력 후, #키에 대한 효과음 추가
			=> void SelectSlotNumber(void(*SlotNumberProcess)(void), void(*BackwardProcess)(void),BYTE RegDelSelect)  함수수정
			=> switch-case문 추가하여 MIDI_INTO 효과음 재생 후에 SlotNumberProcess 진행되게 수정 
			=> SelectSlotNumber 함수 수정으로 [사용자개별등록][리모컨개별등록][지문개별등록][카드개별등록][터치키개별등록] , 개별 등록 모두 적용 됨 
*/
//------------------------------------------------------------------------------

#include "Main.h"

BYTE gbSlotNumberSharpMidiPrcs = 0;	//슬롯 넘버 입력 후, #키에 대한 효과음 재생을 위한 switch-case, Process 변수 

void ModeGotoOneUserCodeRegDelCheck(void)
{
	gbMainMode = MODE_MENU_ONEUSERCODE_REGDEL;
	gbModePrcsStep = 0;
}


void ModeGotoOneUserCodeRegister(void)
{
	gbMainMode = MODE_MENU_ONEUSERCODE_REGISTER;
	gbModePrcsStep = 0;
}

void ModeGotoOneUserCodeDelete(void)
{
	gbMainMode = MODE_MENU_ONEUSERCODE_DELETE;
	gbModePrcsStep = 0;
}


void OneUserCodeModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// User Code 개별 등록
		case 1: 	
			ModeGotoOneUserCodeRegister();
			break;
	
		// User Code 개별 삭제
		case 3: 		
			ModeGotoOneUserCodeDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuOneUserCodeModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// User Code 개별 등록
		case TENKEY_1:			
//			MenuSelectKeyTempSaveNTimeoutReset(53, gbInputKeyValue);	// Register a User code, press the hash to continue
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_PIN_MODE_SHARP, gbInputKeyValue);	// Register a User code, press the hash to continue
			break;

		// User Code 개별 삭제
		case TENKEY_3:		
//			MenuSelectKeyTempSaveNTimeoutReset(9, gbInputKeyValue);		// Delete a user code, press the hash to continue
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_PIN_SHARP, gbInputKeyValue);		// Delete a user code, press the hash to continue
			break;

		case TENKEY_SHARP:
			OneUserCodeModeSetSelected();
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}




void ModeOneUserCodeRegDelCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			Feedback13MenuOn(7, VOL_CHECK);			// press 1 to register a user code press 3 to delete a code
			Feedback13MenuOn(AVML_REGUPIN1_DELUPIN3, VOL_CHECK);			// press 1 to register a user code press 3 to delete a code
			
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuOneUserCodeModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}
}





void ProcessRegisterOneUserCode(void)
{
	BYTE bTmp;

	bTmp = PincodeVerify(0);
	switch(bTmp)
	{
		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		case RET_TEMP_CODE_MATCH:
		case RET_MASTER_CODE_MATCH:
//			FeedbackErrorModeKeepOn(109, VOL_CHECK);		//	That code is already in use
			FeedbackErrorModeKeepOn(AVML_ALREADY_USE_PIN, VOL_CHECK);		//	That code is already in use
	//		ModeClear();
	
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			break;				

		case RET_NO_MATCH:
		default:
			if((bTmp != RET_NO_MATCH) && (bTmp != gSelectedSlotNumber))
			{
//				FeedbackErrorModeKeepOn(109, VOL_CHECK);		//	That code is already in use
				FeedbackErrorModeKeepOn(AVML_ALREADY_USE_PIN, VOL_CHECK);		//	That code is already in use
		//		ModeClear();
		
				TenKeyVariablesClear();
				SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
				break;
			}
			
#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;
	}
}



void OneUserCodeRegDelProcess(void)
{
//	FeedbackModeCompletedKeepMode(42, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);		//completed, Press the star for an additional registration or press the "R" button to finish
	
	gbModePrcsStep++;
}



BYTE ConvertInputKeyToByte(void)
{
	BYTE bTmp1;
	BYTE bTmp2;

	bTmp2 = (gPinInputKeyFromBeginBuf[0] >> 4) & 0x0F;
        
	if(GetInputKeyCount() == 2)
	{
		bTmp1 = gPinInputKeyFromBeginBuf[0] & 0x0F;
		bTmp2 = (bTmp2 * 10) + bTmp1;
	}

    return bTmp2;
}


BYTE ConvertByteToInputKey(BYTE bData)
{
	BYTE bTmp;

	bTmp = (bData/10) << 4;
	bTmp += (bData%10);

	return (bTmp);
}


void UserCodeSlotNumberToRegister(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();

#if (SUPPORTED_USERCODE_NUMBER == 100)	
	if(gSelectedSlotNumber ==0x00) gSelectedSlotNumber = 100;
#endif 	
	
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= SUPPORTED_USERCODE_NUMBER))
	{						
//		FeedbackKeyPadLedOn(45, VOL_CHECK); 			//Enter a 4 to 10 digits usercode, then press the hash to continue
		FeedbackKeyPadLedOn(AVML_IN_0410PIN_SHARP, VOL_CHECK); 			//Enter a 4 to 10 digits usercode, then press the hash to continue

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

		gbModePrcsStep++;
	}
	else
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}


void SelectSlotNumber(void(*SlotNumberProcess)(void), void(*BackwardProcess)(void),BYTE RegDelSelect)
{
	switch(gbSlotNumberSharpMidiPrcs)  //# TENKEY_SHARP에 대한 효과음 재생을 위한 case문 추가 case 0:, case 1:
	{
		case 0:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(2, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
		//				FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
				//		ModeClear();
				
						TenKeyVariablesClear();
						SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
						break;
					}

					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:			
#ifdef P_VOICE_BUSY_T					
					if(RegDelSelect)
					{
						AudioFeedback(VOICE_MIDI_INTO,	gcbBuzSta, VOL_CHECK);   //슬롯 별로 등록시 #tenkey에 대한  INTO MIDI 효과음 재생,2017.05.26 moonsungwoo
						gbSlotNumberSharpMidiPrcs++;
					}
					else 					
#endif 																							 //등록 시에만 재생됨, 삭제 시에는 기능별로 이미 기존 함수<SlotNumberProcess()>에 효과음 재생 포함됨
						SlotNumberProcess();	//효과음 재생 완료 후 동작 하게 수정,2017.05.26 moonsungwoo
					break;

				case TENKEY_STAR:
#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
					FPMotorCoverAction(_CLOSE); // 메뉴 뒤로 가기 시에 지문 모터커버가 OPEN상태면 닫아준다.  
#endif					
					BackwardProcess();
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
		//			FeedbackError(139, VOL_CHECK);
					FeedbackError(((RegDelSelect == 1) ? AVML_ERR_CANCLE_REG : AVML_ERR_CANCLE_DEL), VOL_CHECK);
					ModeClear();
					break;
			}
			break;

		case 1:
			if(GetVoicePrcsStep()||GetBuzPrcsStep())break; 
			SlotNumberProcess();	 //효과음 재생 후, 등록 삭제 프로세스 진행(카드, 지문, 리모컨.....)
			gbSlotNumberSharpMidiPrcs = 0;		 //gbSharpMidi 변수 초기화
			break;
	}

}



void BackwardFunctionForOneUserCodeRegDel(void)
{
	ReInputOrModeMove(ModeGotoOneUserCodeRegDelCheck);
}


void RegisterOneUserCode(void)
{		
	// Slot Number는 1부터 시작, 저장 번지 Index는 0부터 시작
	UpdatePincodeToMemory(USER_CODE+(((WORD)gSelectedSlotNumber-1)*8));

	SetUserStatus(gSelectedSlotNumber, USER_STATUS_OCC_ENABLED);

// 도어록에서 비밀번호를 새로 등록하거나 변경하는 경우에는 기존 Schedule 관련 정보 모두 초기화
	// Schedule status 영역 삭제 
	SetScheduleStatus(gSelectedSlotNumber, SCHEDULE_STATUS_DISABLE);

	// Schedule 영역 삭제 
	if((gSelectedSlotNumber-1) < 30U)
		RomWriteWithSameData(0xFF, USER_SCHEDULE+(((WORD)gSelectedSlotNumber-1)*4), 4); 

	// User Code 추가 Eevnt 전송
	PackTxEventPincodeAdded((WORD)gSelectedSlotNumber);

#ifdef	DDL_CFG_DIMMER
	FeedbackModeCompletedLedOff(VOICE_MIDI_CANCLE, VOL_CHECK);

	SetModeProcessTime(150);
	PrepareDataForDisplayPincode();
	gbModePrcsStep++;
#else
//	FeedbackModeCompletedKeepMode(1, VOL_CHECK);
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R, VOL_CHECK);
	gbModePrcsStep+=2;
#endif

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	ScheduleDelete_Ble30(CREDENTIALTYPE_PINCODE, gSelectedSlotNumber);
#endif
}



void ReInputOneUserCode(void)
{
//	FeedbackKeyPadLedOn(45, VOL_CHECK); 			//Enter a 4 to 10 digits usercode, then press the hash to continue
	FeedbackKeyPadLedOn(AVML_IN_0410PIN_SHARP, VOL_CHECK); 			//Enter a 4 to 10 digits usercode, then press the hash to continue
}

void ModeOneUserCodeRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(8, VOL_CHECK);			// enter the number of the user to register, the user number not the code, then press the hash to continue
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_SHARP, VOL_CHECK);			// enter the number of the user to register, the user number not the code, then press the hash to continue

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;
			
		case 1:
			SelectSlotNumber(UserCodeSlotNumberToRegister, BackwardFunctionForOneUserCodeRegDel,1);
			break;

		case 2:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					ProcessRegisterOneUserCode();
					break;		

				case TENKEY_STAR:
					// 새로운 비밀번호 입력 받는 부분의 실행 부분이 "gbModePrcsStep = 0"가 아니라 해당 부분만 별도 함수 파생 처리
					ReInputOrModeMove_1(ModeGotoOneUserCodeRegister, ReInputOneUserCode);
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 3:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			RegisterOneUserCode();
			break;

// PASSWORD DISPLAY RTN
		case 4:
			DisplayRegisteredPincode(OneUserCodeRegDelProcess);
			break;
			
		case 5:
			GotoPreviousOrComplete(ModeGotoOneUserCodeRegister);
			break;
		
		default:
			ModeClear();
			break;
	}
}





void ProcessDeleteOneUserCode(void)
{
	// Slot Number는 1부터 시작, 저장 번지 Index는 0부터 시작
#if defined (_USE_IREVO_CRYPTO_)
	BYTE bTemp[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};

	EncryptDecryptKey(bTemp, 8);
	RomWrite(bTemp, USER_CODE+((gSelectedSlotNumber-1)*8), 8);	
#else 
	RomWriteWithSameData(0xFF, USER_CODE+((gSelectedSlotNumber-1)*8), 8);
#endif 

	// User status 영역 삭제
	SetUserStatus(gSelectedSlotNumber, USER_STATUS_AVAILABLE);

	// Schedule status 영역 삭제 
	SetScheduleStatus(gSelectedSlotNumber, SCHEDULE_STATUS_DISABLE);

	// Schedule 영역 삭제 
	if((gSelectedSlotNumber-1) < 30U)
		RomWriteWithSameData(0xFF, USER_SCHEDULE+((gSelectedSlotNumber-1)*4), 4); 


/*
	bTmp = 0x00;
	RomWrite(&bTmp, USER_SCHEDULE_STA+1, 1); 

*/
//	FeedbackModeCompletedKeepMode(58, VOL_CHECK);		// Completed, Press the star for an additional deletion or press the "R"button to finish
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_ADDSTAR_R2, VOL_CHECK);		// Completed, Press the star for an additional deletion or press the "R"button to finish

	TenKeyVariablesClear();
	SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

	gbModePrcsStep++;

	// User Code 삭제  Eevnt 전송
	PackTxEventPincodeDeleted((WORD)gSelectedSlotNumber);

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	ScheduleDelete_Ble30(CREDENTIALTYPE_PINCODE, gSelectedSlotNumber);
#endif
}



void UserCodeSlotNumberToDelete(void)
{
	BYTE bTmp;

	bTmp = GetInputKeyCount();
	if(bTmp != 2)
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
		return;
	}

	gSelectedSlotNumber = ConvertInputKeyToByte();

#if (SUPPORTED_USERCODE_NUMBER == 100)	
	if(gSelectedSlotNumber ==0x00) gSelectedSlotNumber = 100;
#endif 	
	
	if((gSelectedSlotNumber != 0) && (gSelectedSlotNumber <= SUPPORTED_USERCODE_NUMBER))
	{						
		TenKeyVariablesClear();

		//# 버튼음 출력
#ifdef	P_VOICE_RST	
		FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
		gbModePrcsStep++;
	}
	else
	{
//		FeedbackErrorModeKeepOn(105, VOL_CHECK);		// That's not the right number of digits
		FeedbackErrorModeKeepOn(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
//		ModeClear();

		TenKeyVariablesClear();
		SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
	}
}




void ModeOneUserCodeDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(49, VOL_CHECK);			// enter the number of the user to delete the user number not the code, then press the hash to continue
			FeedbackKeyPadLedOn(AVML_IN_02SLOT_FOR_DEL_SHARP, VOL_CHECK);			// enter the number of the user to delete the user number not the code, then press the hash to continue

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gSelectedSlotNumber = 0;

			gbModePrcsStep++;
			break;

		case 1:
			SelectSlotNumber(UserCodeSlotNumberToDelete, BackwardFunctionForOneUserCodeRegDel,0);
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessDeleteOneUserCode();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoOneUserCodeDelete);
			break;

		default:
			ModeClear();
			break;
	}
}



