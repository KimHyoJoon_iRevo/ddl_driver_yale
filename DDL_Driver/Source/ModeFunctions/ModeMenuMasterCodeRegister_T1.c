//------------------------------------------------------------------------------
/** 	@file		ModeMenuMasterCodeRegister_T1.c
	@version 0.1.00
	@date	2016.05.17
	@brief	Master Code Register Mode
	@remark	관리자 비밀번호 등록 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.05.17		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gPinCodeTmpBuf[8];



void ModeGotoMasterCodeRegister(void)
{
	gbMainMode = MODE_MENU_MASTERCODE_REGISTER;
	gbModePrcsStep = 0;
}



void ProcessRegisterMasterCodeInputStep(void)
{
	switch(PincodeVerify(0))
	{
		case RET_MASTER_CODE_MATCH:
		case RET_NO_MATCH:
			memcpy(gPinCodeTmpBuf, gPinInputKeyFromBeginBuf, 8);
			
//			FeedbackKeyPadLedOn(51, VOL_CHECK);	// Enter the master code to verify, then press the hash to continue
			FeedbackKeyPadLedOn(AVML_REIN_MPIN_SHARP, VOL_CHECK);	// Enter the master code to verify, then press the hash to continue

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;
	
		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
//			FeedbackErrorModeKeepOn(109, VOL_CHECK);		//	That code is already in use
			FeedbackErrorModeKeepOn(AVML_ALREADY_USE_PIN, VOL_CHECK);		//	That code is already in use
	//		ModeClear();
	
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			break;				
	}
}



void ProcessRegisterMasterCodeConfirmStep(void)
{
	switch(PincodeVerify(0))
	{
		case RET_MASTER_CODE_MATCH:
		case RET_NO_MATCH:
			if(memcmp(gPinCodeTmpBuf, gPinInputKeyFromBeginBuf, 8) != 0)
			{
				FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
				ModeClear();
				break;				
			}
			
			UpdatePincodeToMemory(MASTER_CODE);

			// 기본 비밀번호가 등록될 경우 모터 잠김 금지를 위한 Factory Reset 정보 초기화
			FactoryResetClear();

			// Master Code 등록 Event 전송
			PackTx_MakeAlarmPacket(AL_CODE_CHANGED_ADDED, 0x00, 0x00);

#ifdef	DDL_CFG_DIMMER
			FeedbackModeCompletedLedOff(VOICE_MIDI_CANCLE, VOL_CHECK);
	
			SetModeProcessTime(150);
			PrepareDataForDisplayPincode();
			gbModePrcsStep++;
#else
//			FeedbackModeCompletedKeepMode(1, VOL_CHECK);
			FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
			gbModePrcsStep+=2;
#endif
			break;
	
		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
#if 1	/* 2회차 시도중 다른 비번과 동일하다는 말은 1회차 비번과 다른 다는 의미 이므로 error 종료 */
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
			ModeClear();
#else 
			FeedbackErrorModeKeepOn(AVML_ALREADY_USE_PIN, VOL_CHECK);		//	That code is already in use
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
#endif 			
			break;				
	}
}



void MasterCodeRegDelProcess(void)
{
//	FeedbackModeCompletedKeepMode(39, VOL_CHECK);
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
	
	gbModePrcsStep++;
}


void ModeMasterCodeRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(31, VOL_CHECK);
			FeedbackKeyPadLedOn(AVML_IN_0410MPIN_SHARP, VOL_CHECK);

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;
			
		case 1:
		case 2:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					if(gbModePrcsStep == 1)
					{
						ProcessRegisterMasterCodeInputStep();
					}
					else
					{
						//# 버튼음 출력
#ifdef	P_VOICE_RST	
						FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
						gbModePrcsStep++;
					}
					break;		

				case TENKEY_STAR:
					ReInputOrModeMove(ModeGotoMenuMainSelect);
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 3:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			ProcessRegisterMasterCodeConfirmStep();
			break;
			
// PASSWORD DISPLAY RTN
		case 4:
			DisplayRegisteredPincode(MasterCodeRegDelProcess);
			break;
			
		case 5:
			GotoPreviousOrComplete(ModeGotoMenuMainSelect);
			break;
		
		default:
			ModeClear();
			break;
	}
}





