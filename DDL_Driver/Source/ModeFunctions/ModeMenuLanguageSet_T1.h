//------------------------------------------------------------------------------
/** 	@file		ModeMenuLockSetting_T1.h
	@brief	Language setting 
	hyojoon_20160802
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENULANGUAGESET_T1_INCLUDED
#define __MODEMENULANGUAGESET_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"

void ModeGotoLanguageSetting(void);
void ModeLanguageSetting(void);

#endif


