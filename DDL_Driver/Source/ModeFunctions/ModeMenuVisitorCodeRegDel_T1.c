//------------------------------------------------------------------------------
/** 	@file		ModeMenuVisitorCodeRegDel_T1.c
	@version 0.1.00
	@date	2016.04.26
	@brief	Visitor Code Register/Delete Mode
	@remark	방문자 비밀번호 등록/삭제 모드
	@see	ModeMenuMainSelect_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.26		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"



void ModeGotoVisitorCodeRegDelCheck(void)
{
	gbMainMode = MODE_MENU_VISITORCODE_REGDEL;
	gbModePrcsStep = 0;
}


void ModeGotoVisitorCodeRegister(void)
{
	gbMainMode = MODE_MENU_VISITORCODE_REGISTER;
	gbModePrcsStep = 0;
}

void ModeGotoVisitorCodeDelete(void)
{
	gbMainMode = MODE_MENU_VISITORCODE_DELETE;
	gbModePrcsStep = 0;
}


void VisitorCodeModeSetSelected(void)
{
	switch(gMenuSelectKeyTempSave)
	{
		// Visitor Code 등록
		case 1: 	
			ModeGotoVisitorCodeRegister();
			break;
	
		// Visitor Code 삭제
		case 3: 		
			ModeGotoVisitorCodeDelete();
			break;
	
		default:
			// 메뉴 키 선택 없이 # 키를 눌렀을 경우 아무 처리 없이 모드 유지
			break;
	}			
}



void MenuVisitorCodeModeSelectProcess(void)
{
	switch(gbInputKeyValue)
	{
		// Visitor Code 등록
		case TENKEY_1:			
//			MenuSelectKeyTempSaveNTimeoutReset(10, gbInputKeyValue);	// register a visitor code, press the hash to continue. 	
			MenuSelectKeyTempSaveNTimeoutReset(AVML_REG_VPIN_SHARP, gbInputKeyValue);	// register a visitor code, press the hash to continue. 	
			break;

		// Visitor Code 삭제
		case TENKEY_3:		
//			MenuSelectKeyTempSaveNTimeoutReset(180, gbInputKeyValue);	// Delete a visitor code, press the hash to continue. 
			MenuSelectKeyTempSaveNTimeoutReset(AVML_DEL_VPIN_SHARP, gbInputKeyValue);	// Delete a visitor code, press the hash to continue. 
			break;

		case TENKEY_SHARP:
			VisitorCodeModeSetSelected();
			break;
									
		case TENKEY_STAR:
			ModeGotoMenuMainSelect();
			break;
	
		case FUNKEY_REG:
		case FUNKEY_OPCLOSE:
//			FeedbackError(141,VOL_CHECK); 	//	Menu mode has been cancelled.
			FeedbackError(AVML_ERR_CANCLE_MENU,VOL_CHECK); 	//	Menu mode has been cancelled.
			ModeClear();
			break;
		
		default:
			TimeExpiredCheck();
			break;
	
	}			
}




//------------------------------------------------------------------------------
/** 	@brief	Pincode Register Mode Start
	@param	None
	@return 	None
	@remark 비밀번호 등록 모드 시작
*/
//------------------------------------------------------------------------------
void ModeVisitorCodeRegDelCheck(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			Feedback13MenuOn(43, VOL_CHECK);				// press 1 to register a code press 3 to delete a code
			Feedback13MenuOn(AVML_REGPIN1_DELPIN3, VOL_CHECK);				// press 1 to register a code press 3 to delete a code

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;			
			break;

		case 1: 		
			MenuVisitorCodeModeSelectProcess();
			break;	
	
		default:
			ModeClear();
			break;
	}

}




//------------------------------------------------------------------------------
/** 	@brief	Register User Code Process
	@param	None
	@return 	None
	@remark 입력한 비밀번호 등록을 위한 확인 과정 및 등록 과정
*/
//------------------------------------------------------------------------------
void ProcessRegisterVisitorCode(void)
{
	switch(PincodeVerify(0))
	{
		case 2:
		case RET_NO_MATCH:
#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;
	
		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
//			FeedbackErrorModeKeepOn(109, VOL_CHECK);		//	That code is already in use
			FeedbackErrorModeKeepOn(AVML_ALREADY_USE_PIN, VOL_CHECK);		//	That code is already in use
//			ModeClear();
	
			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
			break;				
	}
}


void RegisterVisitorCode(void)
{
	UpdatePincodeToMemory(USER_CODE+8);

	SetUserStatus(2, USER_STATUS_OCC_ENABLED);

// 도어록에서 비밀번호를 새로 등록하거나 변경하는 경우에는 기존 Schedule 관련 정보 모두 초기화
	// Schedule status 영역 삭제 
	SetScheduleStatus(2, SCHEDULE_STATUS_DISABLE);

	// Schedule 영역 삭제 
	RomWriteWithSameData(0xFF, USER_SCHEDULE+4, 4); 


	// User Code 추가 Eevnt 전송
	PackTxEventPincodeAdded(0x02);

#ifdef	DDL_CFG_DIMMER
	FeedbackModeCompletedLedOff(VOICE_MIDI_CANCLE, VOL_CHECK);

	SetModeProcessTime(150);
	PrepareDataForDisplayPincode();
	gbModePrcsStep++;
#else
//	FeedbackModeCompletedKeepMode(1, VOL_CHECK);
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
	gbModePrcsStep+=2;
#endif

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	ScheduleDelete_Ble30(CREDENTIALTYPE_PINCODE, 2);
#endif
}


void VisitorCodeRegDelProcess(void)
{
//	FeedbackModeCompletedKeepMode(39, VOL_CHECK);
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);
	
	gbModePrcsStep++;
}


void ModeVisitorCodeRegister(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(11, VOL_CHECK);			// Enter only 4 digits visitor code,  then press the hash to continue
			FeedbackKeyPadLedOn(AVML_IN_VPIN_SHARP, VOL_CHECK);			// Enter only 4 digits visitor code,  then press the hash to continue

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gMenuSelectKeyTempSave = TENKEY_NONE;

			gbModePrcsStep++;
			break;
			
		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

#if 1	
	// 언어 설정이 필요 없는 제품도 중국과 다른 지역 FW를 나누어 관리하지 않도록 하기 위해 공용 처리
					if(TenKeySave(gPinInputMinLength, gbInputKeyValue, 0) == TENKEY_INPUT_OVER) 
#else
#ifdef	P_VOICE_RST // defined (DDL_CFG_SUPPORT_CHINESE)
					if(TenKeySave(gPinInputMinLength, gbInputKeyValue, 0) == TENKEY_INPUT_OVER) 
#else 
					if(TenKeySave(MAX_FIX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
#endif 						
#endif
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					ProcessRegisterVisitorCode();
					break;		

				case TENKEY_STAR:
					ReInputOrModeMove(ModeGotoVisitorCodeRegDelCheck);
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			RegisterVisitorCode();
			break;

// PASSWORD DISPLAY RTN
		case 3:
			DisplayRegisteredPincode(VisitorCodeRegDelProcess);
			break;
			
		case 4:
			GotoPreviousOrComplete(ModeGotoVisitorCodeRegDelCheck);
			break;
		
		default:
			ModeClear();
			break;
	}
}





void ProcessDeleteVisitorCode(void)
{
	switch(PincodeVerify(0))
	{
		case 1:
#ifdef	P_VOICE_RST	
			FeedbackTenKeyBlink(VOICE_MIDI_INTO, VOL_CHECK, gbInputKeyValue);
#endif
			gbModePrcsStep++;
			break;

		case RET_NO_INPUT:
		case RET_WRONG_DIGIT_INPUT:
//			FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
			FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
			ModeClear();
			break;				
	
		default:
			FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);		
			ModeClear();
			break;				
	}
}



void VisitorCodeDelete(void)
{
#if defined (_USE_IREVO_CRYPTO_)
	BYTE bTemp[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	EncryptDecryptKey(bTemp, 8);
	RomWrite(bTemp, USER_CODE+8, 8);
#else 	
	RomWriteWithSameData(0xFF, USER_CODE+8, 8);
#endif 
}

void DeleteVisitorCode(void)
{
	VisitorCodeDelete();

	// User status 영역 삭제 
	SetUserStatus(2, USER_STATUS_AVAILABLE);

	// Schedule status 영역 삭제 
	SetScheduleStatus(2, SCHEDULE_STATUS_DISABLE);

	// Schedule 영역 삭제 
	RomWriteWithSameData(0xFF, USER_SCHEDULE+4, 4); 

//	FeedbackModeCompletedKeepMode(39, VOL_CHECK);			//	completed, Press the star for setting other options or press the "R" button to finish
	FeedbackModeCompletedKeepMode(AVML_COMPLETE_STAR_R, VOL_CHECK);			//	completed, Press the star for setting other options or press the "R" button to finish
	gbModePrcsStep++;

	PackTxEventPincodeDeleted(0x02);
}




void ModeVisitorCodeDelete(void)
{
	switch(gbModePrcsStep)
	{
		case 0:
//			FeedbackKeyPadLedOn(16, VOL_CHECK);			//Enter the user code to delete the passcode, then press the hash to continue
			FeedbackKeyPadLedOn(AVML_FOR_DELPIN_IN_PIN_SHARP, VOL_CHECK);			//Enter the user code to delete the passcode, then press the hash to continue

			TenKeyVariablesClear();
			SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);

			gbModePrcsStep++;
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case TENKEY_0:
				case TENKEY_1:
				case TENKEY_2:
				case TENKEY_3:
				case TENKEY_4:
				case TENKEY_5:
				case TENKEY_6:
				case TENKEY_7:
				case TENKEY_8:
				case TENKEY_9:	
					FeedbackTenKeyBlink(VOICE_MIDI_BUTTON, VOL_CHECK, gbInputKeyValue);

					if(TenKeySave(MAX_PIN_LENGTH, gbInputKeyValue, 0) == TENKEY_INPUT_OVER)
					{
//						FeedbackError(105, VOL_CHECK);		// That's not the right number of digits
						FeedbackError(AVML_WRONG_NO_PIN, VOL_CHECK);		// That's not the right number of digits
						ModeClear();
						break;
					}
	
					SetModeTimeOut(_MODE_TIME_OUT_DEFAULT);
					break;

				case TENKEY_SHARP:
					ProcessDeleteVisitorCode();
					break;		

				case TENKEY_STAR:
					ReInputOrModeMove(ModeGotoVisitorCodeRegDelCheck);
					break;

				case TENKEY_NONE:
					TimeExpiredCheck();
					break;
				
				default:
					FeedbackError(VOICE_MIDI_ERROR, VOL_CHECK);
					ModeClear();
					break;
			}	
			break;

		case 2:
			if(GetBuzPrcsStep() || GetVoicePrcsStep() ) break;
			DeleteVisitorCode();
			break;

		case 3:
			GotoPreviousOrComplete(ModeGotoVisitorCodeRegDelCheck);
			break;

		default:
			ModeClear();
			break;
	}
}



