#define		_MODE_PACK_LOCK_OPERATION_C_

//------------------------------------------------------------------------------
/** 	@file		ModePackLockOperation_T1.c
	@version 0.1.00
	@date	2016.06.16
	@brief	Pack Lock Operation Mode
	@remark	RF에 의한 Lock Operation 동작 처리
	@see	MainModeProcess_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.06.16		by Hoon
*/
//------------------------------------------------------------------------------

#include "Main.h"



//------------------------------------------------------------------------------
/** 	@brief	Pack Lock Operation Mode Start
	@param	None
	@return 	None
	@remark RF에 의한 Lock Operation 동작 처리 시작
*/
//------------------------------------------------------------------------------
void	ModeGotoPackLockOperation( void )
{
	gbMainMode = MODE_PACK_LOCK_OPERATION;
	gbModePrcsStep = 0;
}



//------------------------------------------------------------------------------
/** 	@brief	Pack Lock Operation Mode
	@param	None
	@return 	None
	@remark RF에 의한 Lock Operation 동작 처리
*/
//------------------------------------------------------------------------------
void ModePackLockOperation(void)
{
	BYTE bTmp;
	switch(gbModePrcsStep)
	{
		case 0:
#ifdef	BLE_N_SUPPORT
			/* all lock out check PIN 을 제외한 나머지는 slotnumber 를 0xFF 로 */
			if(AllLockOutStatusCheck(0xFF))
			{
				FeedbackAllCodeLockOut();

				ModeClear();
				break;
			}
#endif
			PCErrorClearSet();

			//알람 발생시 알람 해제
			bTmp = AlarmStatusCheck();
			if(bTmp == STATUS_SUCCESS)
			{
				AlarmGotoAlarmClear();
			}

			//3분 락이 설정된 경우, 3분 락은 해제되고, 리모콘의 열림/닫힘 동작은 수행한다.
			if(GetTamperProofPrcsStep()) {
				DDLStatusFlagClear(DDL_STS_TAMPER_PROOF);
				TamperCountClear();
			}

			//내부 강제 잠금 이면 내부강제잠금 알림
			bTmp = MotorSensorCheck();
			if(bTmp & SENSOR_LOCK_STATE)
			{
#ifdef DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN			
				if(bTmp == SENSOR_CLOSELOCK_STATE || bTmp == SENSOR_OPENLOCK_STATE) 
				{
					FeedbackLockInOpenAllowState();
				}
				else
				{
					//PackAlarmReportInnerForcedLockOpenCloseFailSend();
					FeedbackLockIn();
					ModeClear();
					
					if(gbPackRxDataCopiedBuf[4] == 0xff ||gbInnerPackRxDataCopiedBuf[4] == 0xff)		// close door
					{
						PackTx_MakeAlarmPacket(AL_UNLOCK_FAIL_BY_FORCED_LOCKED, 0x00, 0x01);
					}
					else if( ((gbPackRxDataCopiedBuf[4] == 0x00) || (gbPackRxDataCopiedBuf[4] == 0x01)) ||
						((gbInnerPackRxDataCopiedBuf[4] == 0x00) || (gbInnerPackRxDataCopiedBuf[4] == 0x01))) 	// open door
					{
						PackTx_MakeAlarmPacket(AL_UNLOCK_FAIL_BY_FORCED_LOCKED, 0x00, 0x02);
					}

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
					else if((gbPackRxDataCopiedBuf[4] == 0x03) || (gbInnerPackRxDataCopiedBuf[4] == 0x03))		// close door
					{
						PackTx_MakeAlarmPacket(AL_UNLOCK_FAIL_BY_FORCED_LOCKED, 0x00, 0x01);
					}
					else if((gbPackRxDataCopiedBuf[4] == 0x02) || (gbInnerPackRxDataCopiedBuf[4] == 0x02))	// open door
					{
						PackTx_MakeAlarmPacket(AL_UNLOCK_FAIL_BY_FORCED_LOCKED, 0x00, 0x02);
					}
#endif	// DDL_CFG_BLE_30_ENABLE
					break;
				}
#else 	// DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN
				//PackAlarmReportInnerForcedLockOpenCloseFailSend();
				FeedbackLockIn();
				ModeClear();

				if(gbPackRxDataCopiedBuf[4] == 0xff ||gbInnerPackRxDataCopiedBuf[4] == 0xff)		// close door
				{
					PackTx_MakeAlarmPacket(AL_UNLOCK_FAIL_BY_FORCED_LOCKED, 0x00, 0x01);
				}
				else if( ((gbPackRxDataCopiedBuf[4] == 0x00) || (gbPackRxDataCopiedBuf[4] == 0x01)) ||
						((gbInnerPackRxDataCopiedBuf[4] == 0x00) || (gbInnerPackRxDataCopiedBuf[4] == 0x01)))  	// open door
				{
					PackTx_MakeAlarmPacket(AL_UNLOCK_FAIL_BY_FORCED_LOCKED, 0x00, 0x02);
				}

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
				else if((gbPackRxDataCopiedBuf[4] == 0x03) || (gbInnerPackRxDataCopiedBuf[4] == 0x03))		// close door
				{
					PackTx_MakeAlarmPacket(AL_UNLOCK_FAIL_BY_FORCED_LOCKED, 0x00, 0x01);
				}
				else if((gbPackRxDataCopiedBuf[4] == 0x02) || (gbInnerPackRxDataCopiedBuf[4] == 0x02))	// open door
				{
					PackTx_MakeAlarmPacket(AL_UNLOCK_FAIL_BY_FORCED_LOCKED, 0x00, 0x02);
				}
#endif	// DDL_CFG_BLE_30_ENABLE
				break;
#endif	// DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN 				
			}

			if(gbPackRxDataCopiedBuf[4] == 0xff ||gbInnerPackRxDataCopiedBuf[4] == 0xff)		// close door
			{
				// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
				if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
				{
					FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
					ModeClear();
					break;							
				}

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorClose();
#endif
				StartMotorClose();
			}
			else if( ((gbPackRxDataCopiedBuf[4] == 0x00) || (gbPackRxDataCopiedBuf[4] == 0x01)) ||
						((gbInnerPackRxDataCopiedBuf[4] == 0x00) || (gbInnerPackRxDataCopiedBuf[4] == 0x01)))		// open door
			{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorOpen();
#endif
				StartMotorOpen();
				
#ifdef	DDL_CFG_ADD_REMOCON_LINK_FUNCTION
				if(gfPackTypeiRevoBleN)
				{
					PackTxRemoconLinkSend(0x02);
				}
#endif
			}

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
			else if((gbPackRxDataCopiedBuf[4] == 0x03) || (gbInnerPackRxDataCopiedBuf[4] == 0x03))		// close door
			{
				// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
				if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
				{
					FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
					ModeClear();
					break;							
				}

				gfMute = 1;

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorClose();
#endif
				StartMotorClose();
			}
			else if((gbPackRxDataCopiedBuf[4] == 0x02) || (gbInnerPackRxDataCopiedBuf[4] == 0x02))		// open door
			{
				gfMute = 1;

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
				BatteryCheck();
#else
				FeedbackMotorOpen();
#endif
				StartMotorOpen();
			}
#endif	// DDL_CFG_BLE_30_ENABLE

			gbModePrcsStep = 100;
			// buffer 를 다 초기화 하지 말고 일단 쓰는 놈만 땡빵 
			gbPackRxDataCopiedBuf[4] = 0xAB;
			gbInnerPackRxDataCopiedBuf[4] = 0xAB;			
			break;

		case 100:
#if 0
			if(GetMotorPrcsStep())break; //180917 moonsw 백턴기다리고 피드백은 너무 늦어서 해당 조건 삭제함
#endif
			bTmp = GetFinalMotorStatus();

			switch( bTmp ) {
				case	FINAL_MOTOR_STATE_OPEN_ING:
				case	FINAL_MOTOR_STATE_CLOSE_ING:
					break;

				case	FINAL_MOTOR_STATE_OPEN_ERROR:
					FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);

					PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x01);
					gbModePrcsStep++;
					break;
			
				case	FINAL_MOTOR_STATE_CLOSE_ERROR:
					FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);

					PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x00);
					gbModePrcsStep++;
					break;

				case	FINAL_MOTOR_STATE_OPEN:
					DDLStatusFlagClear(DDL_STS_OUTLOCK | DDL_STS_TAMPER_PROOF);		//열림 동작 후 외부 강제 잠금 설정 해제

					TamperCountClear();		//열림 동작 후, 인증 시도 오류 count clear
					AutoRelockProcessClearCheck(); // mode 유지 시간이 짧아서 relock timer 초기화 안되는 문제 
					ModeClear();

					PackTx_MakeAlarmPacket(AL_RF_OP_UNLOCKED, 0x00, 0x01);
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					FeedbackMotorOpen();// 2018-6-18 moonsungwoo
#endif
					//아이레보 통신팩은 열림 상태에서 열림 요청에 의해 회신할 때는 Delay가 필요, 그렇지 않으면 통신팩이 수신하지 못함.
					// 기존 20ms Delay 삭제 후 아래 내용으로 100ms Delay 처리
					PackTxWaitTime_EnQueue(10);
					InnerPackTxWaitTime_EnQueue(10);					
					break;

				case	FINAL_MOTOR_STATE_CLOSE:
					// 잠김 동작에는 외부강제잠금 설정은 해제되지 않음.
					DDLStatusFlagClear(DDL_STS_TAMPER_PROOF);		

					TamperCountClear();		//열림 동작 후, 인증 시도 오류 count clear
					ModeClear();
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					FeedbackMotorClose();// 2018-6-18 moonsungwoo 
#endif
					PackTx_MakeAlarmPacket(AL_RF_OP_LOCKED, 0x00, 0x01);

					//아이레보 통신팩은 닫힘 상태에서 닫힘 요청에 의해 회신할 때는 Delay가 필요, 그렇지 않으면 통신팩이 수신하지 못함.
					// 기존 20ms Delay 삭제 후 아래 내용으로 100ms Delay 처리
					PackTxWaitTime_EnQueue(10);
					InnerPackTxWaitTime_EnQueue(10);
					break;
			}
			break;

		case 101:	
			if(GetLedMode())	break;
			
			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			else if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}
			ModeClear();
			break;

		default:	
			ModeClear();
			break;
	}
}





