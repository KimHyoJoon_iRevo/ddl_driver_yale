#define		_MODE_PACK_SET_LED_PROCESS_C_

#include "Main.h"

BYTE gbPackSetLedMode = 0x00;
BYTE gbPackSetLedColor = 0x00;
uint16_t gbPackSetLedTimer1s = 0x0000;
BYTE gbflashingTogle = 0x00;

void PackSetLedMode(BYTE * data)
{
	gbPackSetLedMode = data[0];
	gbPackSetLedColor = data[1];
	gbPackSetLedTimer1s = (uint16_t)(data[2] << 8 | data[3]);
	gbflashingTogle = 0x00;
}

void PackGetLedMode(BYTE * data)
{
	data[0] = gbPackSetLedMode;
	data[1] = gbPackSetLedColor;
	data[2] = (BYTE)((gbPackSetLedTimer1s & 0xFF00) >> 8);
	data[3] = (BYTE)(gbPackSetLedTimer1s & 0x00FF);
}

void 	PackLedControlCounter(void)
{
	BYTE Temp[4] = {0x00,};
	
	if(gbPackSetLedTimer1s)
	{
		if(GetMainMode() != MODE_PACK_SET_LED_PROCESS)
		{
			PackLedBuzzercontrol(0);
			PackSetLedMode(Temp);
			return;
		}		
		
		if(gbPackSetLedTimer1s == 0xFFFF)
		{
			if(gbPackSetLedMode == 0x01)
			{
				gbflashingTogle ^= 1;
			}
		}
		else
		{
			--gbPackSetLedTimer1s;

			if(gbPackSetLedTimer1s == 0x00)
			{
				PackLedBuzzercontrol(0);
				PackSetLedMode(Temp);
			}
		}
	}
}

void PackLedBuzzercontrol(BYTE OnOff)
{
	static BYTE preControlFlag = 0x00;

	if(OnOff)
	{
		if((gbPackSetLedColor & 0x80) == 0x80)
		{
			// buzzer
			if(GetBuzPrcsStep() == 0x00)
			{
				BuzzerSetting(gcbBuzPackLedControl, VOL_HIGH);
			}
		}

		if((gbPackSetLedColor & 0x40) == 0x40)
		{
			//keypad LED 
			if(preControlFlag != OnOff)
			{
				preControlFlag = OnOff;
#ifdef	DDL_CFG_DIMMER
				LedGenerate(LED_KEYPAD_ALL, DIMM_ON_SLOW, 0);
#endif
				
#ifdef	DDL_CFG_NO_DIMMER
				LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
				LedSetting(gcbNoDimLed2sOn, LED_KEEP_DISPLAY);
#endif
			}
		}

		switch(gbPackSetLedColor & 0x3F)
		{
			case 0x01: //red
			{
				
			}
			break;

			case 0x02: //blue
			{
				
			}
			break;

			case 0x03: // magenta
			{
					
			}
			break;

			case 0x04: // green
			{
				
			}
			break;

			case 0x05: // yellow
			{
				
			}
			break;

			case 0x06: // cyan
			{
					
			}
			break;

			case 0x07: // white
			{
					
			}
			break;

			case 0x3F: // lock specification
			{
					
			}
			break;
			
			default :
			break;
		}
			
	}
	else
	{
		preControlFlag = OnOff;
		BuzzerOff();
		LedModeRefresh();
#ifdef	DDL_CFG_DIMMER
		LedForcedAllOff();
#endif
										
#ifdef	DDL_CFG_NO_DIMMER
		LedSetting(gcbNoDimLedOff, LED_DISPLAY_OFF);
#endif
	}
}

void	ModeGotoPackSetLedProcess( void )
{
	gbMainMode = MODE_PACK_SET_LED_PROCESS;
	gbModePrcsStep = 0;
}

void ModePackSetLedProcess(void)
{
	BYTE Temp[4] = {0x00,};

	if(gbInputKeyValue == FUNKEY_REG ||
		gbInputKeyValue == TENKEY_MULTI ||
		gbInputKeyValue == TENKEY_STAR)
	{
		PackLedBuzzercontrol(0);
		PackSetLedMode(Temp);
		ModeClear();
		return;
	}
	
	switch(gbModePrcsStep)
	{
		case 0 : // all off 
		{
			PackLedBuzzercontrol(0);

			if(gbPackSetLedMode)
			{
				gbModePrcsStep = gbPackSetLedMode;				
			}
			else
			{
				ModeClear();
				PackSetLedMode(Temp);
			}
		}
		break;

		case 0x01 : // flashing
		{
			if(gbPackSetLedTimer1s == 0xFFFF)
			{
				// 연속 토글 
				if(gbflashingTogle)
				{
					PackLedBuzzercontrol(1);
				}
				else
				{
					PackLedBuzzercontrol(0);
				}
			}
			else
			{
				// 제한 토글 
				if(gbPackSetLedTimer1s%2)
				{
					PackLedBuzzercontrol(1);
				}
				else
				{
					PackLedBuzzercontrol(0);
				}
			}

			if(!gbPackSetLedTimer1s)
			{
				PackLedBuzzercontrol(0);
				PackSetLedMode(Temp);
				ModeClear();
			}
		}
		break;

		case 0xFF : // all on 
		{
			if(gbPackSetLedTimer1s)
			{
				PackLedBuzzercontrol(1);
			}
			else
			{
				PackLedBuzzercontrol(0);
				PackSetLedMode(Temp);
				ModeClear();
			}
		}
		break;

		default:
		break;
	}
}

