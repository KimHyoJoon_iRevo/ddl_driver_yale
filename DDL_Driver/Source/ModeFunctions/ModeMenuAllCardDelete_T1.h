//------------------------------------------------------------------------------
/** 	@file		ModeMenuAllCardDelete_T1.c
	@brief	All Card Delete Mode
*/
//------------------------------------------------------------------------------

#ifndef __MODEMENUALLCARDDELETE_T1_INCLUDED
#define __MODEMENUALLCARDDELETE_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"




void ModeGotoAllCardDelete(void);
void ModeAllCardDelete(void);

#endif


