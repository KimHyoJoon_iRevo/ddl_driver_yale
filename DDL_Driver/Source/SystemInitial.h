#ifndef __SYSTEMINITIAL_INCLUDED
#define __SYSTEMINITIAL_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"



void SystemInitial(void);

extern void UnusePinSetAnalog(void);	// 사용하지 않는 핀들



void ChangeDebugPinToAnalog(void);
void ChangeModelCheckPinToAnalog(void);


void SPI_On(void);
void SPI_Off(void);


void Touch_I2C_On(void);
void Touch_I2C_Off(void);
void Dimming_I2C_On(void);
void Dimming_I2C_Off(void);


#ifdef	P_TOUCH_O_C_T
void Touch_O_C_On(void);
void Touch_O_C_Off(void);
#endif


void ChangeMotorSensorPinToInput(void);
void ChangeMotorSensorPinToAnalog(void);


#ifdef	P_MECHA_HALL1_T
void ChangeAdditionalPositionCheckPinToAnalog(void);
void ChangeAdditionalPositionCheckPinToInput(void);
#endif	

void BOR_Config(void);


#endif
