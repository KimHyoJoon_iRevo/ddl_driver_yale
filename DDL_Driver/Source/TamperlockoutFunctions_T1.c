//------------------------------------------------------------------------------
/** 	@file		TamperlockoutFunctions_T1.c
	@version 0.1.00
	@date	2016.04.20
	@brief	3분락 관련 처리 함수
	@warning None
	@remark 3분락 설정, 처리, 해제 관련 함수
	@remark 버튼에 의한 3분락 해제 대기 내용도 포함
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.20		by Jay
			- 3분락 설정, 동작, 해제 내용 포함
*/
//------------------------------------------------------------------------------


#include "Main.h"


BYTE gTamperProofLockoutPrcsStep = 0;					/**< 3분락 동작 수행 단계  */	
uint16_t gTamperProofLockoutTimer1s = TAMPER_PROOF_LOCKOUT_TIME;					/**< 3분락 동작 수행 시간  */
BYTE gAuthFailCnt = 0;									/**< 인증 오류 회수  */


//------------------------------------------------------------------------------
/** 	@brief	Tamper Proof Lockout Mode Clear 
	@param 	None
	@return 	None
	@remark 3분락 모드 종료 및 인증 오류 회수를 초기화하고자 할 경우 사용
*/
//------------------------------------------------------------------------------
void TamperCountClear(void)
{
	gTamperProofLockoutPrcsStep = 0;
	gTamperProofLockoutTimer1s = 0;

	if(gAuthFailCnt)
	{
		gAuthFailCnt = 0;
		RomWrite(&gAuthFailCnt, WRONG_NUM, 1);
	}
}


//------------------------------------------------------------------------------
/** 	@brief	Wrong Code Count Increase 
	@param 	None
	@return 	None
	@remark 인증 오류 회수 증가
*/
//------------------------------------------------------------------------------
void TamperCountIncrease(void)
{
	if(AlarmStatusCheck() == STATUS_SUCCESS)
	{
		//경보 발생 중일 경우에는 인증 오류 회수 증가하지 않고, 초기화
		TamperCountClear();
	}
	else
	{
		gAuthFailCnt++;
		RomWrite(&gAuthFailCnt, WRONG_NUM, 1);
		
		// 3분 동안 도어록을 사용하지 않을 경우 인증 오류 회수(gAuthFailCnt)를 초기화하기 위해 (실제는 3분 이상 소요될 수 있음)
		gTamperProofLockoutTimer1s = TAMPER_PROOF_LOCKOUT_TIME + 120u;	 //재입력 대기 시간 5분 
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Load Wrong Code Count 
	@param 	None
	@return 	None
	@remark 인증 오류 회수 조회/설정
*/
//------------------------------------------------------------------------------
void TamperCountLoad(void)
{
	RomRead(&gAuthFailCnt, WRONG_NUM, 1);
}



//------------------------------------------------------------------------------
/** 	@brief	Wrong Code Count Clear if 3 minutes no access
	@param 	None
	@return 	None
	@remark 3분 동안 도어록을 사용하지 않을 경우 인증 오류 회수(gAuthFailCnt)를 초기화하기 위해 
			(실제는 3분 이상 소요될 수 있음)
*/
//------------------------------------------------------------------------------
void TamperCountClearCheck(void)
{
	if(GetMainMode())				return;
	if(gTamperProofLockoutTimer1s)	return;
	if(gAuthFailCnt == 0)			return;

	TamperCountClear();
}


//------------------------------------------------------------------------------
/** 	@brief	Get Tamper Proof Lockout Process Step
	@param 	None
	@return 	[gTamperProofLockoutPrcsStep] 현재 진행 중인 단계 표시
	@remark 3분락 진행 중인지 확인하기 위한 함수
*/
//------------------------------------------------------------------------------
BYTE GetTamperProofPrcsStep(void)
{
	return (gTamperProofLockoutPrcsStep);
}
	

//------------------------------------------------------------------------------
/** 	@brief	Tamper Proof Lockout Time Count
	@param 	None
	@return 	None
	@remark 3분락 시간 계산을 위한 Counter, 1s Tick
	@remark BasicTimer_1000ms_local 함수에 추가되어야 함.
	@remark Polling 처리 과정에도 내용 포함 필요
*/
//------------------------------------------------------------------------------
void TamperProofLockoutCounter(void)
{
	if(gTamperProofLockoutTimer1s) 		--gTamperProofLockoutTimer1s;
}	



//------------------------------------------------------------------------------
/** 	@brief	Tamper Proof Lockout Process
	@param 	None
	@return 	None
	@remark 3분락 실행 프로세스, 내부 버튼에 의한 해제 내용도 포함, Main 함수에서 수행
*/
//------------------------------------------------------------------------------
void TamperProofProcess(void)
{
	switch(gTamperProofLockoutPrcsStep)
	{
		case 0:
			if(gAuthFailCnt >= MAX_WRONG_CODE_ENTRY_LIMIT)
			{
				ModeClear();
				DDLStatusFlagSet(DDL_STS_TAMPER_PROOF);
				
				// 3분락 Event 전송
				PackTx_MakeAlarmPacket(AL_TAMPER_ALARM, 0x00, 0x01);

				gTamperProofLockoutTimer1s = TAMPER_PROOF_LOCKOUT_TIME;	
				
				gTamperProofLockoutPrcsStep++;

#ifdef DDL_CFG_FP_MOTOR_COVER_TYPE
				FPMotorCoverAction(_CLOSE);	//3분락 동작시, 커버 Close
#endif
			}	

			TamperCountClearCheck();
			break;

		case 1:
			switch(gbInputKeyValue)
			{
				case FUNKEY_REG:
				case FUNKEY_OPCLOSE:
					TamperCountClear();
					FeedbackModeClear();
					break;

				default:
					// 다른 장치에 의해 MainMode가 실행될 경우 3분 락 해제하도록 수정
					if(GetMainMode())
					{
						TamperCountClear();
						break;
					}

					if(gTamperProofLockoutTimer1s == 0)
					{
						TamperCountClear();
						FeedbackModeClear();
						break;
					}
					FeedbackTamperProofLockout(gTamperProofLockoutTimer1s);
					break;	
			}			
			break;			

		default:
			gTamperProofLockoutPrcsStep = 0;
			break;			
	}
}





