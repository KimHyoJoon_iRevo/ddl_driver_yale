//------------------------------------------------------------------------------
/** 	@file		MotorControl_T2.c
	@version 0.1.01
	@date	2016.09.05
	@brief	Doorlock Motor Control Engine
	@see	N:\1. 기술연구소\05 회로팀\04 사내 표준 규격\00 회로 설계 규격\펌웨어\하드웨어제어관련\Motor\
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00	2016.04.01		by Jay
			- 기존 MotorControl.c 프로그램에서 open 일 때와 close 일 때의 TO 설정을 다르게 할 수 있도록 
			   TO_open, TO_close로 분리하여 따로 처리하게끔 알고리즘 수정
		       - MotorOpenCloserTimeSetting 함수와 MotorBackTurnTimeSetting 함수도 각각 _open, _close 경우로 분리
		         (AL-Mecha 대응에 사용)
			- 신규 보조키 IH, DM+ 에도 Motor_RIM 파일만 수정하면 사용 가능하지만, 일단 분리하여 함수 구성

		V0.1.01 2016.09.05		by Jay
			- Open, Close 수행 중 데드볼트 걸림 발생할 경우 Back Turn 동작하도록 추가 

*/
//------------------------------------------------------------------------------

#define		_MOTOR_CONTROL_C_

#include	"main.h"

//모델에 1개의 모터(모티스)가 있으므로 제품에 들어간 모터 특성을 정의한 ddl_motor_ctrl_t를 등록한다.
ddl_motor_ctrl_t	* local_motor_ctrl;

void MotorAbnormalCheck(BYTE bUseVariable);

#ifdef M_CLOSE_PWMCH1_Pin

#define	_MOTOR_PWM_START		HAL_TIM_PWM_Start_IT( gh_mcu_motor_timer9, (local_motor_ctrl->Direction == MCTRL_DIR_OPEN ? TIM_CHANNEL_2 : TIM_CHANNEL_1))
#define	_MOTOR_PWM_STOP_CH1		HAL_TIM_PWM_Stop_IT( gh_mcu_motor_timer9, TIM_CHANNEL_1)
#define	_MOTOR_PWM_STOP_CH2		HAL_TIM_PWM_Stop_IT( gh_mcu_motor_timer9, TIM_CHANNEL_2)

uint16_t Get_PWM_Direction(void)
{
	return local_motor_ctrl->Direction;
}

void Motor_PWM_Init(uint16_t Mdirection)
{
	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;
	TIM_OC_InitTypeDef sConfigOC;
	GPIO_InitTypeDef GPIO_InitStruct;

	local_motor_ctrl->Direction = Mdirection;
	
	gh_mcu_motor_timer9->Instance = TIM9;
	gh_mcu_motor_timer9->Init.Prescaler = 0;
	gh_mcu_motor_timer9->Init.CounterMode = TIM_COUNTERMODE_UP;
	gh_mcu_motor_timer9->Init.Period = 1000;
	gh_mcu_motor_timer9->Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	HAL_TIM_Base_Init(gh_mcu_motor_timer9);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(gh_mcu_motor_timer9, &sClockSourceConfig);

	HAL_TIM_PWM_Init(gh_mcu_motor_timer9);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(gh_mcu_motor_timer9, &sMasterConfig);

	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = PWM_START_DUTY*10;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;

	HAL_TIM_PWM_ConfigChannel(gh_mcu_motor_timer9, &sConfigOC, (local_motor_ctrl->Direction == MCTRL_DIR_OPEN ? TIM_CHANNEL_2 : TIM_CHANNEL_1)); //open GPIO14 clsoe GPIO13
	SetMotorPWMFreq(); 

//	HAL_TIM_MspPostInit(gh_mcu_motor_timer9);

	if(local_motor_ctrl->Direction == MCTRL_DIR_OPEN)
	{
		GPIO_InitStruct.Pin = M_OPEN_PWMCH2_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		GPIO_InitStruct.Alternate = GPIO_AF3_TIM9;
		HAL_GPIO_Init(M_OPEN_PWMCH2_GPIO_Port, &GPIO_InitStruct);
#if 0		
		GPIO_InitStruct.Pin = M_CLOSE_PWMCH1_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
#endif 		
	}
	else 
	{
		GPIO_InitStruct.Pin = M_CLOSE_PWMCH1_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		GPIO_InitStruct.Alternate = GPIO_AF3_TIM9;
		HAL_GPIO_Init(M_CLOSE_PWMCH1_GPIO_Port, &GPIO_InitStruct);
#if 0				
		GPIO_InitStruct.Pin = M_OPEN_PWMCH2_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);		
#endif 		
	}
	tim9_count = 0;
	Delay(SYS_TIMER_2MS);
	_MOTOR_PWM_START;
}

void Motor_PWM_DeInit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	// 언제 어디서 걸릴지 모르니 두 체널 다 timer stop 
	_MOTOR_PWM_STOP_CH1;
	_MOTOR_PWM_STOP_CH2;
	
	HAL_TIM_Base_DeInit(gh_mcu_motor_timer9);
	HAL_TIM_PWM_DeInit(gh_mcu_motor_timer9);
	HAL_TIM_Base_MspDeInit(gh_mcu_motor_timer9);
	
	GPIO_InitStruct.Pin = M_CLOSE_PWMCH1_Pin |M_OPEN_PWMCH2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	P_M_CLOSE_2(0);
	P_M_OPEN_1(0);

	tim9_count = 0;
	local_motor_ctrl->Direction = 0xFFFF;
}
#endif 


//2개의 no-op function
static	void		void_nop_function( void )
{
}

static	uint32_t		uint32_nop_function( void )
{
	return	0;
}



uint32_t		register_motor_ctrl( ddl_motor_ctrl_t *mctrl )
{
	if ( (mctrl->motor_open == NULL) || (mctrl->motor_close == NULL) ||
		(mctrl->motor_stop == NULL) || (mctrl->motor_hold == NULL) ) {
		return	0;
	}
	local_motor_ctrl = mctrl;

 //함수 포인터 중 NULL인 것을 코드의 간결성을 위해 no-op function으로 대치한다.
	if ( !mctrl->init_sensor_hw )	mctrl->init_sensor_hw	= void_nop_function;
	if ( !mctrl->sensor_on )		mctrl->sensor_on		= void_nop_function;
	if ( !mctrl->sensor_off )		mctrl->sensor_off		= void_nop_function;

	if ( !mctrl->is_motor_open )	mctrl->is_motor_open		= uint32_nop_function;
	if ( !mctrl->is_motor_close )	mctrl->is_motor_close		= uint32_nop_function;
	if ( !mctrl->is_motor_center )	mctrl->is_motor_center	= uint32_nop_function;
	if ( !mctrl->is_motor_lock )	mctrl->is_motor_lock    = uint32_nop_function;

#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)
	mctrl->MotorException = 0x00;
	mctrl->Direction = 0xFFFF;
#endif 

	return	1;
}


void		emergence_motor_stop( void )
{
	local_motor_ctrl->motor_process_state = MPROC_STOP;

	(*local_motor_ctrl->motor_hold)();
	Delay( SYS_TIMER_100MS );
	(*local_motor_ctrl->motor_stop)();
	gwMotorCtrlTimer2ms = 0;				// clear MotorCtrl timer
}



static	uint16_t	get_next_mproc_state( void )
{
	uint16_t		curr_state = local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK;
	uint16_t		curr_dir = local_motor_ctrl->motor_process_state & MCTRL_DIR_MASK;
	uint16_t		next_state = MPROC_STOP;

	switch( curr_state ) {
//		case MPROC_STOP:
//		case MPROC_POST:
//			next_state = MPROC_STOP;
//			break;

		case MPROC_PRE:
			next_state = MPROC_TO_1;
			break;

		case MPROC_TO_1:
		case MPROC_TO_2:
		case MPROC_TO_3:
		case MPROC_TO_4:
		case MPROC_TO_5:
		case MPROC_TO_6:
		case MPROC_TO_7:
		case MPROC_TO_8:
			next_state = curr_state + 1;

			switch ( local_motor_ctrl->motor_process_state & MCTRL_DIR_MASK ) {
				case	MCTRL_DIR_OPEN:
					while ( (local_motor_ctrl->TO_open[ next_state - MPROC_TO_1 ] == 0) && (next_state<MPROC_TO_9) )
						next_state ++;

					if ( (next_state == MPROC_TO_9) && ( local_motor_ctrl->TO_open[next_state - MPROC_TO_1] == 0 ) )
						next_state = MPROC_POST;
					break;

				case	MCTRL_DIR_CLOSE:
					while ( (local_motor_ctrl->TO_close[ next_state - MPROC_TO_1 ] == 0) && (next_state<MPROC_TO_9) )
						next_state ++;

					if ( (next_state == MPROC_TO_9) && ( local_motor_ctrl->TO_close[next_state - MPROC_TO_1] == 0 ) )
						next_state = MPROC_POST;
					break;
			}					
			break;

		case MPROC_TO_9:
			next_state = MPROC_POST;
	}

	next_state |= curr_dir;
	return	next_state;
}

static	uint16_t	get_mctrl_to( void )
{
	uint16_t		curr_state = local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK;

	switch ( local_motor_ctrl->motor_process_state & MCTRL_DIR_MASK ) {
		case	MCTRL_DIR_OPEN:
			if ( MPROC_TO_1 <= curr_state && curr_state <= MPROC_TO_9 )
				return	local_motor_ctrl->TO_open[ curr_state - MPROC_TO_1 ];
			else
				return	0;
	
		case	MCTRL_DIR_CLOSE:
			if ( MPROC_TO_1 <= curr_state && curr_state <= MPROC_TO_9 )
				return	local_motor_ctrl->TO_close[ curr_state - MPROC_TO_1 ];
			else
				return	0;
	}
	return	0;
}



/*
	ddl_motor_ctrl_t.TO_open= { 2000, 0, 0, 75, 0, 1500, 0, 0, 100},  unit 2msec 
	motor_control_open_type1 의 상관 관계
	기본적으로 값이 있는 경우 해당 step 을 진행 하겠다는 의미 
	값이 0 인 경우 이전 step 에서 해당 step 으로 가지 않겠다는 의미 
	위와 같은 값이면 
	MPROC_TO_1, MPROC_TO_4 , MPROC_TO_6 , MPROC_TO_9 순으로 state 천이 
	각 state 는 다음 state 로 가기 위한 행동을 자신의 state 에서 실행후 다음 state 에서는 그 행동에 대한 대기 및 종료 처리를 한다. 

	.TO_open = 
	{ 
	   2000, :  MPROC_TO_1 step 종료 타이머
	         0, :  MPROC_TO_2 step 종료 타이머
	         0, :  MPROC_TO_3 step 종료 타이머
	       75, :  MPROC_TO_4 step 종료 타이머
	         0, :  MPROC_TO_5 step 종료 타이머
	   1500, :  MPROC_TO_6 step 종료 타이머
	         0, :  MPROC_TO_7 step 종료 타이머
	         0, :  MPROC_TO_8 step 종료 타이머
	     100  :  MPROC_TO_9 step 종료 타이머
	}

	위 값에 대한 천이 시나리오 

	StartMotorOpen
	MPROC_PRE : motor open 시도 
	MPROC_TO_1 : open sensor 감지 까지 4000ms 대기 open sensor 감지시 motor stop hold 후 MPROC_TO_4  로 이동 
	MPROC_TO_4 : MPROC_TO_1 의 stop hold 를 150ms 유지 후 backturn 실시 MPROC_TO_6 로 이동 
	MPROC_TO_6 : center sensor 감지 까지 3000ms 대기 center 감지후 stop hold 실시 
	MPROC_TO_9 : stop m hold 를 200ms 대기 후 motor process 종료 
*/


//2msec 주기로 호출될 것을 기대
void		motor_control_open_type1( void )
{
	switch( local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK ) {
		case MPROC_STOP:
			return;

		case MPROC_PRE:
			(*local_motor_ctrl->sensor_on)();
			Delay( SYS_TIMER_1MS );					// photo sensor stable time

			MotorAbnormalCheck(FALSE);

#if defined (M_CLOSE_PWMCH1_Pin) ||defined (PWM_BACKTURN_SKIP_COUNT)
			if ( (*local_motor_ctrl->is_motor_open)() && local_motor_ctrl->MotorException == 0x00) {		// if already open
				/* 센서가 open 상태 이나 MotorException 값이 셋팅이 안되어 그냥 빠져 나가는 단계 */
				(*local_motor_ctrl->sensor_off)();			// turn off sensor power
				(*local_motor_ctrl->motor_stop)();			// make motor to idle
				local_motor_ctrl->motor_process_state = MPROC_TO_9 |MCTRL_DIR_OPEN;
				gwMotorCtrlTimer2ms = 1;				// clear MotorCtrl timer
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
				break;
			}
			else 
			{
				/* 센서 open 상태 상관없이 일반적으로 타는 루틴*/
				(*local_motor_ctrl->motor_stop)();			// make motor to idle	
			}
#else 
			if ( (*local_motor_ctrl->is_motor_open)() ) {		// if already open
				(*local_motor_ctrl->sensor_off)();			// turn off sensor power
				(*local_motor_ctrl->motor_stop)();			// make motor to idle
				local_motor_ctrl->motor_process_state = MPROC_TO_9 |MCTRL_DIR_OPEN;
				gwMotorCtrlTimer2ms = 1;				// clear MotorCtrl timer
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
				break;
			}
#endif 

			local_motor_ctrl->motor_process_state = get_next_mproc_state();		// get next step
			gwMotorCtrlTimer2ms =  get_mctrl_to();
			if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_1 )
				(*local_motor_ctrl->motor_open)();			// do motor open
			else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_2 ) {
				(*local_motor_ctrl->motor_open)();			// do motor open

				if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
				{
					gwMotorCtrlTimer2ms = MotorOpenCloseTimeSetting_open();
				}
			}
			break;

		case MPROC_TO_1:
			
			MotorAbnormalCheck(FALSE);
			
			if ( gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN_ERROR);

				local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_TO_5;
				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();
				
				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_6 ) {
					(*local_motor_ctrl->motor_close)(); 		// do motor close
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_7 ) {
					(*local_motor_ctrl->motor_close)(); 		// do motor close
				
					if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
					{
						gwMotorCtrlTimer2ms = MotorBackTurnTimeSetting_open();
					}
				}
				else {
					(*local_motor_ctrl->sensor_off)();			// turn off sensor power
					(*local_motor_ctrl->motor_hold)();			// make motor to idle
					local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_POST;
				}
			}
			else if ( (*local_motor_ctrl->is_motor_open)() ) {		// if open detected
#ifndef P_SNS_CENTER_T
				(*local_motor_ctrl->sensor_off)();			// turn off sensor power
#endif 				

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();

				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_2 ) {
					// Open 센서 감지된 상태에서 더 밀어주는 구간이므로 Motor Stop 수행 안함.	
					if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
					{
						gwMotorCtrlTimer2ms = MotorOpenCloseTimeSetting_open();
					}
#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)
					if(tim9_count < PWM_BACKTURN_SKIP_COUNT && local_motor_ctrl->MotorException == 0xAA)
					{
						/* close 시도중에 open 을 시도 하는 경우 중 PWM 이 충분한 시간을 유지 못하고 온경우 */
						/* 즉 open 시도후 바로 open sensor 에 근접한 경우 */
						/* open 상태로 PWM_HOLD_OPEN_TIME ms 유지후 backturn 을 하지 않는다 */
						/* tim9_count < PWM_BACKTURN_SKIP_COUNT 이값은 mecha 특성에 따라 측정 한다 */
						/* gwMotorCtrlTimer2ms = PWM_HOLD_OPEN_TIME; 역시 mecha 특성에 따라 측정 */
						gwMotorCtrlTimer2ms = PWM_HOLD_OPEN_TIME;
					}
					else 
					{
						local_motor_ctrl->MotorException = 0x00;
					}
#endif 					
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_3 ) {
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_4 ) {
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_POST;
				}
				
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			break;

		case MPROC_TO_2:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();

				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_3 ) {
					// 더 밀어주는 구간이 끝나고 Hold 하기 전 Delay 구간일 경우 아무런 수행 없음
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_4 ) {
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_3:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();

				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_4 ) {
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_4:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();
				
				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_5 ) {
					// Hold 구간이 끝나고 Back-Turn 하기 전 Delay 구간일 경우 아무런 수행 없음
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_6 ) {
					(*local_motor_ctrl->motor_close)(); 		// do motor close
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_7 ) {
				
#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)
					if(local_motor_ctrl->MotorException == 0xAA)
					{
						/* backturn skip */ 
						local_motor_ctrl->MotorException = 0x00;
					}
					else 
					{
						(*local_motor_ctrl->motor_close)(); 		// do motor close					
					}
#else 
					(*local_motor_ctrl->motor_close)(); 		// do motor close
#endif 				
					if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
					{
						gwMotorCtrlTimer2ms = MotorBackTurnTimeSetting_open();
					}
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_5:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();
				
				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_6 ) {
					(*local_motor_ctrl->motor_close)(); 		// do motor close
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_7 ) {
					(*local_motor_ctrl->motor_close)(); 		// do motor close

					if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
					{
						gwMotorCtrlTimer2ms = MotorBackTurnTimeSetting_open();
				}
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_8 ) {
					//	Back-Turn 한 상태에서 더 밀어주는 구간 이후 Hold 하기 전 Delay 구간일 경우 아무런 수행 없음.	
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_9 ) {
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_6:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
					(*local_motor_ctrl->sensor_off)();			// turn off sensor power
					(*local_motor_ctrl->motor_hold)();			// make motor to idle
					local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_POST;
				
					SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN_ERROR);
			}
			else if ( (*local_motor_ctrl->is_motor_center)() ) {	// if center detected
				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();

				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_7 ) {
					// Back-Turn이 끝나고 더 밀어 주는 구간일 경우 아무런 수행 없음
					if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
					{
						gwMotorCtrlTimer2ms = MotorBackTurnTimeSetting_open();
					}
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_8 ) {
					// Back-Turn이 끝나고 Hold 수행 전 Delay 구간일 경우 모터 멈춤
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_9 ) {
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_7:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();
				
				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_8 ) {
					// Back-Turn 이후 더 밀어주는 구간이 끝나고 Hold 하기 전 Delay 구간일 경우 모터 멈춤
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_9 ) {
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_8:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();
				
				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_9 ) {
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_9:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_POST;
			}
			break;


		case MPROC_POST:
		default:
			(*local_motor_ctrl->motor_stop)();			// make motor to idle
			(*local_motor_ctrl->sensor_off)();			// turn off sensor power
			local_motor_ctrl->motor_process_state = MPROC_STOP;
			gwMotorCtrlTimer2ms = 0;				// clear MotorCtrl timer
			LockStatusCheckClear();
#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)
			local_motor_ctrl->MotorException = 0x00;
#endif 
			break;
	}
}


void		motor_control_close_type1( void )
{
	switch( local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK ) {
		case MPROC_STOP:
			return;

		case MPROC_PRE:
			(*local_motor_ctrl->sensor_on)();
			Delay( SYS_TIMER_1MS );					// photo sensor stable time

			MotorAbnormalCheck(FALSE);

#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)
			(*local_motor_ctrl->motor_stop)();
#endif 
			
			if ( (*local_motor_ctrl->is_motor_close)() ) {		// if already open
				(*local_motor_ctrl->sensor_off)();			// turn off sensor power
				(*local_motor_ctrl->motor_stop)();			// make motor to idle
				local_motor_ctrl->motor_process_state = MPROC_STOP;
				gwMotorCtrlTimer2ms = 0;				// clear MotorCtrl timer

				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
				break;
			}

			local_motor_ctrl->motor_process_state = get_next_mproc_state();		// get next step
			gwMotorCtrlTimer2ms =  get_mctrl_to();
			if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_1 )
				(*local_motor_ctrl->motor_close)();			// do motor close
			else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_2 ) {
				(*local_motor_ctrl->motor_close)();			// do motor close

				if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
				{
					gwMotorCtrlTimer2ms = MotorOpenCloseTimeSetting_close();
				}
			}
			break;

		case MPROC_TO_1:

			MotorAbnormalCheck(FALSE);
			
			if ( gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE_ERROR);

				local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_TO_5;
				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();
				
				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_6 ) {
					(*local_motor_ctrl->motor_open)();			// do motor open
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_7 ) {
					(*local_motor_ctrl->motor_open)();			// do motor open
				
					if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
					{
						gwMotorCtrlTimer2ms = MotorBackTurnTimeSetting_close();
					}
				}
				else {
					(*local_motor_ctrl->sensor_off)();			// turn off sensor power
					(*local_motor_ctrl->motor_hold)();			// make motor to idle
					local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_POST;
				}
			}
			else if ( (*local_motor_ctrl->is_motor_close)() ) {		// if open detected
#ifndef P_SNS_CENTER_T
				(*local_motor_ctrl->sensor_off)();			// turn off sensor power
#endif 				

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();

				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_2 ) {
					// Close 센서 감지된 상태에서 더 밀어주는 구간이므로 Motor Stop 수행 안함.	
					if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
					{
						gwMotorCtrlTimer2ms = MotorOpenCloseTimeSetting_close();
					}
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_3 ) {
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_4 ) {
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_POST;
				}

				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}
			break;

		case MPROC_TO_2:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();

				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_3 ) {
					// 더 밀어주는 구간이 끝나고 Hold 하기 전 Delay 구간일 경우 아무런 수행 없음
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_4 ) {
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_3:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();

				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_4 ) {
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_4:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();
				
				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_5 ) {
					// Hold 구간이 끝나고 Back-Turn 하기 전 Delay 구간일 경우 아무런 수행 없음
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_6 ) {
					(*local_motor_ctrl->motor_open)();			// do motor open
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_7 ) {
					(*local_motor_ctrl->motor_open)();			// do motor open
				
					if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
					{
						gwMotorCtrlTimer2ms = MotorBackTurnTimeSetting_close();
					}
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_5:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();
				
				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_6 ) {
					(*local_motor_ctrl->motor_open)();			// do motor open
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_7 ) {
					(*local_motor_ctrl->motor_open)();			// do motor open

					if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
					{
						gwMotorCtrlTimer2ms = MotorBackTurnTimeSetting_close();
				}
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_8 ) {
					//  Back-Turn 한 상태에서 더 밀어주는 구간 이후 Hold 하기 전 Delay 구간일 경우 아무런 수행 없음.	
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_9 ) {
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_6:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->sensor_off)();			// turn off sensor power
				(*local_motor_ctrl->motor_hold)();			// make motor to idle
				local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_POST;

				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE_ERROR);
			}
			else if ( (*local_motor_ctrl->is_motor_center)() ) { 	// if center detected
				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();

				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_7 ) {
					// Back-Turn이 끝나고 더 밀어 주는 구간일 경우 아무런 수행 없음
					if(gwMotorCtrlTimer2ms == _USE_MOTOR_TIME_SET)
					{
						gwMotorCtrlTimer2ms = MotorBackTurnTimeSetting_close();
					}
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_8 ) {
					// Back-Turn이 끝나고 Hold 수행 전 Delay 구간일 경우 모터 멈춤
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_9 ) {
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_7:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();
				
				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_8 ) {
					// Back-Turn 이후 더 밀어주는 구간이 끝나고 Hold 하기 전 Delay 구간일 경우 모터 멈춤
				}
				else if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_9 ) {
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_8:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = get_next_mproc_state();
				gwMotorCtrlTimer2ms =	get_mctrl_to();
				
				if ( (local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) == MPROC_TO_9 ) {
					(*local_motor_ctrl->motor_stop)();			// make motor to idle
					(*local_motor_ctrl->motor_hold)();			// do motor break
				}
				else {
					local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_POST;
				}
			}
			break;

		case MPROC_TO_9:
			if (gwMotorCtrlTimer2ms == 0 ) {		// Timeout
				(*local_motor_ctrl->motor_stop)();			// make motor to idle

				local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_POST;
			}
			break;

		case MPROC_POST:
		default:
			(*local_motor_ctrl->motor_stop)();			// make motor to idle
			(*local_motor_ctrl->sensor_off)();			// turn off sensor power
			local_motor_ctrl->motor_process_state = MPROC_STOP;
			gwMotorCtrlTimer2ms = 0;				// clear MotorCtrl timer
			break;
	}
}


void		StartMotorOpen( void )
{
#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)
	if((local_motor_ctrl->motor_process_state & MCTRL_DIR_MASK) == MCTRL_DIR_CLOSE)
	{
		local_motor_ctrl->MotorException = 0xAA;
	}
#endif 	
	local_motor_ctrl->motor_process_state = MCTRL_DIR_OPEN | MPROC_PRE;

	SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN_ING);
}

void		StartMotorClose( void )
{
	local_motor_ctrl->motor_process_state = MCTRL_DIR_CLOSE | MPROC_PRE;

	SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE_ING);
}

void	CalMotorContol(uint8_t MotorContol )
{
	switch (MotorContol)
	{
		case 0:
			(*local_motor_ctrl->motor_open)();
		break;

		case 1:
			(*local_motor_ctrl->motor_close)();
		break;

		case 2:
			(*local_motor_ctrl->motor_stop)();
		break;

		case 3:
			(*local_motor_ctrl->motor_hold)();
		break;

		default:
		break;
	}
}

void		MotorControlProcess( void )
{
	PCErrorClear();

	if ( !gwMotorCtrlTimer2ms && !(local_motor_ctrl->motor_process_state & MCTRL_STATE_MASK) )
		return;

	switch ( local_motor_ctrl->motor_process_state & MCTRL_DIR_MASK ) {
		case	MCTRL_DIR_OPEN:
			if(PCErrorClearCheck() == STATUS_FAIL)
			{
				(*local_motor_ctrl->motor_stop)();			// make motor to idle
				(*local_motor_ctrl->sensor_off)();			// turn off sensor power
				local_motor_ctrl->motor_process_state = MPROC_STOP;
				gwMotorCtrlTimer2ms = 0;				// clear MotorCtrl timer
#if defined (M_CLOSE_PWMCH1_Pin) || defined (PWM_BACKTURN_SKIP_COUNT)
				local_motor_ctrl->MotorException = 0x00;
#endif 
				FeedbackSystemError(SYSTEM_ERR_MOTOR_SENSOR);
				return;
			}
			
			motor_control_open_type1();
			break;
		case	MCTRL_DIR_CLOSE:
			motor_control_close_type1();
			break;
	}
}


void		Test_MotorControl( void )
{
/*
	(*local_motor_ctrl->sensor_on)();
	Delay( SYS_TIMER_1MS );

	if ( (*local_motor_ctrl->is_motor_open)() )
		StartMotorClose();
	else if ( (*local_motor_ctrl->is_motor_close)() )
		StartMotorOpen();

	(*local_motor_ctrl->sensor_off)();
*/
}


//------------------------------------------------------------------------------
/** 	@brief	Get current motor process state
	@param 	None 
	@return 	모터 구동 process의 local_motor_ctrl->motor_process_state 값을 리턴한다.
	@remark 현재 모터 구동 상태를 가져 온다. mask를 씌우지 않았으므로 Open/Close상태도 파악할 수 있다.
*/
//------------------------------------------------------------------------------
uint16_t GetMotorPrcsStep(void)
{
	return	local_motor_ctrl->motor_process_state;
}



__weak uint32_t MotorOpenCloseTimeSetting(void)
{
	return 0;
}


__weak uint32_t MotorBackTurnTimeSetting(void)
{
	return 0;
}


__weak uint32_t MotorOpenCloseTimeSetting_open(void)
{
	return 0;
}


__weak uint32_t MotorOpenCloseTimeSetting_close(void)
{
	return 0;
}

__weak uint32_t MotorBackTurnTimeSetting_open(void)
{
	return 0;
}

__weak uint32_t MotorBackTurnTimeSetting_close(void)
{
	return 0;
}


//============================================================



BYTE gFinalMotorStatus = 0;					/**< 최종 모터 구동 결과 내용 저장   */


//------------------------------------------------------------------------------
/** 	@brief	Set motor running result
	@param 	[FinalStatus] 최종 모터 구동 내용 
	@return 	None
	@remark 마지막에 모터를 구동한 결과 내용을 저장함 
*/
//------------------------------------------------------------------------------
void SetFinalMotorStatus(BYTE FinalStatus)
{
	gFinalMotorStatus = FinalStatus;
}


//------------------------------------------------------------------------------
/** 	@brief	Get previous motor running result
	@param 	None 
	@return 	[gFinalMotorStatus] 최종 모터 구동 내용 저장
	@remark 마지막에 모터를 구동한 결과 내용을 읽어 옴 
*/
//------------------------------------------------------------------------------
BYTE GetFinalMotorStatus(void)
{
	return (gFinalMotorStatus);
}



//============================================================


BYTE gMotorSensorStatus = 0;	/**< 현재 Motor Sensor 값 저장  */


//------------------------------------------------------------------------------
/** 	@brief	Motor Sensor Status Variable Clear
	@param 	None 
	@return 	None
	@remark 반드시 Main Loop 상에서 매 Loop마다 1회 구동되도록 구현
*/
//------------------------------------------------------------------------------
void MotorSensorClear(void)
{
	gMotorSensorStatus = 0;
}

void CalSensorControl(uint8_t SensorContol )
{
	gFinalMotorStatus = SENSOR_NOT_STATE;
	
	switch (SensorContol)
	{
		case 0:
			(*local_motor_ctrl->sensor_on)();
			Delay(SYS_TIMER_1MS);

#if defined (P_SNS_OPEN_T) ||defined (P_SNS_CLUTCH_T)
			if((*local_motor_ctrl->is_motor_open)())
			{
				gFinalMotorStatus |= SENSOR_OPEN_STATE;
			}
#endif
			
#if defined (P_SNS_CLOSE_T) ||defined (P_SNS_CLUTCH_T)
			if((*local_motor_ctrl->is_motor_close)())	
			{
				gFinalMotorStatus |= SENSOR_CLOSE_STATE;
			}
#endif	

		break;

		case 1:
			(*local_motor_ctrl->sensor_off)();
		break;

		case 2:
#if defined (P_SNS_OPEN_T) ||defined (P_SNS_CLUTCH_T)
			if((*local_motor_ctrl->is_motor_open)())
			{
				gFinalMotorStatus |= SENSOR_OPEN_STATE;
			}
#endif
		break;

		case 3:
#if defined (P_SNS_CLOSE_T) ||defined (P_SNS_CLUTCH_T)
			if((*local_motor_ctrl->is_motor_close)())	
			{
				gFinalMotorStatus |= SENSOR_CLOSE_STATE;
			}
#endif	
		break;

		default:
		break;
	}
}

//------------------------------------------------------------------------------
/** 	@brief	Current Motor Sensor Check
	@param 	None 
	@return 	[gMotorSensorStatus] 현재 Sensor 상태 값 회신
	@remark Main Loop 상에서 모터 구동 경우를 제외하고는 Loop 당 Sensor는 1회만 구동
	@remark  Sensor Power On 할 때마다 1ms 정도의 Delay 시간이 필요해 Loop 동작을 방해할 뿐 아니라,
	@remark  Loop 동작 시간이 그리 길지 않기 때문에 그동안 1회만 구동해도 실제 Sensor 확인에는 문제가 
	@remark  없기 때문에 이와 같이 구동
*/
//------------------------------------------------------------------------------
BYTE MotorSensorCheck(void)
{
	if(gMotorSensorStatus == 0)
	{
		(*local_motor_ctrl->sensor_on)();
		Delay( SYS_TIMER_1MS );

#if defined (P_SNS_OPEN_T) ||defined (P_SNS_CLUTCH_T)
		if((*local_motor_ctrl->is_motor_open)())
		{
			gMotorSensorStatus |= SENSOR_OPEN_STATE;
		}
#endif

#if defined (P_SNS_CLOSE_T) ||defined (P_SNS_CLUTCH_T)
		if((*local_motor_ctrl->is_motor_close)()) 	
		{
			gMotorSensorStatus |= SENSOR_CLOSE_STATE;
		}
#endif	

#ifdef P_SNS_CENTER_T
		if((*local_motor_ctrl->is_motor_center)())	
		{
			gMotorSensorStatus |= SENSOR_CENTER_STATE;
		}
#endif	

		if((*local_motor_ctrl->is_motor_lock)())	
		{
			gMotorSensorStatus |= SENSOR_LOCK_STATE;
		}

// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 시작		
#ifdef		P_SNS_LOCK_T	
#ifdef	DDL_CFG_SNS_LOCK_ACTIVE_HIGH
		if(P_SNS_LOCK_T)
#else 
		if(!P_SNS_LOCK_T)
#endif 			
		{
			gbInnerForceLockDetectionValue = 0x00;	//내부 강제 잠금 Set
		}
		else
		{
			gbInnerForceLockDetectionValue = 0x01;	//내부 강제 잠금 Clear
		}
#endif
// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 끝

		
		// 모터 구동 중에 다른 루틴에서 MotorSensorCheck 함수를 수행할 경우 Sensor를 끄게 되어
		// 모터 구동 중에 센서 확인이 되지 않아 에러 처리되는 문제 수정
		// 모터 구동 중에는 센서 끄지 않도록 처리하고, 모터 구동 함수의 MPROC_POST 단계에서 sensor_off 추가 
		// (프로그램 구조 문제로 인해 센서가 안 꺼지고 Power Down Mode로 들어가는 일이 없도록 하기 위해)
		if(GetMotorPrcsStep() == 0)
		{
			(*local_motor_ctrl->sensor_off)();
		}

#ifdef P_SNS_CENTER_T
		if(gMotorSensorStatus == SENSOR_OPENCENLOCK_STATE)
		{
			gMotorSensorStatus = SENSOR_OPENLOCK_STATE;
		}
		else if(gMotorSensorStatus == SENSOR_CLOSECENLOCK_STATE)
		{
			gMotorSensorStatus = SENSOR_CLOSELOCK_STATE;
		}
#endif	
	}

	MotorAbnormalCheck(TRUE);

	return (gMotorSensorStatus);
}



void MotorStatusInitialCheck(void)
{
	gfMotorSensorErr = 0;			

// 내부강제잠금 초기 상태 확인은 Motor 초기 상태 확인 부분에서 같이 하도록 처리
#ifndef		P_SNS_LOCK_T	
	InnerForcedLockSettingLoad();
#endif

	switch(MotorSensorCheck())
	{
#ifdef P_SNS_CENTER_T
		case SENSOR_CLOSECEN_STATE:
#endif
		case SENSOR_CLOSE_STATE:
		case SENSOR_CLOSELOCK_STATE:
			SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			break;

		case SENSOR_SENSOR_ERROR:
			gfMotorSensorErr = 1;			
			break;

		case SENSOR_OPENLOCK_STATE:
// 스위치 형태가 아닌 내부강제잠금 설정 상태는 Motor가 열림 상태면 무조건 해제 처리			
#ifndef		P_SNS_LOCK_T	
			InnerForcedLockClear();
#endif

		default:
			SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			break;
	}
}





// 전기 충격에 의한 모터 해정 방지 처리 함수

WORD gwPCErrorCheck = 0;


void PCErrorClearSet(void)
{
	gwPCErrorCheck = 0x825A;
}


BYTE PCErrorClearCheck(void)
{
	if(gwPCErrorCheck == 0x825A)
	{
		return (STATUS_SUCCESS);
	}

	return (STATUS_FAIL);
}


void PCErrorClear(void)
{
	if(GetMainMode())			return;	
	if(GetMotorPrcsStep())		return;
	if(GetPackRxProcessStep())	return;
	if(JigModeStatusCheck())	return;
	if(AlarmStatusCheck())		return;

	gwPCErrorCheck = 0;
}

void MotorAbnormalCheck(BYTE bUseVariable)
{
#if (defined (P_SNS_OPEN_T) || defined (P_SNS_CLOSE_T))

	BYTE Temp = 0x00;
	
	if(bUseVariable)
	{
		if((gMotorSensorStatus & (SENSOR_OPEN_STATE|SENSOR_CLOSE_STATE)) == (SENSOR_OPEN_STATE|SENSOR_CLOSE_STATE))
		{
			(*local_motor_ctrl->motor_hold)();
			Temp = 0xCA;
		}
	}
	else
	{
		if((*local_motor_ctrl->is_motor_open)() && (*local_motor_ctrl->is_motor_close)())
		{
			(*local_motor_ctrl->motor_hold)();
			Temp = 0xCA;
		}
	}

	if(Temp == 0xCA)
	{
		RomWrite(&Temp ,LOCK_MODE_STATUS+SILENT_RESET_FLAG,1);
		HAL_NVIC_SystemReset();
	}
#else 
	__NOP();
#endif 	
}

#if defined (P_SNS_CENTER_T) && defined (DDL_CFG_AUTO_KEY_PROCESS) 
void MotorFindCenter(void)
{
	WORD CenterCounter = 1000;
		
	(*local_motor_ctrl->sensor_on)();
	Delay( SYS_TIMER_1MS );		
	(*local_motor_ctrl->motor_open)();
	while(!(*local_motor_ctrl->is_motor_center)())
	{
		Delay( SYS_TIMER_1MS );
		CenterCounter--;
		if(CenterCounter == 0x00)
			break;
	}
	(*local_motor_ctrl->motor_stop)();
	(*local_motor_ctrl->sensor_off)();	
}
#endif 
