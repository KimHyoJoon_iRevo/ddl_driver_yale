

#ifndef	__MOTOR_INCLUDED
#define	__MOTOR_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"



#define 	_MOTOR_CLOSE	{ \
			HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_OPEN_2_GPIO_Port, M_OPEN_2_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_CLOSE_1_GPIO_Port, M_CLOSE_1_Pin, GPIO_PIN_SET); \
			HAL_GPIO_WritePin(M_CLOSE_2_GPIO_Port, M_CLOSE_2_Pin, GPIO_PIN_SET); \
}

#define 	_MOTOR_OPEN	{ \
			HAL_GPIO_WritePin(M_CLOSE_1_GPIO_Port, M_CLOSE_1_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_CLOSE_2_GPIO_Port, M_CLOSE_2_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin, GPIO_PIN_SET); \
			HAL_GPIO_WritePin(M_OPEN_2_GPIO_Port, M_OPEN_2_Pin, GPIO_PIN_SET); \
}

#define	_MOTOR_BREAK	{ \
			HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_OPEN_2_GPIO_Port, M_OPEN_2_Pin, GPIO_PIN_SET); \
			HAL_GPIO_WritePin(M_CLOSE_1_GPIO_Port, M_CLOSE_1_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_CLOSE_2_GPIO_Port, M_CLOSE_2_Pin, GPIO_PIN_SET); \
}


#define 	_MOTOR_STOP	{ \
			HAL_GPIO_WritePin(M_OPEN_1_GPIO_Port, M_OPEN_1_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_OPEN_2_GPIO_Port, M_OPEN_2_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_CLOSE_1_GPIO_Port, M_CLOSE_1_Pin, GPIO_PIN_RESET); \
			HAL_GPIO_WritePin(M_CLOSE_2_GPIO_Port, M_CLOSE_2_Pin, GPIO_PIN_RESET); \
}

void	Init_Motor( void );


//extern BYTE gbTestCnt3;
//extern BYTE gbTestCnt2;
//extern BYTE gbTestCnt1;

//extern BYTE gbTestErrCnt3;
//extern BYTE gbTestErrCnt2;
//extern BYTE gbTestErrCnt1;


void InnerForcedLockSet(void);
void InnerForcedLockClear(void);
void InnerForcedLockSettingLoad(void);
uint32_t InnerForcedLockCheck(void);
#endif

