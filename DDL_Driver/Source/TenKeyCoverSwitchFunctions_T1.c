//------------------------------------------------------------------------------
/** 	@file		TenKeyCoverSwitchFunctions_T1.c
	@version 0.1.00
	@date	2016.04.25
	@brief	Ten Key Cover Switch Process
	@remark Ten Key 처리 기능을 가진 Cover Switch 입력에 따른 처리
	@see 	KeyInput.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.25		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"

void CoverSwProcess(void)
{
	BYTE bTmp;
	
	if(!gfXORCoverSw)	return;						//Cover의 신호가 바뀌어야만 루틴 수행
	gfXORCoverSw = 0;

	if(gfCoverSw)
	{
	// Cover Up
		if(GetMainMode() == 0)
		{
			gbOldFunctionKey = TENKEY_STAR; 
			gbInputKeyValue = TENKEY_STAR;
		}
	}
	else
	{
	// Cover Down
		bTmp = GetMainMode();
#if defined	(__DDL_MODEL_EDGE2_DMP)
		// Edge2 제품의 경우 제품 특성상 커버를 닫아야만 카드 동작이 되는 구조라 카드 개별 등록 과정 처리를 위해 별도로 분리하여 구현
		// 카드 개별 등록 과정에서 커버를 올렸다 내려야 하는 구조 특성상의 문제 때문에 해당 제품에만 별도 처리 
		// BLE-N을 통한 등록 과정에서도 문제가 되어 관련 내용 추가
		if(!((bTmp == 0) || (bTmp == MODE_MENU_ONECARD_REGISTER) 
			|| (bTmp == MODE_CARD_REGISTER_BYBLEN) || (bTmp == MODE_MENU_ONECARD_REGISTER_BYBLEN)))
#else
		if(bTmp)
#endif
		{
			gbOldFunctionKey = TENKEY_STAR; 
			gbInputKeyValue = TENKEY_STAR;
		}
	}
}



