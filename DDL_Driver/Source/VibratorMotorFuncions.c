
#include	"main.h"

BYTE gbVMPrcsStep = 0;
BYTE gbVMHapticIndex = 0;
WORD gbVMTimer10ms=0;
BYTE gbVMLoopCount = 0;

const BYTE HapticVerifyOk[]={
//power on time , power off time , loop delay , loop count
// unit 10ms 
	10 , 10 , 10 , 2
};

const BYTE HapticVerifyFail[]={
//power on time , power off time , loop delay , loop count
// unit 10ms 
	5 , 5 , 5 , 5
};


const BYTE* HapticList[] = 
{
	HapticVerifyOk,
	HapticVerifyFail
};

void VibratorMotorClear(void)
{
	P_COM_HCP_EN(0);
	gbVMTimer10ms = 0;
	gbVMPrcsStep = 0;
	gbVMHapticIndex = 0;
	gbVMLoopCount = 0;
}

void VibratorMotorFeedback(BYTE Index)
{
	VibratorMotorClear();
	gbVMHapticIndex = Index;
	gbVMLoopCount = *(HapticList[gbVMHapticIndex]+3);
	gbVMPrcsStep = 1;
}

void VibratorMotorProcess(void)
{
	switch(gbVMPrcsStep)
	{
		case 0: //idle
		{
			;
		}
		break;

		case 1: 
		{
			P_COM_HCP_EN(1);
			gbVMTimer10ms = *(HapticList[gbVMHapticIndex]);
			gbVMPrcsStep++;
		}
		break;

		case 2: 
		{
			if(gbVMTimer10ms == 0)
			{
				P_COM_HCP_EN(0);
				gbVMTimer10ms = *(HapticList[gbVMHapticIndex]+1);
				gbVMPrcsStep++;
			}
		}
		break;

		case 3: 
		{
			if(gbVMTimer10ms == 0)
			{
				gbVMTimer10ms = *(HapticList[gbVMHapticIndex]+2);
				gbVMPrcsStep++;
			}
		}
		break;

		case 4: 
		{
			if(gbVMTimer10ms == 0)
			{
				if(gbVMLoopCount)
				{
					gbVMLoopCount--;			
					gbVMPrcsStep = 1;
				}
				else
				{
					VibratorMotorClear();
				}
			}
		}
		break;


		default : 
		{
			VibratorMotorClear();
		}
		break;
	}
}

