/** 	@file		DebugFunctions_T1.c
	@brief	Dubug Functions using UART
*/
//------------------------------------------------------------------------------
#ifndef __DEBUGFUNCTIONS_T1_INCLUDED
#define __DEBUGFUNCTIONS_T1_INCLUDED

#include "DefineMacro.h"

extern BYTE DebugDataBuf[20];

//	#define		DEBUG_CARD
//	#define		DEBUG_TOUCH
//	#define		DEBUG_TOUCH_AUTO
//	#define		DEBUG_AGING
//	#define		BATTERY_LIFE_CYCLE_TEST


void DebugTxDataSend(uint8_t *pTxDataBuf, uint8_t TxDataByteNum);
void	SetupUartforPrintf(UART_HandleTypeDef* uart);
void InitPrintfService( void (*TxCallback)(UART_HandleTypeDef *huart), void (*RxCallback)(UART_HandleTypeDef *huart),void (*InitProcess)(void));

#endif

