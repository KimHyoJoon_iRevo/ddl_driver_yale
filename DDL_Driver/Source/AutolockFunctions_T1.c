//------------------------------------------------------------------------------
/** 	@file		AutolockFunctions_T1.c
	@version 0.1.00
	@date	2016.04.19
	@brief	AutoRelock Process
	@remark 문 상태 확인 스위치 있는 경우의 자동잠김 처리
	@remark 문이 열렸다 닫힐 경우 2초 뒤 자동 잠김 수행
	@remark 문이 닫혀 있는 상태에서 모터가 닫힘 상태가 아닐 경우 7초 뒤 자동 잠김 수행
	@remark 자동 잠김 실패할 경우 1회 재시도
	@remark [해당 함수 사용 조건]
	@remark 1.자동 잠김/수동 잠김이 메뉴에 의한 설정일 경우
	@remark 2.자동 잠김/수동 잠김이 외부 스위치에 의한 설정일 경우 (해당 내용 추가 예정)
	@remark 3.문 상태 감지 센서가 있는 경우
	@remark 4.프로그램에 의해 내부강제잠금 설정/해제가 되는 경우
	@see	KeyInput.c
	@see	AlarmFunctions_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.19		by Jay
			[자동 잠김 수행 안하는 경우]
			- 수동잠김 설정되어 있을 경우 자동잠김 수행 안함
			- 문이 열려 있을 경우 자동잠김 수행 안함
			- Main Mode 수행 중일 경우 자동잠김 수행 안함
			- Motor 구동 중일 경우 자동잠김 수행 안함
			- 경보가 발생 중일 경우 자동잠김 수행 안함
			
			[자동 잠김 수행 하는 경우]
			- 문이 닫혀 있고, 모터가 닫힘 상태가 아닐 경우 7초 뒤 자동 잠김 수행
			- 문이 열렸다 닫힐 때, 모터가 닫힘 상태가 아닐 경우 2초 뒤 자동 잠김 수행

		V0.1.01 2016.04.20		by Jay
			[자동 잠김 수행 안하는 경우] 에 내용 추가
			- 3분락 동작 중일 경우 자동잠김 수행 안함
*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gLockStatusPrcsStep = 0;						/**< 자동 잠김 수행 단계  */
BYTE gLockStatusTimer1s = 0;						/**< 자동 잠김 수행을 위한 대기 시간, 1s Tick  */
BYTE gAutoLockTryCnt = 0;							/**< 자동 잠김 재시도 회수  */

BYTE gLockStatusCheckTimer10ms = 0;
BYTE gLockStatusCheckCount = 0;

#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
BYTE gbFeedbackCloseOnceTimercnt100ms = 0;
#endif

// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 시작
#ifdef		P_SNS_LOCK_T	
BYTE gbInnerForceLockDetectionValue;
BYTE gbInnerForceLockDetectionValueBackUp = 0xFF;
#endif
// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 끝



void LockStatusCheckTimeCount(void)
{
	if(gLockStatusCheckTimer10ms)		--gLockStatusCheckTimer10ms;
}


void LockStatusCheckClear(void)
{
	//main mode 나 motor mode 가 살아 있어서 타이밍이 틀어지며 auto lock time 이 원하는 타이밍에 돌지 않음 
	//modeclear 나 motor mode 종료시 호출해서 바로 auto lock 모드에 들어 가도록 한다. 
	gLockStatusCheckTimer10ms = 0;
	gLockStatusCheckCount = 0;
}

void LockStatusCheckTimeClear(void)
{
	// 폴링으로 인한 Wake Up이 아닐 경우에는 상태 확인 바로 시작	
	if(gbWakeUpMinTime10ms)
	{
		gLockStatusCheckCount = 0;
		gLockStatusCheckTimer10ms = 0;
	}
	else
	{
		// 전류 소모를 줄이기 위해 2회 폴링 간격으로 수동 노브나 핸들에 의한 문 열림 확인하도록 수정
		//	핸들이나 수동 노브로 데드볼트를 연 뒤, 문을 열지 않을 경우 최대 4.5초 지연 뒤 열림음 발생
		//	핸들이 수동 노브로 데드볼트를 연 뒤, 문을 열 경우 바로 열림음 발생
		if(gLockStatusCheckCount)	--gLockStatusCheckCount;
		else
		{
			gLockStatusCheckCount = 3;
			gLockStatusCheckTimer10ms = 0;
		}
	}
}
																															
//------------------------------------------------------------------------------
/** 	@brief	Atuto Lock Setting By Menu Check
	@param	None
	@return 	[0] 수동 잠김 설정 상태
	@return 	[1] 자동 잠김 설정 상태
	@remark 메뉴에 의해 설정된 자동 잠김/수동 잠김 설정 
	@remark 추후 외부 스위치에 의한 내용도 추가 예정 
*/
//------------------------------------------------------------------------------
BYTE AutoLockSetCheck(void)
{
#ifdef P_SW_AUTO_T
	// 외부 스위치에 의한 자동 잠김/수동 잠김 설정 처리의 경우 
	if(P_SW_AUTO_T)
	{
		return 0;		//Manuallock set
	}
	
	return 1;			//Autolock set

#else
	// 메뉴에 의한 자동 잠김/수동 잠김 설정 처리의 경우 
	if(gfManualLock)
	{
		return 0;		//Manuallock set
	}

	return 1;			//Autolock set
#endif
}


//------------------------------------------------------------------------------
/** 	@brief	Atuto Lock Enable By Menu
	@param	None
	@return 	None
	@remark 메뉴에 의한 자동 잠김 설정 
*/
//------------------------------------------------------------------------------
void AutoRelockEnable(void)
{
	DDLStatusFlagClear(DDL_STS_MANUALLOCK); 

	gAutoLockTryCnt = 0;
}


//------------------------------------------------------------------------------
/** 	@brief	Atuto Lock Disable By Menu
	@param	None
	@return 	None
	@remark 메뉴에 의한 수동 잠김 설정 
*/
//------------------------------------------------------------------------------
void AutoRelockDisable(void)
{
	DDLStatusFlagSet(DDL_STS_MANUALLOCK); 
}


//------------------------------------------------------------------------------
/** 	@brief	Atuto Relock Time Tick Counter
	@param	None
	@return 	None
	@remark 1s Tick을 사용하며, BasicTimer_1000ms_local 함수에 추가되어야 함. 
*/
//------------------------------------------------------------------------------
void AutoRelockTimeCounter(void)
{
	if(gLockStatusTimer1s)		--gLockStatusTimer1s;
}



//------------------------------------------------------------------------------
/** 	@brief	Door Switch Process by Door Opening or Closing
	@param	None
	@return 	None
	@remark 문 열림/닫힘 감지에 따른 처리 
	@remark 문이 열릴 경우 경보 발생 여부 확인
	@remark 문이 닫힐 경우 자동 잠김 수행 여부 확인 
*/
//------------------------------------------------------------------------------
void DoorSwCheck(void)
{
#ifdef	P_SNS_EDGE_T			// Edge sensor
	if(!gfXORDoorSw)	return;
	gfXORDoorSw = 0;

	// 문을 열고 닫은 이후에는 바로 센서 상태 확인하도록 처리
	gLockStatusCheckTimer10ms = 0;
	gLockStatusCheckCount = 0;

	// 3분락 동작 중 도어 센서가 열림 또는 닫힘이 될 경우 3분락 해제
	if(GetTamperProofPrcsStep())
	{
		TamperCountClear();
	}
	
	if(gfDoorSwOpenState)				
	{
		// If door opened
#if	(DDL_CFG_BLE_30_ENABLE >= 0x29)
		if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
		{
			PackTx_MakeAlarmPacket(AL_DOOR_OPEN_CLOSE_STATE, 0x00, 0x00);
		}
#endif

		gLockStatusPrcsStep = LOCKSTS_ALARM_CHECK; 		
 	}
	else							
	{
		// If door closed
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
		if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
		{
			PackTx_MakeAlarmPacket(AL_DOOR_OPEN_CLOSE_STATE, 0x00, 0x01);
		}
#endif

		gLockStatusPrcsStep = LOCKSTS_AUTOLOCK_CHECK;		
		gAutoLockTryCnt = 0;
		DoorClosedTimeSet();	
	}
#endif
}




//------------------------------------------------------------------------------
/** 	@brief	Auto Relock Process Clear Check
	@param	None
	@return 	[0] 자동 잠김 수행 유지
	@return 	[1] 자동 잠김 수행 취소
	@remark 자동 잠김 수행 취소 조건 확인
	@remark [자동 잠김 수행하지 않는 조건]
	@remark 1.수동 잠김 설정되어 있을 경우 
	@remark 2.문이 열려 있을 경우 
	@remark 3.MainMode가 실행 중일 경우  
	@remark 4.모터 구동 중일 경우  
	@remark 5.모터 닫힘 상태일 경우  
	@remark 6.경보가 발생 중일 경우  
	@remark 7.3분락 동작 중일 경우  
*/
//------------------------------------------------------------------------------
BYTE AutoRelockProcessClearCheck(void)
{
	BYTE bTmp;
	
	// 수동잠김 설정되어 있을 경우 자동 잠김 실행 안함
	if(AutoLockSetCheck() == 0)
	{
		gLockStatusPrcsStep = 0;
		gLockStatusTimer1s = 0;
		return 1;
	}

	// 문이 열려 있을 경우 자동 잠김 실행 안함
#ifdef	P_SNS_EDGE_T			// Edge sensor
	if(gfDoorSwOpenState)
	{
		gLockStatusPrcsStep = 0;
		gLockStatusTimer1s = 0;
		return 1;
	}
#endif

	// MainMode가 실행 중일 경우 자동 잠김 실행 안함
	if(GetMainMode())
	{
		gAutoLockTryCnt = 0;
		gLockStatusPrcsStep = 0;
		gLockStatusTimer1s = 0;		
		return 1;
	}

	// 모터 구동 중일 경우 자동 잠김 실행 안함
	bTmp = GetFinalMotorStatus();
	if((bTmp == FINAL_MOTOR_STATE_OPEN_ING) || (bTmp == FINAL_MOTOR_STATE_CLOSE_ING))
	{
		gLockStatusPrcsStep = 0;
		gLockStatusTimer1s = 0;		
		return 1;
	}

	// 모터가 이미 닫힘 상태일 경우 자동 잠김 실행 안함
	bTmp = MotorSensorCheck();
	// Open, Close 모두 감지될 경우 에러로 인식하여 처리하지 않도록 내용 추가
	if((bTmp & SENSOR_SENSOR_ERROR) == SENSOR_SENSOR_ERROR)
	{
		return 1;
	}
	else if(bTmp & SENSOR_CLOSE_STATE)
	{
		SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);

		// 모터가 닫힘 상태일 경우 MainMode 종료나, 수동노브에 의한 열림이 아닐 경우 다시 자동 잠김 수행 안함
		gAutoLockTryCnt = _MAX_AUTOLOCK_TRYCNT;
		gLockStatusPrcsStep = 0;
		gLockStatusTimer1s = 0;		
		return 1;
	}

#ifndef 	DDL_CFG_INTERNAL_FORCE_LOCK_ALLOW_OPEN	
	else if(bTmp & SENSOR_LOCK_STATE)
	{
		AlarmGotoInnerLockOutWarning();

		gLockStatusPrcsStep = 0;
		gLockStatusTimer1s = 0;
		gAutoLockTryCnt = 0;
		return 1;
	}	
#endif 

	// 경보가 발생 중일 경우 자동 잠김 실행 안함
	if(AlarmStatusCheck() == STATUS_SUCCESS)
	{
		gLockStatusPrcsStep = 0;
		gLockStatusTimer1s = 0;		
		return 1;
	}

	// 3분락 동작 중일 경우 자동 잠김 실행 안함
	if(GetTamperProofPrcsStep())
	{
		gLockStatusPrcsStep = 0;
		gLockStatusTimer1s = 0;		
		return 1;
	}

	// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
	if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
	{
		gLockStatusPrcsStep = 0;
		gLockStatusTimer1s = 0;		
		return 1;
	}

#ifdef	LOCK_TYPE_DEADBOLT
	// 데드볼트 도어록에서 좌우수 자동 설정 중일 경우 자동 잠김 수행 안함.
	if(HaningLockProcessCheck())
	{
		gLockStatusPrcsStep = 0;
		gLockStatusTimer1s = 0;		
		return 1;
	}
#endif

	return 0;
}



//------------------------------------------------------------------------------
/** 	@brief	Auto Relock Time Reset
	@param	None
	@return 	None
	@remark 7초 자동 잠김을 위한 시간 및 자동 잠김 동작 시작 설정
	@remark 자동 잠김 시도 회수가  _MAX_AUTOLOCK_TRYCNT가 넘을 경우에는 더 이상 자동잠김 수행 안함  
*/
//------------------------------------------------------------------------------
void RelockTimeResetCheck(void)
{
	if(AutoRelockProcessClearCheck())	return;

	if(gAutoLockTryCnt < _MAX_AUTOLOCK_TRYCNT)
	{
		gAutoLockTryCnt++;

#if defined (__DDL_MODEL_B2C15MIN) || defined (__DDL_MODEL_YDM7111)  || defined (__DDL_MODEL_PANPAN_FC2A_P) || defined (__DDL_MODEL_YALE_YMH71) || defined (__DDL_MODEL_YMH70A)
		gLockStatusTimer1s = DEFAULT_AUTOLOCK_TIME;
#else 
		gLockStatusTimer1s = DEFAULT_RELOCK_TIME;
#endif 
		
		gLockStatusPrcsStep = LOCKSTS_AUTOLOCK_CLOSE;		
	}
	else
	{
		gLockStatusPrcsStep = 0;
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Auto Relock Condition Check
	@param	None
	@return 	None
	@remark 자동 잠김 수행 설정을 해도 되는 상황인지 확인
	@remark [자동 잠김 수행 설정 조건]
	@remark 1.이전에 모터가 열림 상태였고, 현재도 닫혀 있지 않은 상태일 경우 자동 잠김 수행 설정
	@remark 2.이전에 모터가 중간 상태였고, 현재도 중간 상태일 경우 자동 잠김 수행 설정
	@remark [그 외 상태 확인 내용]
	@remark 1.이전에 모터가 열림 상태였는데, 현재 열림 상태가 아닐 경우 상태 변경
	@remark 2.이전에 모터가 열림 상태가 아니었는데 현재 열림 상태일 경우 열림음 발생
	@remark 3.외부강제잠금 설정 상태에서 이전에 모터가 닫힘 상태였고, 현재 열림 상태일 경우 경보 발생
	@remark 4.이전에 모터가 열림/닫힘 상태가 아니었는데, 현재닫힘 상태일 경우 상태 변경
	@remark 5.이전에 모터가 중간 상태가 아니었는데, 현재중간 상태일 경우 상태 변경
	@remark 이전에 모터가 닫힘 상태이었을 경우에는 아래 조건에서만 자동 잠김 수행
				- MainMode 종료가 발생한 경우
				- 수동노브에 의한 열림이 발생한 경우
			위의 경우가 아니면 자동 잠김 수행 안함
*/
//------------------------------------------------------------------------------
void AutoRelockStartCheck(void)
{
	BYTE FinalStatus;
	BYTE SensorStatus;

	// 0.5초 간격으로 자동 잠김을 확인하게 함으로써 부저 동작이 느려지거나 빨라지는 문제 개선, 
	//	부저 동작은 다시 4ms 간격으로 수행하도록 처리하고, MotorSensorCheck도 무조건 수행하지 않도록 처리
	if(gLockStatusCheckTimer10ms)		return;
	gLockStatusCheckTimer10ms = 50;

	if(GetMainMode() || (AlarmStatusCheck() == STATUS_SUCCESS) || GetMotorPrcsStep())
	{
		// MainMode가 종료되거나 경보가 종료될 경우 바로 자동 잠김 수행이 가능하도록 하기 위해 
		gAutoLockTryCnt = 0;
		return;
	}
	
	FinalStatus = GetFinalMotorStatus();
	SensorStatus = MotorSensorCheck();

// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 시작	
#ifdef		P_SNS_LOCK_T
	if(gbInnerForceLockDetectionValue != gbInnerForceLockDetectionValueBackUp)
	{
		if(gbModuleMode == PACK_ID_CONFIRMED || gbInnerModuleMode == PACK_ID_CONFIRMED)
		{			
			gbInnerForceLockDetectionValueBackUp = gbInnerForceLockDetectionValue;
			if(gbInnerForceLockDetectionValue == 0x00)	//내부 강제 잠금 Set
			{
				PackTx_MakeAlarmPacket(AL_FORCED_LOCKED, 0x00, 0x00);	//내부 강제 잠금 Set
			}
			else										//내부 강제 잠금 Claer
			{
				PackTx_MakeAlarmPacket(AL_FORCED_LOCKED, 0x00, 0x01);	//내부 강제 잠금 Claer
			}
		}
	}
#endif
// H/W Type 내부 강제 잠금 알람 리포트 추가 2016년11월14일 by심재철 추가 끝


	// Open, Close 모두 감지될 경우 에러로 인식하여 처리하지 않도록 내용 추가
	if((SensorStatus & SENSOR_SENSOR_ERROR) == SENSOR_SENSOR_ERROR)		return;

	switch(FinalStatus)
	{
		case FINAL_MOTOR_STATE_OPEN_ING:
		case FINAL_MOTOR_STATE_CLOSE_ING:
			break;

		case FINAL_MOTOR_STATE_OPEN:
			if(SensorStatus & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);

				// Manual Lock Event 전송
				PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);
				break;
			}
			else if((SensorStatus & SENSOR_OPEN_STATE) == 0)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE_ERROR);
			}

			RelockTimeResetCheck();
			break;
		
		default:
			if(SensorStatus & SENSOR_OPEN_STATE)
			{
#if  !defined	(DDL_CFG_CLUTCH) && !defined 	(DDL_CFG_CMPL_CLUTCH)
				if(SensorStatus & SENSOR_LOCK_STATE)
				{
#ifndef		P_SNS_LOCK_T	
					//내부강제잠금 해제
					if(InnerForcedLockCheck())
					{
						InnerForcedLockClear();
					}
#endif					
				}
				
				// 수동 노브에 의해 열림이 된 경우, 바로 자동 잠김 수행이 가능하도록 하기 위해 
				gAutoLockTryCnt = 0;

				if(FinalStatus == FINAL_MOTOR_STATE_CLOSE)
				{
					if(gfOutLock)
					{
						AlarmGotoIntrusionAlarm();		

						SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
						break;
					}
				}

				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);

				// Manual Unlock Event 전송
				PackTx_MakeAlarmPacket(AL_MANUAL_UNLOCKED, 0x00, 0x01);
				
				if(GetTamperProofPrcsStep())
				{
					TamperCountClear();
					FeedbackModeClear();
				}
				else
				{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();// 2018-6-18 moonsungwoo, 수동 노브로 오픈 했을 때,
#endif 
					FeedbackMotorOpen();
					ModeClear();
				}
#endif 				
			}
			else if(SensorStatus & SENSOR_CLOSE_STATE)
			{
				if(FinalStatus != FINAL_MOTOR_STATE_CLOSE)
				{
					SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);

					// Manual Lock Event 전송
					PackTx_MakeAlarmPacket(AL_MANUAL_LOCKED, 0x00, 0x01);
				}
			}
			else
			{
				if((FinalStatus == FINAL_MOTOR_STATE_CLOSE_ERROR) 
					|| (FinalStatus == FINAL_MOTOR_STATE_OPEN_ERROR))
				{
					RelockTimeResetCheck();
				}

			/*
				// 해당 부분은 데드볼트 걸림 LED 표시를 위해 필요하지만, 다른 자동 잠김 동작에 영향을 줄 수 있어 일단 보류,
				//	당장은 사양에 없어도 되지 추후 충분한 테스트 후 사용 결정.
				//	해당 내용이 들어가지 않을 경우에는 닫힘 상태 이후 수동 노브로 중간 상태를 만들면 데드볼트 걸림 LED 표시가 되지 않음.
				else
				{
					FinalStatus == FINAL_MOTOR_STATE_CLOSE_ERROR;
				}
			*/
			}
			break;
	}	
}



//------------------------------------------------------------------------------
/** 	@brief	Auto Lock Condition Check
	@param	None
	@return 	None
	@remark 문이 닫힐 때 2초 자동 잠김 수행 설정
*/
//------------------------------------------------------------------------------
void AutolockStartCheck(void)
{
	// Factory Reset 이후 기본 비밀번호가 등록되지 않을 경우 모터 잠김 수행 안함.
	if(FactoryResetProcessedCheck() == STATUS_SUCCESS)
	{
		FeedbackErrorFactoryReset(VOICE_MIDI_ERROR, VOL_HIGH);
		gLockStatusPrcsStep = 0;
		return;
	}
	
	if(AutoRelockProcessClearCheck())
	{
		gLockStatusPrcsStep = 0;	
		return;
	}

	FeedbackDoorSensorDetect();

	gAutoLockTryCnt = 1;
	gLockStatusTimer1s = DEFAULT_AUTOLOCK_TIME;
	
	gLockStatusPrcsStep = LOCKSTS_AUTOLOCK_CLOSE;		
}



//------------------------------------------------------------------------------
/** 	@brief	Auto Lock Motor Close Check
	@param	None
	@return 	None
	@remark 자동 잠김에 의해 모터가 닫힘이 되었는지 확인
*/
//------------------------------------------------------------------------------
void RelockCloseCompleteCheck(void)
{
	BYTE bTmp;

	bTmp = GetFinalMotorStatus();
	
	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ING)		return;
	
	//if(GetMotorPrcsStep())	return;								//저전압시 자동잠김 카운트 초기화 하던 문제로 조건 추가
	
	if(bTmp == FINAL_MOTOR_STATE_CLOSE_ERROR)
	{
		// 모터 닫힘 동작 중 열림 센서 위치에서 전혀 이동하지 못하고 에러가 발생할 경우 데드볼트 걸림 에러 LED가 켜지지 않는 문제 수정
		gLockStatusPrcsStep = LOCKSTS_AUTOLOCK_CLOSED_ERROR;

		FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);
	
		// Pack Module로 Event 내용 전송 
		PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x00);
		gLockStatusCheckTimer10ms = 100;
	}
	else if(bTmp == FINAL_MOTOR_STATE_CLOSE)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		FeedbackMotorClose();// 2018-6-18 moonsungwoo, 모터 동작 완료 후 피드백 		
		gbFeedbackCloseOnceTimercnt100ms = 0;
#endif
		// Pack Module로 Event 내용 전송 
		PackTx_MakeAlarmPacket(AL_AUTO_LOCK_OP_LOCKED, 0x00, 0x01);

		gfMute =0;

		// 모터가 닫힘 상태일 경우 MainMode 종료나, 수동노브에 의한 열림이 아닐 경우 다시 자동 잠김 수행 안함
		gAutoLockTryCnt = _MAX_AUTOLOCK_TRYCNT;

		gLockStatusPrcsStep = 0;
	}
	else if(bTmp == FINAL_MOTOR_STATE_OPEN_ING || bTmp == FINAL_MOTOR_STATE_OPEN ) 
	{
		// 자동 잠금 실행 중에 외부에 의해 (BLE , 혹은 카드등과 같은 인증 수단에 의해 ) 다시 모터가 opening 이 되면 
		// autorelock 을 다시 확인 할 수 있게 
		gfMute =0; // mute 도 푸는게 맞을 듯 
		gAutoLockTryCnt = 0;
		gLockStatusPrcsStep = 0;
	}
}


//------------------------------------------------------------------------------
/** 	@brief	Intrusion Alarm Check
	@param	None
	@return 	None
	@remark 문 상태 감지 센서가 떨어질 때 
				- 현재 자동 잠김 진행 중일 경우, 모터 열림 수행
				- 현재 모터 닫힘 상태일 경우 경보 발생 
*/
//------------------------------------------------------------------------------
void IntrusionAlarmCheck(void)
{
	BYTE FinalStatus;
	BYTE SensorStatus;
	
	FinalStatus = GetFinalMotorStatus();
	SensorStatus = MotorSensorCheck();
	switch(FinalStatus)
	{
		case FINAL_MOTOR_STATE_OPEN_ING:
		case FINAL_MOTOR_STATE_CLOSE_ING:
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
			BatteryCheck();//moonsungwoo, check
#else
			FeedbackMotorOpen();
#endif
			StartMotorOpen();
			gLockStatusPrcsStep = LOCKSTS_AUTOLOCK_OPENED_CHK;
			break;
	
		default:
			if(SensorStatus & SENSOR_CLOSE_STATE)
			{
				if(SensorStatus & SENSOR_LOCK_STATE)
				{
					//내부강제잠금 해제 	
				}

#ifdef	P_SNS_EDGE_T			// Edge sensor
				if(DoorClosedTimeCheck() == 0)
#endif
				{
					AlarmGotoIntrusionAlarm();		
				}
				
#if defined (FEEDBACK_AFTER_MOTOR_BACKTURN) && defined (P_SNS_EDGE_T)
				else
				{					
					if(gbFeedbackCloseOnceTimercnt100ms)
					{
						gbFeedbackCloseOnceTimercnt100ms = 0;
						FeedbackMotorClose();// 2018-6-18 moonsungwoo, 모터 동작 완료 후 피드백 

						// Pack Module로 Event 내용 전송 
						PackTx_MakeAlarmPacket(AL_AUTO_LOCK_OP_LOCKED, 0x00, 0x01);
				
						gfMute =0;
				
						// 모터가 닫힘 상태일 경우 MainMode 종료나, 수동노브에 의한 열림이 아닐 경우 다시 자동 잠김 수행 안함
						gAutoLockTryCnt = _MAX_AUTOLOCK_TRYCNT;
					}
					
				}
#endif				
			}

			gLockStatusPrcsStep = 0;
			// 핸들이나 수동 노브에 의한 모터 열림이 이루어졌을 경우 바로 열림음 발생하기 위해
			gLockStatusCheckTimer10ms = 0;
			gLockStatusCheckCount = 0;
			break;
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Motor Open Check by running Door sensor open
	@param	None
	@return 	None
	@remark 자동 잠김 진행 중, 모터 열림 수행할 경우 그에 대한 결과 확인
*/
//------------------------------------------------------------------------------
void RelockOpenCompleteCheck(void)
{
	BYTE bTmp;

	bTmp = GetFinalMotorStatus();
	
	if(bTmp == FINAL_MOTOR_STATE_OPEN_ING)		return;
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
	//if(GetMotorPrcsStep())	return;// 2018-6-18 moonsungwoo, 모터가 백턴까지 마친 후, 통과
#endif	
	if(bTmp == FINAL_MOTOR_STATE_OPEN_ERROR)
	{
		// 열림 시도 중 닫힘 위치에서 전혀 움직이지 않고, 데드볼트 걸림 에러가 발생할 경우 데드볼트 걸림 LED가 동작하지 않는 문제 수정
		PackTx_MakeAlarmPacket(AL_DEADBOLT_JAMMED, 0x00, 0x01);
		FeedbackError(VOICE_MIDI_ERROR, VOL_HIGH);

		gLockStatusPrcsStep = LOCKSTS_AUTOLOCK_CLOSED_ERROR;
	}
	else if(bTmp == FINAL_MOTOR_STATE_OPEN)
	{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
		FeedbackMotorOpen();// 2018-6-18 moonsungwoo, 모터 완전히 동작 완료 후, 피드백 
#endif
		// 모터 잠김 동작 중 열림 처리한 것이라 별도의 Pack으로 Event 전송 없음.
		gLockStatusPrcsStep = 0;
	}

}



//------------------------------------------------------------------------------
/** 	@brief	Auto Relock Process
	@param	None
	@return 	None
	@remark 자동 잠김 수행을 위한 프로세스 함수
*/
//------------------------------------------------------------------------------
void LockStatusProcess(void)
{
	static BYTE bPreLockStatusTimer = 0x00;

	DoorSwCheck();

	switch(gLockStatusPrcsStep)
	{
		case 0:
			AutoRelockStartCheck();
			break;

		case LOCKSTS_AUTOLOCK_CHECK:
			AutolockStartCheck();
			break;

		case LOCKSTS_AUTOLOCK_CLOSE:
			if(gLockStatusTimer1s)
			{
				if(bPreLockStatusTimer != gLockStatusTimer1s)
				{
					AutoRelockProcessClearCheck();
					bPreLockStatusTimer = gLockStatusTimer1s;
				}
				break;
			}
			bPreLockStatusTimer = 0x00;
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
			if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				/* 7초나 3초 타이밍 에 맞춰서 close 를 시도 하고 feedback 난 상태 때 자동 잠김 진입시 close feedback 두번 나는 문제*/
				/* 해서 이미 닫힌 상태면 gLockStatusPrcsStep 로 보낸다 */
				gLockStatusPrcsStep = 0;
				break;
			}
			BatteryCheck();
			gbFeedbackCloseOnceTimercnt100ms = 30;
#else
			FeedbackMotorClose();
#endif
			StartMotorClose();
			gLockStatusPrcsStep = LOCKSTS_AUTOLOCK_CLOSED_CHK;
			break;

		case LOCKSTS_AUTOLOCK_CLOSED_CHK:
			RelockCloseCompleteCheck();
			break;			

		// 모터 닫힘 동작 중 열림 센서 위치에서 전혀 이동하지 못하고 에러가 발생할 경우 
		// 모터 열림 동작 중 닫힘 센서 위치에서 전혀 이동하지 못하고 에러가 발생할 경우 
		// 데드볼트 걸림 에러 LED가 켜지지 않는 문제 수정
		case LOCKSTS_AUTOLOCK_CLOSED_ERROR:
			if(GetLedMode())	break;

			if(MotorSensorCheck() & SENSOR_OPEN_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_OPEN);
			}
			else if(MotorSensorCheck() & SENSOR_CLOSE_STATE)
			{
				SetFinalMotorStatus(FINAL_MOTOR_STATE_CLOSE);
			}

			gLockStatusPrcsStep = 0;
			break;			

		case LOCKSTS_ALARM_CHECK:
			PCErrorClearSet();
			IntrusionAlarmCheck();
			break;

		case LOCKSTS_AUTOLOCK_OPENED_CHK:
			RelockOpenCompleteCheck();
			break;			

		default:
			gLockStatusPrcsStep = 0;
			break;
	}
}


BYTE GetLockStatusPrcsStep(void)
{	
	return (gLockStatusPrcsStep);
}	






#ifdef	P_SNS_EDGE_T			// Edge sensor

BYTE gbDoorClosedTimer1s = 0;


void DoorClosedCheckCounter(void)
{
	if(gbDoorClosedTimer1s)		--gbDoorClosedTimer1s;
}

void DoorClosedTimeSet(void)
{
	gbDoorClosedTimer1s = 5;
}

BYTE DoorClosedTimeCheck(void)
{
	return (gbDoorClosedTimer1s);
}

#endif




BYTE GetRemainingAutoRelockTime(void)
{
	return (gLockStatusTimer1s);	
}


// 아래 부분은 연동 과정에서 사용할 수도 있기에 추후 정리할 예정

/*
BYTE gbAutoRelockTimeVal = 0;
BYTE gbAutoGeneralRelockTimeVal = 0;
BYTE gbAutoRemoteRelockTimeVal = 0;
	
	

BYTE AutoRelockTimeValueLoad(void)
{
//	RomRead(&gbAutoGeneralRelockTimeVal, (WORD)RELOCK_TIME, 1);

	if((gbAutoGeneralRelockTimeVal < MIN_RELOCK_TIME) || (gbAutoGeneralRelockTimeVal > MAX_RELOCK_TIME))
	{
		gbAutoGeneralRelockTimeVal = DEFAULT_RELOCK_TIME;
	}

	return gbAutoGeneralRelockTimeVal; 
}


void AutoRelockTimeValueSet(BYTE bVal )
{
	gbAutoGeneralRelockTimeVal = bVal;

	if(gbAutoGeneralRelockTimeVal < MIN_RELOCK_TIME)
	{
		gbAutoGeneralRelockTimeVal = MIN_RELOCK_TIME;
	}
	else if(gbAutoGeneralRelockTimeVal > MAX_RELOCK_TIME)
	{
		gbAutoGeneralRelockTimeVal = MAX_RELOCK_TIME;
	}

//	RomWrite(&gbAutoGeneralRelockTimeVal, (WORD)RELOCK_TIME, 1);
}


void SetGeneralRelockTimeForAutolock(void)
{
	gbAutoRelockTimeVal = gbAutoGeneralRelockTimeVal;
}

void SetRemoteRelockTimeForAutolock(void)
{
	gbAutoRelockTimeVal = gbAutoRemoteRelockTimeVal;
}


*/

#ifdef DDL_CFG_AUTO_KEY_PROCESS
void AutoSwProcess(void)
{
	BYTE bTmp;

	// auto key 가 변경 되는 경우 처리 
	if(!gfXORAutoLockSw)	return;
	gfXORAutoLockSw = 0;

	if(GetMainMode() != 0x00 || JigModeStatusCheck() == STATUS_SUCCESS) 	return;

	bTmp = MotorSensorCheck();

#ifdef P_SNS_CENTER_T

	switch(bTmp)
	{
		case SENSOR_CLOSE_STATE:
		case SENSOR_CLOSELOCK_STATE:
			MotorFindCenter();
		break;

		case SENSOR_NOT_STATE:
		case SENSOR_LOCK_STATE:			
			StartMotorClose();
		break;

		default:
			break;
	}
#else 
	if(!P_SW_AUTO_T)		// 자동
	{	
		switch(bTmp)
		{
			case SENSOR_CLOSELOCK_STATE:
			case SENSOR_CLOSECENLOCK_STATE:
				PackAlarmReportInnerForcedLockOpenCloseFailSend();
				FeedbackLockIn();
				break;

			case SENSOR_CLOSE_STATE:
			case SENSOR_CLOSECEN_STATE:
			case SENSOR_OPEN_STATE:
			case SENSOR_OPENCEN_STATE:
			case SENSOR_OPENLOCK_STATE:
			case SENSOR_OPENCENLOCK_STATE:
			case SENSOR_NOT_STATE:
			case SENSOR_LOCK_STATE:
			case SENSOR_CENTER_STATE:
			case SENSOR_CENTERLOCK_STATE:
//				case SENSOR_CHECK_ERROR:
				if(!gfOutLock)	
				{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorClose();
#endif
					StartMotorClose();
				}
				else 
				{
					FeedbackLockOut();
				}
				break;
				
			default:
				break;
		}
	}
	else			// 수동
	{
		switch(bTmp)
		{
			case SENSOR_OPENLOCK_STATE:
			case SENSOR_OPENCENLOCK_STATE:
				PackAlarmReportInnerForcedLockOpenCloseFailSend();
				FeedbackLockIn();
				break;

			case SENSOR_OPEN_STATE:
			case SENSOR_OPENCEN_STATE:
			case SENSOR_CLOSE_STATE:
			case SENSOR_CLOSECEN_STATE:
			case SENSOR_CLOSELOCK_STATE:
			case SENSOR_CLOSECENLOCK_STATE:
			case SENSOR_NOT_STATE:
			case SENSOR_LOCK_STATE:
			case SENSOR_CENTER_STATE:
			case SENSOR_CENTERLOCK_STATE:
//			case SENSOR_CHECK_ERROR:		
				if(!gfOutLock)	
				{
#ifdef FEEDBACK_AFTER_MOTOR_BACKTURN
					BatteryCheck();
#else
					FeedbackMotorOpen();
#endif
					PCErrorClearSet();
					StartMotorOpen();
				}
				else
				{
					FeedbackLockOut();
				}
				break;

			default:
				break;
		}
	}
#endif 	
}
#endif 

