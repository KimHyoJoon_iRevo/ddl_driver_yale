#ifndef	__UTILITYFUNCTIONS_T1_INCLUDED
#define	__UTILITYFUNCTIONS_T1_INCLUDED

#include "DefineMacro.h"
//#include "Definepin.h"

typedef union
{
	uint64_t InputData;						
	BYTE OutputData[sizeof(InputData)];
}QWORDTOBYTE;

BYTE DataCompare(BYTE *bpData, BYTE bData, BYTE bNum);

#define	_DATA_IDENTIFIED			0x00
#define	_DATA_NOT_IDENTIFIED		0xFF


void QWORDToBYTE(uint64_t Data , BYTE* OutputBuffer , BYTE Order);
void BYTEToQWORD(uint64_t *Data , BYTE* InputBuffer , BYTE Order);

uint64_t RandomNumberGenerator(BYTE* bpData , BYTE bSize);

BYTE BCDToDEC(BYTE BCDData);

#if 0
extern uint32_t*	MSP_psp;
void SaveMSP2ROM(void);
void InitMSPROM(void);
void BootCount(uint8_t clear);
#endif 
#endif

