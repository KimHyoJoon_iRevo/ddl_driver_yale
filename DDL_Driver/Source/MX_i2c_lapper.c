/******************************************************************************
*	CubeMX에서 만들어지는 uart 관련 lib를 사용하기 위한 lapper 들
*
*	MCU의 IP로 I2C를 사용하는 경우와, GPIO로 I2C를 access하는 것을 동일한 인터페이스로 지원
*
*******************************************************************************/

#define		_MX_I2C_LAPPER_C_

#include "stm32l1xx_hal.h"

#include "MX_i2c_lapper.h"
#include "main.h"



/******************************************************************************
*	ddl_i2c_t 변수 초기화 예
*
*
*	MCU I2C를 사용할 경우
*
*		ddl_i2c_t	my_i2c_dev;
*
*		my_i2c_dev.type = DDL_I2C_MCU;
*		my_i2c_dev.dev.mcu.hi2c = gh_mcu_i2c;
*
*
*	gpio I2C를 사용할 경우
*
*		ddl_i2c_t	my_i2c_dev;
*
*		my_i2c_dev.type = DDL_I2C_GPIO;
*		my_i2c_dev.dev.gpio.SCL_GPIOx		= DIM_SCL_GPIO_Port;		//mxconstants.h에 선언되어 있음 
*		my_i2c_dev.dev.gpio.SCL_GPIO_Pin	= DIM_SCL_Pin;
*		my_i2c_dev.dev.gpio.SDA_GPIOx		= DIM_SDA_GPIO_Port;
*		my_i2c_dev.dev.gpio.SDA_GPIO_Pin	= DIM_SDA_Pin;
*
*******************************************************************************/


//------------------------------------------------------------------------------
/** 	@file		MX_i2c_lapper.c
	@version 0.1.03
	@date	2016.03.31
	@brief	I2C Interface Functions
	@see	Stm32l1xx_hal_i2c.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.01	2016.03.29		by Jay
			EEPROM, RTC I2C 통신을 위해 아래의 내용들 추가
			- ddl_i2c_write, ddl_i2c_read 함수에 입력 인자 추가 
				uint16_t address, unit16_t addsize 
		       - ddl_i2c_write, ddl_i2c_read 함수에 처리 내용 추가
		       	HAL_I2C_Mem_Write, HAL_I2C_Mem_Read
			- ddl_i2c_t의 변수 type의 내용 추가 
			    	DDL_I2C_MCU_TYPE1 : EEPROM, RTC I2C 통신을 위한 모드

		V0.1.02 2016.03.30		by Youn
			ddl_i2c_t 에서 slave_id 인자 추가
			ddl_i2c_write, ddl_i2c_read 함수에서 slaveid 입력 인자 삭제

		V0.1.03 2016.03.31		by Jay
			ddl_i2c_write, ddl_i2c_read 함수에서 DDL_I2C_MCU_TYPE1 처리시 
				addsize 확인하여 8bit Address와 16bit Address 구분 처리 내용 추가

*/
//------------------------------------------------------------------------------


//default로 output인 SDA를 input으로 전환하는 함수
static	void change_i2c_sda_input( ddl_i2c_t *ddl_i2c )
{
	GPIO_InitTypeDef GPIO_InitStruct;

	if ( ddl_i2c->type != DDL_I2C_GPIO )
		return;

	/*Configure GPIO SDA pin as input */
	GPIO_InitStruct.Pin = ddl_i2c->dev.gpio.SDA_GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init( ddl_i2c->dev.gpio.SDA_GPIOx, &GPIO_InitStruct );
}

//SDA를 다시 output으로 전환하는 함수
static	void change_i2c_sda_output( ddl_i2c_t *ddl_i2c, int init_val )
{
	GPIO_InitTypeDef GPIO_InitStruct;

	if ( ddl_i2c->type != DDL_I2C_GPIO )
		return;

	// first write output value and change gpio direction
	HAL_GPIO_WritePin( ddl_i2c->dev.gpio.SDA_GPIOx, ddl_i2c->dev.gpio.SDA_GPIO_Pin, (GPIO_PinState) init_val );

	/*Configure GPIO SDA pin as output */
	GPIO_InitStruct.Pin = ddl_i2c->dev.gpio.SDA_GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init( ddl_i2c->dev.gpio.SDA_GPIOx, &GPIO_InitStruct );
}


//assert SCL
static	void	high_gpio_i2c_scl( ddl_i2c_t *ddl_i2c )
{
	if ( ddl_i2c->type != DDL_I2C_GPIO )
		return;

	HAL_GPIO_WritePin( ddl_i2c->dev.gpio.SCL_GPIOx, ddl_i2c->dev.gpio.SCL_GPIO_Pin, GPIO_PIN_SET);
}

//negate SCL
static	void	low_gpio_i2c_scl( ddl_i2c_t *ddl_i2c )
{
	if ( ddl_i2c->type != DDL_I2C_GPIO )
		return;

	HAL_GPIO_WritePin( ddl_i2c->dev.gpio.SCL_GPIOx, ddl_i2c->dev.gpio.SCL_GPIO_Pin, GPIO_PIN_RESET);
}

// assert SDA
static	void	high_gpio_i2c_sda( ddl_i2c_t *ddl_i2c )
{
	if ( ddl_i2c->type != DDL_I2C_GPIO )
		return;

	HAL_GPIO_WritePin( ddl_i2c->dev.gpio.SDA_GPIOx, ddl_i2c->dev.gpio.SDA_GPIO_Pin, GPIO_PIN_SET);
}

// negate SDA
static	void	low_gpio_i2c_sda( ddl_i2c_t *ddl_i2c )
{
	if ( ddl_i2c->type != DDL_I2C_GPIO )
		return;

	HAL_GPIO_WritePin( ddl_i2c->dev.gpio.SDA_GPIOx, ddl_i2c->dev.gpio.SDA_GPIO_Pin, GPIO_PIN_RESET);
}

// read SDA as input
static	uint8_t		get_gpio_i2c_sda( ddl_i2c_t *ddl_i2c )
{
	if ( ddl_i2c->type != DDL_I2C_GPIO )
		return 0xff;

	return	(uint8_t)HAL_GPIO_ReadPin( ddl_i2c->dev.gpio.SDA_GPIOx, ddl_i2c->dev.gpio.SDA_GPIO_Pin );
}



/* ======================= i2c signal functions ========================= */
static	void	gpio_i2c_delay( void )
{
	uint32_t		i;

	for( i=0; i<GPIO_I2C_DELAY; i++ )
		__NOP();
}

static	void	ddl_gpio_i2c_start( ddl_i2c_t *ddl_i2c )
{
	// initial state
	high_gpio_i2c_sda( ddl_i2c );
	high_gpio_i2c_scl( ddl_i2c );
	gpio_i2c_delay();

	// start condition
	low_gpio_i2c_sda( ddl_i2c );
	gpio_i2c_delay();
	low_gpio_i2c_scl( ddl_i2c );
	gpio_i2c_delay();
}


static	void	ddl_gpio_i2c_stop( ddl_i2c_t *ddl_i2c )
{
	low_gpio_i2c_sda( ddl_i2c );
	gpio_i2c_delay();

	// stop condition
	high_gpio_i2c_scl( ddl_i2c );
	gpio_i2c_delay();

	high_gpio_i2c_sda( ddl_i2c );
	gpio_i2c_delay();
}


static	unsigned	ddl_gpio_i2c_wr_addr_cmd( ddl_i2c_t *ddl_i2c, unsigned char cmd, unsigned char ignore_nack )
{
	uint16_t		i;
	uint8_t		bmask, ack;

	for( i=0, bmask=(1<<7); i<7; i++, bmask>>=1 ) {
		if ( ddl_i2c->slave_id & bmask )
			high_gpio_i2c_sda( ddl_i2c );
		else	
			low_gpio_i2c_sda( ddl_i2c );
		gpio_i2c_delay();

		high_gpio_i2c_scl( ddl_i2c );
		gpio_i2c_delay();
		low_gpio_i2c_scl( ddl_i2c );
		gpio_i2c_delay();
	}

	if ( cmd )
		high_gpio_i2c_sda( ddl_i2c );
	else	
		low_gpio_i2c_sda( ddl_i2c );
	gpio_i2c_delay();
		
	high_gpio_i2c_scl( ddl_i2c );
	gpio_i2c_delay();
	low_gpio_i2c_scl( ddl_i2c );
	gpio_i2c_delay();

// check slave ack
	change_i2c_sda_input( ddl_i2c );
	gpio_i2c_delay();

	high_gpio_i2c_scl( ddl_i2c );
	gpio_i2c_delay();

	ack = get_gpio_i2c_sda( ddl_i2c );

	low_gpio_i2c_scl( ddl_i2c );
	gpio_i2c_delay();
	change_i2c_sda_output( ddl_i2c, I2C_PIN_LOW );

//	high_gpio_i2c_sda;
	gpio_i2c_delay();

	if( ignore_nack )
		return	I2C_ACK;
	else
		return	(unsigned) ack;
}


static	unsigned	ddl_gpio_i2c_wr_8bit( ddl_i2c_t *ddl_i2c, unsigned char data, unsigned char ignore_nack )
{
	uint16_t		i;
	uint8_t		bmask, ack;

	for( i=0, bmask=(1<<7); i<8; i++, bmask>>=1) {
		if ( data & bmask )
			high_gpio_i2c_sda( ddl_i2c );
		else	
			low_gpio_i2c_sda( ddl_i2c );
		gpio_i2c_delay();

		high_gpio_i2c_scl( ddl_i2c );
		gpio_i2c_delay();
		low_gpio_i2c_scl( ddl_i2c );
		gpio_i2c_delay();
	}

// check slave ack
	change_i2c_sda_input( ddl_i2c );
	gpio_i2c_delay();

	high_gpio_i2c_scl( ddl_i2c );
	gpio_i2c_delay();

	ack = get_gpio_i2c_sda( ddl_i2c );

	low_gpio_i2c_scl( ddl_i2c );
	gpio_i2c_delay();
	change_i2c_sda_output( ddl_i2c, I2C_PIN_LOW );

	gpio_i2c_delay();

	if( ignore_nack )
		return	I2C_ACK;
	else
		return	(unsigned) ack;
}



static	unsigned	ddl_gpio_i2c_rd_8bit( ddl_i2c_t *ddl_i2c, unsigned char *data, unsigned char last_nack )
{
	uint16_t		i;
	uint8_t		bmask, rbit, tmp;

	change_i2c_sda_input( ddl_i2c );
	gpio_i2c_delay();
	for( i=0, bmask=(1<<7), tmp=0; i<8; i++, bmask>>=1 ){
		high_gpio_i2c_scl( ddl_i2c );
		gpio_i2c_delay();
		rbit = get_gpio_i2c_sda( ddl_i2c );
		low_gpio_i2c_scl( ddl_i2c );
		gpio_i2c_delay();

		if ( rbit )
			tmp |= bmask;
	}

// send ack to slave
	change_i2c_sda_output( ddl_i2c, last_nack );
	gpio_i2c_delay();

	high_gpio_i2c_scl( ddl_i2c );
	gpio_i2c_delay();

	low_gpio_i2c_scl( ddl_i2c );
	gpio_i2c_delay();

//	change_i2c_sda_input( ddl_i2c, I2C_PIN_LOW );

	*data = tmp;

	return	I2C_ACK;
}


/*****************************************************************************/

void	ddl_gpio_i2c_write( ddl_i2c_t *ddl_i2c, uint8_t *data, uint16_t length )
{
	uint16_t		i;

	ddl_gpio_i2c_start( ddl_i2c );
	ddl_gpio_i2c_wr_addr_cmd( ddl_i2c, I2C_CMD_WR, I2C_NORMAL_NACK);

	for( i=0; i<length; i++ )
		ddl_gpio_i2c_wr_8bit( ddl_i2c, data[i], I2C_NORMAL_NACK);

	ddl_gpio_i2c_stop( ddl_i2c );
}


void	ddl_gpio_i2c_read( ddl_i2c_t *ddl_i2c, uint8_t *data, uint16_t length )
{
	uint16_t		i;

	ddl_gpio_i2c_start( ddl_i2c );
	ddl_gpio_i2c_wr_addr_cmd( ddl_i2c, I2C_CMD_RD, I2C_NORMAL_NACK);

	for( i=0; i<length; i++ )
		ddl_gpio_i2c_rd_8bit( ddl_i2c, &data[i], I2C_NORMAL_NACK);

	ddl_gpio_i2c_stop( ddl_i2c );
}


/*****************************************************************************/

// EEPROM, RTC I2C 통신을 위해 입력 인자 추가
//	uint16_t address, unit16_t addsize 

void	ddl_i2c_write( ddl_i2c_t *ddl_i2c, uint16_t address, uint16_t addsize, uint8_t *data, uint16_t length )
{
	uint16_t		i;

	if ( ddl_i2c->type == DDL_I2C_MCU ) {
		if(HAL_OK != HAL_I2C_Master_Transmit( ddl_i2c->dev.mcu.hi2c, ddl_i2c->slave_id, data, length, MCU_I2C_TIMEOUT ))
		{
				ddl_i2c_error_repair();
		}
	}
	else if ( ddl_i2c->type == DDL_I2C_GPIO ) {
		ddl_gpio_i2c_start( ddl_i2c );
		ddl_gpio_i2c_wr_addr_cmd( ddl_i2c, I2C_CMD_WR, I2C_NORMAL_NACK);
		
		for( i=0; i<length; i++ )
			ddl_gpio_i2c_wr_8bit( ddl_i2c, data[i], I2C_NORMAL_NACK);
		
		ddl_gpio_i2c_stop( ddl_i2c );
	}
// EEPROM, RTC I2C 통신을 위한 모드
	else if ( ddl_i2c->type == DDL_I2C_MCU_TYPE1) {		// (I2C_EEPROM_ADDRESS | ((wAdrs >> 7) & 0x06)), 
		// 8bit Address와 16bit Address 구분 처리
		if(addsize == I2C_MEMADD_SIZE_8BIT){
			if(HAL_OK != HAL_I2C_Mem_Write( ddl_i2c->dev.mcu.hi2c, ddl_i2c->slave_id | ((address >> 7) & 0x06), address, addsize, data, length, MCU_I2C_TIMEOUT ))
			{
				ddl_i2c_error_repair();
			}
		}
		else{
			if(HAL_OK != HAL_I2C_Mem_Write( ddl_i2c->dev.mcu.hi2c, ddl_i2c->slave_id, address, addsize, data, length, MCU_I2C_TIMEOUT ))
			{	
				 ddl_i2c_error_repair();
			}
		}
	}
	else {
	}
}


// EEPROM, RTC I2C 통신을 위해 입력 인자 추가
//	uint16_t address, unit16_t addsize 

void	ddl_i2c_read( ddl_i2c_t *ddl_i2c, uint16_t address, uint16_t addsize, uint8_t *data, uint16_t length )
{
	uint16_t		i;

	if ( ddl_i2c->type == DDL_I2C_MCU ) {
		if(HAL_OK != HAL_I2C_Master_Receive( ddl_i2c->dev.mcu.hi2c, ddl_i2c->slave_id, data, length, MCU_I2C_TIMEOUT ))
		{
			ddl_i2c_error_repair();
		}
	}
	else if ( ddl_i2c->type == DDL_I2C_GPIO ) {
		if ( length == 0 )
			return;
		ddl_gpio_i2c_start( ddl_i2c );
		ddl_gpio_i2c_wr_addr_cmd( ddl_i2c, I2C_CMD_RD, I2C_NORMAL_NACK);

		// from 0 to last -1
		for( i=0; i<length-1; i++ )
			ddl_gpio_i2c_rd_8bit( ddl_i2c, &data[i], I2C_ACK);
		// last
		ddl_gpio_i2c_rd_8bit( ddl_i2c, &data[i], I2C_NACK);

		ddl_gpio_i2c_stop( ddl_i2c );
		gpio_i2c_delay();
	}
// EEPROM, RTC I2C 통신을 위한 모드
	else if ( ddl_i2c->type == DDL_I2C_MCU_TYPE1) {
		// 8bit Address와 16bit Address 구분 처리
		if(addsize == I2C_MEMADD_SIZE_8BIT){
			if(HAL_OK != HAL_I2C_Mem_Read( ddl_i2c->dev.mcu.hi2c, ddl_i2c->slave_id | ((address >> 7) & 0x06), address, addsize, data, length, MCU_I2C_TIMEOUT ))
			{	
				ddl_i2c_error_repair();
			}
		}
                else{
			if(HAL_OK != HAL_I2C_Mem_Read( ddl_i2c->dev.mcu.hi2c, ddl_i2c->slave_id, address, addsize, data, length, MCU_I2C_TIMEOUT ))
			{				
				ddl_i2c_error_repair();
			}
		}
	}
	else {
	}
}

void ddl_i2c_error_repair(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
  
	/**I2C1 GPIO Configuration	  
	PB6 	------> I2C1_SCL
	PB7 	------> I2C1_SDA 
	*/
	GPIO_InitStruct.Pin = I2C_SCL_Pin|I2C_SDA_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOB,( I2C_SCL_Pin|I2C_SDA_Pin),GPIO_PIN_RESET);
	HAL_Delay(100);					
	HAL_GPIO_WritePin(GPIOB,( I2C_SCL_Pin|I2C_SDA_Pin),GPIO_PIN_SET);
	
	HAL_I2C_DeInit(gh_mcu_i2c);
	HAL_I2C_Init(gh_mcu_i2c);	
}



