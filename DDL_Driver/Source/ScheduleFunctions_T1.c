#include	"Main.h"




#if (DDL_CFG_BLE_30_ENABLE >= 0x29) && defined (_USE_ST32_EEPROM_)

#define	MODE_YEAR_DAY			0
#define	MODE_DAILY_REPEATING	1

const WORD gStartAdrsForSchedule[2][3] = 
{
	{SCHEDULE_YEAR_DAY_PIN, SCHEDULE_YEAR_DAY_CARD, SCHEDULE_YEAR_DAY_FINGERPRINT},
	{SCHEDULE_DAILY_REPEAT_PIN, SCHEDULE_DAILY_REPEAT_CARD, SCHEDULE_DAILY_REPEAT_FINGERPRINT}
};

BYTE SlotNumberCheck(BYTE CredentialType, BYTE SlotNumber)
{
	if(SlotNumber == 0)		return 0;

	if(CredentialType == CREDENTIALTYPE_PINCODE)
	{
		if(SlotNumber > SUPPORTED_USERCODE_NUMBER)	return 0;
	}
#ifdef 	DDL_CFG_RFID
	else if(CredentialType == CREDENTIALTYPE_CARD)
	{
		if(SlotNumber > SUPPORTED_USERCARD_NUMBER)	return 0;
	}
#endif
#ifdef 	DDL_CFG_FP
	else if(CredentialType == CREDENTIALTYPE_FINGERPRINT)
	{
		if(SlotNumber > SUPPORTED_FINGERPRINT_NUMBER)	return 0;
	}
#endif

	return 1;
}



BYTE SlotNumberExtendCheck(BYTE *SlotNumber)
{
	BYTE CredentialType;	
	BYTE bSlotNum;

	// 전체 사용자 비밀번호
	if(((SlotNumber[0] == 0xFF) && (SlotNumber[1] == 0xFF)) 
		|| ((SlotNumber[0] == 0x0F) && (SlotNumber[1] == 0xFF)))
	{
		return 1;
	}

#ifdef 	DDL_CFG_RFID
	// 전체 사용자 카드
	else if((SlotNumber[0] == 0x1F) && (SlotNumber[1] == 0xFF)) 
	{
		return 1;
	}
#endif	
#ifdef 	DDL_CFG_FP
	// 전체 사용자 지문
	else if((SlotNumber[0] == 0x2F) && (SlotNumber[1] == 0xFF)) 
	{
		return 1;
	}
#endif
	else
	{
		bSlotNum = SlotNumber[1];
		CredentialType = (SlotNumber[0] >> 4) & 0x0F;
		
		if(SlotNumberCheck(CredentialType, bSlotNum) == 0)	return 0;
	}

	return 1;
}


WORD GetStartAddressByCredentialType(BYTE CredentialType, BYTE bMode)
{
	return (gStartAdrsForSchedule[bMode][CredentialType]);
}


BYTE GetUserSchduleStatus(BYTE *SlotNumber)
{
	BYTE bTmp = 0;
	WORD wAdrs;
	BYTE bData;
	BYTE CredentialType;	
	BYTE bSlotNum;

	bSlotNum = SlotNumber[1];
	CredentialType = (SlotNumber[0] >> 4) & 0x0F;

	if(SlotNumberCheck(CredentialType, bSlotNum) == 0)	return (0xFF);
	bSlotNum--;

	wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_YEAR_DAY);
	wAdrs += (WORD)bSlotNum*BYTENUM_OF_YEARDAY;

	RomRead(&bData, wAdrs, 1);
	if(bData == 0xFF)	bTmp |= 0x40;
	
	wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_DAILY_REPEATING);
	wAdrs += (WORD)bSlotNum*BYTENUM_OF_DAILY_REPEAT;

	RomRead(&bData, wAdrs, 1);
	if(bData == 0xFF)	bTmp |= 0xA0;

	return (bTmp);
}



void SetDailyRepeatingSchdule(BYTE *SlotNumber, BYTE *bScheduleData)
{
	BYTE bCnt;
	WORD wAdrs;
	BYTE bCopiedScheduleData[BYTENUM_OF_DAILY_REPEAT];
	BYTE CredentialType;	
	BYTE bSlotNum;

	bCopiedScheduleData[0] = bScheduleData[0];
	for(bCnt = 0; bCnt < (BYTENUM_OF_DAILY_REPEAT-1); bCnt++)
	{
		bCopiedScheduleData[1+bCnt] = bScheduleData[4+bCnt];
	}

	// 전체 사용자 비밀번호
	if(((SlotNumber[0] == 0xFF) && (SlotNumber[1] == 0xFF)) 
		|| ((SlotNumber[0] == 0x0F) && (SlotNumber[1] == 0xFF)))
	{
		for(bCnt = 0; bCnt < SUPPORTED_USERCODE_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_DAILY_REPEAT_PIN + ((WORD)bCnt*BYTENUM_OF_DAILY_REPEAT);
			RomWrite(bCopiedScheduleData, wAdrs, BYTENUM_OF_DAILY_REPEAT);
		}
	}
#ifdef 	DDL_CFG_RFID
	// 전체 사용자 카드
	else if((SlotNumber[0] == 0x1F) && (SlotNumber[1] == 0xFF)) 
	{
		for(bCnt = 0; bCnt < SUPPORTED_USERCARD_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_DAILY_REPEAT_CARD + ((WORD)bCnt*BYTENUM_OF_DAILY_REPEAT);
			RomWrite(bCopiedScheduleData, wAdrs, BYTENUM_OF_DAILY_REPEAT);
		}
	}
#endif	
#ifdef 	DDL_CFG_FP
	// 전체 사용자 지문
	else if((SlotNumber[0] == 0x2F) && (SlotNumber[1] == 0xFF)) 
	{
		for(bCnt = 0; bCnt < SUPPORTED_FINGERPRINT_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_DAILY_REPEAT_FINGERPRINT+ ((WORD)bCnt*BYTENUM_OF_DAILY_REPEAT);
			RomWrite(bCopiedScheduleData, wAdrs, BYTENUM_OF_DAILY_REPEAT);
		}
	}
#endif
	else
	{
		bSlotNum = SlotNumber[1];
		CredentialType = (SlotNumber[0] >> 4) & 0x0F;
		
		if(SlotNumberCheck(CredentialType, bSlotNum) == 0)	return;
		bSlotNum--;

		wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_DAILY_REPEATING);
		wAdrs += (WORD)bSlotNum*BYTENUM_OF_DAILY_REPEAT;

		RomWrite(bCopiedScheduleData, wAdrs, BYTENUM_OF_DAILY_REPEAT);
	}

	if(gfPackTypeiRevo == 0)
	{
#ifdef DDL_CFG_YALE_ACCESS_SUPPORT
		if((gfPackTypeCBABle || gfInnerPackTypeCBABle) && GetPackCBARegisterSet())
		{
			uint8_t tempSlotnumber = (SlotNumber[1] == 0xFF) ? SlotNumber[1] : (SlotNumber[1]+DDL_CFG_YALE_ACCESS_SUPPORT)-1;
			
			PackTx_MakeAlarmPacket(AL_DAILY_SCH_SET, SlotNumber[0], tempSlotnumber);
		}
		else
		{
			PackTx_MakeAlarmPacket(AL_DAILY_SCH_SET, SlotNumber[0], SlotNumber[1]);
		}
#else		
		PackTx_MakeAlarmPacket(AL_DAILY_SCH_SET, SlotNumber[0], SlotNumber[1]);
#endif 
		
		PackTxWaitTime_EnQueue(5); // pack 에서 응답이 빠르다고 해서  보통 위 함수들이 등록 삭제 시에 불리는데 , 다른 event 와 겹치게 발생함 
		InnerPackTxWaitTime_EnQueue(5); 
	}
}




void SetYearDaySchdule(BYTE *SlotNumber, BYTE *bScheduleData)
{
	BYTE bCnt;
	WORD wAdrs;
	BYTE bCopiedScheduleData[BYTENUM_OF_YEARDAY];
	BYTE CredentialType;	
	BYTE bSlotNum;

	bCopiedScheduleData[0] = bScheduleData[0];

	for(bCnt = 0; bCnt < (BYTENUM_OF_YEARDAY-1); bCnt++)
	{
		bCopiedScheduleData[1+bCnt] = bScheduleData[4+bCnt];
	}
	
	// 전체 사용자 비밀번호
	if(((SlotNumber[0] == 0xFF) && (SlotNumber[1] == 0xFF)) 
		|| ((SlotNumber[0] == 0x0F) && (SlotNumber[1] == 0xFF)))
	{
		for(bCnt = 0; bCnt < SUPPORTED_USERCODE_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_DAILY_REPEAT_PIN + ((WORD)bCnt*BYTENUM_OF_YEARDAY);
			RomWrite(bCopiedScheduleData, wAdrs, BYTENUM_OF_YEARDAY);
		}
	}
#ifdef 	DDL_CFG_RFID
	// 전체 사용자 카드
	else if((SlotNumber[0] == 0x1F) && (SlotNumber[1] == 0xFF)) 
	{
		for(bCnt = 0; bCnt < SUPPORTED_USERCARD_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_YEAR_DAY_CARD + ((WORD)bCnt*BYTENUM_OF_YEARDAY);
			RomWrite(bCopiedScheduleData, wAdrs, BYTENUM_OF_YEARDAY);
		}
	}
#endif	
#ifdef 	DDL_CFG_FP
	// 전체 사용자 지문
	else if((SlotNumber[0] == 0x2F) && (SlotNumber[1] == 0xFF)) 
	{
		for(bCnt = 0; bCnt < SUPPORTED_FINGERPRINT_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_YEAR_DAY_FINGERPRINT+ ((WORD)bCnt*BYTENUM_OF_YEARDAY);
			RomWrite(bCopiedScheduleData, wAdrs, BYTENUM_OF_YEARDAY);
		}
	}
#endif
	else
	{
		bSlotNum = SlotNumber[1];
		CredentialType = (SlotNumber[0] >> 4) & 0x0F;
		
		if(SlotNumberCheck(CredentialType, bSlotNum) == 0)	return;
		bSlotNum--;
		
		wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_YEAR_DAY);
		wAdrs += (WORD)bSlotNum*BYTENUM_OF_YEARDAY;

		RomWrite(bCopiedScheduleData, wAdrs, BYTENUM_OF_YEARDAY);
	}
	
	if(gfPackTypeiRevo == 0)
	{
#ifdef DDL_CFG_YALE_ACCESS_SUPPORT
		if((gfPackTypeCBABle || gfInnerPackTypeCBABle) && GetPackCBARegisterSet())
		{
			uint8_t tempSlotnumber = (SlotNumber[1] == 0xFF) ? SlotNumber[1] : (SlotNumber[1]+DDL_CFG_YALE_ACCESS_SUPPORT)-1;
			PackTx_MakeAlarmPacket(AL_YEARDAY_SCH_SET, SlotNumber[0],tempSlotnumber);
		}
		else
		{
			PackTx_MakeAlarmPacket(AL_YEARDAY_SCH_SET, SlotNumber[0], SlotNumber[1]);
		}
#else
		PackTx_MakeAlarmPacket(AL_YEARDAY_SCH_SET, SlotNumber[0], SlotNumber[1]);
#endif 
		PackTxWaitTime_EnQueue(5); // pack 에서 응답이 빠르다고 해서  보통 위 함수들이 등록 삭제 시에 불리는데 , 다른 event 와 겹치게 발생함 
		InnerPackTxWaitTime_EnQueue(5); 
	}
}



void SetDailyRepeatingEnable(BYTE *SlotNumber, BYTE *bEnableData)
{
	BYTE bCnt;
	WORD wAdrs;
	BYTE CredentialType;	
	BYTE bSlotNum;

	// 전체 사용자 비밀번호
	if(((SlotNumber[0] == 0xFF) && (SlotNumber[1] == 0xFF)) 
		|| ((SlotNumber[0] == 0x0F) && (SlotNumber[1] == 0xFF)))
	{
		for(bCnt = 0; bCnt < SUPPORTED_USERCODE_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_DAILY_REPEAT_PIN + ((WORD)bCnt*BYTENUM_OF_DAILY_REPEAT);
			RomWrite(bEnableData, wAdrs, 1);
		}
	}
#ifdef 	DDL_CFG_RFID
	// 전체 사용자 카드
	else if((SlotNumber[0] == 0x1F) && (SlotNumber[1] == 0xFF)) 
	{
		for(bCnt = 0; bCnt < SUPPORTED_USERCARD_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_DAILY_REPEAT_CARD + ((WORD)bCnt*BYTENUM_OF_DAILY_REPEAT);
			RomWrite(bEnableData, wAdrs, 1);
		}
	}
#endif	
#ifdef 	DDL_CFG_FP
	// 전체 사용자 지문
	else if((SlotNumber[0] == 0x2F) && (SlotNumber[1] == 0xFF)) 
	{
		for(bCnt = 0; bCnt < SUPPORTED_FINGERPRINT_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_DAILY_REPEAT_FINGERPRINT+ ((WORD)bCnt*BYTENUM_OF_DAILY_REPEAT);
			RomWrite(bEnableData, wAdrs, 1);
		}
	}
#endif
	else
	{
		bSlotNum = SlotNumber[1];
		CredentialType = (SlotNumber[0] >> 4) & 0x0F;
		
		if(SlotNumberCheck(CredentialType, bSlotNum) == 0)	return;
		bSlotNum--;

		wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_DAILY_REPEATING);
		wAdrs += (WORD)bSlotNum*BYTENUM_OF_DAILY_REPEAT;
		
		RomWrite(bEnableData, wAdrs, 1);
	}

#ifdef DDL_CFG_YALE_ACCESS_SUPPORT
	if((gfPackTypeCBABle || gfInnerPackTypeCBABle) && GetPackCBARegisterSet())
	{
		uint8_t tempSlotnumber = (SlotNumber[1] == 0xFF) ? SlotNumber[1] : (SlotNumber[1]+DDL_CFG_YALE_ACCESS_SUPPORT)-1;
		PackTx_MakeAlarmPacket(AL_DAILY_SCH_ENABLE_DISABLE, SlotNumber[0],tempSlotnumber);
	}
	else
	{
		PackTx_MakeAlarmPacket(AL_DAILY_SCH_ENABLE_DISABLE, SlotNumber[0], SlotNumber[1]);
	}
#else	
	PackTx_MakeAlarmPacket(AL_DAILY_SCH_ENABLE_DISABLE, SlotNumber[0], SlotNumber[1]);
#endif 
	PackTxWaitTime_EnQueue(5); // pack 에서 응답이 빠르다고 해서  보통 위 함수들이 등록 삭제 시에 불리는데 , 다른 event 와 겹치게 발생함 
	InnerPackTxWaitTime_EnQueue(5); 
}




void SetYearDayEnable(BYTE *SlotNumber, BYTE *bEnableData)
{
	BYTE bCnt;
	WORD wAdrs;
	BYTE CredentialType;	
	BYTE bSlotNum;

	// 전체 사용자 비밀번호
	if(((SlotNumber[0] == 0xFF) && (SlotNumber[1] == 0xFF)) 
		|| ((SlotNumber[0] == 0x0F) && (SlotNumber[1] == 0xFF)))
	{
		for(bCnt = 0; bCnt < SUPPORTED_USERCODE_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_YEAR_DAY_PIN + ((WORD)bCnt*BYTENUM_OF_YEARDAY);
			RomWrite(bEnableData, wAdrs, 1);
		}
	}
#ifdef 	DDL_CFG_RFID
	// 전체 사용자 카드
	else if((SlotNumber[0] == 0x1F) && (SlotNumber[1] == 0xFF)) 
	{
		for(bCnt = 0; bCnt < SUPPORTED_USERCARD_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_YEAR_DAY_CARD + ((WORD)bCnt*BYTENUM_OF_YEARDAY);
			RomWrite(bEnableData, wAdrs, 1);
		}
	}
#endif	
#ifdef 	DDL_CFG_FP
	// 전체 사용자 지문
	else if((SlotNumber[0] == 0x2F) && (SlotNumber[1] == 0xFF)) 
	{
		for(bCnt = 0; bCnt < SUPPORTED_FINGERPRINT_NUMBER; bCnt++)
		{
#ifdef	DDL_CFG_WATCHDOG
			RefreshIwdg();
#endif
			wAdrs = SCHEDULE_YEAR_DAY_FINGERPRINT+ ((WORD)bCnt*BYTENUM_OF_YEARDAY);
			RomWrite(bEnableData, wAdrs, 1);
		}
	}
#endif
	else
	{
		bSlotNum = SlotNumber[1];
		CredentialType = (SlotNumber[0] >> 4) & 0x0F;
		
		if(SlotNumberCheck(CredentialType, bSlotNum) == 0)	return;
		bSlotNum--;

		wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_YEAR_DAY);
		wAdrs += (WORD)bSlotNum*BYTENUM_OF_YEARDAY;

		RomWrite(bEnableData, wAdrs, 1);
	}

#ifdef DDL_CFG_YALE_ACCESS_SUPPORT
	if((gfPackTypeCBABle || gfInnerPackTypeCBABle) && GetPackCBARegisterSet())
	{
		uint8_t tempSlotnumber = (SlotNumber[1] == 0xFF) ? SlotNumber[1] : (SlotNumber[1]+DDL_CFG_YALE_ACCESS_SUPPORT)-1;
		PackTx_MakeAlarmPacket(AL_YEARDAY_SCH_ENABLE_DISABLE, SlotNumber[0],tempSlotnumber);
	}
	else
	{
		PackTx_MakeAlarmPacket(AL_YEARDAY_SCH_ENABLE_DISABLE, SlotNumber[0], SlotNumber[1]);
	}
#else
	PackTx_MakeAlarmPacket(AL_YEARDAY_SCH_ENABLE_DISABLE, SlotNumber[0], SlotNumber[1]);
#endif 
	PackTxWaitTime_EnQueue(5); // pack 에서 응답이 빠르다고 해서  보통 위 함수들이 등록 삭제 시에 불리는데 , 다른 event 와 겹치게 발생함 
	InnerPackTxWaitTime_EnQueue(5); 
}




BYTE GetDailyRepeatingSchdule(BYTE *SlotNumber, BYTE *bScheduleData)
{
	BYTE bCnt;
	WORD wAdrs;
	BYTE bCopiedScheduleData[BYTENUM_OF_DAILY_REPEAT];
	BYTE CredentialType;	
	BYTE bSlotNum;

	bSlotNum = SlotNumber[1];
	CredentialType = (SlotNumber[0] >> 4) & 0x0F;

	if(SlotNumberCheck(CredentialType, bSlotNum) == 0)	return 0;
	bSlotNum--;

	wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_DAILY_REPEATING);
	wAdrs += (WORD)bSlotNum*BYTENUM_OF_DAILY_REPEAT;

	RomRead(bCopiedScheduleData, wAdrs, BYTENUM_OF_DAILY_REPEAT);

	bScheduleData[0] = bCopiedScheduleData[0];
	for(bCnt = 0; bCnt < (BYTENUM_OF_DAILY_REPEAT-1); bCnt++)
	{
		bScheduleData[4+bCnt] = bCopiedScheduleData[1+bCnt];
	}

	return 1;
}




BYTE GetYearDaySchdule(BYTE *SlotNumber, BYTE *bScheduleData)
{
	BYTE bCnt;
	WORD wAdrs;
	BYTE bCopiedScheduleData[BYTENUM_OF_YEARDAY];
	BYTE CredentialType;	
	BYTE bSlotNum;

	bSlotNum = SlotNumber[1];
	CredentialType = (SlotNumber[0] >> 4) & 0x0F;

	if(SlotNumberCheck(CredentialType, bSlotNum) == 0)	return 0;
	bSlotNum--;

	wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_YEAR_DAY);
	wAdrs += (WORD)bSlotNum*BYTENUM_OF_YEARDAY;

	RomRead(bCopiedScheduleData, wAdrs, BYTENUM_OF_YEARDAY);

	bScheduleData[0] = bCopiedScheduleData[0];

	for(bCnt = 0; bCnt < (BYTENUM_OF_YEARDAY-1); bCnt++)
	{
		bScheduleData[4+bCnt] = bCopiedScheduleData[1+bCnt];
	}

	return 1;
}



BYTE ScheduleEnableCheck_Ble30(BYTE CredentialType, BYTE bSlotNum)
{
	WORD wAdrs;
	BYTE bCopiedScheduleData;

	wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_YEAR_DAY);
	wAdrs += (WORD)bSlotNum*BYTENUM_OF_YEARDAY;

	RomRead(&bCopiedScheduleData, wAdrs, 1);
	if(bCopiedScheduleData == 0xFF)		return 1;

	wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_DAILY_REPEATING);
	wAdrs += (WORD)bSlotNum*BYTENUM_OF_DAILY_REPEAT;

	RomRead(&bCopiedScheduleData, wAdrs, 1);
	if(bCopiedScheduleData == 0xFF)		return 1;

	return 0;
}



BYTE ScheduleYearDayVerify(BYTE CredentialType, BYTE *pCurrentTimeBuf, BYTE bSlotNum)
{
	WORD wAdrs;
	BYTE bCopiedScheduleData[BYTENUM_OF_YEARDAY];
	uint64_t dwStartData;
	uint64_t dwEndData;
	uint64_t dwCurrentData;
	BYTE bDataBuf[8];
	
	wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_YEAR_DAY);
	wAdrs += (WORD)bSlotNum*BYTENUM_OF_YEARDAY;

	RomRead(bCopiedScheduleData, wAdrs, BYTENUM_OF_YEARDAY);

	if(DataCompare(bCopiedScheduleData, 0xFF, BYTENUM_OF_YEARDAY) == _DATA_IDENTIFIED)
	{
		return 1;
	}

	if(DataCompare(bCopiedScheduleData, 0x00, BYTENUM_OF_YEARDAY) == _DATA_IDENTIFIED)
	{
		return 1;
	}

	if(bCopiedScheduleData[0] != 0xFF)
	{
		return 1;
	}
		
	bDataBuf[0] = 0x00;
	bDataBuf[1] = 0x00;

	memcpy(&bDataBuf[2], &bCopiedScheduleData[1], 6);
	BYTEToQWORD(&dwStartData, bDataBuf, 0);

	memcpy(&bDataBuf[2], &bCopiedScheduleData[7], 6);
	BYTEToQWORD(&dwEndData, bDataBuf, 0);

	memcpy(&bDataBuf[2], &pCurrentTimeBuf[1], 6);
	BYTEToQWORD(&dwCurrentData, bDataBuf, 0);

	if((dwCurrentData >= dwStartData) && (dwCurrentData <= dwEndData))
	{
		return 1;
	}

	return 0;
}





BYTE ScheduleDailyRepeatVerify(BYTE CredentialType, BYTE *pCurrentTimeBuf, BYTE bSlotNum)
{
	WORD wAdrs;
	BYTE bCopiedScheduleData[BYTENUM_OF_DAILY_REPEAT];
	BYTE bCnt;
	WORD StartTime;
	WORD EndTime;
	WORD CurrentTime;
	BYTE DayOfWeek;

	wAdrs = GetStartAddressByCredentialType(CredentialType, MODE_DAILY_REPEATING);
	wAdrs += (WORD)bSlotNum*BYTENUM_OF_DAILY_REPEAT;

	RomRead(bCopiedScheduleData, wAdrs, BYTENUM_OF_DAILY_REPEAT);

	if(DataCompare(bCopiedScheduleData, 0xFF, BYTENUM_OF_DAILY_REPEAT) == _DATA_IDENTIFIED)
	{  							
		// 해당 Schedule Data 없음으로 Schedule 확인 통과		
		return 1;				
	}

	if(DataCompare(bCopiedScheduleData, 0x00, BYTENUM_OF_DAILY_REPEAT) == _DATA_IDENTIFIED)
	{  							
		// 해당 Schedule Data 없음으로 Schedule 확인 통과		
		return 1;				
	}

	if(bCopiedScheduleData[0] != 0xFF)
	{
		return 1;
	}

	// 시작 시간
	StartTime = (WORD)BCDToDEC(bCopiedScheduleData[2]) * 60;
	StartTime += (WORD)BCDToDEC(bCopiedScheduleData[3]);

	// 종료 시간 (= 시작 시간+사용 시간) 
	EndTime = StartTime; 
	EndTime += (WORD)BCDToDEC(bCopiedScheduleData[4]) * 60;
	EndTime += (WORD)BCDToDEC(bCopiedScheduleData[5]);

	CurrentTime = (WORD)BCDToDEC(pCurrentTimeBuf[5]) * 60;
	CurrentTime += (WORD)BCDToDEC(pCurrentTimeBuf[6]);

	DayOfWeek = bCopiedScheduleData[1];

	//60 * 24 = 1440 ; 1일을 나타내는 시간.
	//EndTime >= 1440 이면 하루를 지나는 것으로 요일 비교에서 다음 날도 체크가 들어 가야 함.
	// 그리고 다음날 종료 시간은 EndTime - 1440;  	

	// 하루가 지나는 지 확인  (1440 = 24*60)
	if(EndTime >= 1440)
	{	
		// 종료 시간이 다음 날임 
		if(CurrentTime < StartTime)
		{
			EndTime -= 1440;	
			if(CurrentTime > EndTime)
			{
				return 0;
			}

			// 하루가 지나서도 동작해야 하는 경우면, 다음날의 요일도 임의 설정하여 처리되도록 함
			for(bCnt = 0; bCnt < 7; bCnt++)
			{
				if((bCopiedScheduleData[1] & ShiftDayOfWeekCheck[bCnt]) == ShiftDayOfWeekCheck[bCnt])
				{
					DayOfWeek |= ShiftDayOfWeekCheck[(bCnt+1)];
				}
			}					
		}
	}
	else
	{	
		// 종료 시간이 시작 시간과 동일한 날임
		// '시작 시간 < 현재 시간 <= 종료 시간' 일 조건에서만 Schedule 정상 처리됨
		if((CurrentTime < StartTime) || (CurrentTime > EndTime))
		{
			return 0;
		}
	}

	// 요일 비교 
	if((DayOfWeek & pCurrentTimeBuf[0]) == 0)
	{
		// 실행 안함
		return 0;  
	}

	// 시간 안에 존재 하므로 실행 
	return 1; 
}



BYTE ScheduleVerify_Ble30(BYTE CredentialType, BYTE *pCurrentTimeBuf, BYTE bSlotNum)
{
	BYTE bTmp;
	
	if(bSlotNum == 0)	return 0;
	bSlotNum--;

	bTmp = ScheduleYearDayVerify(CredentialType, pCurrentTimeBuf, bSlotNum);
	if(bTmp == 0)	return 0;

	bTmp = ScheduleDailyRepeatVerify(CredentialType, pCurrentTimeBuf, bSlotNum);
	if(bTmp == 0)	return 0;

	return 1;
}


void ScheduleDelete_Ble30(BYTE CredentialType, BYTE bSlotNum)
{
	BYTE bDataBuf[BYTENUM_OF_YEARDAY+4];
	BYTE SlotNumber[2];

	memset(bDataBuf, 0x00, (BYTENUM_OF_YEARDAY+4));

	if(CredentialType == CREDENTIALTYPE_CARD)
	{
		SlotNumber[0] = 0x10;
	}
	else if(CredentialType == CREDENTIALTYPE_FINGERPRINT)
	{
		SlotNumber[0] = 0x20;
	}
	else
	{
		SlotNumber[0] = 0x00;
	}
		
	SlotNumber[1] = bSlotNum;

	SetYearDaySchdule(SlotNumber, bDataBuf);
	SetDailyRepeatingSchdule(SlotNumber, bDataBuf);
}



void ScheduleDeleteOfAllUserCode(void)
{
	RomWriteWithSameData(0x00, SCHEDULE_YEAR_DAY_PIN, (BYTENUM_OF_YEARDAY*SUPPORTED_USERCODE_NUMBER));
	RomWriteWithSameData(0x00, SCHEDULE_DAILY_REPEAT_PIN, (BYTENUM_OF_DAILY_REPEAT*SUPPORTED_USERCODE_NUMBER));
}


void ScheduleDeleteOfAllUserCard(void)
{
#ifdef 	DDL_CFG_RFID
	RomWriteWithSameData(0x00, SCHEDULE_YEAR_DAY_CARD, (BYTENUM_OF_YEARDAY*SUPPORTED_USERCARD_NUMBER));
	RomWriteWithSameData(0x00, SCHEDULE_DAILY_REPEAT_CARD, (BYTENUM_OF_DAILY_REPEAT*SUPPORTED_USERCARD_NUMBER));
#endif
}

void ScheduleDeleteOfAllUserFingerprint(void)
{
#ifdef 	DDL_CFG_FP
	RomWriteWithSameData(0x00, SCHEDULE_YEAR_DAY_FINGERPRINT, (BYTENUM_OF_YEARDAY*SUPPORTED_FINGERPRINT_NUMBER));
	RomWriteWithSameData(0x00, SCHEDULE_DAILY_REPEAT_FINGERPRINT, (BYTENUM_OF_DAILY_REPEAT*SUPPORTED_FINGERPRINT_NUMBER));
#endif
}




#endif


