//------------------------------------------------------------------------------
/** 	@file		DebugFunctions_T1.c
	@version 0.1.00
	@date	2016.04.26
	@brief	Dubug Functions using UART
	@remark 통신팩 단자를 이용한 Debug 함수
	@see 	KeyInput.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.26		by Jay
*/
//------------------------------------------------------------------------------

#include "Main.h"

#include "stm32l1xx_hal_usart.h"


BYTE DebugDataBuf[20];

uint8_t gbPrintfOn =0x0;;

void DebugTxDataSend(uint8_t *pTxDataBuf, uint8_t TxDataByteNum)
{
	UartSendCommPack(pTxDataBuf, TxDataByteNum);
}

void	SetupUartforPrintf(UART_HandleTypeDef* uart)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	HAL_UART_DeInit(uart);

	GPIO_InitStruct.Pin = COM_DDL_TX_Pin|COM_DDL_RX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;				
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART1;			
	HAL_GPIO_Init(COM_DDL_TX_GPIO_Port, &GPIO_InitStruct);

	uart->Instance = MCU_COMMPACK_UART;
	uart->Init.BaudRate = 115200;
	uart->Init.WordLength = UART_WORDLENGTH_8B;
	uart->Init.StopBits = UART_STOPBITS_1;
	uart->Init.Parity = UART_PARITY_NONE;
	uart->Init.Mode = UART_MODE_TX_RX;
	uart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
	uart->Init.OverSampling = UART_OVERSAMPLING_16;

	HAL_UART_Init( uart );

	gbPrintfOn = 0x01;
}

void InitPrintfService( void (*TxCallback)(UART_HandleTypeDef *huart), void (*RxCallback)(UART_HandleTypeDef *huart),void (*InitProcess)(void))
{
	RegisterCommPack_INT_Callback(TxCallback,RxCallback );
	CommPackRxInterruptRequest();
	if(InitProcess != NULL)	
	{
		InitProcess();
	}
}

int putchar(int ch)
{
	BYTE data = ch;
	while(__HAL_USART_GET_FLAG(gh_mcu_uart_com,USART_FLAG_TXE) == RESET);
	__HAL_USART_CLEAR_FLAG(gh_mcu_uart_com,USART_FLAG_TXE);
	HAL_UART_Transmit( gh_mcu_uart_com,&data,1,5);
	return ch;
}
