/******************************************************************************
*	CubeMX에서 만들어지는 uart 관련 lib를 사용하기 위한 lapper 들
*
*	UART : UART1, UART2, UART3
*
*******************************************************************************/

#define		_MX_UART_LAPPER_C_

#include "stm32l1xx_hal.h"

#include "MX_uart_lapper.h"



/******************************************************************************
*	UART lapper
*
*	CubeMX에서 UART interrupt 발생시 호출되는 callback function은 복수의 UART를 사용하더라도
*	하나의 callback function이 호출된다. 구분되는 것은 interrupt의 종류로만 구분된다.
*	따라서 UART별로 callback을 제공하기 위해 한번 더 lapper를 등록해서 사용한다.
*
*******************************************************************************/


// UART별 callback function pointer array (UART1~UART3)
void	(*UART_TxCpltCallback[3])(UART_HandleTypeDef *huart) = { NULL, NULL, NULL };
void	(*UART_RxCpltCallback[3])(UART_HandleTypeDef *huart) = { NULL, NULL, NULL };



// Tx complete callback 등록용
uint32_t	register_uart_txcplt_callback( UART_HandleTypeDef *huart, void (*uart_txcplt_callback)(UART_HandleTypeDef *huart) )
{
	uint32_t		ret=0;

	if ( huart == NULL )
		return	ret;
	
	if ( huart->Instance == USART1 ) {
		UART_TxCpltCallback[0] = uart_txcplt_callback;
		ret = 1;
	}
	else if ( huart->Instance == USART2 ) {
		UART_TxCpltCallback[1] = uart_txcplt_callback;
		ret = 1;
	}
	else if ( huart->Instance == USART3 ) {
		UART_TxCpltCallback[2] = uart_txcplt_callback;
		ret = 1;
	}

	return	ret;
}

// Rx complete callback 등록용
uint32_t	register_uart_rxcplt_callback( UART_HandleTypeDef *huart, void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) )
{
	uint32_t		ret=0;

	if ( huart == NULL )
		return	ret;
	
	if ( huart->Instance == USART1 ) {
		UART_RxCpltCallback[0] = uart_rxcplt_callback;
		ret = 1;
	}
	else if ( huart->Instance == USART2 ) {
		UART_RxCpltCallback[1] = uart_rxcplt_callback;
		ret = 1;
	}
	else if ( huart->Instance == USART3 ) {
		UART_RxCpltCallback[2] = uart_rxcplt_callback;
		ret = 1;
	}

	return	ret;
}


uint32_t	Err_UART=0;
/******************************************************************************
*	UART error가 발생하면 호출되는 callback 함수
*******************************************************************************/
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
#if 0	
	uint32_t	err_code;

	err_code = HAL_UART_GetError( huart );
#endif 	
	if ( huart->Instance == USART1 ) {
		Err_UART = 1;
	}
	else if ( huart->Instance == USART2 ) {
		Err_UART = 2;
	}
	else if ( huart->Instance == USART3 ) {
		Err_UART = 3;
	}
}


/******************************************************************************
*	CubeMX에 의해 호출되는 Tx Complete callback 함수
*******************************************************************************/
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if ( (huart->Instance == USART1) && (UART_TxCpltCallback[0] != NULL) )
		(*UART_TxCpltCallback[0])( huart );
	else if ( (huart->Instance == USART2) && (UART_TxCpltCallback[1] != NULL) )
		(*UART_TxCpltCallback[1])( huart );
	else if ( (huart->Instance == USART3) && (UART_TxCpltCallback[2] != NULL) )
		(*UART_TxCpltCallback[2])( huart );
}

/******************************************************************************
*	CubeMX에 의해 호출되는 Rx Complete callback 함수
*******************************************************************************/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if ( (huart->Instance == USART1) && (UART_RxCpltCallback[0] != NULL) )
		(*UART_RxCpltCallback[0])( huart );
	else if ( (huart->Instance == USART2) && (UART_RxCpltCallback[1] != NULL) )
		(*UART_RxCpltCallback[1])( huart );
	else if ( (huart->Instance == USART3) && (UART_RxCpltCallback[2] != NULL) )
		(*UART_RxCpltCallback[2])( huart );
}




