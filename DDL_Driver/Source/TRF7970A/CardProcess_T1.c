//------------------------------------------------------------------------------
/** 	@file		CardProcess_T1.c
	@version 0.1.02
	@date	2016.05.26
	@brief	Card UID Read Functions 
	@remark	 Mifare Classic Card의 4~7 byte UID를 읽어 처리
	@see	ISO14443AFunctions_T1.c
	@see	TRF7970Functions_T1.c
	@see	ParallelFunctions_T1.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.08		by Jay
			- 4byte UID 카드 및 7byte UID 카드를 모두 읽어서 해당 Data를 출력
			- 외부에서 Card Readig Start and Stop 제어가 가능
			- Card Reading Start가 설정되면, Stop 설정 시 까지 반복하여 카드 읽기 수행
			- 최대 5장의 카드가 동시에 입력되어도 모두 읽기 가능 (Anticollision),
			       현재는 하드웨어 영향인지 아래의 경우 제대로 안됨
			       -> 3장 이상의 카드가 동식에 입력되 경우 제대로 된 Data 수신 불가능
			       -> 4byte UID 카드와 7byte UID 카드가 동시에 입력될 경우에는 제대로 된 Data 수신 불가능
			- 입력된 모든 카드의 UID를 읽었을 경우 카드가 떨어질 때까지 추가 Reading Process는 중지

		V0.1.01 2016.04.22		by Jay
			- CardDetectionProcess 함수에서 카드가 감지될 경우 ModeGotoCardVerify 함수 호출하도록 추가
			=> 이렇게 하지 않을 경우 CardProcess 루틴의 단계가 삭제되지 않고 계속 구동되는 문제 있음

		V0.1.02 2016.05.26		by Jay, Moonsungwoo(commit)
			- 중국 상해 교통카드 읽기 오류 관련 수정
			- CardCheckCountReset(); 추가
					<증상>
					1.중국 상해 교통카드를 접촉할 경우 일정 시간이 지난 뒤에 에러 처리되고, 카드를 떼면 정상 동작함 
					 (등록된 카드는 열림 처리, 등록되지 않은 카드는 에러 처리)
					<원인>
					1.문제의 카드가 ISO14443A 명령어인 HLTA(Halt 명령어)에 대해 “not acknowledge” 반응을 보임

*/
//------------------------------------------------------------------------------

#include "Main.h"


BYTE gCardMode = 0;													/**< 카드 읽기 모드  */
BYTE gCardProcessStep = 0;											/**< 카드 읽기 모드 내에서 처리를 위한 단계  */

BYTE gCardReadStatus = 0;											/**< 카드 읽기 처리 결과  */

BYTE gCardOneUid[MAX_CARD_UID_SIZE];								/**< 읽어들인 1장의 카드 UID 저장  */
BYTE gCardAllUidBuf[MAX_CARD_ANTI_SIZE][MAX_CARD_UID_SIZE];			/**< 읽어들인 카드 UID를 모두 저장  */
BYTE gCardUidLength = 0;											/**< 읽어들인 카드의 UID 길이 */
BYTE gCardInputNumber = 0;											/**< 입력된 카드 중 읽어들인 카드의 개수를 저장하는 버퍼 */

BYTE gCardPrcsTimer2ms = 0;											/**< 카드 읽기 처리를 위한 Timer, 2ms Tick, 0~500ms 설정 동작  */
BYTE gCardAwayTimer2ms = 0;											/**< 입력된 카드 떨어짐 감지를 위한 Timer, 2ms Tick, 50ms 설정 동작  */

BYTE gCardCheckLoopCnt = 0;											/**< 입력된 카드의 UID를 모두 읽었는지 확인하기 위해 반복 시도하는 회수 
																			또는, 카드가 떨어지는 걸 check 하기 위해 반복 시도하는 회수 */

#ifdef	FELICA_CARD_SUPPORTED
BYTE gCardProcessedType = 0;										/**< 입력된 카드의 종류 저장, 이후 처리 방법을 달리 하기 위한 변수  */
#endif											
																			
//------------------------------------------------------------------------------
/** 	@brief	Card Process Time Counter
	@param	None
	@return 	None
	@remark 카드 읽기 처리 중에 사용하는 Timer, Timer Tick에서 호출하여 사용
	@remark 2ms Tick, 0~500ms 설정 동작
*/
//------------------------------------------------------------------------------
void CardProcessTimeCounter(void)
{
	if(gCardPrcsTimer2ms)		--gCardPrcsTimer2ms;
}


//------------------------------------------------------------------------------
/** 	@brief	Card Process Time Clear
	@param	None
	@return 	None
	@remark 카드 폴링을 위해 Sleep Mode에서 Wake Up 될 경우 바로 카드를 읽기 위해 처리 
*/
//------------------------------------------------------------------------------
void CardProcessTimeClear(void)
{
	gCardPrcsTimer2ms = 0;;
}



//------------------------------------------------------------------------------
/** 	@brief	Card Off Check Time Counter
	@param	None
	@return 	None
	@remark 읽어 들인 카드의 떨어짐 감지에 사용하는 Timer, Timer Tick에서 호출하여 사용
	@remark 2ms Tick, 50ms 설정 동작
*/
//------------------------------------------------------------------------------
void CardAwayCheckTimeCounter(void)
{
	if(gCardAwayTimer2ms)		--gCardAwayTimer2ms;
}


//------------------------------------------------------------------------------
/** 	@brief	Current Card Process State Check
	@param 	None
	@return 	[gCardProcessStep] : 0이 아닐 경우 카드 읽기 처리 완료
							    0일 경우 카드 읽기 처리 중
	@remark 현재 카드 읽기 동작이 완료되었는지 확인하는 함수
*/
//------------------------------------------------------------------------------
BYTE GetCardMode(void)
{
	return (gCardMode);	
}



//------------------------------------------------------------------------------
/** 	@brief	Card Mode Clear
	@param 	None
	@return 	None
	@remark 카드 읽기 동작 종료
*/
//------------------------------------------------------------------------------
void CardModeClear(void)
{
	RfidPowerOff();

	gCardMode = 0;
	gCardProcessStep = 0;
}



//------------------------------------------------------------------------------
/** 	@brief	Card UID buffer all clear
	@param 	None
	@return 	None
	@remark 읽어 들인 카드의 UID가 저장되는 buffer를 모두 Clear
	@remark 카드 읽기 시작할 때 수행 
*/
//------------------------------------------------------------------------------
void CardAllUidBufferClear(void)
{
	gCardInputNumber = 0;

	memset(gCardOneUid, 0xFF, MAX_CARD_UID_SIZE);
	memset(gCardAllUidBuf, 0xFF, sizeof(gCardAllUidBuf));


#ifdef	FELICA_CARD_SUPPORTED
	SetInputCardType(CARD_TYPE_NONE);
#endif
}



//------------------------------------------------------------------------------
/** 	@brief	Temporary save Card UID 
	@param 	None
	@return 	[UID_NONE] : UID가 모두 00h이거나 FFh일 경우 
	@return 	[UID_AVAILABLE] : UID가 사용 가능할 경우
	@return 	[UID_DUPICATED] : UID가 이미 입력된 카드의 것과 동일할 경우 
	@remark 입력된 카드의 UID의 임시 저장을 위해 UID가 저장 가능한 것인지 비교
*/
//------------------------------------------------------------------------------
BYTE TempSaveCompletedUID(void)
{
	BYTE bCnt;
	
	if( DataCompare(gCardOneUid, 0xFF, gCardUidLength) == _DATA_IDENTIFIED	)		return (UID_NONE);		// UID가 모두 FF인지 검사
	if( DataCompare(gCardOneUid, 0x00, gCardUidLength) == _DATA_IDENTIFIED	)		return (UID_NONE);		// UID가 모두 00인지 검사

	// 1개 이상 UID를 읽어왔다면  같은 UID를 저장했었는지 검사
	for(bCnt = 0; bCnt < gCardInputNumber; bCnt++)
	{
		if( (BYTE)memcmp(gCardOneUid, gCardAllUidBuf[bCnt], MAX_CARD_UID_SIZE) == 0 )	return (UID_DUPICATED); 		// 이미 같은 UID가 저장되어 있다면 리턴
	}

	memcpy(gCardAllUidBuf[gCardInputNumber], gCardOneUid, MAX_CARD_UID_SIZE);
	gCardInputNumber++;

	return (UID_AVAILABLE);
}



//------------------------------------------------------------------------------
/** 	@brief	Card Read Start
	@param 	None
	@return 	None
	@remark 카드 읽기 시작
	@remark CardModePowerOnOff 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoReadStart(void)
{
	gCardProcessStep = 0;	

	gCardMode = CARDMODE_POWER_ONOFF;
	gCardProcessStep = CARDPRCS_RFID_POWERON;
	gCardReadStatus = 0;

	CardAllUidBufferClear();
}


//------------------------------------------------------------------------------
/** 	@brief	Card Read Stop
	@param 	None
	@return 	None
	@remark 카드 읽기 종료
	@remark CardModePowerOnOff 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoReadStop(void)
{
	gCardMode = CARDMODE_POWER_ONOFF;
	gCardProcessStep = CARDPRCS_READ_STOP;
	gCardReadStatus = 0;
}



//------------------------------------------------------------------------------
/** 	@brief	Read IC Off
	@param 	None
	@return 	None
	@remark 카드 출력 Off
	@remark CardModePowerOnOff 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoRfTxOff(void)
{
	gCardMode = CARDMODE_POWER_ONOFF;
	gCardProcessStep = CARDPRCS_RFID_TXOFF;

#ifdef	FELICA_CARD_SUPPORTED
	SetInputCardType(CARD_TYPE_NONE);
#endif
}

//------------------------------------------------------------------------------
/** 	@brief	CardCheckCountReset
	@param 	None
	@return 	None
	@remark 
	@remark CardCheckCountReset
*/
//------------------------------------------------------------------------------

void CardCheckCountReset(void)  
{
	gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;
}



//------------------------------------------------------------------------------
/** 	@brief	Card WUPA Start
	@param 	None
	@return 	None
	@remark 카드 UID 읽기 시작
	@remark CardModeSelect 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoReqaStart(void)
{
	gCardMode = CARDMODE_SELECT;
	gCardProcessStep = CARDPRCS_WUPA_SEND;
}



//------------------------------------------------------------------------------
/** 	@brief	Card Select Start
	@param 	None
	@return 	None
	@remark 카드 UID 읽기 시작
	@remark CardModeSelect 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoAnticollisionStart(void)
{
	//gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;   //2017.05.23 moonsungwoo 
	
	AntiColVariInit();
	
	CascadeLevelSet(1);
	
	gCardMode = CARDMODE_SELECT;
	gCardProcessStep = CARDPRCS_ANTICOLLISION_SEND;
}



//------------------------------------------------------------------------------
/** 	@brief	Card Reading Result Process
	@param 	[ProcessStep] : 카드 결과 처리 단계
	@return 	None
	@remark 카드 UID 읽기 결과 처리
	@remark 카드 종류에 따라 ProcessStep으로 다르게 처리 가능
*/
//------------------------------------------------------------------------------
void CardGotoReadResult(BYTE ProcessStep)
{
	gCardMode = CARDMODE_READ_RESULT;
	gCardProcessStep = ProcessStep;
}



//------------------------------------------------------------------------------
/** 	@brief	Card Away from Reader Check
	@param 	None
	@return 	None
	@remark 입력된 카드 떨어짐 확인 시작
	@remark CardModeAwayFromReaderCheck	 함수로 이동
*/
//------------------------------------------------------------------------------
void CardGotoAwayFromReaderCheck(void)
{
	RfidTxOff();

	// 카드가 떨어지는 걸 check 하기 위해 반복 시도하는 회수 설정 
	gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;

	gCardMode = CARDMODE_CARD_AWAY_CHK;
	gCardProcessStep = CARDPRCS_AWAY_CHECK;
}



//------------------------------------------------------------------------------
/** 	@brief	Card Mode - Idle 
	@param 	None
	@return 	None
	@remark 카드 읽기 동작 수행 중 아님
*/
//------------------------------------------------------------------------------
void CardModeIdleState(void)
{
	switch(gCardProcessStep)
	{
		case CARDPRCS_IDLE_STATE:
			break;

		default:
			break;
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Card Mode - Reader IC Power On/Off 
	@param 	None
	@return 	None
	@remark 카드 읽기를 위한 Reader IC의 EN ON/OFF
	@remark 카드 읽기를 위한 Reader IC의 RF TX ON/OFF
	@remark 카드 읽기 종료를 하지 않을 경우 500ms 간격으로 카드 읽기 반복 수행
	@remark 카드 출력만 ON/OFF 반복하면서 카드 읽기 수행
	@remark 카드 읽기 시작 및 종료는 외부에서 수행, 이 때는 EN ON/OFF
*/
//------------------------------------------------------------------------------
void CardModePowerOnOff(void)
{
	switch(gCardProcessStep)
	{
		case CARDPRCS_RFID_POWERON:
			if(gCardPrcsTimer2ms)		break;

			RfidPowerOn();
			gCardPrcsTimer2ms = 0;
			gCardProcessStep = CARDPRCS_RFID_TXON;

		case CARDPRCS_RFID_TXON:
			if(gCardPrcsTimer2ms)		break;

			RfidTxOn();

			CardAllUidBufferClear();

#ifdef	FELICA_CARD_SUPPORTED
			CardCheckCountReset();    //2017.05.23 moonsungwoo 

			// Falica 카드를 지원할 경우 Felica 카드 먼저 확인

			// Maximum start-up tim of card = 20ms
			// It is recommened that the Reader/Writer continues generating the magnetic field for a period of time
			//	of at least 20.ms. thereafter, the Reader/Writer can transmit the Polling command.
			//	Although the Reader/Writer may transmit the Polling command before the 20.4ms period has 
			//	elapsed, it is recommened to retry the transmission of the Polling command, while considering
			//	the cae where no response is returned from the card.
			gCardPrcsTimer2ms = 10;

			CardGotoFelicaPollingStart();			
#else		// FELICA_CARD_SUPPORTED

			// When a PICC is exposed to an unmodulated operating field
			// it shall be able to accept a quest within 5 ms.
			// PCDs should periodically present an unmodulated field of at least
			// 5,1 ms duration. (ISO14443-3)
#ifdef    CARD_READ_OPTIMIZATION
			gCardCheckLoopCnt = 1;

			gCardPrcsTimer2ms = 2;
#else
			CardCheckCountReset();    //2017.05.23 moonsungwoo 

			gCardPrcsTimer2ms = 3;
#endif


			CardGotoReqaStart();
#endif		// FELICA_CARD_SUPPORTED
			break;

		case CARDPRCS_RFID_TXOFF:
			RfidTxOff();
						
			gCardPrcsTimer2ms = 250;
			gCardProcessStep = CARDPRCS_RFID_TXON;
			break;
			
		case CARDPRCS_RFID_POWEROFF:
			if(gCardPrcsTimer2ms)		break;
		
		case CARDPRCS_READ_STOP:
		default:
			CardModeClear();
			break;
	}
}


#ifdef	DEBUG_CARD
enum{
	DEBUG_CARD_REQA = 1,
	DEBUG_CARD_ATQA_OK,
	DEBUG_CARD_ATQA_FAIL,
	DEBUG_CARD_ANTICOLLISION_OK,
	DEBUG_CARD_ANTICOLLISION_FAIL,
	DEBUG_CARD_SELECT_OK,
	DEBUG_CARD_SELECT_FAIL,
};

void DebugCardTransactionReport(BYTE TransMode, BYTE *TransData, BYTE TransDataSize);
#endif


//------------------------------------------------------------------------------
/** 	@brief	Card Mode - Card Select 
	@param 	None
	@return 	None
	@remark 카드 읽기 동작 수행
	@remark RF On -> REQA 전송 -> ATQA 확인 -> Anticollision 알고리즘 수행 -> Select 수행
	@remark 카드 UID 읽기가 완료되지 않을 경우 gCardCheckLoopCnt 회수만큼 반복 수행
	@remark 입력된 카드의 UID를 모두 읽었는지 확인하기 위해 gCardCheckLoopCnt 회수만큼 반복 수행
	@remark ISO14443A 규격 참조
*/
//------------------------------------------------------------------------------
void CardModeSelect(void)
{
	BYTE bTmp;
#ifdef	DEBUG_CARD
BYTE bBuf[8];
#endif

	switch(gCardProcessStep)
	{
		case CARDPRCS_REQA_SEND:
			if(gCardPrcsTimer2ms)		break;

			IrqClear();
			Iso14443aReqaSend(ISO14443A_REQA);

#ifdef	DEBUG_CARD
DebugCardTransactionReport(DEBUG_CARD_REQA, &bTmp, 0);
#endif

			gCardPrcsTimer2ms = 2; 
			gCardProcessStep = CARDPRCS_REQA_RESPOND;
			break;
			
		case CARDPRCS_WUPA_SEND:
			if(gCardPrcsTimer2ms)		break;

			IrqClear();
			Iso14443aReqaSend(ISO14443A_WUPA);
		
#ifdef	DEBUG_CARD
DebugCardTransactionReport(DEBUG_CARD_REQA, &bTmp, 0);
#endif

			gCardPrcsTimer2ms = 2; 
			gCardProcessStep = CARDPRCS_REQA_RESPOND;
			break;

		case CARDPRCS_REQA_RESPOND:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gCardPrcsTimer2ms)	break;

				// 여러 개의 카드가 동시에 접촉될 경우에는 
				// 접촉된 카드를 모두 읽어들였는지를 
				//일정 회수동안 Loop를 수행하면서 확인할 수 밖에 없음.
				if(gCardCheckLoopCnt == 0)
				{
					if(gCardInputNumber == 0)
					{
						gCardReadStatus = CARDREAD_NO_CARD;
						CardGotoRfTxOff();
					}
					else
					{
						CardGotoReadResult(CARDPRCS_MIFARE_PROCESS);
					}
					break;
				}
				else
				{
					gCardCheckLoopCnt--;
#ifdef	FELICA_CARD_SUPPORTED
					if(GetInputCardType() == CARD_TYPE_NONE)
					{
						CardGotoFelicaPollingStart();			
					}
					else
#endif
					{
						gCardProcessStep = CARDPRCS_REQA_SEND;
					}
				}	
				break;	
			}

			
			if(Iso14443aReqaRespond(&gCardUidLength) == STATUS_SUCCESS)
			{

#ifdef	DEBUG_CARD
DebugCardTransactionReport(DEBUG_CARD_ATQA_OK, gTrfFifoDataBuf, 2);
#endif

				CardGotoAnticollisionStart();
			}
			else
			{
#ifdef	DEBUG_CARD
DebugCardTransactionReport(DEBUG_CARD_ATQA_FAIL, gTrfFifoDataBuf, 2);
#endif

#ifdef	FELICA_CARD_SUPPORTED
				if(GetInputCardType() == CARD_TYPE_NONE)
				{
					CardGotoFelicaPollingStart();			
				}
				else
#endif
				{
					gCardProcessStep = CARDPRCS_REQA_SEND;				
				}	
			}
			break;

		case CARDPRCS_ANTICOLLISION_SEND:
			IrqClear();
			if(Iso14443aAnticollisionSend() == STATUS_FAIL)
			{
				gCardProcessStep = CARDPRCS_SELECT_SEND;
				break;
			}
			gCardPrcsTimer2ms = 5; 
			gCardProcessStep = CARDPRCS_ANTICOLLISION_RESPOND;
			break;

		case CARDPRCS_ANTICOLLISION_RESPOND:
			bTmp = IrqCheck();
			if((bTmp != IRQ_RX_COMPLETED) && (bTmp != IRQ_COLLISION_ERROR)) 
			{
				if(gCardPrcsTimer2ms)	break;

				gCardProcessStep = CARDPRCS_REQA_SEND;
				break;
			}

#ifdef	DEBUG_CARD
extern BYTE gIso14443aTempUidBuf[5];
#endif

			bTmp = Iso14443aAnticollisionRespond(bTmp);
			if(bTmp == STATUS_SUCCESS)
			{
#ifdef	DEBUG_CARD
DebugCardTransactionReport(DEBUG_CARD_ANTICOLLISION_OK, gIso14443aTempUidBuf, 5);
#endif
				gCardProcessStep = CARDPRCS_SELECT_SEND;
			}
			else if(bTmp == STATUS_FAIL)
			{
#ifdef	DEBUG_CARD
DebugCardTransactionReport(DEBUG_CARD_ANTICOLLISION_FAIL, gIso14443aTempUidBuf, 5);
#endif

				gCardProcessStep = CARDPRCS_REQA_SEND;				
			}
			else
			{
#ifdef	DEBUG_CARD
DebugCardTransactionReport(DEBUG_CARD_ANTICOLLISION_OK, gIso14443aTempUidBuf, 5);
#endif
				gCardProcessStep = CARDPRCS_ANTICOLLISION_SEND;
			}
			break;

		case CARDPRCS_SELECT_SEND:
			IrqClear();
			Iso14443aSelectSend();
			gCardPrcsTimer2ms = 5; 
			gCardProcessStep = CARDPRCS_SELECT_RESPOND;
			break;

		case CARDPRCS_SELECT_RESPOND:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gCardPrcsTimer2ms)	break;

				gCardProcessStep = CARDPRCS_REQA_SEND;
				break;				
			}

			bTmp = Iso14443aSelectRespond();
			if(bTmp == SAK_UID_COMPLETE)
			{
				Iso14443aHalt();

				CopyCardUid(gCardOneUid, gCardUidLength);

#ifdef	DEBUG_CARD
memcpy(bBuf+1, gCardOneUid, 7);
bBuf[0] = gTrfFifoDataBuf[0];
DebugCardTransactionReport(DEBUG_CARD_SELECT_OK, bBuf, 8);
#endif
/*
				if(TempSaveCompletedUID() == UID_AVAILABLE)
				{
					gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;
					break;
				}	
				*/
				//2017.05.23 moonsungwoo 
				
				bTmp = TempSaveCompletedUID();
				if(bTmp == UID_AVAILABLE)
				{
					gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;
					break;
				}
				else if(bTmp == UID_DUPICATED)
				{
					if(gCardCheckLoopCnt)    --gCardCheckLoopCnt;

					if(gCardCheckLoopCnt == 0)
					{
						if(gCardInputNumber == 0)
						{
							gCardReadStatus = CARDREAD_NO_CARD;
							CardGotoRfTxOff();
						}
						else
						{
#ifdef	FELICA_CARD_SUPPORTED
							SetInputCardType(CARD_TYPE_MIFARE);
#endif
							CardGotoReadResult(CARDPRCS_MIFARE_PROCESS);
						}
						break;
					}
				}

				gCardPrcsTimer2ms = 0;
				gCardProcessStep = CARDPRCS_REQA_SEND;
				// UID가 모두 00h 또는 FFh이거나 이미 등록한 UID와 중복될 경우 처리
				break;
			}
			// 만약 UID 읽기가 complete되지 않았다면
			else if(bTmp == SAK_UID_NOT_COMPLETE)		
			{
#ifdef	DEBUG_CARD
memcpy(bBuf+1, gCardOneUid, 7);
bBuf[0] = gTrfFifoDataBuf[0];
DebugCardTransactionReport(DEBUG_CARD_SELECT_FAIL, bBuf, 8);
#endif
				CopyCardUid(gCardOneUid, gCardUidLength);

				if(gCardUidLength == 7)
				{
					AntiColVariInit();

					CascadeLevelSet(2);

					gCardProcessStep = CARDPRCS_ANTICOLLISION_SEND;
					break;
				}
			}

			gCardProcessStep = CARDPRCS_REQA_SEND;
			break;

		default:
			CardModeClear();
			break;
	}		
}


//------------------------------------------------------------------------------
/** 	@brief	Card Mode - Card Read Result Process 
	@param 	None
	@return 	None
	@remark 카드 읽기 결과 처리
	@remark Mifare Classic 카드만 처리
	@remark 카드 종류 및 필요한 처리 내용에 따라 추가 가능
*/
//------------------------------------------------------------------------------
void CardModeReadResult(void)
{
	switch(gCardProcessStep)
	{
		case CARDPRCS_MIFARE_PROCESS:
			gCardReadStatus = CARDREAD_SUCCESS;
			CardGotoAwayFromReaderCheck();
			break;

		default:
			CardModeClear();
			break;
	}
}




//------------------------------------------------------------------------------
/** 	@brief	Card Mode - Card Off Check 
	@param 	None
	@return 	None
	@remark 입력된 카드의 떨어짐 감지를 위한 처리
	@remark 50ms 간격으로 REQA 전송
	@remark 5회 연속으로 응답이 없을 경우 떨어짐 처리 (50ms*5회 = 250ms)
*/
//------------------------------------------------------------------------------
void CardModeAwayFromReaderCheck(void)
{
	BYTE Dummy;

	switch(gCardProcessStep)
	{
		case CARDPRCS_AWAY_CHECK:
			if(gCardAwayTimer2ms)		break;
			gCardAwayTimer2ms = 25;

			RfidTxOn();
			
#ifdef	FELICA_CARD_SUPPORTED
			if(GetInputCardType() == CARD_TYPE_FELICA)
			{
				gCardPrcsTimer2ms = 10;
				gCardProcessStep = CARDPRCS_AWAY_REQC_SEND;
			}
			else
#endif
			{
				gCardPrcsTimer2ms = 2;
				gCardProcessStep = CARDPRCS_AWAY_REQA_SEND;
			}
			break;				

		case CARDPRCS_AWAY_REQA_SEND:
			if(gCardPrcsTimer2ms)		break;

			IrqClear();
			Iso14443aReqaSend(ISO14443A_WUPA);
			gCardPrcsTimer2ms = 5; 
			gCardProcessStep = CARDPRCS_AWAY_REQA_RESPOND;
			break;
			
		case CARDPRCS_AWAY_REQA_RESPOND:
			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gCardPrcsTimer2ms)	break;

				if(gCardCheckLoopCnt)	gCardCheckLoopCnt--;
			}
			else
			{
				if(Iso14443aReqaRespond(&Dummy) == STATUS_SUCCESS)
				{
					gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;
				}
				else
				{
					if(gCardCheckLoopCnt)	gCardCheckLoopCnt--;
				}
			}
			RfidTxOff();

			if(gCardCheckLoopCnt)
			{
				gCardProcessStep = CARDPRCS_AWAY_CHECK;
				break;
			}

			gCardReadStatus= CARDREAD_NO_CARD;
			CardGotoRfTxOff();
			break;			

#ifdef	FELICA_CARD_SUPPORTED
		case CARDPRCS_AWAY_REQC_SEND:
			if(gCardPrcsTimer2ms)		break;
		
			IrqClear();
			Felica_Polling_Send();
			gCardPrcsTimer2ms = 5; 
			gCardProcessStep = CARDPRCS_AWAY_REQC_RESPOND;
			break;
			
		case CARDPRCS_AWAY_REQC_RESPOND:
			// 카드 ID 읽어 오는 동안 불필요한 Access를 줄이기 위해 최소 200us 간격으로 확인
			Delay(SYS_TIMER_200US);

			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gCardPrcsTimer2ms)	break;
		
				if(gCardCheckLoopCnt)	gCardCheckLoopCnt--;
			}
			else
			{
				// 18byte를 읽어들이기 위해 대기
				Delay(SYS_TIMER_2MS);

				if(Felica_Polling_Respond() == STATUS_SUCCESS)
				{
					gCardCheckLoopCnt = CARD_LOOP_DEFAULT_COUNT;
				}
				else
				{
					if(gCardCheckLoopCnt)	gCardCheckLoopCnt--;
				}
			}
			RfidTxOff();
		
			if(gCardCheckLoopCnt)
			{
				gCardProcessStep = CARDPRCS_AWAY_CHECK;
				break;
			}

			gCardReadStatus= CARDREAD_NO_CARD;
			CardGotoRfTxOff();
			break;			
#endif		

		default:
			CardModeClear();
			break;
	}
}



void CardReadStatusClear(void)
{
	gCardReadStatus = 0;
}


BYTE GetCardReadStatus(void)
{
	return (gCardReadStatus);
}


BYTE GetCardInputNumber(void)
{
	return (gCardInputNumber);
}



void(*const CardModeProcess_Tbl[])(void) = {
	CardModeIdleState,
	CardModePowerOnOff,
	CardModeSelect,
#ifdef	FELICA_CARD_SUPPORTED
	CardModeFelicaPolling,
#endif	
	CardModeReadResult,
	CardModeAwayFromReaderCheck,
};


void CardModeProcess(void)
{
	CardModeProcess_Tbl[gCardMode]();
}




//------------------------------------------------------------------------------
/** 	@brief	Card Detection Process 
	@param 	None
	@return 	None
	@remark 250ms 간격으로 폴링하면서 계속 카드 입력을 감지하는 역할
	@remark Power Down <-> Wake Up 간격으로 폴링하면서 계속 카드 입력을 감지하는 역할
	@remark 카드가 감지될 경우 바로 Anticollision 수행으로 이동
*/
//------------------------------------------------------------------------------
void CardDetectionProcess(void)
{
#ifndef	_KEYPAD_WAKEUP_FOR_CARD_USE
	// 3분락 수행 중일 경우 MainMode 처리하지 않음
	if(GetTamperProofPrcsStep())	return;

	if(GetMainMode())		return;
	if(gCardMode)			return;
	if(PowerDownModeForJig() == STATUS_SUCCESS) return;
	

	if(gCardPrcsTimer2ms)	return;
	gCardPrcsTimer2ms = 250;


	// KC 인증을 위한 카드 출력 ON 처리
#ifdef	DDL_TEST_SET_FOR_CARD
	if(IsCardPowerOnForTest() == true)		return;
#endif

#ifdef	FELICA_CARD_SUPPORTED
	// Falica 카드를 지원할 경우 Felica 카드 먼저 확인
	CardAllUidBufferClear();

	if(FelicaFindTag())
	{
		CopyFelicaCardUid(gCardOneUid);
		gCardUidLength = 8;

		if(TempSaveCompletedUID() == UID_AVAILABLE)
		{
			SetInputCardType(CARD_TYPE_FELICA);

			CardGotoReadResult(CARDPRCS_MIFARE_PROCESS);
			ModeGotoCardVerify();
		}
		return;
	}

#endif

	if(Iso14443aFindTag(&gCardUidLength))
	{
#ifdef	FELICA_CARD_SUPPORTED
		SetInputCardType(CARD_TYPE_MIFARE);
#endif

		CardAllUidBufferClear();
		CardCheckCountReset();   //2017.05.23 moonsungwoo 
		CardGotoAnticollisionStart();
		ModeGotoCardVerify();
	}
#endif			// _KEYPAD_WAKEUP_FOR_CARD_USE
}



void UpdateCardUidToMemoryByModule(BYTE* pCardUid, BYTE SlotNumber)
{
	if(gbManageMode == _AD_MODE_SET)
	{
		// Slot Numebr에 대해 한 번 더 확실하게 하기 위해
		if( (SlotNumber < 1) || (SlotNumber > SUPPORTED_USERCARD_NUMBER) )
		{
			return;
		}
#if defined (_USE_IREVO_CRYPTO_)
		EncryptDecryptKey(pCardUid,8);
#endif 	
		RomWrite(pCardUid, CARD_UID+(((WORD)SlotNumber-1)*MAX_CARD_UID_SIZE), MAX_CARD_UID_SIZE);
	}
}



// KC 인증을 위한 카드 출력 ON 처리
#ifdef	DDL_TEST_SET_FOR_CARD

BYTE gfCardPowerOnForTest = 0;
WORD gKCFeedbackTimer1s = 0;

bool IsCardPowerOnForTest(void)
{
	if(gfCardPowerOnForTest)	return (true);

	return (false);
}


void CardPowerOnForTest(void)
{
	RfidPowerOn();
	Delay(SYS_TIMER_4MS);
	RfidTxOn();
	gfCardPowerOnForTest = 1;
	gKCFeedbackTimer1s = 5;
}


void CardPowerOffForTest(void)
{
	RfidPowerOff();
	//gfCardPowerOnForTest = 0; //new KC
}

#endif



//=============================================================
//	TEST FUNCTIONS
//=============================================================

// 테스트 외에는 아래 내용은 모두 주석 처리 가능

#ifdef	DEBUG_CARD


#define	JIG_CMD_REQA_OK		0x90
#define	JIG_CMD_ATQA_OK		0x91
#define	JIG_CMD_ATQA_FAIL	0x92
#define	JIG_CMD_ANTI_OK		0x93
#define	JIG_CMD_ANTI_FAIL	0x94
#define	JIG_CMD_SEL_OK		0x95
#define	JIG_CMD_SEL_FAIL	0x96



void DebugCardTransactionReport(BYTE TransMode, BYTE *TransData, BYTE TransDataSize)
{
	BYTE bytecount = 0;

	switch(TransMode)
	{
		case DEBUG_CARD_REQA:
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = JIG_CMD_REQA_OK;
			DebugDataBuf[bytecount++] = 0;
			DebugDataBuf[bytecount++] = 0x03;

			DebugTxDataSend(DebugDataBuf, bytecount);
			break;

		case DEBUG_CARD_ATQA_OK:
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = JIG_CMD_ATQA_OK;
			DebugDataBuf[bytecount++] = 2;
			DebugDataBuf[bytecount++] = TransData[0];
			DebugDataBuf[bytecount++] = TransData[1];
			DebugDataBuf[bytecount++] = 0x03;
		
			DebugTxDataSend(DebugDataBuf, bytecount);
			break;

		case DEBUG_CARD_ATQA_FAIL:
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = JIG_CMD_ATQA_FAIL;
			DebugDataBuf[bytecount++] = 2;
			DebugDataBuf[bytecount++] = TransData[0];
			DebugDataBuf[bytecount++] = TransData[1];
			DebugDataBuf[bytecount++] = 0x03;
		
			DebugTxDataSend(DebugDataBuf, bytecount);
			break;

		case DEBUG_CARD_ANTICOLLISION_OK:
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = JIG_CMD_ANTI_OK;
			DebugDataBuf[bytecount++] = 5;
			DebugDataBuf[bytecount++] = TransData[0];
			DebugDataBuf[bytecount++] = TransData[1];
			DebugDataBuf[bytecount++] = TransData[2];
			DebugDataBuf[bytecount++] = TransData[3];
			DebugDataBuf[bytecount++] = TransData[4];
			DebugDataBuf[bytecount++] = 0x03;
		
			DebugTxDataSend(DebugDataBuf, bytecount);
			break;

		case DEBUG_CARD_ANTICOLLISION_FAIL:
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = JIG_CMD_ANTI_FAIL;
			DebugDataBuf[bytecount++] = 5;
			DebugDataBuf[bytecount++] = TransData[0];
			DebugDataBuf[bytecount++] = TransData[1];
			DebugDataBuf[bytecount++] = TransData[2];
			DebugDataBuf[bytecount++] = TransData[3];
			DebugDataBuf[bytecount++] = TransData[4];
			DebugDataBuf[bytecount++] = 0x03;
		
			DebugTxDataSend(DebugDataBuf, bytecount);
			break;

		case DEBUG_CARD_SELECT_OK:
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = 0x02;
			DebugDataBuf[bytecount++] = JIG_CMD_SEL_OK;
			DebugDataBuf[bytecount++] = 5;
			DebugDataBuf[bytecount++] = TransData[0];
			DebugDataBuf[bytecount++] = TransData[1];
			DebugDataBuf[bytecount++] = TransData[2];
			DebugDataBuf[bytecount++] = TransData[3];
			DebugDataBuf[bytecount++] = TransData[4];
			DebugDataBuf[bytecount++] = 0x03;
		
			DebugTxDataSend(DebugDataBuf, bytecount);
			break;

			
	}
}

#endif



#ifdef	FELICA_CARD_SUPPORTED
void SetInputCardType(BYTE bType)
{
	gCardProcessedType = bType;
}


BYTE GetInputCardType(void)
{
	return (gCardProcessedType);
}


void CardGotoReqaRestart(void)
{
	gCardMode = CARDMODE_SELECT;
	gCardProcessStep = CARDPRCS_REQA_SEND;
}


void CardGotoFelicaPollingStart(void)
{
	gCardMode = CARDMODE_FELICA_POLLING;
	gCardProcessStep = CARDPRCS_FELICA_POLLING_SEND;
}

void CardModeFelicaPolling(void)
{
	switch(gCardProcessStep)
	{
		case CARDPRCS_FELICA_POLLING_SEND:
			IrqClear();
			Felica_Polling_Send();
			gCardPrcsTimer2ms = 5; 
			gCardProcessStep = CARDPRCS_FELICA_POLLING_RESPOND;
			break;

		case CARDPRCS_FELICA_POLLING_RESPOND:
			// 카드 ID 읽어 오는 동안 불필요한 Access를 줄이기 위해 최소 200us 간격으로 확인
			Delay(SYS_TIMER_200US);

			if(IrqCheck() != IRQ_RX_COMPLETED)
			{
				if(gCardPrcsTimer2ms)	break;

				CardGotoReqaRestart();
				break;				
			}

			if(Felica_Polling_Respond() == STATUS_SUCCESS)
			{
				CopyFelicaCardUid(gCardOneUid);
				gCardUidLength = 8;
				
				if(TempSaveCompletedUID() == UID_AVAILABLE)
				{
					SetInputCardType(CARD_TYPE_FELICA);
				
					CardGotoReadResult(CARDPRCS_MIFARE_PROCESS);
				}
				break;
			}

			CardGotoReqaRestart();
			break;

		default:
			CardModeClear();
			break;
	}
}
#endif	


