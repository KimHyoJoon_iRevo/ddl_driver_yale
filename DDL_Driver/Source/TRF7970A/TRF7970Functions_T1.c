//------------------------------------------------------------------------------
/** 	@file		TRF7970Functions_T1.c
	@version 0.1.00
	@date	2016.04.08
	@brief	TI TRF7970 Interface Functions 
	@remark	 TI 의 Card Reader IC인 TRF7970을 제어하기 위한 함수
	@remark	 IC와는 SPI 통신 수행
	@see	ISO14443AFunctions_T1.c
	@see	CardProcess_T1.c
	@see	ParallelFunctions_T1.c
	@see	MX_spi_lapper.c
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.08		by Jay
			- TRF7970을 SPI로 Serial 제어
			- 해당 함수는 TRF7970을 Serial 및 Parallel로 제어하는 내용을 포함하며, 카드 구동을 위해서는 
				ISO14443AFunctions_T1 과 CardProcess_T1 이 모두 연결되어 있어 같이 결합하여 사용해야 함.
			- 설정에 의해 Serial 및 Parallel로 제어 선택 가능 (Parallel은 테스트 미진행)
*/
//------------------------------------------------------------------------------


#include "Main.h"



#ifndef	DDL_CFG_RFID_PARALLEL
// TRF7970A SPI define 내용 추가
ddl_spi_t ddl_trf7970a_spi_dev;				/**< SPI 관련 설정에 사용  */
#endif 

BYTE gTrfFifoDataBuf[FIFO_MAX_SIZE];		/**< TRF7970 FIFO에 Write/Read하는 Buffer  */



//------------------------------------------------------------------------------
/** 	@brief	Send command
	@param 	[Command] : 전송할 Command 
	@return 	None
	@remark Command를 전송할 때 사용
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void TrfSendCommand(BYTE Command)
{
	Command |= 0x80;								
	Command &= 0x9F;								
#ifdef DDL_CFG_RFID_PARALLEL
	ParallelSOF();
	ParallelDataSend(&Command, 1);
	ParallelEOF();
#else 
		// NSS는 Active Low
		P_RFID_SPI_NSS(0);
		ddl_spi_write(&ddl_trf7970a_spi_dev, (uint8_t*)&Command, 1);
		P_RFID_SPI_NSS(1);
#endif 
}



//------------------------------------------------------------------------------
/** 	@brief	Set Data to Register
	@param 	[Register] : 설정할 Register 주소
	@param 	[SetData] : 설정할 1 byte Data
	@return 	None
	@remark Register를 설정할 때 사용
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void TrfSetRegister(BYTE Register, BYTE SetData)
{
#ifdef DDL_CFG_RFID_PARALLEL
	ParallelSOF();
	ParallelDataSend(&Register, 1);
	ParallelDataSend(&SetData, 1);
	ParallelEOF();
#else 
		// NSS는 Active Low
		P_RFID_SPI_NSS(0);
		ddl_spi_write(&ddl_trf7970a_spi_dev, (uint8_t*)&Register, 1);
		ddl_spi_write(&ddl_trf7970a_spi_dev, (uint8_t*)&SetData, 1);
		P_RFID_SPI_NSS(1);
#endif 
}



//------------------------------------------------------------------------------
/** 	@brief	Send Inventory Command
	@param 	[WriteDataBuf] : 전송할 1 byte 이상의 Data 버퍼의 포인터 	
	@param 	[DataSize] : 전송할 Data의 byte 수 	
	@return 	None
	@remark 1 byte 이상의 Data 전송할 경우에 사용, 해당 함수는 Inventory Command에 사용
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void TrfSendInventoryCommand(BYTE* WriteDataBuf, BYTE DataSize)
{
#ifdef DDL_CFG_RFID_PARALLEL
	ParallelSOF();
	ParallelDataSend(WriteDataBuf, DataSize);
	if(DataSize > 1)
	{
		ParallelEOFContinuous();
	}
	else
	{
		ParallelEOF();
	}
#else 
	// NSS는 Active Low
	P_RFID_SPI_NSS(0);
	ddl_spi_write(&ddl_trf7970a_spi_dev, (uint8_t*)WriteDataBuf, (uint16_t)DataSize);
	P_RFID_SPI_NSS(1);
#endif 
}



//------------------------------------------------------------------------------
/** 	@brief	Get Data from Register
	@param 	[Register] : 읽어들일 Register 주소
	@param 	[ReadDataBuf] : 읽어들인 1 byte 이상의 Data를 저장할 버퍼의 포인터 	
	@param 	[DataSize] : 읽어들일 Data의 byte 수 	
	@return 	None
	@remark 1 byte 이상의 Data를 읽어들일 때 사용
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void TrfGetRegisterData(BYTE Register, BYTE* ReadDataBuf, BYTE DataSize)
{
	if(DataSize > 1)
	{
		Register |= 0x60;							
	}
	else
	{
		Register |= 0x40;							
	}

#ifdef DDL_CFG_RFID_PARALLEL
	ParallelSOF();
	ParallelDataSend(&Register, 1);
	ParallelDataReceive(ReadDataBuf, DataSize);
	if(DataSize > 1)
	{
		ParallelEOFContinuous();
	}
	else
	{
		ParallelEOF();
	}
#else 
	// NSS는 Active Low
	P_RFID_SPI_NSS(0);
	ddl_spi_write(&ddl_trf7970a_spi_dev, (uint8_t*)&Register, 1);

		// 수신할 때 MOSI를 Low로 만들기 위해 설정
	memset(ReadDataBuf, 0x00, DataSize);													
	ddl_spi_read(&ddl_trf7970a_spi_dev, (uint8_t*)ReadDataBuf, (uint16_t)DataSize);
	P_RFID_SPI_NSS(1);
#endif 
}




//------------------------------------------------------------------------------
/** 	@brief	TRF7970 Register Intialization
	@param 	None
	@return 	None
	@remark TRF7970 IC의 EN 설정 이후 초기 Regiater Setting
	@remark    [1] Software Initialization
	@remark    [2] Idle
	@remark    [3] ISO14443A, 106kbps에 맞게 Register 반드시 재설정해야 함.
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void Trf797xInitialSettings(void)
{	
	// 해당 설정은 EN = 1 될 때마다 하도록 하였으나 SOFT_INIT 으로 인해 불필요한 RF 출력이 나오는 관계로
	// 최초 전원 인가 시에만 해당 설정 진행하도록 수정

	TrfSendCommand(SOFT_INIT);
	TrfSendCommand(IDLE);

	//SPI 모드 사용시 시작 구간에서 Register 0x18을 클리어 해야 합니다. 
	//이 동작은 idle command가 전송되고 난 직후에 이루어져야 하는 동작입니다. 
	//Errata에 해당 내용에 대해 나타나 있습니다. 
	TrfSetRegister(NFC_TARGET_DET_LEVEL, 0x00);

	// Software Initialization 이후에는 몇몇 Register 값이 변하기 때문에 반드시 다시 설정해 주어야 한다.
	//TRF797x ISO14443A, bit rate 106kbit/s Setting
	TrfSetRegister(CHIP_STATE_CONTROL, 0x01);
	TrfSetRegister(ISO_CONTROL, 0x08);					
	TrfSetRegister(RX_NO_RESPONSE_WAIT_TIME, 0x0E);					
	TrfSetRegister(RX_WAIT_TIME, 0x07);					

	//Reference Source에는 SYS_CLK Output 설정되어 있으나 우리는 사용하지 않으므로 Disabled로 수정
	TrfSetRegister(MODULATOR_CONTROL, 0x01);	// OOK 100%
	TrfSetRegister(REGULATOR_CONTROL, 0x07);


	TrfSetRegister(RX_SPECIAL_SETTINGS, 0x20);					
}



//------------------------------------------------------------------------------
/** 	@brief	TRF7970 RF Turn On
	@param 	None
	@return 	None
	@remark TRF7970 IC의 RF 출력 ON
	@remark    [1] Get Chip Status Control Register (0x00) 
	@remark    [2] RF 출력 관련 부분 모두 Clear하고 아래와 같이 설정
					-> RF output active
					-> full output power
					-> 3V operation
						-> Reference Source에는 이와 같이 설정되어 있으나 우리는 5V operation으로 수정
	@remark	   [3] ISO Control이 다른 Mode로 전환되지 않도록 RFID Mode로 재설정
			   		-> 이 설정은 반드시 할 필요는 없으나 
			   			ISO Control 설정이 바뀌면 원하는 시점에 RF 출력이 바로 되지 않을 수 있어 재설정
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void Trf797xTurnRfOn(void)
{
	// RF output active, full output power, 5V operation
	//	Reference Source에는 3V operation으로 설정되어 있으나 우리는 5V operation으로 수정
	TrfSetRegister(MODULATOR_CONTROL, 0x01);	// OOK 100%

	//Reference Source에서는 
	//	REGULATOR_CONTROL의 auto_reg를 설정하여 사용하지 않아 0x07로 되어 있음.
	//	CHIP_STATE_CONTROL의 agc_on을 설정하여 사용하지 않아 0x21로 되어 있음.
	// 우리가 사용하던 기존 Source의 설정이 아래와 같이 되어 있어 그대로 활용
	TrfSetRegister(REGULATOR_CONTROL, 0x87);

	TrfSetRegister(CHIP_STATE_CONTROL, 0x25);

	TrfSetRegister(ISO_CONTROL, 0x08);					
	TrfSetRegister(RX_WAIT_TIME, 0x07);					
	TrfSetRegister(RX_SPECIAL_SETTINGS, 0x20);					
}



//------------------------------------------------------------------------------
/** 	@brief	TRF7970 RF Turn Off
	@param 	None
	@return 	None
	@remark TRF7970 IC의 RF 출력 OFF
	@remark    [1] Get Chip Status Control Register (0x00) 
	@remark    [2] RF 출력 관련 부분 모두 Clear하고 아래와 같이 설정
					-> RF output not active
					-> Reference Source에는 0x1F만 하여 RF output active 상태는 유지되도록 되어 있으나 출력 제어를 위해 수정
	@remark	   [3] ISO Control이 다른 Mode로 전환되지 않도록 RFID Mode로 재설정
			   		-> 이 설정은 반드시 할 필요는 없으나 
			   			ISO Control 설정이 바뀌면 원하는 시점에 RF 출력이 바로 되지 않을 수 있어 재설정
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void Trf797xTurnRfOff(void)
{
	// RF output not active
	// Reference Source에는 0x1F만 하여 RF output active 상태는 유지되도록 되어 있으나 출력 제어를 위해 수정
	TrfSetRegister(CHIP_STATE_CONTROL, 0x01);
}



// 아래의 함수들은 RFID Reader IC가 바뀌더라도 외부에서는 동일한 함수를 호출할 수 있도록 함수명을 고정



//------------------------------------------------------------------------------
/** 	@brief	IC Initial
	@param 	None 	
	@return 	None 
	@remark TRF7970 IC On 이후 안정화 대기
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void RfidInitial(void)
{
	P_RFID_EN(0);
#ifdef DDL_CFG_RFID_PARALLEL
	P_RFID_EN2(0);// 이 PIN 을 제어하면 소모 전류가 높음 제어 안하고 low 로 두는 것으로 
#endif 

	Delay(SYS_TIMER_2MS);
	
#ifndef DDL_CFG_RFID_PARALLEL	
	ddl_trf7970a_spi_dev.dev.mcu.hspi = gh_mcu_spi;
#endif 	

	RfidPowerOn();
	Trf797xInitialSettings();
	RfidPowerOff();
}


//------------------------------------------------------------------------------
/** 	@brief	IC EN on & Initialization
	@param 	None 	
	@return 	None 
	@remark TRF7970 EN pin high, EN2 pin은 항상 low
	@remark RF 출력을 ON하기 위해서는 별도의 RfidTxOn 함수를 수행해야 함
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void RfidPowerOn(void)
{
	BYTE bNum = 50;
	BYTE bTmp;

#ifdef DDL_CFG_RFID_PARALLEL
//	P_RFID_EN2(1); // 이거 안켜줘도 동작은 하는데... 
//	P_RFID_EN2(0); // RfidInitial 에서 한번 low 로 만들어 줌  
	//Delay(SYS_TIMER_1MS); 			
	P_RFID_EN(1); 

	SPI_On();
#else 
		SPI_On();

		P_RFID_SPI_NSS(1);

		// Datasheet에는 SS = 1 이후 EN = 1까지 4ms 정도의 Delay가 필요하다고 되어 있지만, 
		//	폴링 과정에서의 전류 소모를 줄이기 위해 해당 부분 삭제
//		Delay(SYS_TIMER_4MS);
	P_RFID_EN(1);
#endif 

	Delay(SYS_TIMER_1MS);				// wait until system clock started

	while(--bNum)
	{
		TrfGetRegisterData(RSSI_LEVELS, &bTmp, 1); 
		if(bTmp & 0x40)		// 크리스탈 안정화 확인 
		{
			break;
		}
		Delay(SYS_TIMER_200US);
	}										// 1140 us	(안전 화 시간 800us)
}


void RfidSpiSetOff(void)
{
#ifdef DDL_CFG_RFID_PARALLEL
	SPI_Off();
#else 
		SPI_Off();
		// TI TRF7970A가 EN=0일 때 SPI 핀들이 모두 Output Low로 설정되어야 함
		P_RFID_SPI_NSS(0);
		P_RFID_SPI_CLK(0);
		P_RFID_SPI_MOSI(0);
		P_RFID_SPI_MISO(0);
#endif 	
}


//------------------------------------------------------------------------------
/** 	@brief	IC EN off
	@param 	None 	
	@return 	None 
	@remark TRF7970 EN pin low, EN2 pin은 항상 low, Complete power down
	@remark RF 출력도 자동으로 OFF됨
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void RfidPowerOff(void)
{
	Trf797xTurnRfOff();
	P_RFID_EN(0);
	RfidSpiSetOff();
}


//------------------------------------------------------------------------------
/** 	@brief	IC RF On
	@param 	None 	
	@return 	None 
	@remark TRF7970 RF 출력 On
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void RfidTxOn(void)
{
	Trf797xTurnRfOn();
}


//------------------------------------------------------------------------------
/** 	@brief	IC RF Off
	@param 	None 	
	@return 	None 
	@remark TRF7970 RF 출력 Off
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void RfidTxOff(void)
{
	Trf797xTurnRfOff();
}


//------------------------------------------------------------------------------
/** 	@brief	IRQ Check
	@param 	None 	
	@return 	[IRQ_NONE] : IRQ 미수신 또는 FIFO 관련 Warning이나 TX end 발생했을 경우 
	@return 	[IRQ_RX_COMPLETED] : RX start이고, Collision 에러나, 기타 Error가 발생하지 않았을 경우
	@return 	[IRQ_COLLISION_ERROR] : Collision error 발생 
	@return 	[IRQ_ERROR] : 기타 Error가 발생했을 경우 
	@remark IRQ_STATUS register 값을 확인하여 TX/RX 상태 확인
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
BYTE IrqCheck(void)
{
	BYTE IrqStatus[2];
		
	// IRQ Interrupt 발생할 경우에만 수행
	if(gbRFID_IRQ_In)
	{
		gbRFID_IRQ_In = 0;

#ifdef	DDL_CFG_RFID_PARALLEL
	TrfGetRegisterData(IRQ_STATUS, IrqStatus, 1);
#else 
	// read second reg. as dummy read
	TrfGetRegisterData(IRQ_STATUS, IrqStatus, 2);
#endif 

		// Collision Error 발생
		if(IrqStatus[0] & 0x02)
		{
			return IRQ_COLLISION_ERROR;	
		}	
		// Collision Error를 제외한 다른 Error 발생
		else if(IrqStatus[0] & 0x1F)
		{
			return IRQ_ERROR;
		}
		// RX Start
//		else if(IrqStatus[0] & 0x40) 
		else if((IrqStatus[0] & 0xC0) == 0xC0) 
		{
			Delay(SYS_TIMER_500US);
			return IRQ_RX_COMPLETED;
		}
		else if((IrqStatus[0] & 0xC0) == 0x40) 
		{
			return IRQ_RX_COMPLETED;
		}
	}
	// 그 외 FIFO 관련 Warning이나 TX end 발생, IRQ 미수신 등은 모두 0으로 return
	return IRQ_NONE;
}



//------------------------------------------------------------------------------
/** 	@brief	IRQ Clear
	@param 	None 	
	@return 	None 
	@remark IRQ_STATUS register 값을 읽음으로써 Register 값 Clear
	@remark TRF7970 Datasheet 참조
*/
//------------------------------------------------------------------------------
void IrqClear(void)
{
	BYTE IrqStatus[2];

	gbRFID_IRQ_In = 0;
#ifdef	DDL_CFG_RFID_PARALLEL
	TrfGetRegisterData(IRQ_STATUS, IrqStatus, 1);
#else 
	// read second reg. as dummy read
	TrfGetRegisterData(IRQ_STATUS, IrqStatus, 2);
#endif 	
}






