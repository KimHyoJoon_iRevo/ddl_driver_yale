//------------------------------------------------------------------------------
/** 	@file		CardProcess_T1.h
	@brief	Card UID Read Functions 
*/
//------------------------------------------------------------------------------

#ifndef __CARDPROCESS_T1_INCLUDED
#define __CARDPROCESS_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"

#ifdef DDL_CFG_RFID_TRF7970A
#ifdef	DDL_CFG_RFID_PARALLEL
#include "ParallelFunctions_T1.h"
#else 
#include "MX_spi_lapper.h"
#endif 
#include "TRF7970Functions_T1.h"
#include "ISO14443AFunctions_T1.h"
#endif

#ifdef	FELICA_CARD_SUPPORTED
#include "FelicaFunctions_T1.h"
#endif

//==============================================================================
//      S U C C E S S                                                                                            
// name Success
//==============================================================================

//------------------------------------------------------------------------------
/*! 	\def	MAX_CARD_ANTI_SIZE		
	Anticollision 알고리즘 수행을 통해 읽어들일 수 있는 최대 카드 수
	
	\def	MAX_CARD_UID_SIZE		
	읽어들일 수 있는 카드 UID의 최대 Byte 수

	\def	CARD_LOOP_DEFAULT_COUNT		
	입력된 카드의 UID를 읽어들이기 위한 전체 과정 반복 회수,
	READY 또는 ACTIVE 상태에서 REQA 전송할 경우에는 다시 IDLE 상태가 되기 때문에 최소 2회는 반복해야 함.
*/
//------------------------------------------------------------------------------
#define	MAX_CARD_ANTI_SIZE		5
#define	MAX_CARD_UID_SIZE		8

#define	CARD_LOOP_DEFAULT_COUNT		3


//------------------------------------------------------------------------------
/*! 	\def	CARDREAD_SUCCESS		
	카드 읽기 결과 - 카드 UID 읽기 성공

	\def	CARDREAD_NO_CARD		
	카드 읽기 결과 - 카드 떨어짐

	\def	CARDREAD_FAIL		
	카드 읽기 결과 - 카드 UID 읽기 실패

	\def	CARDREAD_INNER_FORCED_LOCK		
	카드 읽기 결과 - 내부강제잠금 설정 상태

///	제품에 따라 설정 추가 가능
*/
//------------------------------------------------------------------------------
#define	CARDREAD_SUCCESS					1
#define	CARDREAD_NO_CARD					2
#define	CARDREAD_FAIL						3
#define	CARDREAD_INNER_FORCED_LOCK			8


//------------------------------------------------------------------------------
/*! 	\def	UID_NONE		
	TempSaveCompletedUID 함수의 Return 값 - UID가 모두 00h이거나 FFh일 경우 
	
	\def	UID_AVAILABLE		
	TempSaveCompletedUID 함수의 Return 값 - UID가 사용 가능할 경우 

	\def	UID_DUPICATED		
	TempSaveCompletedUID 함수의 Return 값 - UID가 이미 입력된 카드의 것과 동일할 경우 
*/
//------------------------------------------------------------------------------
#define	UID_NONE			0
#define	UID_AVAILABLE		1
#define	UID_DUPICATED		2


//gCardMode
enum{
	CARDMODE_POWER_ONOFF = 1,		//CardModePowerOnOff();	
	CARDMODE_SELECT,				//CardModeSelect();
#ifdef	FELICA_CARD_SUPPORTED
	CARDMODE_FELICA_POLLING,		//CardModeFelicaPolling();
#endif	
	CARDMODE_READ_RESULT,			//CardModeReadResult();
	CARDMODE_CARD_AWAY_CHK,			//CardModeAwayFromReaderCheck();
	
};

//gCardProcessStep
enum
{
	//CardModePowerOnOff 함수에서의 ProcessStep
	CARDPRCS_IDLE_STATE = 0,
	CARDPRCS_RFID_POWERON,
	CARDPRCS_RFID_POWEROFF,
	CARDPRCS_RFID_TXON,
	CARDPRCS_RFID_TXOFF,
	CARDPRCS_READ_STOP,

	//CardModeSelect 함수에서의 ProcessStep	
	CARDPRCS_REQA_SEND,	
	CARDPRCS_WUPA_SEND,
	CARDPRCS_REQA_RESPOND,
	CARDPRCS_ANTICOLLISION_SEND,
	CARDPRCS_ANTICOLLISION_RESPOND,
	CARDPRCS_SELECT_SEND,
	CARDPRCS_SELECT_RESPOND,

#ifdef	FELICA_CARD_SUPPORTED
	CARDPRCS_FELICA_POLLING_SEND,
	CARDPRCS_FELICA_POLLING_RESPOND,
#endif

	//CardModeReadResult 함수에서의 ProcessStep
	CARDPRCS_MIFARE_PROCESS,

	//CardModeAwayFromReaderCheck 함수에서의 ProcessStep
	CARDPRCS_AWAY_CHECK,
	CARDPRCS_AWAY_REQA_SEND,
	CARDPRCS_AWAY_REQA_RESPOND,

#ifdef	FELICA_CARD_SUPPORTED
	CARDPRCS_AWAY_REQC_SEND,
	CARDPRCS_AWAY_REQC_RESPOND,
#endif
};

extern BYTE gCardAllUidBuf[MAX_CARD_ANTI_SIZE][MAX_CARD_UID_SIZE];


void CardProcessTimeCounter(void);
void CardProcessTimeClear(void);
void CardAwayCheckTimeCounter(void);
BYTE GetCardMode(void);


void CardGotoReadStart(void);
void CardGotoReadStop(void);
void CardGotoAwayFromReaderCheck(void);


void CardReadStatusClear(void);
BYTE GetCardReadStatus(void);
BYTE GetCardInputNumber(void);

void CardModeProcess(void);


void CardDetectionProcess(void);



#define	MIN_CARDUID_LENGTH		4
#define	MAX_CARDUID_LENGTH		8


void UpdateCardUidToMemoryByModule(BYTE* pCardUid, BYTE SlotNumber);


// KC 인증을 위한 카드 출력 ON 처리
#ifdef	DDL_TEST_SET_FOR_CARD
bool IsCardPowerOnForTest(void);
void CardPowerOnForTest(void);
void CardPowerOffForTest(void);
extern BYTE gfCardPowerOnForTest;
extern WORD gKCFeedbackTimer1s;
#endif




#ifdef	FELICA_CARD_SUPPORTED

//gCardProcessedType
enum{
	CARD_TYPE_NONE = 0,
	CARD_TYPE_MIFARE,
	CARD_TYPE_FELICA,	
};

void SetInputCardType(BYTE bType);
BYTE GetInputCardType(void);

void CardGotoFelicaPollingStart(void);
void CardModeFelicaPolling(void);


#endif

#endif


