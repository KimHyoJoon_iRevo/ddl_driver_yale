//------------------------------------------------------------------------------
/** 	@file		TRF7970Functions_T1.h
	@brief	TI TRF7970 Interface Functions 
*/
//------------------------------------------------------------------------------


#ifndef __TRF7970FUNCTIONS_T1_INCLUDED
#define __TRF7970FUNCTIONS_T1_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"



//=============================================================
//	TI TRF7970
//=============================================================
//------------------------------------------------------------------------------
/*! 	\def	CHIP_STATUS_CONTROL		
	TRF7970 Control Registers - Chip Status Control (00h)
	
	\def	ISO_CONTROL		
	TRF7970 Control Registers - ISO Control (01h)

	\def	ISO_14443B_OPTIONS		
	TRF7970 Control Registers - ISO14443 TX Options (02h)

	\def	ISO_14443A_OPTIONS		
	TRF7970 Control Registers - ISO14443 High-Bit-Rate and Parity Options (03h)

	\def	TX_TIMER_EPC_HIGH		
	TRF7970 Control Registers - TX Timer High Byte Control (04h)

	\def	TX_TIMER_EPC_LOW		
	TRF7970 Control Registers - TX Timer Low Byte Control(05h)

	\def	TX_PULSE_LENGTH_CONTROL		
	TRF7970 Control Registers - TX Pulse Length Control (06h)

	\def	RX_NO_RESPONSE_WAIT_TIME		
	TRF7970 Control Registers - RX No Response Wait Time (07h)

	\def	RX_WAIT_TIME		
	TRF7970 Control Registers - RX Wait Time (08h)

	\def	MODULATOR_CONTROL		
	TRF7970 Control Registers - Modulatot and SYS_CLK Control (09h)

	\def	RX_SPECIAL_SETTINGS 	
	TRF7970 Control Registers - RX Special Setting (0Ah)

	\def	REGULATOR_CONTROL	
	TRF7970 Control Registers - Regulator and I/O Control (0Bh)

	\def	IRQ_STATUS	
	TRF7970 Control Registers - IRQ Status  (0Ch)

	\def	IRQ_MASK	
	TRF7970 Status Registers - Interrupt Mask (0Dh)

	\def	COLLISION_POSITION	
	TRF7970 Status Registers - Collision Position (0Eh)

	\def	RSSI_LEVELS	
	TRF7970 Status Registers - RSSI Levels and Oscillator Status (0Fh)

	\def	SPECIAL_FUNCTION
	TRF7970 Status Registers - Special Functions (10h)

	\def	RAM_START_ADDRESS
	TRF7970 Status Registers - Special Functions (11h)

	\def	FIFO_CONTROL 
	TRF7970 FIFO Control Registers - FIFO Status (1Ch)

	\def	TX_LENGTH_BYTE_1 
	TRF7970 FIFO Control Registers - TX Length Byte1 (1Dh)

	\def	TX_LENGTH_BYTE_2 
	TRF7970 FIFO Control Registers - TX Length Byte2 (1Eh)

	\def	FIFO 
	TRF7970 FIFO - FIFO (1Fh)

*/
//------------------------------------------------------------------------------


//---- Reader register ------------------------------------------

#define CHIP_STATE_CONTROL			0x00
#define ISO_CONTROL					0x01
#define ISO_14443B_OPTIONS			0x02
#define ISO_14443A_OPTIONS			0x03
#define TX_TIMER_EPC_HIGH			0x04
#define TX_TIMER_EPC_LOW			0x05
#define TX_PULSE_LENGTH_CONTROL		0x06
#define RX_NO_RESPONSE_WAIT_TIME	0x07
#define RX_WAIT_TIME				0x08
#define MODULATOR_CONTROL			0x09
#define RX_SPECIAL_SETTINGS			0x0A
#define REGULATOR_CONTROL			0x0B
#define IRQ_STATUS					0x0C	
#define IRQ_MASK					0x0D	
#define	COLLISION_POSITION			0x0E
#define RSSI_LEVELS					0x0F
#define SPECIAL_FUNCTION			0x10
#define RAM_START_ADDRESS			0x11	
#define	NFC_TARGET_DET_LEVEL		0x18
#define FIFO_CONTROL				0x1C
#define TX_LENGTH_BYTE_1			0x1D
#define TX_LENGTH_BYTE_2			0x1E
#define FIFO						0x1F


//---- Direct commands ------------------------------------------

//------------------------------------------------------------------------------
/*! 	\def	IDLE		
	TRF7960 Command Codes - Idle (00h)
	
	\def	SOFT_INIT		
	TRF7960 Command Codes - Software Initialization (03h)

	\def	INITIAL_RF_COLLISION		
	TRF7960 Command Codes - Perform RF Collision Avoidance (04h)

	\def	RESPONSE_RF_COLLISION_N		
	TRF7960 Command Codes - Perform response RF Collision Avoidance (05h)

	\def	RESPONSE_RF_COLLISION_0		
	TRF7960 Command Codes - Perform response RF Collision Avoidance (n=0) (06h)

	\def	RESET		
	TRF7960 Command Codes - Reset (0Fh)

	\def	TRANSMIT_NO_CRC		
	TRF7960 Command Codes - Transmission without CRC (10h)

	\def	TRANSMIT_CRC		
	TRF7960 Command Codes - Transmission with CRC (11h)

	\def	DELAY_TRANSMIT_NO_CRC		
	TRF7960 Command Codes - Delayed Transmission without CRC (12h)

	\def	DELAY_TRANSMIT_CRC		
	TRF7960 Command Codes - Delayed Transmission with CRC (13h)

	\def	TRANSMIT_NEXT_SLOT		
	TRF7960 Command Codes - End of Frame/Transmit Next Time Slot (14h)

	\def	CLOSE_SLOT_SEQUENCE 	
	TRF7960 Command Codes - Close Slot Sequence (15h)

	\def	STOP_DECODERS 	
	TRF7960 Command Codes - Block receiver (16h)

	\def	RUN_DECODERS	
	TRF7960 Command Codes - Enable receiver (17h)

	\def	CHECK_INTERNAL_RF	
	TRF7960 Command Codes - Test internal RF (18h)

	\def	CHECK_EXTERNAL_RF	
	TRF7960 Command Codes - Test external RF (19h)

	\def	ADJUST_GAIN	
	TRF7960 Command Codes - Receiver gain adjust (1Ah)

*/
//------------------------------------------------------------------------------

#define IDLE						0x00
#define SOFT_INIT					0x03
#define INITIAL_RF_COLLISION		0x04
#define RESPONSE_RF_COLLISION_N		0x05
#define RESPONSE_RF_COLLISION_0		0x06
#define	RESET						0x0F
#define TRANSMIT_NO_CRC				0x10
#define TRANSMIT_CRC				0x11
#define DELAY_TRANSMIT_NO_CRC		0x12
#define DELAY_TRANSMIT_CRC			0x13
#define TRANSMIT_NEXT_SLOT			0x14
#define CLOSE_SLOT_SEQUENCE			0x15
#define STOP_DECODERS				0x16
#define RUN_DECODERS				0x17
#define CHECK_INTERNAL_RF			0x18
#define CHECK_EXTERNAL_RF			0x19
#define ADJUST_GAIN					0x1A


//------------------------------------------------------------------------------
/*! 	\def	IRQ_NONE		
	IrqCheck 함수 Return 값 - None status

	\def	IRQ_RX_COMPLETED		
	IrqCheck 함수 Return 값 - RX Start or RX Completed

	\def	IRQ_COLLISION_ERROR		
	IrqCheck 함수 Return 값 - Collision Error	

	\def	IRQ_ERROR		
	IrqCheck 함수 Return 값 - Etc Error
*/
//------------------------------------------------------------------------------
#define	IRQ_NONE 				0
#define	IRQ_RX_COMPLETED		1
#define	IRQ_COLLISION_ERROR		2
#define	IRQ_ERROR				3



//------------------------------------------------------------------------------
/*! 	\def	FIFO_MAX_SIZE		
	TRF7970 FIFO Write/Read를 위한 gTrfFifoDataBuf의 Size 
	127 byte까지 적용할 수 있지만 TRF7960에 맞추어 현재 12 byte로 제한 
*/
//------------------------------------------------------------------------------
#define	FIFO_MAX_SIZE		12
extern BYTE gTrfFifoDataBuf[FIFO_MAX_SIZE];

void TrfSendInventoryCommand(BYTE* WriteDataBuf, BYTE DataSize);
void TrfGetRegisterData(BYTE Register, BYTE* ReadDataBuf, BYTE DataSize);
void TrfSetRegister(BYTE Register, BYTE SetData);
void TrfSendCommand(BYTE Command);



void RfidInitial(void);
void RfidPowerOn(void);
void RfidPowerOff(void);
void RfidTxOn(void);
void RfidTxOff(void);


BYTE IrqCheck(void);
void IrqClear(void);








void Trf797xInitialSettings(void);
void RfidSpiSetOff(void);



#endif

