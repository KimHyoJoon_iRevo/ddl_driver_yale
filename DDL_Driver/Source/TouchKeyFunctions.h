#ifndef	_TOUCH_KEY_FUNCTIONS_H_

#define	_TOUCH_KEY_FUNCTIONS_H_


#include "DefineMacro.h"
//#include "DefinePin.h"

#define	I2C_TOUCHKEY_ADDRESS		0xD0

extern FLAG TKeyFLAG;
#define gfTkeyIn			TKeyFLAG.STATEFLAG.Bit0
#define gfTKeySleep		TKeyFLAG.STATEFLAG.Bit2
#define gfTKeyOutIn		TKeyFLAG.STATEFLAG.Bit3


#ifdef		_TOUCH_KEY_FUNCTIONS_C_


#else

void TouchKeyInitProc( BYTE count );
void RegisterTouchKey( ddl_i2c_t *h_i2c_dev );
void TouchKeyGotoSleep( void );
void TKeyCheckRtn(void);

extern	BYTE	gbTKeyTimer2ms;

#endif


#ifdef	DEBUG_TOUCH
void DebugTouchKeyTimeCount(void);
void DebugTouchKeySend(void);
void DebugTouchKeyAutoSend(void);
#endif


void TouchKeyCheckBeforeSleep(void);



#endif

