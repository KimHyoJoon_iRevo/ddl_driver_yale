//------------------------------------------------------------------------------
/** 	@file		FingerPFM_3000.c
	@brief	Encryption 및 uart 통신 protocl 관련 파일 
	@brief	20161020 시점에서 필리아 PFM-3000 지문 모듈과 / 중국 Biosec TS1072M 모듈의 
	@brief	공용 들라이버 로 두 모듈이 공동으로 사용 한다. 
*/
//------------------------------------------------------------------------------


#define		_FINGER_PFM_3000_C_

#include	"main.h"

FLAG BioFlag;
BYTE BioRetryCnt;
DWORD gBioEnrollCnt;

WORD gbBioTimer10ms=0;
BYTE gbFingerRetryCounter;

BYTE gbFingerPrint_txd_end;
BYTE FingerPrint_txding_point;
BYTE FingerPrint_txd_backup;
BYTE FingerPrint_txding_buf[30];

BYTE gbFingerPrint_rxd_begin;
BYTE gbFingerPrint_rxd_end;
BYTE gbFingerPrint_rxding_point;
BYTE gbFingerPrint_rxding_buf[30] ;
BYTE gbFingerPrint_rxd_length;
BYTE gbFingerPrint_rxd_buf[30];

BYTE gbEncryptionKeyBuffer[16];
BYTE gbfingerdatacounter;
BYTE gbEncryptionAccessflag;
BYTE gbFingerPrint1TimeEnrollMode;
BYTE gbCoverAccessDelaytime100ms = 0x00;
WORD gbfingerCloseDelaycnt = 0;
BYTE gbErrorCnt = 0;
BYTE gbFingerLongTimeWait100ms = 0;
BYTE gbFID=0;
BYTE gbBIOErrorCNT = 0;
BYTE gbFingerRegisterDisplayStep;
BYTE dbDisplayOn = 0x00;

WORD gbFingerPrintTouchSwitchChatteringCounter;
BYTE gbFingerPrintTouchSwitchValue = 0;
BYTE gbOldFingerPrintTouchSwitchValue = 0;
BYTE FingerPower;

uint8_t		pfm_3000_rx_byte;
uint8_t		pfm_3000_tx_byte;


const BYTE gbEncryptionKeyDefaultBuffer[16] = {
	'A','S','S','A','A','B','L','O','Y','K','R','I','r','e','v','o'
};

uint32_t	uart3_rxcplt_count;
uint32_t	uart3_txcplt_count;


void PFM_3000_uart_rxcplt_callback(UART_HandleTypeDef *huart)
{
	BYTE bTmp = 0x00;
	BYTE checksum = 0x00;
	BYTE i = 0x00;

	uart3_rxcplt_count++;

	bTmp = pfm_3000_rx_byte;

	if(gbFingerPrint_rxd_begin==0)
	{
		if(bTmp == 0x44 || bTmp == 0xE1 || bTmp == 0xE2 || bTmp == 0xEF)	//STX
		{	
			gbFingerPrint_rxd_begin = 1;
			gbFingerPrint_rxding_point = 0;
			gbFingerPrint_rxding_buf[gbFingerPrint_rxding_point++] = bTmp;
		}
	}
	else
	{
		if( gbFingerPrint_rxding_point >= 20)	//STX를 제외한 data의 길이 (예:STX가 'A'일때 프레임"A12345678"을 수신 받으면 BLE_rxding_point==8이 된다.)
		{	            
			gbFingerPrint_rxding_buf[gbFingerPrint_rxding_point++] = bTmp;
			
			checksum = 0;
			 for(i=0;i<(gbFingerPrint_rxding_point-1);i++)
		        {
		            checksum ^= gbFingerPrint_rxding_buf[i];
		        }
			if(checksum == gbFingerPrint_rxding_buf[gbFingerPrint_rxding_point-1])
			{
				for(i=0 ; i<16 ; i++)
				{		
					gbFingerPrint_rxding_buf[4+i] ^=  gbFingerPrint_rxding_buf[2];
					gbFingerPrint_rxding_buf[4+i] ^=  gbEncryptionKeyBuffer[i];
				}
				
				gbFingerPrint_rxd_end = 1;
				gbFingerPrint_rxd_begin = 0;
				gbFingerPrint_rxd_length = gbFingerPrint_rxding_point;
				memcpy(&gbFingerPrint_rxd_buf[0],&gbFingerPrint_rxding_buf[0],gbFingerPrint_rxd_length);							
				gbFingerPrint_rxding_point = 0;
			}
		}
		else
		{
			gbFingerPrint_rxding_buf[gbFingerPrint_rxding_point++] = bTmp;
		}
	}	

	//반복적인 Rx를 위해 다시 Receive 요청
	HAL_UART_Receive_IT( gh_mcu_uart_finger, &pfm_3000_rx_byte, sizeof(pfm_3000_rx_byte) );
}


void PFM_3000_uart_txcplt_callback(UART_HandleTypeDef *huart)
{
	uart3_txcplt_count++;
	if(gbFingerPrint_txd_end == 0)
	{
		if(FingerPrint_txding_point == FingerPrint_txd_backup)
		{			
			FingerPrint_txding_point=0;
			gbFingerPrint_txd_end = 1;
		}
		else
		{				
			pfm_3000_tx_byte = FingerPrint_txding_buf[FingerPrint_txding_point++];
			//UartSendPFM_3000(pfm_3000_tx_byte);  ISR 이니 아래로 바로 
			HAL_UART_Transmit_IT(gh_mcu_uart_finger,&pfm_3000_tx_byte,sizeof(pfm_3000_tx_byte));
		}
	}	
}

// PFM-3000와 통신을 위해 UART 초기화 와 Tx/Rx Complete Callback함수를 등록하고
// 처음 Rx를 위해 Receive IT를 요청한다.
void		InitialSetupFingerModule( void )
{
	RegisterPFM_3000_INT_Callback( PFM_3000_uart_txcplt_callback, PFM_3000_uart_rxcplt_callback);
}



///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////



void FPCoverSwProcess(void)
{
#if 0
	if(gbCoverAccessDelaytime100ms) return;
	if(GetVoicePrcsStep()) return;
	if(GetTamperProofPrcsStep()) return;
	if ( gfBrokenAlarm )	return;

	gbFingerPrintTouchSwitchValue = P_IBUTTON_COVER_T;

	if(gbFingerPrintTouchSwitchValue != gbOldFingerPrintTouchSwitchValue)
	{
		if(gbFingerPrintTouchSwitchChatteringCounter++ > 100)
		{
			gbFingerPrintTouchSwitchChatteringCounter = 0;
			gbOldFingerPrintTouchSwitchValue = gbFingerPrintTouchSwitchValue;
		}
	}
	else	
	{
		gbFingerPrintTouchSwitchChatteringCounter = 0;
		return;
	}

	if(gbOldFingerPrintTouchSwitchValue == 0) return;

	if(GetMainMode() == 0)
	{		
#ifdef DDL_CFG_FP_COVER_ACTIVE_HIGH	
		if(P_IBUTTON_COVER_T)
#else 
		if(!P_IBUTTON_COVER_T)
#endif 
		{
			if(GetMotorPrcsStep())		return;
			ModeGotoFingerVerify();
		}
	}
	else
	{
		if(GetMainMode() == MODE_PIN_VERIFY && gfMute == 1 && gPinInputKeyCnt == 0)
		{
			ModeGotoFingerVerify();
		}
	}
#else 
	if(!gfXORCoverSw)	return;						//Cover의 신호가 바뀌어야만 루틴 수행
	gfXORCoverSw = 0;

	if(gbCoverAccessDelaytime100ms) return;
	if(GetVoicePrcsStep() && !gfIntrusionAlarm) return; //경보 중일 경우는 열수 있게 
	if(GetTamperProofPrcsStep()) return;
	if ( gfBrokenAlarm )	return;

	if(GetMainMode() == 0)
	{		
#ifdef DDL_CFG_FP_COVER_ACTIVE_HIGH	
		if(P_IBUTTON_COVER_T)
#else 
		if(!P_IBUTTON_COVER_T)
#endif 
		{
			if(GetMotorPrcsStep())		return;
			ModeGotoFingerVerify();
		}
	}
	else
	{
		if(GetMainMode() == MODE_PIN_VERIFY && gfMute == 1 && gPinInputKeyCnt == 0)
		{
			ModeGotoFingerVerify();
		}
	}
#endif 	
}

void BioModuleON( void )
{
	P_FP_EN(1);		//	enable Finger module power
	Delay(SYS_TIMER_100MS);
	SetupPFM_3000_UART();		
	Delay(SYS_TIMER_20MS);	
	Delay(SYS_TIMER_30MS);		
	gbErrorCnt = 0;   // 재 전송을 하기  위해서 
	// 최초 UART receive complete INT가 들어오도록 호출
	HAL_UART_Receive_IT( gh_mcu_uart_finger, &pfm_3000_rx_byte, sizeof(pfm_3000_rx_byte));
	FingerPower = 1;
}



void BioModuleOff( void )
{
	if(FingerPower)
	{
	HAL_UART_DeInit( gh_mcu_uart_finger );
	//FingerModuleUartPinSetOutPutLow();
	FingerModuleUartPinSetInput();
	P_FP_EN(0);
	OffGpioPFM_3000();
#if 0
	Delay(SYS_TIMER_100MS);
	Delay(SYS_TIMER_100MS);
	Delay(SYS_TIMER_100MS);
	Delay(SYS_TIMER_100MS);
	Delay(SYS_TIMER_100MS);	
#endif 
	FingerPrint_txding_point = 0;
	FingerPrint_txd_backup = 0;
	gbFingerPrint_txd_end = 0;
	gbFingerPrint_rxd_begin = 0;
	gbFingerPrint_rxd_end = 0;
	gbFingerPrint_rxding_point = 0;
	gbFingerPrint_rxd_length = 0;	
		FingerPower = 0;
	}
}


void BioOffModeClear( void )
{
	BioModuleOff();
	ModeClear();
	gbModeChangeFlag4Finger = 0;
}


void BioReTry( void )				// 지문 모듈 이상 하면 전체 off on
{
	gbBioTimer10ms--;

	if(!P_FP_DDL_RX_T) {
		//RX가 LOW 일 경우
		HAL_UART_DeInit( gh_mcu_uart_finger );

		P_FP_EN(0);			// 파워만 조절 하여 이상 현상 제거 가능 (그러나 UART도 초기화  안전하게 가기 위해서 )
		Delay(SYS_TIMER_15MS);
		P_FP_EN(1);
		Delay(SYS_TIMER_100MS);

		SetupPFM_3000_UART();

		// 최초 UART receive complete INT가 들어오도록 호출
		HAL_UART_Receive_IT( gh_mcu_uart_finger, &pfm_3000_rx_byte, sizeof(pfm_3000_rx_byte) );
	}
}

//이전 코드에선 void FingerStatusLoad(void) 함수로 EEPROM에 저장된 지문의 수를 읽어 들인다.
BYTE GetNoFingerFromEeprom( void )
{
	BYTE	no_eeprom_key;

	RomRead(&no_eeprom_key, (WORD)KEY_NUM, 1);					// 현재 등록된 지문수 불러오기..
	return	no_eeprom_key;
}

void FingerPrintModuleCheck(void)
{
	BYTE i = 0x00;
	BYTE Checksum = 0x00;
	
	FingerPrint_txding_point = 0;
	FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x44;		//event
	FingerPrint_txding_buf[FingerPrint_txding_point++] = 0xB0;		//source
	FingerPrint_txding_buf[FingerPrint_txding_point++] = gbfingerdatacounter++;		//Counter
	FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x03;	
	
	FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x70;		//Lock Type High BYTE(None RF module)
	FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x71;		//Lock Type Low BYTE(Lock Micom (Main Micom))
	
	FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x01;		//Encryption Type(XOR)
	
		
	for(i=0;i<13;i++)
	{
		FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x00;	
	}
	
	for(i=0 ; i<20 ; i++)
	{
		Checksum ^= FingerPrint_txding_buf[i];
	}
	FingerPrint_txding_buf[FingerPrint_txding_point++] = Checksum;	

	
	FingerPrint_txd_backup = FingerPrint_txding_point;
	FingerPrint_txding_point = 0;
	gbFingerPrint_txd_end = 0;

	pfm_3000_tx_byte = FingerPrint_txding_buf[FingerPrint_txding_point++];
	HAL_UART_Transmit_IT(gh_mcu_uart_finger,&pfm_3000_tx_byte,sizeof(pfm_3000_tx_byte));
	
}

void MakeEncryptionKey(void)
{
	DWORD TempEncryptionKey;
	WORD RandSeed;
	BYTE i ;
	
	if(gbEncryptionKeyBuffer[0] == 0x00)
	{
		P_LOWBAT_EN(1);
		RandSeed = ADCCheck(ADC_BATTERY_CHECK);
		P_LOWBAT_EN(0);
		RandSeed += SystickLoad();
	}
	else 
	{
		RandSeed = SystickLoad();
	}

	srand(RandSeed);

	for(i = 0 ; i < 16 ; i++)
	{
		TempEncryptionKey = rand();
		gbEncryptionKeyBuffer[i] = (BYTE)(TempEncryptionKey & 0xFF);
	}
}

void FingerPrintTxEncryptionKeyMakeFrame(void)
{
	BYTE i = 0x00;
	BYTE Checksum = 0x00;
	
	FingerPrint_txding_point = 0;
	FingerPrint_txding_buf[FingerPrint_txding_point++] = 0xE2;		//event
	FingerPrint_txding_buf[FingerPrint_txding_point++] = 0xB0;		//source
	FingerPrint_txding_buf[FingerPrint_txding_point++] = gbfingerdatacounter++;		//Counter
	FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x10;		//16 BYTE 의 Additional Data 중 실제 사용하는 Data의 임시 길이(최종 계산을 checksum data를 만들기 전에 구성 한다)

	MakeEncryptionKey();
		
	for(i=0;i<16;i++)
	{
		FingerPrint_txding_buf[FingerPrint_txding_point++] = gbEncryptionKeyBuffer[i];
	}

	for(i=0 ; i<16 ; i++)
	{		
		FingerPrint_txding_buf[4+i] ^=  gbEncryptionKeyDefaultBuffer[i];	//Default : ASSAABLOYKRIrevo
		FingerPrint_txding_buf[4+i] ^=  FingerPrint_txding_buf[2];//gbfingerdatacounter;
	}
	
	for(i=0 ; i<20 ; i++)
	{
		Checksum ^= FingerPrint_txding_buf[i];
	}
	FingerPrint_txding_buf[FingerPrint_txding_point++] = Checksum;	

	
	FingerPrint_txd_backup = FingerPrint_txding_point;
	FingerPrint_txding_point=0;
	gbFingerPrint_txd_end = 0;
	
	pfm_3000_tx_byte = FingerPrint_txding_buf[FingerPrint_txding_point++];
	HAL_UART_Transmit_IT(gh_mcu_uart_finger,&pfm_3000_tx_byte,sizeof(pfm_3000_tx_byte));
}

void FingerPrintTxEncryptionDataMakeFrame(BYTE source , BYTE command , BYTE data , BYTE subdata)
{
	BYTE i = 0x00;
	BYTE Checksum = 0x00;

	
	FingerPrint_txding_point = 0;
	gbEncryptionAccessflag = 0xCC;
	if(gbEncryptionAccessflag == 0xCC)		FingerPrint_txding_buf[FingerPrint_txding_point++] = ENCRYPTED_MESSAGE;		//event
	else								FingerPrint_txding_buf[FingerPrint_txding_point++] = NON_ENCRYPTED_MESSAGE;		//event
	FingerPrint_txding_buf[FingerPrint_txding_point++] = source;		//source
	FingerPrint_txding_buf[FingerPrint_txding_point++] = gbfingerdatacounter++;		//Counter
	FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x01;		//16 BYTE 의 Additional Data 중 실제 사용하는 Data의 임시 길이(최종 계산을 checksum data를 만들기 전에 구성 한다)
	FingerPrint_txding_buf[FingerPrint_txding_point++] = command;

	if(gbfingerdatacounter == 0)
		gbfingerdatacounter = 1;
	
	switch(command)
	{
		case CMD_ENROLL : 
			//지문을 3 회 스캔 한 후 template 을 등록 한다.
			//(Busy 상태에서 Cancel 명령 사용 가능, 3 회 Scan 완료 후 150mS~250mS 후 결과 전송)
			FingerPrint_txding_buf[FingerPrint_txding_point++] = data;	//저장될 slot 번호 (1~128) 0 일 경우 1 번부터 비어있는 slot 에 자동 저장
			FingerPrint_txding_buf[FingerPrint_txding_point++] = subdata;	//dummy
			break;
	                
		case CMD_VERIFY : 
			//지문을 1 회 스캔 한 후 등록여부를 확인한다.
			//(Busy 상태에서 Cancel 명령 사용 가능)
			FingerPrint_txding_buf[FingerPrint_txding_point++] = data;	//인증 할 slot 번호 (1~128) 0 일 경우 모든 slot 의 template 과 비교
			FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x00;	//dummy
			break; 
	                
		case CMD_DELETE :
			//등록된 slot 의(Start Slot ~ End Slot) 지문 template 을 삭제한다.
			//등록 및 승인중에는 삭제 되지 않고 응답(Output Packet)도 없음.
			FingerPrint_txding_buf[FingerPrint_txding_point++] = data;	//삭제 시작할 slot 번호 (1~128)0 일 경우 전체 삭제
			FingerPrint_txding_buf[FingerPrint_txding_point++] = subdata;	//삭제할 마지막 slot 번호 ( >=StartSlot )
			break;
			
		case CMD_PRODUCT_SN : 
			FingerPrint_txding_buf[FingerPrint_txding_point++] = data;
			FingerPrint_txding_buf[FingerPrint_txding_point++] = subdata;
			break;
			
		case CMD_SETSECURITY_LEVEL : 	//보안 등급을 확인/조절한다.
			//------------Verify------------//
			//SECURLEVEL1_VALUE = 20
			//SECURLEVEL2_VALUE = 30
			//SECURLEVEL3_VALUE = 40 <- Default ...
			//SECURLEVEL4_VALUE = 50
			//SECURLEVEL5_VALUE = 60
			//----------Enrollment----------//
			//SECURLEVEL1_VALUE = 20
			//SECURLEVEL2_VALUE = 30
			//SECURLEVEL3_VALUE = 40 <- Default ...
			//SECURLEVEL4_VALUE = 50
			//SECURLEVEL5_VALUE = 60
			FingerPrint_txding_buf[FingerPrint_txding_point++] = data;	//보안 등급 (0) : 현재의 보안 등급을 확인한다.보안 등급 (1~5)1 : 최저, 2 : 중하, 3 : 중간, 4 : 중상, 5 : 최고(보안등급 설정에 따른 보안 문제나 편의성 문제가 발생 될 수 있음)
			FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x00;	//dummy
			break;

		case CMD_LISTALL :			//등록된 모든 지문 Template list 를 보고한다.
		case CMD_STATUS :				//PFM-3000 의 상태정보를 요청한다. Busy Status 는 Busy 상태 일 경우 해당 Command Tag (high nibble) + Scan Code(0,1,2,3 . low nibble)로 구성 된다.
		case CMD_GET_RESULT :			//지문 등록이나, 인증의 결과를 요청한다.
		case CMD_CANCEL :			//Enrollment , Verify command에 의해 사용자 입력으로 Status 가 Busy 가 된 경우 이를 Cancel 시킨다.
		case CMD_GET_VERSION :		//H/W 버전 및 F/W 버전을 요청한다 
		case CMD_PRODUCT_SN_READ : 	//Product Serial ID 를 Read 를 요청 한다. Serial ID0, ID1 값이 0, 0 이면 Read 수행, ID 2 개중 1 개라도 값이 있으면 Write 수행
		default : 
			FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x00;
			FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x00;
			break;
	} 
	
	FingerPrint_txding_buf[3] = (FingerPrint_txding_point-4);		//16 BYTE 의 Additional Data 중 실제 사용하는 Data의 실재 길이
	for(i=0;i<(16-FingerPrint_txding_buf[3]);i++)
	{
		FingerPrint_txding_buf[FingerPrint_txding_point++] = 0x00;
	}


	for(i=0 ; i<16 ; i++)
	{		
		FingerPrint_txding_buf[4+i] ^=  gbEncryptionKeyBuffer[i];
		FingerPrint_txding_buf[4+i] ^=  FingerPrint_txding_buf[2];//gbfingerdatacounter;
	}

	for(i=0 ; i<20 ; i++)
	{
		Checksum ^= FingerPrint_txding_buf[i];
	}
	FingerPrint_txding_buf[FingerPrint_txding_point++] = Checksum;	

	
	FingerPrint_txd_backup = FingerPrint_txding_point;
	FingerPrint_txding_point=0;
	gbFingerPrint_txd_end = 0;

	pfm_3000_tx_byte = FingerPrint_txding_buf[FingerPrint_txding_point++];
	HAL_UART_Transmit_IT(gh_mcu_uart_finger,&pfm_3000_tx_byte,sizeof(pfm_3000_tx_byte));
}


void BioCommCheck(BYTE bsave)
{
	BYTE	no_eeprom_key;
	FingerPower = 1;
	BioModuleOff();
	Delay(SYS_TIMER_100MS);
	//POWER ON, Uart On
	BioModuleON();

	if(!P_FP_DDL_RX_T) {
		//RX가 LOW 일 경우
		gfBioErr = 1;
		BioModuleOff();
		return;
	}

	FingerPrintModuleCheck();
	gbBioTimer10ms = 200;

	while(1)
	{
		gbBioTimer10ms--;
		if(gbBioTimer10ms==0){
			gfBioErr = 1;
			BioModuleOff();
			return;
		}
		
		Delay(SYS_TIMER_1MS);

		if(gbFingerPrint_rxd_end)
		{
			gbFingerPrint_rxd_end = 0;
			if(gbFingerPrint_rxd_buf[0] == 0x44 &&  gbFingerPrint_rxd_buf[1] == 0xA1)
			{
				FingerPrintTxEncryptionKeyMakeFrame();
				gbBioTimer10ms = 200;
				break;
			}
		}
	}

#ifdef	DDL_CFG_WATCHDOG
		RefreshIwdg();
#endif

	while(1)
	{
		gbBioTimer10ms--;
		if(gbBioTimer10ms==0){
			gfBioErr = 1;
			BioModuleOff();
			return;
		}

		Delay(SYS_TIMER_1MS);

		if(gbFingerPrint_rxd_end)
		{
			gbFingerPrint_rxd_end = 0;
			if(gbFingerPrint_rxd_buf[0] == 0xE2 &&  gbFingerPrint_rxd_buf[1] == 0xA1)
			{
				FingerPrintTxEncryptionDataMakeFrame(FINGER_SOURCE_LOCK_EVENT,CMD_LISTALL,NO_FINGER_DATA,NO_FINGER_DATA);	
				gbBioTimer10ms = 100;
				break;
			}
		}
	}

#ifdef	DDL_CFG_WATCHDOG
		RefreshIwdg();
#endif

	no_eeprom_key = GetNoFingerFromEeprom();

	while(1)
	{
		gbBioTimer10ms--;
		if(gbBioTimer10ms==0){
			gfBioErr = 1;
			BioModuleOff();
			break;
		}	
		Delay(SYS_TIMER_1MS);
		
		if(gbFingerPrint_rxd_end)
		{
			gbFingerPrint_rxd_end = 0;
			if(gbFingerPrint_rxd_buf[4] == 0x84)
			{
				switch(gbFingerPrint_rxd_buf[5])
				{
					case 0x00:	//명령 성공 
						if(no_eeprom_key != gbFingerPrint_rxd_buf[6])
						{
							no_eeprom_key = gbFingerPrint_rxd_buf[6];
							RomWrite(&no_eeprom_key, (WORD)KEY_NUM, 1);

							if(gbManageMode != _AD_MODE_SET)
							{
								BYTE bTemp[2] = {0x00,0x00};
								for(BYTE i = 0x00 ; i < no_eeprom_key; i++)
								{
									bTemp[0]++;
									bTemp[1]++;
									RomWrite(bTemp, (WORD)BIO_ID+(2*i), 2);
								}
							}
							gfBioErr = 0;
							BioModuleOff();
							return;
						}	
						else
						{
#ifdef	DDL_CFG_WATCHDOG
							RefreshIwdg();
#endif
							gfBioErr = 0;
							BioModuleOff();
							return;
						}
						break;		
						
					case 0x01:	//전송 패킷 오류
					case 0x02:	//Check sum 오류	
					case 0x1E:	//BUSY 상태면 Cancer명령어를 송신 하고 다시 인증을 시도함						
					case 0x1F:	//명령 실패	
						gfBioErr = 1;
						BioModuleOff();
						return;
						break;
				}
			}				
		}
	}
}

void FingerPrintRetry(void)
{
	if(gbFingerRetryCounter < 3)
	{
		BioModuleOff();
		Delay(SYS_TIMER_100MS);
		BioModuleON();
		gbFingerRetryCounter++;
	}
	else
	{
		gbModePrcsStep = 200;	//지문인증 모듈이 정상적으로 작동되지 않습니다
	}
}
void FingerPrintRegisterDisplay(void)
{
	if(gbFingerLongTimeWait100ms >= 5)	
	{
		if(dbDisplayOn == 0x00)
		{
			LedSetting(gcbLedAREAFMREG, LED_KEEP_DISPLAY);
			dbDisplayOn = 0x01;
		}
	}
	else if(gbFingerLongTimeWait100ms < 5)
	{ 
		if(dbDisplayOn == 0x01)
		{
			LedSetting(&gcbLedAREAFMREGSTEP[gbFingerRegisterDisplayStep][0], LED_KEEP_DISPLAY);
			dbDisplayOn = 0x00;
		}

		if(gbFingerLongTimeWait100ms == 0) 
			gbFingerLongTimeWait100ms = 10;
	}
}



