#ifndef		_FINGER_PFM_3000_H_
#define		_FINGER_PFM_3000_H_

#include "DefineMacro.h"
//#include "DefinePin.h"

#define	ENCRYPTED_MESSAGE				0xE1
#define	ENCRYPTED_KEY_EXCHANGE		0xE2
#define	NON_ENCRYPTED_MESSAGE		0xEF
#define	FINGER_PRINT_MODULE_CHECK		0x44
#define	FINGER_SOURCE_MODULE_EVENT	0xA0
#define	FINGER_SOURCE_MODULE_ACK		0xA1
#define	FINGER_SOURCE_MODULE_NACK	0xA2
#define	FINGER_SOURCE_LOCK_EVENT		0xB0
#define	FINGER_SOURCE_LOCK_ACK		0xB1
#define	FINGER_SOURCE_LOCK_NACK		0xB2

#define	CMD_NOTHING			0x00
#define	CMD_ENROLL				0x01
#define	CMD_VERIFY				0x02
#define	CMD_DELETE				0x03
#define	CMD_LISTALL				0x04
#define	CMD_MOVE				0x05
#define	CMD_SETSECURITY_LEVEL	0x06
#define	CMD_STATUS				0x07
#define	CMD_GET_RESULT			0x08
#define	CMD_CANCEL				0x09
#define	CMD_GET_VERSION		0x0A

#define	CMD_PRODUCT_SN		0x0B
#define	CMD_PRODUCT_SN_READ	0x91
#define	CMD_UART_LED 			0x0D

#define	NO_FINGER_DATA			0x00
#define	FINGER_ALL_REGISTER	0x00
#define	FINGER_ALL_VERIFY	0x00

#define SETSECURITY_LEVEL_VALUE_1	0x01		
#define SETSECURITY_LEVEL_VALUE_2	0x02		
#define SETSECURITY_LEVEL_VALUE_3	0x03		
#define SETSECURITY_LEVEL_VALUE_4	0x04		
#define SETSECURITY_LEVEL_VALUE_5	0x05		

#define BIO_WAKEUP_TM		250

extern  FLAG BioFlag;

#define gfModuleOK		BioFlag.STATEFLAG.Bit0						//정상적인 모듈
#define gfUART0TxEnd	BioFlag.STATEFLAG.Bit1
#define gfUART0RxEnd	BioFlag.STATEFLAG.Bit2
#define gfBioCoverIn		BioFlag.STATEFLAG.Bit3
#define gfBioCoverDown	BioFlag.STATEFLAG.Bit4
#define gfBioUARTST		BioFlag.STATEFLAG.Bit5
#define gfBioUARTStuff	BioFlag.STATEFLAG.Bit6
#define gfBioTXStuff		BioFlag.STATEFLAG.Bit7

#define FINGER_COVER_OPENED				1
#define FINGER_COVER_CLOSED				2
#define FINGER_COVER_CLOSED_ERROR		3

extern BYTE BioRetryCnt;
extern DWORD gBioEnrollCnt;
extern WORD gbBioTimer10ms;
extern BYTE gbFingerRetryCounter;

extern BYTE gbFingerPrint_txd_end;
extern BYTE FingerPrint_txding_point;
extern BYTE FingerPrint_txd_backup;
extern BYTE FingerPrint_txding_buf[30];

extern BYTE gbFingerPrint_rxd_begin;
extern BYTE gbFingerPrint_rxd_end;
extern BYTE gbFingerPrint_rxding_point;
extern BYTE gbFingerPrint_rxding_buf[30] ;
extern BYTE gbFingerPrint_rxd_length;
extern BYTE gbFingerPrint_rxd_buf[30];
extern BYTE gbEncryptionKeyBuffer[16];
extern BYTE gbfingerdatacounter;
extern BYTE gbEncryptionAccessflag;
extern BYTE gbFingerPrint1TimeEnrollMode;
extern BYTE gbCoverAccessDelaytime100ms;
extern WORD gbfingerCloseDelaycnt;
extern BYTE gbErrorCnt;
extern BYTE gbFingerLongTimeWait100ms;
extern BYTE gbFID;
extern BYTE gbBIOErrorCNT;
extern BYTE gbFingerRegisterDisplayStep;
extern BYTE dbDisplayOn;

void BioModuleON( void );
void BioModuleOff( void );
void BioOffModeClear( void );
void BioReTry( void );
BYTE GetNoFingerFromEeprom( void );
void InitialSetupFingerModule( void );
void FingerPrintModuleCheck(void);
void FPCoverSwProcess(void);
void FingerPrintTxEncryptionKeyMakeFrame(void);
void FingerPrintTxEncryptionDataMakeFrame(BYTE source , BYTE command , BYTE data , BYTE subdata);
void MakeEncryptionKey(void);
void BioCommCheck(BYTE bsave);
void FingerPrintRetry(void);
void FingerPrintRegisterDisplay(void);

#endif

