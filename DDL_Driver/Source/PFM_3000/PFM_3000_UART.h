//------------------------------------------------------------------------------
/** 	@file	PFM_3000_UART.h
	@brief	UART functions for PFM_3000_UART communication
*/
//------------------------------------------------------------------------------


#ifndef		_PFM_3000_UART_H_
#define		_PFM_3000_UART_H_

#include "DefineMacro.h"
//nclude "DefinePin.h"


#define		MCU_PFM_3000_UART		USART3

void	SetupPFM_3000_UART( void );
void 	FingerModuleUartPinSetOutPutLow(void);
void 	FingerModuleUartPinSetInput(void);
void 	OffGpioPFM_3000(void);
void	RegisterPFM_3000_INT_Callback( void (*uart_txcplt_callback)(UART_HandleTypeDef *huart), void (*uart_rxcplt_callback)(UART_HandleTypeDef *huart) );
void	UartSendPFM_3000( BYTE data );

#endif

