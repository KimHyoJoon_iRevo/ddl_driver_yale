//------------------------------------------------------------------------------
/** 	@file		AlarmFunctions_T1.h
	@brief	Alarm Process (Intrusion, Broken, High Temperature)
*/
//------------------------------------------------------------------------------
#ifndef __ALARMFUNCTIONS_T1_INCLUDED
#define __ALARMFUNCTIONS_T1_INCLUDED

#include "DefineMacro.h"
//#include "DefinePin.h"


//------------------------------------------------------------------------------
/*! 	\def	ALARM_RUNNING_TIME		
	gAlarmOnTimer1s에서 참조- 경보음 발생 시간, 30분 설정
*/
//------------------------------------------------------------------------------
#define	ALARM_RUNNING_TIME			1800

extern WORD gAbnormalOnTimer1s;

//------------------------------------------------------------------------------
// gAlarmPrcsStep에서 참조 - 자동 잠김 수행 단계
//------------------------------------------------------------------------------
enum{
	ALARMPRCS_INTRUSION_SET = 1,
	ALARMPRCS_INTRUSION_RUN,
	ALARMPRCS_HIGH_TEMPERATURE_SET,
	ALARMPRCS_HIGH_TEMPERATURE_RUN,
	ALARMPRCS_BROKEN_SET,
	ALARMPRCS_BROKEN_RUN,
	ALARMPRCS_INNER_LOCKOUT_WARNING_SET,
#ifdef DDL_CFG_EMERGENCY_119
	ALARMPRCS_EMERGENCY_119_ALARM_SET,
	ALARMPRCS_EMERGENCY_119_ALARM_RUN,		
#endif 
#ifdef P_KEY_INTER_CL_T
	ALARMPRCS_KEY_INTER_CLYNDER_SET,
	ALARMPRCS_KEY_INTER_CLYNDER_RUN,	
#endif 
	ALARMPRCS_ALARM_CLEAR,
	ALARMPRCS_ALARM_CLEAR_NO_FEEDBACK
	
};	


void AlarmOnTimeCounter(void);

void AlarmGotoIntrusionAlarm(void);
void AlarmGotoHighTempAlarm(void);
void AlarmGotoBrokenAlarm(void);
void AlarmGotoInnerLockOutWarning(void);
#ifdef DDL_CFG_EMERGENCY_119
void AlarmGotoEmergency119();
#endif 
void AlarmGotoAlarmClear(void);
void AlarmGotoAlarmClearNoFeedback(void);
BYTE AlarmStatusCheck(void);
void AlarmProcess(void);


WORD TempSensorCheck(void);

#ifdef		DDL_CFG_HIGHTEMP_SENSOR
void TempSensorCheckCounter(void);
void TempSensorProcess(void);
#endif

void AlarmGotoAbnormalAlarm(void);
void ModeAbnormalProcess(void);

#endif

