#ifndef __MEMORYMAP_T2_INCLUDED
#define __MEMORYMAP_T2_INCLUDED


/*
Version : 1.2.00
ST 내부 EEPROM(8K)에 맞게 설계
신규 UI에 맞게 설계 
iRevo BLE for CBA(Connected By August)에 맞게 설계
User Card : 100개 
User Code : 100개
User Fingerprint : 100개 
Audit Data 저장하지 않음
*/

#include "DefineMacro.h"
//#include "DefinePin.h"


#define	SUPPORTED_USERCODE_NUMBER		100
#define	SUPPORTED_USERCARD_NUMBER		100
#define	SUPPORTED_USERKEY_NUMBER			40
#define	SUPPORTED_DS1972KEY_NUMBER		20

#ifdef 	DDL_CFG_FP
#define	SUPPORTED_FINGERPRINT_NUMBER		_MAX_REG_BIO  
#else
#define	SUPPORTED_FINGERPRINT_NUMBER		0  
#endif

//------------------------------------------------------------------------------
//	EEPROM MEMORY MAP
//------------------------------------------------------------------------------
// 공용 사용 영역 (0x000 ~ 0x02F) - 임의 변경 금지
//------------------------------------------------------------------------------
/*! 	\def	ROM_TEST		
	Serial EEPROM 통신 테스트에 사용되는 내용을 저장	1 byte * 1
	
	\def	DDL_STATE		
	Doorlock Status Flag 내용 저장					1 byte * 1

	\def	WRONG_NUM		
	3분락 진입을 위한 인증 시도 실패 회수 저장			1 byte * 1

	\def	KEY_NUM 	
	카드, iButton, 지문 등의 인증 수단의 등록된 개수 저장	1 byte * 1

	\def	VOICE_STATE 	
	Volume 설정 상태 저장 (고음, 저음, 무음 등) 			1 byte * 1

	\def	RIGHT_LEFT		
		좌수/우수 설정 상태 저장						1 byte * 1

	\def	INLOCK_STATE		
	내부강제잠금 설정 상태 저장						1 byte * 1

	\def	LANG_MODE		
	음성 안내 언어 설정 상태 저장						1 byte * 1
	상위 니블은 default 값 하위 니블은 현재 설정값을 저장 한다
	0x13 이면 한국어 default 에 현재 영어 상태임 

	\def	WORKING_MODE		
	동작 모드 저장 (Easy Mode, Advanced Mode 등)		1 byte * 1

	\def	FACTORY_RST		
	Factory Reset 진행 내용 저장						1 byte * 1

	\def	MANAGE_MODE 	
	Normal Mode/Advanced Mode 상태 저장			1 byte * 1

	\def	OCBUTTON_MODE 	
	이중 열림/닫힘 버튼 설정 상태 저장 				1 byte * 1

	\def	ALL_LOCK_OUT 	
	전체 LOCKOUT 설정 상태 저장					1 byte * 1

	\def	OPEN_UID_EN 	
	카드 제품에서 Open UID 기능 지원 여부 내용 저장		1 byte * 1

	\def	OTA_FROM 	
	OTA요청이 어떤 Port 로 부터 발생 했는지 저장 		1 byte * 1

	\def	CBA_SET	
	CBA 장치가 등록될 경우 상태를 저장			1 byte * 1
*/	
//---------------------------------------------------------------------------
#define	ROM_TEST	0x00						
#define	DDL_STATE	(ROM_TEST+1)
#define	WRONG_NUM	(ROM_TEST+2)					 	
#define	KEY_NUM		(ROM_TEST+3)	
#define	VOICE_STATE	(ROM_TEST+4)	
#define	RIGHT_LEFT	(ROM_TEST+5) 
#define	INLOCK_STATE	(ROM_TEST+6)	
#define	LANG_MODE	(ROM_TEST+7)	
#define	WORKING_MODE	(ROM_TEST+8)	
#define	FACTORY_RST	(ROM_TEST+9)
#define	MANAGE_MODE	(ROM_TEST+10)
#define	OCBUTTON_MODE	(ROM_TEST+11)
#define	ALL_LOCK_OUT	(ROM_TEST+12)
#define	OPEN_UID_EN	(ROM_TEST+13)
#define	OTA_FROM	(ROM_TEST+14)
#define	CBA_SET		(ROM_TEST+15)

//------------------------------------------------------------------------------
/*! 	\def	PACK_ID		
	통신팩 ID 저장
	4 byte * 1
*/
//---------------------------------------------------------------------------
#define	PACK_ID			0x010

//------------------------------------------------------------------------------
/*! 	\def	INNER_PACK_ID		
	통신팩 ID 저장
	4 byte * 1
*/
//---------------------------------------------------------------------------
#define	INNER_PACK_ID	0x014

//------------------------------------------------------------------------------
/*! 	\def	MP_HISTORY		
	통신팩 ID 저장
	1 byte * 5
	MP_HISTORY_COUNTER : 총 초기화한 count 저장 
	MP_HISTORY_COUNTER+1~4 : 초기화한 수단 (종류) 저장 
	
*/
//---------------------------------------------------------------------------
#define	MP_HISTORY_COUNTER			0x18


/**************************************************/
/************* 0x1D , 0x1E , 0x1F reserved **************/
/**************************************************/


//------------------------------------------------------------------------------
/*! 	\def	DOORLOCK_ID		
	도어록 ID 저장
	16 byte * 1
*/
//---------------------------------------------------------------------------
#define	DOORLOCK_ID			0x020


/**************************************************/
/**************** 0x30 ~ 0x37 reserved ****************/
/**************************************************/



//------------------------------------------------------------------------------
/*! 	\def	ENCRYPT_KEY		
	HFPM_MODULE_ID 값 저장 
	16 byte 
*/
//---------------------------------------------------------------------------
#define	HFPM_MODULE_ID	0x38


//------------------------------------------------------------------------------
/*! 	\def	COM_PACK_ENCRYPT_KEY		
	uart1 comm pack key 
	16 byte 
*/
//---------------------------------------------------------------------------
#define	COM_PACK_ENCRYPT_KEY		0x48 

//------------------------------------------------------------------------------
/*! 	\def	INNER_PACK_ENCRYPT_KEY		
	uart2 inner pack key 
	16 byte 
*/
//---------------------------------------------------------------------------
#define	INNER_PACK_ENCRYPT_KEY	0x58


//------------------------------------------------------------------------------
/*! 	\def	USER_SCH_STA		
	User Code의 Schedule 상태 저장 (Schedule Enale/Disable 등)
	1 byte * 30
*/
//---------------------------------------------------------------------------
#define USER_SCH_STA	0x068


/**************************************************/
/**************** 0x86 , 0x87  reserved ****************/
/**************************************************/


//------------------------------------------------------------------------------
/*! 	\def	USER_SCHEDULE		
	User Code의 Schedule 저장 (4byte 단위)
	4 byte * 30
*/
//---------------------------------------------------------------------------
#define USER_SCHEDULE	0x088


//------------------------------------------------------------------------------
/*! 	\def	USER_STATUS		
	User Code의 사용 상태 저장 (Available, Occupied & Enable, Occupied & Disable 등)
	1 byte * 100
*/
//---------------------------------------------------------------------------
#define USER_STATUS		0x100



/*****************************************************************/
/**************** 0x164 , 0x165 , 0x166 , 0x167  reserved ****************/
/*****************************************************************/



//------------------------------------------------------------------------------
/*! 	\def	ONETIME_CODE		
	One Time Code 저장 (8byte 단위)
	8 byte * 1
*/
//---------------------------------------------------------------------------
#define ONETIME_CODE	0x168

//------------------------------------------------------------------------------
/*! 	\def	MASTER_CODE		
	Master Code 저장 (8byte 단위)
	8 byte * 1
*/
//---------------------------------------------------------------------------
#define MASTER_CODE		0x170


//------------------------------------------------------------------------------
/*! 	\def	USER_CODE		
	User Code 저장 (8byte 단위)
	8 byte * 100

	USER_CODE1                                     0x178
	USER_CODE2 / VISITOR CODE          0x180
	USER_CODE3                                     0x188 ~ USER_CODE100 0x490
*/
//---------------------------------------------------------------------------
#define USER_CODE		0x178

//------------------------------------------------------------------------------
/*! 	\def	CARD_UID		
	Card UID 저장 (8byte 단위)
	8 byte * 100

	CARD_UID1            0x498
	CARD_UID2            0x4A0  ~ CARD_UID100 0x7B0
*/
//---------------------------------------------------------------------------
#define CARD_UID		0x498

//------------------------------------------------------------------------------
/*! 	\def	BIO_ID		
	Fingerprint ID 저장 (2byte 단위)
	2 byte * 100

	BIO_UID1               0x7B8
	BIO_UID2               0x7BA ~ BIO_ID100 0x87E
*/
//---------------------------------------------------------------------------
#define	BIO_ID			0x7B8




//------------------------------------------------------------------------------
/*! 	\def	SCHEDULE_YEAR_DAY_PIN		
	13 byte * 100
	SCHEDULE_YEAR_DAY_PIN1		0x880
	SCHEDULE_YEAR_DAY_PIN2		0x88D ~ SCHEDULE_YEAR_DAY_PIN100 0xD87
*/
//---------------------------------------------------------------------------
#define	SCHEDULE_YEAR_DAY_PIN		0x880


//------------------------------------------------------------------------------
/*! 	\def	SCHEDULE_DAILY_REPEAT_PIN		
	6 byte * 100
	SCHEDULE_DAILY_REPEAT_PIN1		0xD98
	SCHEDULE_DAILY_REPEAT_PIN2		0xD9E ~ SCHEDULE_YEAR_DAY_PIN100 0xFEA
*/
//---------------------------------------------------------------------------
#define	SCHEDULE_DAILY_REPEAT_PIN	0xD98


//------------------------------------------------------------------------------
/*! 	\def	SCHEDULE_YEAR_DAY_CARD		
	13 byte * 100
	SCHEDULE_YEAR_DAY_CARD1		0xFF0
	SCHEDULE_YEAR_DAY_CARD2		0xFFD ~ SCHEDULE_YEAR_DAY_CARD100   0x14F7
*/
//---------------------------------------------------------------------------
#define	SCHEDULE_YEAR_DAY_CARD		0xFF0


//------------------------------------------------------------------------------
/*! 	\def	SCHEDULE_DAILY_REPEAT_CARD		
	6 byte * 100
	SCHEDULE_DAILY_REPEAT_CARD1		0x1508
	SCHEDULE_DAILY_REPEAT_CARD2		0x150E ~ SCHEDULE_DAILY_REPEAT_CARD100  0x175A
*/
//---------------------------------------------------------------------------
#define	SCHEDULE_DAILY_REPEAT_CARD	0x1508


//------------------------------------------------------------------------------
/*! 	\def	SCHEDULE_YEAR_DAY_FINGERPRINT		
	13 byte * 100
	SCHEDULE_YEAR_DAY_FINGERPRINT1		0x1760
	SCHEDULE_YEAR_DAY_FINGERPRINT2		0x176D ~ SCHEDULE_YEAR_DAY_FINGERPRINT100  0x1C67
*/
//---------------------------------------------------------------------------
#define	SCHEDULE_YEAR_DAY_FINGERPRINT		0x1760

//------------------------------------------------------------------------------
/*! 	\def	SCHEDULE_DAILY_REPEAT_FINGERPRINT		
	6 byte * 100
	SCHEDULE_DAILY_REPEAT_FINGERPRINT		0x1C80
	SCHEDULE_DAILY_REPEAT_FINGERPRINT		0x1C86 ~ SCHEDULE_DAILY_REPEAT_FINGERPRINT  0x1ED2
*/
//---------------------------------------------------------------------------

#define	SCHEDULE_DAILY_REPEAT_FINGERPRINT		0x1C80


/*****************************************************************/
/******************** 0x1EE0 ~  0x1FC7  reserved *********************/
/*****************************************************************/
#ifdef	__DDL_MODEL_PANPAN_FC2A_P
#define	CYLINDER_KEY_ALARM_SET	0x1EE8
#endif

#define	PRODUCT_TYPE_ID			0x1FC8
#define	PRODUCT_ID				0x1FCA
#define	PRODUCT_ID_ENABLE		0x1FCC
#define	VOICE_OPTION			0x1FCD


#define	RELOCK_TIME				0x1FD0
#define	WRONG_CODE_ENTRY_LIMIT	(RELOCK_TIME+1)
#define	SHUT_DOWN_TIME			(RELOCK_TIME+2)
#define	OPERATING_MODE			(RELOCK_TIME+3)
#define	ONE_TOUCH_LOCKING			(RELOCK_TIME+4)
#define	PRIVACY_BUTTON				(RELOCK_TIME+5)
#define	LOCK_STATUS_LED			(RELOCK_TIME+6)


/*****************************************************************/
/********************* 0x1FD6 ,  0x1FD7  reserved *********************/
/*****************************************************************/


//------------------------------------------------------------------------------
/*! 	\def	SILENT_RESET		
	SILENT_RESET 
	gwLockModeStatus 내용 저장 
	24 byte

	AbnormalStatus
	4 byte gwLockModeStatus 저장 
	gbLockInfo[0] = gwLockModeStatus LSB
	gbLockInfo[1] = gwLockModeStatus 
	gbLockInfo[2] = gwLockModeStatus 
	gbLockInfo[3] = gwLockModeStatus MSB

	gbLockInfo[4] = Reset 전 MainMode 값 
	gbLockInfo[5] = Reset 전 gbModePrcsStep 값 	
	gbLockInfo[6] = Reset 전 GetCardMode() 값 
	gbLockInfo[7] = Reset 전 gCardProcessStep 값

	gbLockInfo[8] = uart1 Pack Connection Mode 
	gbLockInfo[9] = uart1 Pack Tx mode
	gbLockInfo[10] = uart1 Pack Rx mode

	gbLockInfo[11] = uart2 Pack Connection Mode
	gbLockInfo[12] = uart2 Pack Tx mode
	gbLockInfo[13] = uart2 Pack Rx mode
	
	gbLockInfo[14] = 마지막 GPIO interrupt PIN number LSB
	gbLockInfo[15] = 마지막 GPIO interrupt PIN number MSB	
	gbLockInfo[16] = SYSTEM_ERROR_FLAG
	gbLockInfo[17] = DDL_STATEFLAG	
	gbLockInfo[18] = 마지막 key 값 		
	gbLockInfo[19] = 마지막 switch 값 

	RESERVED 
	
	gbLockInfo[22] = Reset Count	
	gbLockInfo[23] = Silent Reset Flag 	
*/
//---------------------------------------------------------------------------
#define	LOCK_MODE_STATUS		0x1FD8


//------------------------------------------------------------------------------
/*! 	\def	MODEL_IDENTIFIER_STRING		
	16 byte * 1
*/
//---------------------------------------------------------------------------
#define	MODEL_IDENTIFIER_STRING	0x1FF0



#if (EX_EEPROM == 0x80000000)
//------------------------------------------------------------------------------
/*
	I2C Ex EEPROM 512k bit
*/
//---------------------------------------------------------------------------

#define	EX_ROM_TEST	(0x00 | EX_EEPROM)

//------------------------------------------------------------------------------
/*! 	\def	LOG_INDEX		
	log index 저장 
	2 byte 
*/
//---------------------------------------------------------------------------
#define	LOG_INDEX	(0x01 | EX_EEPROM)

//------------------------------------------------------------------------------
/*! 	\def	LOG_DATA		
	log data 저장 
	10 byte * 500 개 
*/
//---------------------------------------------------------------------------
#define	LOG_DATA_START		(0x03 | EX_EEPROM)
#define	LOG_DATA_END		(((0x03 | EX_EEPROM)+10*500)-1)


#endif 
#endif
