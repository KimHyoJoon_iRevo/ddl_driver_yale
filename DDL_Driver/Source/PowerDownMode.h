#ifndef __POWERDOWNMODE_INCLUDED
#define __POWERDOWNMODE_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


__weak	uint32_t		StopModeBeforeCallback( void );
__weak	void		StopModePrepareCallback( void );
__weak	void		StopModeAfterCallback( void );
void		StopMode(void);



#endif


