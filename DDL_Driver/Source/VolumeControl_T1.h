//------------------------------------------------------------------------------
/** 	\file		VolumeControl_T1.h
	@date	2016.03.31
	@brief	Programmable Doorlock Volume Control Header
*/
//------------------------------------------------------------------------------


#ifndef __VOLUMECONTROL_T1_INCLUDED
#define __VOLUMECONTROL_T1_INCLUDED


#include "DefineMacro.h"
//#include "DefinePin.h"


void VolumeSettingLoad(void);
void VolumeSetting(BYTE bVal);
BYTE VolumeSettingCheck(void);

//------------------------------------------------------------------------------
/*! 	\def	MANNER_CHK		
	Followed by Silent Mode set - 무음 처리를 할 것인지에 대한 결정.		

	\def	VOL_CHECK		
	Followed by Volume Mode set - 볼륨 설정에 따라 처리 결정.

	\def	VOL_HIGH		
	Volume High Mode

*/
//------------------------------------------------------------------------------

#define	MANNER_CHK			0x01				// Followed by Silent Mode set	
#define	VOL_CHECK			0x02				// Followed by Volume Mode set
#define	VOL_HIGH			0x04				// Volume High Mode
#define	VOL_MID				0x08				// Volume Middle Mode
#define	VOL_LOW				0x10				// Volume Low Mode
#define	VOL_SILENT			0x20				// Silent Mode

extern BYTE gbVolumeMode;
#endif


