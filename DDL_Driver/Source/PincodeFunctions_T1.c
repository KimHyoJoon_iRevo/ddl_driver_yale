//------------------------------------------------------------------------------
/** 	@file		PincodeFunctions_T1.c
	@version 0.1.02
	@date	2016.05.10
	@brief	Pincode Process Functions
	@remark 비밀번호 처리에 필요한 함수
	@section MODIFYINFO
		[Revision History]
		
		V0.1.00 2016.04.14		by Jay
			- 신규 UI에서 Pincode 처리에 필요한 함수들 정리

		V0.1.01 2016.04.20		by Jay
			- MAX_PIN_LENGTH를 12에서 10으로 수정

		V0.1.02 2016.05.10		by Jay
			- RearrangeShiftTenKeyBufForLastCompare 함수가 아래의 경우 오동작하여 알고리즘 수정
				=> 비밀번호 12345를 등록하고, 9012345로 문을 열 경우 에러 발생
			- 관련 테스트 내용을 UnitTestTenKeySave 함수에 추가 적용
		V0.1.03 2016.06.29		by Hoon
			UpdatePincodeToMemoryByModule  함수에서 User Slot 변수 자료형 수정 : BYTE -> WORD
		
*/
//------------------------------------------------------------------------------


#include "Main.h"


BYTE gPinInputKeyCnt = 0;


#if 1	
	// 언어 설정이 필요 없는 제품도 중국과 다른 지역 FW를 나누어 관리하지 않도록 하기 위해 공용 처리
BYTE gPinInputMinLength = 0;
#else
#ifdef	P_VOICE_RST // defined (DDL_CFG_SUPPORT_CHINESE)
BYTE gPinInputMinLength = 0;
#endif 
#endif

// Pincode는 nibble당 1자리가 저장된다. 1234라면 0x12, 0x34로 저장됨.
// MAX_PIN_LENGTH가 홀수가 될 경우에는 "(MAX_PIN_LENGTH>>1)+1" 처리 필요
#ifdef DDL_CFG_EMERGENCY_119
// 119 가 들어 갈 수 있게 2 를 더 더해 준다 
BYTE gPinInputKeyFromBeginBuf[(MAX_PIN_LENGTH>>1)+2];				/**< Input Pincode buffer */						
BYTE gPinInputKeyToEndBuf[(MAX_PIN_LENGTH>>1)+2];
#else 
BYTE gPinInputKeyFromBeginBuf[MAX_PIN_LENGTH>>1];				/**< Input Pincode buffer */						
BYTE gPinInputKeyToEndBuf[MAX_PIN_LENGTH>>1];
#endif

const BYTE ShiftDayOfWeekCheck[8] = {DAYOFWEEK_SUN, DAYOFWEEK_MON, DAYOFWEEK_TUE, DAYOFWEEK_WED, DAYOFWEEK_THU, DAYOFWEEK_FRI, DAYOFWEEK_SAT, DAYOFWEEK_SUN};


//------------------------------------------------------------------------------
/** 	@brief	Ten Key Variables Clear to input new keys  
	@param 	None 
	@return 	None
	@remark 새로운 Ten Key 입력을 받기 위해 버퍼 및 변수들 초기화
	@remark 매번 새로운 Ten Key 입력을 받기 위해서는 외부에서 별도로 해당 함수 수행해야 함
*/
//------------------------------------------------------------------------------
void TenKeyVariablesClear(void)
{
	BYTE bCnt;

	gPinInputKeyCnt	= 0;

	// MAX_PIN_LENGTH가 홀수가 될 경우에는 "(MAX_PIN_LENGTH>>1)+1" 처리 필요
#ifdef DDL_CFG_EMERGENCY_119
	// 119 가 들어 갈 수 있게 2 를 더 더해 준다 
	for(bCnt = 0; bCnt < (MAX_PIN_LENGTH>>1)+2; bCnt++)
#else 
	for(bCnt = 0; bCnt < (MAX_PIN_LENGTH>>1); bCnt++)
#endif 		
	{
		gPinInputKeyFromBeginBuf[bCnt] = 0xFF;
		gPinInputKeyToEndBuf[bCnt] = 0xFF;
	}
}


//------------------------------------------------------------------------------
/** 	@brief	Save a second data for fake pincode process 
	@param 	[NewInputKey] 입력된 새로운 키 
	@return 	None
	@remark 뒤쪽에 배치된 허수 처리를 위한 버퍼에 입력된 키 저장
	@remark FIFO 구조로 처리되며, 새로운 키가 가장 마지막 nibble에 저장
	@remark 최대 비밀번호 자리 수가 넘을 경우 맨 마지막 입력값이 가장 끝에 위치하도록 조정
	@remark ex) 최대 비밀번호 자리 수가 4일 경우
	@remark       0x12, 0x34 상태에서 '5'가 입력되면 최종 Data는 0x23, 0x45가 됨
*/
//------------------------------------------------------------------------------
void ShiftTenKeyBufToEnd(BYTE NewInputKey)
{
	BYTE bCnt;
	WORD wTmp;
	BYTE bMaxCnt;

#ifdef DDL_CFG_EMERGENCY_119
	// 119 가 들어 갈 수 있게 2 를 더 더해 준다 
	bMaxCnt = (MAX_PIN_LENGTH>>1)+2;
#else 		
	bMaxCnt = (MAX_PIN_LENGTH>>1);
#endif 

	if(bMaxCnt & 0x01)
	{
		for(bCnt = 0; bCnt < bMaxCnt; bCnt+=2)
		{
			wTmp = gPinInputKeyToEndBuf[bCnt];
			wTmp = (wTmp << 8);
			if((bCnt+1) != bMaxCnt)
			{
				wTmp |= gPinInputKeyToEndBuf[bCnt+1];
			}
		
			if(bCnt != 0)
			{
				// 1nibble 왼쪽으로 Shift된 바로 이전 Byte의 하위 nibble에 다음 Byte의 최상위 nibble 적용
				gPinInputKeyToEndBuf[bCnt-1] |= (wTmp >> 12);
			}
		
			// 2byte씩 WORD 형태로 만들어 1nibble만 왼쪽으로 Shift한 뒤 다시 나누어 저장
			wTmp <<= 4;
			gPinInputKeyToEndBuf[bCnt] = (wTmp >> 8);
			if((bCnt+1) != bMaxCnt)
			{
				gPinInputKeyToEndBuf[bCnt+1] = (wTmp & 0xF0);
			}
		}	
		
		gPinInputKeyToEndBuf[bCnt-2] |= NewInputKey;
	}
	else
	{
		for(bCnt = 0; bCnt < bMaxCnt; bCnt+=2)
	{
		wTmp = gPinInputKeyToEndBuf[bCnt];
		wTmp = (wTmp << 8) | gPinInputKeyToEndBuf[bCnt+1];

		if(bCnt != 0)
		{
			// 1nibble 왼쪽으로 Shift된 바로 이전 Byte의 하위 nibble에 다음 Byte의 최상위 nibble 적용
			gPinInputKeyToEndBuf[bCnt-1] |= (wTmp >> 12);
		}

		// 2byte씩 WORD 형태로 만들어 1nibble만 왼쪽으로 Shift한 뒤 다시 나누어 저장
		wTmp <<= 4;
		gPinInputKeyToEndBuf[bCnt] = (wTmp >> 8);
		gPinInputKeyToEndBuf[bCnt+1] = (wTmp & 0xF0);
	}	

	gPinInputKeyToEndBuf[bCnt-1] |= NewInputKey;
}		
}		




//------------------------------------------------------------------------------
/** 	@brief	Save Key Data for real & fake pincode process 
	@param 	[NewInputKey] 입력된 새로운 키 
	@param 	[InputKeyCnt] 현재까지 입력된 키 자리수 (새로운 키도 포함), MAX_PIN_LENGTH를 넘지 않음. 
	@return 	None
	@remark 새로 입력된 키를 처리를 위한 버퍼에 저장 (앞쪽, 뒤쪽 배치된 허수를 위한 버퍼에 모두 저장)
*/
//------------------------------------------------------------------------------
void TempSaveInputKeyToBuffer(BYTE NewInputKey, BYTE InputKeyCnt)
{
	BYTE ByteCntToSave;

	//InputKeyCnt 값은 반드시 외부에서 +1 한 뒤에 호출되어야 함.
	if(InputKeyCnt == 0)	return;

	ByteCntToSave = (InputKeyCnt-1) >> 1;				
	if(InputKeyCnt & 0x01)
	{										
		//홀수일 경우 High Nibble에 저장+Low Nibble은 'F'로 채움
		gPinInputKeyFromBeginBuf[ByteCntToSave] = (NewInputKey << 4) | 0x0F;					

		gPinInputKeyToEndBuf[ByteCntToSave] = (NewInputKey << 4) | 0x0F;					
	}
	else
	{	
		//짝수일 경우 Low Nibble에 저장
		gPinInputKeyFromBeginBuf[ByteCntToSave] &= 0xF0;
		gPinInputKeyFromBeginBuf[ByteCntToSave] |= NewInputKey;	

		gPinInputKeyToEndBuf[ByteCntToSave] &= 0xF0;
		gPinInputKeyToEndBuf[ByteCntToSave] |= NewInputKey; 
	}		
}



//------------------------------------------------------------------------------
/** 	@brief	Save Input Key process as operating situation 
	@param 	[LimitKeyCnt] 최대 입력 가능 키 수, MAX_PIN_LENGTH를 넘지 않음 
	@param 	[NewInputKey] 입력된 새로운 키 
	@param 	[FakeEnable] 허수 비교 허용 여부 (1일 경우 허용, 0일 경우 허용하지 않음) 
	@return 	None
	@remark 새로 입력된 키를 처리를 위한 버퍼에 저장 (앞쪽, 뒤쪽 배치된 허수를 위한 버퍼에 모두 저장)
*/
//------------------------------------------------------------------------------
BYTE TenKeySave(BYTE LimitKeyCnt, BYTE NewInputKey, BYTE FakeEnable)
{
	// 허수 처리 필요가 없어, 최대 입력 키 제한 처리가 필요할 경우에 수행
//	if(gbManageMode || gfTamperProofSet || (FakeEnable == 0))
	if(gfTamperProofSet || (FakeEnable == 0))
	{
		if(gPinInputKeyCnt >= LimitKeyCnt) 	
		{
			return TENKEY_INPUT_OVER;
		}

		gPinInputKeyCnt++;		
		TempSaveInputKeyToBuffer(NewInputKey, gPinInputKeyCnt);
			
		if(gPinInputKeyCnt == LimitKeyCnt)		
		{
			return TENKEY_INPUT_SAME;
		}
		
		return TENKEY_INPUT_OK;
	}	
	// 허수 처리가 필요하여, 최대 입력 키 제한 처리가 필요없을 때 수행
	else 
	{
#ifdef DDL_CFG_EMERGENCY_119
	// 두자리가 늘어나므로 count 는 4 를 더해 준다 
		if(gPinInputKeyCnt < MAX_PIN_LENGTH+4)
#else 		
		if(gPinInputKeyCnt < MAX_PIN_LENGTH )
#endif 			
		{
			// 최대 비밀번호 자리 수까지는 2개의 버퍼에 동일한 Data 저장
			gPinInputKeyCnt++;		
			TempSaveInputKeyToBuffer(NewInputKey, gPinInputKeyCnt);
		}
		else
		{
			// 최대 비밀번호 자리 수가 넘을 경우에는 뒤쪽에 배치된 허수 처리를 위해 2번째 버퍼의 Data만 추가 저장
			ShiftTenKeyBufToEnd(NewInputKey);
		}
	}
	
	return TENKEY_INPUT_OK;
}




//------------------------------------------------------------------------------
/** 	@brief	Rearrange input ten key data to compare the save data to first input codes
	@param 	[pRearrangeBuf] 앞쪽에 배치된 허수를 비교를 위한 버퍼 
	@param 	[PincodeLength] 저장된 비밀번호의 자리수 
	@return 	None
	@remark 입력한 키들의 앞쪽에 배치된 허수를 저장된 비밀번호와의 비교를 위해 처리하는 함수
	@remark ex) 버퍼에 0x12, 0x34, 0x56이 입력되어 있고, 저장된 비밀번호 자리 수가 4일 경우
	@remark       해당 버퍼의 값을 0x12, 0x34, 0xFF로 앞쪽 허수 비교 가능하도록 재조정  
*/
//------------------------------------------------------------------------------
void RearrangeTenKeyBufForFirstCompare(BYTE *pRearrangeBuf, BYTE PincodeLength)
{
	BYTE bCnt;

	// 저장된 비밀번호 자리수가 입력된 자리수보다 많을 경우에는 뒤쪽 허수 비교 처리가 필요 없음
	if(gPinInputKeyCnt < PincodeLength)
	{
		return;
	}	

	for(bCnt = 0; bCnt < (PincodeLength >> 1); bCnt++)
	{
		pRearrangeBuf[bCnt] = gPinInputKeyFromBeginBuf[bCnt];
	}

	// 저장된 비밀번호 자리수가 홀수일 경우에는 마지막 자리 처리하고 하위 Nibble은 'F'로 채운다.
	if(PincodeLength & 0x01)
	{
		pRearrangeBuf[bCnt] = gPinInputKeyFromBeginBuf[bCnt] | 0x0F;
	}
}		




//------------------------------------------------------------------------------
/** 	@brief	Make rearrange data with condition
	@param 	[pRearrangeBuf] 뒤쪽에 배치된 허수를 비교를 위해 앞쪽에 다시 정렬하기 위한 버퍼 
	@param 	[StartDataByte] 입력된 버퍼에서 비교할 비밀번호를 추출하기 위한 시작 번지
	@param 	[LastDataByte] 입력된 버퍼에서 비교할 비밀번호를 추출하기 위한 종료 번지
	@param 	[Condition] 다른 Byte의 Nibble을 반영해야 하는지, 바로 Copy만 하면 되는지 확인
	@return 	None
	@remark 경우에 따라 비교할 비밀번호를 앞쪽에 다시 정렬하도록 처리하는 함수
*/
//------------------------------------------------------------------------------
void MakeRearrangeData(BYTE *pRearrangeBuf, BYTE StartDataByte, BYTE LastDataByte, BYTE Condition)
{
	BYTE bCnt;
	BYTE bTmp;	

	if(Condition)
	{
		for(bCnt = StartDataByte; bCnt < LastDataByte; bCnt++)
		{
			bTmp = gPinInputKeyToEndBuf[bCnt];
			pRearrangeBuf[bCnt-StartDataByte] = (bTmp << 4);
			
			bTmp = gPinInputKeyToEndBuf[bCnt+1];
			pRearrangeBuf[bCnt-StartDataByte] |= (bTmp >> 4);
		}	
		
		bTmp = gPinInputKeyToEndBuf[bCnt];
		pRearrangeBuf[bCnt-StartDataByte] = (bTmp << 4) | 0x0F;
	}
	else
	{
		for(bCnt = StartDataByte; bCnt < LastDataByte; bCnt++)
		{
			pRearrangeBuf[bCnt-StartDataByte] = gPinInputKeyToEndBuf[bCnt];
		}
	}
}



//------------------------------------------------------------------------------
/** 	@brief	Rearrange input ten key data to compare the save data to last input codes
	@param 	[pRearrangeBuf] 뒤쪽에 배치된 허수를 비교를 위해 앞쪽에 다시 정렬하기 위한 버퍼 
	@param 	[PincodeLength] 저장된 비밀번호의 자리수 
	@return 	None
	@remark 입력한 키들의 뒤쪽에 배치된 허수를 저장된 비밀번호와의 비교를 위해 저장된 비밀번호 
	              자리수에 맞추어 앞쪽에 다시 정렬하도록 처리하는 함수
	@remark ex) 버퍼에 0x12, 0x34, 0x56이 입력되어 있고, 저장된 비밀번호 자리 수가 4일 경우
	@remark       해당 버퍼의 값을 0x34, 0x56, 0xFF로 뒤쪽 허수 비교 가능하도록 재조정  
*/
//------------------------------------------------------------------------------
void RearrangeShiftTenKeyBufForLastCompare(BYTE *pRearrangeBuf, BYTE PincodeLength)
{
	BYTE StartDataByte;
	BYTE LastDataByte;
	BYTE ProcessCase;

	if(gPinInputKeyCnt < PincodeLength)
	{
		return;
	}	

	ProcessCase = 0;

	if(gPinInputKeyCnt & 0x01)
	{
		ProcessCase |= 0x01;
	}

	if(PincodeLength & 0x01)
	{
		ProcessCase |= 0x02;
	}
	
			LastDataByte = (gPinInputKeyCnt >> 1);
			StartDataByte = LastDataByte - (PincodeLength >> 1);

	switch(ProcessCase)
	{
		// 입력된 Pincode의 자리수는 홀수일 경우 
		// 비교할 Pincode의 자리수가 홀수일 경우 
		case 0x03:
			LastDataByte++;

		// 입력된 Pincode의 자리수는 짝수일 경우 
		// 비교할 Pincode의 자리수가 짝수일 경우 
		case 0x00:
			MakeRearrangeData(pRearrangeBuf, StartDataByte, LastDataByte, 0);
			break;

		// 입력된 Pincode의 자리수는 짝수일 경우 
		// 비교할 Pincode의 자리수가 홀수일 경우 
		case 0x02:
			if(StartDataByte)	StartDataByte--;
			if(LastDataByte)	LastDataByte--;

		// 입력된 Pincode의 자리수는 홀수일 경우 
		// 비교할 Pincode의 자리수가 짝수일 경우 
		case 0x01:
			MakeRearrangeData(pRearrangeBuf, StartDataByte, LastDataByte, 1);
			break;

		default:
			break;
	}		
}		

#ifdef DDL_CFG_EMERGENCY_119
//------------------------------------------------------------------------------
/** 	@brief	허수에 119 여부 확인 
	허수판단 이후에 gPinInputKeyFromBeginBuf , gPinInputKeyToEndBuf 에 119 를 검출 하여 
	119 가 있으면 flag 를 set 해서 emergency PIN 을 enable 한다. 
	PINCODE + 119 허수 = Position 에 1 
	허수 119 + PINCODE = Position 에 0 
*/
//------------------------------------------------------------------------------
void Detect119Emergency(BYTE Position, BYTE PincodeLength)
{
	BYTE Index = 0x00;
	BYTE LastDataByte;
	BYTE GoAlarm = FALSE;

	if(Position) // PINCODE + 119 + 허수 
	{
		Index = (PincodeLength / 2);	
		
		if(PincodeLength & 0x01)
		{
			if((gPinInputKeyFromBeginBuf[Index] & 0x0F) == 0x01 && gPinInputKeyFromBeginBuf[Index+1] == 0x19)
				GoAlarm = TRUE;	
		}
		else 
		{
			if(gPinInputKeyFromBeginBuf[Index] == 0x11 && (gPinInputKeyFromBeginBuf[Index+1] & 0xF0) == 0x90)
				GoAlarm = TRUE;	
		}
	}
	else // 허수 + 119 + PINCODE 
	{
		LastDataByte = (gPinInputKeyCnt >> 1);
		Index = LastDataByte - (PincodeLength >> 1);
			
		if(gPinInputKeyCnt & 0x01)
		{
			if(PincodeLength & 0x01)
			{
				if(gPinInputKeyToEndBuf[Index-1] == 0x19 && (gPinInputKeyToEndBuf[Index-2] & 0x0F) == 0x01)
					GoAlarm = TRUE;
			}
			else 
			{
				if((gPinInputKeyToEndBuf[Index] & 0xF0) == 0x90 && gPinInputKeyToEndBuf[Index-1] == 0x11)
					GoAlarm = TRUE;
			}
		}
		else 
		{
			if(PincodeLength & 0x01)
			{
				if((gPinInputKeyToEndBuf[Index-1] & 0xF0) == 0x90 && gPinInputKeyToEndBuf[Index-2] == 0x11)
					GoAlarm = TRUE;	
			}
			else 
			{
				if(gPinInputKeyToEndBuf[Index-1] == 0x19 && (gPinInputKeyToEndBuf[Index-2] & 0x0F) == 0x01)
					GoAlarm = TRUE;	
			}
		}
	}

	if(GoAlarm)
	{
		AlarmGotoEmergency119();
	}
}
#endif 

//------------------------------------------------------------------------------
/** 	@brief	Verify Input Pincode with given condition
	@param 	[Address] 비교할 비밀번호가 저장된 메모리의 시작 주소 
	@param 	[MaxLoopNum] 비교할 저장된 비밀번호의 총 개수 
	@param 	[FakeEnable] 허수 비교 허용 여부 (1일 경우 허용, 0일 경우 허용하지 않음) 
	@return 	[RET_NO_MATCH] 일치하는 비밀번호가 없을 경우
	@return 	[0~최대 비밀번호 최대 저장 순서] 일치하는 비밀번호의 저장 순서 회신
	@remark 허수를 비교하지 않을 경우에는 비밀번호 전체가 동일한지 비교 
	@remark 허수를 비교할 경우에는 앞쪽, 뒤쪽에 배치된 비밀번호 모두 비교 
*/
//------------------------------------------------------------------------------
#ifdef DDL_CFG_EMERGENCY_119
// 119 를 위해 buffer length 에 2 를 더해 준다 
BYTE VerityInputPincode(WORD Address, BYTE MaxLoopNum, BYTE FakeEnable)
{
	BYTE bCnt;
	BYTE VerifyBuf[8];
	BYTE RearrangeBuf[(MAX_PIN_LENGTH>>1)+2];
	
	for(bCnt = 0; bCnt < MaxLoopNum; bCnt++)
	{
		RomRead(VerifyBuf, Address+((WORD)bCnt*8), 8);	

#if defined (_USE_IREVO_CRYPTO_)
		EncryptDecryptKey(VerifyBuf,8);
#endif 	

		if(VerifyBuf[0] == 0xFF)	continue;

		//허수 적용되지 않은 비교
		if(memcmp(VerifyBuf, gPinInputKeyFromBeginBuf, (MAX_PIN_LENGTH>>1)+2) == 0)
		{
			return (bCnt);				
		}

		//FakeEnable이 1로 설정되어 있을 경우에는 허수 비교
		if(FakeEnable == 0)	continue;

		// 앞쪽에 배치된 허수 비교 부분 PINCODE + 허수 
		memset(RearrangeBuf, 0xFF, (MAX_PIN_LENGTH>>1)+2);
		RearrangeTenKeyBufForFirstCompare(RearrangeBuf, VerifyBuf[7]);

		if(memcmp(VerifyBuf, RearrangeBuf, (MAX_PIN_LENGTH>>1)+2) == 0)
		{
			Detect119Emergency(1,VerifyBuf[7]);
			return (bCnt);				
		}

		// 뒤쪽에 배치된 허수 비교 부분 허수 + PINCODE 
		memset(RearrangeBuf, 0xFF, (MAX_PIN_LENGTH>>1)+2);
		RearrangeShiftTenKeyBufForLastCompare(RearrangeBuf, VerifyBuf[7]);

		if(memcmp(VerifyBuf, RearrangeBuf, (MAX_PIN_LENGTH>>1)+2) == 0)
		{
			Detect119Emergency(0,VerifyBuf[7]);
			return (bCnt);				
		}
	}
	
	return (RET_NO_MATCH);
}
#else 
BYTE VerityInputPincode(WORD Address, BYTE MaxLoopNum, BYTE FakeEnable)
{
	BYTE bCnt;
	BYTE VerifyBuf[8];
	BYTE RearrangeBuf[(MAX_PIN_LENGTH>>1)];
	
	for(bCnt = 0; bCnt < MaxLoopNum; bCnt++)
	{
		RomRead(VerifyBuf, Address+((WORD)bCnt*8), 8);	

#if defined (_USE_IREVO_CRYPTO_)
		EncryptDecryptKey(VerifyBuf,8);
#endif 	

		if(VerifyBuf[0] == 0xFF)	continue;

		//허수 적용되지 않은 비교
		if(memcmp(VerifyBuf, gPinInputKeyFromBeginBuf, (MAX_PIN_LENGTH>>1)) == 0)
		{
#if defined (DDL_CFG_MS)
			/* MS 사양은 허수 비교 않는다 */
			/* 인증된 pincode 의 마지막 byte 를 가지고 pin code type 을 setting 한다 */
			MS_Set_Verified_Type(VerifyBuf[7]);
			//VerifyBuf[7] = (VerifyBuf[7] & 0x0F) + CODE_LENGTH_OFFSET;
#endif 
			return (bCnt);				
		}

		//FakeEnable이 1로 설정되어 있을 경우에는 허수 비교
		if(FakeEnable == 0)	continue;

		// 앞쪽에 배치된 허수 비교 부분
		memset(RearrangeBuf, 0xFF, (MAX_PIN_LENGTH>>1));
		RearrangeTenKeyBufForFirstCompare(RearrangeBuf, VerifyBuf[7]);

		if(memcmp(VerifyBuf, RearrangeBuf, (MAX_PIN_LENGTH>>1)) == 0)
		{
			return (bCnt);				
		}

		// 뒤쪽에 배치된 허수 비교 부분
		memset(RearrangeBuf, 0xFF, (MAX_PIN_LENGTH>>1));
		RearrangeShiftTenKeyBufForLastCompare(RearrangeBuf, VerifyBuf[7]);

		if(memcmp(VerifyBuf, RearrangeBuf, (MAX_PIN_LENGTH>>1)) == 0)
		{
			return (bCnt);				
		}
	}
	
	return (RET_NO_MATCH);
}
#endif 

#if 1	
	// 언어 설정이 필요 없는 제품도 중국과 다른 지역 FW를 나누어 관리하지 않도록 하기 위해 공용 처리
BYTE GetMinimum4DigitPinCodeAllowState(void)
{
	BYTE Temp = 0x00;
	
	switch(GetMainMode())
	{
		case MODE_PIN_VERIFY:
		case MODE_MENU_ALLCREDENTIALS_DELETE:
		case MODE_MENU_ONETIMECODE_DELETE:
		case MODE_MENU_REMOCON_DELETE:		
		case MODE_MENU_RFLINKMODULE_DELETE:
		case MODE_MENU_VISITORCODE_DELETE:
#ifdef  DDL_CFG_FP	 
		case MODE_MENU_FINGER_DELETE:
#endif 			
#ifdef 	DDL_CFG_RFID
		case MODE_MENU_ALLCARD_DELETE:
#endif 			
#ifdef 	DDL_CFG_DS1972_IBUTTON		
		case MODE_MENU_ALLDS1972TOUCHKEY_DELETE:
#endif 			
#ifdef 	DDL_CFG_IBUTTON
		case MODE_MENU_ALLGMTOUCHKEY_DELETE:
#endif 			
		Temp = 0x01;
		break;
		default : 
			break;
	}
	return Temp;
}
#else
#ifdef	P_VOICE_RST // defined (DDL_CFG_SUPPORT_CHINESE)
BYTE GetMinimum4DigitPinCodeAllowState(void)
{
	BYTE Temp = 0x00;
	
	switch(GetMainMode())
	{
		case MODE_PIN_VERIFY:
		case MODE_MENU_ALLCREDENTIALS_DELETE:
		case MODE_MENU_ONETIMECODE_DELETE:
		case MODE_MENU_REMOCON_DELETE:		
		case MODE_MENU_RFLINKMODULE_DELETE:
		case MODE_MENU_VISITORCODE_DELETE:
#ifdef  DDL_CFG_FP	 
		case MODE_MENU_FINGER_DELETE:
#endif 			
#ifdef 	DDL_CFG_RFID
		case MODE_MENU_ALLCARD_DELETE:
#endif 			
#ifdef 	DDL_CFG_DS1972_IBUTTON		
		case MODE_MENU_ALLDS1972TOUCHKEY_DELETE:
#endif 			
#ifdef 	DDL_CFG_IBUTTON
		case MODE_MENU_ALLGMTOUCHKEY_DELETE:
#endif 			
		Temp = 0x01;
		break;
		default : 
			break;
	}
	return Temp;
}
#endif 
#endif

//------------------------------------------------------------------------------
/** 	@brief	Verify Input Pincode with all saved pincode
	@param 	[FakeEnable] 허수 비교 허용 여부 (1일 경우 허용, 0일 경우 허용하지 않음)  
	@return 	[RET_NO_INPUT] 입력한 비밀번호가 없을 경우
	@return 	[RET_WRONG_DIGIT_INPUT] 입력한 비밀번호의 자리수가 범위에 있지 않을 경우
	@return 	[RET_MASTER_CODE_MATCH] Master Code 일치
	@return 	[RET_TEMP_CODE_MATCH] Onetime Code 일치
	@return 	[RET_NO_MATCH] 일치하는 비밀번호가 없을 경우
	@return 	[0~최대 비밀번호 최대 저장 순서] 일치하는 User Code의 저장 순서 회신
	@remark 허수를 비교하지 않을 경우에는 비밀번호 전체가 동일한지 비교 
	@remark 허수를 비교할 경우에는 앞쪽, 뒤쪽에 배치된 비밀번호 모두 비교 
*/
//------------------------------------------------------------------------------
BYTE PincodeVerify(BYTE FakeEnable)
{
	BYTE bReturnVal;
	BYTE bTmpArray[8];

	memset(bTmpArray, 0xFF, 8);
	CopyCredentialDataForPack(bTmpArray, 8);
	CopySlotNumberForPack(0);

#if 1	
	// 언어 설정이 필요 없는 제품도 중국과 다른 지역 FW를 나누어 관리하지 않도록 하기 위해 공용 처리
	if(GetMinimum4DigitPinCodeAllowState() == 0x01) // verify 시에만 언어에 상관 없이 4 자리로 비교 한다 
	{
		if(gPinInputKeyCnt < 4)
		{
			if(gPinInputKeyCnt == 0)			
			{
				return (RET_NO_INPUT);
			}
			return (RET_WRONG_DIGIT_INPUT); 						
		}
	}
	else 
	{
		if(gPinInputKeyCnt < gPinInputMinLength)
		{
			if(gPinInputKeyCnt == 0)			
			{
				return (RET_NO_INPUT);
			}
			return (RET_WRONG_DIGIT_INPUT); 						
		}
	}
#else
#ifdef	P_VOICE_RST // defined (DDL_CFG_SUPPORT_CHINESE)
	if(GetMinimum4DigitPinCodeAllowState() == 0x01) // verify 시에만 언어에 상관 없이 4 자리로 비교 한다 
	{
		if(gPinInputKeyCnt < 4)
		{
			if(gPinInputKeyCnt == 0)			
			{
				return (RET_NO_INPUT);
			}
			return (RET_WRONG_DIGIT_INPUT); 						
		}
	}
	else 
	{
		if(gPinInputKeyCnt < gPinInputMinLength)
		{
			if(gPinInputKeyCnt == 0)			
			{
				return (RET_NO_INPUT);
			}
			return (RET_WRONG_DIGIT_INPUT); 						
		}
	}
#else 
	if(gPinInputKeyCnt < MIN_PIN_LENGTH)
	{
		if(gPinInputKeyCnt == 0)			
		{
			return (RET_NO_INPUT);
		}
		return (RET_WRONG_DIGIT_INPUT);							
	}
#endif 		
#endif
	// 입력된 비밀번호의 빈 공간은 모두 FFh 로 채움 
	// 키 입력 받기 전에 매번 버퍼를 FFh로 초기화하기 때문에 이 과정을 불필요해 보임.
/*
	if(gPinInputKeyCnt % 2)	gPinInputKeyCnt++;

	for(bCnt = (gPinInputKeyCnt/2); bCnt < (MAX_PIN_LENGTH>>1); bCnt++)
	{
		gPinInputKeyFromBeginBuf[bCnt] = 0xFF;									
	}
*/
	if((gPinInputKeyFromBeginBuf[0] == 0xFF) || (gPinInputKeyFromBeginBuf[1] == 0xFF))
	{
		return (RET_WRONG_DIGIT_INPUT);
	}	

#ifdef DDL_CFG_EMERGENCY_119
	// 119 가 들어 갈 수 있게 2 를 더 더해 준다
	CopyCredentialDataForPack(gPinInputKeyFromBeginBuf, (MAX_PIN_LENGTH>>1)+2);	
#else 	
	CopyCredentialDataForPack(gPinInputKeyFromBeginBuf, (MAX_PIN_LENGTH>>1));
#endif 

#if defined (DDL_CFG_MS)
	/* Jig 연결 중이면 MS_TEMP_PINCODE 만 검사 */
	if(JigModeStatusCheck() == STATUS_SUCCESS)
	{
		bReturnVal = VerityInputPincode(MS_TEMP_PINCODE, 2, 0); 
		if(bReturnVal != RET_NO_MATCH)
		{
			RomWriteWithSameData(0xFF, MS_TEMP_PINCODE, 8);// PC 생산 지그 시에 setting 했던 간이 PW 삭제 			
			return (bReturnVal+1);
		}
	}
#else 
//------------------------------------------------------------------------------ OneTime Code
	bReturnVal = VerityInputPincode(ONETIME_CODE, 1, 0); 
	if(bReturnVal == 0)
	{
		CopySlotNumberForPack(RET_TEMP_CODE_MATCH);
		
		return (RET_TEMP_CODE_MATCH);
	}

//------------------------------------------------------------------------------ Master Code & User Code
#endif 

	if(gbManageMode == _AD_MODE_SET)
	{
		bReturnVal = VerityInputPincode(MASTER_CODE, (SUPPORTED_USERCODE_NUMBER+1), 0); 
		if(bReturnVal == 0)
		{
			CopySlotNumberForPack(RET_MASTER_CODE_MATCH);

			return (RET_MASTER_CODE_MATCH);
		}

		if(bReturnVal != RET_NO_MATCH)
		{
			// User Code의 Slot Number는 1부터 시작
			CopySlotNumberForPack(bReturnVal);

			return (bReturnVal);
		}
	}
	else
	{
	    if(gfTamperProofSet)
		{
			//In Easy mode, only a user code and a visitor code are verified.
#if defined (DDL_CFG_MS)
			bReturnVal = VerityInputPincode(MS_GetSupported_Credential_Start_Address(MANAGER_CODE_INDEX),MS_GetSupported_Credential_Number(SUM_OF_CODE), 0); 
#else 
			bReturnVal = VerityInputPincode(USER_CODE, 2, 0); 
#endif 
			if(bReturnVal != RET_NO_MATCH)
			{
				// User Code의 Slot Number는 1부터 시작
				CopySlotNumberForPack(bReturnVal+1);
				
				return (bReturnVal+1);
			}
		}
		else
		{
			//In Easy mode, only a user code and a visitor code are verified.
#if defined (DDL_CFG_MS)
			bReturnVal = VerityInputPincode(MS_GetSupported_Credential_Start_Address(MANAGER_CODE_INDEX),MS_GetSupported_Credential_Number(SUM_OF_CODE), 0); 
#else 
			bReturnVal = VerityInputPincode(USER_CODE, 2, FakeEnable); 
#endif 
			if(bReturnVal != RET_NO_MATCH)
			{
				// User Code의 Slot Number는 1부터 시작
				CopySlotNumberForPack(bReturnVal+1);

				return (bReturnVal+1);
			}
		}
	}
	
	return RET_NO_MATCH;
}




//------------------------------------------------------------------------------
/** 	@brief	Verify schefule of Input Pincode
	@param 	[pScheduleBuf] 비밀번호의 저장된 Schedule Data  
	@param 	[pCurrentTimeBuf] 현재 시간 Data  
	@return 	[1] Schedule Data 검증이 통과하였을 경우
	@return 	[0] Schedule 검증이 통과하지 못하였을 경우
	@remark 저장된 Schedule 정보를 현재 시간과 비교하여 유효한지 확인
	@remark 1. Schedule의 시작 시간, 종료 시간 및 비교하는 현재 시간 계산
	@remark 2. Schedule의 요일 정보 Load
	@remark 3. 현재 시간이 시작 시간 날인지 같을 경우 무조건 에러 처리 
	@remark 4. 종료 시간이 시작 시간 다음 날인지 확인
	@remark 5. 요일 비교 					
*/
//------------------------------------------------------------------------------
BYTE VerifyScheduleOfInputPincode(BYTE *pScheduleBuf, BYTE *pCurrentTimeBuf)
{
	BYTE bCnt;
	WORD StartTime;
	WORD EndTime;
	WORD CurrentTime;
	BYTE DayOfWeek;

	//   시작 시간 변환
	StartTime = ((WORD)pScheduleBuf[1] << 8) | (WORD)pScheduleBuf[2];
	StartTime >>= 4;  

	// 종료 시간 (= 시작 시간+사용 시간) 
	EndTime = (((WORD)pScheduleBuf[2] & 0x0F) << 8) | (WORD)pScheduleBuf[3];
	EndTime += StartTime; 

	CurrentTime = ((WORD)pCurrentTimeBuf[1] << 8) | (WORD)pCurrentTimeBuf[2];

	//60 * 24 = 1440 = 0x5a0; 1일을 나타내는 시간.
	//wStime + wEtime = 1440 이면 하루를 지나것으로 요일 비교에서 다음 날도 체크가 들어 가야 함.
	// 그리고 다음날 종료 시간은 wStime + wEtime - 1440;  	

	if(CurrentTime == StartTime)
	{
		//현재 시간이 시작 시간과 같은 경우 무조건 에러 처리
		return 0;
	}		

	DayOfWeek = pScheduleBuf[0];

	// 하루가 지나는 지 확인  (1440 = 24*60)
	if(EndTime > 1440)
	{	
		// 종료 시간이 다음 날임 
		if(CurrentTime < StartTime)
		{
			EndTime -= 1440;	
			if(CurrentTime > EndTime)
			{
				return 0;
			}

			// 하루가 지나서도 동작해야 하는 경우면, 다음날의 요일도 임의 설정하여 처리되도록 함
			for(bCnt = 0; bCnt < 7; bCnt++)
			{
				if((pScheduleBuf[0] & ShiftDayOfWeekCheck[bCnt]) == ShiftDayOfWeekCheck[bCnt])
				{
					DayOfWeek |= ShiftDayOfWeekCheck[(bCnt+1)];
				}
			}					
		}
	}
	else
	{	
		// 종료 시간이 시작 시간과 동일한 날임
		// '시작 시간 < 현재 시간 <= 종료 시간' 일 조건에서만 Schedule 정상 처리됨
		if((CurrentTime < StartTime) || (CurrentTime > EndTime))
		{
			return 0;
		}
	}

	// 요일 비교 
	if((DayOfWeek & pCurrentTimeBuf[0]) == 0)
	{
		// 실행 안함
		return 0;  
	}

	// 시간 안에 존재 하므로 실행 
	return 1; 
}



//------------------------------------------------------------------------------
/** 	@brief	Verify schefule of Input Pincode
	@param 	[pCurrentTimeBuf] 현재 시간 정보 
	@param 	[SlotNumber] 확인해야 할 Code의 Slot Number 
	@return 	[1] 해당 비밀번호의 Schedule Data가 없거나 Schedule Data 검증이 통과하였을 경우
	@return 	[0] Schedule 검증이 통과하지 못하였을 경우
	@remark User Code의 4byte Schedule Data를 현재 시간과 비교, 처리하는 함수
	@remark User Code가 아닐 경우 Schedule 기능 사용하지 못함
*/
//------------------------------------------------------------------------------
BYTE ScheduleVerify(BYTE *pCurrentTimeBuf, BYTE SlotNumber)
{
	BYTE RetValue;
	WORD wAdrs;
	BYTE ScheduleDataBuf[6];

	if((SlotNumber-1) >= 30U)
	{
		return 1;
	}
	
	wAdrs = USER_SCHEDULE+((WORD)(SlotNumber-1)*4);
	RomRead(ScheduleDataBuf, (WORD)wAdrs, 4); 

	if(DataCompare(ScheduleDataBuf, 0xFF, 4) == _DATA_IDENTIFIED)
	{  							
		// 해당 Schedule Data 없음으로 Schedule 확인 통과		
		return 1;				
	}

	// 1 또는 0 으로 응답
	RetValue = VerifyScheduleOfInputPincode(ScheduleDataBuf, pCurrentTimeBuf); 

	return (RetValue);
}


void UpdatePincodeToMemory(WORD Address)
{
	BYTE pDataBuf[8];
	BYTE bPoint;
	BYTE bCnt;

	bPoint = (MAX_PIN_LENGTH>>1);
	memcpy(pDataBuf, gPinInputKeyFromBeginBuf, bPoint);

	for(bCnt = bPoint; bCnt < 7; bCnt++)
	{
		pDataBuf[bCnt] = 0xFF;
	}
#if defined (DDL_CFG_MS)
	if(JigModeStatusCheck() == STATUS_SUCCESS)
		pDataBuf[7] = gPinInputKeyCnt;
	else 
	MS_Add_Credential_Info(&pDataBuf[7],gPinInputKeyCnt);
#else 
	pDataBuf[7] = gPinInputKeyCnt;
#endif 

#if defined (_USE_IREVO_CRYPTO_)
	EncryptDecryptKey(pDataBuf,8);
#endif 	
	RomWrite(pDataBuf, Address, 8);

}


void CopyPincodeForDisplay(BYTE *pTargetBuf)
{
	memcpy(pTargetBuf, gPinInputKeyFromBeginBuf, (MAX_PIN_LENGTH>>1));
}



void UpdatePincodeToMemoryByModule(BYTE *pPincodeData, BYTE ByteLength, WORD SlotNumber, BYTE PincodeLength)
{
	BYTE TempBuf[8];

	memset(TempBuf, 0xFF, 8);
	if(ByteLength > 8)	ByteLength = 8;

	if(SlotNumber == RET_TEMP_CODE_MATCH)
	{
		memcpy(TempBuf, pPincodeData, ByteLength);
		TempBuf[7] = PincodeLength;

#if defined (_USE_IREVO_CRYPTO_)
		EncryptDecryptKey(TempBuf,8);
#endif 	
		RomWrite(TempBuf, ONETIME_CODE, 8);
	}
	else
	{
		if(gbManageMode == _AD_MODE_SET)
		{
			// Advanced Mode
			if(SlotNumber == RET_MASTER_CODE_MATCH)
			{
				memcpy(TempBuf, pPincodeData, ByteLength);
				TempBuf[7] = PincodeLength;

#if defined (_USE_IREVO_CRYPTO_)
				EncryptDecryptKey(TempBuf,8);
#endif 	
				RomWrite(TempBuf, MASTER_CODE, 8);

				// 기본 비밀번호가 등록될 경우 모터 잠김 금지를 위한 Factory Reset 정보 초기화
				FactoryResetClear();
			}
			else if((SlotNumber != 0) && (SlotNumber <= SUPPORTED_USERCODE_NUMBER))
			{
				memcpy(TempBuf, pPincodeData, ByteLength);
				TempBuf[7] = PincodeLength;

#if defined (_USE_IREVO_CRYPTO_)
				EncryptDecryptKey(TempBuf,8);
#endif 	
				
				RomWrite(TempBuf, USER_CODE + (SlotNumber - 1) * 8, 8);
			}
		}
		else
		{
			// Normal Mode
			if((SlotNumber != 0) && (SlotNumber <= 2))
			{
				memcpy(TempBuf, pPincodeData, ByteLength);
				TempBuf[7] = PincodeLength;

#if defined (_USE_IREVO_CRYPTO_)
				EncryptDecryptKey(TempBuf,8);
#endif 	
				RomWrite(TempBuf, USER_CODE + (SlotNumber - 1) * 8, 8);

				if(SlotNumber == 0x01)
				{
					// 기본 비밀번호가 등록될 경우 모터 잠김 금지를 위한 Factory Reset 정보 초기화
					FactoryResetClear();
				}	
			}
		}
	}
}



BYTE GetInputKeyCount(void)
{
	return (gPinInputKeyCnt);
}
	



BYTE gbTimeBuff[3];

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
BYTE gbTimeBuff_Ble30[7];
#endif

void TimeDataClear(void)
{
	BYTE bCnt;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
	{
		for(bCnt = 0; bCnt < 7; bCnt++)
		{
			gbTimeBuff_Ble30[bCnt] = 0xFF;
		}
	}
	else
#endif
	{	
		for(bCnt = 0; bCnt < 3; bCnt++)
		{
			gbTimeBuff[bCnt] = 0xFF;
		}
	}
}

void TimeDataSet(BYTE* pData)
{
	BYTE bCnt;

#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
	{
		gbTimeBuff_Ble30[0] = pData[4]; 	// Weekday
		gbTimeBuff_Ble30[1] = pData[0]; 	// Year
		gbTimeBuff_Ble30[2] = pData[1]; 	// Year
		gbTimeBuff_Ble30[3] = pData[2]; 	// Month
		gbTimeBuff_Ble30[4] = pData[3]; 	// Day
		gbTimeBuff_Ble30[5] = pData[5]; 	// Hour
		gbTimeBuff_Ble30[6] = pData[6]; 	// Munite
	}
	else
#endif
	{
		for(bCnt = 0; bCnt < 3; bCnt++)
		{
			gbTimeBuff[bCnt] = pData[bCnt];
		}
	}
}


bool IsGetTimeDataCompleted(void)
{
#if (DDL_CFG_BLE_30_ENABLE >= 0x29)
	if(gfPackTypeiRevoBleN30 || gfInnerPackTypeiRevoBleN30)
	{
		if(gbTimeBuff_Ble30[0] != 0xFF) 	return (true);
		else								return (false);
	}
	else
#endif		
	{
		if(gbTimeBuff[0] != 0xFF)	return (true);
		else						return (false);
	}
}


//==============================================================
//	TEST FUNCTIONS
//==============================================================
// 아래 함수는 테스트 대상 함수가 수정되었을 때 제대로 동작하는지 여부를 확인하기 위한 테스트 함수임

#if 0

#define	PINCODE_TEST_NUM	9

const BYTE unitTenKeyTestSize[PINCODE_TEST_NUM] = {4, 7, 10, 13, 15, 16, 7, 6, 10};
const BYTE unitTenKeyByteCnt[PINCODE_TEST_NUM] = {5, 6, 7, 8, 9, 10, 5, 4, 9};


const BYTE unitTenKeySaveData_Type1[4] = {TENKEY_1, TENKEY_2, TENKEY_3, TENKEY_4};

const BYTE unitTenKeySaveData_Type2[7] = {TENKEY_1, TENKEY_2, TENKEY_3, TENKEY_4, TENKEY_5 
											,TENKEY_6, TENKEY_7};

const BYTE unitTenKeySaveData_Type3[10] = {TENKEY_1, TENKEY_2, TENKEY_3, TENKEY_4, TENKEY_5 
											,TENKEY_6, TENKEY_7, TENKEY_8, TENKEY_9, TENKEY_0};

const BYTE unitTenKeySaveData_Type4[13] = {TENKEY_1, TENKEY_2, TENKEY_3, TENKEY_4, TENKEY_5 
											,TENKEY_6, TENKEY_7, TENKEY_8, TENKEY_9, TENKEY_0
											,TENKEY_1, TENKEY_2, TENKEY_3};

const BYTE unitTenKeySaveData_Type5[15] = {TENKEY_1, TENKEY_2, TENKEY_3, TENKEY_4, TENKEY_5 
											,TENKEY_6, TENKEY_7, TENKEY_8, TENKEY_9, TENKEY_0
											,TENKEY_1, TENKEY_2, TENKEY_3, TENKEY_4, TENKEY_5};

const BYTE unitTenKeySaveData_Type6[16] = {TENKEY_1, TENKEY_2, TENKEY_3, TENKEY_4, TENKEY_5 
											,TENKEY_6, TENKEY_7, TENKEY_8, TENKEY_9, TENKEY_0
											,TENKEY_1, TENKEY_2, TENKEY_3, TENKEY_4, TENKEY_5
											,TENKEY_6};
const BYTE unitTenKeySaveData_Type7[7] = {TENKEY_9, TENKEY_0, TENKEY_1, TENKEY_2, TENKEY_3 
											,TENKEY_4, TENKEY_5};

const BYTE unitTenKeySaveData_Type8[6] = {TENKEY_9, TENKEY_0, TENKEY_1, TENKEY_2, TENKEY_3 
											,TENKEY_4};

const BYTE unitTenKeySaveData_Type9[10] = {TENKEY_1, TENKEY_2, TENKEY_3, TENKEY_4, TENKEY_5 
											,TENKEY_6, TENKEY_7, TENKEY_8, TENKEY_9, TENKEY_0};


// MAX_PIN_LENGTH = 10일 경우 
const BYTE unitExpectedInputKeyFromBegin_Type1[6] = {0x12, 0x34, 0xFF, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyFromBegin_Type2[6] = {0x12, 0x34, 0x56, 0x7F, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyFromBegin_Type3[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xFF};
const BYTE unitExpectedInputKeyFromBegin_Type4[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xFF};
const BYTE unitExpectedInputKeyFromBegin_Type5[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xFF};
const BYTE unitExpectedInputKeyFromBegin_Type6[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xFF};
const BYTE unitExpectedInputKeyFromBegin_Type7[6] = {0x90, 0x12, 0x34, 0x5F, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyFromBegin_Type8[6] = {0x90, 0x12, 0x34, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyFromBegin_Type9[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xFF};

const BYTE unitExpectedInputKeyToEnd_Type1[6] = {0x12, 0x34, 0xFF, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyToEnd_Type2[6] = {0x12, 0x34, 0x56, 0x7F, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyToEnd_Type3[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xFF};
const BYTE unitExpectedInputKeyToEnd_Type4[6] = {0x45, 0x67, 0x89, 0x01, 0x23, 0xFF};
const BYTE unitExpectedInputKeyToEnd_Type5[6] = {0x67, 0x89, 0x01, 0x23, 0x45, 0xFF};
const BYTE unitExpectedInputKeyToEnd_Type6[6] = {0x78, 0x90, 0x12, 0x34, 0x56, 0xFF};
const BYTE unitExpectedInputKeyToEnd_Type7[6] = {0x90, 0x12, 0x34, 0x5F, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyToEnd_Type8[6] = {0x90, 0x12, 0x34, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyToEnd_Type9[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xFF};

const BYTE unitExpectedRearrangeFirstBuf_Type1[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type2[6] = {0x12, 0x34, 0x56, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type3[6] = {0x12, 0x34, 0x56, 0x7F, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type4[6] = {0x12, 0x34, 0x56, 0x78, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type5[6] = {0x12, 0x34, 0x56, 0x78, 0x9F, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type6[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type7[6] = {0x90, 0x12, 0x3F, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type8[6] = {0x90, 0x12, 0xFF, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type9[6] = {0x12, 0x34, 0x56, 0x78, 0x9F, 0xFF};

const BYTE unitExpectedRearrangeLastBuf_Type1[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type2[6] = {0x23, 0x45, 0x67, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type3[6] = {0x45, 0x67, 0x89, 0x0F, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type4[6] = {0x67, 0x89, 0x01, 0x23, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type5[6] = {0x78, 0x90, 0x12, 0x34, 0x5F, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type6[6] = {0x78, 0x90, 0x12, 0x34, 0x56, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type7[6] = {0x12, 0x34, 0x5F, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type8[6] = {0x12, 0x34, 0xFF, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type9[6] = {0x23, 0x45, 0x67, 0x89, 0x0F, 0xFF};

/*
// MAX_PIN_LENGTH = 12일 경우 
const BYTE unitExpectedInputKeyFromBegin_Type1[6] = {0x12, 0x34, 0xFF, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyFromBegin_Type2[6] = {0x12, 0x34, 0x56, 0x7F, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyFromBegin_Type3[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xFF};
const BYTE unitExpectedInputKeyFromBegin_Type4[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0x12};
const BYTE unitExpectedInputKeyFromBegin_Type5[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0x12};
const BYTE unitExpectedInputKeyFromBegin_Type6[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0x12};

const BYTE unitExpectedInputKeyToEnd_Type1[6] = {0x12, 0x34, 0xFF, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyToEnd_Type2[6] = {0x12, 0x34, 0x56, 0x7F, 0xFF, 0xFF};
const BYTE unitExpectedInputKeyToEnd_Type3[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xFF};
const BYTE unitExpectedInputKeyToEnd_Type4[6] = {0x23, 0x45, 0x67, 0x89, 0x01, 0x23};
const BYTE unitExpectedInputKeyToEnd_Type5[6] = {0x45, 0x67, 0x89, 0x01, 0x23, 0x45};
const BYTE unitExpectedInputKeyToEnd_Type6[6] = {0x56, 0x78, 0x90, 0x12, 0x34, 0x56};

const BYTE unitExpectedRearrangeFirstBuf_Type1[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type2[6] = {0x12, 0x34, 0x56, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type3[6] = {0x12, 0x34, 0x56, 0x7F, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type4[6] = {0x12, 0x34, 0x56, 0x78, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type5[6] = {0x12, 0x34, 0x56, 0x78, 0x9F, 0xFF};
const BYTE unitExpectedRearrangeFirstBuf_Type6[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xFF};

const BYTE unitExpectedRearrangeLastBuf_Type1[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type2[6] = {0x23, 0x45, 0x67, 0xFF, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type3[6] = {0x45, 0x67, 0x89, 0x0F, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type4[6] = {0x67, 0x89, 0x01, 0x23, 0xFF, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type5[6] = {0x78, 0x90, 0x12, 0x34, 0x5F, 0xFF};
const BYTE unitExpectedRearrangeLastBuf_Type6[6] = {0x78, 0x90, 0x12, 0x34, 0x56, 0xFF};
*/

const BYTE *unitTenKeyTestProcedure[PINCODE_TEST_NUM][5] = { 
	 {unitTenKeySaveData_Type1, unitExpectedInputKeyFromBegin_Type1, unitExpectedInputKeyToEnd_Type1, unitExpectedRearrangeFirstBuf_Type1, unitExpectedRearrangeLastBuf_Type1}
    ,{unitTenKeySaveData_Type2, unitExpectedInputKeyFromBegin_Type2, unitExpectedInputKeyToEnd_Type2, unitExpectedRearrangeFirstBuf_Type2, unitExpectedRearrangeLastBuf_Type2}
	,{unitTenKeySaveData_Type3, unitExpectedInputKeyFromBegin_Type3, unitExpectedInputKeyToEnd_Type3, unitExpectedRearrangeFirstBuf_Type3, unitExpectedRearrangeLastBuf_Type3}
	,{unitTenKeySaveData_Type4, unitExpectedInputKeyFromBegin_Type4, unitExpectedInputKeyToEnd_Type4, unitExpectedRearrangeFirstBuf_Type4, unitExpectedRearrangeLastBuf_Type4}
	,{unitTenKeySaveData_Type5, unitExpectedInputKeyFromBegin_Type5, unitExpectedInputKeyToEnd_Type5, unitExpectedRearrangeFirstBuf_Type5, unitExpectedRearrangeLastBuf_Type5}
	,{unitTenKeySaveData_Type6, unitExpectedInputKeyFromBegin_Type6, unitExpectedInputKeyToEnd_Type6, unitExpectedRearrangeFirstBuf_Type6, unitExpectedRearrangeLastBuf_Type6}
	,{unitTenKeySaveData_Type7, unitExpectedInputKeyFromBegin_Type7, unitExpectedInputKeyToEnd_Type7, unitExpectedRearrangeFirstBuf_Type7, unitExpectedRearrangeLastBuf_Type7}
	,{unitTenKeySaveData_Type8, unitExpectedInputKeyFromBegin_Type8, unitExpectedInputKeyToEnd_Type8, unitExpectedRearrangeFirstBuf_Type8, unitExpectedRearrangeLastBuf_Type8}
	,{unitTenKeySaveData_Type9, unitExpectedInputKeyFromBegin_Type9, unitExpectedInputKeyToEnd_Type9, unitExpectedRearrangeFirstBuf_Type9, unitExpectedRearrangeLastBuf_Type9}
};


BYTE gErrorCodeBuf[PINCODE_TEST_NUM];


//------------------------------------------------------------------------------
/** 	@brief	Unit test For Ten Key Save Process  
	@param 	None 
	@return 	[1] 테스트 성공
	@return 	[0] 테스트 실패
	@remark 입력된 Ten Key가 제대로 저장, 처리되는지 확인하는 테스트 함수
	@remark 테스트 대상 함수 : TenKeySave, RearrangeTenKeyBufForFirstCompare
							, RearrangeShiftTenKeyBufForLastCompare
*/
//------------------------------------------------------------------------------
BYTE UnitTestTenKeySave(void)
{
	BYTE bTmp;
	BYTE bCnt;
	BYTE bLoop;
	BYTE RearrangeFirstBuf[(MAX_PIN_LENGTH>>1)];
	BYTE RearrangeLastBuf[(MAX_PIN_LENGTH>>1)];
	
	memset(gErrorCodeBuf, 0x00, (MAX_PIN_LENGTH>>1));

	for(bLoop = 0; bLoop < PINCODE_TEST_NUM; bLoop++)
	{
		TenKeyVariablesClear();
		for(bCnt = 0; bCnt < unitTenKeyTestSize[bLoop]; bCnt++)
		{
			TenKeySave(MAX_PIN_LENGTH, (unitTenKeyTestProcedure[bLoop][0])[bCnt], 1);
		}	

		bTmp = (BYTE)memcmp(unitTenKeyTestProcedure[bLoop][1], gPinInputKeyFromBeginBuf, (MAX_PIN_LENGTH>>1));
		if(bTmp)
		{
			gErrorCodeBuf[bLoop] |= 0x01;
		}

		bTmp = (BYTE)memcmp(unitTenKeyTestProcedure[bLoop][2], gPinInputKeyToEndBuf, (MAX_PIN_LENGTH>>1));
		if(bTmp)
		{
			gErrorCodeBuf[bLoop] |= 0x02;
		}

		memset(RearrangeFirstBuf, 0xFF, (MAX_PIN_LENGTH>>1));
		RearrangeTenKeyBufForFirstCompare(RearrangeFirstBuf, unitTenKeyByteCnt[bLoop]);	
	
		bTmp = (BYTE)memcmp(unitTenKeyTestProcedure[bLoop][3], RearrangeFirstBuf, (MAX_PIN_LENGTH>>1));
		if(bTmp)
		{
			gErrorCodeBuf[bLoop] |= 0x04;
		}
	
		memset(RearrangeLastBuf, 0xFF, 6);
		RearrangeShiftTenKeyBufForLastCompare(RearrangeLastBuf, unitTenKeyByteCnt[bLoop]);	

		bTmp = (BYTE)memcmp(unitTenKeyTestProcedure[bLoop][4], RearrangeLastBuf, (MAX_PIN_LENGTH>>1));
		if(bTmp)
		{
			gErrorCodeBuf[bLoop] |= 0x08;
		}
	}

	if(DataCompare(gErrorCodeBuf,0x00,(MAX_PIN_LENGTH>>1)) == 0)
	{
		// 테스트 성공
		return 1;
	}		
	else
	{
		//테스트 실패
		return 0;
	}		
}

/*
BYTE UnitTestTenKeySave(void)
{
	BYTE bTmp;
	BYTE bCnt;
	BYTE bLoop;
	BYTE RearrangeFirstBuf[6];
	BYTE RearrangeLastBuf[6];
	
	memset(gErrorCodeBuf, 0x00, 6);

	for(bLoop = 0; bLoop < 6; bLoop++)
	{
		TenKeyVariablesClear();
		for(bCnt = 0; bCnt < unitTenKeyTestSize[bLoop]; bCnt++)
		{
			TenKeySave(12, (unitTenKeyTestProcedure[bLoop][0])[bCnt], 1);
		}	

		bTmp = (BYTE)memcmp(unitTenKeyTestProcedure[bLoop][1], gPinInputKeyFromBeginBuf, 6);
		if(bTmp)
		{
			gErrorCodeBuf[bLoop] |= 0x01;
		}

		bTmp = (BYTE)memcmp(unitTenKeyTestProcedure[bLoop][2], gPinInputKeyToEndBuf, 6);
		if(bTmp)
		{
			gErrorCodeBuf[bLoop] |= 0x02;
		}

		memset(RearrangeFirstBuf, 0xFF, 6);
		RearrangeTenKeyBufForFirstCompare(RearrangeFirstBuf, (5+bLoop));	
	
		bTmp = (BYTE)memcmp(unitTenKeyTestProcedure[bLoop][3], RearrangeFirstBuf, 6);
		if(bTmp)
		{
			gErrorCodeBuf[bLoop] |= 0x04;
		}
	
		memset(RearrangeLastBuf, 0xFF, 6);
		RearrangeShiftTenKeyBufForLastCompare(RearrangeLastBuf, (5+bLoop));	

		bTmp = (BYTE)memcmp(unitTenKeyTestProcedure[bLoop][4], RearrangeLastBuf, 6);
		if(bTmp)
		{
			gErrorCodeBuf[bLoop] |= 0x08;
		}
	}

	if(DataCompare(gErrorCodeBuf,0x00,6) == 0)
	{
		// 테스트 성공
		return 1;
	}		
	else
	{
		//테스트 실패
		return 0;
	}		
}
*/

// Sun, Tue ; 09:00 ~ 18:00
const BYTE unitTestScheduleData4byte_Type1[4] = {0x50, 0x21, 0xC2, 0x1C};

// Mon, Wed, Thu ; 13:00 ~ 20:30
const BYTE unitTestScheduleData4byte_Type2[4] = {0x2C, 0x30, 0xC1, 0xC2};

// Sun, Tue ; 09:00 ~ 08:00
const BYTE unitTestScheduleData4byte_Type3[4] = {0x50, 0x21, 0xC5, 0x64};

// Tue, Fri, Sat ; 21:00 ~ 03:00
const BYTE unitTestScheduleData4byte_Type4[4] = {0x13, 0x4E, 0xC1, 0x68};


//Mon ; 11:30
const BYTE unitTestCurrentTime_Type1[3] = {0x20, 0x02, 0xB2};

//Tue ; 09:30
const BYTE unitTestCurrentTime_Type2[3] = {0x10, 0x02, 0x3A};

//Wed ; 14:00
const BYTE unitTestCurrentTime_Type3[3] = {0x08, 0x03, 0x48};

//Thu ; 20:00
const BYTE unitTestCurrentTime_Type4[3] = {0x04, 0x04, 0xB0};

//Fri ; 24:00
const BYTE unitTestCurrentTime_Type5[3] = {0x02, 0x05, 0xA0};

//Sat ; 23:59
const BYTE unitTestCurrentTime_Type6[3] = {0x01, 0x05, 0x9F};

//Sun ; 21:00
const BYTE unitTestCurrentTime_Type7[3] = {0x40, 0x04, 0xEC};

//Mon ; 07:00
const BYTE unitTestCurrentTime_Type8[3] = {0x20, 0x01, 0xA4};



const BYTE unitTestExpectedResult_Type1[8] = {0x0F		// 0x01|0x02|0x04|0x08
											,0x0A		// 0x02|0x08
											,0x0D		// 0x01|0x04|0x08
											,0x0D		// 0x01|0x04|0x08
											,0x07		// 0x01|0x02|0x04
											,0x07		// 0x01|0x02|0x04
											,0x0B		// 0x01|0x02|0x08
											,0x0B		// 0x01|0x02|0x08
										};


const BYTE *unitScheduleTestProcedure[4]= {
	unitTestScheduleData4byte_Type1, unitTestScheduleData4byte_Type2, unitTestScheduleData4byte_Type3, unitTestScheduleData4byte_Type4
};

const BYTE *unitTimeTestProcedure[8]= {
	unitTestCurrentTime_Type1, unitTestCurrentTime_Type2, unitTestCurrentTime_Type3, unitTestCurrentTime_Type4
   ,unitTestCurrentTime_Type5, unitTestCurrentTime_Type6, unitTestCurrentTime_Type7, unitTestCurrentTime_Type8		
};

//------------------------------------------------------------------------------
/** @brief	Unit test For Pincode Schedule Process  
	@param 	None 
	@return [1] 테스트 성공
	@return [0] 테스트 실패
	@remark Schedule 처리 함수가 정상적으로 동작하는지 확인하는 테스트 함수
	@remark 테스트 대상 함수 : VerifyScheduleOfInputPincode
*/
//------------------------------------------------------------------------------
BYTE UnitTestSchedule_Type1(void)
{
	BYTE CurrentTimeLoop;
	BYTE ScheduleLoop;
	BYTE ErrorCodeUnit;
	BYTE RetValue;

	memset(gErrorCodeBuf, 0x00, 8);
	for(CurrentTimeLoop = 0; CurrentTimeLoop < 8; CurrentTimeLoop++)
	{
		ErrorCodeUnit = 0;
		for(ScheduleLoop = 0; ScheduleLoop < 4; ScheduleLoop++)
		{
			RetValue = VerifyScheduleOfInputPincode((BYTE*)unitScheduleTestProcedure[ScheduleLoop], (BYTE*)unitTimeTestProcedure[CurrentTimeLoop]);	
			if(RetValue == 0)
			{
				ErrorCodeUnit |= (1<<ScheduleLoop);
			}
		}
		gErrorCodeBuf[CurrentTimeLoop] = ErrorCodeUnit;
	}

	if(memcmp(gErrorCodeBuf,unitTestExpectedResult_Type1,8) == 0)
	{
		// 테스트 성공
		return 1;
	}		
	else
	{
		//테스트 실패
		return 0;
	}		
}

#endif

